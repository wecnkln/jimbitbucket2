﻿''' <summary>
''' Class operates the JTnjram panel. It queries operator about new accounts being entered and whether they should be added
''' as a new account.
''' </summary>
Public Class JTnjram

    Dim JTnjramFlag As Boolean = False
    Dim JTnjramAction As String = ""
    Public Function JTnjramInit(ByRef tmpNewAcct As String, ByRef tmpAcctDesc As String) As String
        JTnjramAction = ""
        JTnjramFlag = False
        txtJTnjramAccount.Text = tmpNewAcct
        txtJTnjramDescription.Text = ""
        Me.Show()
        '
        Do Until JTnjramFlag
            ' Application.DoEvents()
            wait(3)
        Loop
        Me.Hide()
        Return JTnjramAction
    End Function


    ''' <summary>Re: adding new accounts. This sub is queued when the operator does not want to add an account.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTnjramNo_Click(sender As Object, e As EventArgs) Handles btnJTnjramNo.Click
        Me.Hide()
        JTnjramAction = "DONOTADD"
        JTnjramFlag = True
    End Sub

    ''' <summary>
    ''' Re: adding new accounts. This sub is called when the operator want to add the accounts. The sub checkas to make sure a description has been entered before creating the account. If also queues rebuilding the autofill table.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTnjramYes_Click(sender As Object, e As EventArgs) Handles btnJTnjramYes.Click
        If txtJTnjramDescription.Text = "" Then
            txtJTnjramDescription.Select()
            Exit Sub
        End If
        If JTVarMaint.JTvmValidateUpdateNJREA("Update", txtJTnjramAccount.Text, txtJTnjramDescription.Text) = "UPDATED" Then
            Me.Hide()
            JTnjramAction = "ADDED"
            JTnjramFlag = True
        Else
            MsgBox("NJR Account not updated = JTnjram~42")
        End If
        Me.Hide()
        JTnjramFlag = True
    End Sub

End Class