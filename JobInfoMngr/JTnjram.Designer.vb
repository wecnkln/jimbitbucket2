﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTnjram
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.btnJTnjramYes = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtJTnjramDescription = New System.Windows.Forms.TextBox()
        Me.btnJTnjramNo = New System.Windows.Forms.Button()
        Me.txtJTnjramAccount = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(5, 43)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(106, 17)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Description --->"
        '
        'btnJTnjramYes
        '
        Me.btnJTnjramYes.Location = New System.Drawing.Point(122, 238)
        Me.btnJTnjramYes.Name = "btnJTnjramYes"
        Me.btnJTnjramYes.Size = New System.Drawing.Size(181, 50)
        Me.btnJTnjramYes.TabIndex = 5
        Me.btnJTnjramYes.Text = "Yes" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Add New Account"
        Me.btnJTnjramYes.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(15, 10)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(280, 18)
        Me.Label24.TabIndex = 4
        Me.Label24.Text = "Add account description prior to updating."
        '
        'txtJTnjramDescription
        '
        Me.txtJTnjramDescription.Location = New System.Drawing.Point(133, 43)
        Me.txtJTnjramDescription.Name = "txtJTnjramDescription"
        Me.txtJTnjramDescription.Size = New System.Drawing.Size(252, 22)
        Me.txtJTnjramDescription.TabIndex = 3
        '
        'btnJTnjramNo
        '
        Me.btnJTnjramNo.Location = New System.Drawing.Point(172, 87)
        Me.btnJTnjramNo.Name = "btnJTnjramNo"
        Me.btnJTnjramNo.Size = New System.Drawing.Size(83, 48)
        Me.btnJTnjramNo.TabIndex = 2
        Me.btnJTnjramNo.Text = "No"
        Me.btnJTnjramNo.UseVisualStyleBackColor = True
        '
        'txtJTnjramAccount
        '
        Me.txtJTnjramAccount.Location = New System.Drawing.Point(140, 56)
        Me.txtJTnjramAccount.Name = "txtJTnjramAccount"
        Me.txtJTnjramAccount.ReadOnly = True
        Me.txtJTnjramAccount.Size = New System.Drawing.Size(145, 22)
        Me.txtJTnjramAccount.TabIndex = 1
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(27, 9)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(376, 36)
        Me.Label22.TabIndex = 109
        Me.Label22.Text = "Account entered  is a new account. Do you want " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "to add it to the Non-Job-Related" &
    " account list."
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.txtJTnjramDescription)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 145)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(401, 82)
        Me.GroupBox1.TabIndex = 110
        Me.GroupBox1.TabStop = False
        '
        'JTnjram
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(431, 301)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.btnJTnjramYes)
        Me.Controls.Add(Me.btnJTnjramNo)
        Me.Controls.Add(Me.txtJTnjramAccount)
        Me.Name = "JTnjram"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - NJR Account Maintenance"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label25 As Label
    Friend WithEvents btnJTnjramYes As Button
    Friend WithEvents Label24 As Label
    Friend WithEvents txtJTnjramDescription As TextBox
    Friend WithEvents btnJTnjramNo As Button
    Friend WithEvents txtJTnjramAccount As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents GroupBox1 As GroupBox
End Class
