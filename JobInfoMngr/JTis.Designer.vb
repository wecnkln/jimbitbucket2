﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTis))
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtJTisChgToJobDol = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnDeleteItem = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnAddUpdate = New System.Windows.Forms.Button()
        Me.txtJTisChgToJob = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtJTisUnitCost = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtJTisItem = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtJTisSellUnit = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtJTisPrVerSrce = New System.Windows.Forms.TextBox()
        Me.txtJTisPrVerInv = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtJTisCategory = New System.Windows.Forms.TextBox()
        Me.btnSessionCancel = New System.Windows.Forms.Button()
        Me.btnSessionComplete = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.JTssChgToJob = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssPrVerInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssPrVerSrce = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssCstPErUnit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssSellingUnit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssItem = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTssCategory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTisData = New System.Windows.Forms.ListView()
        Me.SLKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(12, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(326, 29)
        Me.Label11.TabIndex = 31
        Me.Label11.Text = "In-Stock Material Selection"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox2.Controls.Add(Me.txtJTisChgToJobDol)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.btnDeleteItem)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.btnAddUpdate)
        Me.GroupBox2.Controls.Add(Me.txtJTisChgToJob)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtJTisUnitCost)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtJTisItem)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtJTisSellUnit)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtJTisPrVerSrce)
        Me.GroupBox2.Controls.Add(Me.txtJTisPrVerInv)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtJTisCategory)
        Me.GroupBox2.Location = New System.Drawing.Point(1027, 111)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(368, 435)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        '
        'txtJTisChgToJobDol
        '
        Me.txtJTisChgToJobDol.Location = New System.Drawing.Point(136, 326)
        Me.txtJTisChgToJobDol.Name = "txtJTisChgToJobDol"
        Me.txtJTisChgToJobDol.ReadOnly = True
        Me.txtJTisChgToJobDol.Size = New System.Drawing.Size(100, 22)
        Me.txtJTisChgToJobDol.TabIndex = 7
        Me.txtJTisChgToJobDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 331)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 17)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Chg to Job($$$$)"
        '
        'btnDeleteItem
        '
        Me.btnDeleteItem.Location = New System.Drawing.Point(184, 372)
        Me.btnDeleteItem.Name = "btnDeleteItem"
        Me.btnDeleteItem.Size = New System.Drawing.Size(145, 42)
        Me.btnDeleteItem.TabIndex = 20
        Me.btnDeleteItem.Text = "Delete Item"
        Me.btnDeleteItem.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(159, 218)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 17)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Source"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(270, 218)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 17)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Invoice #"
        '
        'btnAddUpdate
        '
        Me.btnAddUpdate.Location = New System.Drawing.Point(17, 372)
        Me.btnAddUpdate.Name = "btnAddUpdate"
        Me.btnAddUpdate.Size = New System.Drawing.Size(145, 42)
        Me.btnAddUpdate.TabIndex = 16
        Me.btnAddUpdate.Text = "Add/Update"
        Me.btnAddUpdate.UseVisualStyleBackColor = True
        '
        'txtJTisChgToJob
        '
        Me.txtJTisChgToJob.Location = New System.Drawing.Point(136, 284)
        Me.txtJTisChgToJob.Name = "txtJTisChgToJob"
        Me.txtJTisChgToJob.Size = New System.Drawing.Size(100, 22)
        Me.txtJTisChgToJob.TabIndex = 6
        Me.txtJTisChgToJob.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 289)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(118, 17)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Chg to Job(Units)"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(234, 138)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(109, 17)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "(Ea., bf, lf,  etc.)"
        '
        'txtJTisUnitCost
        '
        Me.txtJTisUnitCost.Location = New System.Drawing.Point(133, 177)
        Me.txtJTisUnitCost.Name = "txtJTisUnitCost"
        Me.txtJTisUnitCost.Size = New System.Drawing.Size(100, 22)
        Me.txtJTisUnitCost.TabIndex = 3
        Me.txtJTisUnitCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(4, 67)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 17)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "item"
        '
        'txtJTisItem
        '
        Me.txtJTisItem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTisItem.Location = New System.Drawing.Point(133, 64)
        Me.txtJTisItem.Multiline = True
        Me.txtJTisItem.Name = "txtJTisItem"
        Me.txtJTisItem.Size = New System.Drawing.Size(201, 42)
        Me.txtJTisItem.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 17)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Selling Unit"
        '
        'txtJTisSellUnit
        '
        Me.txtJTisSellUnit.Location = New System.Drawing.Point(134, 135)
        Me.txtJTisSellUnit.Name = "txtJTisSellUnit"
        Me.txtJTisSellUnit.Size = New System.Drawing.Size(78, 22)
        Me.txtJTisSellUnit.TabIndex = 2
        Me.txtJTisSellUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 182)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Cost per unit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 241)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Price Verification"
        '
        'txtJTisPrVerSrce
        '
        Me.txtJTisPrVerSrce.Location = New System.Drawing.Point(136, 238)
        Me.txtJTisPrVerSrce.Name = "txtJTisPrVerSrce"
        Me.txtJTisPrVerSrce.Size = New System.Drawing.Size(100, 22)
        Me.txtJTisPrVerSrce.TabIndex = 4
        '
        'txtJTisPrVerInv
        '
        Me.txtJTisPrVerInv.Location = New System.Drawing.Point(251, 238)
        Me.txtJTisPrVerInv.Name = "txtJTisPrVerInv"
        Me.txtJTisPrVerInv.Size = New System.Drawing.Size(100, 22)
        Me.txtJTisPrVerInv.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Category"
        '
        'txtJTisCategory
        '
        Me.txtJTisCategory.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTisCategory.Location = New System.Drawing.Point(134, 19)
        Me.txtJTisCategory.Name = "txtJTisCategory"
        Me.txtJTisCategory.Size = New System.Drawing.Size(124, 22)
        Me.txtJTisCategory.TabIndex = 0
        '
        'btnSessionCancel
        '
        Me.btnSessionCancel.Location = New System.Drawing.Point(184, 24)
        Me.btnSessionCancel.Name = "btnSessionCancel"
        Me.btnSessionCancel.Size = New System.Drawing.Size(145, 42)
        Me.btnSessionCancel.TabIndex = 25
        Me.btnSessionCancel.Text = "Cancel Session"
        Me.btnSessionCancel.UseVisualStyleBackColor = True
        '
        'btnSessionComplete
        '
        Me.btnSessionComplete.Location = New System.Drawing.Point(17, 24)
        Me.btnSessionComplete.Name = "btnSessionComplete"
        Me.btnSessionComplete.Size = New System.Drawing.Size(145, 42)
        Me.btnSessionComplete.TabIndex = 24
        Me.btnSessionComplete.Text = "Session Complete"
        Me.btnSessionComplete.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox1.Controls.Add(Me.btnSessionCancel)
        Me.GroupBox1.Controls.Add(Me.btnSessionComplete)
        Me.GroupBox1.Location = New System.Drawing.Point(1027, 558)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 81)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'JTssChgToJob
        '
        Me.JTssChgToJob.Text = "Chg to Job"
        Me.JTssChgToJob.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.JTssChgToJob.Width = 80
        '
        'JTssPrVerInv
        '
        Me.JTssPrVerInv.Text = "Pr Ver Inv #"
        Me.JTssPrVerInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.JTssPrVerInv.Width = 80
        '
        'JTssPrVerSrce
        '
        Me.JTssPrVerSrce.Text = "Pr Ver Source"
        Me.JTssPrVerSrce.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.JTssPrVerSrce.Width = 100
        '
        'JTssCstPErUnit
        '
        Me.JTssCstPErUnit.Text = "Cst Per Unit"
        Me.JTssCstPErUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.JTssCstPErUnit.Width = 85
        '
        'JTssSellingUnit
        '
        Me.JTssSellingUnit.Text = "Selling Unit"
        Me.JTssSellingUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.JTssSellingUnit.Width = 85
        '
        'JTssItem
        '
        Me.JTssItem.Text = "Item"
        Me.JTssItem.Width = 155
        '
        'JTssCategory
        '
        Me.JTssCategory.Text = "Category"
        Me.JTssCategory.Width = 95
        '
        'lvJTisData
        '
        Me.lvJTisData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.JTssCategory, Me.JTssItem, Me.JTssSellingUnit, Me.JTssCstPErUnit, Me.JTssPrVerSrce, Me.JTssPrVerInv, Me.JTssChgToJob, Me.SLKey})
        Me.lvJTisData.FullRowSelect = True
        Me.lvJTisData.HideSelection = False
        Me.lvJTisData.Location = New System.Drawing.Point(8, 111)
        Me.lvJTisData.MultiSelect = False
        Me.lvJTisData.Name = "lvJTisData"
        Me.lvJTisData.Size = New System.Drawing.Size(1011, 528)
        Me.lvJTisData.TabIndex = 27
        Me.lvJTisData.UseCompatibleStateImageBehavior = False
        Me.lvJTisData.View = System.Windows.Forms.View.Details
        '
        'SLKey
        '
        Me.SLKey.Width = 0
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(765, 27)
        Me.TextBox9.Multiline = True
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(630, 61)
        Me.TextBox9.TabIndex = 30
        Me.TextBox9.Text = resources.GetString("TextBox9.Text")
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'JTis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1407, 653)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lvJTisData)
        Me.Controls.Add(Me.TextBox9)
        Me.Name = "JTis"
        Me.Text = "Job Information Manager - In-Stock Material"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label11 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtJTisChgToJobDol As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnDeleteItem As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents btnAddUpdate As Button
    Friend WithEvents txtJTisChgToJob As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtJTisUnitCost As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtJTisItem As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtJTisSellUnit As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtJTisPrVerSrce As TextBox
    Friend WithEvents txtJTisPrVerInv As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtJTisCategory As TextBox
    Friend WithEvents btnSessionCancel As Button
    Friend WithEvents btnSessionComplete As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents JTssChgToJob As ColumnHeader
    Friend WithEvents JTssPrVerInv As ColumnHeader
    Friend WithEvents JTssPrVerSrce As ColumnHeader
    Friend WithEvents JTssCstPErUnit As ColumnHeader
    Friend WithEvents JTssSellingUnit As ColumnHeader
    Friend WithEvents JTssItem As ColumnHeader
    Friend WithEvents JTssCategory As ColumnHeader
    Friend WithEvents lvJTisData As ListView
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents SLKey As ColumnHeader
End Class
