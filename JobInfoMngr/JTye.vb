﻿Public Class JTye
    Public JTyeInProgress As Boolean = False
    Public JTyeMenuPurge As Boolean = False
    Public Sub JTyeInitYERoutines()
        txtJTyeCalendarYear.Text = System.DateTime.Now.Year.ToString
        txtJTyeSystemYear.Text = JTVarMaint.JTvmSystemYear
        gboxJTyeJobPurge.Visible = False
        TextBox1.Visible = True
        Me.MdiParent = JTstart
        tboxJTyeYEprocStep1.Visible = False
        tboxJTyeYEprocStep1a.Visible = False
        tboxJTyeYEprocStep2.Visible = False
        '       tboxJTyeYEprocStep3.Visible = False
        '      tboxJTyeYEprocStep4.Visible = False
        tboxJTyeYEprocStep5.Visible = False
        btnJTyeDone.Visible = False
        btnJTyeProcessYE.Select()
        Me.Show()
    End Sub
    Private Sub btnJTyeProcessYE_Click(sender As Object, e As EventArgs) Handles btnJTyeProcessYE.Click
        JTyeProcessYE()
    End Sub

    Private Function JTyeProcessYE()

        JTyeInProgress = True
        '
        btnJTyeDeferYE.Visible = False
        btnJTyeProcessYE.Visible = False
        If JTyeMenuPurge = False Then
            tboxJTyeYEprocStep1.Visible = True
            tboxJTyeYEprocStep1.Select()
        End If
        ' -----------------------------------------------------------
        ' Logic to print Invoice Report as part of the Y/E routines
        ' is comments out. Currently has a bug where no invoice
        ' records are selected for the report ... not right!!!
        ' -----------------------------------------------------------

        JTjim.JTjimJobArrayRestore()
        JTaup.JTaupLoad()
        JTas.JTasHistLoad()
        ' Print YE Invoice Report
        '
        '    Two version are printed ... 
        '         one for primary invoice sequence ... 
        '         the Second for alternate.
        '
        JTir.rbtnJTirCustom.Checked = True
        JTir.cboxPrimary.Checked = True
        JTir.cboxAlternate.Checked = False
        JTir.txtJTirStMth.Text = "01"
        JTir.txtJTirEndMth.Text = "12"
        JTir.txtJTirStYr.Text = JTVarMaint.JTvmSystemYear
        JTir.txtJTirEndYr.Text = JTVarMaint.JTvmSystemYear
        JTvPDF.JTvPDFDelayFlag = True
        If JTyeMenuPurge = False Then
            JTir.JTirBldPrntRprt()

            ' Delay loop to stop processing while Invoice Report PDF is being displayed
            Do While JTvPDF.JTvPDFDelayFlag = True
                Threading.Thread.Sleep(500) ' 500 milliseconds = 0.5 seconds
                Application.DoEvents()
            Loop
        End If
        '
        JTir.rbtnJTirCustom.Checked = True
        JTir.cboxPrimary.Checked = False
        JTir.cboxAlternate.Checked = True
        JTir.txtJTirStMth.Text = "01"
        JTir.txtJTirEndMth.Text = "12"
        JTir.txtJTirStYr.Text = JTVarMaint.JTvmSystemYear
        JTir.txtJTirEndYr.Text = JTVarMaint.JTvmSystemYear
        JTvPDF.JTvPDFDelayFlag = True
        If JTyeMenuPurge = False Then
            JTir.JTirBldPrntRprt()

            ' Delay loop to stop processing while Invoice Report PDF is being displayed
            Do While JTvPDF.JTvPDFDelayFlag = True
                Threading.Thread.Sleep(500) ' 500 milliseconds = 0.5 seconds
                Application.DoEvents()
            Loop
        End If
        '
        If JTyeMenuPurge = False Then
            tboxJTyeYEprocStep1a.Visible = True
            tboxJTyeYEprocStep1a.Select()
        End If

        '
        If JTyeMenuPurge = False Then
            ' Execute YE backup
            If JTstart.JTstBackup("Y", "") = "ABORTED" Then
                Me.Close()
                JTstart.btnSignIn_Click_Continued()
                Return False
            End If
        End If
        Me.Show()
        If JTyeMenuPurge = False Then
            tboxJTyeYEprocStep2.Visible = True
            tboxJTyeYEprocStep2.Select()
        End If
        '
        JTyeLoadListView()
        gboxJTyeJobPurge.Visible = True
        TextBox1.Visible = False
        Return True
    End Function
    Private Sub btnJTyePurgeJobs_Click(sender As Object, e As EventArgs) Handles btnJTyePurgeJobs.Click
        ' --------------------------------------------------------------
        ' Logic to physically purge records related to selected jobs.
        ' -     -     -     -     -     -     -     -     -     -
        ' Record will be deleted from JTjim (including job specific pricing),
        ' JTinvhist, JTaup. Job History report is also generated and printed for
        ' job being purged.
        ' --------------------------------------------------------------
        '
        Dim tmpPrntJobRprtFlag As Boolean = False
        Dim Msg, Style, Title, Response
        Msg = "Do you want to print a Job Report for each Job being purged?"    ' Define message.
        Style = vbYesNo     ' Define buttons.
        Title = "Menu Purge Print"    ' Define title.
        '       Help = ""    ' Define Help file.
        '      Ctxt = 1000    ' Define topic context. 
        ' Display message.
        Response = MsgBox(Msg, Style, Title)
        If Response = vbYes Then    ' User chose Yes.
            tmpPrntJobRprtFlag = True    ' Perform some action.
        Else    ' User chose No.
            tmpPrntJobRprtFlag = False    ' Perform some action.
        End If
        Dim tmpNumJobsPurged As Integer = 0
        If lvJTyePurgeElig.CheckedItems.Count = 0 Then
            JTyeProcessYE_Continued()
        Else
            gboxJTyePurgeStatus.Visible = True
            tboxJTyeJPJobsToPurge.Text = lvJTyePurgeElig.CheckedItems.Count.ToString
            For i = 0 To lvJTyePurgeElig.CheckedItems.Count - 1
                Dim tmppjob As String = lvJTyePurgeElig.CheckedItems(i).Text
                Dim tmppsubjob As String = lvJTyePurgeElig.CheckedItems(i).SubItems(1).Text
                Dim tmppjobtype As String = lvJTyePurgeElig.CheckedItems(i).SubItems(2).Text
                Dim tmppjobcstmeth As String = lvJTyePurgeElig.CheckedItems(i).SubItems(3).Text
                If tmpPrntJobRprtFlag = True Then
                    'call routine to build job report for job to be purged
                    ' 
                    ' Settings in call set report to ...
                    '     - show margin,
                    '     - do not consolidate master and subjobs
                    '     - print activity for the life of the job.
                    '
                    JTvPDF.JTvPDFDelayFlag = True
                    JTjr.JTjrBuildReport("Y/E Purge", tmppjob, tmppsubjob, True, False, True)
                    ' Code to delay processing while JTjr executes and displays
                    Do While JTvPDF.JTvPDFDelayFlag = True
                        Threading.Thread.Sleep(500) ' 500 milliseconds = 0.5 seconds
                        Application.DoEvents()
                    Loop
                End If
                ' logic to purge job here
                ' delete records for selected job from ...
                '     - job info
                '     - job specific pricing
                '     - invoice history
                '     - AUP invoice and data
                '
                '            gboxJTyePurgeStatus.Visible = False
                '
                JTas.JtasPurgeJobInvHistory(tmppjob, tmppsubjob)
                JTjim.JTjimPurgeJobSpecificPricing(tmppjob, tmppsubjob)
                JTjim.JTjimPurgeJobInfoRecord(tmppjob, tmppsubjob)
                JTaup.JTaupPurgeAUPInfo(tmppjob, tmppsubjob)
                tmpNumJobsPurged += 1
                tboxJTyeJPJobsPurged.Text = tmpNumJobsPurged.ToString
                Dim tmpPercentComplete As Integer =
                    (tmpNumJobsPurged / lvJTyePurgeElig.CheckedItems.Count) * 100
                tboxJTyeJPPCComp.Text = String.Format("{0:000.0}", tmpPercentComplete)

                gboxJTyePurgeStatus.Visible = True
                '
                ' ------------------------------------------------------------------------------
                ' delay put in here so that screen will visibly update as jobs are being purged.
                ' not quite sure why this needs to be done.
                ' ------------------------------------------------------------------------------
                Threading.Thread.Sleep(300) ' 300 milliseconds = 0.3 seconds
                Application.DoEvents()

            Next i
        End If
        MsgBox("Purge Complete...")
        '
        ' Purge completed
        '
        If JTyeMenuPurge = False Then
            JTyeProcessYE_Continued()
        End If
        Me.Hide()
    End Sub

    Private Sub btnJTyeAbortJobPurge_Click(sender As Object, e As EventArgs) Handles btnJTyeAbortJobPurge.Click
        If JTyeMenuPurge = False Then
            tboxJTyeYEprocStep5.Text = "Job purge not completed --- Operator Abort."
            tboxJTyeYEprocStep5.Visible = True
            JTyeProcessYE_Continued()
        Else
        End If
        Me.Hide()
    End Sub

    Sub JTyeProcessYE_Continued()
        '
        ' Update system date to calendar date.
        '
        JTVarMaint.JTvmSystemYear = System.DateTime.Now.Year.ToString
        JTVarMaint.JTvmFlush()
        '
        gboxJTyeJobPurge.Visible = False
        TextBox1.Visible = True
        btnJTyeDone.Visible = True
        btnJTyeDone.Select()
    End Sub
    Private Sub btnJTyeDeferYE_Click(sender As Object, e As EventArgs) Handles btnJTyeDeferYE.Click
        Me.Close()
        JTstart.btnSignIn_Click_Continued()
    End Sub
    Private Sub btnJTyeDone_Click(sender As Object, e As EventArgs) Handles btnJTyeDone.Click
        Me.Close()
        JTyeInProgress = False
        JTstart.btnSignIn_Click_Continued()
    End Sub
    Private Function JTyeLoadListView() As String
        ' load NLC Array
        JTnlc.JTnlcArrayLoad()
        ' load tk array
        JTtk.JTtkRestoreTKFile()
        ' load aup array
        JTaup.JTaupLoad()
        ' Loop through jobs looking for purge candidates.
        Dim JTjimfPOS As Integer = 0
        Dim JTjimfJobName As String = ""
        Dim JTjimfSubname As String = ""
        Dim JTjimfDeactDate As String = ""
        Dim JTjimfJobType As String = ""
        Dim JTjimfCstMeth As String = ""
        Dim tmpLstInvDate As String = ""
        Dim tmpPurgeElig As Boolean = True
        '
        lvJTyePurgeElig.Items.Clear()
        Dim itmBuild(5) As String
        Dim lvLine As New ListViewItem
        '
        Do Until JTjim.JTjimReadFunc(JTjimfPOS, JTjimfJobName, JTjimfSubname, JTjimfDeactDate,
                           JTjimfJobType, JTjimfCstMeth) <> "DATA"
            tmpPurgeElig = True
            ' Retrieve and check last invoice date.
            tmpLstInvDate = ""
            If JTas.JTasHasBeenInvd(JTjimfJobName, JTjimfSubname, tmpLstInvDate) = True Then
                Dim tmpDateTime As DateTime = CDate(tmpLstInvDate)
                If tmpDateTime.Year >= JTVarMaint.JTvmSystemYear Then
                    tmpPurgeElig = False
                End If
            End If
            ' Check for labor items.
            If tmpPurgeElig = True Then
                If JTtk.JTtkJobHasActivity(JTjimfJobName, JTjimfSubname) = True Then
                    tmpPurgeElig = False
                End If
            End If
            ' Check for nlc items.
            If tmpPurgeElig = True Then
                If JTnlc.JTnlcJobHasActivity(JTjimfJobName, JTjimfSubname) = True Then
                    tmpPurgeElig = False
                End If
            End If
            ' Check to see if AUP jobs has been finalized.
            If tmpPurgeElig = True Then
                If JTjimfJobType = "AUP" Then
                    If JTaup.JTaupFinalFlagSet(JTjimfJobName, JTjimfSubname) = True Then
                        tmpPurgeElig = False
                    End If
                End If
            End If
            If tmpPurgeElig = True Then
                ' Job is eligible to be purged. Fill line in ListView.
                '
                itmBuild(0) = JTjimfJobName
                itmBuild(1) = JTjimfSubname
                itmBuild(2) = JTjimfJobType
                itmBuild(3) = JTjimfCstMeth
                itmBuild(4) = tmpLstInvDate
                '
                lvLine = New ListViewItem(itmBuild)
                lvJTyePurgeElig.Items.Add(lvLine)
            End If
            '
            JTjimfPOS += 1
        Loop
        '
        '       MsgBox("Data should be in purge candidate lv - JTye~377")
        '
        Dim i As Integer

        For i = 0 To lvJTyePurgeElig.Items.Count - 1
            lvJTyePurgeElig.Items(i).Checked = True
        Next i

        Return "ListViewLoaded"
    End Function
    '
    Public Function JtyeJobPurge() As Boolean
        Dim tmpReturnFlag As Boolean = True
        JTyeMenuPurge = True
        Me.Label1.Text = "Inactive Job Purge"
        Me.Label2.Visible = False
        Me.txtJTyeSystemYear.Visible = False
        Me.Label3.Visible = False
        Me.txtJTyeCalendarYear.Visible = False
        Me.tboxJTyeYEprocStep1.Visible = False
        Me.tboxJTyeYEprocStep1a.Visible = False
        Me.tboxJTyeYEprocStep2.Visible = False
        Me.tboxJTyeYEprocStep5.Visible = False
        Me.btnJTyeDone.Visible = False
        JTyeProcessYE()



        Return tmpReturnFlag
    End Function


End Class