﻿Imports System.IO
Imports System.Net
Public Class JTQBExt
    '
    Public Shared JTQBfileName As String = Nothing
    '
    Public Shared JTQBExtCustName As String = Nothing
    Public Shared JTQBExtAddr1 As String = Nothing
    Public Shared JTQBExtAddr2 As String = Nothing
    Public Shared JTQBExtAddr3 As String = Nothing
    '
    ''' <summary>
    ''' First function to be called when creating an extract file.
    ''' Calling module will send file name information that will be
    ''' save as and used to create the .iif file name. Function will also output
    ''' the header records to the extract file.
    ''' </summary>
    ''' <param name="tmpFileName"></param>
    ''' <returns></returns>
    Public Shared Function JTQBInit(tmpFileName As String) As Boolean
        JTQBfileName = tmpFileName
        Dim TRNSHeader As String = "!TRNS" & vbTab &
                                   "TRNSID" & vbTab &
                                   "TRNSTYPE" & vbTab &
                                   "DATE" & vbTab &
                                   "ACCNT" & vbTab &
                                   "NAME" & vbTab &
                                   "CLASS" & vbTab &
                                   "AMOUNT" & vbTab &
                                   "DOCNUM" & vbTab &
                                   "MEMO" & vbTab &
                                   "CLEAR" & vbTab &
                                   "TOPRINT" & vbTab &
                                   "NAMEISTAXABLE" & vbTab &
                                   "DUEDATE" & vbTab &
                                   "TERMS" & vbTab &
                                   "PAYMETH" & vbTab &
                                   "SHIPVIA" & vbTab &
                                   "SHIPDATE" & vbTab &
                                   "REP" & vbTab &
                                   "FOB" & vbTab &
                                   "PONUM" & vbTab &
                                   "INVMEMO" & vbTab &
                                   "ADDR1" & vbTab &
                                   "ADDR2" & vbTab &
                                   "ADDR3" & vbTab &
                                   "ADDR4" & vbTab &
                                   "ADDR5" & vbTab &
                                   "SADDR1" & vbTab &
                                   "SADDR2" & vbTab &
                                   "SADDR3" & vbTab &
                                   "SADDR4" & vbTab &
                                   "SADDR5"
        '
        JTQBextWrite(TRNSHeader)
        '
        Dim SPLHeader As String = "!SPL" & vbTab &
                                  "SPLID" & vbTab &
                                  "TRNSTYPE" & vbTab &
                                  "DATE" & vbTab &
                                  "ACCNT" & vbTab &
                                  "NAME" & vbTab &
                                  "CLASS" & vbTab &
                                  "AMOUNT" & vbTab &
                                  "DOCNUM" & vbTab &
                                  "MEMO" & vbTab &
                                  "CLEAR" & vbTab &
                                  "QNTY" & vbTab &
                                  "PRICE" & vbTab &
                                  "INVITEM" & vbTab &
                                  "PAYMETH" & vbTab &
                                  "TAXABLE" & vbTab &
                                  "EXTRA" & vbTab &
                                  "VATCODE" & vbTab &
                                  "VATAMOUNT" & vbTab &
                                  "VALADJ" & vbTab &
                                  "SERVICEDATE" & vbTab &
                                  "TAXCODE" & vbTab &
                                  "TAXRATE" & vbTab &
                                  "TAXAMOUNT" & vbTab &
                                  "OTHER2" & vbTab &
                                  "OTHER3"
        '
        JTQBextWrite(SPLHeader)
        '
        Dim ENDTRNSHeader As String = "!ENDTRNS"
        '
        JTQBextWrite(ENDTRNSHeader)
        '
        Dim ACCNTHeader As String = "!ACCNT" & vbTab &
                                    "NAME" & vbTab &
                                    "REFNUM" & vbTab &
                                    "ACCNTTYPE" & vbTab &
                                    "ACCNUM"
        '
        JTQBextWrite(ACCNTHeader)
        '
        Dim TIMEACTHeader As String = "!TIMEACT" & vbTab &
                                      "DATE" & vbTab &
                                      "JOB" & vbTab &
                                      "EMP" & vbTab &
                                      "ITEM" & vbTab &
                                      "PITEM" & vbTab &
                                      "DURATION" & vbTab &
                                      "PROJ" & vbTab &
                                      "NOTE" & vbTab &
                                      "XFERTOPAYROLL" & vbTab &
                                      "BILLINGSTATUS"
        '
        JTQBextWrite(TIMEACTHeader)
        '
        Dim INVITEMHeader As String = "!INVITEM" & vbTab &
                                      "NAME" & vbTab &
                                      "INVITEMTYPE" & vbTab &
                                      "DESC" & vbTab &
                                      "PURCHASEDESC" & vbTab &
                                      "ACCNT" & vbTab &
                                      "ASSETACCNT" & vbTab &
                                      "COGSACCNT" & vbTab &
                                      "PRICE" & vbTab &
                                      "COST" & vbTab &
                                      "TAXABLE" & vbTab &
                                      "PAYMETH" & vbTab &
                                      "TAXVEND" & vbTab &
                                      "TAXDIST" & vbTab &
                                      "PREFVEND" & vbTab &
                                      "REORDERPOINT" & vbTab &
                                      "EXTRA" & vbTab &
                                      "CUSTFLD1" & vbTab &
                                      "CUSTFLD2" & vbTab &
                                      "CUSTFLD3" & vbTab &
                                      "CUSTFLD4" & vbTab &
                                      "CUSTFLD5" & vbTab &
                                      "DEP_TYPE" & vbTab &
                                      "ISPASSEDTHRU" & vbTab &
                                      "HIDDEN"
        '
        JTQBextWrite(INVITEMHeader)
        '
        Dim CUSTHeader As String = "!CUST" & vbTab &
                                   "NAME" & vbTab &
                                   "BADDR1" & vbTab &
                                   "BADDR2" & vbTab &
                                   "BADDR3" & vbTab &
                                   "BADDR4" & vbTab &
                                   "BADDR5" & vbTab &
                                   "COMPANYNAME" & vbTab &
                                   "FIRSTNAME" & vbTab &
                                   "MIDINIT" & vbTab &
                                   "LASTNAME" & vbTab &
                                   "CONT1" & vbTab &
                                   "EMAIL"
        '
        JTQBextWrite(CUSTHeader)
        '
        Dim VENDHeader As String = "!VEND" & vbTab &
                                   "NAME" & vbTab &
                                   "ADDR1" & vbTab &
                                   "ADDR2" & vbTab &
                                   "ADDR3" & vbTab &
                                   "ADDR4" & vbTab &
                                   "ADDR5"
        '
        JTQBextWrite(VENDHeader)
        '
        Dim OTHERNAMEHeader As String = "!OTHERNAME" & vbTab &
                                        "NAME" & vbTab &
                                        "BADDR1" & vbTab &
                                        "BADDR2" & vbTab &
                                        "BADDR3" & vbTab &
                                        "BADDR4" & vbTab &
                                        "BADDR5"
        '
        JTQBextWrite(OTHERNAMEHeader)
        '
        Return True
    End Function
    Public Shared Function JTQBCustName(tmpJobId As String,
                                        JTjimCIContact As String,
                                        JTjimCIBillStreet As String,
                                        JTjimCIBillCity As String) As Boolean
        '
        JTQBExtCustName = tmpJobId
        JTQBExtAddr1 = JTjimCIContact
        JTQBExtAddr2 = JTjimCIBillStreet
        JTQBExtAddr3 = JTjimCIBillCity
        '
        Dim CUSTRecord As String = "CUST" & vbTab &
                                   tmpJobId & vbTab &            ' Cust Name
                                   JTjimCIContact & vbTab &      ' Address 1
                                   JTjimCIBillStreet & vbTab &   ' Address 2
                                   JTjimCIBillCity & vbTab &     ' Address 3
                                   "" & vbTab &                  ' Address 4
                                   "" & vbTab &                  ' Address 5
                                   "" & vbTab &                  ' Company Name
                                   "" & vbTab &                  ' First Name
                                   "" & vbTab &                  ' Middle Initial
                                   "" & vbTab &                  ' Last Name
                                   "N" & vbTab &                 ' Cont1
                                   "N"                           ' Email
        '
        JTQBextWrite(CUSTRecord)
        '
        Return True
    End Function
    Public Shared Function JTQBExtInvAmts(InvAmts As JTas.JTasIHStru) As Boolean
        '
        '
        Dim TRNSRecord As String = "TRNS" & vbTab &                   ' TRNS
                                   " " & vbTab &                      ' TRNSID
                                   "INVOICE" & vbTab &                ' TRNSTYPE
                                   InvAmts.ihInvDate & vbTab &        ' DATE
                                   "Accounts Receivable" & vbTab &    ' ACCNT
                                   JTQBExtCustName & vbTab &          ' NAME
                                   "" & vbTab &                       ' CLASS
        String.Format("{0:0.00}", Val(InvAmts.ihInvTl)) & vbTab &     ' AMOUNT
                                   InvAmts.ihInvNum & vbTab &         ' DOCNUM
                                   "" & vbTab &                       ' MEMO
                                   "N" & vbTab &                      ' CLEAR
                                   "N" & vbTab &                      ' TOPRINT
                                   "" & vbTab &                       ' NAMEISTAXABLE
                                   "" & vbTab &                       ' DUEDATE
                                   "" & vbTab &                       ' TERMS
                                   "" & vbTab &                       ' PAYMETH
                                   "" & vbTab &                       ' SHIPVIA
                                   "" & vbTab &                       ' SHIPDATE
                                   "" & vbTab &                       ' REP
                                   "" & vbTab &                       ' FOB
                                   "" & vbTab &                       ' PONUM
                                   "" & vbTab &                       ' INVMEMO
                                   JTQBExtAddr1 & vbTab &             ' ADDR1
                                   JTQBExtAddr2 & vbTab &             ' ADDR2
                                   JTQBExtAddr3 & vbTab &             ' ADDR3
                                   "" & vbTab &                       ' ADDR4
                                   "" & vbTab &                       ' ADDR5
                                   "" & vbTab &                       ' SADDR1
                                   "" & vbTab &                       ' SADDR2
                                   "" & vbTab &                       ' SADDR3
                                   "" & vbTab &                       ' SADDR4
                                   ""                                 ' SADDR5
        '
        JTQBExtWrite(TRNSRecord)
        '
        Dim SPLRecord As String = "SPL" & vbTab &                                  ' SPL
                                  " " & vbTab &                                    ' SPLID
                                  "INVOICE" & vbTab &                              ' TRANTYPE
                                  InvAmts.ihInvDate & vbTab &                      ' DATE
                                  "INVOICEDAMTS" & vbTab &                         ' ACCNT
                                  "" & vbTab &                                     ' NAME
                                  "" & vbTab &                                     ' CLASS
                 String.Format("{0:0.00}", (Val(InvAmts.ihInvTl) * -1)) & vbTab &   ' AMOUNT
                                  "" & vbTab &                                     ' DOCNUM
                                  "" & vbTab &                                     ' MEMO
                                  "" & vbTab &                                     ' CLEAR
                                  "" & vbTab &                                     ' QNTY
                                  "" & vbTab &                                     ' PRICE
                                  "" & vbTab &                                     ' INVITEM
                                  "" & vbTab &                                     ' PAYMETH
                                  "" & vbTab &                                     ' TAXABLE
                                  "" & vbTab &                                     ' EXTRA
                                  "" & vbTab &                                     ' VATCODE
                                  "" & vbTab &                                     ' VATAMOUNT
                                  "" & vbTab &                                     ' VATADJ
                                  "" & vbTab &                                     ' SERVICEDATE
                                  "" & vbTab &                                     ' TAXCODE
                                  "" & vbTab &                                     ' TAXRATE
                                  "" & vbTab &                                     ' TAXAMOUNT
                                  "" & vbTab &                                     ' OTHER2
                                  ""                                               ' OTHER3
        '
        JTQBextWrite(SPLRecord)
        '
        Dim ENDTRNSRecord As String = "ENDTRNS"
        '
        JTQBextWrite(ENDTRNSRecord)
        '

        Return True
    End Function
    Public Shared Function JTQBExtStmtHeader(NLCRec As JTnlc.JTnlcStructure) As Boolean
        '

        '
        Dim TRNSRecord As String = "TRNS" & vbTab &                               ' TRNS
                                   " " & vbTab &                                  ' TRNSID
                                   "BILL" & vbTab &                               ' TRNSTYPE
                                   NLCRec.InvDate & vbTab &                       ' DATE
                                   "Accounts Payable" & vbTab &                   ' ACCNT
                                   NLCRec.Supplier & vbTab &                      ' NAME
                                   "" & vbTab &                                   ' CLASS
                           String.Format("{0:0.00}", (Val(NLCRec.SupCost) * -1)) & vbTab &  ' AMOUNT
                                   NLCRec.SupInvNum & vbTab &                     ' DOCNUM
                                   "" & vbTab &                                   ' MEMO
                                   "N" & vbTab &                                  ' CLEAR
                                   "N" & vbTab &                                  ' TOPRINT
                                   "" & vbTab &                                   ' NAMEISTAXABLE
                                   NLCRec.viDueDate & vbTab &                     ' DUEDATE
                                   "" & vbTab &                                   ' TERMS
                                   "" & vbTab &                                   ' PAYMETH
                                   "" & vbTab &                                   ' SHIPVIA
                                   "" & vbTab &                                   ' SHIPDATE
                                   "" & vbTab &                                   ' REP
                                   "" & vbTab &                                   ' FOB
                                   "" & vbTab &                                   ' PONUM
                                   NLCRec.viAcctNum & vbTab &                     ' INVMEMO
                                   NLCRec.viVendName & vbTab &                    ' ADDR1
                                   NLCRec.viStreet & vbTab &                      ' ADDR2
                                   NLCRec.viCityState & vbTab &                   ' ADDR3
                                   "" & vbTab &                                   ' ADDR4
                                   "" & vbTab &                                   ' ADDR5
                                   "" & vbTab &                                   ' SADDR1
                                   "" & vbTab &                                   ' SADDR2
                                   "" & vbTab &                                   ' SADDR3
                                   "" & vbTab &                                   ' SADDR4
                                   ""                                             ' SADDR5
        '
        JTQBextWrite(TRNSRecord)
        '
        Return True
    End Function
    Public Shared Function JTQBExtStmtSPL(NLCRec As JTnlc.JTnlcStructure) As Boolean
        '

        '
        Dim SPLRecord As String = "SPL" & vbTab &               ' SPL
                                  " " & vbTab &                 ' SPLID
                                  "BILL" & vbTab &              ' TRANTYPE
                                  NLCRec.InvDate & vbTab &      ' DATE
                                  NLCRec.NJREAcct & vbTab &     ' ACCNT
                                  "" & vbTab &                  ' NAME
                                  "" & vbTab &                  ' CLASS
                       String.Format("{0:0.00}", Val(NLCRec.SupCost)) & vbTab &      ' AMOUNT
                                  "" & vbTab &                  ' DOCNUM
                                  "" & vbTab &                  ' MEMO
                                  "" & vbTab &                  ' CLEAR
                                  "" & vbTab &                  ' QNTY
                                  "" & vbTab &                  ' PRICE
                                  "" & vbTab &                  ' INVITEM
                                  "" & vbTab &                  ' PAYMETH
                                  "N" & vbTab &                 ' TAXABLE
                                  "" & vbTab &                  ' EXTRA
                                  "" & vbTab &                  ' VATCODE
                                  "" & vbTab &                  ' VATAMOUNT
                                  "" & vbTab &                  ' VATADJ
                                  "" & vbTab &                  ' SERVICEDATE
                                  "" & vbTab &                  ' TAXCODE
                                  "" & vbTab &                  ' TAXRATE
                                  "" & vbTab &                  ' TAXAMOUNT
                                  "" & vbTab &                  ' OTHER2
                                  ""                            ' OTHER3
        '
        JTQBextWrite(SPLRecord)
        '
        Return True
    End Function
    Public Shared Function JTQBExtStmtENDTRNS() As Boolean
        '
        Dim ENDTRNSRecord As String = "ENDTRNS"
        '
        JTQBextWrite(ENDTRNSRecord)
        '
        Return True
    End Function
    Public Shared Function JTQBExtVendInvoices(tmpInvType As String, NLCRec As JTnlc.JTnlcStructure) As Boolean

        '   Public Structure JTnlcStructure
        '      Dim EntryDate As String
        '      Dim InvDate As String
        '      Dim YrWkNum As String
        '      Dim JobID As String
        '      Dim SubID As String
        '      Dim MorS As String          'M, S, IS, IH, ADJ, NJR
        '      Dim Descrpt As String
        '      Dim Supplier As String
        '      Dim SupInvNum As String
        '      Dim SupCrossRefNum As String     ' added 2/21/2020 --- Used to document receiver papwerwork to invoice
        '      Dim SupInvImageFile As String    ' added 2/21/2020 --- PDF file name of invoice image
        '      Dim SupCost As String
        '      Dim MrgnPC As String
        '      Dim TBInvd As String
        '      Dim CustInvDate As String
        '      Dim CustInvNumber As String
        '      Dim ReconcileDate As String
        '      Dim ReconcileDoc As String     ' Name of Reconcile report
        '      Dim AcctExtDate As String
        '      Dim AcctExtDoc As String     ' Name of File/Report when extracted to Accounting
        '      Dim PymtSwitch As String ' R - Reconciled, M - Manual, E - Export to Acct, NA - used on IS, IH and ADJ
        '    ' Fields added related to reconciliation and NJR
        '      Dim NJREAcct As String           'added 4/8/2019
        '      Dim viAcctNum As String          'added 4/8/2019
        '      Dim viVendName As String         'added 4/8/2019
        '      Dim viStreet As String           'added 4/8/2019
        '      Dim viCityState As String        'added 4/8/2019
        '      Dim viTerms As String            'added 4/8/2019
        '      Dim viDueDate As String          'added 4/8/2019
        '      Dim viDiscpercent As String      'added 4/8/2019
        '      Dim viDiscAmt As String          'added 4/8/2019
        '      Dim viRcrdEntrySrce As String    'added 4/16/2019  null most of the time; on NJR entries - NLC or REC for where it was entered
        '  End Structure
        '
        Dim tmpGLAcct As String = Nothing
        Select Case NLCRec.MorS
            Case "M"
                tmpGLAcct = "JOBRELATED:MAT"
            Case "S"
                tmpGLAcct = "JOBRELATED:SER"
            Case Else
                tmpGLAcct = NLCRec.NJREAcct
        End Select

        '
        Dim TRNSRecord As String = "TRNS" & vbTab &                               ' TRNS
                                   " " & vbTab &                                  ' TRNSID
                                   "BILL" & vbTab &                               ' TRNSTYPE
                                   NLCRec.InvDate & vbTab &                       ' DATE
                                   "Accounts Payable" & vbTab &                   ' ACCNT
                                   NLCRec.Supplier & vbTab &                      ' NAME
                                   "" & vbTab &                                   ' CLASS
                    String.Format("{0:0.00}", (Val(NLCRec.SupCost) * -1)) & vbTab &  ' AMOUNT
                                   NLCRec.SupInvNum & vbTab &                     ' DOCNUM
                                   "" & vbTab &                                   ' MEMO
                                   "N" & vbTab &                                  ' CLEAR
                                   "N" & vbTab &                                  ' TOPRINT
                                   "" & vbTab &                                   ' NAMEISTAXABLE
                                   NLCRec.viDueDate & vbTab &                     ' DUEDATE
                                   "" & vbTab &                                   ' TERMS
                                   "" & vbTab &                                   ' PAYMETH
                                   "" & vbTab &                                   ' SHIPVIA
                                   "" & vbTab &                                   ' SHIPDATE
                                   "" & vbTab &                                   ' REP
                                   "" & vbTab &                                   ' FOB
                                   "" & vbTab &                                   ' PONUM
                                   NLCRec.viAcctNum & vbTab &                     ' INVMEMO
                                   NLCRec.viVendName & vbTab &                    ' ADDR1
                                   NLCRec.viStreet & vbTab &                      ' ADDR2
                                   NLCRec.viCityState & vbTab &                   ' ADDR3
                                   "" & vbTab &                                   ' ADDR4
                                   "" & vbTab &                                   ' ADDR5
                                   "" & vbTab &                                   ' SADDR1
                                   "" & vbTab &                                   ' SADDR2
                                   "" & vbTab &                                   ' SADDR3
                                   "" & vbTab &                                   ' SADDR4
                                   ""                                             ' SADDR5
        '
        JTQBextWrite(TRNSRecord)
        '

        '

        '
        Dim SPLRecord As String = "SPL" & vbTab &               ' SPL
                                  " " & vbTab &                 ' SPLID
                                  "BILL" & vbTab &              ' TRANTYPE
                                  NLCRec.InvDate & vbTab &      ' DATE
                                  tmpGLAcct & vbTab &     ' ACCNT
                                  "" & vbTab &                  ' NAME
                                  "" & vbTab &                  ' CLASS
                     String.Format("{0:0.00}", Val(NLCRec.SupCost)) & vbTab &      ' AMOUNT
                                  "" & vbTab &                  ' DOCNUM
                                  "" & vbTab &                  ' MEMO
                                  "" & vbTab &                  ' CLEAR
                                  "" & vbTab &                  ' QNTY
                                  "" & vbTab &                  ' PRICE
                                  "" & vbTab &                  ' INVITEM
                                  "" & vbTab &                  ' PAYMETH
                                  "N" & vbTab &                 ' TAXABLE
                                  "" & vbTab &                  ' EXTRA
                                  "" & vbTab &                  ' VATCODE
                                  "" & vbTab &                  ' VATAMOUNT
                                  "" & vbTab &                  ' VATADJ
                                  "" & vbTab &                  ' SERVICEDATE
                                  "" & vbTab &                  ' TAXCODE
                                  "" & vbTab &                  ' TAXRATE
                                  "" & vbTab &                  ' TAXAMOUNT
                                  "" & vbTab &                  ' OTHER2
                                  ""                            ' OTHER3
        '
        JTQBextWrite(SPLRecord)
        '

        '
        Dim ENDTRNSRecord As String = "ENDTRNS"
        '
        JTQBextWrite(ENDTRNSRecord)
        '
        Return True
    End Function

    Public Shared Function JTQBExtWrite(tmpItem As String) As Boolean
        Dim tmpTodayYear As String = Date.Today.Year
        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "QBPDF\" & tmpTodayYear
        If Not System.IO.Directory.Exists(tmpFilePath) Then
            System.IO.Directory.CreateDirectory(tmpFilePath)
        End If

        Dim filePath As String = tmpFilePath & "\" & JTQBfileName & ".iif"
        '
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter(filePath, True)
        file.WriteLine(tmpItem)
        file.Close()
        Return True
    End Function
End Class
