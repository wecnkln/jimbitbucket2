﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTdb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtJTdbBUSStatus = New System.Windows.Forms.TextBox()
        Me.txtJTdbBUSFreq = New System.Windows.Forms.TextBox()
        Me.txtJTdbBUSLstBckUp = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJTdbUAMat = New System.Windows.Forms.TextBox()
        Me.txtJTdbUATotal = New System.Windows.Forms.TextBox()
        Me.txtJTdbUAAdj = New System.Windows.Forms.TextBox()
        Me.txtJTdbUASub = New System.Windows.Forms.TextBox()
        Me.txtJTdbUALabor = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtJTdbIAStatus = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtJTdbHSHours2 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSDate2 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSHours3 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSDate3 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSHours1 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSDate1 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSHours4 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSDate4 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSHours5 = New System.Windows.Forms.TextBox()
        Me.txtJTdbHSDate5 = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtJTdbPENLCUnRecAmt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPENLCUnRecCnt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPENLCManAmt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPENLCQBAmt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPENLCQBCnt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPENLCManCnt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPEInvAmt = New System.Windows.Forms.TextBox()
        Me.txtJTdbPEInvCnt = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtJTdbSSRCompCom = New System.Windows.Forms.TextBox()
        Me.txtJTdbSSROutofCompCnt = New System.Windows.Forms.TextBox()
        Me.txtJTdbSSRSucConCnt = New System.Windows.Forms.TextBox()
        Me.txtJTdbSSRSupCnt = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.lvJTdbLI = New System.Windows.Forms.ListView()
        Me.LVJob = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvDays = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvBalance = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtJTdbNECount = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(455, 518)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(180, 40)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Click to Continue"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(364, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(375, 164)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtJTdbBUSStatus)
        Me.GroupBox1.Controls.Add(Me.txtJTdbBUSFreq)
        Me.GroupBox1.Controls.Add(Me.txtJTdbBUSLstBckUp)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(782, 439)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(223, 141)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "BackUp Status"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(187, 34)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "These statisitcs refer to total" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " system backups."
        '
        'txtJTdbBUSStatus
        '
        Me.txtJTdbBUSStatus.Location = New System.Drawing.Point(112, 74)
        Me.txtJTdbBUSStatus.Name = "txtJTdbBUSStatus"
        Me.txtJTdbBUSStatus.ReadOnly = True
        Me.txtJTdbBUSStatus.Size = New System.Drawing.Size(90, 22)
        Me.txtJTdbBUSStatus.TabIndex = 5
        Me.txtJTdbBUSStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbBUSFreq
        '
        Me.txtJTdbBUSFreq.Location = New System.Drawing.Point(112, 49)
        Me.txtJTdbBUSFreq.Name = "txtJTdbBUSFreq"
        Me.txtJTdbBUSFreq.ReadOnly = True
        Me.txtJTdbBUSFreq.Size = New System.Drawing.Size(47, 22)
        Me.txtJTdbBUSFreq.TabIndex = 4
        Me.txtJTdbBUSFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbBUSLstBckUp
        '
        Me.txtJTdbBUSLstBckUp.Location = New System.Drawing.Point(112, 25)
        Me.txtJTdbBUSLstBckUp.Name = "txtJTdbBUSLstBckUp"
        Me.txtJTdbBUSLstBckUp.ReadOnly = True
        Me.txtJTdbBUSLstBckUp.Size = New System.Drawing.Size(72, 22)
        Me.txtJTdbBUSLstBckUp.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Status:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Frequency(Days):"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Last Backup:"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtJTdbUAMat)
        Me.GroupBox2.Controls.Add(Me.txtJTdbUATotal)
        Me.GroupBox2.Controls.Add(Me.txtJTdbUAAdj)
        Me.GroupBox2.Controls.Add(Me.txtJTdbUASub)
        Me.GroupBox2.Controls.Add(Me.txtJTdbUALabor)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(782, 214)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(223, 203)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Unbilled Activity"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(21, 155)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(198, 34)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Figures represent all activity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " within J.I.M pending invoicing."
        '
        'txtJTdbUAMat
        '
        Me.txtJTdbUAMat.Location = New System.Drawing.Point(101, 54)
        Me.txtJTdbUAMat.Name = "txtJTdbUAMat"
        Me.txtJTdbUAMat.ReadOnly = True
        Me.txtJTdbUAMat.Size = New System.Drawing.Size(91, 22)
        Me.txtJTdbUAMat.TabIndex = 9
        Me.txtJTdbUAMat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbUATotal
        '
        Me.txtJTdbUATotal.Location = New System.Drawing.Point(101, 127)
        Me.txtJTdbUATotal.Name = "txtJTdbUATotal"
        Me.txtJTdbUATotal.ReadOnly = True
        Me.txtJTdbUATotal.Size = New System.Drawing.Size(91, 22)
        Me.txtJTdbUATotal.TabIndex = 8
        Me.txtJTdbUATotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbUAAdj
        '
        Me.txtJTdbUAAdj.Location = New System.Drawing.Point(101, 102)
        Me.txtJTdbUAAdj.Name = "txtJTdbUAAdj"
        Me.txtJTdbUAAdj.ReadOnly = True
        Me.txtJTdbUAAdj.Size = New System.Drawing.Size(91, 22)
        Me.txtJTdbUAAdj.TabIndex = 7
        Me.txtJTdbUAAdj.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbUASub
        '
        Me.txtJTdbUASub.Location = New System.Drawing.Point(101, 78)
        Me.txtJTdbUASub.Name = "txtJTdbUASub"
        Me.txtJTdbUASub.ReadOnly = True
        Me.txtJTdbUASub.Size = New System.Drawing.Size(91, 22)
        Me.txtJTdbUASub.TabIndex = 6
        Me.txtJTdbUASub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbUALabor
        '
        Me.txtJTdbUALabor.Location = New System.Drawing.Point(101, 30)
        Me.txtJTdbUALabor.Name = "txtJTdbUALabor"
        Me.txtJTdbUALabor.ReadOnly = True
        Me.txtJTdbUALabor.Size = New System.Drawing.Size(91, 22)
        Me.txtJTdbUALabor.TabIndex = 5
        Me.txtJTdbUALabor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 17)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Total"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 17)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Materials"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 81)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 17)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Subcontract"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 17)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Adjustments"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 17)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Labor"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.txtJTdbIAStatus)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Location = New System.Drawing.Point(95, 37)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(223, 143)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Internet Status"
        '
        'txtJTdbIAStatus
        '
        Me.txtJTdbIAStatus.Location = New System.Drawing.Point(70, 44)
        Me.txtJTdbIAStatus.Name = "txtJTdbIAStatus"
        Me.txtJTdbIAStatus.ReadOnly = True
        Me.txtJTdbIAStatus.Size = New System.Drawing.Size(100, 22)
        Me.txtJTdbIAStatus.TabIndex = 2
        Me.txtJTdbIAStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 71)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(223, 68)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "If the Internet is available, emails, " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(invoices, payroll, etc.), system " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "backu" &
    "ps to the Cloud, etc. can be " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "executed."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(20, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(122, 17)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Internet access is:"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSHours2)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSDate2)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSHours3)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSDate3)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSHours1)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSDate1)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSHours4)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSDate4)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSHours5)
        Me.GroupBox4.Controls.Add(Me.txtJTdbHSDate5)
        Me.GroupBox4.Location = New System.Drawing.Point(324, 214)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(223, 203)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Timekeeping Hour Summary"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 153)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(202, 34)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Labor hours recorded in J.I.M. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "by week not yet invoiced."
        '
        'txtJTdbHSHours2
        '
        Me.txtJTdbHSHours2.Location = New System.Drawing.Point(130, 100)
        Me.txtJTdbHSHours2.Name = "txtJTdbHSHours2"
        Me.txtJTdbHSHours2.ReadOnly = True
        Me.txtJTdbHSHours2.Size = New System.Drawing.Size(61, 22)
        Me.txtJTdbHSHours2.TabIndex = 9
        Me.txtJTdbHSHours2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbHSDate2
        '
        Me.txtJTdbHSDate2.Location = New System.Drawing.Point(34, 100)
        Me.txtJTdbHSDate2.Name = "txtJTdbHSDate2"
        Me.txtJTdbHSDate2.ReadOnly = True
        Me.txtJTdbHSDate2.Size = New System.Drawing.Size(83, 22)
        Me.txtJTdbHSDate2.TabIndex = 8
        Me.txtJTdbHSDate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbHSHours3
        '
        Me.txtJTdbHSHours3.Location = New System.Drawing.Point(130, 75)
        Me.txtJTdbHSHours3.Name = "txtJTdbHSHours3"
        Me.txtJTdbHSHours3.ReadOnly = True
        Me.txtJTdbHSHours3.Size = New System.Drawing.Size(61, 22)
        Me.txtJTdbHSHours3.TabIndex = 7
        Me.txtJTdbHSHours3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbHSDate3
        '
        Me.txtJTdbHSDate3.Location = New System.Drawing.Point(34, 75)
        Me.txtJTdbHSDate3.Name = "txtJTdbHSDate3"
        Me.txtJTdbHSDate3.ReadOnly = True
        Me.txtJTdbHSDate3.Size = New System.Drawing.Size(83, 22)
        Me.txtJTdbHSDate3.TabIndex = 6
        Me.txtJTdbHSDate3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbHSHours1
        '
        Me.txtJTdbHSHours1.Location = New System.Drawing.Point(130, 126)
        Me.txtJTdbHSHours1.Name = "txtJTdbHSHours1"
        Me.txtJTdbHSHours1.ReadOnly = True
        Me.txtJTdbHSHours1.Size = New System.Drawing.Size(61, 22)
        Me.txtJTdbHSHours1.TabIndex = 5
        Me.txtJTdbHSHours1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbHSDate1
        '
        Me.txtJTdbHSDate1.Location = New System.Drawing.Point(34, 126)
        Me.txtJTdbHSDate1.Name = "txtJTdbHSDate1"
        Me.txtJTdbHSDate1.ReadOnly = True
        Me.txtJTdbHSDate1.Size = New System.Drawing.Size(83, 22)
        Me.txtJTdbHSDate1.TabIndex = 4
        Me.txtJTdbHSDate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbHSHours4
        '
        Me.txtJTdbHSHours4.Location = New System.Drawing.Point(130, 49)
        Me.txtJTdbHSHours4.Name = "txtJTdbHSHours4"
        Me.txtJTdbHSHours4.ReadOnly = True
        Me.txtJTdbHSHours4.Size = New System.Drawing.Size(61, 22)
        Me.txtJTdbHSHours4.TabIndex = 3
        Me.txtJTdbHSHours4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbHSDate4
        '
        Me.txtJTdbHSDate4.Location = New System.Drawing.Point(34, 49)
        Me.txtJTdbHSDate4.Name = "txtJTdbHSDate4"
        Me.txtJTdbHSDate4.ReadOnly = True
        Me.txtJTdbHSDate4.Size = New System.Drawing.Size(83, 22)
        Me.txtJTdbHSDate4.TabIndex = 2
        Me.txtJTdbHSDate4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTdbHSHours5
        '
        Me.txtJTdbHSHours5.Location = New System.Drawing.Point(130, 24)
        Me.txtJTdbHSHours5.Name = "txtJTdbHSHours5"
        Me.txtJTdbHSHours5.ReadOnly = True
        Me.txtJTdbHSHours5.Size = New System.Drawing.Size(61, 22)
        Me.txtJTdbHSHours5.TabIndex = 1
        Me.txtJTdbHSHours5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbHSDate5
        '
        Me.txtJTdbHSDate5.Location = New System.Drawing.Point(34, 24)
        Me.txtJTdbHSDate5.Name = "txtJTdbHSDate5"
        Me.txtJTdbHSDate5.ReadOnly = True
        Me.txtJTdbHSDate5.Size = New System.Drawing.Size(83, 22)
        Me.txtJTdbHSDate5.TabIndex = 0
        Me.txtJTdbHSDate5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCUnRecAmt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCUnRecCnt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCManAmt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCQBAmt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCQBCnt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPENLCManCnt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPEInvAmt)
        Me.GroupBox5.Controls.Add(Me.txtJTdbPEInvCnt)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.Label16)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Location = New System.Drawing.Point(95, 215)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(223, 202)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Activity Pending Export"
        '
        'txtJTdbPENLCUnRecAmt
        '
        Me.txtJTdbPENLCUnRecAmt.Location = New System.Drawing.Point(107, 157)
        Me.txtJTdbPENLCUnRecAmt.Name = "txtJTdbPENLCUnRecAmt"
        Me.txtJTdbPENLCUnRecAmt.ReadOnly = True
        Me.txtJTdbPENLCUnRecAmt.Size = New System.Drawing.Size(84, 22)
        Me.txtJTdbPENLCUnRecAmt.TabIndex = 11
        Me.txtJTdbPENLCUnRecAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPENLCUnRecCnt
        '
        Me.txtJTdbPENLCUnRecCnt.Location = New System.Drawing.Point(40, 157)
        Me.txtJTdbPENLCUnRecCnt.Name = "txtJTdbPENLCUnRecCnt"
        Me.txtJTdbPENLCUnRecCnt.ReadOnly = True
        Me.txtJTdbPENLCUnRecCnt.Size = New System.Drawing.Size(48, 22)
        Me.txtJTdbPENLCUnRecCnt.TabIndex = 10
        Me.txtJTdbPENLCUnRecCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPENLCManAmt
        '
        Me.txtJTdbPENLCManAmt.Location = New System.Drawing.Point(107, 117)
        Me.txtJTdbPENLCManAmt.Name = "txtJTdbPENLCManAmt"
        Me.txtJTdbPENLCManAmt.ReadOnly = True
        Me.txtJTdbPENLCManAmt.Size = New System.Drawing.Size(84, 22)
        Me.txtJTdbPENLCManAmt.TabIndex = 9
        Me.txtJTdbPENLCManAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPENLCQBAmt
        '
        Me.txtJTdbPENLCQBAmt.Location = New System.Drawing.Point(107, 78)
        Me.txtJTdbPENLCQBAmt.Name = "txtJTdbPENLCQBAmt"
        Me.txtJTdbPENLCQBAmt.ReadOnly = True
        Me.txtJTdbPENLCQBAmt.Size = New System.Drawing.Size(84, 22)
        Me.txtJTdbPENLCQBAmt.TabIndex = 7
        Me.txtJTdbPENLCQBAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPENLCQBCnt
        '
        Me.txtJTdbPENLCQBCnt.Location = New System.Drawing.Point(40, 78)
        Me.txtJTdbPENLCQBCnt.Name = "txtJTdbPENLCQBCnt"
        Me.txtJTdbPENLCQBCnt.ReadOnly = True
        Me.txtJTdbPENLCQBCnt.Size = New System.Drawing.Size(48, 22)
        Me.txtJTdbPENLCQBCnt.TabIndex = 6
        Me.txtJTdbPENLCQBCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPENLCManCnt
        '
        Me.txtJTdbPENLCManCnt.Location = New System.Drawing.Point(40, 117)
        Me.txtJTdbPENLCManCnt.Name = "txtJTdbPENLCManCnt"
        Me.txtJTdbPENLCManCnt.ReadOnly = True
        Me.txtJTdbPENLCManCnt.Size = New System.Drawing.Size(48, 22)
        Me.txtJTdbPENLCManCnt.TabIndex = 8
        Me.txtJTdbPENLCManCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPEInvAmt
        '
        Me.txtJTdbPEInvAmt.Location = New System.Drawing.Point(107, 38)
        Me.txtJTdbPEInvAmt.Name = "txtJTdbPEInvAmt"
        Me.txtJTdbPEInvAmt.ReadOnly = True
        Me.txtJTdbPEInvAmt.Size = New System.Drawing.Size(84, 22)
        Me.txtJTdbPEInvAmt.TabIndex = 5
        Me.txtJTdbPEInvAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbPEInvCnt
        '
        Me.txtJTdbPEInvCnt.Location = New System.Drawing.Point(40, 38)
        Me.txtJTdbPEInvCnt.Name = "txtJTdbPEInvCnt"
        Me.txtJTdbPEInvCnt.ReadOnly = True
        Me.txtJTdbPEInvCnt.Size = New System.Drawing.Size(48, 22)
        Me.txtJTdbPEInvCnt.TabIndex = 4
        Me.txtJTdbPEInvCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(21, 60)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(68, 17)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "NLC - QB"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(20, 139)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(131, 17)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "NLC - Unreconciled"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(21, 101)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(94, 17)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "NLC - Manual"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(21, 18)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 17)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Invoices"
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox6.Controls.Add(Me.txtJTdbSSRCompCom)
        Me.GroupBox6.Controls.Add(Me.txtJTdbSSROutofCompCnt)
        Me.GroupBox6.Controls.Add(Me.txtJTdbSSRSucConCnt)
        Me.GroupBox6.Controls.Add(Me.txtJTdbSSRSupCnt)
        Me.GroupBox6.Controls.Add(Me.Label20)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.Label18)
        Me.GroupBox6.Location = New System.Drawing.Point(782, 37)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(223, 153)
        Me.GroupBox6.TabIndex = 7
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Supplier SubContractor Recap"
        '
        'txtJTdbSSRCompCom
        '
        Me.txtJTdbSSRCompCom.Location = New System.Drawing.Point(6, 105)
        Me.txtJTdbSSRCompCom.Multiline = True
        Me.txtJTdbSSRCompCom.Name = "txtJTdbSSRCompCom"
        Me.txtJTdbSSRCompCom.ReadOnly = True
        Me.txtJTdbSSRCompCom.Size = New System.Drawing.Size(210, 37)
        Me.txtJTdbSSRCompCom.TabIndex = 6
        '
        'txtJTdbSSROutofCompCnt
        '
        Me.txtJTdbSSROutofCompCnt.Location = New System.Drawing.Point(164, 68)
        Me.txtJTdbSSROutofCompCnt.Name = "txtJTdbSSROutofCompCnt"
        Me.txtJTdbSSROutofCompCnt.ReadOnly = True
        Me.txtJTdbSSROutofCompCnt.Size = New System.Drawing.Size(38, 22)
        Me.txtJTdbSSROutofCompCnt.TabIndex = 5
        Me.txtJTdbSSROutofCompCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbSSRSucConCnt
        '
        Me.txtJTdbSSRSucConCnt.Location = New System.Drawing.Point(164, 43)
        Me.txtJTdbSSRSucConCnt.Name = "txtJTdbSSRSucConCnt"
        Me.txtJTdbSSRSucConCnt.ReadOnly = True
        Me.txtJTdbSSRSucConCnt.Size = New System.Drawing.Size(38, 22)
        Me.txtJTdbSSRSucConCnt.TabIndex = 4
        Me.txtJTdbSSRSucConCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTdbSSRSupCnt
        '
        Me.txtJTdbSSRSupCnt.Location = New System.Drawing.Point(164, 19)
        Me.txtJTdbSSRSupCnt.Name = "txtJTdbSSRSupCnt"
        Me.txtJTdbSSRSupCnt.ReadOnly = True
        Me.txtJTdbSSRSupCnt.Size = New System.Drawing.Size(38, 22)
        Me.txtJTdbSSRSupCnt.TabIndex = 3
        Me.txtJTdbSSRSupCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 66)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(202, 34)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Subcontractors with paperwork" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "out of compliance:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(7, 47)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(152, 17)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "SubContractors on file:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 23)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(113, 17)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Suppliers on file:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Elephant", 16.2!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(472, 167)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(187, 37)
        Me.Label21.TabIndex = 8
        Me.Label21.Text = "Dashboard"
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox7.Controls.Add(Me.lvJTdbLI)
        Me.GroupBox7.Location = New System.Drawing.Point(553, 214)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(223, 203)
        Me.GroupBox7.TabIndex = 9
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Days Since Last Invoice"
        '
        'lvJTdbLI
        '
        Me.lvJTdbLI.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.LVJob, Me.lvDays, Me.lvBalance})
        Me.lvJTdbLI.Location = New System.Drawing.Point(7, 22)
        Me.lvJTdbLI.Name = "lvJTdbLI"
        Me.lvJTdbLI.Size = New System.Drawing.Size(210, 171)
        Me.lvJTdbLI.TabIndex = 0
        Me.lvJTdbLI.UseCompatibleStateImageBehavior = False
        Me.lvJTdbLI.View = System.Windows.Forms.View.Details
        '
        'LVJob
        '
        Me.LVJob.Text = "Job"
        Me.LVJob.Width = 75
        '
        'lvDays
        '
        Me.lvDays.Text = "Days"
        Me.lvDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvDays.Width = 40
        '
        'lvBalance
        '
        Me.lvBalance.Text = "Balance"
        Me.lvBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvBalance.Width = 65
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox8.Controls.Add(Me.Label23)
        Me.GroupBox8.Controls.Add(Me.txtJTdbNECount)
        Me.GroupBox8.Controls.Add(Me.Label22)
        Me.GroupBox8.Location = New System.Drawing.Point(95, 439)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(223, 132)
        Me.GroupBox8.TabIndex = 10
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Invoices Not Emailed"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(6, 51)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(282, 68)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Note: Count is only for invoices that belong " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "to jobs where email delivery has b" &
    "een " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "selected. Go to Utilities to email the invoice " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and cancel the request."
        '
        'txtJTdbNECount
        '
        Me.txtJTdbNECount.Location = New System.Drawing.Point(167, 23)
        Me.txtJTdbNECount.Name = "txtJTdbNECount"
        Me.txtJTdbNECount.ReadOnly = True
        Me.txtJTdbNECount.Size = New System.Drawing.Size(42, 22)
        Me.txtJTdbNECount.TabIndex = 1
        Me.txtJTdbNECount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(19, 25)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(137, 17)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Invoices not Emailed"
        '
        'JTdb
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1077, 592)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "JTdb"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Dashboard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtJTdbBUSStatus As TextBox
    Friend WithEvents txtJTdbBUSFreq As TextBox
    Friend WithEvents txtJTdbBUSLstBckUp As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtJTdbUAMat As TextBox
    Friend WithEvents txtJTdbUATotal As TextBox
    Friend WithEvents txtJTdbUAAdj As TextBox
    Friend WithEvents txtJTdbUASub As TextBox
    Friend WithEvents txtJTdbUALabor As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtJTdbIAStatus As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents txtJTdbHSDate5 As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtJTdbHSHours2 As TextBox
    Friend WithEvents txtJTdbHSDate2 As TextBox
    Friend WithEvents txtJTdbHSHours3 As TextBox
    Friend WithEvents txtJTdbHSDate3 As TextBox
    Friend WithEvents txtJTdbHSHours1 As TextBox
    Friend WithEvents txtJTdbHSDate1 As TextBox
    Friend WithEvents txtJTdbHSHours4 As TextBox
    Friend WithEvents txtJTdbHSDate4 As TextBox
    Friend WithEvents txtJTdbHSHours5 As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtJTdbPENLCUnRecAmt As TextBox
    Friend WithEvents txtJTdbPENLCUnRecCnt As TextBox
    Friend WithEvents txtJTdbPENLCManAmt As TextBox
    Friend WithEvents txtJTdbPENLCQBAmt As TextBox
    Friend WithEvents txtJTdbPENLCQBCnt As TextBox
    Friend WithEvents txtJTdbPENLCManCnt As TextBox
    Friend WithEvents txtJTdbPEInvAmt As TextBox
    Friend WithEvents txtJTdbPEInvCnt As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txtJTdbSSRCompCom As TextBox
    Friend WithEvents txtJTdbSSROutofCompCnt As TextBox
    Friend WithEvents txtJTdbSSRSucConCnt As TextBox
    Friend WithEvents txtJTdbSSRSupCnt As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents lvJTdbLI As ListView
    Friend WithEvents LVJob As ColumnHeader
    Friend WithEvents lvDays As ColumnHeader
    Friend WithEvents lvBalance As ColumnHeader
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtJTdbNECount As TextBox
    Friend WithEvents Label22 As Label
End Class
