﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
'
''' <summary>
''' This class produces the Accounting Extract Report. The report is divided into sections. Sections
''' will segregated between statements and invoices that need to be paid and other that will not be
''' input into the accounting systems.
''' 
''' The first phase of this report will only give printed output. The second phase will also output a 
''' file to be imported into accounting (A/R and A/P).
''' 
''' The purpose of the first [hase is for audit ... it should be used to input the data into accounting.
''' During that process, the data should be scrutinized for omission or inaccuracies.
''' 
''' The report is produced using the iTextSharp toolbox.
''' </summary>
Public Class JTae
    Public slJTaeIH As New SortedList
    Public slJTaeNLC As New SortedList
    '
    Dim JTaeFileRef As String = Nothing
    '
    ''' <summary>
    ''' Entry point to produce the Accounting Extract Audit Listing
    ''' </summary>
    Public Sub JTaeInit()
        JTaeITextInit()
    End Sub
    ''' <summary>
    ''' Function control all the controls related to building the actual print content. It also technically
    ''' does the iText stuff.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTaeITextInit() As String
        Dim tmpTodayYear = Date.Today.Year
        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "QBPDF\" & tmpTodayYear
        If Not System.IO.Directory.Exists(tmpFilePath) Then
            System.IO.Directory.CreateDirectory(tmpFilePath)
        End If
        Dim tmpLetter As String = "A"
        Dim JulDate As String = Gregarian2Julian(Date.Now)
        JTaeFileRef = "JIMActngExpt" & JulDate & tmpLetter
        Dim filename As String = tmpFilePath & "\" & JTaeFileRef & ".PDF"
        Do While System.IO.File.Exists(filename) = True
            tmpLetter = Chr(Asc(tmpLetter) + 1)
            JTaeFileRef = "JIMActngExpt" & JulDate & tmpLetter
            filename = tmpFilePath & "\" & JTaeFileRef & ".PDF"
        Loop
        JTQBExt.JTQBInit(JTaeFileRef)   'open .iif file and write header records
        ' ----------------------------------------------------------------
        ' Generate iTextSharp PDF report and update Reconciliation Date for NLC item.
        ' ----------------------------------------------------------------
        Try
            Using fs As System.IO.FileStream = New FileStream(filename, FileMode.Create)
                '
                Dim myDoc As Document = New Document(iTextSharp.text.PageSize.LETTER.Rotate(), 72, 72, 72, 72)
                Dim Writer As PdfWriter = PdfWriter.GetInstance(myDoc, fs)
                Dim ev As New JTaeiteEvents

                Writer.PageEvent = ev
                '
                '
                '
                myDoc.AddTitle("JIM Accounting Extract Report")
                myDoc.Open()
                '
                '
                JTraeBldRprtHeader(myDoc, JTaeFileRef, "ACCOUNTING")      ' Build report headings
                '
                ' -------------------------------------------------------------
                ' Invoices Extracted for Accounting
                ' -------------------------------------------------------------
                Dim tmpType As String = "E"      ' transactions exported to accounting
                If JTaeExtractInvoiceSection(tmpType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Customer Invoices - Extracted for Accounting(A/R)")
                    JTaeBldInvoiceSection(myDoc, tmpType)      'Build cleared Job Related Invoice table
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Statements Extracted for Accounting
                ' -------------------------------------------------------------
                tmpType = "E"     'Extract to accounting
                If JTaeExtractStatementSection(tmpType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Vendor Statement - Extracted for Accounting(A/P)")
                    JTaeBldStatementSection(myDoc, tmpType)      'Build cleared Job Related Invoice table'
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Material Invoices Paid Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                Dim tmpPymtType As String = "E"      'To be Exported
                Dim tmpInvType As String = "M"       'Material
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Material Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Services Invoices Paid Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                tmpPymtType = "E"      'To be Exported
                tmpInvType = "S"       'Services
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Services Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor NJR Invoices Paid Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                tmpPymtType = "E"      'To be Exported
                tmpInvType = "NJR"       'Non-Job-Related
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "NJR Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                '
                ' -------------------------------------------------------------
                ' Vendor Material Invoices Journal Only (JO) Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                tmpPymtType = "JO"      'To be Exported
                tmpInvType = "M"       'Material
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Material Journal-Only Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Services Invoices Journal Only (JO) Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                tmpPymtType = "JO"      'To be Exported
                tmpInvType = "S"       'Services
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Services Journal-Only Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor NJR Invoices Journal Only (JO) Extracted for Accounting(A/P)
                ' -------------------------------------------------------------
                tmpPymtType = "JO"      'To be Exported
                tmpInvType = "NJR"       'Non-Job-Related
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "NJR Journal-Only Invoices - Extracted for Accounting(A/P)")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                '  Took new page out to make report more compact.
                '           myDoc.NewPage()    'page break between items going to accounting and all others.
                '
                JTraeBldRprtHeader(myDoc, JTaeFileRef, "DONTEXTRACT")      ' Print second section heading
                '
                ' -------------------------------------------------------------
                ' Invoices marked as do not extract to Accounting
                ' -------------------------------------------------------------
                tmpType = "M"    'To be paid manually
                If JTaeExtractInvoiceSection(tmpType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Customer Invoices - Secondary Invoice Sequence")
                    JTaeBldInvoiceSection(myDoc, tmpType)      'Build cleared Job Related Invoice table'
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Statements Paid Manually
                ' -------------------------------------------------------------
                '             MsgBox(" Vendor Statements Paid Manually - started JRse~160")
                tmpType = "M"      'To be paid manually
                If JTaeExtractStatementSection(tmpType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Vendor Statement - Paid Manually")
                    JTaeBldStatementSection(myDoc, tmpType)      'Build cleared Job Related Invoice table'
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor Material Invoices Paid Manually
                ' -------------------------------------------------------------
                '           MsgBox("Vendor Material Invoices Paid Manually - started JRse~172")
                tmpPymtType = "M"      'To be Exported
                tmpInvType = "M"       'Material
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Material Invoices - Manually Paid")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                ' -------------------------------------------------------------
                ' Vendor Services Invoices Paid Manually
                ' -------------------------------------------------------------
                tmpPymtType = "M"      'To be Exported
                tmpInvType = "S"       'Services
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "Services Invoices - Manually Paid")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                ' -------------------------------------------------------------
                ' Vendor NJR Invoices Paid Manually
                ' -------------------------------------------------------------
                tmpPymtType = "M"      'To be Exported
                tmpInvType = "NJR"       'Non-Job-Related
                If JTaeExtractVndInvSection(tmpInvType, tmpPymtType) > 0 Then
                    JTaePrtSectionHeader(myDoc, "NJR Invoices - Paid Manually")
                    JTaeBldVndInvSection(myDoc, tmpInvType, tmpPymtType)      'Build and print report section.
                End If
                '
                Dim JIMwhtFooter As New Phrase(FP_H6Blue(vbCrLf & "Information managed and reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                myDoc.Add(JIMwhtFooter)
                '
                myDoc.Close()
                '
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        JTnlc.JTnlcArrayFlush("")
        JTas.JTasHistFlush()

        JTvPDF.JTvPDFDispPDF(filename)

        JTstart.JTstartMenuOn()

        Return ""
    End Function
    ''' <summary>
    ''' Function builds the report headers for the Accounting Extract Report
    ''' </summary>
    ''' <param name="myDoc"></param>
    ''' <returns></returns>
    Private Function JTraeBldRprtHeader(ByRef myDoc As Document, tmpReportName As String, tmpSection As String) As String
        ' ----------------------------------------------------------------
        ' Generate PDF Invoice Header section..
        ' ----------------------------------------------------------------

        ' ----------------------------------------------------------
        ' table structure to hold logo and date
        ' ----------------------------------------------------------
        Dim table As New PdfPTable(2)
        ' Actual width of table in points
        table.TotalWidth = 680.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {8.5F, 6.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 0.0F
        table.SpacingAfter = 10.0F
        '
        If tmpSection = "ACCOUNTING" Then
            Dim logolocation As String = JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.CompanyLogo
            '
            ' add code to see if logo files exists
            '
            If Not File.Exists(logolocation) Then
                iTSFillCell("-------- No logo file found -------", table, 0, 0, 0, "RED")
            Else
                '
                '
                Dim tmpImage As Image = Image.GetInstance(logolocation)
                tmpImage.ScaleAbsolute(140.0F, 50.0F)
                Dim CellContents = New PdfPCell(tmpImage)
                CellContents.Border = 0
                table.AddCell(CellContents)
            End If
            ' -----------------------------------------------------------------
            ' Add invoice Date.
            ' -----------------------------------------------------------------
            Dim tmpHeadingInfo As String = "JOB INFORMATION MANAGER" & vbCrLf & "ACCOUNTING EXTRACT AUDIT LISTING" & vbCrLf & vbCrLf &
                "Extract Run: " & String.Format("{0:MM/dd/yyyy HH:mm}", DateTime.Now) & vbCrLf & vbCrLf &
                 "Report Name: " & tmpReportName

            '
            iTSFillCell(tmpHeadingInfo, table, 0, 1, 0, "")
            '
            iTSFillCell("", table, 0, 2, 0, "")
        End If
        '
        ' Add Heading for Extracted Section
        '
        If tmpSection = "ACCOUNTING" Then
            iTSFillCell("TRANSACTIONS EXTRACTED TO ACCOUNTING", table, 0, 2, 2, "YELLOW")
        Else
            iTSFillCell("MISCELLANEOUS TRANSACTIONS", table, 0, 2, 2, "YELLOW")
        End If

        '
        myDoc.Add(table)
        '
        Return ""
    End Function
    Private Function JTaePrtSectionHeader(ByRef myDoc As Document, tmpHeaderText As String) As String
        Dim table As New PdfPTable(1)
        ' Actual width of table in points
        table.TotalWidth = 640.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 0.0F
        '

        iTSFillCell(tmpHeaderText, table, 1, 1, 1, "YELLOW")
        '
        myDoc.Add(table)
        Return ""
    End Function


    Private Function JTaeExtractInvoiceSection(tmpType As String) As Integer
        Dim tmpExtrctedKeys As New List(Of String)

        Dim tmpSub As Integer = 0
        Dim tmpJob As String = ""
        Dim tmpKey As String = ""
        Dim tmpRecordsExtracted As Integer = 0
        slJTaeIH.Clear()
        Dim tmpJTasItem As JTas.JTasIHStru = Nothing
        Do While JTas.JTasJRExt(tmpSub, tmpJob, tmpJTasItem, tmpKey) = "DATA"
            If tmpJTasItem.ihAcctExptDate = "" Then
                If tmpType = "E" Then
                    If tmpJTasItem.ihDoNotExpt = "" Then
                        tmpExtrctedKeys.Add(tmpKey)
                        slJTaeIH.Add(tmpKey, tmpJTasItem)
                        tmpRecordsExtracted += 1
                    End If
                Else
                    If tmpJTasItem.ihDoNotExpt = "X" Then
                        tmpExtrctedKeys.Add(tmpKey)
                        slJTaeIH.Add(tmpKey, tmpJTasItem)
                        tmpRecordsExtracted += 1
                    End If
                End If
            End If
        Loop
        tmpSub = 0
        Do While tmpSub < tmpExtrctedKeys.Count

            JTas.JTasUpdtExtrctDate(tmpExtrctedKeys(tmpSub))
            tmpSub += 1
        Loop
        tmpExtrctedKeys.Clear()
        Return tmpRecordsExtracted
    End Function
    Private Function JTaeBldInvoiceSection(ByRef myDoc As Document, tmpType As String) As String
        ' -----------------------------------------------------------------------------------------
        ' Builds printlines for AE report.
        ' -----------------------------------------------------------------------------------------
        Dim tmpTotalRec As JTas.JTasIHStru = Nothing
        Dim tmpLastCustomer As String = ""
        Dim tmpLastInvNum As String = ""
        Dim tmpFirstRec As Boolean = True
        Dim keys As ICollection = slJTaeIH.Keys
        Dim k As String = ""
        Dim tmpSub As Integer = 0
        Do While tmpSub < keys.Count
            k = keys(tmpSub)
            '     For Each k In keys
            If tmpFirstRec Then
                tmpTotalRec = slJTaeIH(k)
                tmpFirstRec = False
            End If
            If tmpLastInvNum <> "" Then
                If tmpLastCustomer <> "" Then
                    If slJTaeIH(k).ihInvNum = tmpLastInvNum Then
                        If slJTaeIH(k).ihJobId = tmpLastCustomer Then
                            ' Add to inv totals
                            tmpTotalRec.ihJobID = "Total"
                            tmpTotalRec.ihSubID = slJTaeIH(k).ihInvNum
                            tmpTotalRec.ihLab = Val(tmpTotalRec.ihLab) + Val(slJTaeIH(k).ihLab)
                            tmpTotalRec.ihMat = Val(tmpTotalRec.ihMat) + Val(slJTaeIH(k).ihMat)
                            tmpTotalRec.ihSub = Val(tmpTotalRec.ihSub) + Val(slJTaeIH(k).ihSub)
                            tmpTotalRec.ihAdj = Val(tmpTotalRec.ihAdj) + Val(slJTaeIH(k).ihAdj)
                            tmpTotalRec.ihIH = Val(tmpTotalRec.ihIH) + Val(slJTaeIH(k).ihIH)
                            tmpTotalRec.ihAUPInv = Val(tmpTotalRec.ihAUPInv) + Val(slJTaeIH(k).ihAUPInv)
                            tmpTotalRec.ihInvTl = Val(tmpTotalRec.ihInvTl) + Val(slJTaeIH(k).ihInvTl)
                        End If
                    End If
                End If
            End If

            If slJTaeIH(k).ihInvNum <> tmpLastInvNum Then
                If tmpTotalRec.ihJobID = "Total" Then
                    'print invoice total line and clear totals
                    JTBldInvLines(myDoc, tmpTotalRec)
                    If tmpType = "E" Then         ' If Extract mode, put out invoice total amount to QBExt
                        JTQBExt.JTQBExtInvAmts(tmpTotalRec)
                    End If
                End If
                tmpTotalRec = slJTaeIH(k)
            End If
            If tmpSub + 1 <> keys.Count Then
                If tmpType = "E" Then
                    If slJTaeIH(k).ihJobId <> tmpLastCustomer Then
                        JTaeBldCustName(myDoc, slJTaeIH(k).ihJobId, slJTaeIH(k).ihSubID, tmpType)
                    End If
                End If
            End If
            tmpLastCustomer = slJTaeIH(k).ihJobId
            tmpLastInvNum = slJTaeIH(k).ihInvNum
            JTBldInvLines(myDoc, slJTaeIH(k))
            If tmpType = "E" Then          'output invoice data for simple, single job invoice
                If tmpSub + 1 = keys.Count Then
                    JTQBExt.JTQBExtInvAmts(slJTaeIH(k))
                Else
                    If slJTaeIH(k).ihInvNum <> slJTaeIH(keys(tmpSub + 1)).ihInvNum Then
                        JTQBExt.JTQBExtInvAmts(slJTaeIH(k))
                    End If
                End If
            End If
            '       Next
            tmpSub += 1
        Loop
        ' See if last invoice printed needed a total.
        If tmpTotalRec.ihJobID = "Total" Then
            'print invoice total line and clear totals
            JTBldInvLines(myDoc, tmpTotalRec)
            If tmpType = "E" Then         ' If Extract mode, put out invoice total amount to QBExt
                JTQBExt.JTQBExtInvAmts(tmpTotalRec)
            End If
        End If
        tmpTotalRec = slJTaeIH(k)
        Return ""
        '
    End Function

    Private Function JTaeBldCustName(ByRef myDoc As Document,
                                     tmpJobId As String,
                                     tmpSubID As String,
                                     tmpType As String) As String
        Dim table As New PdfPTable(3)
        ' Actual width of table in points
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.0F, 4.0F, 3.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 5.0F
        table.SpacingAfter = 0.0F
        '
        Dim tmpCustomerID As String = "Customer Key:" & vbCrLf & tmpJobId
        '
        iTSFillCell(tmpCustomerID, table, 0, 1, 0, "")
        Dim tmpCustInfo As JTjim.JTjimCustInfo = Nothing
        Dim tmpCustAddress As String = ""
        If JTjim.JTjimReadCustRec(tmpCustInfo, tmpJobId) = "DATA" Then
            tmpCustAddress = tmpCustInfo.JTjimCIContact & vbCrLf &
                tmpCustInfo.JTjimCIBillStreet & vbCrLf &
                tmpCustInfo.JTjimCIBillCity
            If tmpType = "E" Then       ' output customer info to QBExt if in Extract mode
                JTQBExt.JTQBCustName(tmpJobId,
                                 tmpCustInfo.JTjimCIContact,
                                 tmpCustInfo.JTjimCIBillStreet,
                                 tmpCustInfo.JTjimCIBillCity)
            End If
        Else
                MsgBox("Customer Information not available for " & tmpJobId & "(AE.001")
        End If
        iTSFillCell(tmpCustAddress, table, 0, 1, 0, "")
        iTSFillCell("", table, 1, 0, 0, "")
        myDoc.Add(table)
        tmpCustomerID = Nothing
        tmpCustAddress = Nothing
        tmpCustInfo = Nothing

        Return ""
    End Function
    Private Function JTBldInvLines(ByRef myDoc As Document, tmpRec As JTas.JTasIHStru) As String
        Dim table As New PdfPTable(6)
        ' Actual width of table in points
        table.TotalWidth = 640.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.0F, 1.4F, 4.3F, 3.0F, 2.0F, 1.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 0.0F
        table.SpacingAfter = 0.0F
        '
        '

        iTSFillCell(tmpRec.ihInvDate, table, 1, 1, 1, "")
        iTSFillCell(tmpRec.ihInvNum, table, 1, 1, 1, "")
        iTSFillCell(tmpRec.ihJobID & "~" & tmpRec.ihSubID, table, 1, 1, 1, "")
        Dim tmpCatAmts As String = ""
        If Val(tmpRec.ihLab) <> 0 Then
            tmpCatAmts = "Labor     -->" & String.Format("{0:0.00}", Val(tmpRec.ihLab))
        End If
        If Val(tmpRec.ihMat) <> 0 Then
            If tmpCatAmts.Length > 0 Then
                tmpCatAmts = tmpCatAmts & vbCrLf
            End If
            tmpCatAmts = tmpCatAmts & "Materials -->" & String.Format("{0:0.00}", Val(tmpRec.ihMat))
        End If

        If Val(tmpRec.ihSub) <> 0 Then
            If tmpCatAmts.Length > 0 Then
                tmpCatAmts = tmpCatAmts & vbCrLf
            End If
            tmpCatAmts = tmpCatAmts & "Services -->" & String.Format("{0:0.00}", Val(tmpRec.ihSub))
        End If
        If Val(tmpRec.ihAdj) <> 0 Then
            If tmpCatAmts.Length > 0 Then
                tmpCatAmts = tmpCatAmts & vbCrLf
            End If
            tmpCatAmts = tmpCatAmts & "Adjustment -->" & String.Format("{0:0.00}", Val(tmpRec.ihAdj))
        End If
        If Val(tmpRec.ihIH) <> 0 Then
            If tmpCatAmts.Length > 0 Then
                tmpCatAmts = tmpCatAmts & vbCrLf
            End If
            tmpCatAmts = tmpCatAmts & "In-House -->" & String.Format("{0:0.00}", Val(tmpRec.ihIH))
        End If
        If Val(tmpRec.ihAUPInv) <> 0 Then
            If tmpCatAmts.Length > 0 Then
                tmpCatAmts = tmpCatAmts & vbCrLf
            End If
            tmpCatAmts = tmpCatAmts & "AUP -->" & String.Format("{0:0.00}", Val(tmpRec.ihAUPInv))
        End If
        iTSFillCell(tmpCatAmts, table, 2, 1, 1, "")
        '
        iTSFillCell(String.Format("{0:0.00}", Val(tmpRec.ihInvTl)), table, 2, 1, 1, "")
        iTSFillCell("", table, 1, 1, 1, "")
        '
        myDoc.Add(table)
        '


        Return ""
    End Function
    Private Function JTaeExtractStatementSection(tmpType) As Integer
        Dim tmpExtrctedKeys As New List(Of String)
        Dim tmpStmtsExtracted As Integer = 0
        slJTaeNLC.Clear()
        Dim tmpNLCItem As JTnlc.JTnlcStructure = Nothing
        Dim tmpNLCKey As String = Nothing
        Dim tmpSub As Integer = 0
        Dim ExtRecType As String = "STMT"
        JTnlc.JTnlcPutinJobSeq()     ' call function to make sure DB is in Job sequence
        Do While JTnlc.JTnlcAEExtract(tmpSub, ExtRecType, tmpType, tmpNLCKey, tmpNLCItem) = "DATA"
            tmpStmtsExtracted += 1
            '        If ExtRecType = "STMT" Then
            '       tmpNLCKey = tmpNLCItem.JobID & tmpNLCItem.SubID
            '      End If
            tmpExtrctedKeys.Add(tmpNLCKey)
            Dim tmpFLSupplier As String = tmpNLCItem.Supplier.PadRight(15, "_")
            Dim tmpFLStmtNum As String = tmpNLCItem.SupInvNum.PadRight(10, "_")
            tmpNLCKey = tmpFLSupplier & tmpFLStmtNum & tmpNLCItem.SubID 
            If slJTaeNLC.ContainsKey(tmpNLCKey) Then
                MsgBox("Accounting Extract Report - Extract Error" & vbCrLf &
                       "Extract Type - " & tmpType & vbCrLf &
                       "Duplicate Key found - " & tmpNLCKey & vbCrLf & vbCrLf &
                       "Duplicate Record is excluded from report." & vbCrLf & "JTae~537")
            Else
                slJTaeNLC.Add(tmpNLCKey, tmpNLCItem)
            End If
        Loop
        tmpSub = 0
        Do While tmpSub < tmpExtrctedKeys.Count
            JTnlc.JTnlcUpdtAcctExtrctDate(tmpExtrctedKeys(tmpSub), JTaeFileRef)
            tmpSub += 1
        Loop
        tmpExtrctedKeys.Clear()
        ' --------------------------------------------------------------------------------
        ' Add code here to update Acct Ext Date on slJTnlc.
        ' --------------------------------------------------------------------------------
        '
        ' --------------------------------------------------------------------------------
        ' code  to reindex slJTaeNLC so that record come in correct order for printing..
        ' --------------------------------------------------------------------------------
        '   Dim keys As ICollection = slJTaeNLC.Keys
        '  Dim key2 As ICollection = keys
        ' tmpSub = 0
        '     Do While tmpSub < keys.Count
        '    tmpNLCItem = slJTaeNLC(key2(tmpSub))
        '   If slJTaeNLC.ContainsKey(key2(tmpSub)) = True Then
        '  slJTaeNLC.Remove(key2(tmpSub))
        '      Else
        '    End If

        '       Dim tmpFLSupplier As String = tmpNLCItem.Supplier.PadRight(15, "_")
        '      Dim tmpFLStmtNum As String = tmpNLCItem.SupInvNum.PadRight(10, "_")
        '     tmpNLCKey = tmpFLSupplier & tmpFLStmtNum & tmpNLCItem.SubID
        '    slJTaeNLC.Add(tmpNLCKey, tmpNLCItem)
        '   tmpSub += 1
        '  Loop
        tmpNLCItem = Nothing
        tmpNLCKey = Nothing

        Return tmpStmtsExtracted
    End Function
    Private Function JTaeBldStatementSection(ByRef myDoc As Document, tmpType As String) As String
        '
        Dim tmpFirstAccountLine As Boolean = True
        Dim tmpFirstStmt As Boolean = True
        Dim keys As ICollection = slJTaeNLC.Keys
        Dim k As String = Nothing
        Dim tmpAccumKeys As String = Nothing
        For Each k In keys
            tmpAccumKeys = tmpAccumKeys & vbCrLf & k
            If slJTaeNLC(k).SubID.substring(0, 1) = "0" Then
                If tmpType = "E" Then
                    If tmpFirstStmt <> True Then
                        JTQBExt.JTQBExtStmtENDTRNS()
                    End If
                    tmpFirstStmt = False
                    JTQBExt.JTQBExtStmtHeader(slJTaeNLC(k))
                End If
                ' Set up table for Statement header record.
                Dim table As New PdfPTable(4)
                ' Actual width of table in points
                table.TotalWidth = 640.0F 'fix the absolute width of the table
                table.LockedWidth = True
                table.HeaderRows = 0
                '
                ' Column widths are proportional to other columns and the table width.
                Dim widths() As Single = {2.0F, 4.0F, 2.0F, 2.0F}
                table.SetWidths(widths)
                '
                table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
                ' leave a gap before and after the table
                table.SpacingBefore = 10.0F
                table.SpacingAfter = 2.5F
                ' Load data into table
                Dim tmpData As String = "Vendor:" & vbCrLf & slJTaeNLC(k).Supplier
                iTSFillCell(tmpData, table, 0, 1, 1, "")
                Dim tmpJTviRec As JTvi.JTviArrayStructure = Nothing
                If JTvi.JTviRetrieveVendInfo(slJTaeNLC(k).Supplier & "_STMT", tmpJTviRec) = "DATA" Then
                    tmpData = "Acct # - " & tmpJTviRec.JTviAcctNum & vbCrLf &
                        tmpJTviRec.JTviVendName & vbCrLf &
                        tmpJTviRec.JTviStreet & vbCrLf &
                        tmpJTviRec.JTviCityStateZip
                Else
                    tmpData = "No Data for Vendor"
                End If
                iTSFillCell(tmpData, table, 0, 1, 1, "")
                tmpData = "Invoice:" & vbCrLf &
                        "Number - " & slJTaeNLC(k).SupInvNum & vbCrLf &
                        "Date - " & slJTaeNLC(k).InvDate
                iTSFillCell(tmpData, table, 0, 1, 1, "")
                tmpData = "Total - " & slJTaeNLC(k).SupCost & vbCrLf &
                        "Due Date - " & slJTaeNLC(k).viDueDate
                iTSFillCell(tmpData, table, 2, 1, 1, "")
                '
                myDoc.Add(table)
                tmpFirstAccountLine = True
            End If
            If slJTaeNLC(k).SubID.substring(0, 1) = "2" Then
                If tmpType = "E" Then
                    JTQBExt.JTQBExtStmtSPL(slJTaeNLC(k))
                End If
                ' Set up table for Statement detail records.
                Dim table As New PdfPTable(3)
                ' Actual width of table in points
                table.TotalWidth = 420.0F 'fix the absolute width of the table
                table.LockedWidth = True
                table.HeaderRows = 0
                '
                ' Column widths are proportional to other columns and the table width.
                Dim widths() As Single = {2.0F, 4.0F, 1.0F}
                table.SetWidths(widths)
                '
                table.HorizontalAlignment = 170 ' Determines the location of the table on the page.
                ' leave a gap before and after the table
                table.SpacingBefore = 0.0F
                table.SpacingAfter = 0.0F
                'Load data into table.
                If tmpFirstAccountLine = True Then
                    iTSFillCell("Statement Charges by Account", table, 1, 3, 1, "")
                    tmpFirstAccountLine = False
                End If
                iTSFillCell(slJTaeNLC(k).NJREAcct, table, 0, 1, 1, "")
                Dim tmpAcct As String = slJTaeNLC(k).NJREAcct
                Dim tmpDesc As String = Nothing
                If JTVarMaint.JTvmValidateUpdateNJREA("Validate", tmpAcct, tmpDesc) = "EXISTS" Then
                    iTSFillCell(tmpDesc, table, 0, 1, 1, "")
                Else
                    Select Case tmpAcct
                        Case "JOBRELATED:MAT"
                            iTSFillCell("MATERIAL - CHARGED TO JOBS", table, 0, 1, 1, "")
                        Case "JOBRELATED:SER"
                            iTSFillCell("SUBCONTRACTORS - CHARGED TO JOBS", table, 0, 1, 1, "")
                        Case Else
                            iTSFillCell("", table, 0, 1, 1, "")
                    End Select

                End If
                '
                iTSFillCell(String.Format("{0:0.00}", Val(slJTaeNLC(k).SupCost)), table, 2, 1, 1, "")
                '
                myDoc.Add(table)

            End If
        Next
        If tmpType = "E" Then                 ' Put out ENDTRNS record for last Stmt
            JTQBExt.JTQBExtStmtENDTRNS()
        End If
        Return ""
    End Function
    Private Function JTaeExtractVndInvSection(tmpInvType As String, tmpPymtType As String) As Integer
        Dim tmpExtrctedKeys As New List(Of String)
        Dim tmpVndInvExtracted As Integer = 0
        slJTaeNLC.Clear()
        Dim tmpNLCItem As JTnlc.JTnlcStructure = Nothing
        Dim tmpNLCKey As String = Nothing
        Dim tmpSub As Integer = 0
        '      MsgBox("Starting array build - JTae~654")
        Do While JTnlc.JTnlcAEExtract(tmpSub, tmpInvType, tmpPymtType, tmpNLCKey, tmpNLCItem) = "DATA"
            tmpVndInvExtracted += 1
            tmpExtrctedKeys.Add(tmpNLCKey)
            slJTaeNLC.Add(tmpNLCKey, tmpNLCItem)
        Loop
        ' --------------------------------------------------------------------------------
        ' Add code here to update Acct Ext Date on slJTnlc.
        ' --------------------------------------------------------------------------------
        '
        tmpSub = 0
        Do While tmpSub < tmpExtrctedKeys.Count
            JTnlc.JTnlcUpdtAcctExtrctDate(tmpExtrctedKeys(tmpSub), JTaeFileRef)
            tmpSub += 1
        Loop
        tmpExtrctedKeys.Clear()
        '
        tmpNLCItem = Nothing
        tmpNLCKey = Nothing

        Return tmpVndInvExtracted
    End Function
    Private Function JTaeBldVndInvSection(ByRef myDoc As Document, tmpInvType As String, tmpPymtType As String) As String
        Dim table As New PdfPTable(8)
        ' Actual width of table in points
        table.TotalWidth = 640.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 1
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.2F, 2.5F, 4.6F, 1.4F, 4.0F, 2.0F, 2.2F, 3.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        ' -----------------------------------------------------------------------
        ' Build Heading Line
        ' -----------------------------------------------------------------------
        Dim tmpData As String = "Invoice Date"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Supplier ID"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Supplier Name & Address"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Inv. #"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Description"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Invoice" & vbCrLf & "Amount"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Terms /" & vbCrLf & "Due Date"
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        tmpData = "Job" & vbCrLf & "Inv. #/Date"
        If tmpInvType = "NJR" Then
            tmpData = "NJR Account"
        End If
        iTSFillCell(tmpData, table, 1, 1, 1, "")
        '
        Dim keys As ICollection = slJTaeNLC.Keys
        Dim k As String = Nothing

        For Each k In keys

            ' Set up table for Statement header record.

            ' Load data into table
            tmpData = "Vendor:" & vbCrLf & slJTaeNLC(k).Supplier
            iTSFillCell(slJTaeNLC(k).InvDate, table, 1, 1, 1, "")
            iTSFillCell(slJTaeNLC(k).Supplier, table, 0, 1, 1, "")
            '
            Dim tmpJTviRec As JTvi.JTviArrayStructure = Nothing
            If JTvi.JTviRetrieveVendInfo(slJTaeNLC(k).Supplier, tmpJTviRec) = "DATA" Then
                tmpData = "Account #" & tmpJTviRec.JTviAcctNum & vbCrLf &
                    tmpJTviRec.JTviVendName & vbCrLf &
                    tmpJTviRec.JTviStreet & vbCrLf &
                    tmpJTviRec.JTviCityStateZip
            Else
                tmpData = "No Vendor Record Found"
            End If
            iTSFillCell(tmpData, table, 0, 1, 1, "")
            iTSFillCell(slJTaeNLC(k).SupInvNum, table, 1, 1, 1, "")
            iTSFillCell(slJTaeNLC(k).Descrpt, table, 0, 1, 1, "")
            iTSFillCell(String.Format("{0:0.00}", Val(slJTaeNLC(k).SupCost)), table, 2, 1, 1, "")
            iTSFillCell(slJTaeNLC(k).viDueDate & vbCrLf & slJTaeNLC(k).viTerms, table, 0, 1, 1, "")
            If slJTaeNLC(k).MorS <> "NJR" Then
                tmpData = slJTaeNLC(k).JobID & "~" & slJTaeNLC(k).SubID
            Else
                tmpData = slJTaeNLC(k).NJREAcct
            End If
            If slJTaeNLC(k).CustInvNumber <> "" Then
                tmpData = tmpData & vbCrLf & slJTaeNLC(k).CustInvNumber & "-" & slJTaeNLC(k).CustInvDate
            End If
            iTSFillCell(tmpData, table, 1, 1, 1, "")
            If tmpPymtType = "E" Then
                JTQBExt.JTQBExtVendInvoices(tmpInvType, slJTaeNLC(k))
            End If
        Next
        myDoc.Add(table)
        Return ""
    End Function
End Class
