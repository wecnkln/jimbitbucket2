﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTVarMaint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTVarMaint))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabCompInfo = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.txtJTvmAltCompanyLogo = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtJTvmAltCompAddL2 = New System.Windows.Forms.TextBox()
        Me.txtJTvmAltCompAddL1 = New System.Windows.Forms.TextBox()
        Me.txtJTvmAltCompAddL3 = New System.Windows.Forms.TextBox()
        Me.txtJTvmAltCompName = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmGoogleValidation = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtJTvmCompanyLogo = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtJTvmCompFax = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompAddL2 = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompContact = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompPhone = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompEmail = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompAddL1 = New System.Windows.Forms.TextBox()
        Me.txtJTVMCompAddL3 = New System.Windows.Forms.TextBox()
        Me.txtJTvmCompName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.tabMargin = New System.Windows.Forms.TabPage()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtJTvmINHOUSEMargin = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtJTvmSubMargin = New System.Windows.Forms.TextBox()
        Me.txtJTvmMatMargin = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.tabDataMgmt = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmLstTKPurge = New System.Windows.Forms.TextBox()
        Me.txtJTvmLstNLCPurge = New System.Windows.Forms.TextBox()
        Me.txtJTvmPurgeTiming = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmDPJMstr = New System.Windows.Forms.TextBox()
        Me.txtJTvmDPJSub = New System.Windows.Forms.TextBox()
        Me.txtJTvmDPJNorm = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtJTvmNLCMinLife = New System.Windows.Forms.TextBox()
        Me.txtJTvmMinDaysAfterInv = New System.Windows.Forms.TextBox()
        Me.txtJTvmLabMinLife = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.tabPayroll = New System.Windows.Forms.TabPage()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmPRTaxLoad = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmPREmailCC = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtJTvmPREmailFrom = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtJTvmPREmailTo = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.txtJTvm1099EmailText = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cboxJTvm1099Email = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmW2Email = New System.Windows.Forms.CheckBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.rtxtJTvmW2EmailText = New System.Windows.Forms.RichTextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.tabBkupSec = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cboxJTvmBkupAch = New System.Windows.Forms.CheckBox()
        Me.txtJTvmBkupAchSerNum = New System.Windows.Forms.TextBox()
        Me.txtJTvmBkupAchDate = New System.Windows.Forms.TextBox()
        Me.txtJTvmBkupAchPath = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtJTvmBkupAchGen = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtJTvmBkupAchFreq = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboxJTvmUsePW = New System.Windows.Forms.CheckBox()
        Me.txtJTvmPWConfirm = New System.Windows.Forms.TextBox()
        Me.txtJTvmPWNew = New System.Windows.Forms.TextBox()
        Me.txtJTvmCurPW = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TabNLCRecon = New System.Windows.Forms.TabPage()
        Me.gboxJTvmNJRAMaint = New System.Windows.Forms.GroupBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtJTvmNJRAMDesc = New System.Windows.Forms.TextBox()
        Me.txtJTvmNJRAMAcct = New System.Windows.Forms.TextBox()
        Me.btnJTgvNJRAMCancel = New System.Windows.Forms.Button()
        Me.btnJTgvNJRAMDelete = New System.Windows.Forms.Button()
        Me.btnJTgvNJRAMAddChg = New System.Windows.Forms.Button()
        Me.GroupBox27 = New System.Windows.Forms.GroupBox()
        Me.cboxJTvmNJRAccountPrint = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmNJRAccountAdd = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmNJRAccountChgDel = New System.Windows.Forms.CheckBox()
        Me.lvJTvmNJRAccounts = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.cboxReconSuggest = New System.Windows.Forms.CheckBox()
        Me.cboxReconNJR = New System.Windows.Forms.CheckBox()
        Me.cboxRecon1099 = New System.Windows.Forms.CheckBox()
        Me.cboxReconSub = New System.Windows.Forms.CheckBox()
        Me.cboxReconMat = New System.Windows.Forms.CheckBox()
        Me.TextBox44 = New System.Windows.Forms.TextBox()
        Me.TabInvSettings = New System.Windows.Forms.TabPage()
        Me.GroupBox25 = New System.Windows.Forms.GroupBox()
        Me.txtInvFooter = New System.Windows.Forms.TextBox()
        Me.GroupBox21 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmInvCCEmail = New System.Windows.Forms.TextBox()
        Me.GroupBox19 = New System.Windows.Forms.GroupBox()
        Me.txtJTvmAltInvNumber = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtJTvmInvNumber = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.cboxCstPrntSub = New System.Windows.Forms.CheckBox()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.GroupBox20 = New System.Windows.Forms.GroupBox()
        Me.rbtnInvMatTotals = New System.Windows.Forms.RadioButton()
        Me.rbtnInvMatDetail = New System.Windows.Forms.RadioButton()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.rbtnInvSubSerDetails = New System.Windows.Forms.RadioButton()
        Me.rbtnInvSubSerTotals = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbtnInvLaborTotals = New System.Windows.Forms.RadioButton()
        Me.rbtnInvWrkrDet = New System.Windows.Forms.RadioButton()
        Me.rbtnInvLabTskSum = New System.Windows.Forms.RadioButton()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.cboxCstPrntMat = New System.Windows.Forms.CheckBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TabGenSysVar = New System.Windows.Forms.TabPage()
        Me.gbocJTgvEventLog = New System.Windows.Forms.GroupBox()
        Me.cboxJTgvEventLogEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox26 = New System.Windows.Forms.GroupBox()
        Me.cboxJTvmScanAUP = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmScanReconcil = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmScanNLC = New System.Windows.Forms.CheckBox()
        Me.GroupBox24 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTgvNLCSeqDate = New System.Windows.Forms.RadioButton()
        Me.rbtnJTgvNLCSeqJob = New System.Windows.Forms.RadioButton()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox22 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTgvMMJobAmt = New System.Windows.Forms.RadioButton()
        Me.rbtnJTgvMMJobAlpha = New System.Windows.Forms.RadioButton()
        Me.TabSalesTax = New System.Windows.Forms.TabPage()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtJTvmSTStRate = New System.Windows.Forms.TextBox()
        Me.CboxJTvmSTStHasTax = New System.Windows.Forms.CheckBox()
        Me.txtJTvmSTState = New System.Windows.Forms.TextBox()
        Me.btnJTvmSTCancel = New System.Windows.Forms.Button()
        Me.btnJTvmSTUpdtAdd = New System.Windows.Forms.Button()
        Me.txtJTvmStSubRate = New System.Windows.Forms.TextBox()
        Me.txtJTvmSTMatRate = New System.Windows.Forms.TextBox()
        Me.txtJTvmStLabRate = New System.Windows.Forms.TextBox()
        Me.cboxJTvmSTSubHasTax = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmSTMatHasTax = New System.Windows.Forms.CheckBox()
        Me.cboxJTvmSTLabHasTax = New System.Windows.Forms.CheckBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.lvJtvmTax = New System.Windows.Forms.ListView()
        Me.ColState = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTaxSwitch = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTaxRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColLabTxSw = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColLabRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColSubTxSw = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColSubRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColMatTxSw = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColMatRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabWebData = New System.Windows.Forms.TabPage()
        Me.GroupBox28 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTvmEmailExtOff = New System.Windows.Forms.RadioButton()
        Me.rbtnJTvmEmailExtOn = New System.Windows.Forms.RadioButton()
        Me.GroupBox30 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTvmImageExtOff = New System.Windows.Forms.RadioButton()
        Me.rbtnJTvmImageExtOn = New System.Windows.Forms.RadioButton()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.btnDone = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cboxJTgvDashBoard = New System.Windows.Forms.CheckBox()
        Me.GboJTStrtUpSysRecap = New System.Windows.Forms.GroupBox()
        Me.TabControl1.SuspendLayout()
        Me.tabCompInfo.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.tabMargin.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.tabDataMgmt.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.tabPayroll.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.tabBkupSec.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabNLCRecon.SuspendLayout()
        Me.gboxJTvmNJRAMaint.SuspendLayout()
        Me.GroupBox27.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.TabInvSettings.SuspendLayout()
        Me.GroupBox25.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.TabGenSysVar.SuspendLayout()
        Me.gbocJTgvEventLog.SuspendLayout()
        Me.GroupBox26.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.GroupBox22.SuspendLayout()
        Me.TabSalesTax.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        Me.TabWebData.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.GboJTStrtUpSysRecap.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabCompInfo)
        Me.TabControl1.Controls.Add(Me.tabMargin)
        Me.TabControl1.Controls.Add(Me.tabDataMgmt)
        Me.TabControl1.Controls.Add(Me.tabPayroll)
        Me.TabControl1.Controls.Add(Me.tabBkupSec)
        Me.TabControl1.Controls.Add(Me.TabNLCRecon)
        Me.TabControl1.Controls.Add(Me.TabInvSettings)
        Me.TabControl1.Controls.Add(Me.TabGenSysVar)
        Me.TabControl1.Controls.Add(Me.TabSalesTax)
        Me.TabControl1.Controls.Add(Me.TabWebData)
        Me.TabControl1.Location = New System.Drawing.Point(68, 38)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(795, 456)
        Me.TabControl1.TabIndex = 0
        '
        'tabCompInfo
        '
        Me.tabCompInfo.Controls.Add(Me.GroupBox5)
        Me.tabCompInfo.Controls.Add(Me.GroupBox11)
        Me.tabCompInfo.Controls.Add(Me.TextBox7)
        Me.tabCompInfo.Location = New System.Drawing.Point(4, 25)
        Me.tabCompInfo.Name = "tabCompInfo"
        Me.tabCompInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCompInfo.Size = New System.Drawing.Size(787, 427)
        Me.tabCompInfo.TabIndex = 0
        Me.tabCompInfo.Text = "Comp. Info"
        Me.tabCompInfo.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox5.Controls.Add(Me.TextBox8)
        Me.GroupBox5.Controls.Add(Me.Label45)
        Me.GroupBox5.Controls.Add(Me.txtJTvmAltCompanyLogo)
        Me.GroupBox5.Controls.Add(Me.Label46)
        Me.GroupBox5.Controls.Add(Me.txtJTvmAltCompAddL2)
        Me.GroupBox5.Controls.Add(Me.txtJTvmAltCompAddL1)
        Me.GroupBox5.Controls.Add(Me.txtJTvmAltCompAddL3)
        Me.GroupBox5.Controls.Add(Me.txtJTvmAltCompName)
        Me.GroupBox5.Controls.Add(Me.Label47)
        Me.GroupBox5.Controls.Add(Me.Label51)
        Me.GroupBox5.Location = New System.Drawing.Point(405, 114)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(365, 282)
        Me.GroupBox5.TabIndex = 22
        Me.GroupBox5.TabStop = False
        '
        'TextBox8
        '
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(16, 11)
        Me.TextBox8.Multiline = True
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(331, 73)
        Me.TextBox8.TabIndex = 26
        Me.TextBox8.Text = "Optional Alternate Company Name & Address" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If these fields are populated, they wi" &
    "ll be used as the company information for the Alt. Invoice Number sequence. If b" &
    "lank, the primary entries will be used."
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(14, 231)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(185, 16)
        Me.Label45.TabIndex = 25
        Me.Label45.Text = "Alt. Company Logo File Name"
        '
        'txtJTvmAltCompanyLogo
        '
        Me.txtJTvmAltCompanyLogo.Location = New System.Drawing.Point(23, 252)
        Me.txtJTvmAltCompanyLogo.Name = "txtJTvmAltCompanyLogo"
        Me.txtJTvmAltCompanyLogo.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmAltCompanyLogo.TabIndex = 24
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(15, 330)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(0, 16)
        Me.Label46.TabIndex = 23
        '
        'txtJTvmAltCompAddL2
        '
        Me.txtJTvmAltCompAddL2.Location = New System.Drawing.Point(22, 181)
        Me.txtJTvmAltCompAddL2.Name = "txtJTvmAltCompAddL2"
        Me.txtJTvmAltCompAddL2.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmAltCompAddL2.TabIndex = 20
        '
        'txtJTvmAltCompAddL1
        '
        Me.txtJTvmAltCompAddL1.Location = New System.Drawing.Point(22, 155)
        Me.txtJTvmAltCompAddL1.Name = "txtJTvmAltCompAddL1"
        Me.txtJTvmAltCompAddL1.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmAltCompAddL1.TabIndex = 16
        '
        'txtJTvmAltCompAddL3
        '
        Me.txtJTvmAltCompAddL3.Location = New System.Drawing.Point(22, 206)
        Me.txtJTvmAltCompAddL3.Name = "txtJTvmAltCompAddL3"
        Me.txtJTvmAltCompAddL3.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmAltCompAddL3.TabIndex = 15
        '
        'txtJTvmAltCompName
        '
        Me.txtJTvmAltCompName.Location = New System.Drawing.Point(22, 112)
        Me.txtJTvmAltCompName.Name = "txtJTvmAltCompName"
        Me.txtJTvmAltCompName.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmAltCompName.TabIndex = 14
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(14, 135)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(140, 16)
        Me.Label47.TabIndex = 4
        Me.Label47.Text = "Alt. Company Address"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(14, 91)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(126, 16)
        Me.Label51.TabIndex = 0
        Me.Label51.Text = "Alt. Company Name"
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox11.Controls.Add(Me.txtJTvmGoogleValidation)
        Me.GroupBox11.Controls.Add(Me.Label10)
        Me.GroupBox11.Controls.Add(Me.Label39)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompanyLogo)
        Me.GroupBox11.Controls.Add(Me.Label38)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompFax)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompAddL2)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompContact)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompPhone)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompEmail)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompAddL1)
        Me.GroupBox11.Controls.Add(Me.txtJTVMCompAddL3)
        Me.GroupBox11.Controls.Add(Me.txtJTvmCompName)
        Me.GroupBox11.Controls.Add(Me.Label6)
        Me.GroupBox11.Controls.Add(Me.Label5)
        Me.GroupBox11.Controls.Add(Me.Label4)
        Me.GroupBox11.Controls.Add(Me.Label3)
        Me.GroupBox11.Controls.Add(Me.Label2)
        Me.GroupBox11.Location = New System.Drawing.Point(34, 16)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(365, 380)
        Me.GroupBox11.TabIndex = 21
        Me.GroupBox11.TabStop = False
        '
        'txtJTvmGoogleValidation
        '
        Me.txtJTvmGoogleValidation.Location = New System.Drawing.Point(24, 214)
        Me.txtJTvmGoogleValidation.Name = "txtJTvmGoogleValidation"
        Me.txtJTvmGoogleValidation.Size = New System.Drawing.Size(192, 22)
        Me.txtJTvmGoogleValidation.TabIndex = 27
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 192)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(191, 16)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Google 2-step Validation Code"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(14, 330)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(164, 16)
        Me.Label39.TabIndex = 25
        Me.Label39.Text = "Company Logo File Name"
        '
        'txtJTvmCompanyLogo
        '
        Me.txtJTvmCompanyLogo.Location = New System.Drawing.Point(23, 351)
        Me.txtJTvmCompanyLogo.Name = "txtJTvmCompanyLogo"
        Me.txtJTvmCompanyLogo.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmCompanyLogo.TabIndex = 24
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(15, 330)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(0, 16)
        Me.Label38.TabIndex = 23
        '
        'txtJTvmCompFax
        '
        Me.txtJTvmCompFax.Location = New System.Drawing.Point(185, 262)
        Me.txtJTvmCompFax.Name = "txtJTvmCompFax"
        Me.txtJTvmCompFax.Size = New System.Drawing.Size(146, 22)
        Me.txtJTvmCompFax.TabIndex = 22
        '
        'txtJTvmCompAddL2
        '
        Me.txtJTvmCompAddL2.Location = New System.Drawing.Point(22, 96)
        Me.txtJTvmCompAddL2.Name = "txtJTvmCompAddL2"
        Me.txtJTvmCompAddL2.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmCompAddL2.TabIndex = 20
        '
        'txtJTvmCompContact
        '
        Me.txtJTvmCompContact.Location = New System.Drawing.Point(22, 308)
        Me.txtJTvmCompContact.Name = "txtJTvmCompContact"
        Me.txtJTvmCompContact.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmCompContact.TabIndex = 19
        '
        'txtJTvmCompPhone
        '
        Me.txtJTvmCompPhone.Location = New System.Drawing.Point(24, 262)
        Me.txtJTvmCompPhone.Name = "txtJTvmCompPhone"
        Me.txtJTvmCompPhone.Size = New System.Drawing.Size(140, 22)
        Me.txtJTvmCompPhone.TabIndex = 18
        '
        'txtJTvmCompEmail
        '
        Me.txtJTvmCompEmail.Location = New System.Drawing.Point(24, 166)
        Me.txtJTvmCompEmail.Name = "txtJTvmCompEmail"
        Me.txtJTvmCompEmail.Size = New System.Drawing.Size(273, 22)
        Me.txtJTvmCompEmail.TabIndex = 17
        '
        'txtJTvmCompAddL1
        '
        Me.txtJTvmCompAddL1.Location = New System.Drawing.Point(22, 70)
        Me.txtJTvmCompAddL1.Name = "txtJTvmCompAddL1"
        Me.txtJTvmCompAddL1.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmCompAddL1.TabIndex = 16
        '
        'txtJTVMCompAddL3
        '
        Me.txtJTVMCompAddL3.Location = New System.Drawing.Point(22, 121)
        Me.txtJTVMCompAddL3.Name = "txtJTVMCompAddL3"
        Me.txtJTVMCompAddL3.Size = New System.Drawing.Size(326, 22)
        Me.txtJTVMCompAddL3.TabIndex = 15
        '
        'txtJTvmCompName
        '
        Me.txtJTvmCompName.Location = New System.Drawing.Point(22, 26)
        Me.txtJTvmCompName.Name = "txtJTvmCompName"
        Me.txtJTvmCompName.Size = New System.Drawing.Size(326, 22)
        Me.txtJTvmCompName.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Company Address"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 146)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(219, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Company Email Address - Invoicing"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 242)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(211, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Company Telephone - Voice / Fax"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 287)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Company Contact"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Company Name"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(405, 6)
        Me.TextBox7.Multiline = True
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(376, 102)
        Me.TextBox7.TabIndex = 13
        Me.TextBox7.Text = "Company Info" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "These fields will be used in invoicing  as well as any other corres" &
    "pondence generated through the system."
        '
        'tabMargin
        '
        Me.tabMargin.Controls.Add(Me.GroupBox12)
        Me.tabMargin.Controls.Add(Me.TextBox5)
        Me.tabMargin.Location = New System.Drawing.Point(4, 25)
        Me.tabMargin.Name = "tabMargin"
        Me.tabMargin.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMargin.Size = New System.Drawing.Size(787, 427)
        Me.tabMargin.TabIndex = 1
        Me.tabMargin.Text = "Mrgn Settings"
        Me.tabMargin.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox12.Controls.Add(Me.Label43)
        Me.GroupBox12.Controls.Add(Me.txtJTvmINHOUSEMargin)
        Me.GroupBox12.Controls.Add(Me.Label42)
        Me.GroupBox12.Controls.Add(Me.txtJTvmSubMargin)
        Me.GroupBox12.Controls.Add(Me.txtJTvmMatMargin)
        Me.GroupBox12.Controls.Add(Me.Label9)
        Me.GroupBox12.Controls.Add(Me.Label8)
        Me.GroupBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox12.Location = New System.Drawing.Point(187, 153)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(440, 195)
        Me.GroupBox12.TabIndex = 15
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Margin Percentages"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(32, 164)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(354, 16)
        Me.Label43.TabIndex = 17
        Me.Label43.Text = "Margin % for INHOUSE only used for job margin estimates."
        '
        'txtJTvmINHOUSEMargin
        '
        Me.txtJTvmINHOUSEMargin.Location = New System.Drawing.Point(307, 126)
        Me.txtJTvmINHOUSEMargin.Name = "txtJTvmINHOUSEMargin"
        Me.txtJTvmINHOUSEMargin.Size = New System.Drawing.Size(75, 27)
        Me.txtJTvmINHOUSEMargin.TabIndex = 16
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(61, 131)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(207, 18)
        Me.Label42.TabIndex = 15
        Me.Label42.Text = "Imputed Margin % - INHOUSE"
        '
        'txtJTvmSubMargin
        '
        Me.txtJTvmSubMargin.Location = New System.Drawing.Point(307, 77)
        Me.txtJTvmSubMargin.Name = "txtJTvmSubMargin"
        Me.txtJTvmSubMargin.Size = New System.Drawing.Size(75, 27)
        Me.txtJTvmSubMargin.TabIndex = 14
        '
        'txtJTvmMatMargin
        '
        Me.txtJTvmMatMargin.Location = New System.Drawing.Point(307, 30)
        Me.txtJTvmMatMargin.Name = "txtJTvmMatMargin"
        Me.txtJTvmMatMargin.Size = New System.Drawing.Size(75, 27)
        Me.txtJTvmMatMargin.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(58, 33)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(118, 18)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Material Mrgin %"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(58, 83)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(168, 18)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Subcontractor Margin %"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(339, 6)
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(442, 131)
        Me.TextBox5.TabIndex = 12
        Me.TextBox5.Text = resources.GetString("TextBox5.Text")
        '
        'tabDataMgmt
        '
        Me.tabDataMgmt.Controls.Add(Me.GroupBox1)
        Me.tabDataMgmt.Controls.Add(Me.GroupBox8)
        Me.tabDataMgmt.Controls.Add(Me.GroupBox7)
        Me.tabDataMgmt.Controls.Add(Me.TextBox2)
        Me.tabDataMgmt.Controls.Add(Me.TextBox1)
        Me.tabDataMgmt.Location = New System.Drawing.Point(4, 25)
        Me.tabDataMgmt.Name = "tabDataMgmt"
        Me.tabDataMgmt.Size = New System.Drawing.Size(787, 427)
        Me.tabDataMgmt.TabIndex = 2
        Me.tabDataMgmt.Text = "DataMgmt"
        Me.tabDataMgmt.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox1.Controls.Add(Me.txtJTvmLstTKPurge)
        Me.GroupBox1.Controls.Add(Me.txtJTvmLstNLCPurge)
        Me.GroupBox1.Controls.Add(Me.txtJTvmPurgeTiming)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label44)
        Me.GroupBox1.Location = New System.Drawing.Point(37, 141)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(319, 98)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Purge Timing / Status"
        '
        'txtJTvmLstTKPurge
        '
        Me.txtJTvmLstTKPurge.Location = New System.Drawing.Point(184, 43)
        Me.txtJTvmLstTKPurge.Name = "txtJTvmLstTKPurge"
        Me.txtJTvmLstTKPurge.ReadOnly = True
        Me.txtJTvmLstTKPurge.Size = New System.Drawing.Size(108, 22)
        Me.txtJTvmLstTKPurge.TabIndex = 14
        '
        'txtJTvmLstNLCPurge
        '
        Me.txtJTvmLstNLCPurge.Location = New System.Drawing.Point(184, 68)
        Me.txtJTvmLstNLCPurge.Name = "txtJTvmLstNLCPurge"
        Me.txtJTvmLstNLCPurge.ReadOnly = True
        Me.txtJTvmLstNLCPurge.Size = New System.Drawing.Size(108, 22)
        Me.txtJTvmLstNLCPurge.TabIndex = 13
        '
        'txtJTvmPurgeTiming
        '
        Me.txtJTvmPurgeTiming.Location = New System.Drawing.Point(231, 17)
        Me.txtJTvmPurgeTiming.Name = "txtJTvmPurgeTiming"
        Me.txtJTvmPurgeTiming.Size = New System.Drawing.Size(61, 22)
        Me.txtJTvmPurgeTiming.TabIndex = 12
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(27, 71)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(132, 16)
        Me.Label26.TabIndex = 7
        Me.Label26.Text = "Last NLC Purge Date"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(27, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(164, 16)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "# of Days Between Purges"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(27, 44)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(123, 16)
        Me.Label44.TabIndex = 2
        Me.Label44.Text = "Last TK Purge Date"
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox8.Controls.Add(Me.txtJTvmDPJMstr)
        Me.GroupBox8.Controls.Add(Me.txtJTvmDPJSub)
        Me.GroupBox8.Controls.Add(Me.txtJTvmDPJNorm)
        Me.GroupBox8.Controls.Add(Me.Label13)
        Me.GroupBox8.Controls.Add(Me.Label18)
        Me.GroupBox8.Controls.Add(Me.Label17)
        Me.GroupBox8.Controls.Add(Me.Label14)
        Me.GroupBox8.Location = New System.Drawing.Point(38, 250)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(318, 136)
        Me.GroupBox8.TabIndex = 16
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Job Status Change Settings"
        '
        'txtJTvmDPJMstr
        '
        Me.txtJTvmDPJMstr.Location = New System.Drawing.Point(215, 46)
        Me.txtJTvmDPJMstr.Name = "txtJTvmDPJMstr"
        Me.txtJTvmDPJMstr.Size = New System.Drawing.Size(60, 22)
        Me.txtJTvmDPJMstr.TabIndex = 13
        '
        'txtJTvmDPJSub
        '
        Me.txtJTvmDPJSub.Location = New System.Drawing.Point(215, 70)
        Me.txtJTvmDPJSub.Name = "txtJTvmDPJSub"
        Me.txtJTvmDPJSub.Size = New System.Drawing.Size(60, 22)
        Me.txtJTvmDPJSub.TabIndex = 12
        '
        'txtJTvmDPJNorm
        '
        Me.txtJTvmDPJNorm.Location = New System.Drawing.Point(215, 21)
        Me.txtJTvmDPJNorm.Name = "txtJTvmDPJNorm"
        Me.txtJTvmDPJNorm.Size = New System.Drawing.Size(60, 22)
        Me.txtJTvmDPJNorm.TabIndex = 11
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(35, 96)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(222, 32)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "* Master job status not changed until " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  all its Sub jobs are inactive."
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(27, 46)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(150, 16)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "Job Cagtegory ""Master"""
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(27, 73)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(125, 16)
        Me.Label17.TabIndex = 8
        Me.Label17.Text = "Job Category ""Sub"""
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(27, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(145, 16)
        Me.Label14.TabIndex = 5
        Me.Label14.Text = "Job Category ""Normal"""
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox7.Controls.Add(Me.Label7)
        Me.GroupBox7.Controls.Add(Me.txtJTvmNLCMinLife)
        Me.GroupBox7.Controls.Add(Me.txtJTvmMinDaysAfterInv)
        Me.GroupBox7.Controls.Add(Me.txtJTvmLabMinLife)
        Me.GroupBox7.Controls.Add(Me.Label16)
        Me.GroupBox7.Controls.Add(Me.Label12)
        Me.GroupBox7.Controls.Add(Me.Label11)
        Me.GroupBox7.Location = New System.Drawing.Point(38, 10)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(319, 120)
        Me.GroupBox7.TabIndex = 15
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Data Purge Timing"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(64, 95)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(168, 16)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "*entry  represents # of days"
        '
        'txtJTvmNLCMinLife
        '
        Me.txtJTvmNLCMinLife.Location = New System.Drawing.Point(184, 43)
        Me.txtJTvmNLCMinLife.Name = "txtJTvmNLCMinLife"
        Me.txtJTvmNLCMinLife.Size = New System.Drawing.Size(61, 22)
        Me.txtJTvmNLCMinLife.TabIndex = 14
        '
        'txtJTvmMinDaysAfterInv
        '
        Me.txtJTvmMinDaysAfterInv.Location = New System.Drawing.Point(184, 68)
        Me.txtJTvmMinDaysAfterInv.Name = "txtJTvmMinDaysAfterInv"
        Me.txtJTvmMinDaysAfterInv.Size = New System.Drawing.Size(61, 22)
        Me.txtJTvmMinDaysAfterInv.TabIndex = 13
        '
        'txtJTvmLabMinLife
        '
        Me.txtJTvmLabMinLife.Location = New System.Drawing.Point(184, 19)
        Me.txtJTvmLabMinLife.Name = "txtJTvmLabMinLife"
        Me.txtJTvmLabMinLife.Size = New System.Drawing.Size(61, 22)
        Me.txtJTvmLabMinLife.TabIndex = 12
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(27, 71)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(113, 16)
        Me.Label16.TabIndex = 7
        Me.Label16.Text = "Min Days After Inv"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(27, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 16)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Labor Min Life"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 44)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 16)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "NLC Min Life"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(411, 234)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(364, 157)
        Me.TextBox2.TabIndex = 11
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(410, 8)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(364, 214)
        Me.TextBox1.TabIndex = 10
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'tabPayroll
        '
        Me.tabPayroll.Controls.Add(Me.GroupBox17)
        Me.tabPayroll.Controls.Add(Me.GroupBox9)
        Me.tabPayroll.Controls.Add(Me.TextBox31)
        Me.tabPayroll.Controls.Add(Me.txtJTvm1099EmailText)
        Me.tabPayroll.Controls.Add(Me.GroupBox6)
        Me.tabPayroll.Controls.Add(Me.TextBox6)
        Me.tabPayroll.Controls.Add(Me.GroupBox10)
        Me.tabPayroll.Location = New System.Drawing.Point(4, 25)
        Me.tabPayroll.Name = "tabPayroll"
        Me.tabPayroll.Size = New System.Drawing.Size(787, 427)
        Me.tabPayroll.TabIndex = 3
        Me.tabPayroll.Text = "P/R Info"
        Me.tabPayroll.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox17.Controls.Add(Me.txtJTvmPRTaxLoad)
        Me.GroupBox17.Controls.Add(Me.Label37)
        Me.GroupBox17.Location = New System.Drawing.Point(19, 7)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(303, 27)
        Me.GroupBox17.TabIndex = 21
        Me.GroupBox17.TabStop = False
        '
        'txtJTvmPRTaxLoad
        '
        Me.txtJTvmPRTaxLoad.Location = New System.Drawing.Point(181, 2)
        Me.txtJTvmPRTaxLoad.Name = "txtJTvmPRTaxLoad"
        Me.txtJTvmPRTaxLoad.Size = New System.Drawing.Size(52, 22)
        Me.txtJTvmPRTaxLoad.TabIndex = 1
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(7, 5)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(124, 16)
        Me.Label37.TabIndex = 0
        Me.Label37.Text = "Payroll Tax Load %"
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox9.Controls.Add(Me.txtJTvmPREmailCC)
        Me.GroupBox9.Controls.Add(Me.Label33)
        Me.GroupBox9.Controls.Add(Me.txtJTvmPREmailFrom)
        Me.GroupBox9.Controls.Add(Me.Label32)
        Me.GroupBox9.Controls.Add(Me.txtJTvmPREmailTo)
        Me.GroupBox9.Controls.Add(Me.Label31)
        Me.GroupBox9.Location = New System.Drawing.Point(19, 121)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(303, 142)
        Me.GroupBox9.TabIndex = 30
        Me.GroupBox9.TabStop = False
        '
        'txtJTvmPREmailCC
        '
        Me.txtJTvmPREmailCC.Location = New System.Drawing.Point(8, 68)
        Me.txtJTvmPREmailCC.Name = "txtJTvmPREmailCC"
        Me.txtJTvmPREmailCC.Size = New System.Drawing.Size(256, 22)
        Me.txtJTvmPREmailCC.TabIndex = 24
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(4, 48)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(182, 16)
        Me.Label33.TabIndex = 23
        Me.Label33.Text = """W2"" Email ""CC"" Addressees"
        '
        'txtJTvmPREmailFrom
        '
        Me.txtJTvmPREmailFrom.Location = New System.Drawing.Point(8, 113)
        Me.txtJTvmPREmailFrom.Name = "txtJTvmPREmailFrom"
        Me.txtJTvmPREmailFrom.Size = New System.Drawing.Size(256, 22)
        Me.txtJTvmPREmailFrom.TabIndex = 22
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(8, 93)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(190, 16)
        Me.Label32.TabIndex = 21
        Me.Label32.Text = "Payroll  Email ""From""  Address"
        '
        'txtJTvmPREmailTo
        '
        Me.txtJTvmPREmailTo.Location = New System.Drawing.Point(9, 22)
        Me.txtJTvmPREmailTo.Name = "txtJTvmPREmailTo"
        Me.txtJTvmPREmailTo.Size = New System.Drawing.Size(256, 22)
        Me.txtJTvmPREmailTo.TabIndex = 15
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(6, 2)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(174, 16)
        Me.Label31.TabIndex = 14
        Me.Label31.Text = """W2"" Email ""To"" Addressee"
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(339, 200)
        Me.TextBox31.Multiline = True
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.ReadOnly = True
        Me.TextBox31.Size = New System.Drawing.Size(442, 63)
        Me.TextBox31.TabIndex = 29
        Me.TextBox31.Text = "Email standard text refers to the wording on the email message for the two type o" &
    "f emails. This text will be combined with data from the Payroll screen to make a" &
    " complete message."
        '
        'txtJTvm1099EmailText
        '
        Me.txtJTvm1099EmailText.Location = New System.Drawing.Point(397, 291)
        Me.txtJTvm1099EmailText.Multiline = True
        Me.txtJTvm1099EmailText.Name = "txtJTvm1099EmailText"
        Me.txtJTvm1099EmailText.Size = New System.Drawing.Size(364, 103)
        Me.txtJTvm1099EmailText.TabIndex = 28
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox6.Controls.Add(Me.cboxJTvm1099Email)
        Me.GroupBox6.Controls.Add(Me.cboxJTvmW2Email)
        Me.GroupBox6.Location = New System.Drawing.Point(19, 40)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(303, 75)
        Me.GroupBox6.TabIndex = 20
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Payroll Email Options"
        '
        'cboxJTvm1099Email
        '
        Me.cboxJTvm1099Email.AutoSize = True
        Me.cboxJTvm1099Email.Location = New System.Drawing.Point(36, 45)
        Me.cboxJTvm1099Email.Name = "cboxJTvm1099Email"
        Me.cboxJTvm1099Email.Size = New System.Drawing.Size(209, 20)
        Me.cboxJTvm1099Email.TabIndex = 19
        Me.cboxJTvm1099Email.Text = "Send ""1099"" worker info  Email"
        Me.cboxJTvm1099Email.UseVisualStyleBackColor = True
        '
        'cboxJTvmW2Email
        '
        Me.cboxJTvmW2Email.AutoSize = True
        Me.cboxJTvmW2Email.Location = New System.Drawing.Point(36, 21)
        Me.cboxJTvmW2Email.Name = "cboxJTvmW2Email"
        Me.cboxJTvmW2Email.Size = New System.Drawing.Size(201, 20)
        Me.cboxJTvmW2Email.TabIndex = 18
        Me.cboxJTvmW2Email.Text = "Send ""W2"" worker info  Email"
        Me.cboxJTvmW2Email.UseVisualStyleBackColor = True
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(338, 7)
        Me.TextBox6.Multiline = True
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(442, 187)
        Me.TextBox6.TabIndex = 13
        Me.TextBox6.Text = resources.GetString("TextBox6.Text")
        '
        'GroupBox10
        '
        Me.GroupBox10.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox10.Controls.Add(Me.rtxtJTvmW2EmailText)
        Me.GroupBox10.Controls.Add(Me.Label35)
        Me.GroupBox10.Controls.Add(Me.Label34)
        Me.GroupBox10.Location = New System.Drawing.Point(19, 269)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(757, 137)
        Me.GroupBox10.TabIndex = 31
        Me.GroupBox10.TabStop = False
        '
        'rtxtJTvmW2EmailText
        '
        Me.rtxtJTvmW2EmailText.Location = New System.Drawing.Point(6, 21)
        Me.rtxtJTvmW2EmailText.Name = "rtxtJTvmW2EmailText"
        Me.rtxtJTvmW2EmailText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtxtJTvmW2EmailText.Size = New System.Drawing.Size(366, 104)
        Me.rtxtJTvmW2EmailText.TabIndex = 25
        Me.rtxtJTvmW2EmailText.Text = ""
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(402, 1)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(160, 16)
        Me.Label35.TabIndex = 27
        Me.Label35.Text = """1099"" email standard text"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(27, 1)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(152, 16)
        Me.Label34.TabIndex = 25
        Me.Label34.Text = """W2"" email standard text"
        '
        'tabBkupSec
        '
        Me.tabBkupSec.Controls.Add(Me.GroupBox3)
        Me.tabBkupSec.Controls.Add(Me.TextBox4)
        Me.tabBkupSec.Controls.Add(Me.TextBox3)
        Me.tabBkupSec.Controls.Add(Me.GroupBox4)
        Me.tabBkupSec.Location = New System.Drawing.Point(4, 25)
        Me.tabBkupSec.Name = "tabBkupSec"
        Me.tabBkupSec.Size = New System.Drawing.Size(787, 427)
        Me.tabBkupSec.TabIndex = 4
        Me.tabBkupSec.Text = "Bkup/Security"
        Me.tabBkupSec.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.cboxJTvmBkupAch)
        Me.GroupBox3.Controls.Add(Me.txtJTvmBkupAchSerNum)
        Me.GroupBox3.Controls.Add(Me.txtJTvmBkupAchDate)
        Me.GroupBox3.Controls.Add(Me.txtJTvmBkupAchPath)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.txtJTvmBkupAchGen)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.txtJTvmBkupAchFreq)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 10)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(318, 240)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Achival Backups"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(24, 149)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(66, 16)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Last Bkup"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(27, 192)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(108, 16)
        Me.Label20.TabIndex = 33
        Me.Label20.Text = "--- Serial Number"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(24, 166)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(54, 16)
        Me.Label19.TabIndex = 32
        Me.Label19.Text = " --- Date"
        '
        'cboxJTvmBkupAch
        '
        Me.cboxJTvmBkupAch.AutoSize = True
        Me.cboxJTvmBkupAch.Location = New System.Drawing.Point(123, 14)
        Me.cboxJTvmBkupAch.Name = "cboxJTvmBkupAch"
        Me.cboxJTvmBkupAch.Size = New System.Drawing.Size(188, 20)
        Me.cboxJTvmBkupAch.TabIndex = 31
        Me.cboxJTvmBkupAch.Text = "Generate Achival Backups"
        Me.cboxJTvmBkupAch.UseVisualStyleBackColor = True
        '
        'txtJTvmBkupAchSerNum
        '
        Me.txtJTvmBkupAchSerNum.Location = New System.Drawing.Point(154, 189)
        Me.txtJTvmBkupAchSerNum.Name = "txtJTvmBkupAchSerNum"
        Me.txtJTvmBkupAchSerNum.ReadOnly = True
        Me.txtJTvmBkupAchSerNum.Size = New System.Drawing.Size(66, 22)
        Me.txtJTvmBkupAchSerNum.TabIndex = 18
        '
        'txtJTvmBkupAchDate
        '
        Me.txtJTvmBkupAchDate.Location = New System.Drawing.Point(154, 161)
        Me.txtJTvmBkupAchDate.Name = "txtJTvmBkupAchDate"
        Me.txtJTvmBkupAchDate.ReadOnly = True
        Me.txtJTvmBkupAchDate.Size = New System.Drawing.Size(115, 22)
        Me.txtJTvmBkupAchDate.TabIndex = 17
        '
        'txtJTvmBkupAchPath
        '
        Me.txtJTvmBkupAchPath.Location = New System.Drawing.Point(15, 120)
        Me.txtJTvmBkupAchPath.Name = "txtJTvmBkupAchPath"
        Me.txtJTvmBkupAchPath.Size = New System.Drawing.Size(278, 22)
        Me.txtJTvmBkupAchPath.TabIndex = 26
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(23, 96)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 16)
        Me.Label25.TabIndex = 25
        Me.Label25.Text = "File Path"
        '
        'txtJTvmBkupAchGen
        '
        Me.txtJTvmBkupAchGen.Location = New System.Drawing.Point(154, 71)
        Me.txtJTvmBkupAchGen.Name = "txtJTvmBkupAchGen"
        Me.txtJTvmBkupAchGen.Size = New System.Drawing.Size(53, 22)
        Me.txtJTvmBkupAchGen.TabIndex = 24
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(12, 71)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(122, 16)
        Me.Label24.TabIndex = 23
        Me.Label24.Text = "# of copies to retain"
        '
        'txtJTvmBkupAchFreq
        '
        Me.txtJTvmBkupAchFreq.Location = New System.Drawing.Point(154, 44)
        Me.txtJTvmBkupAchFreq.Name = "txtJTvmBkupAchFreq"
        Me.txtJTvmBkupAchFreq.Size = New System.Drawing.Size(53, 22)
        Me.txtJTvmBkupAchFreq.TabIndex = 22
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(12, 44)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(71, 16)
        Me.Label23.TabIndex = 21
        Me.Label23.Text = "Frequency"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(332, 216)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(442, 168)
        Me.TextBox4.TabIndex = 12
        Me.TextBox4.Text = resources.GetString("TextBox4.Text")
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(332, 10)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(442, 200)
        Me.TextBox3.TabIndex = 11
        Me.TextBox3.Text = resources.GetString("TextBox3.Text")
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox4.Controls.Add(Me.cboxJTvmUsePW)
        Me.GroupBox4.Controls.Add(Me.txtJTvmPWConfirm)
        Me.GroupBox4.Controls.Add(Me.txtJTvmPWNew)
        Me.GroupBox4.Controls.Add(Me.txtJTvmCurPW)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 256)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(318, 150)
        Me.GroupBox4.TabIndex = 21
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Password Mgmt."
        '
        'cboxJTvmUsePW
        '
        Me.cboxJTvmUsePW.AutoSize = True
        Me.cboxJTvmUsePW.Location = New System.Drawing.Point(142, 12)
        Me.cboxJTvmUsePW.Name = "cboxJTvmUsePW"
        Me.cboxJTvmUsePW.Size = New System.Drawing.Size(124, 20)
        Me.cboxJTvmUsePW.TabIndex = 36
        Me.cboxJTvmUsePW.Text = "Use Passwords"
        Me.cboxJTvmUsePW.UseVisualStyleBackColor = True
        '
        'txtJTvmPWConfirm
        '
        Me.txtJTvmPWConfirm.Location = New System.Drawing.Point(141, 99)
        Me.txtJTvmPWConfirm.Name = "txtJTvmPWConfirm"
        Me.txtJTvmPWConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtJTvmPWConfirm.Size = New System.Drawing.Size(166, 22)
        Me.txtJTvmPWConfirm.TabIndex = 35
        '
        'txtJTvmPWNew
        '
        Me.txtJTvmPWNew.Location = New System.Drawing.Point(141, 69)
        Me.txtJTvmPWNew.Name = "txtJTvmPWNew"
        Me.txtJTvmPWNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtJTvmPWNew.Size = New System.Drawing.Size(166, 22)
        Me.txtJTvmPWNew.TabIndex = 34
        '
        'txtJTvmCurPW
        '
        Me.txtJTvmCurPW.Location = New System.Drawing.Point(141, 39)
        Me.txtJTvmCurPW.Name = "txtJTvmCurPW"
        Me.txtJTvmCurPW.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtJTvmCurPW.Size = New System.Drawing.Size(166, 22)
        Me.txtJTvmCurPW.TabIndex = 31
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(15, 71)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(90, 16)
        Me.Label30.TabIndex = 33
        Me.Label30.Text = "Enter NewPW"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(15, 102)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(114, 16)
        Me.Label29.TabIndex = 32
        Me.Label29.Text = "Re-enter New PW"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(15, 41)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(108, 16)
        Me.Label28.TabIndex = 31
        Me.Label28.Text = "Enter Current PW"
        '
        'TabNLCRecon
        '
        Me.TabNLCRecon.Controls.Add(Me.gboxJTvmNJRAMaint)
        Me.TabNLCRecon.Controls.Add(Me.GroupBox27)
        Me.TabNLCRecon.Controls.Add(Me.TextBox10)
        Me.TabNLCRecon.Controls.Add(Me.GroupBox13)
        Me.TabNLCRecon.Controls.Add(Me.TextBox44)
        Me.TabNLCRecon.Location = New System.Drawing.Point(4, 25)
        Me.TabNLCRecon.Name = "TabNLCRecon"
        Me.TabNLCRecon.Padding = New System.Windows.Forms.Padding(3)
        Me.TabNLCRecon.Size = New System.Drawing.Size(787, 427)
        Me.TabNLCRecon.TabIndex = 5
        Me.TabNLCRecon.Text = "NLC Settings"
        Me.TabNLCRecon.UseVisualStyleBackColor = True
        '
        'gboxJTvmNJRAMaint
        '
        Me.gboxJTvmNJRAMaint.BackColor = System.Drawing.Color.PowderBlue
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.Label53)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.Label52)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.txtJTvmNJRAMDesc)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.txtJTvmNJRAMAcct)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.btnJTgvNJRAMCancel)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.btnJTgvNJRAMDelete)
        Me.gboxJTvmNJRAMaint.Controls.Add(Me.btnJTgvNJRAMAddChg)
        Me.gboxJTvmNJRAMaint.Location = New System.Drawing.Point(220, 169)
        Me.gboxJTvmNJRAMaint.Name = "gboxJTvmNJRAMaint"
        Me.gboxJTvmNJRAMaint.Size = New System.Drawing.Size(392, 194)
        Me.gboxJTvmNJRAMaint.TabIndex = 21
        Me.gboxJTvmNJRAMaint.TabStop = False
        Me.gboxJTvmNJRAMaint.Text = "NJR Account Maintenance"
        Me.gboxJTvmNJRAMaint.Visible = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(32, 71)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(75, 16)
        Me.Label53.TabIndex = 6
        Me.Label53.Text = "Description"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(32, 26)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(55, 16)
        Me.Label52.TabIndex = 5
        Me.Label52.Text = "Account"
        '
        'txtJTvmNJRAMDesc
        '
        Me.txtJTvmNJRAMDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTvmNJRAMDesc.Location = New System.Drawing.Point(24, 89)
        Me.txtJTvmNJRAMDesc.Name = "txtJTvmNJRAMDesc"
        Me.txtJTvmNJRAMDesc.Size = New System.Drawing.Size(345, 22)
        Me.txtJTvmNJRAMDesc.TabIndex = 4
        '
        'txtJTvmNJRAMAcct
        '
        Me.txtJTvmNJRAMAcct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTvmNJRAMAcct.Location = New System.Drawing.Point(24, 46)
        Me.txtJTvmNJRAMAcct.Name = "txtJTvmNJRAMAcct"
        Me.txtJTvmNJRAMAcct.Size = New System.Drawing.Size(210, 22)
        Me.txtJTvmNJRAMAcct.TabIndex = 3
        '
        'btnJTgvNJRAMCancel
        '
        Me.btnJTgvNJRAMCancel.Location = New System.Drawing.Point(275, 132)
        Me.btnJTgvNJRAMCancel.Name = "btnJTgvNJRAMCancel"
        Me.btnJTgvNJRAMCancel.Size = New System.Drawing.Size(82, 42)
        Me.btnJTgvNJRAMCancel.TabIndex = 2
        Me.btnJTgvNJRAMCancel.Text = "Cancel"
        Me.btnJTgvNJRAMCancel.UseVisualStyleBackColor = True
        '
        'btnJTgvNJRAMDelete
        '
        Me.btnJTgvNJRAMDelete.Location = New System.Drawing.Point(152, 132)
        Me.btnJTgvNJRAMDelete.Name = "btnJTgvNJRAMDelete"
        Me.btnJTgvNJRAMDelete.Size = New System.Drawing.Size(82, 42)
        Me.btnJTgvNJRAMDelete.TabIndex = 1
        Me.btnJTgvNJRAMDelete.Text = "Delete"
        Me.btnJTgvNJRAMDelete.UseVisualStyleBackColor = True
        '
        'btnJTgvNJRAMAddChg
        '
        Me.btnJTgvNJRAMAddChg.Location = New System.Drawing.Point(33, 132)
        Me.btnJTgvNJRAMAddChg.Name = "btnJTgvNJRAMAddChg"
        Me.btnJTgvNJRAMAddChg.Size = New System.Drawing.Size(82, 42)
        Me.btnJTgvNJRAMAddChg.TabIndex = 0
        Me.btnJTgvNJRAMAddChg.Text = "Add"
        Me.btnJTgvNJRAMAddChg.UseVisualStyleBackColor = True
        '
        'GroupBox27
        '
        Me.GroupBox27.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox27.Controls.Add(Me.cboxJTvmNJRAccountPrint)
        Me.GroupBox27.Controls.Add(Me.cboxJTvmNJRAccountAdd)
        Me.GroupBox27.Controls.Add(Me.cboxJTvmNJRAccountChgDel)
        Me.GroupBox27.Controls.Add(Me.lvJTvmNJRAccounts)
        Me.GroupBox27.Location = New System.Drawing.Point(299, 143)
        Me.GroupBox27.Name = "GroupBox27"
        Me.GroupBox27.Size = New System.Drawing.Size(471, 278)
        Me.GroupBox27.TabIndex = 20
        Me.GroupBox27.TabStop = False
        Me.GroupBox27.Text = "Non-job-related Account Listing"
        '
        'cboxJTvmNJRAccountPrint
        '
        Me.cboxJTvmNJRAccountPrint.AutoSize = True
        Me.cboxJTvmNJRAccountPrint.Location = New System.Drawing.Point(277, 199)
        Me.cboxJTvmNJRAccountPrint.Name = "cboxJTvmNJRAccountPrint"
        Me.cboxJTvmNJRAccountPrint.Size = New System.Drawing.Size(129, 20)
        Me.cboxJTvmNJRAccountPrint.TabIndex = 21
        Me.cboxJTvmNJRAccountPrint.Text = "Print Account List"
        Me.cboxJTvmNJRAccountPrint.UseVisualStyleBackColor = True
        '
        'cboxJTvmNJRAccountAdd
        '
        Me.cboxJTvmNJRAccountAdd.AutoSize = True
        Me.cboxJTvmNJRAccountAdd.Location = New System.Drawing.Point(17, 231)
        Me.cboxJTvmNJRAccountAdd.Name = "cboxJTvmNJRAccountAdd"
        Me.cboxJTvmNJRAccountAdd.Size = New System.Drawing.Size(139, 20)
        Me.cboxJTvmNJRAccountAdd.TabIndex = 20
        Me.cboxJTvmNJRAccountAdd.Text = "Check to add item."
        Me.cboxJTvmNJRAccountAdd.UseVisualStyleBackColor = True
        '
        'cboxJTvmNJRAccountChgDel
        '
        Me.cboxJTvmNJRAccountChgDel.AutoSize = True
        Me.cboxJTvmNJRAccountChgDel.Location = New System.Drawing.Point(17, 190)
        Me.cboxJTvmNJRAccountChgDel.Name = "cboxJTvmNJRAccountChgDel"
        Me.cboxJTvmNJRAccountChgDel.Size = New System.Drawing.Size(174, 36)
        Me.cboxJTvmNJRAccountChgDel.TabIndex = 19
        Me.cboxJTvmNJRAccountChgDel.Text = "Check to delete/change " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "highlighted entry."
        Me.cboxJTvmNJRAccountChgDel.UseVisualStyleBackColor = True
        '
        'lvJTvmNJRAccounts
        '
        Me.lvJTvmNJRAccounts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvJTvmNJRAccounts.FullRowSelect = True
        Me.lvJTvmNJRAccounts.HideSelection = False
        Me.lvJTvmNJRAccounts.Location = New System.Drawing.Point(14, 20)
        Me.lvJTvmNJRAccounts.MultiSelect = False
        Me.lvJTvmNJRAccounts.Name = "lvJTvmNJRAccounts"
        Me.lvJTvmNJRAccounts.ShowItemToolTips = True
        Me.lvJTvmNJRAccounts.Size = New System.Drawing.Size(447, 164)
        Me.lvJTvmNJRAccounts.TabIndex = 18
        Me.lvJTvmNJRAccounts.UseCompatibleStateImageBehavior = False
        Me.lvJTvmNJRAccounts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Account Key"
        Me.ColumnHeader1.Width = 115
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Account Description"
        Me.ColumnHeader2.Width = 200
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(299, 14)
        Me.TextBox10.Multiline = True
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(471, 119)
        Me.TextBox10.TabIndex = 19
        Me.TextBox10.Text = resources.GetString("TextBox10.Text")
        '
        'GroupBox13
        '
        Me.GroupBox13.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox13.Controls.Add(Me.cboxReconSuggest)
        Me.GroupBox13.Controls.Add(Me.cboxReconNJR)
        Me.GroupBox13.Controls.Add(Me.cboxRecon1099)
        Me.GroupBox13.Controls.Add(Me.cboxReconSub)
        Me.GroupBox13.Controls.Add(Me.cboxReconMat)
        Me.GroupBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox13.Location = New System.Drawing.Point(12, 192)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(281, 171)
        Me.GroupBox13.TabIndex = 17
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Reconciliation Switches"
        '
        'cboxReconSuggest
        '
        Me.cboxReconSuggest.AutoSize = True
        Me.cboxReconSuggest.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxReconSuggest.Location = New System.Drawing.Point(16, 133)
        Me.cboxReconSuggest.Name = "cboxReconSuggest"
        Me.cboxReconSuggest.Size = New System.Drawing.Size(153, 22)
        Me.cboxReconSuggest.TabIndex = 4
        Me.cboxReconSuggest.Text = "Make Suggestions"
        Me.cboxReconSuggest.UseVisualStyleBackColor = True
        '
        'cboxReconNJR
        '
        Me.cboxReconNJR.AutoSize = True
        Me.cboxReconNJR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxReconNJR.Location = New System.Drawing.Point(16, 105)
        Me.cboxReconNJR.Name = "cboxReconNJR"
        Me.cboxReconNJR.Size = New System.Drawing.Size(186, 22)
        Me.cboxReconNJR.TabIndex = 3
        Me.cboxReconNJR.Text = "Non-Job Related Costs"
        Me.cboxReconNJR.UseVisualStyleBackColor = True
        '
        'cboxRecon1099
        '
        Me.cboxRecon1099.AutoSize = True
        Me.cboxRecon1099.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxRecon1099.Location = New System.Drawing.Point(16, 77)
        Me.cboxRecon1099.Name = "cboxRecon1099"
        Me.cboxRecon1099.Size = New System.Drawing.Size(161, 22)
        Me.cboxRecon1099.TabIndex = 2
        Me.cboxRecon1099.Text = "1099 Payroll Entries"
        Me.cboxRecon1099.UseVisualStyleBackColor = True
        '
        'cboxReconSub
        '
        Me.cboxReconSub.AutoSize = True
        Me.cboxReconSub.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxReconSub.Location = New System.Drawing.Point(16, 53)
        Me.cboxReconSub.Name = "cboxReconSub"
        Me.cboxReconSub.Size = New System.Drawing.Size(168, 22)
        Me.cboxReconSub.TabIndex = 1
        Me.cboxReconSub.Text = "Subcontractor Costs"
        Me.cboxReconSub.UseVisualStyleBackColor = True
        '
        'cboxReconMat
        '
        Me.cboxReconMat.AutoSize = True
        Me.cboxReconMat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxReconMat.Location = New System.Drawing.Point(16, 30)
        Me.cboxReconMat.Name = "cboxReconMat"
        Me.cboxReconMat.Size = New System.Drawing.Size(166, 22)
        Me.cboxReconMat.TabIndex = 0
        Me.cboxReconMat.Text = "Materials Cost Items"
        Me.cboxReconMat.UseVisualStyleBackColor = True
        '
        'TextBox44
        '
        Me.TextBox44.Location = New System.Drawing.Point(12, 14)
        Me.TextBox44.Multiline = True
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.ReadOnly = True
        Me.TextBox44.Size = New System.Drawing.Size(281, 172)
        Me.TextBox44.TabIndex = 16
        Me.TextBox44.Text = resources.GetString("TextBox44.Text")
        '
        'TabInvSettings
        '
        Me.TabInvSettings.Controls.Add(Me.GroupBox25)
        Me.TabInvSettings.Controls.Add(Me.GroupBox21)
        Me.TabInvSettings.Controls.Add(Me.GroupBox19)
        Me.TabInvSettings.Controls.Add(Me.GroupBox14)
        Me.TabInvSettings.Controls.Add(Me.GroupBox16)
        Me.TabInvSettings.Controls.Add(Me.GroupBox15)
        Me.TabInvSettings.Controls.Add(Me.TextBox21)
        Me.TabInvSettings.Location = New System.Drawing.Point(4, 25)
        Me.TabInvSettings.Name = "TabInvSettings"
        Me.TabInvSettings.Size = New System.Drawing.Size(787, 427)
        Me.TabInvSettings.TabIndex = 6
        Me.TabInvSettings.Text = "Inv Settings"
        Me.TabInvSettings.UseVisualStyleBackColor = True
        '
        'GroupBox25
        '
        Me.GroupBox25.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox25.Controls.Add(Me.txtInvFooter)
        Me.GroupBox25.Location = New System.Drawing.Point(21, 296)
        Me.GroupBox25.Name = "GroupBox25"
        Me.GroupBox25.Size = New System.Drawing.Size(758, 125)
        Me.GroupBox25.TabIndex = 28
        Me.GroupBox25.TabStop = False
        Me.GroupBox25.Text = "Invoice Footer Text (Optional)"
        '
        'txtInvFooter
        '
        Me.txtInvFooter.Location = New System.Drawing.Point(9, 23)
        Me.txtInvFooter.Multiline = True
        Me.txtInvFooter.Name = "txtInvFooter"
        Me.txtInvFooter.Size = New System.Drawing.Size(740, 93)
        Me.txtInvFooter.TabIndex = 19
        '
        'GroupBox21
        '
        Me.GroupBox21.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox21.Controls.Add(Me.txtJTvmInvCCEmail)
        Me.GroupBox21.Location = New System.Drawing.Point(19, 252)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Size = New System.Drawing.Size(441, 37)
        Me.GroupBox21.TabIndex = 27
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "Inv. Email Copy Address"
        '
        'txtJTvmInvCCEmail
        '
        Me.txtJTvmInvCCEmail.Location = New System.Drawing.Point(176, 8)
        Me.txtJTvmInvCCEmail.Name = "txtJTvmInvCCEmail"
        Me.txtJTvmInvCCEmail.Size = New System.Drawing.Size(254, 22)
        Me.txtJTvmInvCCEmail.TabIndex = 0
        '
        'GroupBox19
        '
        Me.GroupBox19.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox19.Controls.Add(Me.txtJTvmAltInvNumber)
        Me.GroupBox19.Controls.Add(Me.Label41)
        Me.GroupBox19.Controls.Add(Me.txtJTvmInvNumber)
        Me.GroupBox19.Controls.Add(Me.Label40)
        Me.GroupBox19.Location = New System.Drawing.Point(488, 240)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(244, 49)
        Me.GroupBox19.TabIndex = 26
        Me.GroupBox19.TabStop = False
        '
        'txtJTvmAltInvNumber
        '
        Me.txtJTvmAltInvNumber.Location = New System.Drawing.Point(130, 23)
        Me.txtJTvmAltInvNumber.Name = "txtJTvmAltInvNumber"
        Me.txtJTvmAltInvNumber.Size = New System.Drawing.Size(94, 22)
        Me.txtJTvmAltInvNumber.TabIndex = 27
        Me.txtJTvmAltInvNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(145, 3)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(58, 16)
        Me.Label41.TabIndex = 26
        Me.Label41.Text = "Alt. Inv  #"
        '
        'txtJTvmInvNumber
        '
        Me.txtJTvmInvNumber.Location = New System.Drawing.Point(17, 22)
        Me.txtJTvmInvNumber.Name = "txtJTvmInvNumber"
        Me.txtJTvmInvNumber.Size = New System.Drawing.Size(94, 22)
        Me.txtJTvmInvNumber.TabIndex = 25
        Me.txtJTvmInvNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(32, 2)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(60, 16)
        Me.Label40.TabIndex = 24
        Me.Label40.Text = "Invoice #"
        '
        'GroupBox14
        '
        Me.GroupBox14.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox14.Controls.Add(Me.cboxCstPrntSub)
        Me.GroupBox14.Location = New System.Drawing.Point(337, 173)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(442, 64)
        Me.GroupBox14.TabIndex = 17
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Printing Options - Subcontracting"
        '
        'cboxCstPrntSub
        '
        Me.cboxCstPrntSub.AutoSize = True
        Me.cboxCstPrntSub.Location = New System.Drawing.Point(52, 21)
        Me.cboxCstPrntSub.Name = "cboxCstPrntSub"
        Me.cboxCstPrntSub.Size = New System.Drawing.Size(228, 36)
        Me.cboxCstPrntSub.TabIndex = 1
        Me.cboxCstPrntSub.Text = "Include cost figures on the invoice" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "for subcontractor entries."
        Me.cboxCstPrntSub.UseVisualStyleBackColor = True
        '
        'GroupBox16
        '
        Me.GroupBox16.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox16.Controls.Add(Me.GroupBox20)
        Me.GroupBox16.Controls.Add(Me.GroupBox18)
        Me.GroupBox16.Controls.Add(Me.GroupBox2)
        Me.GroupBox16.Controls.Add(Me.Label36)
        Me.GroupBox16.Controls.Add(Me.TextBox25)
        Me.GroupBox16.Location = New System.Drawing.Point(17, 11)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(311, 235)
        Me.GroupBox16.TabIndex = 18
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Printing Options - Labor"
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.rbtnInvMatTotals)
        Me.GroupBox20.Controls.Add(Me.rbtnInvMatDetail)
        Me.GroupBox20.Location = New System.Drawing.Point(155, 80)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(146, 70)
        Me.GroupBox20.TabIndex = 24
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "Materials"
        '
        'rbtnInvMatTotals
        '
        Me.rbtnInvMatTotals.AutoSize = True
        Me.rbtnInvMatTotals.Location = New System.Drawing.Point(16, 43)
        Me.rbtnInvMatTotals.Name = "rbtnInvMatTotals"
        Me.rbtnInvMatTotals.Size = New System.Drawing.Size(96, 20)
        Me.rbtnInvMatTotals.TabIndex = 1
        Me.rbtnInvMatTotals.TabStop = True
        Me.rbtnInvMatTotals.Text = "Totals Only"
        Me.rbtnInvMatTotals.UseVisualStyleBackColor = True
        '
        'rbtnInvMatDetail
        '
        Me.rbtnInvMatDetail.AutoSize = True
        Me.rbtnInvMatDetail.Location = New System.Drawing.Point(16, 21)
        Me.rbtnInvMatDetail.Name = "rbtnInvMatDetail"
        Me.rbtnInvMatDetail.Size = New System.Drawing.Size(70, 20)
        Me.rbtnInvMatDetail.TabIndex = 0
        Me.rbtnInvMatDetail.TabStop = True
        Me.rbtnInvMatDetail.Text = "Details"
        Me.rbtnInvMatDetail.UseVisualStyleBackColor = True
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.rbtnInvSubSerDetails)
        Me.GroupBox18.Controls.Add(Me.rbtnInvSubSerTotals)
        Me.GroupBox18.Location = New System.Drawing.Point(159, 159)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(143, 66)
        Me.GroupBox18.TabIndex = 23
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "Subcontractors"
        '
        'rbtnInvSubSerDetails
        '
        Me.rbtnInvSubSerDetails.AutoSize = True
        Me.rbtnInvSubSerDetails.Location = New System.Drawing.Point(12, 21)
        Me.rbtnInvSubSerDetails.Name = "rbtnInvSubSerDetails"
        Me.rbtnInvSubSerDetails.Size = New System.Drawing.Size(70, 20)
        Me.rbtnInvSubSerDetails.TabIndex = 2
        Me.rbtnInvSubSerDetails.TabStop = True
        Me.rbtnInvSubSerDetails.Text = "Details"
        Me.rbtnInvSubSerDetails.UseVisualStyleBackColor = True
        '
        'rbtnInvSubSerTotals
        '
        Me.rbtnInvSubSerTotals.AutoSize = True
        Me.rbtnInvSubSerTotals.Location = New System.Drawing.Point(12, 45)
        Me.rbtnInvSubSerTotals.Name = "rbtnInvSubSerTotals"
        Me.rbtnInvSubSerTotals.Size = New System.Drawing.Size(96, 20)
        Me.rbtnInvSubSerTotals.TabIndex = 3
        Me.rbtnInvSubSerTotals.TabStop = True
        Me.rbtnInvSubSerTotals.Text = "Totals Only"
        Me.rbtnInvSubSerTotals.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbtnInvLaborTotals)
        Me.GroupBox2.Controls.Add(Me.rbtnInvWrkrDet)
        Me.GroupBox2.Controls.Add(Me.rbtnInvLabTskSum)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 82)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(130, 104)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Labor"
        '
        'rbtnInvLaborTotals
        '
        Me.rbtnInvLaborTotals.AutoSize = True
        Me.rbtnInvLaborTotals.Location = New System.Drawing.Point(8, 69)
        Me.rbtnInvLaborTotals.Name = "rbtnInvLaborTotals"
        Me.rbtnInvLaborTotals.Size = New System.Drawing.Size(96, 20)
        Me.rbtnInvLaborTotals.TabIndex = 2
        Me.rbtnInvLaborTotals.TabStop = True
        Me.rbtnInvLaborTotals.Text = "Totals Only"
        Me.rbtnInvLaborTotals.UseVisualStyleBackColor = True
        '
        'rbtnInvWrkrDet
        '
        Me.rbtnInvWrkrDet.AutoSize = True
        Me.rbtnInvWrkrDet.Location = New System.Drawing.Point(8, 47)
        Me.rbtnInvWrkrDet.Name = "rbtnInvWrkrDet"
        Me.rbtnInvWrkrDet.Size = New System.Drawing.Size(117, 20)
        Me.rbtnInvWrkrDet.TabIndex = 1
        Me.rbtnInvWrkrDet.TabStop = True
        Me.rbtnInvWrkrDet.Text = "Worker Details"
        Me.rbtnInvWrkrDet.UseVisualStyleBackColor = True
        '
        'rbtnInvLabTskSum
        '
        Me.rbtnInvLabTskSum.AutoSize = True
        Me.rbtnInvLabTskSum.Location = New System.Drawing.Point(7, 22)
        Me.rbtnInvLabTskSum.Name = "rbtnInvLabTskSum"
        Me.rbtnInvLabTskSum.Size = New System.Drawing.Size(119, 20)
        Me.rbtnInvLabTskSum.TabIndex = 0
        Me.rbtnInvLabTskSum.TabStop = True
        Me.rbtnInvLabTskSum.Text = "Task Summary"
        Me.rbtnInvLabTskSum.UseVisualStyleBackColor = True
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(9, 32)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(274, 32)
        Me.Label36.TabIndex = 20
        Me.Label36.Text = "Check one or more options. If no option is " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "selected only a  labor total line wi" &
    "ll be printed."
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(326, 27)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(75, 22)
        Me.TextBox25.TabIndex = 13
        '
        'GroupBox15
        '
        Me.GroupBox15.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox15.Controls.Add(Me.cboxCstPrntMat)
        Me.GroupBox15.Location = New System.Drawing.Point(337, 104)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(442, 63)
        Me.GroupBox15.TabIndex = 18
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "Printing Options - Material"
        '
        'cboxCstPrntMat
        '
        Me.cboxCstPrntMat.AutoSize = True
        Me.cboxCstPrntMat.Location = New System.Drawing.Point(52, 21)
        Me.cboxCstPrntMat.Name = "cboxCstPrntMat"
        Me.cboxCstPrntMat.Size = New System.Drawing.Size(231, 36)
        Me.cboxCstPrntMat.TabIndex = 0
        Me.cboxCstPrntMat.Text = "Include cost figures  on the invoice" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " for  material entries."
        Me.cboxCstPrntMat.UseVisualStyleBackColor = True
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(337, 11)
        Me.TextBox21.Multiline = True
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.Size = New System.Drawing.Size(442, 87)
        Me.TextBox21.TabIndex = 16
        Me.TextBox21.Text = resources.GetString("TextBox21.Text")
        '
        'TabGenSysVar
        '
        Me.TabGenSysVar.Controls.Add(Me.GboJTStrtUpSysRecap)
        Me.TabGenSysVar.Controls.Add(Me.gbocJTgvEventLog)
        Me.TabGenSysVar.Controls.Add(Me.GroupBox26)
        Me.TabGenSysVar.Controls.Add(Me.GroupBox24)
        Me.TabGenSysVar.Controls.Add(Me.Label15)
        Me.TabGenSysVar.Controls.Add(Me.GroupBox22)
        Me.TabGenSysVar.Location = New System.Drawing.Point(4, 25)
        Me.TabGenSysVar.Name = "TabGenSysVar"
        Me.TabGenSysVar.Padding = New System.Windows.Forms.Padding(3)
        Me.TabGenSysVar.Size = New System.Drawing.Size(787, 427)
        Me.TabGenSysVar.TabIndex = 7
        Me.TabGenSysVar.Text = "GenSysVar"
        Me.TabGenSysVar.UseVisualStyleBackColor = True
        '
        'gbocJTgvEventLog
        '
        Me.gbocJTgvEventLog.BackColor = System.Drawing.Color.PowderBlue
        Me.gbocJTgvEventLog.Controls.Add(Me.cboxJTgvEventLogEnabled)
        Me.gbocJTgvEventLog.Location = New System.Drawing.Point(16, 233)
        Me.gbocJTgvEventLog.Name = "gbocJTgvEventLog"
        Me.gbocJTgvEventLog.Size = New System.Drawing.Size(236, 53)
        Me.gbocJTgvEventLog.TabIndex = 4
        Me.gbocJTgvEventLog.TabStop = False
        Me.gbocJTgvEventLog.Text = "Event Log"
        '
        'cboxJTgvEventLogEnabled
        '
        Me.cboxJTgvEventLogEnabled.AutoSize = True
        Me.cboxJTgvEventLogEnabled.Location = New System.Drawing.Point(25, 21)
        Me.cboxJTgvEventLogEnabled.Name = "cboxJTgvEventLogEnabled"
        Me.cboxJTgvEventLogEnabled.Size = New System.Drawing.Size(143, 20)
        Me.cboxJTgvEventLogEnabled.TabIndex = 3
        Me.cboxJTgvEventLogEnabled.Text = "Event Log Enabled"
        Me.cboxJTgvEventLogEnabled.UseVisualStyleBackColor = True
        '
        'GroupBox26
        '
        Me.GroupBox26.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox26.Controls.Add(Me.cboxJTvmScanAUP)
        Me.GroupBox26.Controls.Add(Me.cboxJTvmScanReconcil)
        Me.GroupBox26.Controls.Add(Me.cboxJTvmScanNLC)
        Me.GroupBox26.Location = New System.Drawing.Point(16, 171)
        Me.GroupBox26.Name = "GroupBox26"
        Me.GroupBox26.Size = New System.Drawing.Size(236, 53)
        Me.GroupBox26.TabIndex = 3
        Me.GroupBox26.TabStop = False
        Me.GroupBox26.Text = "Document Scanning"
        '
        'cboxJTvmScanAUP
        '
        Me.cboxJTvmScanAUP.AutoSize = True
        Me.cboxJTvmScanAUP.Location = New System.Drawing.Point(164, 25)
        Me.cboxJTvmScanAUP.Name = "cboxJTvmScanAUP"
        Me.cboxJTvmScanAUP.Size = New System.Drawing.Size(57, 20)
        Me.cboxJTvmScanAUP.TabIndex = 2
        Me.cboxJTvmScanAUP.Text = "AUP"
        Me.cboxJTvmScanAUP.UseVisualStyleBackColor = True
        '
        'cboxJTvmScanReconcil
        '
        Me.cboxJTvmScanReconcil.AutoSize = True
        Me.cboxJTvmScanReconcil.Location = New System.Drawing.Point(76, 25)
        Me.cboxJTvmScanReconcil.Name = "cboxJTvmScanReconcil"
        Me.cboxJTvmScanReconcil.Size = New System.Drawing.Size(82, 20)
        Me.cboxJTvmScanReconcil.TabIndex = 1
        Me.cboxJTvmScanReconcil.Text = "Reconcil"
        Me.cboxJTvmScanReconcil.UseVisualStyleBackColor = True
        '
        'cboxJTvmScanNLC
        '
        Me.cboxJTvmScanNLC.AutoSize = True
        Me.cboxJTvmScanNLC.Location = New System.Drawing.Point(13, 25)
        Me.cboxJTvmScanNLC.Name = "cboxJTvmScanNLC"
        Me.cboxJTvmScanNLC.Size = New System.Drawing.Size(55, 20)
        Me.cboxJTvmScanNLC.TabIndex = 0
        Me.cboxJTvmScanNLC.Text = "NLC"
        Me.cboxJTvmScanNLC.UseVisualStyleBackColor = True
        '
        'GroupBox24
        '
        Me.GroupBox24.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox24.Controls.Add(Me.rbtnJTgvNLCSeqDate)
        Me.GroupBox24.Controls.Add(Me.rbtnJTgvNLCSeqJob)
        Me.GroupBox24.Location = New System.Drawing.Point(16, 110)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Size = New System.Drawing.Size(236, 53)
        Me.GroupBox24.TabIndex = 2
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "NLC Table Sequence"
        '
        'rbtnJTgvNLCSeqDate
        '
        Me.rbtnJTgvNLCSeqDate.AutoSize = True
        Me.rbtnJTgvNLCSeqDate.Location = New System.Drawing.Point(88, 21)
        Me.rbtnJTgvNLCSeqDate.Name = "rbtnJTgvNLCSeqDate"
        Me.rbtnJTgvNLCSeqDate.Size = New System.Drawing.Size(57, 20)
        Me.rbtnJTgvNLCSeqDate.TabIndex = 1
        Me.rbtnJTgvNLCSeqDate.TabStop = True
        Me.rbtnJTgvNLCSeqDate.Text = "Date"
        Me.rbtnJTgvNLCSeqDate.UseVisualStyleBackColor = True
        '
        'rbtnJTgvNLCSeqJob
        '
        Me.rbtnJTgvNLCSeqJob.AutoSize = True
        Me.rbtnJTgvNLCSeqJob.Location = New System.Drawing.Point(17, 22)
        Me.rbtnJTgvNLCSeqJob.Name = "rbtnJTgvNLCSeqJob"
        Me.rbtnJTgvNLCSeqJob.Size = New System.Drawing.Size(51, 20)
        Me.rbtnJTgvNLCSeqJob.TabIndex = 0
        Me.rbtnJTgvNLCSeqJob.TabStop = True
        Me.rbtnJTgvNLCSeqJob.Text = "Job"
        Me.rbtnJTgvNLCSeqJob.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(24, 13)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(228, 20)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "General System Variables"
        '
        'GroupBox22
        '
        Me.GroupBox22.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox22.Controls.Add(Me.rbtnJTgvMMJobAmt)
        Me.GroupBox22.Controls.Add(Me.rbtnJTgvMMJobAlpha)
        Me.GroupBox22.Location = New System.Drawing.Point(16, 48)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(236, 53)
        Me.GroupBox22.TabIndex = 0
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "Main Menu Vendor Sequence"
        '
        'rbtnJTgvMMJobAmt
        '
        Me.rbtnJTgvMMJobAmt.AutoSize = True
        Me.rbtnJTgvMMJobAmt.Location = New System.Drawing.Point(88, 21)
        Me.rbtnJTgvMMJobAmt.Name = "rbtnJTgvMMJobAmt"
        Me.rbtnJTgvMMJobAmt.Size = New System.Drawing.Size(116, 20)
        Me.rbtnJTgvMMJobAmt.TabIndex = 1
        Me.rbtnJTgvMMJobAmt.TabStop = True
        Me.rbtnJTgvMMJobAmt.Text = "Amounts Open"
        Me.rbtnJTgvMMJobAmt.UseVisualStyleBackColor = True
        '
        'rbtnJTgvMMJobAlpha
        '
        Me.rbtnJTgvMMJobAlpha.AutoSize = True
        Me.rbtnJTgvMMJobAlpha.Location = New System.Drawing.Point(17, 22)
        Me.rbtnJTgvMMJobAlpha.Name = "rbtnJTgvMMJobAlpha"
        Me.rbtnJTgvMMJobAlpha.Size = New System.Drawing.Size(63, 20)
        Me.rbtnJTgvMMJobAlpha.TabIndex = 0
        Me.rbtnJTgvMMJobAlpha.TabStop = True
        Me.rbtnJTgvMMJobAlpha.Text = "Alpha"
        Me.rbtnJTgvMMJobAlpha.UseVisualStyleBackColor = True
        '
        'TabSalesTax
        '
        Me.TabSalesTax.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TabSalesTax.Controls.Add(Me.GroupBox23)
        Me.TabSalesTax.Controls.Add(Me.TextBox9)
        Me.TabSalesTax.Controls.Add(Me.lvJtvmTax)
        Me.TabSalesTax.Location = New System.Drawing.Point(4, 25)
        Me.TabSalesTax.Name = "TabSalesTax"
        Me.TabSalesTax.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSalesTax.Size = New System.Drawing.Size(787, 427)
        Me.TabSalesTax.TabIndex = 8
        Me.TabSalesTax.Text = "SalesTax"
        '
        'GroupBox23
        '
        Me.GroupBox23.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox23.Controls.Add(Me.Label50)
        Me.GroupBox23.Controls.Add(Me.txtJTvmSTStRate)
        Me.GroupBox23.Controls.Add(Me.CboxJTvmSTStHasTax)
        Me.GroupBox23.Controls.Add(Me.txtJTvmSTState)
        Me.GroupBox23.Controls.Add(Me.btnJTvmSTCancel)
        Me.GroupBox23.Controls.Add(Me.btnJTvmSTUpdtAdd)
        Me.GroupBox23.Controls.Add(Me.txtJTvmStSubRate)
        Me.GroupBox23.Controls.Add(Me.txtJTvmSTMatRate)
        Me.GroupBox23.Controls.Add(Me.txtJTvmStLabRate)
        Me.GroupBox23.Controls.Add(Me.cboxJTvmSTSubHasTax)
        Me.GroupBox23.Controls.Add(Me.cboxJTvmSTMatHasTax)
        Me.GroupBox23.Controls.Add(Me.cboxJTvmSTLabHasTax)
        Me.GroupBox23.Controls.Add(Me.Label55)
        Me.GroupBox23.Controls.Add(Me.Label54)
        Me.GroupBox23.Controls.Add(Me.Label49)
        Me.GroupBox23.Controls.Add(Me.Label48)
        Me.GroupBox23.Controls.Add(Me.Label22)
        Me.GroupBox23.Location = New System.Drawing.Point(219, 189)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(554, 206)
        Me.GroupBox23.TabIndex = 2
        Me.GroupBox23.TabStop = False
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(32, 80)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(139, 16)
        Me.Label50.TabIndex = 21
        Me.Label50.Text = "---------------------------------"
        '
        'txtJTvmSTStRate
        '
        Me.txtJTvmSTStRate.Location = New System.Drawing.Point(122, 114)
        Me.txtJTvmSTStRate.Name = "txtJTvmSTStRate"
        Me.txtJTvmSTStRate.Size = New System.Drawing.Size(77, 22)
        Me.txtJTvmSTStRate.TabIndex = 20
        '
        'CboxJTvmSTStHasTax
        '
        Me.CboxJTvmSTStHasTax.AutoSize = True
        Me.CboxJTvmSTStHasTax.Location = New System.Drawing.Point(34, 116)
        Me.CboxJTvmSTStHasTax.Name = "CboxJTvmSTStHasTax"
        Me.CboxJTvmSTStHasTax.Size = New System.Drawing.Size(80, 20)
        Me.CboxJTvmSTStHasTax.TabIndex = 19
        Me.CboxJTvmSTStHasTax.Text = "Has Tax"
        Me.CboxJTvmSTStHasTax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CboxJTvmSTStHasTax.UseVisualStyleBackColor = True
        '
        'txtJTvmSTState
        '
        Me.txtJTvmSTState.Location = New System.Drawing.Point(78, 51)
        Me.txtJTvmSTState.Name = "txtJTvmSTState"
        Me.txtJTvmSTState.Size = New System.Drawing.Size(41, 22)
        Me.txtJTvmSTState.TabIndex = 18
        '
        'btnJTvmSTCancel
        '
        Me.btnJTvmSTCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTvmSTCancel.Location = New System.Drawing.Point(284, 154)
        Me.btnJTvmSTCancel.Name = "btnJTvmSTCancel"
        Me.btnJTvmSTCancel.Size = New System.Drawing.Size(128, 33)
        Me.btnJTvmSTCancel.TabIndex = 17
        Me.btnJTvmSTCancel.Text = "Delete"
        Me.btnJTvmSTCancel.UseVisualStyleBackColor = True
        '
        'btnJTvmSTUpdtAdd
        '
        Me.btnJTvmSTUpdtAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTvmSTUpdtAdd.Location = New System.Drawing.Point(139, 154)
        Me.btnJTvmSTUpdtAdd.Name = "btnJTvmSTUpdtAdd"
        Me.btnJTvmSTUpdtAdd.Size = New System.Drawing.Size(129, 33)
        Me.btnJTvmSTUpdtAdd.TabIndex = 15
        Me.btnJTvmSTUpdtAdd.Text = "Update/Add"
        Me.btnJTvmSTUpdtAdd.UseVisualStyleBackColor = True
        '
        'txtJTvmStSubRate
        '
        Me.txtJTvmStSubRate.Location = New System.Drawing.Point(433, 86)
        Me.txtJTvmStSubRate.Name = "txtJTvmStSubRate"
        Me.txtJTvmStSubRate.Size = New System.Drawing.Size(77, 22)
        Me.txtJTvmStSubRate.TabIndex = 14
        '
        'txtJTvmSTMatRate
        '
        Me.txtJTvmSTMatRate.Location = New System.Drawing.Point(433, 111)
        Me.txtJTvmSTMatRate.Name = "txtJTvmSTMatRate"
        Me.txtJTvmSTMatRate.Size = New System.Drawing.Size(77, 22)
        Me.txtJTvmSTMatRate.TabIndex = 13
        '
        'txtJTvmStLabRate
        '
        Me.txtJTvmStLabRate.Location = New System.Drawing.Point(433, 60)
        Me.txtJTvmStLabRate.Name = "txtJTvmStLabRate"
        Me.txtJTvmStLabRate.Size = New System.Drawing.Size(77, 22)
        Me.txtJTvmStLabRate.TabIndex = 12
        '
        'cboxJTvmSTSubHasTax
        '
        Me.cboxJTvmSTSubHasTax.AutoSize = True
        Me.cboxJTvmSTSubHasTax.Location = New System.Drawing.Point(325, 87)
        Me.cboxJTvmSTSubHasTax.Name = "cboxJTvmSTSubHasTax"
        Me.cboxJTvmSTSubHasTax.Size = New System.Drawing.Size(102, 20)
        Me.cboxJTvmSTSubHasTax.TabIndex = 11
        Me.cboxJTvmSTSubHasTax.Text = "SubContract"
        Me.cboxJTvmSTSubHasTax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cboxJTvmSTSubHasTax.UseVisualStyleBackColor = True
        '
        'cboxJTvmSTMatHasTax
        '
        Me.cboxJTvmSTMatHasTax.AutoSize = True
        Me.cboxJTvmSTMatHasTax.Location = New System.Drawing.Point(325, 113)
        Me.cboxJTvmSTMatHasTax.Name = "cboxJTvmSTMatHasTax"
        Me.cboxJTvmSTMatHasTax.Size = New System.Drawing.Size(84, 20)
        Me.cboxJTvmSTMatHasTax.TabIndex = 10
        Me.cboxJTvmSTMatHasTax.Text = "Materials"
        Me.cboxJTvmSTMatHasTax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cboxJTvmSTMatHasTax.UseVisualStyleBackColor = True
        '
        'cboxJTvmSTLabHasTax
        '
        Me.cboxJTvmSTLabHasTax.AutoSize = True
        Me.cboxJTvmSTLabHasTax.Location = New System.Drawing.Point(325, 63)
        Me.cboxJTvmSTLabHasTax.Name = "cboxJTvmSTLabHasTax"
        Me.cboxJTvmSTLabHasTax.Size = New System.Drawing.Size(64, 20)
        Me.cboxJTvmSTLabHasTax.TabIndex = 9
        Me.cboxJTvmSTLabHasTax.Text = "Labor"
        Me.cboxJTvmSTLabHasTax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cboxJTvmSTLabHasTax.UseVisualStyleBackColor = True
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(143, 96)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(36, 16)
        Me.Label55.TabIndex = 7
        Me.Label55.Text = "Rate"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(344, 40)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(58, 16)
        Me.Label54.TabIndex = 6
        Me.Label54.Text = "Has Tax"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(454, 40)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(36, 16)
        Me.Label49.TabIndex = 2
        Me.Label49.Text = "Rate"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(31, 54)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(38, 16)
        Me.Label48.TabIndex = 1
        Me.Label48.Text = "State"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(9, 11)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(237, 18)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "State Sales Tax Entry Maintenance"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(12, 15)
        Me.TextBox9.Multiline = True
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(194, 380)
        Me.TextBox9.TabIndex = 1
        Me.TextBox9.Text = resources.GetString("TextBox9.Text")
        '
        'lvJtvmTax
        '
        Me.lvJtvmTax.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColState, Me.ColTaxSwitch, Me.ColTaxRate, Me.ColLabTxSw, Me.ColLabRate, Me.ColSubTxSw, Me.ColSubRate, Me.ColMatTxSw, Me.ColMatRate})
        Me.lvJtvmTax.FullRowSelect = True
        Me.lvJtvmTax.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvJtvmTax.HideSelection = False
        Me.lvJtvmTax.Location = New System.Drawing.Point(217, 15)
        Me.lvJtvmTax.MultiSelect = False
        Me.lvJtvmTax.Name = "lvJtvmTax"
        Me.lvJtvmTax.Size = New System.Drawing.Size(557, 163)
        Me.lvJtvmTax.TabIndex = 0
        Me.lvJtvmTax.UseCompatibleStateImageBehavior = False
        Me.lvJtvmTax.View = System.Windows.Forms.View.Details
        '
        'ColState
        '
        Me.ColState.Text = "State"
        Me.ColState.Width = 40
        '
        'ColTaxSwitch
        '
        Me.ColTaxSwitch.Text = "Has Tax"
        Me.ColTaxSwitch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColTaxSwitch.Width = 55
        '
        'ColTaxRate
        '
        Me.ColTaxRate.Text = "Rate"
        Me.ColTaxRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColTaxRate.Width = 50
        '
        'ColLabTxSw
        '
        Me.ColLabTxSw.Text = "Labor"
        Me.ColLabTxSw.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColLabTxSw.Width = 40
        '
        'ColLabRate
        '
        Me.ColLabRate.Text = "Rate"
        Me.ColLabRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColLabRate.Width = 50
        '
        'ColSubTxSw
        '
        Me.ColSubTxSw.Text = "Sub"
        Me.ColSubTxSw.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColSubTxSw.Width = 38
        '
        'ColSubRate
        '
        Me.ColSubRate.Text = "Rate"
        Me.ColSubRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColSubRate.Width = 50
        '
        'ColMatTxSw
        '
        Me.ColMatTxSw.Text = "Mat"
        Me.ColMatTxSw.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColMatTxSw.Width = 38
        '
        'ColMatRate
        '
        Me.ColMatRate.Text = "Rate"
        Me.ColMatRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColMatRate.Width = 50
        '
        'TabWebData
        '
        Me.TabWebData.Controls.Add(Me.GroupBox28)
        Me.TabWebData.Controls.Add(Me.GroupBox30)
        Me.TabWebData.Controls.Add(Me.TextBox22)
        Me.TabWebData.Location = New System.Drawing.Point(4, 25)
        Me.TabWebData.Name = "TabWebData"
        Me.TabWebData.Padding = New System.Windows.Forms.Padding(3)
        Me.TabWebData.Size = New System.Drawing.Size(787, 427)
        Me.TabWebData.TabIndex = 9
        Me.TabWebData.Text = "Web Data"
        Me.TabWebData.UseVisualStyleBackColor = True
        '
        'GroupBox28
        '
        Me.GroupBox28.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox28.Controls.Add(Me.rbtnJTvmEmailExtOff)
        Me.GroupBox28.Controls.Add(Me.rbtnJTvmEmailExtOn)
        Me.GroupBox28.Location = New System.Drawing.Point(24, 107)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Size = New System.Drawing.Size(319, 73)
        Me.GroupBox28.TabIndex = 21
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "Email Message Extract"
        '
        'rbtnJTvmEmailExtOff
        '
        Me.rbtnJTvmEmailExtOff.AutoSize = True
        Me.rbtnJTvmEmailExtOff.Location = New System.Drawing.Point(155, 32)
        Me.rbtnJTvmEmailExtOff.Name = "rbtnJTvmEmailExtOff"
        Me.rbtnJTvmEmailExtOff.Size = New System.Drawing.Size(54, 20)
        Me.rbtnJTvmEmailExtOff.TabIndex = 1
        Me.rbtnJTvmEmailExtOff.TabStop = True
        Me.rbtnJTvmEmailExtOff.Text = "OFF"
        Me.rbtnJTvmEmailExtOff.UseVisualStyleBackColor = True
        '
        'rbtnJTvmEmailExtOn
        '
        Me.rbtnJTvmEmailExtOn.AutoSize = True
        Me.rbtnJTvmEmailExtOn.Location = New System.Drawing.Point(81, 32)
        Me.rbtnJTvmEmailExtOn.Name = "rbtnJTvmEmailExtOn"
        Me.rbtnJTvmEmailExtOn.Size = New System.Drawing.Size(48, 20)
        Me.rbtnJTvmEmailExtOn.TabIndex = 0
        Me.rbtnJTvmEmailExtOn.TabStop = True
        Me.rbtnJTvmEmailExtOn.Text = "ON"
        Me.rbtnJTvmEmailExtOn.UseVisualStyleBackColor = True
        '
        'GroupBox30
        '
        Me.GroupBox30.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox30.Controls.Add(Me.rbtnJTvmImageExtOff)
        Me.GroupBox30.Controls.Add(Me.rbtnJTvmImageExtOn)
        Me.GroupBox30.Location = New System.Drawing.Point(25, 24)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Size = New System.Drawing.Size(319, 73)
        Me.GroupBox30.TabIndex = 20
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "Image File Extract"
        '
        'rbtnJTvmImageExtOff
        '
        Me.rbtnJTvmImageExtOff.AutoSize = True
        Me.rbtnJTvmImageExtOff.Location = New System.Drawing.Point(155, 32)
        Me.rbtnJTvmImageExtOff.Name = "rbtnJTvmImageExtOff"
        Me.rbtnJTvmImageExtOff.Size = New System.Drawing.Size(54, 20)
        Me.rbtnJTvmImageExtOff.TabIndex = 1
        Me.rbtnJTvmImageExtOff.TabStop = True
        Me.rbtnJTvmImageExtOff.Text = "OFF"
        Me.rbtnJTvmImageExtOff.UseVisualStyleBackColor = True
        '
        'rbtnJTvmImageExtOn
        '
        Me.rbtnJTvmImageExtOn.AutoSize = True
        Me.rbtnJTvmImageExtOn.Location = New System.Drawing.Point(81, 32)
        Me.rbtnJTvmImageExtOn.Name = "rbtnJTvmImageExtOn"
        Me.rbtnJTvmImageExtOn.Size = New System.Drawing.Size(48, 20)
        Me.rbtnJTvmImageExtOn.TabIndex = 0
        Me.rbtnJTvmImageExtOn.TabStop = True
        Me.rbtnJTvmImageExtOn.Text = "ON"
        Me.rbtnJTvmImageExtOn.UseVisualStyleBackColor = True
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(397, 22)
        Me.TextBox22.Multiline = True
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.ReadOnly = True
        Me.TextBox22.Size = New System.Drawing.Size(364, 345)
        Me.TextBox22.TabIndex = 18
        Me.TextBox22.Text = resources.GetString("TextBox22.Text")
        '
        'btnDone
        '
        Me.btnDone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDone.Location = New System.Drawing.Point(393, 500)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(139, 34)
        Me.btnDone.TabIndex = 1
        Me.btnDone.Text = "Done"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(218, 22)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Global System Settings"
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'cboxJTgvDashBoard
        '
        Me.cboxJTgvDashBoard.AutoSize = True
        Me.cboxJTgvDashBoard.Location = New System.Drawing.Point(25, 21)
        Me.cboxJTgvDashBoard.Name = "cboxJTgvDashBoard"
        Me.cboxJTgvDashBoard.Size = New System.Drawing.Size(155, 20)
        Me.cboxJTgvDashBoard.TabIndex = 3
        Me.cboxJTgvDashBoard.Text = "Dash Board Enabled"
        Me.cboxJTgvDashBoard.UseVisualStyleBackColor = True
        '
        'GboJTStrtUpSysRecap
        '
        Me.GboJTStrtUpSysRecap.BackColor = System.Drawing.Color.PowderBlue
        Me.GboJTStrtUpSysRecap.Controls.Add(Me.cboxJTgvDashBoard)
        Me.GboJTStrtUpSysRecap.Location = New System.Drawing.Point(16, 301)
        Me.GboJTStrtUpSysRecap.Name = "GboJTStrtUpSysRecap"
        Me.GboJTStrtUpSysRecap.Size = New System.Drawing.Size(236, 44)
        Me.GboJTStrtUpSysRecap.TabIndex = 5
        Me.GboJTStrtUpSysRecap.TabStop = False
        Me.GboJTStrtUpSysRecap.Text = "System Start Up Dash Board"
        '
        'JTVarMaint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(912, 547)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "JTVarMaint"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - System Settings"
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.tabCompInfo.ResumeLayout(False)
        Me.tabCompInfo.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.tabMargin.ResumeLayout(False)
        Me.tabMargin.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.tabDataMgmt.ResumeLayout(False)
        Me.tabDataMgmt.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.tabPayroll.ResumeLayout(False)
        Me.tabPayroll.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.tabBkupSec.ResumeLayout(False)
        Me.tabBkupSec.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TabNLCRecon.ResumeLayout(False)
        Me.TabNLCRecon.PerformLayout()
        Me.gboxJTvmNJRAMaint.ResumeLayout(False)
        Me.gboxJTvmNJRAMaint.PerformLayout()
        Me.GroupBox27.ResumeLayout(False)
        Me.GroupBox27.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.TabInvSettings.ResumeLayout(False)
        Me.TabInvSettings.PerformLayout()
        Me.GroupBox25.ResumeLayout(False)
        Me.GroupBox25.PerformLayout()
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox20.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.TabGenSysVar.ResumeLayout(False)
        Me.TabGenSysVar.PerformLayout()
        Me.gbocJTgvEventLog.ResumeLayout(False)
        Me.gbocJTgvEventLog.PerformLayout()
        Me.GroupBox26.ResumeLayout(False)
        Me.GroupBox26.PerformLayout()
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.TabSalesTax.ResumeLayout(False)
        Me.TabSalesTax.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        Me.TabWebData.ResumeLayout(False)
        Me.TabWebData.PerformLayout()
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox28.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox30.PerformLayout()
        Me.GboJTStrtUpSysRecap.ResumeLayout(False)
        Me.GboJTStrtUpSysRecap.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tabCompInfo As TabPage
    Friend WithEvents tabMargin As TabPage
    Friend WithEvents tabDataMgmt As TabPage
    Friend WithEvents tabPayroll As TabPage
    Friend WithEvents tabBkupSec As TabPage
    Friend WithEvents btnDone As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents txtJTvmBkupAchSerNum As TextBox
    Friend WithEvents txtJTvmBkupAchDate As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtJTvmBkupAchPath As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents txtJTvmBkupAchGen As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents txtJTvmBkupAchFreq As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents txtJTvmPWConfirm As TextBox
    Friend WithEvents txtJTvmPWNew As TextBox
    Friend WithEvents txtJTvmCurPW As TextBox
    Friend WithEvents txtJTvmCompPhone As TextBox
    Friend WithEvents txtJTvmCompEmail As TextBox
    Friend WithEvents txtJTvmCompAddL1 As TextBox
    Friend WithEvents txtJTVMCompAddL3 As TextBox
    Friend WithEvents txtJTvmCompName As TextBox
    Friend WithEvents txtJTvmCompContact As TextBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents cboxJTvm1099Email As CheckBox
    Friend WithEvents cboxJTvmW2Email As CheckBox
    Friend WithEvents txtJTvmPREmailTo As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents TextBox31 As TextBox
    Friend WithEvents txtJTvm1099EmailText As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents txtJTvmPREmailCC As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents txtJTvmPREmailFrom As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents txtJTvmSubMargin As TextBox
    Friend WithEvents txtJTvmMatMargin As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtJTvmLabMinLife As TextBox
    Friend WithEvents txtJTvmNLCMinLife As TextBox
    Friend WithEvents txtJTvmMinDaysAfterInv As TextBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents txtJTvmDPJMstr As TextBox
    Friend WithEvents txtJTvmDPJSub As TextBox
    Friend WithEvents txtJTvmDPJNorm As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboxJTvmBkupAch As CheckBox
    Friend WithEvents cboxJTvmUsePW As CheckBox
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents GroupBox12 As GroupBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents txtJTvmCompFax As TextBox
    Friend WithEvents txtJTvmCompAddL2 As TextBox
    Friend WithEvents TabNLCRecon As TabPage
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents cboxRecon1099 As CheckBox
    Friend WithEvents cboxReconSub As CheckBox
    Friend WithEvents cboxReconMat As CheckBox
    Friend WithEvents TextBox44 As TextBox
    Friend WithEvents TabInvSettings As TabPage
    Friend WithEvents GroupBox14 As GroupBox
    Friend WithEvents cboxCstPrntSub As CheckBox
    Friend WithEvents txtInvFooter As TextBox
    Friend WithEvents GroupBox16 As GroupBox
    Friend WithEvents Label36 As Label
    Friend WithEvents TextBox25 As TextBox
    Friend WithEvents GroupBox15 As GroupBox
    Friend WithEvents cboxCstPrntMat As CheckBox
    Friend WithEvents TextBox21 As TextBox
    Friend WithEvents GroupBox17 As GroupBox
    Friend WithEvents txtJTvmPRTaxLoad As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents txtJTvmCompanyLogo As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents GroupBox19 As GroupBox
    Friend WithEvents txtJTvmInvNumber As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents txtJTvmAltInvNumber As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents txtJTvmINHOUSEMargin As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtJTvmLstTKPurge As TextBox
    Friend WithEvents txtJTvmLstNLCPurge As TextBox
    Friend WithEvents txtJTvmPurgeTiming As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents txtJTvmGoogleValidation As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox20 As GroupBox
    Friend WithEvents rbtnInvMatTotals As RadioButton
    Friend WithEvents rbtnInvMatDetail As RadioButton
    Friend WithEvents GroupBox18 As GroupBox
    Friend WithEvents rbtnInvSubSerDetails As RadioButton
    Friend WithEvents rbtnInvSubSerTotals As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbtnInvLaborTotals As RadioButton
    Friend WithEvents rbtnInvWrkrDet As RadioButton
    Friend WithEvents rbtnInvLabTskSum As RadioButton
    Friend WithEvents GroupBox21 As GroupBox
    Friend WithEvents txtJTvmInvCCEmail As TextBox
    Friend WithEvents TabGenSysVar As TabPage
    Friend WithEvents Label15 As Label
    Friend WithEvents GroupBox22 As GroupBox
    Friend WithEvents rbtnJTgvMMJobAmt As RadioButton
    Friend WithEvents rbtnJTgvMMJobAlpha As RadioButton
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents txtJTvmAltCompanyLogo As TextBox
    Friend WithEvents Label46 As Label
    Friend WithEvents txtJTvmAltCompAddL2 As TextBox
    Friend WithEvents txtJTvmAltCompAddL1 As TextBox
    Friend WithEvents txtJTvmAltCompAddL3 As TextBox
    Friend WithEvents txtJTvmAltCompName As TextBox
    Friend WithEvents Label47 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents TabSalesTax As TabPage
    Friend WithEvents GroupBox23 As GroupBox
    Friend WithEvents Label50 As Label
    Friend WithEvents txtJTvmSTStRate As TextBox
    Friend WithEvents CboxJTvmSTStHasTax As CheckBox
    Friend WithEvents txtJTvmSTState As TextBox
    Friend WithEvents btnJTvmSTCancel As Button
    Friend WithEvents btnJTvmSTUpdtAdd As Button
    Friend WithEvents txtJTvmStSubRate As TextBox
    Friend WithEvents txtJTvmSTMatRate As TextBox
    Friend WithEvents txtJTvmStLabRate As TextBox
    Friend WithEvents cboxJTvmSTSubHasTax As CheckBox
    Friend WithEvents cboxJTvmSTMatHasTax As CheckBox
    Friend WithEvents cboxJTvmSTLabHasTax As CheckBox
    Friend WithEvents Label55 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents lvJtvmTax As ListView
    Friend WithEvents ColState As ColumnHeader
    Friend WithEvents ColTaxSwitch As ColumnHeader
    Friend WithEvents ColTaxRate As ColumnHeader
    Friend WithEvents ColLabTxSw As ColumnHeader
    Friend WithEvents ColLabRate As ColumnHeader
    Friend WithEvents ColSubTxSw As ColumnHeader
    Friend WithEvents ColSubRate As ColumnHeader
    Friend WithEvents ColMatTxSw As ColumnHeader
    Friend WithEvents ColMatRate As ColumnHeader
    Friend WithEvents GroupBox24 As GroupBox
    Friend WithEvents rbtnJTgvNLCSeqDate As RadioButton
    Friend WithEvents rbtnJTgvNLCSeqJob As RadioButton
    Friend WithEvents GroupBox25 As GroupBox
    Friend WithEvents GroupBox26 As GroupBox
    Friend WithEvents cboxJTvmScanAUP As CheckBox
    Friend WithEvents cboxJTvmScanReconcil As CheckBox
    Friend WithEvents cboxJTvmScanNLC As CheckBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents lvJTvmNJRAccounts As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents GroupBox27 As GroupBox
    Friend WithEvents cboxJTvmNJRAccountChgDel As CheckBox
    Friend WithEvents cboxReconNJR As CheckBox
    Friend WithEvents cboxJTvmNJRAccountAdd As CheckBox
    Friend WithEvents gboxJTvmNJRAMaint As GroupBox
    Friend WithEvents Label53 As Label
    Friend WithEvents Label52 As Label
    Friend WithEvents txtJTvmNJRAMDesc As TextBox
    Friend WithEvents txtJTvmNJRAMAcct As TextBox
    Friend WithEvents btnJTgvNJRAMCancel As Button
    Friend WithEvents btnJTgvNJRAMDelete As Button
    Friend WithEvents btnJTgvNJRAMAddChg As Button
    Friend WithEvents cboxJTvmNJRAccountPrint As CheckBox
    Friend WithEvents cboxReconSuggest As CheckBox
    Friend WithEvents gbocJTgvEventLog As GroupBox
    Friend WithEvents cboxJTgvEventLogEnabled As CheckBox
    Friend WithEvents rtxtJTvmW2EmailText As RichTextBox
    Friend WithEvents TabWebData As TabPage
    Friend WithEvents GroupBox30 As GroupBox
    Friend WithEvents TextBox22 As TextBox
    Friend WithEvents GroupBox28 As GroupBox
    Friend WithEvents rbtnJTvmEmailExtOff As RadioButton
    Friend WithEvents rbtnJTvmEmailExtOn As RadioButton
    Friend WithEvents rbtnJTvmImageExtOff As RadioButton
    Friend WithEvents rbtnJTvmImageExtOn As RadioButton
    Friend WithEvents GboJTStrtUpSysRecap As GroupBox
    Friend WithEvents cboxJTgvDashBoard As CheckBox
End Class
