﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTErrHandle
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.JTEHcalledFrom = New System.Windows.Forms.TextBox()
        Me.JTEHexit = New System.Windows.Forms.Button()
        Me.JTEHtext = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(280, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Detailed Error Explanations." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(369, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Panel called from:"
        '
        'JTEHcalledFrom
        '
        Me.JTEHcalledFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JTEHcalledFrom.Location = New System.Drawing.Point(519, 16)
        Me.JTEHcalledFrom.Name = "JTEHcalledFrom"
        Me.JTEHcalledFrom.Size = New System.Drawing.Size(269, 26)
        Me.JTEHcalledFrom.TabIndex = 2
        '
        'JTEHexit
        '
        Me.JTEHexit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JTEHexit.Location = New System.Drawing.Point(283, 499)
        Me.JTEHexit.Name = "JTEHexit"
        Me.JTEHexit.Size = New System.Drawing.Size(220, 36)
        Me.JTEHexit.TabIndex = 3
        Me.JTEHexit.Text = "Close Panel"
        Me.JTEHexit.UseVisualStyleBackColor = True
        '
        'JTEHtext
        '
        Me.JTEHtext.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.JTEHtext.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JTEHtext.Location = New System.Drawing.Point(23, 56)
        Me.JTEHtext.Multiline = True
        Me.JTEHtext.Name = "JTEHtext"
        Me.JTEHtext.ReadOnly = True
        Me.JTEHtext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.JTEHtext.Size = New System.Drawing.Size(760, 403)
        Me.JTEHtext.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 472)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(423, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "When this panel is closed, the program returns to where it left off. "
        '
        'JTErrHandle
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(800, 548)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.JTEHtext)
        Me.Controls.Add(Me.JTEHexit)
        Me.Controls.Add(Me.JTEHcalledFrom)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "JTErrHandle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "J.I.M. General Error Explanations"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents JTEHcalledFrom As TextBox
    Friend WithEvents JTEHexit As Button
    Friend WithEvents JTEHtext As TextBox
    Friend WithEvents Label3 As Label
End Class
