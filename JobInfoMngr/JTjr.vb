﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
''' <summary>Class prepares job report and displays it in PDF format. Created using iTextSharp.
''' PDF file is deleted after it is printed. This report can be executed on any job at any time.</summary>
Public Class JTjr
    '
    Public Structure JTjrJobsStru
        Dim Job As String
        Dim Subjob As String
        Dim JobDesc As String
        Dim CostMeth As String
        Dim DeactDate As String
        Dim AUPLabor As String
        Dim AUPMat As String
        Dim AUPSub As String
        Dim HasUnbilled As Boolean
        Dim UnBHours As Double
        Dim UnBLabor As Double
        Dim UnBMat As Double
        Dim UnBSub As Double
        Dim UnBIH As Double
        Dim UnbAdj As Double
        Dim UnBEstMrgn As Double
    End Structure
    '
    Dim slJTjrJobs As New SortedList
    Dim slJTjrInv As New SortedList
    '
    ''' <summary>This sub is the entry into the Job Report logic. It is called from the 
    '''          ribbon menu. It prepares the data on the selection panel(JTjr).</summary>
    Public Sub JTjrInit()
        '
        Dim tmpJobSub = 0
        Dim tmpJobName As String = ""
        Dim tmpSubName As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpCstMeth As String = ""

        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If comboJTjrJob.Items.Contains(tmpJobName) = False Then

                '       If tmpSubName = "" Then
                comboJTjrJob.Items.Add(tmpJobName)
            End If
            tmpJobSub += 1
        Loop
        ' Then Set ComboBox AutoComplete properties
        comboJTjrJob.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        comboJTjrJob.AutoCompleteSource = AutoCompleteSource.ListItems

        cboxConsolidate.Visible = True
        Me.MdiParent = JTstart
        Me.Show()
    End Sub

    ''' <summary>Button 2 on JTjr is the cancel button. Hit it ... reporting is cancelled. 
    '''          This sub performs the necessary logic to do the cancel.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        comboJTjrJob.Items.Clear()
        comboJTjrSubjob.Items.Clear()
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub

    Private Sub cboxJTjrJob_SelectedIndexChanged(sender As Object, e As EventArgs) Handles comboJTjrJob.LostFocus
        Dim JTjimfPOS As String = ""
        Dim tmpJobName As String = ""
        Dim tmpSubName As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpCstMeth As String = ""
        If cboxConsolidate.Checked = False Then


            If JTjim.JTjimFindFunc(JTjimfPOS, comboJTjrJob.Text,
                                   tmpSubName, tmpJobType,
                                   tmpDeactDate) = "DATA" Then
                comboJTjrSubjob.Items.Add("BlankJobName")
            End If
            '
            lblSubjob.Visible = True
            comboJTjrSubjob.Visible = True
            cboxConsolidate.Visible = True
            Dim tmpSubNameSave As String = Nothing    ' Save the last sub name to be processed.
            Dim tmpJobSub = 0
            '
            Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
                If tmpSubName <> "" And
                            tmpJobName = comboJTjrJob.Text Then
                    comboJTjrSubjob.Items.Add(tmpSubName)
                    tmpSubNameSave = tmpSubName
                End If
                tmpJobSub += 1
            Loop
            comboJTjrSubjob.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            comboJTjrSubjob.AutoCompleteSource = AutoCompleteSource.ListItems
            '
            lblSubjob.Visible = True
            comboJTjrSubjob.Visible = True
            If comboJTjrSubjob.Items.Count > 1 Then

                comboJTjrSubjob.Text = ""
                comboJTjrSubjob.Select()
            Else
                comboJTjrSubjob.Text = tmpSubNameSave
                cboxConsolidate.Visible = False
                btnGenerate.Select()
            End If
            Exit Sub
        Else
            lblSubjob.Visible = False
            comboJTjrSubjob.Visible = False
            comboJTjrSubjob.Text = ""
            btnGenerate.Select()
            Exit Sub
        End If
        '
    End Sub

    Private Sub cboxConsolidate_CheckedChanged(sender As Object, e As EventArgs) Handles cboxConsolidate.CheckedChanged
        If cboxConsolidate.Checked = True Then
            comboJTjrSubjob.Text = ""
            lblSubjob.Visible = False
            comboJTjrSubjob.Visible = False
        Else
            lblSubjob.Visible = True
            comboJTjrSubjob.Visible = True
        End If
    End Sub

    Private Sub comboJTjrSubjob_LostFocus(sender As Object, e As EventArgs) Handles comboJTjrSubjob.LostFocus
        If comboJTjrSubjob.Text = "BlankJobName" Then
            comboJTjrSubjob.Text = ""
        End If
        btnGenerate.Select()
    End Sub

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click

        JTjrBuildReport("SELECTED", comboJTjrJob.Text, comboJTjrSubjob.Text, cboxShowMrgn.Checked,
                        cboxConsolidate.Checked, rbtnLife.Checked)
    End Sub
    Public Sub JTjrBuildReport(tmpRequester As String, tmpJob As String, tmpSubjob As String,
                               tmpMargin As Boolean, tmpConsolidate As Boolean,
                               tmpJobLife As Boolean)
        Try
            Using fs As System.IO.FileStream = New FileStream(JTVarMaint.JTvmFilePath & "JTjrJobReport" & ".PDF", FileMode.Create)

                Dim mydoc As Document = New Document(iTextSharp.text.PageSize.LETTER.Rotate(), 30, 10, 42, 35)
                '
                Dim writer As PdfWriter = PdfWriter.GetInstance(mydoc, fs)
                Dim ev As New JTiritsEvents
                writer.PageEvent = ev
                '
                mydoc.AddTitle("JIM Job Report")
                mydoc.Open()
                '
                JTjrHeadingInfo(mydoc, tmpJob, tmpSubjob)
                '
                Dim tmpKey As String = tmpJob & tmpSubjob
                Dim tmpItem As JTjrJobsStru
                tmpItem.Job = tmpJob
                If cboxConsolidate.Checked = False Then
                    tmpItem.Subjob = tmpSubjob
                Else
                    ' ----------------------------------------------------------------
                    ' Code to plug subjob to allow for 'old' master jobs that have not
                    ' been convert to have a job name.
                    ' ---  tmpItem.Subjob = comboJTjrSubjob.Items(0)  ---- old code
                    ' ----------------------------------------------------------------
                    tmpItem.Subjob = ""

                End If

                '
                Dim JimInfo As JTjim.JTjimJobInfo = Nothing
                '
                ' retrieve JIM info for this job.
                '
                Dim tmpJobSub = 0
                Dim tmpJRJobName As String = tmpJob
                Dim tmpJRSubName As String = tmpSubjob
                Dim tmpJRDeactDate As String = ""
                Dim tmpJRJobType As String = ""
                Dim tmpJRCstMeth As String = ""

                If tmpConsolidate = True Then
                    '
                    Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJRJobName, tmpJRSubName, tmpJRDeactDate,
                                                 tmpJRJobType, tmpJRCstMeth) = "DATA"
                        If tmpJRJobName = comboJTjrJob.Text Then
                            Exit Do
                        End If
                        tmpJobSub += 1
                    Loop
                End If
                '
                If JTjim.JTjimJTjrReadRec(JimInfo, tmpJRJobName, tmpJRSubName) <> "DATA" Then
                    MsgBox("Error Retrieving JIM data - JTjrHeadingInfo" & vbCrLf & "Job >" & tmpJRJobName &
                       "<" & vbCrLf & "Subjob >" & tmpJRSubName & "< - JTjr~160")
                    Me.Close()
                    If JTye.JTyeInProgress = False Then
                        JTMainMenu.JTMMinit()
                    End If
                End If
                '          End If
                tmpItem.DeactDate = JimInfo.JTjimJIDeactDate
                tmpItem.CostMeth = JimInfo.JTjimJIJobPricMeth
                tmpItem.JobDesc = JimInfo.JTjimJIDescName
                tmpItem.AUPLabor = JimInfo.JTjimJIAUPLabor
                tmpItem.AUPMat = JimInfo.JTjimJIAUPMat
                tmpItem.AUPSub = JimInfo.JTjimJIAUPServices
                tmpItem.HasUnbilled = False
                '
                If tmpConsolidate = True Then
                    tmpKey = tmpJRJobName & ""
                Else
                    tmpKey = tmpJRJobName & tmpJRSubName
                End If
                '
                slJTjrJobs.Add(tmpKey, tmpItem)
                If cboxConsolidate.Checked = True Then
                        ' Get the subjob ID's from JTJim
                        tmpJobSub = 0
                        tmpJRJobName = ""
                        tmpJRSubName = ""
                        tmpJRDeactDate = ""
                        tmpJRJobType = ""
                        tmpJRCstMeth = ""
                    '
                    Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJRJobName, tmpJRSubName, tmpJRDeactDate,
                                                 tmpJRJobType, tmpJRCstMeth) = "DATA"
                        If tmpJRSubName <> "" And
                           tmpJRJobName = comboJTjrJob.Text Then
                            '                If tmpJRJobName = comboJTjrJob.Text Then

                            tmpKey = tmpJRJobName & tmpJRSubName

                            tmpItem.Job = tmpJRJobName
                            tmpItem.Subjob = tmpJRSubName
                            tmpItem.DeactDate = tmpJRDeactDate
                            tmpItem.CostMeth = tmpJRCstMeth
                            ' add logic here to get additional field information
                            '
                            ' retrieve JIM info for this job.
                            If JTjim.JTjimJTjrReadRec(JimInfo, tmpItem.Job, tmpItem.Subjob) <> "DATA" Then
                                MsgBox("Error Retrieving JIM data - JTjrHeadingInfo" & vbCrLf & "Job >" & tmpJob &
                                     "<" & vbCrLf & "Subjob >" & tmpSubjob & "< - JTjr~199")
                                Me.Close()
                                JTMainMenu.JTMMinit()
                            End If
                            tmpItem.JobDesc = JimInfo.JTjimJIDescName
                            tmpItem.AUPLabor = JimInfo.JTjimJIAUPLabor
                            tmpItem.AUPMat = JimInfo.JTjimJIAUPMat
                            tmpItem.AUPSub = JimInfo.JTjimJIAUPServices
                            tmpItem.HasUnbilled = False
                            slJTjrJobs.Add(tmpKey, tmpItem)
                        End If
                        tmpJobSub += 1
                    Loop
                Else
                    ' --------------------------------------------------------------
                    ' logic goes here to preparet table entry in the case of a job
                    ' report for just one customer/job key.
                    ' --- apparently nothing is needed ???
                    ' --------------------------------------------------------------
                End If
                ' --------------------------------------------------------------
                ' Retrieve unbilled activity ... labor and nlc.
                ' --------------------------------------------------------------
                '
                ' Labor
                '
                Dim tmpsub As Integer = 0
                Dim tmpTKArrayItem As JTtk.JTtkArrayStructure = Nothing
                Do While JTtk.JTtkReturnUnbilledTK(tmpsub, tmpTKArrayItem) = "DATA"
                    If slJTjrJobs.ContainsKey(tmpTKArrayItem.JTtkJobID & tmpTKArrayItem.JTtkSubID) Then
                        Dim tmpJTjrItem As JTjrJobsStru = slJTjrJobs(tmpTKArrayItem.JTtkJobID & tmpTKArrayItem.JTtkSubID)
                        slJTjrJobs.Remove(tmpTKArrayItem.JTtkJobID & tmpTKArrayItem.JTtkSubID)
                        tmpJTjrItem.UnBHours += Val(tmpTKArrayItem.JTtkHours)
                        tmpJTjrItem.UnBLabor += Val(tmpTKArrayItem.JTtkDolAmt)
                        '
                        ' ----------------------------------------------------------------
                        ' Look up employee pay rate and tax load to calculate est. margin.
                        ' ----------------------------------------------------------------
                        Dim tmpPOS As Integer = 0
                        Dim tmpID As String = ""
                        Dim tmpName As String = ""
                        Dim tmpLabCat As String = ""
                        Dim tmpPayMeth As String = ""
                        Dim tmpPRID As String = ""
                        Dim tmpPayRate As String = ""
                        Dim tmpEmail As String = ""
                        Dim tmpTermDate As String = ""
                        Dim tmpMrgnDol As Double = 0

                        Do While JThr.JThrNewReadFunc(tmpPOS, tmpID, tmpName, tmpLabCat, tmpPayMeth, tmpPRID,
                                          tmpPayRate, tmpEmail, tmpTermDate) = "DATA"

                            If tmpID = tmpTKArrayItem.JTtkEmpID Then
                                If tmpPayMeth = "W2" Then
                                    tmpMrgnDol = Val(tmpTKArrayItem.JTtkDolAmt) - (Val(tmpPayRate) * Val(tmpTKArrayItem.JTtkHours) *
                            (Val(JTVarMaint.PayrollTaxLoadPercent) + 100) / 100)
                                Else
                                    tmpMrgnDol = Val(tmpTKArrayItem.JTtkDolAmt) - (Val(tmpPayRate) * Val(tmpTKArrayItem.JTtkHours))
                                End If
                                tmpJTjrItem.UnBEstMrgn += tmpMrgnDol
                                Exit Do
                            End If
                            tmpPOS += 1
                            '
                        Loop
                        '
                        tmpJTjrItem.HasUnbilled = True
                        slJTjrJobs.Add(tmpTKArrayItem.JTtkJobID & tmpTKArrayItem.JTtkSubID, tmpJTjrItem)

                    End If
                Loop
                tmpTKArrayItem = Nothing
                '
                ' NLC
                '
                Dim tmpRprtName As String = "JR"
                Dim tmpNLCType As String = ""
                Dim tmpExptMode As String = ""
                Dim tmpSLkey As String = ""
                Dim tmpCurrRow As Integer = 0
                Dim tmpNLCArrayItem As JTnlc.JTnlcStructure = Nothing
                Do While JTnlc.JTasNLCExt(tmpRprtName, tmpNLCType, tmpExptMode, tmpCurrRow,
                                tmpSLkey, tmpNLCArrayItem) = "DATA"
                    If slJTjrJobs.ContainsKey(tmpNLCArrayItem.JobID & tmpNLCArrayItem.SubID) Then
                        Dim tmpJTjrItem As JTjrJobsStru = slJTjrJobs(tmpNLCArrayItem.JobID & tmpNLCArrayItem.SubID)
                        slJTjrJobs.Remove(tmpNLCArrayItem.JobID & tmpNLCArrayItem.SubID)
                        tmpJTjrItem.HasUnbilled = True
                        Select Case tmpNLCArrayItem.MorS
                            Case "M"
                                tmpJTjrItem.UnBMat += Val(tmpNLCArrayItem.TBInvd)
                            Case "S"
                                tmpJTjrItem.UnBSub += Val(tmpNLCArrayItem.TBInvd)
                            Case "IH"
                                tmpJTjrItem.UnBIH += Val(tmpNLCArrayItem.TBInvd)
                            Case "IS"
                                tmpJTjrItem.UnBMat += Val(tmpNLCArrayItem.TBInvd)
                            Case "ADJ"
                                tmpJTjrItem.UnbAdj += Val(tmpNLCArrayItem.TBInvd)
                        End Select
                        tmpJTjrItem.UnBEstMrgn += (Val(tmpNLCArrayItem.TBInvd) - Val(tmpNLCArrayItem.SupCost))
                        '
                        slJTjrJobs.Add(tmpNLCArrayItem.JobID & tmpNLCArrayItem.SubID, tmpJTjrItem)
                        '
                    End If
                Loop
                '
                ' ----------------------------------------------------------------------------------------
                ' Logic to get IH records for the Job (and Subjobs) that are being reported.
                ' ----------------------------------------------------------------------------------------
                tmpsub = 0
                Dim keyA As ICollection = slJTjrJobs.Keys
                Dim tmpJTasKey As String = ""        'Not used in this class --- installed for JTae
                tmpJob = slJTjrJobs(keyA(0)).Job
                Dim tmpJTasItem As JTas.JTasIHStru = Nothing
                Do While JTas.JTasJRExt(tmpsub, tmpJob, tmpJTasItem, tmpJTasKey) = "DATA"
                    Dim tmpInvDate As Date = CDate(tmpJTasItem.ihInvDate)
                    tmpKey = tmpJTasItem.ihJobID & tmpJTasItem.ihSubID &
                        Gregarian2Julian(tmpInvDate) & tmpJTasItem.ihInvNum
                    If slJTjrInv.Contains(tmpKey) Then
                        MsgBox("Job Report Build" & vbCrLf &
                               "Duplicate Invoice Record exists -" & vbCrLf &
                               "Key - " & tmpKey & vbCrLf &
                               "JTjr~322")
                    Else

                        If rbtnTY.Checked = True Then
                            If tmpInvDate.Year = Today.Date.Year Then
                                slJTjrInv.Add(tmpKey, tmpJTasItem)
                            End If
                        Else
                            '              MsgBox("JR Invoice record add - >" & tmpKey & "< - JTjr~363")
                            slJTjrInv.Add(tmpKey, tmpJTasItem)

                        End If
                    End If
                Loop



                '
                ' ----------------------------------------------------------------------------------------
                ' Logic to call routines to print title Row, Unbilled Section and invoice history section.
                ' ----------------------------------------------------------------------------------------
                '
                Dim key As ICollection = slJTjrJobs.Keys
                Dim k As String = ""
                '
                For Each k In key
                    ' --------------------------------------------------------------------------------
                    ' Logic to determine if there is anything to print to this customer/job key.
                    ' --------------------------------------------------------------------------------

                    Dim tmpHasInvoices As Boolean = False
                    keyA = slJTjrInv.Keys
                    Dim kA As String = ""
                    For Each kA In keyA
                        If slJTjrInv(kA).ihJobID = slJTjrJobs(k).Job Then
                            If slJTjrInv(kA).ihSubID = slJTjrJobs(k).Subjob Then
                                tmpHasInvoices = True     ' Key has invoices to print.
                                Exit For
                            End If
                        End If
                    Next
                    If tmpHasInvoices = True Or slJTjrJobs(k).HasUnbilled = True Then
                        JTjrBldClassHeader(mydoc, k)
                    End If


                    If slJTjrJobs(k).HasUnbilled = True Then
                        JTjrBldUnbilled(mydoc, k)
                    End If
                    If tmpHasInvoices = True Then
                        JTjrBldInv(mydoc, k)
                    End If
                    tmpHasInvoices = False    ' reset for next k
                Next
                '
                Dim JIMwhtFooter As New Phrase(FP_H8Blue("Information managed and reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                mydoc.Add(JIMwhtFooter)
                '
                mydoc.Close()
                '
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        '
        slJTjrJobs.Clear()
        slJTjrInv.Clear()
        '
        JTvPDF.JTvPDFDispPDF(JTVarMaint.JTvmFilePath & "JTjrJobReport" & ".PDF")
        ' deleting temporary pdf report file    
        My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTjrJobReport" & ".PDF")
        '
        Me.Close()
        If JTye.JTyeInProgress = False Then
            JTMainMenu.JTMMinit()
        End If
        '
    End Sub
    Public Function JTjrBldClassHeader(ByRef mydoc As Document, k As String)
        Dim table As New PdfPTable(5)
        ' actual width of table in points
        table.TotalWidth = 675.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0   ' reprint first & second rows at top of each page
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single =
           {1.3F, 2.6F, 0.6F, 1.9F, 1.9F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
        '
        ' leave a gap before And after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim ArrayData As String = slJTjrJobs(k).Job
        If slJTjrJobs(k).Subjob <> "" Then
            ArrayData = ArrayData & ":" & slJTjrJobs(k).Subjob
        End If
        JTjrFillCell(ArrayData, table, 0, 1, 1, "YELLOW")
        '

        ArrayData = slJTjrJobs(k).JobDesc
        JTjrFillCell(ArrayData, table, 0, 1, 1, "YELLOW")
        '
        ArrayData = slJTjrJobs(k).CostMeth
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = " "
        If slJTjrJobs(k).CostMeth = "AUP" Then

            If slJTjrJobs(k).AUPLabor = " " Or
            slJTjrJobs(k).AUPMat = " " Or
            slJTjrJobs(k).AUPSub = " " Then
                ArrayData = "Excluded from AUP - "
                If slJTjrJobs(k).AUPLabor = " " Then
                    ArrayData = ArrayData & "Labor "
                End If
                If slJTjrJobs(k).AUPMat = " " Then
                    ArrayData = ArrayData & "Material "
                End If
                If slJTjrJobs(k).AUPSub = " " Then
                    ArrayData = ArrayData & "Services "
                End If
            End If
        End If
        JTjrFillCell(ArrayData, table, 0, 1, 1, "YELLOW")
        '
        ArrayData = " "
        If slJTjrJobs(k).DeactDate <> "" Then
            ArrayData = "Deactivated - " & slJTjrJobs(k).DeactDate
        End If
        JTjrFillCell(ArrayData, table, 0, 1, 1, "YELLOW")
        '

        mydoc.Add(table)
        Return ""
    End Function
    '
    Private Function JTjrBldUnbilled(ByRef mydoc As Document, k As String)
        Dim table As New PdfPTable(8)
        ' actual width of table in points
        table.TotalWidth = 600.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2   ' reprint first & second rows at top of each page
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single =
           {0.9F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
        '
        ' leave a gap before And after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim ArrayData As String = "Unbilled Activity"
        JTjrFillCell(ArrayData, table, 0, 8, 1, "YELLOW")
        '
        ArrayData = "Hours"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Labor"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Material"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Services"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "InHouse"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Adjustments"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Tl. Unbilled"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Est. Margin"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBHours <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBHours)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBLabor <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBLabor)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBMat <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBMat)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBSub <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBSub)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBIH <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBIH)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnbAdj <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnbAdj)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        Dim tmpTlUnbilled As Double = slJTjrJobs(k).UnbAdj + slJTjrJobs(k).UnbLabor +
            slJTjrJobs(k).UnbMat + slJTjrJobs(k).UnbSub + slJTjrJobs(k).UnbIH
        ArrayData = String.Format("{0:0.00}", tmpTlUnbilled)
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = " "
        If slJTjrJobs(k).UnBEstMrgn <> 0 Then
            ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnBEstMrgn)
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        '   Dim UnBHours As Double
        '   Dim UnBLabor As Double
        '   Dim UnBMat As Double
        '   Dim UnBSub As Double
        '   Dim UnBIH As Double
        '   Dim UnbAdj As Double
        '   Dim UnBEstMrgn As Double
        '
        mydoc.Add(table)
        Return " "
    End Function
    Public Function JTjrBldInv(ByRef mydoc As Document, k As String)
        ' -------------------------------------------------------------------
        ' Build table to print selected invoice history records.
        ' -------------------------------------------------------------------
        Dim table As New PdfPTable(9)
        ' actual width of table in points
        table.TotalWidth = 650.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2   ' reprint first & second rows at top of each page
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single =
           {1.3F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
        '
        ' leave a gap before And after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim ArrayData As String = "Invoice History"
        JTjrFillCell(ArrayData, table, 0, 9, 1, "YELLOW")
        '
        ArrayData = "Inv. Date/#"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Labor"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Material"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Services"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "InHouse"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Adjustments"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "AUP"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Invoice Tl"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        '
        ArrayData = "Est. Mrgn"
        JTjrFillCell(ArrayData, table, 1, 1, 1, "YELLOW")
        Dim keyA As ICollection = slJTjrInv.Keys
        Dim tmpNumofInv As Integer = 0
        Dim tmpJobTotal As JTas.JTasIHStru = Nothing
        Dim kA As String = ""
        For Each kA In keyA
            If slJTjrInv(kA).ihJobID = slJTjrJobs(k).Job Then
                If slJTjrInv(kA).ihSubID = slJTjrJobs(k).Subjob Then
                    tmpNumofInv += 1
                    ArrayData = slJTjrInv(kA).ihInvDate & "-" & slJTjrInv(kA).ihInvNum
                    JTjrFillCell(ArrayData, table, 1, 1, 1, "")
                    '
                    JTjrFillInvAmtFlds(table, slJTjrInv(kA))
                    '
                    ' Update total fields
                    tmpJobTotal.ihLab = Val(tmpJobTotal.ihLab) + Val(slJTjrInv(kA).ihLab)
                    tmpJobTotal.ihMat = Val(tmpJobTotal.ihMat) + Val(slJTjrInv(kA).ihMat)
                    tmpJobTotal.ihSub = Val(tmpJobTotal.ihSub) + Val(slJTjrInv(kA).ihSub)
                    tmpJobTotal.ihIH = Val(tmpJobTotal.ihIH) + Val(slJTjrInv(kA).ihIH)
                    tmpJobTotal.ihAdj = Val(tmpJobTotal.ihAdj) + Val(slJTjrInv(kA).ihAdj)
                    tmpJobTotal.ihAUPInv = Val(tmpJobTotal.ihAUPInv) + Val(slJTjrInv(kA).ihAUPInv)
                    tmpJobTotal.ihInvTl = Val(tmpJobTotal.ihInvTl) + Val(slJTjrInv(kA).ihInvTl)
                    tmpJobTotal.ihEstMrgn = Val(tmpJobTotal.ihEstMrgn) + Val(slJTjrInv(kA).ihEstMrgn)
                End If
            End If
        Next
        If tmpNumofInv > 1 Then
            ArrayData = " "
            JTjrFillCell(ArrayData, table, 0, 9, 0, "")
            ArrayData = "Job Total"
            JTjrFillCell(ArrayData, table, 0, 1, 1, "YELLOW")
            '
            JTjrFillInvAmtFlds(table, tmpJobTotal)
            '
            Dim tmpTlUnbilled As Double = slJTjrJobs(k).UnbAdj + slJTjrJobs(k).UnbLabor +
            slJTjrJobs(k).UnbMat + slJTjrJobs(k).UnbSub + slJTjrJobs(k).UnbIH
            '
            If tmpTlUnbilled <> 0 Then
                ArrayData = "Invoive Total + Unbilled Amounts -------->"
                JTjrFillCell(ArrayData, table, 2, 7, 1, "")
                ArrayData = String.Format("{0:0.00}", tmpTlUnbilled + Val(tmpJobTotal.ihInvTl))
                JTjrFillCell(ArrayData, table, 2, 1, 1, "")
                '
                ArrayData = String.Format("{0:0.00}", slJTjrJobs(k).UnbEstMrgn + Val(tmpJobTotal.ihEstMrgn))
                JTjrFillCell(ArrayData, table, 2, 1, 1, "")
            End If
        End If

        mydoc.Add(table)
        Return ""
    End Function
    '
    Public Function JTjrFillInvAmtFlds(ByRef table As PdfPTable, slJTjrInv As JTas.JTasIHStru)
        Dim ArrayData As String = "  "
        If Val(slJTjrInv.ihLab) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihLab))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihMat) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihMat))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihSub) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihSub))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihIH) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihIH))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihAdj) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihAdj))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihAUPInv) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihAUPInv))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihInvTl) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihInvTl))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        ArrayData = "  "
        If Val(slJTjrInv.ihEstMrgn) <> "0" Then
            ArrayData = String.Format("{0:0.00}", Val(slJTjrInv.ihEstMrgn))
        End If
        JTjrFillCell(ArrayData, table, 2, 1, 1, "")
        '
        Return ""
    End Function

    '
    Public Function JTjrHeadingInfo(mydoc As Document, tmpJob As String, tmpSubjob As String)
        ' ---------------------------------------------------------------------
        ' Build a table with heading information for the Job Report
        ' ---------------------------------------------------------------------
        Dim table As New PdfPTable(6)
        ' actual width of table in points
        table.TotalWidth = 675.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2   ' reprint first & second rows at top of each page
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single =
           {1.0F, 2.0F, 1.0F, 2.0F, 1.0F, 2.4F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
        '
        ' leave a gap before And after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim JimInfo As JTjim.JTjimJobInfo = Nothing
        '
        ' Code to find the first active job for this customer. Will only be used on consolidated 
        ' Job Reports.
        '
        If cboxConsolidate.Checked = True Then
            Dim tmpJobName As String = ""
            Dim tmpSubName As String = ""
            Dim tmpDeactDate As String = ""
            Dim tmpJobType As String = ""
            Dim tmpCstMeth As String = ""
            Dim tmpJobSub = 0
            '
            Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
                If tmpJobName = comboJTjrJob.Text And
                        tmpDeactDate = "" Then
                    tmpJob = tmpJobName
                    tmpSubjob = tmpSubName
                    Exit Do
                End If
                tmpDeactDate = ""
                tmpJobSub += 1
            Loop
        End If
        ' retrieve JIM infor for this job.
        If JTjim.JTjimJTjrReadRec(JimInfo, tmpJob, tmpSubjob) <> "DATA" Then
            MsgBox("Error Retrieving JIM data - JTjrHeadingInfo(JR.001)")
            Me.Close()
            JTMainMenu.JTMMinit()
        End If
        ' retrieve Cust information
        Dim CustInfo As JTjim.JTjimCustInfo = Nothing
        If JTjim.JTjimReadCustRec(CustInfo, tmpJob) <> "DATA" Then
            MsgBox("Error Retrieving JIM customer data - JTjrHeadingInfo(JR.001)")
            Me.Close()
            JTMainMenu.JTMMinit()
        End If
        Dim ArrayData As String = "Job ID "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJIJobName
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Job Location"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        '       ArrayData = " "
        '      JTjrFillCell(ArrayData, table)
        '
        ArrayData = "Contact Information"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        ArrayData = "Job Name"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        If cboxConsolidate.Checked = True Then
            ArrayData = "Consolidated Customer Report"
        Else
            ArrayData = JimInfo.JTjimJIDescName
        End If
        '    ArrayData = JimInfo.JTjimJIDescName
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Street"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJIJobLocStreet
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Primary"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        ArrayData = "Tax Exempt"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCITaxExemptJob
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "City, State"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJIJobLocCity
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Name"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIContact
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Creation Date"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJICreateDate
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Billing Address"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        ArrayData = "Telephone"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCITelephone
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Lst Act Dt"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJILastActDate
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Street"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIBillStreet
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Email"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIEmail
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Deact Date"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJIDeactDate
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "City, State"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIBillCity
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Secondary"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Name"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCISecContact
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Margin %"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        ArrayData = "Invoice Delivery"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        ArrayData = "Telephone"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCISecContPhone
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Materials"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJIMatMargin
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "PDF/Email"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIInvPDF
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "Email"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCISecEmail
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Services"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = JimInfo.JTjimJISubMargin
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "USPS"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIInvUSPS
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "Invoice Email Options"
        JTjrFillCell(ArrayData, table, 1, 2, 1, "YELLOW")
        '
        ArrayData = "Inv $ Seq."
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        If JimInfo.JTjimJISndToAcct = "X" Then
            ArrayData = "Normal"
        Else
            ArrayData = "Alternate"
        End If
        '
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "File"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = CustInfo.JTjimCIInvFile
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = "Primary"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        If CustInfo.JTjimCIInvEmailPrim = "X" Then
            ArrayData = "yes"
        Else
            ArrayData = " "
        End If
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = " "
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        ArrayData = "Secondary"
        JTjrFillCell(ArrayData, table, 0, 1, 0, "")
        '
        If CustInfo.JTjimCIInvEmailAlt = "X" Then
            ArrayData = "yes"
        Else
            ArrayData = " "
        End If
        JTjrFillCell(ArrayData, table, 1, 1, 0, "")
        '
        mydoc.Add(table)
        '
        JimInfo = Nothing

        Return ""
    End Function
    Public Function JTjrFillCell(ArrayData As String, ByRef table As PdfPTable,
                                 Position As Integer, Span As Integer,
                                 Border As Integer, Color As String) As String
        If ArrayData = "" Then
            ArrayData = " "
        End If
        Dim CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
        CellContents.HorizontalAlignment = Position
        CellContents.Colspan = Span
        '
        If Border = 0 Then
            CellContents.Border = Border
        End If
        If Color <> "" Then
            Select Case Color.ToUpper
                Case "YELLOW"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(249, 231, 159)  ' Yellow
                Case "BLUE"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(0, 0, 255)
                Case "GREEN"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(143, 188, 139)  ' Yellow
                Case "RED"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(255, 0, 0)
            End Select
        End If
        '
        table.AddCell(CellContents)
        Return ""
    End Function


End Class