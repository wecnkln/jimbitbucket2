﻿Imports System.IO
Imports System.Timers
Imports System.Reflection
Imports System.Runtime.Remoting.Services

Public Class JTstart
    Public JTstartbtnPressed As Boolean = False
    Public JTstEOJReporting As Boolean = False
    Public JTediInvDispCount As Integer = 0
    Public JTediFileTBDeleted As String = Nothing
    Dim screenHeight As Integer = My.Computer.Screen.Bounds.Height
    Dim screenWidth As Integer = My.Computer.Screen.Bounds.Width
    Dim tmpMSGString As String = "Initializing Labor Category Arrays"
    ''' <summary>
    ''' This sub is triggered by clicking the sign in button on the start screen. 
    ''' It triggers several events. The first is to verify that system data files exist.
    ''' If not this is probably a new installation and skeleton files and directories are created.
    ''' It also checks to see if Year-end processing needs to be run. After these checks, 
    ''' it organizes the initial load of existing data, links out to display the dashboard 
    ''' and then send the system to normal operation. 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSignIn_Click(sender As Object, e As EventArgs) Handles btnSignIn.Click

        JTstartbtnPressed = True
        ' -----------------------------------------------------------------------------
        ' Code to check to see if directories and files exist. If they do not exist,
        ' operator is asked if this is a new install. If so, directories and file
        ' stub are created. After creation the system continues into normal operation.
        ' If it is not a new install. Problem must be resolved prior to operation.
        ' -----------------------------------------------------------------------------
        Dim dir As String = Environ("OneDrive") & "/JobInfoMngr"
        '
        If Not Directory.Exists(dir) Then
            Dim result As Integer = MsgBox(dir & " Does not Exist or cannot be found." & vbCrLf &
                "Is this an initial installation? If YES, Data directories and 'Stub' files will " & vbCrLf & "be created. If NO, system will abort. See manual for possible solutions. (ST.006)" _
                                , 4, "J.I.M. - Initial Installation")
            If result = DialogResult.Yes Then
                JTstFileDirCreate()
                JTstFileStubCreate()
                MsgBox("Directories created and skeleton file are ready for use. (ST.008)")
            Else
                MsgBox("System Aborted --- See manual to resolve problems (ST.007)",, "J.I.M. - Initial Installation")
                Me.Close()
                Application.Exit()
            End If
        End If
        '
        ' --------------------------------------------------------------------------------
        ' Password validation should go here.
        '       JTstartPWCheck()
        ' --------------------------------------------------------------------------------
        '
        JTVarMaint.JTvmVarLoad()
        '
        ' --------------------------------------------------------------------
        ' Check on software release and also system signon status.
        ' --------------------------------------------------------------------
        If System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed = True Then
            Dim tmpVersion As String = ""
            With System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion
                tmpVersion = .Major & "." & .Minor & "." & .Build & "." & .Revision
            End With
            If JTVarMaint.JTvmSWRelease = Nothing Then
                JTVarMaint.JTvmSWRelease = tmpVersion    ' set initial version
            End If
            If tmpVersion < JTVarMaint.JTvmSWRelease Then
                MsgBox("JobInfoMgr Software Version ---- (ST.001)" & vbCrLf & vbCrLf &
                       "Current Latest Version --->" & JTVarMaint.JTvmSWRelease & "<" & vbCrLf &
                       "Version on this computer-->" & tmpVersion & "<" & vbCrLf & vbCrLf &
                       "Software on this computer requires an update." & vbCrLf &
                       ">>>>>>>> SYSTEM ABORTED <<<<<<<<")
                Me.Close()
                Application.Exit()
            Else
                If tmpVersion > JTVarMaint.JTvmSWRelease Then
                    JTVarMaint.JTvmSWRelease = tmpVersion
                    JTVarMaint.JTvmFlush()
                End If
            End If
        End If
        '
        ' ------------------------------------------------------------------------------
        ' Check to see if system is currently in use by another user. Only one user can
        ' updates at a time. 
        ' The following actions can take place here ---->
        '   1. Realize someone else is using system.
        '       - Exit the system until the other user is finished.
        '       - Override their signin (taking over the system) --- The system will lock
        '         the previous user so that no more updates are allowed.
        '       - Sign on Read-only --- The prior user can continue --- you can enter
        '         system but will be locked out of any updating.
        '   2. System is available ---->
        '       - Update your user name and time of login.
        '       - Flush VarMaint file to memorialize the login.
        ' ------------------------------------------------------------------------------
        '
        If txtUserID.Text = "" Then
            ToolTip1.Show("A User ID must be entered. Correct and Resubmit.(ST.009)",
                          txtUserID, 35, -55, 7000)
            txtUserID.Select()
            Exit Sub
        End If
        '
        txtUserID.Text = txtUserID.Text.ToUpper
        '
        If JTVarMaint.JTvmSignedInUser <> "" Then
            If JTVarMaint.JTvmSignedInUser <> txtUserID.Text Then
                Dim tmpAnswer = MsgBox("A user is currently logged into the system." & vbCrLf &
                       "User ID --->" & JTVarMaint.JTvmSignedInUser & vbCrLf &
                       "Log In Time --->" & JTVarMaint.JTvmSignInTime & vbCrLf &
                       "--------------------------------------------------" & vbCrLf &
                       "If you want to override the current logged in user, Enter 'YES'" & vbCrLf &
                       "If you do not want to interupt the current session, Enter 'NO'" & vbCrLf &
                       "(The system will terminate.) (ST.010)", 4, "WHT Job Information Manager")
                If tmpAnswer = vbNo Then
                    Me.Close()
                    Application.Exit()
                    Exit Sub
                End If
            End If
        End If
        JTVarMaint.JTvmSignedInUser = txtUserID.Text
        JTVarMaint.JTvmSignInTime = DateAndTime.Now
        '
        JTVarMaint.JTvmFlush()
        '
        ' Do maintenance on JTJIMWorkingFiles folder
        PrepWFFolder()
        '

        Me.Visible = False
        Me.txtUserID.Visible = False
        Me.btnSignIn.Visible = False
        Me.btnExit.Visible = False
        Me.Label2.Visible = False
        Me.chkboxJTStartForgotPW.Visible = False
        '
        Dim tmpJimScreenHeight As Integer = screenHeight
        Dim tmpJimScreenwidth As Integer = screenWidth

        If tmpJimScreenwidth > 1600 Then
            tmpJimScreenwidth = 1600
        End If
        If tmpJimScreenHeight > 960 Then
            tmpJimScreenHeight = 960
        End If

        Me.Location = New Point(0, 0)
        Me.Size = New Size(tmpJimScreenwidth, tmpJimScreenHeight)
        Me.PictureBox1.Location = New Point(tmpJimScreenwidth - 210, tmpJimScreenHeight - 200)
        Me.TextBox1.Location = New Point(18, tmpJimScreenHeight - 90)
        '
        Me.Visible = True
        '
        LogFileWrite("JTstart", "System Signon - " & JTVarMaint.JTvmSafeClose)
        '
        ' ------------------------------------------------------------------------------
        ' Logic inserted to delete PDFWorkFiles from prior JIM sessions.
        ' ------------------------------------------------------------------------------
        If (Directory.Exists(JTVarMaint.JTvmFilePathDoc & "JTJIMPdfWF/")) Then
            My.Computer.FileSystem.DeleteDirectory(JTVarMaint.JTvmFilePathDoc & "JTJIMPdfWF/", FileIO.DeleteDirectoryOption.DeleteAllContents)
        End If
        '
        ' =======================================================================
        '
        ' --------------------------------------------------------------------
        ' Check to see if Year-end processing should be executed. Ask operator
        ' if it should be done now.
        ' --------------------------------------------------------------------
        If JTVarMaint.JTvmSystemYear <> System.DateTime.Now.Year.ToString Then
            JTye.JTyeInitYERoutines()
        Else
            btnSignIn_Click_Continued()
        End If
    End Sub
    Public Sub btnSignIn_Click_Continued()
        '
        ' ----------------------------------------------------
        ' Flush JTGlobalSettings.dat with "ACTIVE" status.
        ' ----------------------------------------------------
        JTVarMaint.JTvmSafeClose = "ACTIVE"
        JTVarMaint.JTvmFlush()
        '
        ' --------------------------------------------------------------------------------------------
        ' Check to see if a folder exists for incoming invoices. If it doesn't exist, create it'
        ' Folder name --- "/EDIInputFiles"
        ' --------------------------------------------------------------------------------------------
        Dim dir As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "/JobInfoMngr/EDIInputFiles"
        If Not Directory.Exists(dir) Then
            Directory.CreateDirectory(dir)
        End If
        '
        Dim tmpTBW As Integer = (screenWidth / 2) - 200
        Dim tmpTBH As Integer = screenHeight / 2 - 175


        txtJTInitStatus.Location = New Point(tmpTBW, tmpTBH)
        '
        txtJTInitStatus.Size = New Size(300, 300)
        '     Dim tmpMSGString As String = "Initializing Labor Category Arrays"
        txtJTInitStatus.Text = tmpMSGString
        '     txtJTInitStatus.BringToFront()
        ' ---------------------------------------------------------------------------
        ' Making txtJTinitStatus invisible --- trying to clean up start screen.
        ' ---------------------------------------------------------------------------
        txtJTInitStatus.Visible = False

        txtJTInitStatus.Select()
        '
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing TK Arrays"
        ' ---------------------------------------------------------
        ' Load Global Settings
        ' ---------------------------------------------------------
        '
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing Global Settings"
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing Labor Category Arrays"
        txtJTInitStatus.Text = tmpMSGString
        txtJTInitStatus.Select()
        LaborCategories.JTlcArrayFill()
        '
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing Human Resource Arrays"
        txtJTInitStatus.Text = tmpMSGString
        txtJTInitStatus.Select()
        JThr.JThrArrayFill()
        '
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing Job Information Arrays"
        txtJTInitStatus.Text = tmpMSGString
        txtJTInitStatus.Select()
        '
        ' --------------------------------------------------------------
        ' Code to set initial value for job sequence on mainmenu.
        ' --------------------------------------------------------------
        If JTVarMaint.JTgvMMJobSeq = "J" Then
            JTMainMenu.rbtnJTmmJSAlpha.Checked = True
        Else
            JTMainMenu.rbtnJTMMJSAmt.Checked = True
        End If
        '
        '
        JTjim.JTjimJobArrayRestore()
        '
        tmpMSGString = tmpMSGString & vbCrLf & "Initializing Vendor Information Arrays"
        txtJTInitStatus.Text = tmpMSGString
        txtJTInitStatus.Select()
        JTvi.JTviLoad()
        '
        ' -------------------------------------------
        ' Files for TK, NLC, Inv Hist and AUP are all loaded as part of MMinit.
        ' -------------------------------------------
        '
        txtJTInitStatus.Visible = False
        '
        ' -------------------------------------------------------------------------------
        ' Check to see if dashboard should be shown. True = Yes
        ' -------------------------------------------------------------------------------
        If JTVarMaint.JTvmShowDashBoard = True Then
            JTdb.Show()
        Else
            '     JTMainMenu.MdiParent = JTstart
            JTMainMenu.JTMMinit()
        End If
        '
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        JTstartbtnPressed = True
        Me.Close()
        Application.Exit()
    End Sub
    Sub JTstFileDirCreate()
        ' ------------------------------------------------------------
        ' Directories for JIM system use created.
        ' ------------------------------------------------------------        
        Dim dir As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "/JobInfoMngr/"
        Directory.CreateDirectory(dir)
        Dim dir1 As String = dir & "/Logo"
        Directory.CreateDirectory(dir1)
        dir1 = dir & "/EDIInputFiles"
        Directory.CreateDirectory(dir1)
        '
        dir = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "/JobInfoMngrDocs/"
        Directory.CreateDirectory(dir)
        '

    End Sub
    Sub JTstFileStubCreate()
        ' -------------------------------------------------------------
        ' Creates stub files for new installation use.
        ' -------------------------------------------------------------
        '
        Dim dir As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\JobInfoMngr\"
        dir = dir.Replace("Documents", "OneDrive")
        JTVarMaint.JTvmFilePath = dir
        '

        ' Create JTGlobalSettings.dat
        JTVarMaint.JTvmSystemYear = System.DateTime.Now.Year.ToString
        JTVarMaint.JTvmFilePath = dir
        JTVarMaint.JTvmSafeClose = "SAFE"
        JTVarMaint.JTvmFlush()

        '

        ' Create LabCat.dat
        Dim filePath As String = dir & "JTLabCat.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '

        ' Create hrData.dat
        filePath = dir & "JThrData.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<hrRecord>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '

        ' Create Jobs.dat
        filePath = dir & "JTJobs.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<Job>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' Create JTCustomers.dat
        filePath = dir & "JTCustomers.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<Customers>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' Create NonLaborCosts.dat
        filePath = dir & "JTNonLaborCosts.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<nlcRecord>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '

        ' Create TimeKeeping.dat
        filePath = dir & "JTTimeKeeping.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<tkRecord>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '

        ' Create isLibrary.dat
        filePath = dir & "JTisLibrary.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<isRecord>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '

        ' Create InvHist.dat
        filePath = dir & "JTInvHist.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<InvHistory>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' Create JTAUP.dat
        filePath = dir & "JTAUP.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<AUPJobRecord>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' Create JTVendInfo.dat
        filePath = dir & "JTVendInfo.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            writeFile.WriteLine("<VendInfo>")
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub JTstart_FormClosing(sender As Object, e As FormClosingEventArgs) _
     Handles Me.FormClosing
        ' -------------------------------------------------------------------------------
        ' Handles situation where operator hits 'X' on the JTstart panel. This is not the
        ' normal way to close the system. It will. however, attempt to close the 
        ' JTGlobalSettings.dat file with a 'SAFE' status and then close the system.
        ' -------------------------------------------------------------------------------

        If e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            Me.Hide()
            ' ----------------------------------------------------
            ' Flush JTGlobalSettings.dat with "SAFE" close status.
            ' ----------------------------------------------------
            If JTVarMaint.JTvmSafeClose = "ACTIVE" Then
                JTVarMaint.JTvmSafeClose = "SAFE"
                JTVarMaint.JTvmFlush()
            End If
            '
            ' Log user off
            JTVarMaint.JTvmSignedInUser = Nothing
            JTVarMaint.JTvmSignInTime = Nothing
            '
            Application.Exit()
            '
            ' ---------
            ' Goodbye '
            ' ---------
        End If

    End Sub

    Private Sub JTstart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' ---------------------------------------------------------------------
        ' Code to put current version in JTstart footer on productive editions.
        ' ---------------------------------------------------------------------
        If System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed = True Then
            Dim tmpVersion As String = ""
            With System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion
                tmpVersion = .Major & "." & .Minor & "." & .Build & "." & .Revision
            End With
            TextBox1.Text =
            "______________________________________________________________________________________________________" &
            vbCrLf & "WWW.WatchHillTech.Com                    Release " & tmpVersion &
            "                      Copyright 2017 - All Rights Reserved"
        End If
        '
        Dim ctl As Control
        Dim ctlMDI As MdiClient

        ' Loop through all of the form's controls looking
        ' for the control of type MdiClient.
        For Each ctl In Me.Controls
            Try
                ' Attempt to cast the control to type MdiClient.
                ctlMDI = CType(ctl, MdiClient)

                ' Set the BackColor of the MdiClient control.
                ctlMDI.BackColor = Me.BackColor

            Catch exc As InvalidCastException
                ' Catch and ignore the error if casting failed.
            End Try
        Next
        ' turn off menu strip for initial start up.
        JTstartMenuOff()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click

        JTstartExitSys()
    End Sub
    ''' <summary>
    ''' This sub is the exit route for the system. It can be initiated from
    ''' the EXIT button on the MainMenu or from the Exit entry on the menu
    ''' (when it enabled). It flushes the files (labeling the files with a 
    ''' "SAFE" mode signifying the system was shout down safely. If also 
    ''' checks and executes system backups.
    ''' </summary>
    Public Sub JTstartExitSys()
        ' --------------------------------------------------------------
        ' Turn off menu and hide mainmenu
        ' --------------------------------------------------------------
        JTstartMenuOff()
        JTMainMenu.Hide()
        '

        '
        ' ------------------------------------------------------------------------
        ' Check to see if there are invoices or NLC items pending accting extract.
        ' ------------------------------------------------------------------------
        Dim tmpnlcQBcount As Integer = JTnlc.JTnlcQBExtractchk()
        Dim tmpasQBcount As Integer = JTas.JTasQBExtractchk()
        If tmpnlcQBcount + tmpasQBcount > 0 Then
            Dim result As Integer = MessageBox.Show("----- Items exist that are pending accounting extract. ----- " _
                                                    & vbCrLf & vbCrLf &
                    "Invoices pending extract ----------------> " & String.Format("{0:0}", tmpasQBcount) & vbCrLf &
                    "NLC items pending extract ---------------> " & String.Format("{0:0}", tmpnlcQBcount) & vbCrLf &
                  vbCrLf & "Would you like create an extract - File & Report (Yes) or delay to later (No)?",
                                                    "JIM - Accounting Extract Check", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                LogFileWrite("JTstart", "QBExtract executed - Invoices - " & String.Format("{0:0}", tmpasQBcount) & "nlc - " &
                            String.Format("{0:0}", tmpnlcQBcount))
                JTstEOJReporting = True
                Dim tmpClassName As New JTae
                tmpClassName.JTaeInit()      ' call routine to create extract file and print report.
            Else
                JTstartExitSysCont()
            End If
        Else
            JTstartExitSysCont()
        End If
    End Sub
    Public Sub JTstartExitSysCont()
        ' ----------------------------------------------------
        ' Flush JTGlobalSettings.dat with "SAFE" close status.
        ' ----------------------------------------------------
        JTVarMaint.JTvmSafeClose = "SAFE"
        ' Log user off
        JTVarMaint.JTvmSignedInUser = Nothing
        JTVarMaint.JTvmSignInTime = Nothing
        '
        JTVarMaint.JTvmFlush()
        LogFileWrite("JTstart", "System Shutdown - SAFE ")
        ' ----------------------------------------------------------
        ' Archival backup logic
        ' ----------------------------------------------------------
        If JTVarMaint.JTAchBkupSwitch = True Then
            Dim tmpDateConst As String = Str(Date.Today.Year) &
                                String.Format("{0:000}", Date.Today.DayOfYear)
            Dim tmpDaysSinceLstBkup As Integer = Val(tmpDateConst) - Val(JTVarMaint.JTAchBkupDate)
            If tmpDaysSinceLstBkup - Val(JTVarMaint.JTAchBkupFreq) >= 0 Then
                '
                ' ------------------------------------------------------
                ' Add new text to JTstart saying 'Backup in progress'
                ' ------------------------------------------------------
                txtJTInitStatus.Text = "Archival Backup in Progress"
                    txtJTInitStatus.Font = New Font("Times New Roman", 14, FontStyle.Bold)
                    txtJTInitStatus.Visible = True
                    txtJTInitStatus.Select()
                    '
                    '
                    Dim tmpBkupSerNum As String = (Val(JTVarMaint.JTAchBkupSerNum) + 1).ToString().PadLeft(4, "0")
                    Dim tmpBkupDir As String = "JIMAchBkup" & Trim(tmpDateConst) & tmpBkupSerNum
                    '
                    If JTstBackup("N", tmpDateConst) = "COMPLETE" Then
                        '                        '
                        ' --------------------------------------------------------------------------
                        ' Backup completed.
                        ' Value in Global Variables are updated and flushed to disk.
                        ' --------------------------------------------------------------------------
                        JTVarMaint.JTAchBkupSerNum = tmpBkupSerNum
                        JTVarMaint.JTAchBkupDate = tmpDateConst
                        JTVarMaint.JTvmFlush()
                        '
                        ' ---------------------------------------------------------------------------
                        ' Logic to scan directory looking for backup entries that are over the
                        ' specified # of generations (Global Variables) old . These entries will be 
                        ' deleted.
                        ' ---------------------------------------------------------------------------
                        If Str(Val(JTVarMaint.JTAchBkupSerNum) > Val(JTVarMaint.JTAchBkupGen)) Then
                            Dim tmpPurgeSerNum As String = Str(Val(JTVarMaint.JTAchBkupSerNum) - Val(JTVarMaint.JTAchBkupGen))
                            For Each tmpDir As String In Directory.GetDirectories(JTVarMaint.JTAchBkupPath)
                                If tmpDir.Contains("JIMAchBkup") Then
                                    ' Calculate position of serial number with the directory entry.
                                    Dim tmpPOS As Short = 0
                                For tmpPOS = 0 To (Len(tmpDir) - 10)
                                    If tmpDir.Substring(tmpPOS, 10) = "JIMAchBkup" Then
                                        tmpPOS += 17 ' add offset to get to serial number
                                        Exit For
                                    End If
                                Next
                                ' ---------------------------------------------------------
                                ' I don't think the delete logic is working ... need to
                                ' verify. 8/25/2020
                                ' ---------------------------------------------------------
                                Dim tmpSerNum As String = tmpDir.Substring(tmpPOS, 4)
                                    If Val(tmpSerNum) <= Val(tmpPurgeSerNum) Then
                                        My.Computer.FileSystem.DeleteDirectory(tmpDir,
                                              FileIO.DeleteDirectoryOption.DeleteAllContents)
                                    End If
                                End If

                            Next
                        End If
                        '
                        MessageBox.Show("Achival Backup Complete" & vbCrLf & "Backup Directory Name - " &
                                 ">" & tmpBkupDir & "<", "J.I.M. - System Backup")
                        LogFileWrite("JTstart", "Archival Backup Completed - " & tmpBkupDir)
                    End If
                End If
            End If
        '
        Application.Exit()
        ' ---------
        ' Goodbye '
        ' ---------
    End Sub

    Private Sub EntryMaintenanceToolStripMenuItem_Click(sender As Object, e As EventArgs)

        JTMainMenu.Close()
        JTstartMenuOff()
        JTnlc.MdiParent = Me
        JTnlc.JTnlcInit()
    End Sub



    Private Sub LaborCategoryMaintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LaborCategoryMaintToolStripMenuItem.Click
        JTMainMenu.Hide()
        LaborCategories.MdiParent = Me
        LaborCategories.LCinit()
    End Sub

    Private Sub WorkerInformationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WorkerInformationToolStripMenuItem.Click
        ' -----------------------------------------------------------
        ' Entry into HR Maintenance routines.
        ' -----------------------------------------------------------
        JTMainMenu.Hide()
        JThr.MdiParent = Me
        JThr.JThrInit()
    End Sub

    Private Sub PayrollTransmittalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PayrollTransmittalToolStripMenuItem.Click
        JTMainMenu.Close()
        JTpr.MdiParent = Me
        JTpr.JTprInit()
    End Sub

    Private Sub GlobalSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GlobalSettingsToolStripMenuItem.Click
        JTMainMenu.Close()
        JTVarMaint.MdiParent = Me
        JTVarMaint.JTvmInit()
    End Sub

    Private Sub DocArchiveToolStripMenuItem_Click(sender As Object, e As EventArgs)
        JTMainMenu.Close()
        JTutil.MdiParent = Me
        JTutil.JTutilInitPFA()
    End Sub

    Private Sub ToolStripComboBox1_Click(sender As Object, e As EventArgs)
        JTMainMenu.Close()
        JTutil.MdiParent = Me
        JTutil.JTutilInitIV()
    End Sub

    Private Sub JobDeactivationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JobDeactivationToolStripMenuItem.Click
        JTMainMenu.Close()
        JTutil.MdiParent = Me
        JTutil.JTutiInitJDeact()
    End Sub

    Function JTstartHideTitles() As String
        Me.Label1.Visible = False
        Me.TextBox1.Visible = False
        Return "Done"
    End Function
    Function JTstartShowTitles() As String
        Me.Label1.Visible = True
        Me.TextBox1.Visible = True
        Return "Done"
    End Function
    Function JTstartMenuOn() As String
        Me.MenuStrip1.Enabled = True
        Return "Done"
    End Function
    Function JTstartMenuOff() As String
        Me.MenuStrip1.Enabled = False
        Return "Done"
    End Function

    Private Sub ReportPrintingToolStripMenuItem_Click_1(sender As Object, e As EventArgs)
        JTMainMenu.Hide()
        JTjm.MdiParent = Me
        JTjm.Show()
    End Sub

    Private Sub InvoiceVoidMenuItem_Click(sender As Object, e As EventArgs) Handles InvoiceVoidMenuItem.Click
        JTMainMenu.Close()
        JTutil.MdiParent = Me
        JTutil.JTutilInitIV()
    End Sub

    Private Sub InvoiceLookupToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InvoiceLookupToolStripMenuItem.Click
        JTMainMenu.Close()
        JTutil.MdiParent = Me
        JTutil.JTutilInitIL()
    End Sub

    Private Sub QBReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QBReportToolStripMenuItem.Click
        '
        Dim subtocall As New JTae
        subtocall.JTaeInit()
        '
    End Sub

    Private Sub InvoiceReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InvoiceReportToolStripMenuItem.Click
        JTMainMenu.Hide()
        JTir.Show()
    End Sub

    Private Sub JobReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JobReportToolStripMenuItem.Click
        JTMainMenu.Close()
        JTstartMenuOff()
        JTjr.JTjrInit()
    End Sub
    ''' <summary>
    ''' General function to create total system backups. The function runs in two
    ''' modes.
    ''' 1. Normal backups ... queued by parameters on a scheduled basis.
    ''' 2. Year-end backup ... executes as part of the year-end routines.
    ''' </summary>
    ''' <param name="Type">
    ''' N = Normal rediodic backup mode.
    ''' Y = Year-end backup.
    ''' </param>
    ''' <returns>
    ''' "ABORTED" = Process aborted.
    ''' "COMPLETE" = Process successfully completed. Backup created.
    ''' </returns>
    Public Function JTstBackup(Type As String, tmpDateConst As String) As String

        Do While My.Computer.FileSystem.DirectoryExists(JTVarMaint.JTAchBkupPath) = False
            Dim result As Integer = MessageBox.Show("SYSTEM BACKUP SETUP" & vbCrLf &
                                                "Make sure backup media ..." & vbCrLf &
                                                "(Directory, remote hardrive, Thumb Drive, Etc.)" _
                                                & vbCrLf & " is available at > " &
                                                JTVarMaint.JTAchBkupPath & " <. If you are using the Cloud for backup, make sure you are signed-in." & vbCrLf &
                                                "Note - If you want to change the backup location, go to Global Variables.",
                                                 "J.I.M. - System Backup",
                                                MessageBoxButtons.OKCancel)
            If result = DialogResult.Cancel Then
                If Type = "Y" Then
                    MessageBox.Show("Archival Backup failed." & vbCrLf &
                                    "Year-end routines cannot continue.",
                                    "J.I.M. - Year-End Processing")
                Else
                    MessageBox.Show("Backup routine has been aborted." & vbCrLf &
                                    "A scheduled backup will be queued again at the next J.I.M. system exit.",
                                    "J.I.M. - System Backup")
                End If
                Return "ABORTED"
            End If
        Loop
        ' Build output directory name
        Dim tmpBkupDir As String = ""
        If Type <> "Y" Then
            Dim tmpBkupSerNum As String = (Val(JTVarMaint.JTAchBkupSerNum) + 1).ToString().PadLeft(4, "0")
            tmpBkupDir = "JIMAchBkup" & Trim(tmpDateConst) & tmpBkupSerNum
        Else
            tmpBkupDir = "JIMYEBkup" & JTVarMaint.JTvmSystemYear
        End If
        My.Computer.FileSystem.CopyDirectory(JTVarMaint.JTvmFilePath,
                         JTVarMaint.JTAchBkupPath & "\" & tmpBkupDir & "\JobInfoMngr\", True)
        '
        My.Computer.FileSystem.CopyDirectory(JTVarMaint.JTvmFilePathDoc,
                         JTVarMaint.JTAchBkupPath & "\" & tmpBkupDir & "\JobInfoMngrDocs\", True)
        '
        Return "COMPLETE"
    End Function


    ''' <summary>
    ''' Little function to show or Hide WH light logo picture. Used if panel is to big.
    ''' </summary>
    ''' <param name="tmpStatus">HIDE or SHOW to determine what is supposed to be done.</param>
    ''' <returns></returns>
    Public Function JTstPictureBox1(tmpStatus As String)
        If tmpStatus = "HIDE" Or tmpStatus = "SHOW" Then
            Dim tmpOperation As String = "False"
            If tmpStatus = "SHOW" Then
                tmpOperation = "true"
            End If
            PictureBox1.Visible = tmpOperation
        End If
        Return ""
    End Function

    Private Sub NameChangeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NameChangeToolStripMenuItem.Click
        JTMainMenu.Hide()
        JTjm.MdiParent = Me
        JTjm.Show()
    End Sub


    ''' <summary>
    ''' Simple little sub used so that JTedi can be closed between scroll's while looking at invoices. Axacropdf
    ''' does funny things if panels are refreshed without clsing the class. Coming to this sub, closing JTedi, etc.
    ''' seems to do the trick.
    ''' </summary>
    Public Sub EDINavigationSub()
        If JTediFileTBDeleted <> Nothing Then
            If My.Computer.FileSystem.FileExists(JTediFileTBDeleted) Then
                '
                Try
                    My.Computer.FileSystem.DeleteFile(JTediFileTBDeleted)
                Catch
                    MsgBox("EDI File Delete Failed - JTstart~884" & vbCrLf &
                           JTediFileTBDeleted)
                End Try
                '
            End If
            JTediInvDispCount = 0
            JTediFileTBDeleted = Nothing
        End If
        '
        JTedi.JTediTopInvNum = JTediInvDispCount
        JTedi.JTediFillInvImages()
    End Sub

    Private Sub NonLaborEntryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NonLaborEntryToolStripMenuItem.Click
        LogFileWrite("JTStart", "JTnlcM session Started")
        JTMainMenu.Hide()
        JTnlcM.MdiParent = Me
        JTnlcM.JTnlcMinit()

    End Sub

    Private Sub JobPurgeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JobPurgeToolStripMenuItem.Click
        JTye.JtyeJobPurge()
    End Sub



    '
End Class