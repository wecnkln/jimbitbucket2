﻿Imports System.IO
Public Class JTis
    ''' <summary>
    ''' Switch is used to determine variable differences in JTis session ... production mode 
    ''' (queued from NLC) vs. Maint. Mode (queued from JTstart menu).
    ''' </summary>
    Public JTisMaintMode As Boolean = False
    Public Structure JTisStructure
        Dim JTissCategory As String
        Dim JTissItem As String
        Dim JTissSellUnit As String
        Dim JTissUnitCost As String
        Dim JTissPrVerSrce As String
        Dim JTissPrVerInv As String
        Dim JTissUnitToJob As String
    End Structure
    '
    Public JTisItem As New JTisStructure
    '
    Public slJTisData As New SortedList
    '
    Dim JTisJobID As String = Nothing
    Dim JTisSubID As String = Nothing
    Dim JTisEntDate As String = Nothing
    ''' <summary>
    ''' Entry point into InStock Processing. Make a few changes to the panel if in maintenance mode.
    ''' </summary>
    Sub JTisNLCRequest(ByVal tmpJobID As String, ByVal tmpSubID As String, ByVal tmpEntDate As String)
        JTisJobID = tmpJobID
        JTisSubID = tmpSubID
        JTisEntDate = tmpEntDate
        '
        ' Read JTisLibrary.dat file
        JTisInit()
        ' Load LV to select, add, change or delete i.s. items
        JTisLoadLV()
        If JTisMaintMode = True Then
            Label8.Visible = False
            Label2.Visible = False
            txtJTisChgToJob.Visible = False
            txtJTisChgToJobDol.Visible = False
        End If
        ' display panel
        Me.MdiParent = JTstart
        Me.Show()

    End Sub
    ''' <summary>
    ''' Loads values from JTisLibrary.dat file into sl for IS processing. Data for IS
    ''' is not maintained in memory between sessions ... it is restored each time a JTis 
    ''' request is made.
    ''' </summary>
    Sub JTisInit()
        ' ---------------------------------------------------------------------------------
        ' Contains code to read the JTisLibrary.dat file and loads the SortedList slJTisData.
        ' ---------------------------------------------------------------------------------
        slJTisData.Clear()
        '
        Dim fileRecord As String
        ' doesn't seem to be used - - -      Dim tmpChar As String
        Dim tmpLoc As Integer
        Dim tmpLocKey As Integer
        Dim JTisReadFileKey As String
        Dim JTisReadFileData As String
        '
        Dim tmpISR As New JTisStructure
        '
        'not used - - -     Dim tmpRecordType As String
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTisLibrary.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTisLibrary.Bkup") Then
            MsgBox("JTisLibrary --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (IS.009)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTisLibrary.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTisLibrary.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTisLibrary.Bkup", "JTisLibrary.dat")
        End If
        ' ------------------------------------------------------------------
        Dim tmpRecNumber As Short = 0
        '
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            '      MsgBox("IS Debugging - got to 85")
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTisReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTisReadFileKey = "isRecord" Or JTisReadFileKey = "/isRecord" Then
                        JTisReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTisReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case JTisReadFileKey
                        '
                        Case "isCategory"
                            tmpISR.JTissCategory = JTisReadFileData
                            '
                            ' clear fields that might not have data
                            tmpISR.JTissPrVerSrce = ""
                            tmpISR.JTissPrVerInv = ""
                            tmpISR.JTissUnitToJob = ""
                        Case "isITem"
                            tmpISR.JTissItem = JTisReadFileData
                        Case "isSellUnit"
                            tmpISR.JTissSellUnit = JTisReadFileData
                        Case "isCstPerUnt"
                            tmpISR.JTissUnitCost = JTisReadFileData
                        Case "isPrVerSrc"
                            tmpISR.JTissPrVerSrce = JTisReadFileData
                        Case "isPrVerInv"
                            tmpISR.JTissPrVerInv = JTisReadFileData

                        '
                        Case "/isRecord"
                            '
                            slJTisData.Add(tmpISR.JTissCategory & tmpISR.JTissItem, tmpISR)
                            '

                            '
                    End Select
                    '
                End If
                '              MsgBox("IS Debugging - got to 146")
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        MsgBox("IS Debugging - got to 153")
    End Sub
    ''' <summary>
    ''' Saves data from current JTis session to disk (JTisLibrary.dat)
    ''' </summary>
    Sub JTisFlush()
        ' -------------------------------------------------------------------------------------
        ' This sub takes the updated data in the JTisData SL and writes them to JTisLibrary.dat.
        ' IT only writes the file if new items have been added or existing items modified
        ' during the current session.
        ' --------------------------------------------------------------------------------------
        Dim fileRecord As String
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTisLibrary.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTisLibrary.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            '

            ' Get a collection of the keys. 
            Dim key As ICollection = slJTisData.Keys
            Dim k As String
            For Each k In key
                '
                fileRecord = "<isRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<isCategory>" & slJTisData(k).JTissCategory & "</isCategory>"
                writeFile.WriteLine(fileRecord)
                '
                Dim tmpissItem = slJTisData(k).JTissItem.Replace(vbCr, "").Replace(vbLf, "")
                fileRecord = "<isITem>" & tmpissItem & "</isItem>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<isSellUnit>" & slJTisData(k).JTissSellUnit & "</isSellUnit>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<isCstPerUnt>" & slJTisData(k).JTissUnitCost & "</isCstPerUnt>"
                writeFile.WriteLine(fileRecord)
                '
                If slJTisData(k).JTissPrVerSrce <> "" Then
                    fileRecord = "<isPrVerSrc>" & slJTisData(k).JTissPrVerSrce & "</isPrVerSrc>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTisData(k).JTissPrVerInv <> "" Then
                    fileRecord = "<isPrVerInv>" & slJTisData(k).JTissPrVerInv & "</isPrVerInv>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "</isRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try

        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTisLibrary.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTisLibrary.Bkup")
        End If
        ' ------------------------------------------------------------------------

    End Sub
    ''' <summary>
    ''' Loaded data from sl onto lv on JTis panel
    ''' </summary>
    Sub JTisLoadLV()
        ' -------------------------------------------------------------------------
        ' Load and reformat data into the LV
        ' -------------------------------------------------------------------------
        lvJTisData.Items.Clear()
        lvJTisData.Enabled = True
        '
        Dim tmpLastCategory As String = Nothing
        '
        Dim keys As ICollection = slJTisData.Keys
        Dim k As String
        '
        Dim tmpLVLoopSW As String = "F"
        Dim itmBuild(8) As String
        Dim lvLine As ListViewItem
        '
        Do While tmpLVLoopSW <> Nothing
            For Each k In keys
                ' --------------------------------------------------------------------------------------
                ' move items to listview
                '
                ' tmpLVLoopSw logic makes it so "INHOUSE" items are display last in the LV.
                ' --------------------------------------------------------------------------------------
                If (tmpLVLoopSW = "F" And slJTisData(k).JTissCategory <> "INHOUSE") Or
                   (tmpLVLoopSW = "L" And slJTisData(k).JTissCategory = "INHOUSE") Then
                    If slJTisData(k).JTissCategory <> tmpLastCategory Then
                        itmBuild(0) = slJTisData(k).JTissCategory
                        tmpLastCategory = slJTisData(k).JTissCategory
                    Else
                        itmBuild(0) = ""
                    End If
                    itmBuild(1) = slJTisData(k).JTissItem
                    itmBuild(2) = slJTisData(k).JTissSellUnit
                    itmBuild(3) = String.Format("{0:0.00}", slJTisData(k).JTissUnitCost)
                    itmBuild(4) = slJTisData(k).JTissPrVerSrce
                    itmBuild(5) = slJTisData(k).JTissPrVerInv
                    itmBuild(6) = String.Format("{0:0.0}", slJTisData(k).JTissUnitToJob)
                    itmBuild(7) = k
                    lvLine = New ListViewItem(itmBuild)
                    lvJTisData.Items.Add(lvLine)
                End If
                '
            Next k
            If tmpLVLoopSW = "F" Then
                tmpLVLoopSW = "L"
            Else
                tmpLVLoopSW = Nothing
            End If
        Loop
        '
    End Sub
    ''' <summary>
    ''' Handles cancel button ... cleaned everything up and, if in maint mode, resets screen fields.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSessionCancel_Click(sender As Object, e As EventArgs) Handles btnSessionCancel.Click
        MsgBox("All items selected in the session have been cancelled. Nothing new will be charged to the job.(IS.001)")
        ' Clear fields on NLC screen
        Me.Hide()
        slJTisData.Clear()
        lvJTisData.Items.Clear()

        JTisMaintMode = False
        JTnlcM.JTnlcMinit()
        '
        Me.Close()
        '
    End Sub
    ''' <summary>
    ''' Handles JTis session complete button.
    '''   - If maint mode, JtisLibrary is updated and panel fields are reset.
    '''   - If in normal mode in addition to saving data, any item chg's to job are transferred to current NLC panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSessionComplete_Click(sender As Object, e As EventArgs) Handles btnSessionComplete.Click
        ' Save JTisLibrary.dat file to disk
        JTisFlush()
        If JTisMaintMode = False Then
            ' ----------------------------------------------------------------------------------------
            ' Call sub to update nlc items that have been selected and then clean up and reset fields.
            ' ----------------------------------------------------------------------------------------
            JTnlc.JTnlcISItemUpdt(JTisJobID, JTisSubID, JTisEntDate)
            JTnlcE.JtnlcEInit("ISRETURN", "")
        Else
            '          Label8.Visible = True
            '         Label2.Visible = True
            '        txtJTisChgToJob.Visible = True
            '       txtJTisChgToJobDol.Visible = True
            JTisMaintMode = False
            JTnlcM.JTnlcMinit()
        End If
        '
        slJTisData.Clear()
        lvJTisData.Items.Clear()
        '
        Me.Close()
    End Sub
    Private Sub txtJTisCategory_LostFocus(sender As Object, e As EventArgs) Handles txtJTisCategory.LostFocus
        ' ------------------------------------------------------------
        ' Verify that a category has been entered.
        ' ------------------------------------------------------------
        If txtJTisCategory.Text = "" Then
            ToolTip1.Show("In Stock Item must have a category ... Correct and resubmit.(IS.003)", txtJTisCategory, 80, 10, 7000)
            txtJTisCategory.Select()
        End If
        txtJTisCategory.Text = txtJTisCategory.Text.ToUpper
        If txtJTisCategory.Text = "INHOUSE" Then
            txtJTisPrVerSrce.ReadOnly = True
            txtJTisPrVerInv.ReadOnly = True
        Else
            txtJTisPrVerSrce.ReadOnly = False
            txtJTisPrVerInv.ReadOnly = False
        End If
    End Sub
    Private Sub txtJTisItem_LostFocus(sender As Object, e As EventArgs) Handles txtJTisItem.LostFocus
        If txtJTisItem.Text = "" Then
            ToolTip1.Show("In Stock Item must have a description ... Correct and resubmit.(IS.004)", txtJTisItem, 80, 10, 7000)
            txtJTisCategory.Select()
        End If
    End Sub
    Private Sub txtJTisSellUnit_LostFocus(sender As Object, e As EventArgs) Handles txtJTisSellUnit.LostFocus
        If txtJTisSellUnit.Text = "" Then
            ToolTip1.Show("In Stock Item must have a unit of measure (Ea.,Pr.,BF, LF, etc.) ... Correct and resubmit.(IS.005)", txtJTisSellUnit, 80, 10, 7000)
            txtJTisCategory.Select()
        End If
    End Sub
    Private Sub txtJTisUnitCost_LostFocus(sender As Object, e As EventArgs) Handles txtJTisUnitCost.LostFocus
        If txtJTisUnitCost.Text = "" Then
            ToolTip1.Show("No I.S. Cost entered ... Correct and resubmit.(IS.006)", txtJTisUnitCost, 80, 10, 7000)
            txtJTisCategory.Select()
        Else
            txtJTisUnitCost.Text = String.Format("{0:0.00}", Val(txtJTisUnitCost.Text))
        End If
    End Sub
    Private Sub txtJTisPrVerSrce_LostFocus(sender As Object, e As EventArgs) Handles txtJTisPrVerSrce.LostFocus
        If txtJTisPrVerSrce.Text = "" Then
            ToolTip1.Show("No In Stock price verify source entered ... Correct and resubmit.(IS.007)", txtJTisPrVerSrce, 80, 10, 7000)
            txtJTisCategory.Select()
        End If
    End Sub
    Private Sub txtJTisPrVerInv_LostFocus(sender As Object, e As EventArgs) Handles txtJTisPrVerInv.LostFocus
        If txtJTisPrVerInv.Text = "" Then
            ToolTip1.Show("No In Stock price verify source invoice # entered ... Correct and resubmit.(IS.008)", txtJTisPrVerInv, 80, 10, 7000)
            txtJTisCategory.Select()
        End If

    End Sub
    Private Sub txtJTisChgToJob_LostFocus(sender As Object, e As EventArgs) Handles txtJTisChgToJob.LostFocus
        txtJTisChgToJobDol.Text = String.Format("{0:0.00}", Val(txtJTisChgToJob.Text) * Val(txtJTisUnitCost.Text))
    End Sub
    Private Sub btnAddUpdate_Click(sender As Object, e As EventArgs) Handles btnAddUpdate.Click
        ' -----------------------------------------------------
        ' Accept and update lv with data entered or updated.
        ' -----------------------------------------------------
        If txtJTisCategory.Text.ToUpper <> "INHOUSE" Then
            If txtJTisPrVerSrce.Text = "" Then
                ToolTip1.Show("In Stock Item must have a SOURCE ... Correct and resubmit.(IS.003)", txtJTisCategory, 80, 10, 7000)
                Exit Sub
            End If
        End If
        JTisItem.JTissCategory = txtJTisCategory.Text
        JTisItem.JTissItem = txtJTisItem.Text
        JTisItem.JTissSellUnit = txtJTisSellUnit.Text
        JTisItem.JTissUnitCost = txtJTisUnitCost.Text
        JTisItem.JTissPrVerSrce = txtJTisPrVerSrce.Text
        JTisItem.JTissPrVerInv = txtJTisPrVerInv.Text
        JTisItem.JTissUnitToJob = txtJTisChgToJob.Text
        If slJTisData.ContainsKey(JTisItem.JTissCategory & JTisItem.JTissItem) Then
            slJTisData.Remove(JTisItem.JTissCategory & JTisItem.JTissItem)
        End If
        slJTisData.Add(JTisItem.JTissCategory & JTisItem.JTissItem, JTisItem)
        '
        txtJTisCategory.Text = ""
        txtJTisItem.Text = ""
        txtJTisSellUnit.Text = ""
        txtJTisUnitCost.Text = ""
        txtJTisPrVerSrce.Text = ""
        txtJTisPrVerInv.Text = ""
        txtJTisChgToJob.Text = ""
        txtJTisChgToJobDol.Text = ""
        Me.Hide()
        JTisLoadLV()
        Me.Show()

    End Sub
    Private Sub lvJTisData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTisData.SelectedIndexChanged
        Dim k As String = lvJTisData.SelectedItems(0).SubItems(7).Text
        txtJTisCategory.Text = slJTisData(k).JTissCategory
        txtJTisItem.Text = slJTisData(k).JTissItem
        txtJTisSellUnit.Text = slJTisData(k).JTissSellUnit
        txtJTisUnitCost.Text = slJTisData(k).JTissUnitCost
        txtJTisPrVerSrce.Text = slJTisData(k).JTissPrVerSrce
        txtJTisPrVerInv.Text = slJTisData(k).JTissPrVerInv
        txtJTisChgToJob.Text = slJTisData(k).JTissUnitToJob
        '
        lvJTisData.Enabled = False
        Me.txtJTisChgToJob.Select()
        '
    End Sub

    Private Sub btnDeleteItem_Click(sender As Object, e As EventArgs) Handles btnDeleteItem.Click
        ' -----------------------------------------------------
        ' Delete the highlighted item permanently from the I.S. library.
        ' -----------------------------------------------------
        JTisItem.JTissCategory = txtJTisCategory.Text
        JTisItem.JTissItem = txtJTisItem.Text

        If slJTisData.ContainsKey(JTisItem.JTissCategory & JTisItem.JTissItem) Then
            slJTisData.Remove(JTisItem.JTissCategory & JTisItem.JTissItem)
        End If

        '
        txtJTisCategory.Text = ""
        txtJTisItem.Text = ""
        txtJTisSellUnit.Text = ""
        txtJTisUnitCost.Text = ""
        txtJTisPrVerSrce.Text = ""
        txtJTisPrVerInv.Text = ""
        txtJTisChgToJob.Text = ""
        txtJTisChgToJobDol.Text = ""
        Me.Hide()
        JTisLoadLV()
        Me.Show()

    End Sub
End Class

