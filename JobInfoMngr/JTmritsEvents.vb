﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Public Class JTmritsEvents
    Inherits PdfPageEventHelper
    '   Dim Header
    Dim Total As PdfTemplate

    Public Overrides Sub onOpenDocument(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
        Total = writer.DirectContent().CreateTemplate(30, 12)
    End Sub



    '     Total = writer.getDirectContent().createTemplate(30, 16)
    Public Overrides Sub onEndPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
        '
        Dim cb As PdfContentByte = writer.DirectContent()
        '
        Dim table As New PdfPTable(4)
        ' Actual width of table in points
        table.TotalWidth = document.Right - document.Left
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {4.0F, 1.0F, 1.0F, 4.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 0.0F
        table.SpacingAfter = 0.0F
        '
        '
        Dim PageNumber As String = "Page  " & writer.PageNumber & "  of   "
        iTSFillCell("", table, 1, 1, 0, "")
        iTSFillCell(PageNumber, table, 2, 1, 0, "")
        Dim CellContents = New PdfPCell(Image.GetInstance(Total))
        CellContents.Border = 0
        CellContents.HorizontalAlignment = 0
        CellContents.VerticalAlignment = 0
        table.AddCell(CellContents)
        iTSFillCell("", table, 1, 1, 0, "")
        '
        table.WriteSelectedRows(0, -1, document.LeftMargin, document.BottomMargin, writer.DirectContent)


        Dim CU As PdfContentByte = writer.DirectContentUnder()
        Dim tmpDocWidth As Integer = (document.Right - document.Left)
        '
        If JTVarMaint.JTvmDemoBypass <> "DexterCoventry" Then
            If tmpDocWidth < 600 Then
                ' Portrait
                '
                '   Following commented code put logo on demo reports. Not completely working.
                '      Dim WHTLogo As iTextSharp.text.Image
                '      WHTLogo = iTextSharp.text.Image.GetInstance(JTVarMaint.JTvmFilePath & "logo\" & "WHTLogo.jpg")
                '      WHTLogo.SetAbsolutePosition(100, 500)
                '      document.Add(WHTLogo)
                '
                ColumnText.ShowTextAligned(CU, Element.ALIGN_CENTER, New Phrase(FP_H52BG("DEMO SOFTWARE")), 297, 375, 45)
            Else
                ' Landscape
                ColumnText.ShowTextAligned(CU, Element.ALIGN_CENTER, New Phrase(FP_H52BG("DEMO SOFTWARE")), 430, 250, 45)
            End If
        End If
        '
    End Sub

    Public Overrides Sub onCloseDocument(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
        '
        ColumnText.ShowTextAligned(Total, Element.ALIGN_LEFT, New Phrase(FP_H8(Str(writer.PageNumber))), 2, 2, 0)
        '
    End Sub
End Class

