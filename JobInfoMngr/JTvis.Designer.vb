﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTvis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTvis))
        Me.lvVISvendors = New System.Windows.Forms.ListView()
        Me.JTvisShortName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTvisLongName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTvisVendCat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTvisNewVendor = New System.Windows.Forms.RadioButton()
        Me.cboxJTvisEdit = New System.Windows.Forms.CheckBox()
        Me.cboxJTvisShowAll = New System.Windows.Forms.CheckBox()
        Me.btnJTvisClosePanel = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvVISvendors
        '
        Me.lvVISvendors.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.JTvisShortName, Me.JTvisLongName, Me.JTvisVendCat})
        Me.lvVISvendors.FullRowSelect = True
        Me.lvVISvendors.HideSelection = False
        Me.lvVISvendors.Location = New System.Drawing.Point(13, 46)
        Me.lvVISvendors.Name = "lvVISvendors"
        Me.lvVISvendors.Size = New System.Drawing.Size(537, 362)
        Me.lvVISvendors.TabIndex = 0
        Me.lvVISvendors.UseCompatibleStateImageBehavior = False
        Me.lvVISvendors.View = System.Windows.Forms.View.Details
        '
        'JTvisShortName
        '
        Me.JTvisShortName.Text = "JobTrackingMain"
        Me.JTvisShortName.Width = 150
        '
        'JTvisLongName
        '
        Me.JTvisLongName.Text = "Vendor Name"
        Me.JTvisLongName.Width = 250
        '
        'JTvisVendCat
        '
        Me.JTvisVendCat.Text = "Category"
        Me.JTvisVendCat.Width = 120
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(13, 421)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(537, 22)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "--- Double Click desired line to edit entry or extract to NLC entry."
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox1.Controls.Add(Me.rbtnJTvisNewVendor)
        Me.GroupBox1.Controls.Add(Me.cboxJTvisEdit)
        Me.GroupBox1.Controls.Add(Me.cboxJTvisShowAll)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(566, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(206, 108)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Options"
        '
        'rbtnJTvisNewVendor
        '
        Me.rbtnJTvisNewVendor.AutoSize = True
        Me.rbtnJTvisNewVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTvisNewVendor.Location = New System.Drawing.Point(7, 78)
        Me.rbtnJTvisNewVendor.Name = "rbtnJTvisNewVendor"
        Me.rbtnJTvisNewVendor.Size = New System.Drawing.Size(135, 21)
        Me.rbtnJTvisNewVendor.TabIndex = 2
        Me.rbtnJTvisNewVendor.TabStop = True
        Me.rbtnJTvisNewVendor.Text = "Add New Vendor"
        Me.rbtnJTvisNewVendor.UseVisualStyleBackColor = True
        '
        'cboxJTvisEdit
        '
        Me.cboxJTvisEdit.AutoSize = True
        Me.cboxJTvisEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTvisEdit.Location = New System.Drawing.Point(7, 51)
        Me.cboxJTvisEdit.Name = "cboxJTvisEdit"
        Me.cboxJTvisEdit.Size = New System.Drawing.Size(185, 21)
        Me.cboxJTvisEdit.TabIndex = 1
        Me.cboxJTvisEdit.Text = "Edit - Chk, then Dbl Click"
        Me.cboxJTvisEdit.UseVisualStyleBackColor = True
        '
        'cboxJTvisShowAll
        '
        Me.cboxJTvisShowAll.AutoSize = True
        Me.cboxJTvisShowAll.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTvisShowAll.Location = New System.Drawing.Point(7, 24)
        Me.cboxJTvisShowAll.Name = "cboxJTvisShowAll"
        Me.cboxJTvisShowAll.Size = New System.Drawing.Size(152, 21)
        Me.cboxJTvisShowAll.TabIndex = 0
        Me.cboxJTvisShowAll.Text = "Show all categories"
        Me.cboxJTvisShowAll.UseVisualStyleBackColor = True
        '
        'btnJTvisClosePanel
        '
        Me.btnJTvisClosePanel.Location = New System.Drawing.Point(608, 176)
        Me.btnJTvisClosePanel.Name = "btnJTvisClosePanel"
        Me.btnJTvisClosePanel.Size = New System.Drawing.Size(117, 37)
        Me.btnJTvisClosePanel.TabIndex = 3
        Me.btnJTvisClosePanel.Text = "Close Panel"
        Me.btnJTvisClosePanel.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TextBox2.Location = New System.Drawing.Point(566, 221)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(206, 222)
        Me.TextBox2.TabIndex = 4
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "NLC - Vendor Search"
        '
        'JTvis
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(788, 462)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.btnJTvisClosePanel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lvVISvendors)
        Me.Name = "JTvis"
        Me.Text = "Job Information Manager - Vendor Search"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lvVISvendors As ListView
    Friend WithEvents JTvisShortName As ColumnHeader
    Friend WithEvents JTvisLongName As ColumnHeader
    Friend WithEvents JTvisVendCat As ColumnHeader
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cboxJTvisShowAll As CheckBox
    Friend WithEvents cboxJTvisEdit As CheckBox
    Friend WithEvents btnJTvisClosePanel As Button
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents rbtnJTvisNewVendor As RadioButton
End Class
