﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTjim
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtJTjimJob = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTjimSubJob = New System.Windows.Forms.TextBox()
        Me.txtJTjimJobDesc = New System.Windows.Forms.TextBox()
        Me.txtJTjimJobDet = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtJtjimCreateDate = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.chkboxJTjimBillAddSwitch = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtJTjimJobLocStreet = New System.Windows.Forms.TextBox()
        Me.txtJTjimJobLocCity = New System.Windows.Forms.TextBox()
        Me.txtJTjimJobLocState = New System.Windows.Forms.TextBox()
        Me.txtJTjimBillCity = New System.Windows.Forms.TextBox()
        Me.txtJTjimBillStreet = New System.Windows.Forms.TextBox()
        Me.txtJTjimContact = New System.Windows.Forms.TextBox()
        Me.txtJTjimTel = New System.Windows.Forms.TextBox()
        Me.txtJTjimEmail = New System.Windows.Forms.TextBox()
        Me.txtJTjimAltContact = New System.Windows.Forms.TextBox()
        Me.txtJTjimAltTel = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblJTjimJobLocStateNme = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ChkBoxInvAlt = New System.Windows.Forms.CheckBox()
        Me.ChkBoxInvPrim = New System.Windows.Forms.CheckBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtJTjimAltEmail = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtJTjimMatMu = New System.Windows.Forms.TextBox()
        Me.txtJTjimSubMu = New System.Windows.Forms.TextBox()
        Me.chkboxJTjimJobSpecRates = New System.Windows.Forms.CheckBox()
        Me.lvJTjimLabRates = New System.Windows.Forms.ListView()
        Me.lvJTjimLabCat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chkboxJTjimInvPDF = New System.Windows.Forms.CheckBox()
        Me.chkboxJTjimInvUSPS = New System.Windows.Forms.CheckBox()
        Me.chkboxJTjimInvFile = New System.Windows.Forms.CheckBox()
        Me.lvJTjimInvHist = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTjimCurAct = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnDone = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtErrMsg = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLCat = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLStan = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLshop = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLtext1 = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLtext3 = New System.Windows.Forms.TextBox()
        Me.txtJTjimJSLtext2 = New System.Windows.Forms.TextBox()
        Me.chkboxJTjimJSLclone = New System.Windows.Forms.CheckBox()
        Me.txtJTjimJSLerrmsg = New System.Windows.Forms.TextBox()
        Me.btnJTjimJSLupdate = New System.Windows.Forms.Button()
        Me.btnJTjimJSLcancel = New System.Windows.Forms.Button()
        Me.txtJTjimLastActDate = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.chkboxJTjimSndToAcct = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cboxJTjimTaxExempt = New System.Windows.Forms.CheckBox()
        Me.chkboxJTjimNonStdFrmt = New System.Windows.Forms.CheckBox()
        Me.txtJTjimAUPInvdTD = New System.Windows.Forms.TextBox()
        Me.txtJTjimAUPNetRemain = New System.Windows.Forms.TextBox()
        Me.txtJTjimAUPQuoted = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtJTjimAUPQueueAmt = New System.Windows.Forms.TextBox()
        Me.radiobtnJTjimCstPlus = New System.Windows.Forms.RadioButton()
        Me.radiobtnJTjimAgreedUpon = New System.Windows.Forms.RadioButton()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.radiobtnJTjimCstPlusDeposits = New System.Windows.Forms.RadioButton()
        Me.gboxJTjimAUP = New System.Windows.Forms.GroupBox()
        Me.gboxJTjimAUPIncludes = New System.Windows.Forms.GroupBox()
        Me.btnJTjimAUPJob = New System.Windows.Forms.Button()
        Me.cboxJTjimAUPLabor = New System.Windows.Forms.CheckBox()
        Me.cboxJTjimAUPServices = New System.Windows.Forms.CheckBox()
        Me.cboxJTjimAUPMat = New System.Windows.Forms.CheckBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.gboxJTjimJSLframe = New System.Windows.Forms.GroupBox()
        Me.txtJTjimDeactDate = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.labDeactDate = New System.Windows.Forms.Label()
        Me.gboxJTjimCPWD = New System.Windows.Forms.GroupBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtJTjimCPWDUnbilldAct = New System.Windows.Forms.TextBox()
        Me.txtJTjimCPWDSchedDep = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtJTjimCPWDInvdDep = New System.Windows.Forms.TextBox()
        Me.txtJTjimCPWDInvdAct = New System.Windows.Forms.TextBox()
        Me.txtJTjimCPWDDepBal = New System.Windows.Forms.TextBox()
        Me.txtJTjimSJPRevDate = New System.Windows.Forms.TextBox()
        Me.lblJTjimPrcgRev = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.gboxJTjimAUP.SuspendLayout()
        Me.gboxJTjimAUPIncludes.SuspendLayout()
        Me.gboxJTjimJSLframe.SuspendLayout()
        Me.gboxJTjimCPWD.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Customer"
        '
        'txtJTjimJob
        '
        Me.txtJTjimJob.Location = New System.Drawing.Point(92, 5)
        Me.txtJTjimJob.Name = "txtJTjimJob"
        Me.txtJTjimJob.ReadOnly = True
        Me.txtJTjimJob.Size = New System.Drawing.Size(109, 23)
        Me.txtJTjimJob.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Job"
        '
        'txtJTjimSubJob
        '
        Me.txtJTjimSubJob.Location = New System.Drawing.Point(244, 4)
        Me.txtJTjimSubJob.Name = "txtJTjimSubJob"
        Me.txtJTjimSubJob.ReadOnly = True
        Me.txtJTjimSubJob.Size = New System.Drawing.Size(124, 23)
        Me.txtJTjimSubJob.TabIndex = 3
        '
        'txtJTjimJobDesc
        '
        Me.txtJTjimJobDesc.Location = New System.Drawing.Point(188, 33)
        Me.txtJTjimJobDesc.Name = "txtJTjimJobDesc"
        Me.txtJTjimJobDesc.Size = New System.Drawing.Size(224, 23)
        Me.txtJTjimJobDesc.TabIndex = 5
        '
        'txtJTjimJobDet
        '
        Me.txtJTjimJobDet.Location = New System.Drawing.Point(187, 58)
        Me.txtJTjimJobDet.Multiline = True
        Me.txtJTjimJobDet.Name = "txtJTjimJobDet"
        Me.txtJTjimJobDet.Size = New System.Drawing.Size(329, 59)
        Me.txtJTjimJobDet.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 479)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 17)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Job Creationed"
        '
        'txtJtjimCreateDate
        '
        Me.txtJtjimCreateDate.Location = New System.Drawing.Point(18, 498)
        Me.txtJtjimCreateDate.Name = "txtJtjimCreateDate"
        Me.txtJtjimCreateDate.ReadOnly = True
        Me.txtJtjimCreateDate.Size = New System.Drawing.Size(70, 23)
        Me.txtJtjimCreateDate.TabIndex = 14
        Me.txtJtjimCreateDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(96, 16)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Street Address"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(11, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "City, State, Zip"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(11, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(121, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Work Performed in:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(7, 48)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 16)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "City, State, Zip"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(7, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 16)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Street Address"
        '
        'chkboxJTjimBillAddSwitch
        '
        Me.chkboxJTjimBillAddSwitch.AutoSize = True
        Me.chkboxJTjimBillAddSwitch.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimBillAddSwitch.Location = New System.Drawing.Point(134, 4)
        Me.chkboxJTjimBillAddSwitch.Name = "chkboxJTjimBillAddSwitch"
        Me.chkboxJTjimBillAddSwitch.Size = New System.Drawing.Size(162, 20)
        Me.chkboxJTjimBillAddSwitch.TabIndex = 20
        Me.chkboxJTjimBillAddSwitch.Text = "Same as Job location."
        Me.chkboxJTjimBillAddSwitch.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(4, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(124, 17)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Contact/Invoice to:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(2, 38)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 17)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Telephone"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(2, 65)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(98, 17)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Email Address"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(2, 91)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 17)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "Alt. Contact"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(4, 116)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(102, 17)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "Alt.Cont.Phone"
        '
        'txtJTjimJobLocStreet
        '
        Me.txtJTjimJobLocStreet.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimJobLocStreet.Location = New System.Drawing.Point(134, 22)
        Me.txtJTjimJobLocStreet.Name = "txtJTjimJobLocStreet"
        Me.txtJTjimJobLocStreet.Size = New System.Drawing.Size(208, 22)
        Me.txtJTjimJobLocStreet.TabIndex = 26
        '
        'txtJTjimJobLocCity
        '
        Me.txtJTjimJobLocCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimJobLocCity.Location = New System.Drawing.Point(134, 47)
        Me.txtJTjimJobLocCity.Name = "txtJTjimJobLocCity"
        Me.txtJTjimJobLocCity.Size = New System.Drawing.Size(208, 22)
        Me.txtJTjimJobLocCity.TabIndex = 27
        '
        'txtJTjimJobLocState
        '
        Me.txtJTjimJobLocState.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimJobLocState.Location = New System.Drawing.Point(148, 74)
        Me.txtJTjimJobLocState.Name = "txtJTjimJobLocState"
        Me.txtJTjimJobLocState.Size = New System.Drawing.Size(35, 22)
        Me.txtJTjimJobLocState.TabIndex = 28
        '
        'txtJTjimBillCity
        '
        Me.txtJTjimBillCity.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimBillCity.Location = New System.Drawing.Point(134, 50)
        Me.txtJTjimBillCity.Name = "txtJTjimBillCity"
        Me.txtJTjimBillCity.Size = New System.Drawing.Size(208, 22)
        Me.txtJTjimBillCity.TabIndex = 30
        '
        'txtJTjimBillStreet
        '
        Me.txtJTjimBillStreet.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimBillStreet.Location = New System.Drawing.Point(134, 25)
        Me.txtJTjimBillStreet.Name = "txtJTjimBillStreet"
        Me.txtJTjimBillStreet.Size = New System.Drawing.Size(208, 22)
        Me.txtJTjimBillStreet.TabIndex = 29
        '
        'txtJTjimContact
        '
        Me.txtJTjimContact.Location = New System.Drawing.Point(132, 9)
        Me.txtJTjimContact.Name = "txtJTjimContact"
        Me.txtJTjimContact.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimContact.TabIndex = 31
        '
        'txtJTjimTel
        '
        Me.txtJTjimTel.Location = New System.Drawing.Point(132, 35)
        Me.txtJTjimTel.Name = "txtJTjimTel"
        Me.txtJTjimTel.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimTel.TabIndex = 32
        '
        'txtJTjimEmail
        '
        Me.txtJTjimEmail.Location = New System.Drawing.Point(132, 62)
        Me.txtJTjimEmail.Name = "txtJTjimEmail"
        Me.txtJTjimEmail.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimEmail.TabIndex = 33
        '
        'txtJTjimAltContact
        '
        Me.txtJTjimAltContact.Location = New System.Drawing.Point(132, 88)
        Me.txtJTjimAltContact.Name = "txtJTjimAltContact"
        Me.txtJTjimAltContact.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimAltContact.TabIndex = 34
        '
        'txtJTjimAltTel
        '
        Me.txtJTjimAltTel.Location = New System.Drawing.Point(132, 113)
        Me.txtJTjimAltTel.Name = "txtJTjimAltTel"
        Me.txtJTjimAltTel.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimAltTel.TabIndex = 35
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.lblJTjimJobLocStateNme)
        Me.GroupBox2.Controls.Add(Me.txtJTjimJobLocState)
        Me.GroupBox2.Controls.Add(Me.txtJTjimJobLocCity)
        Me.GroupBox2.Controls.Add(Me.txtJTjimJobLocStreet)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(548, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(349, 106)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Job Location"
        '
        'lblJTjimJobLocStateNme
        '
        Me.lblJTjimJobLocStateNme.AutoSize = True
        Me.lblJTjimJobLocStateNme.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJTjimJobLocStateNme.Location = New System.Drawing.Point(188, 77)
        Me.lblJTjimJobLocStateNme.Name = "lblJTjimJobLocStateNme"
        Me.lblJTjimJobLocStateNme.Size = New System.Drawing.Size(0, 16)
        Me.lblJTjimJobLocStateNme.TabIndex = 29
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.txtJTjimBillCity)
        Me.GroupBox3.Controls.Add(Me.txtJTjimBillStreet)
        Me.GroupBox3.Controls.Add(Me.chkboxJTjimBillAddSwitch)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(550, 123)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(349, 89)
        Me.GroupBox3.TabIndex = 37
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Billing Address"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.ChkBoxInvAlt)
        Me.GroupBox4.Controls.Add(Me.ChkBoxInvPrim)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.txtJTjimAltEmail)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.txtJTjimAltContact)
        Me.GroupBox4.Controls.Add(Me.txtJTjimAltTel)
        Me.GroupBox4.Controls.Add(Me.txtJTjimEmail)
        Me.GroupBox4.Controls.Add(Me.txtJTjimTel)
        Me.GroupBox4.Controls.Add(Me.txtJTjimContact)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Location = New System.Drawing.Point(550, 219)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(347, 198)
        Me.GroupBox4.TabIndex = 38
        Me.GroupBox4.TabStop = False
        '
        'ChkBoxInvAlt
        '
        Me.ChkBoxInvAlt.AutoSize = True
        Me.ChkBoxInvAlt.Location = New System.Drawing.Point(262, 168)
        Me.ChkBoxInvAlt.Name = "ChkBoxInvAlt"
        Me.ChkBoxInvAlt.Size = New System.Drawing.Size(46, 21)
        Me.ChkBoxInvAlt.TabIndex = 40
        Me.ChkBoxInvAlt.Text = "Alt"
        Me.ChkBoxInvAlt.UseVisualStyleBackColor = True
        '
        'ChkBoxInvPrim
        '
        Me.ChkBoxInvPrim.AutoSize = True
        Me.ChkBoxInvPrim.Location = New System.Drawing.Point(175, 168)
        Me.ChkBoxInvPrim.Name = "ChkBoxInvPrim"
        Me.ChkBoxInvPrim.Size = New System.Drawing.Size(58, 21)
        Me.ChkBoxInvPrim.TabIndex = 39
        Me.ChkBoxInvPrim.Text = "Prim"
        Me.ChkBoxInvPrim.UseVisualStyleBackColor = True
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(7, 169)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(116, 17)
        Me.Label30.TabIndex = 38
        Me.Label30.Text = "Email Inv. To --->" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtJTjimAltEmail
        '
        Me.txtJTjimAltEmail.Location = New System.Drawing.Point(132, 138)
        Me.txtJTjimAltEmail.Name = "txtJTjimAltEmail"
        Me.txtJTjimAltEmail.Size = New System.Drawing.Size(208, 23)
        Me.txtJTjimAltEmail.TabIndex = 37
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(5, 144)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(95, 17)
        Me.Label29.TabIndex = 36
        Me.Label29.Text = "Alt.Cont.Email"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(6, 26)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(66, 16)
        Me.Label17.TabIndex = 39
        Me.Label17.Text = "Margin %:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(77, 25)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 16)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Materials"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(185, 25)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(61, 16)
        Me.Label19.TabIndex = 41
        Me.Label19.Text = "SubCont."
        '
        'txtJTjimMatMu
        '
        Me.txtJTjimMatMu.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimMatMu.Location = New System.Drawing.Point(148, 22)
        Me.txtJTjimMatMu.Name = "txtJTjimMatMu"
        Me.txtJTjimMatMu.Size = New System.Drawing.Size(32, 22)
        Me.txtJTjimMatMu.TabIndex = 42
        '
        'txtJTjimSubMu
        '
        Me.txtJTjimSubMu.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTjimSubMu.Location = New System.Drawing.Point(253, 22)
        Me.txtJTjimSubMu.Name = "txtJTjimSubMu"
        Me.txtJTjimSubMu.Size = New System.Drawing.Size(32, 22)
        Me.txtJTjimSubMu.TabIndex = 43
        '
        'chkboxJTjimJobSpecRates
        '
        Me.chkboxJTjimJobSpecRates.AutoSize = True
        Me.chkboxJTjimJobSpecRates.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimJobSpecRates.Location = New System.Drawing.Point(24, 51)
        Me.chkboxJTjimJobSpecRates.Name = "chkboxJTjimJobSpecRates"
        Me.chkboxJTjimJobSpecRates.Size = New System.Drawing.Size(226, 20)
        Me.chkboxJTjimJobSpecRates.TabIndex = 44
        Me.chkboxJTjimJobSpecRates.Text = "Use Job Specific Labor Charges."
        Me.chkboxJTjimJobSpecRates.UseVisualStyleBackColor = True
        '
        'lvJTjimLabRates
        '
        Me.lvJTjimLabRates.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvJTjimLabCat, Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lvJTjimLabRates.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvJTjimLabRates.FullRowSelect = True
        Me.lvJTjimLabRates.HideSelection = False
        Me.lvJTjimLabRates.Location = New System.Drawing.Point(7, 82)
        Me.lvJTjimLabRates.Name = "lvJTjimLabRates"
        Me.lvJTjimLabRates.Size = New System.Drawing.Size(278, 169)
        Me.lvJTjimLabRates.TabIndex = 45
        Me.lvJTjimLabRates.UseCompatibleStateImageBehavior = False
        Me.lvJTjimLabRates.View = System.Windows.Forms.View.Details
        '
        'lvJTjimLabCat
        '
        Me.lvJTjimLabCat.Text = "LabCat"
        Me.lvJTjimLabCat.Width = 70
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Field Rate"
        Me.ColumnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader1.Width = 70
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Shop Rate"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader2.Width = 70
        '
        'chkboxJTjimInvPDF
        '
        Me.chkboxJTjimInvPDF.AutoSize = True
        Me.chkboxJTjimInvPDF.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimInvPDF.Location = New System.Drawing.Point(8, 21)
        Me.chkboxJTjimInvPDF.Name = "chkboxJTjimInvPDF"
        Me.chkboxJTjimInvPDF.Size = New System.Drawing.Size(94, 20)
        Me.chkboxJTjimInvPDF.TabIndex = 47
        Me.chkboxJTjimInvPDF.Text = "PDF/Email"
        Me.chkboxJTjimInvPDF.UseVisualStyleBackColor = True
        '
        'chkboxJTjimInvUSPS
        '
        Me.chkboxJTjimInvUSPS.AutoSize = True
        Me.chkboxJTjimInvUSPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimInvUSPS.Location = New System.Drawing.Point(8, 42)
        Me.chkboxJTjimInvUSPS.Name = "chkboxJTjimInvUSPS"
        Me.chkboxJTjimInvUSPS.Size = New System.Drawing.Size(107, 20)
        Me.chkboxJTjimInvUSPS.TabIndex = 48
        Me.chkboxJTjimInvUSPS.Text = "Paper/USPS"
        Me.chkboxJTjimInvUSPS.UseVisualStyleBackColor = True
        '
        'chkboxJTjimInvFile
        '
        Me.chkboxJTjimInvFile.AutoSize = True
        Me.chkboxJTjimInvFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimInvFile.Location = New System.Drawing.Point(8, 64)
        Me.chkboxJTjimInvFile.Name = "chkboxJTjimInvFile"
        Me.chkboxJTjimInvFile.Size = New System.Drawing.Size(92, 20)
        Me.chkboxJTjimInvFile.TabIndex = 49
        Me.chkboxJTjimInvFile.Text = "Paper/File"
        Me.chkboxJTjimInvFile.UseVisualStyleBackColor = True
        '
        'lvJTjimInvHist
        '
        Me.lvJTjimInvHist.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lvJTjimInvHist.FullRowSelect = True
        Me.lvJTjimInvHist.HideSelection = False
        Me.lvJTjimInvHist.Location = New System.Drawing.Point(474, 438)
        Me.lvJTjimInvHist.MultiSelect = False
        Me.lvJTjimInvHist.Name = "lvJTjimInvHist"
        Me.lvJTjimInvHist.Size = New System.Drawing.Size(219, 104)
        Me.lvJTjimInvHist.TabIndex = 50
        Me.lvJTjimInvHist.UseCompatibleStateImageBehavior = False
        Me.lvJTjimInvHist.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Invoice Date"
        Me.ColumnHeader3.Width = 90
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Invoice Total"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader4.Width = 90
        '
        'lvJTjimCurAct
        '
        Me.lvJTjimCurAct.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lvJTjimCurAct.HideSelection = False
        Me.lvJTjimCurAct.Location = New System.Drawing.Point(699, 438)
        Me.lvJTjimCurAct.Name = "lvJTjimCurAct"
        Me.lvJTjimCurAct.Size = New System.Drawing.Size(192, 104)
        Me.lvJTjimCurAct.TabIndex = 51
        Me.lvJTjimCurAct.UseCompatibleStateImageBehavior = False
        Me.lvJTjimCurAct.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Category"
        Me.ColumnHeader5.Width = 90
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Amount"
        Me.ColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader6.Width = 80
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(472, 420)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(100, 17)
        Me.Label21.TabIndex = 52
        Me.Label21.Text = "Invoice History"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(701, 419)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(159, 17)
        Me.Label22.TabIndex = 53
        Me.Label22.Text = "CstPlus related -TBInv'd"
        '
        'btnDone
        '
        Me.btnDone.Location = New System.Drawing.Point(17, 539)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(141, 56)
        Me.btnDone.TabIndex = 54
        Me.btnDone.Text = "Update/Exit"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(164, 539)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(141, 56)
        Me.btnCancel.TabIndex = 55
        Me.btnCancel.Text = "Cancel Changes/Exit"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(699, 545)
        Me.TextBox18.Multiline = True
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(193, 20)
        Me.TextBox18.TabIndex = 56
        Me.TextBox18.Text = "Dbl-Clk for detail/invcng."
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Controls.Add(Me.lvJTjimLabRates)
        Me.GroupBox5.Controls.Add(Me.chkboxJTjimJobSpecRates)
        Me.GroupBox5.Controls.Add(Me.txtJTjimSubMu)
        Me.GroupBox5.Controls.Add(Me.txtJTjimMatMu)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(12, 219)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(291, 257)
        Me.GroupBox5.TabIndex = 57
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Job Specific Variables"
        '
        'txtErrMsg
        '
        Me.txtErrMsg.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtErrMsg.Location = New System.Drawing.Point(445, 568)
        Me.txtErrMsg.Name = "txtErrMsg"
        Me.txtErrMsg.ReadOnly = True
        Me.txtErrMsg.Size = New System.Drawing.Size(445, 23)
        Me.txtErrMsg.TabIndex = 58
        '
        'txtJTjimJSLCat
        '
        Me.txtJTjimJSLCat.Location = New System.Drawing.Point(54, 109)
        Me.txtJTjimJSLCat.Name = "txtJTjimJSLCat"
        Me.txtJTjimJSLCat.ReadOnly = True
        Me.txtJTjimJSLCat.Size = New System.Drawing.Size(51, 22)
        Me.txtJTjimJSLCat.TabIndex = 60
        '
        'txtJTjimJSLStan
        '
        Me.txtJTjimJSLStan.Location = New System.Drawing.Point(132, 108)
        Me.txtJTjimJSLStan.Name = "txtJTjimJSLStan"
        Me.txtJTjimJSLStan.Size = New System.Drawing.Size(48, 22)
        Me.txtJTjimJSLStan.TabIndex = 61
        Me.txtJTjimJSLStan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTjimJSLshop
        '
        Me.txtJTjimJSLshop.Location = New System.Drawing.Point(212, 108)
        Me.txtJTjimJSLshop.Name = "txtJTjimJSLshop"
        Me.txtJTjimJSLshop.Size = New System.Drawing.Size(48, 22)
        Me.txtJTjimJSLshop.TabIndex = 62
        Me.txtJTjimJSLshop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTjimJSLtext1
        '
        Me.txtJTjimJSLtext1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.txtJTjimJSLtext1.Location = New System.Drawing.Point(30, 23)
        Me.txtJTjimJSLtext1.Multiline = True
        Me.txtJTjimJSLtext1.Name = "txtJTjimJSLtext1"
        Me.txtJTjimJSLtext1.ReadOnly = True
        Me.txtJTjimJSLtext1.Size = New System.Drawing.Size(256, 48)
        Me.txtJTjimJSLtext1.TabIndex = 63
        Me.txtJTjimJSLtext1.Tag = "0"
        Me.txtJTjimJSLtext1.Text = "Type over the rates shown with the desired Job Specific Rates."
        '
        'txtJTjimJSLtext3
        '
        Me.txtJTjimJSLtext3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.txtJTjimJSLtext3.Location = New System.Drawing.Point(212, 83)
        Me.txtJTjimJSLtext3.Name = "txtJTjimJSLtext3"
        Me.txtJTjimJSLtext3.ReadOnly = True
        Me.txtJTjimJSLtext3.Size = New System.Drawing.Size(48, 22)
        Me.txtJTjimJSLtext3.TabIndex = 64
        Me.txtJTjimJSLtext3.Text = "Shop"
        Me.txtJTjimJSLtext3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTjimJSLtext2
        '
        Me.txtJTjimJSLtext2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.txtJTjimJSLtext2.Location = New System.Drawing.Point(123, 83)
        Me.txtJTjimJSLtext2.Name = "txtJTjimJSLtext2"
        Me.txtJTjimJSLtext2.ReadOnly = True
        Me.txtJTjimJSLtext2.Size = New System.Drawing.Size(67, 22)
        Me.txtJTjimJSLtext2.TabIndex = 65
        Me.txtJTjimJSLtext2.Text = "Standard"
        Me.txtJTjimJSLtext2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chkboxJTjimJSLclone
        '
        Me.chkboxJTjimJSLclone.AutoSize = True
        Me.chkboxJTjimJSLclone.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimJSLclone.Location = New System.Drawing.Point(23, 136)
        Me.chkboxJTjimJSLclone.Name = "chkboxJTjimJSLclone"
        Me.chkboxJTjimJSLclone.Size = New System.Drawing.Size(242, 19)
        Me.chkboxJTjimJSLclone.TabIndex = 66
        Me.chkboxJTjimJSLclone.Text = "Clone all Labor Rates from Master Job."
        Me.chkboxJTjimJSLclone.UseVisualStyleBackColor = True
        '
        'txtJTjimJSLerrmsg
        '
        Me.txtJTjimJSLerrmsg.Location = New System.Drawing.Point(23, 162)
        Me.txtJTjimJSLerrmsg.Multiline = True
        Me.txtJTjimJSLerrmsg.Name = "txtJTjimJSLerrmsg"
        Me.txtJTjimJSLerrmsg.ReadOnly = True
        Me.txtJTjimJSLerrmsg.Size = New System.Drawing.Size(259, 50)
        Me.txtJTjimJSLerrmsg.TabIndex = 67
        '
        'btnJTjimJSLupdate
        '
        Me.btnJTjimJSLupdate.Location = New System.Drawing.Point(81, 218)
        Me.btnJTjimJSLupdate.Name = "btnJTjimJSLupdate"
        Me.btnJTjimJSLupdate.Size = New System.Drawing.Size(75, 35)
        Me.btnJTjimJSLupdate.TabIndex = 68
        Me.btnJTjimJSLupdate.Text = "Update"
        Me.btnJTjimJSLupdate.UseVisualStyleBackColor = True
        '
        'btnJTjimJSLcancel
        '
        Me.btnJTjimJSLcancel.Location = New System.Drawing.Point(166, 218)
        Me.btnJTjimJSLcancel.Name = "btnJTjimJSLcancel"
        Me.btnJTjimJSLcancel.Size = New System.Drawing.Size(75, 35)
        Me.btnJTjimJSLcancel.TabIndex = 69
        Me.btnJTjimJSLcancel.Text = "Cancel"
        Me.btnJTjimJSLcancel.UseVisualStyleBackColor = True
        Me.btnJTjimJSLcancel.Visible = False
        '
        'txtJTjimLastActDate
        '
        Me.txtJTjimLastActDate.Location = New System.Drawing.Point(110, 499)
        Me.txtJTjimLastActDate.Name = "txtJTjimLastActDate"
        Me.txtJTjimLastActDate.ReadOnly = True
        Me.txtJTjimLastActDate.Size = New System.Drawing.Size(70, 23)
        Me.txtJTjimLastActDate.TabIndex = 71
        Me.txtJTjimLastActDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(111, 479)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(83, 17)
        Me.Label23.TabIndex = 70
        Me.Label23.Text = "Last Activity"
        '
        'chkboxJTjimSndToAcct
        '
        Me.chkboxJTjimSndToAcct.AutoSize = True
        Me.chkboxJTjimSndToAcct.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimSndToAcct.Location = New System.Drawing.Point(137, 42)
        Me.chkboxJTjimSndToAcct.Name = "chkboxJTjimSndToAcct"
        Me.chkboxJTjimSndToAcct.Size = New System.Drawing.Size(101, 20)
        Me.chkboxJTjimSndToAcct.TabIndex = 73
        Me.chkboxJTjimSndToAcct.Text = "Snd->Acct'g"
        Me.chkboxJTjimSndToAcct.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox6.Controls.Add(Me.cboxJTjimTaxExempt)
        Me.GroupBox6.Controls.Add(Me.chkboxJTjimNonStdFrmt)
        Me.GroupBox6.Controls.Add(Me.chkboxJTjimSndToAcct)
        Me.GroupBox6.Controls.Add(Me.chkboxJTjimInvFile)
        Me.GroupBox6.Controls.Add(Me.chkboxJTjimInvUSPS)
        Me.GroupBox6.Controls.Add(Me.chkboxJTjimInvPDF)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(12, 123)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(291, 89)
        Me.GroupBox6.TabIndex = 75
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Invoicing"
        '
        'cboxJTjimTaxExempt
        '
        Me.cboxJTjimTaxExempt.AutoSize = True
        Me.cboxJTjimTaxExempt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTjimTaxExempt.Location = New System.Drawing.Point(137, 64)
        Me.cboxJTjimTaxExempt.Name = "cboxJTjimTaxExempt"
        Me.cboxJTjimTaxExempt.Size = New System.Drawing.Size(126, 20)
        Me.cboxJTjimTaxExempt.TabIndex = 93
        Me.cboxJTjimTaxExempt.Text = "Tax Exempt Job"
        Me.cboxJTjimTaxExempt.UseVisualStyleBackColor = True
        '
        'chkboxJTjimNonStdFrmt
        '
        Me.chkboxJTjimNonStdFrmt.AutoSize = True
        Me.chkboxJTjimNonStdFrmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxJTjimNonStdFrmt.Location = New System.Drawing.Point(137, 21)
        Me.chkboxJTjimNonStdFrmt.Name = "chkboxJTjimNonStdFrmt"
        Me.chkboxJTjimNonStdFrmt.Size = New System.Drawing.Size(107, 20)
        Me.chkboxJTjimNonStdFrmt.TabIndex = 76
        Me.chkboxJTjimNonStdFrmt.Text = "Non-Std Frmt"
        Me.chkboxJTjimNonStdFrmt.UseVisualStyleBackColor = True
        '
        'txtJTjimAUPInvdTD
        '
        Me.txtJTjimAUPInvdTD.Location = New System.Drawing.Point(148, 32)
        Me.txtJTjimAUPInvdTD.Name = "txtJTjimAUPInvdTD"
        Me.txtJTjimAUPInvdTD.ReadOnly = True
        Me.txtJTjimAUPInvdTD.Size = New System.Drawing.Size(78, 23)
        Me.txtJTjimAUPInvdTD.TabIndex = 84
        Me.txtJTjimAUPInvdTD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimAUPNetRemain
        '
        Me.txtJTjimAUPNetRemain.Location = New System.Drawing.Point(148, 57)
        Me.txtJTjimAUPNetRemain.Name = "txtJTjimAUPNetRemain"
        Me.txtJTjimAUPNetRemain.ReadOnly = True
        Me.txtJTjimAUPNetRemain.Size = New System.Drawing.Size(78, 23)
        Me.txtJTjimAUPNetRemain.TabIndex = 83
        Me.txtJTjimAUPNetRemain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimAUPQuoted
        '
        Me.txtJTjimAUPQuoted.Location = New System.Drawing.Point(148, 7)
        Me.txtJTjimAUPQuoted.Name = "txtJTjimAUPQuoted"
        Me.txtJTjimAUPQuoted.ReadOnly = True
        Me.txtJTjimAUPQuoted.Size = New System.Drawing.Size(78, 23)
        Me.txtJTjimAUPQuoted.TabIndex = 81
        Me.txtJTjimAUPQuoted.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(8, 60)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(93, 17)
        Me.Label27.TabIndex = 80
        Me.Label27.Text = "Net Remain'g"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(8, 86)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(113, 17)
        Me.Label26.TabIndex = 79
        Me.Label26.Text = "Queued for Inv'g"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(8, 37)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 17)
        Me.Label25.TabIndex = 78
        Me.Label25.Text = "Invoiced TD"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 17)
        Me.Label5.TabIndex = 77
        Me.Label5.Text = "Quoted Price"
        '
        'txtJTjimAUPQueueAmt
        '
        Me.txtJTjimAUPQueueAmt.Location = New System.Drawing.Point(148, 83)
        Me.txtJTjimAUPQueueAmt.Name = "txtJTjimAUPQueueAmt"
        Me.txtJTjimAUPQueueAmt.ReadOnly = True
        Me.txtJTjimAUPQueueAmt.Size = New System.Drawing.Size(78, 23)
        Me.txtJTjimAUPQueueAmt.TabIndex = 82
        Me.txtJTjimAUPQueueAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radiobtnJTjimCstPlus
        '
        Me.radiobtnJTjimCstPlus.AutoSize = True
        Me.radiobtnJTjimCstPlus.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radiobtnJTjimCstPlus.Location = New System.Drawing.Point(11, 22)
        Me.radiobtnJTjimCstPlus.Name = "radiobtnJTjimCstPlus"
        Me.radiobtnJTjimCstPlus.Size = New System.Drawing.Size(84, 20)
        Me.radiobtnJTjimCstPlus.TabIndex = 2
        Me.radiobtnJTjimCstPlus.TabStop = True
        Me.radiobtnJTjimCstPlus.Text = "Cost Plus"
        Me.radiobtnJTjimCstPlus.UseVisualStyleBackColor = True
        '
        'radiobtnJTjimAgreedUpon
        '
        Me.radiobtnJTjimAgreedUpon.AutoSize = True
        Me.radiobtnJTjimAgreedUpon.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radiobtnJTjimAgreedUpon.Location = New System.Drawing.Point(11, 62)
        Me.radiobtnJTjimAgreedUpon.Name = "radiobtnJTjimAgreedUpon"
        Me.radiobtnJTjimAgreedUpon.Size = New System.Drawing.Size(146, 20)
        Me.radiobtnJTjimAgreedUpon.TabIndex = 3
        Me.radiobtnJTjimAgreedUpon.TabStop = True
        Me.radiobtnJTjimAgreedUpon.Text = "Agreed Upon Pric'g"
        Me.radiobtnJTjimAgreedUpon.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox7.Controls.Add(Me.radiobtnJTjimCstPlusDeposits)
        Me.GroupBox7.Controls.Add(Me.radiobtnJTjimAgreedUpon)
        Me.GroupBox7.Controls.Add(Me.radiobtnJTjimCstPlus)
        Me.GroupBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(309, 123)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(235, 89)
        Me.GroupBox7.TabIndex = 85
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Job Pricing"
        '
        'radiobtnJTjimCstPlusDeposits
        '
        Me.radiobtnJTjimCstPlusDeposits.AutoSize = True
        Me.radiobtnJTjimCstPlusDeposits.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radiobtnJTjimCstPlusDeposits.Location = New System.Drawing.Point(11, 42)
        Me.radiobtnJTjimCstPlusDeposits.Name = "radiobtnJTjimCstPlusDeposits"
        Me.radiobtnJTjimCstPlusDeposits.Size = New System.Drawing.Size(166, 20)
        Me.radiobtnJTjimCstPlusDeposits.TabIndex = 4
        Me.radiobtnJTjimCstPlusDeposits.TabStop = True
        Me.radiobtnJTjimCstPlusDeposits.Text = "Cost Plus with Deposits"
        Me.radiobtnJTjimCstPlusDeposits.UseVisualStyleBackColor = True
        '
        'gboxJTjimAUP
        '
        Me.gboxJTjimAUP.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTjimAUP.Controls.Add(Me.gboxJTjimAUPIncludes)
        Me.gboxJTjimAUP.Controls.Add(Me.Label5)
        Me.gboxJTjimAUP.Controls.Add(Me.Label25)
        Me.gboxJTjimAUP.Controls.Add(Me.Label27)
        Me.gboxJTjimAUP.Controls.Add(Me.txtJTjimAUPQuoted)
        Me.gboxJTjimAUP.Controls.Add(Me.Label26)
        Me.gboxJTjimAUP.Controls.Add(Me.txtJTjimAUPInvdTD)
        Me.gboxJTjimAUP.Controls.Add(Me.txtJTjimAUPNetRemain)
        Me.gboxJTjimAUP.Controls.Add(Me.txtJTjimAUPQueueAmt)
        Me.gboxJTjimAUP.Location = New System.Drawing.Point(309, 218)
        Me.gboxJTjimAUP.Name = "gboxJTjimAUP"
        Me.gboxJTjimAUP.Size = New System.Drawing.Size(236, 198)
        Me.gboxJTjimAUP.TabIndex = 87
        Me.gboxJTjimAUP.TabStop = False
        '
        'gboxJTjimAUPIncludes
        '
        Me.gboxJTjimAUPIncludes.Controls.Add(Me.btnJTjimAUPJob)
        Me.gboxJTjimAUPIncludes.Controls.Add(Me.cboxJTjimAUPLabor)
        Me.gboxJTjimAUPIncludes.Controls.Add(Me.cboxJTjimAUPServices)
        Me.gboxJTjimAUPIncludes.Controls.Add(Me.cboxJTjimAUPMat)
        Me.gboxJTjimAUPIncludes.Location = New System.Drawing.Point(9, 107)
        Me.gboxJTjimAUPIncludes.Name = "gboxJTjimAUPIncludes"
        Me.gboxJTjimAUPIncludes.Size = New System.Drawing.Size(220, 100)
        Me.gboxJTjimAUPIncludes.TabIndex = 95
        Me.gboxJTjimAUPIncludes.TabStop = False
        Me.gboxJTjimAUPIncludes.Text = "AUP Pricing Includes:"
        '
        'btnJTjimAUPJob
        '
        Me.btnJTjimAUPJob.Location = New System.Drawing.Point(96, 21)
        Me.btnJTjimAUPJob.Name = "btnJTjimAUPJob"
        Me.btnJTjimAUPJob.Size = New System.Drawing.Size(116, 62)
        Me.btnJTjimAUPJob.TabIndex = 93
        Me.btnJTjimAUPJob.Text = "Mod/Create Job/Chg or Inv'g"
        Me.btnJTjimAUPJob.UseVisualStyleBackColor = True
        '
        'cboxJTjimAUPLabor
        '
        Me.cboxJTjimAUPLabor.AutoSize = True
        Me.cboxJTjimAUPLabor.Location = New System.Drawing.Point(6, 18)
        Me.cboxJTjimAUPLabor.Name = "cboxJTjimAUPLabor"
        Me.cboxJTjimAUPLabor.Size = New System.Drawing.Size(67, 21)
        Me.cboxJTjimAUPLabor.TabIndex = 88
        Me.cboxJTjimAUPLabor.Text = "Labor"
        Me.cboxJTjimAUPLabor.UseVisualStyleBackColor = True
        '
        'cboxJTjimAUPServices
        '
        Me.cboxJTjimAUPServices.AutoSize = True
        Me.cboxJTjimAUPServices.Location = New System.Drawing.Point(6, 60)
        Me.cboxJTjimAUPServices.Name = "cboxJTjimAUPServices"
        Me.cboxJTjimAUPServices.Size = New System.Drawing.Size(84, 21)
        Me.cboxJTjimAUPServices.TabIndex = 89
        Me.cboxJTjimAUPServices.Text = "Services"
        Me.cboxJTjimAUPServices.UseVisualStyleBackColor = True
        '
        'cboxJTjimAUPMat
        '
        Me.cboxJTjimAUPMat.AutoSize = True
        Me.cboxJTjimAUPMat.Location = New System.Drawing.Point(6, 38)
        Me.cboxJTjimAUPMat.Name = "cboxJTjimAUPMat"
        Me.cboxJTjimAUPMat.Size = New System.Drawing.Size(57, 21)
        Me.cboxJTjimAUPMat.TabIndex = 90
        Me.cboxJTjimAUPMat.Text = "Mat."
        Me.cboxJTjimAUPMat.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(475, 545)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(218, 20)
        Me.TextBox1.TabIndex = 88
        Me.TextBox1.Text = "Dbl-Clk Inv. for history display"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(156, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Job Name (Descriptive)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Job Details/Notes"
        '
        'gboxJTjimJSLframe
        '
        Me.gboxJTjimJSLframe.BackColor = System.Drawing.SystemColors.Menu
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLtext1)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLerrmsg)
        Me.gboxJTjimJSLframe.Controls.Add(Me.gboxJTjimCPWD)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLCat)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLStan)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLshop)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLtext3)
        Me.gboxJTjimJSLframe.Controls.Add(Me.txtJTjimJSLtext2)
        Me.gboxJTjimJSLframe.Controls.Add(Me.btnJTjimJSLcancel)
        Me.gboxJTjimJSLframe.Controls.Add(Me.chkboxJTjimJSLclone)
        Me.gboxJTjimJSLframe.Controls.Add(Me.btnJTjimJSLupdate)
        Me.gboxJTjimJSLframe.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTjimJSLframe.Location = New System.Drawing.Point(309, 218)
        Me.gboxJTjimJSLframe.Name = "gboxJTjimJSLframe"
        Me.gboxJTjimJSLframe.Size = New System.Drawing.Size(312, 273)
        Me.gboxJTjimJSLframe.TabIndex = 89
        Me.gboxJTjimJSLframe.TabStop = False
        Me.gboxJTjimJSLframe.Text = "Job Specific Labor Charge Mainenance"
        '
        'txtJTjimDeactDate
        '
        Me.txtJTjimDeactDate.Location = New System.Drawing.Point(279, 498)
        Me.txtJTjimDeactDate.Name = "txtJTjimDeactDate"
        Me.txtJTjimDeactDate.ReadOnly = True
        Me.txtJTjimDeactDate.Size = New System.Drawing.Size(70, 23)
        Me.txtJTjimDeactDate.TabIndex = 91
        Me.txtJTjimDeactDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(279, 479)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(79, 17)
        Me.Label32.TabIndex = 90
        Me.Label32.Text = "Deact Date"
        '
        'labDeactDate
        '
        Me.labDeactDate.AutoSize = True
        Me.labDeactDate.Location = New System.Drawing.Point(387, 8)
        Me.labDeactDate.Name = "labDeactDate"
        Me.labDeactDate.Size = New System.Drawing.Size(0, 17)
        Me.labDeactDate.TabIndex = 92
        '
        'gboxJTjimCPWD
        '
        Me.gboxJTjimCPWD.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTjimCPWD.Controls.Add(Me.Label36)
        Me.gboxJTjimCPWD.Controls.Add(Me.Label24)
        Me.gboxJTjimCPWD.Controls.Add(Me.Label35)
        Me.gboxJTjimCPWD.Controls.Add(Me.txtJTjimCPWDUnbilldAct)
        Me.gboxJTjimCPWD.Controls.Add(Me.txtJTjimCPWDSchedDep)
        Me.gboxJTjimCPWD.Controls.Add(Me.Button1)
        Me.gboxJTjimCPWD.Controls.Add(Me.Label28)
        Me.gboxJTjimCPWD.Controls.Add(Me.Label33)
        Me.gboxJTjimCPWD.Controls.Add(Me.Label34)
        Me.gboxJTjimCPWD.Controls.Add(Me.txtJTjimCPWDInvdDep)
        Me.gboxJTjimCPWD.Controls.Add(Me.txtJTjimCPWDInvdAct)
        Me.gboxJTjimCPWD.Controls.Add(Me.txtJTjimCPWDDepBal)
        Me.gboxJTjimCPWD.Location = New System.Drawing.Point(0, 1)
        Me.gboxJTjimCPWD.Name = "gboxJTjimCPWD"
        Me.gboxJTjimCPWD.Size = New System.Drawing.Size(235, 199)
        Me.gboxJTjimCPWD.TabIndex = 94
        Me.gboxJTjimCPWD.TabStop = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(27, 90)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(172, 16)
        Me.Label36.TabIndex = 98
        Me.Label36.Text = "---------------------------------"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(10, 111)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(119, 16)
        Me.Label24.TabIndex = 94
        Me.Label24.Text = "Unbilled Activity"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(10, 133)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(130, 16)
        Me.Label35.TabIndex = 95
        Me.Label35.Text = "Sched'd Deposits"
        '
        'txtJTjimCPWDUnbilldAct
        '
        Me.txtJTjimCPWDUnbilldAct.Location = New System.Drawing.Point(135, 107)
        Me.txtJTjimCPWDUnbilldAct.Name = "txtJTjimCPWDUnbilldAct"
        Me.txtJTjimCPWDUnbilldAct.ReadOnly = True
        Me.txtJTjimCPWDUnbilldAct.Size = New System.Drawing.Size(78, 22)
        Me.txtJTjimCPWDUnbilldAct.TabIndex = 97
        Me.txtJTjimCPWDUnbilldAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimCPWDSchedDep
        '
        Me.txtJTjimCPWDSchedDep.Location = New System.Drawing.Point(135, 132)
        Me.txtJTjimCPWDSchedDep.Name = "txtJTjimCPWDSchedDep"
        Me.txtJTjimCPWDSchedDep.ReadOnly = True
        Me.txtJTjimCPWDSchedDep.Size = New System.Drawing.Size(78, 22)
        Me.txtJTjimCPWDSchedDep.TabIndex = 96
        Me.txtJTjimCPWDSchedDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 161)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(207, 34)
        Me.Button1.TabIndex = 93
        Me.Button1.Text = "Mod/Create Job/Chg or Inv'g"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(10, 23)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(106, 16)
        Me.Label28.TabIndex = 77
        Me.Label28.Text = "Inv'd Deposits"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(10, 46)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(94, 16)
        Me.Label33.TabIndex = 78
        Me.Label33.Text = "Inv'd Activity"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(10, 68)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(122, 16)
        Me.Label34.TabIndex = 80
        Me.Label34.Text = "Deposit Balance"
        '
        'txtJTjimCPWDInvdDep
        '
        Me.txtJTjimCPWDInvdDep.Location = New System.Drawing.Point(135, 19)
        Me.txtJTjimCPWDInvdDep.Name = "txtJTjimCPWDInvdDep"
        Me.txtJTjimCPWDInvdDep.ReadOnly = True
        Me.txtJTjimCPWDInvdDep.Size = New System.Drawing.Size(78, 22)
        Me.txtJTjimCPWDInvdDep.TabIndex = 81
        Me.txtJTjimCPWDInvdDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimCPWDInvdAct
        '
        Me.txtJTjimCPWDInvdAct.Location = New System.Drawing.Point(135, 44)
        Me.txtJTjimCPWDInvdAct.Name = "txtJTjimCPWDInvdAct"
        Me.txtJTjimCPWDInvdAct.ReadOnly = True
        Me.txtJTjimCPWDInvdAct.Size = New System.Drawing.Size(78, 22)
        Me.txtJTjimCPWDInvdAct.TabIndex = 84
        Me.txtJTjimCPWDInvdAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimCPWDDepBal
        '
        Me.txtJTjimCPWDDepBal.Location = New System.Drawing.Point(135, 69)
        Me.txtJTjimCPWDDepBal.Name = "txtJTjimCPWDDepBal"
        Me.txtJTjimCPWDDepBal.ReadOnly = True
        Me.txtJTjimCPWDDepBal.Size = New System.Drawing.Size(78, 22)
        Me.txtJTjimCPWDDepBal.TabIndex = 83
        Me.txtJTjimCPWDDepBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTjimSJPRevDate
        '
        Me.txtJTjimSJPRevDate.Location = New System.Drawing.Point(196, 498)
        Me.txtJTjimSJPRevDate.Name = "txtJTjimSJPRevDate"
        Me.txtJTjimSJPRevDate.ReadOnly = True
        Me.txtJTjimSJPRevDate.Size = New System.Drawing.Size(70, 23)
        Me.txtJTjimSJPRevDate.TabIndex = 96
        Me.txtJTjimSJPRevDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblJTjimPrcgRev
        '
        Me.lblJTjimPrcgRev.AutoSize = True
        Me.lblJTjimPrcgRev.Location = New System.Drawing.Point(200, 479)
        Me.lblJTjimPrcgRev.Name = "lblJTjimPrcgRev"
        Me.lblJTjimPrcgRev.Size = New System.Drawing.Size(76, 17)
        Me.lblJTjimPrcgRev.TabIndex = 95
        Me.lblJTjimPrcgRev.Text = "Pric'g Rev."
        '
        'JTjim
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.Menu
        Me.ClientSize = New System.Drawing.Size(908, 606)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtJTjimSJPRevDate)
        Me.Controls.Add(Me.lblJTjimPrcgRev)
        Me.Controls.Add(Me.labDeactDate)
        Me.Controls.Add(Me.txtJTjimDeactDate)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.gboxJTjimJSLframe)
        Me.Controls.Add(Me.gboxJTjimAUP)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtJTjimLastActDate)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.txtErrMsg)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.TextBox18)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.lvJTjimCurAct)
        Me.Controls.Add(Me.lvJTjimInvHist)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.txtJtjimCreateDate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtJTjimJobDet)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtJTjimJobDesc)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtJTjimSubJob)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtJTjimJob)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "JTjim"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Job Information Screen"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.gboxJTjimAUP.ResumeLayout(False)
        Me.gboxJTjimAUP.PerformLayout()
        Me.gboxJTjimAUPIncludes.ResumeLayout(False)
        Me.gboxJTjimAUPIncludes.PerformLayout()
        Me.gboxJTjimJSLframe.ResumeLayout(False)
        Me.gboxJTjimJSLframe.PerformLayout()
        Me.gboxJTjimCPWD.ResumeLayout(False)
        Me.gboxJTjimCPWD.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtJTjimJob As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTjimSubJob As TextBox
    Friend WithEvents txtJTjimJobDesc As TextBox
    Friend WithEvents txtJTjimJobDet As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtJtjimCreateDate As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents chkboxJTjimBillAddSwitch As CheckBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtJTjimJobLocStreet As TextBox
    Friend WithEvents txtJTjimJobLocCity As TextBox
    Friend WithEvents txtJTjimJobLocState As TextBox
    Friend WithEvents txtJTjimBillCity As TextBox
    Friend WithEvents txtJTjimBillStreet As TextBox
    Friend WithEvents txtJTjimContact As TextBox
    Friend WithEvents txtJTjimTel As TextBox
    Friend WithEvents txtJTjimEmail As TextBox
    Friend WithEvents txtJTjimAltContact As TextBox
    Friend WithEvents txtJTjimAltTel As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtJTjimMatMu As TextBox
    Friend WithEvents txtJTjimSubMu As TextBox
    Friend WithEvents chkboxJTjimJobSpecRates As CheckBox
    Friend WithEvents lvJTjimLabRates As ListView
    Friend WithEvents lvJTjimLabCat As ColumnHeader
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents chkboxJTjimInvPDF As CheckBox
    Friend WithEvents chkboxJTjimInvUSPS As CheckBox
    Friend WithEvents chkboxJTjimInvFile As CheckBox
    Friend WithEvents lvJTjimInvHist As ListView
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents lvJTjimCurAct As ListView
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents btnDone As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents TextBox18 As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtErrMsg As TextBox
    Friend WithEvents txtJTjimJSLCat As TextBox
    Friend WithEvents txtJTjimJSLStan As TextBox
    Friend WithEvents txtJTjimJSLshop As TextBox
    Friend WithEvents txtJTjimJSLtext1 As TextBox
    Friend WithEvents txtJTjimJSLtext3 As TextBox
    Friend WithEvents txtJTjimJSLtext2 As TextBox
    Friend WithEvents chkboxJTjimJSLclone As CheckBox
    Friend WithEvents txtJTjimJSLerrmsg As TextBox
    Friend WithEvents btnJTjimJSLupdate As Button
    Friend WithEvents btnJTjimJSLcancel As Button
    Friend WithEvents txtJTjimLastActDate As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents chkboxJTjimSndToAcct As CheckBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txtJTjimAUPInvdTD As TextBox
    Friend WithEvents txtJTjimAUPNetRemain As TextBox
    Friend WithEvents txtJTjimAUPQuoted As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtJTjimAUPQueueAmt As TextBox
    Friend WithEvents radiobtnJTjimCstPlus As RadioButton
    Friend WithEvents radiobtnJTjimAgreedUpon As RadioButton
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents gboxJTjimAUP As GroupBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtJTjimAltEmail As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents ChkBoxInvAlt As CheckBox
    Friend WithEvents ChkBoxInvPrim As CheckBox
    Friend WithEvents Label30 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents gboxJTjimJSLframe As GroupBox
    Friend WithEvents cboxJTjimAUPMat As CheckBox
    Friend WithEvents cboxJTjimAUPServices As CheckBox
    Friend WithEvents cboxJTjimAUPLabor As CheckBox
    Friend WithEvents txtJTjimDeactDate As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents labDeactDate As Label
    Friend WithEvents btnJTjimAUPJob As Button
    Friend WithEvents chkboxJTjimNonStdFrmt As CheckBox
    Friend WithEvents lblJTjimJobLocStateNme As Label
    Friend WithEvents cboxJTjimTaxExempt As CheckBox
    Friend WithEvents radiobtnJTjimCstPlusDeposits As RadioButton
    Friend WithEvents gboxJTjimAUPIncludes As GroupBox
    Friend WithEvents gboxJTjimCPWD As GroupBox
    Friend WithEvents Label36 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents txtJTjimCPWDUnbilldAct As TextBox
    Friend WithEvents txtJTjimCPWDSchedDep As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label28 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents txtJTjimCPWDInvdDep As TextBox
    Friend WithEvents txtJTjimCPWDInvdAct As TextBox
    Friend WithEvents txtJTjimCPWDDepBal As TextBox
    Friend WithEvents txtJTjimSJPRevDate As TextBox
    Friend WithEvents lblJTjimPrcgRev As Label
End Class
