﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JTnlcM
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTnlcM))
        Me.btnJTnlcMInvEntMan = New System.Windows.Forms.Button()
        Me.btnJTnlcMRecon = New System.Windows.Forms.Button()
        Me.btnJTnlcMInStk = New System.Windows.Forms.Button()
        Me.btnJTnlcMRevMod = New System.Windows.Forms.Button()
        Me.btnJTnlcMInvEntEDI = New System.Windows.Forms.Button()
        Me.btnJTnlcMExit = New System.Windows.Forms.Button()
        Me.lvJTnlcM = New System.Windows.Forms.ListView()
        Me.ItemCategory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemCount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Total = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Age0_30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Age31_60 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Age61_90 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AgeOver90 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnJTnlcMInvEntMan
        '
        Me.btnJTnlcMInvEntMan.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMInvEntMan.Location = New System.Drawing.Point(21, 21)
        Me.btnJTnlcMInvEntMan.Name = "btnJTnlcMInvEntMan"
        Me.btnJTnlcMInvEntMan.Size = New System.Drawing.Size(238, 78)
        Me.btnJTnlcMInvEntMan.TabIndex = 0
        Me.btnJTnlcMInvEntMan.Text = "NLC Entry" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " Manual"
        Me.btnJTnlcMInvEntMan.UseVisualStyleBackColor = True
        '
        'btnJTnlcMRecon
        '
        Me.btnJTnlcMRecon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMRecon.Location = New System.Drawing.Point(183, 105)
        Me.btnJTnlcMRecon.Name = "btnJTnlcMRecon"
        Me.btnJTnlcMRecon.Size = New System.Drawing.Size(155, 78)
        Me.btnJTnlcMRecon.TabIndex = 1
        Me.btnJTnlcMRecon.Text = "Vendor Statement Reconciliation"
        Me.btnJTnlcMRecon.UseVisualStyleBackColor = True
        '
        'btnJTnlcMInStk
        '
        Me.btnJTnlcMInStk.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMInStk.Location = New System.Drawing.Point(344, 105)
        Me.btnJTnlcMInStk.Name = "btnJTnlcMInStk"
        Me.btnJTnlcMInStk.Size = New System.Drawing.Size(155, 78)
        Me.btnJTnlcMInStk.TabIndex = 2
        Me.btnJTnlcMInStk.Text = "InStock/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "InHouse" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Maintenance" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnJTnlcMInStk.UseVisualStyleBackColor = True
        '
        'btnJTnlcMRevMod
        '
        Me.btnJTnlcMRevMod.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMRevMod.Location = New System.Drawing.Point(20, 105)
        Me.btnJTnlcMRevMod.Name = "btnJTnlcMRevMod"
        Me.btnJTnlcMRevMod.Size = New System.Drawing.Size(155, 78)
        Me.btnJTnlcMRevMod.TabIndex = 4
        Me.btnJTnlcMRevMod.Text = "NLC Review," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Modify or Delete"
        Me.btnJTnlcMRevMod.UseVisualStyleBackColor = True
        '
        'btnJTnlcMInvEntEDI
        '
        Me.btnJTnlcMInvEntEDI.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMInvEntEDI.Location = New System.Drawing.Point(265, 21)
        Me.btnJTnlcMInvEntEDI.Name = "btnJTnlcMInvEntEDI"
        Me.btnJTnlcMInvEntEDI.Size = New System.Drawing.Size(234, 78)
        Me.btnJTnlcMInvEntEDI.TabIndex = 5
        Me.btnJTnlcMInvEntEDI.Text = "NLC Entry" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "EDI"
        Me.btnJTnlcMInvEntEDI.UseVisualStyleBackColor = True
        '
        'btnJTnlcMExit
        '
        Me.btnJTnlcMExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcMExit.Location = New System.Drawing.Point(509, 21)
        Me.btnJTnlcMExit.Name = "btnJTnlcMExit"
        Me.btnJTnlcMExit.Size = New System.Drawing.Size(128, 162)
        Me.btnJTnlcMExit.TabIndex = 6
        Me.btnJTnlcMExit.Text = "Exit to " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Main Menu"
        Me.btnJTnlcMExit.UseVisualStyleBackColor = True
        '
        'lvJTnlcM
        '
        Me.lvJTnlcM.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ItemCategory, Me.ItemCount, Me.Total, Me.Age0_30, Me.Age31_60, Me.Age61_90, Me.AgeOver90})
        Me.lvJTnlcM.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvJTnlcM.HideSelection = False
        Me.lvJTnlcM.Location = New System.Drawing.Point(20, 219)
        Me.lvJTnlcM.MultiSelect = False
        Me.lvJTnlcM.Name = "lvJTnlcM"
        Me.lvJTnlcM.Scrollable = False
        Me.lvJTnlcM.Size = New System.Drawing.Size(627, 159)
        Me.lvJTnlcM.TabIndex = 7
        Me.lvJTnlcM.UseCompatibleStateImageBehavior = False
        Me.lvJTnlcM.View = System.Windows.Forms.View.Details
        '
        'ItemCategory
        '
        Me.ItemCategory.Text = "Category"
        Me.ItemCategory.Width = 80
        '
        'ItemCount
        '
        Me.ItemCount.Text = "Count"
        Me.ItemCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Total
        '
        Me.Total.Text = "Total"
        Me.Total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Total.Width = 90
        '
        'Age0_30
        '
        Me.Age0_30.Text = "0 - 30"
        Me.Age0_30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Age0_30.Width = 90
        '
        'Age31_60
        '
        Me.Age31_60.Text = "31 - 60"
        Me.Age31_60.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Age31_60.Width = 90
        '
        'Age61_90
        '
        Me.Age61_90.Text = "61 - 90"
        Me.Age61_90.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Age61_90.Width = 90
        '
        'AgeOver90
        '
        Me.AgeOver90.Text = "Over 90"
        Me.AgeOver90.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.AgeOver90.Width = 90
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 193)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(364, 18)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "NLC Transactions Pending Invoicing (Rounded)"
        '
        'JTnlcM
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(662, 393)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lvJTnlcM)
        Me.Controls.Add(Me.btnJTnlcMExit)
        Me.Controls.Add(Me.btnJTnlcMInvEntEDI)
        Me.Controls.Add(Me.btnJTnlcMRevMod)
        Me.Controls.Add(Me.btnJTnlcMInStk)
        Me.Controls.Add(Me.btnJTnlcMRecon)
        Me.Controls.Add(Me.btnJTnlcMInvEntMan)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "JTnlcM"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JTnlcM - Job Information Manager - Non-Labor Cost  Menu"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnJTnlcMInvEntMan As Button
    Friend WithEvents btnJTnlcMRecon As Button
    Friend WithEvents btnJTnlcMInStk As Button
    Friend WithEvents btnJTnlcMRevMod As Button
    Friend WithEvents btnJTnlcMInvEntEDI As Button
    Friend WithEvents btnJTnlcMExit As Button
    Friend WithEvents lvJTnlcM As ListView
    Friend WithEvents ItemCategory As ColumnHeader
    Friend WithEvents ItemCount As ColumnHeader
    Friend WithEvents Total As ColumnHeader
    Friend WithEvents Age0_30 As ColumnHeader
    Friend WithEvents Age31_60 As ColumnHeader
    Friend WithEvents Age61_90 As ColumnHeader
    Friend WithEvents AgeOver90 As ColumnHeader
    Friend WithEvents Label1 As Label
End Class
