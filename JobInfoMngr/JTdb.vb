﻿''' <summary>JTdb ... meaning dash board. This class builds and presents the dashboard panel. The panel is for information only. It is presented on system-startup.</summary>
Public Class JTdb
    Structure GB7Stru
        Dim GB7SJob As String
        Dim GB7SSub As String
        Dim GB7SLstDate As String
        Dim GB7SUnbilled As Double
    End Structure
    Dim slGB7 As SortedList = New SortedList



    ''' <summary>call sub soutines to fill the information boxes presented on the dashboard panel. Upon completion, it displays the panel.</summary>
    ''' <param name="sender">not used</param>
    ''' <param name="e">not used</param>
    Private Sub JTdb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' -------------------------------------------------------------------------------
        ' Check to see if dashboard should be shown. True = Yes
        ' -------------------------------------------------------------------------------
        If JTVarMaint.JTvmShowDashBoard = True Then
            Me.MdiParent = JTstart
            If JTVarMaint.CompanyLogo <> "" Then
                PictureBox1.Image = Image.FromFile(JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.CompanyLogo)
            Else
                PictureBox1.Image = JobInfoMngr.My.Resources.Resources.Logo2
            End If
            JTdbGroupBox1Backups()
            JTdbGroupBox2UnbilledActivity()
            JTdbGroupBox3InternetAccess()
            JTdbGroupBox5PendingExport()
            JTdbGroupBox6SupplierRecap()
            JTdbGroupBox7LstInvoice()
        End If
    End Sub

    ''' <summary>This button accepts user input that enable the system to close dashboard and proceed with normal activity.</summary>
    ''' <param name="sender">not used</param>
    ''' <param name="e">not used</param>
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
        JTMainMenu.MdiParent = JTstart
        JTMainMenu.JTMMinit()
    End Sub
    '
    ''' <summary>Fills groupbox1 on dashboard panel with data related to backup status.</summary>
    Private Function JTdbGroupBox1Backups()
        Me.txtJTdbBUSLstBckUp.Text = Julian2Gregorian(JTVarMaint.JTAchBkupDate)
        Me.txtJTdbBUSFreq.Text = JTVarMaint.JTAchBkupFreq
        Dim tmpNxtSchedBkupDate As String = Val(JTVarMaint.JTAchBkupDate) + Val(JTVarMaint.JTAchBkupFreq)
        Dim result As Integer = DateTime.Compare(CDate(Julian2Gregorian(tmpNxtSchedBkupDate)), Date.Today)


        If result >= 0 Then
            Me.txtJTdbBUSStatus.Text = "Current"
        Else
            Me.txtJTdbBUSStatus.Text = "Past Due"
        End If
        Return ""
    End Function
    ''' <summary>Fills unbilled activity amounts in GB2 on dashboard panel.</summary>
    Private Function JTdbGroupBox2UnbilledActivity()
        JTtk.JTtkRestoreTKFile()
        '
        Dim w5date As String = Nothing
        Dim w4date As String = Nothing
        Dim w3date As String = Nothing
        Dim w2date As String = Nothing
        Dim w1date As String = Nothing
        '
        Dim w5hours As Double = Nothing
        Dim w4hours As Double = Nothing
        Dim w3hours As Double = Nothing
        Dim w2hours As Double = Nothing
        Dim w1hours As Double = Nothing
        '
        Dim tmpLabor As Double = JTtk.JTtkUnbilledLabor(w5date, w5hours, w4date, w4hours, w3date, w3hours,
                                                        w2date, w2hours, w1date, w1hours)
        '

        txtJTdbUALabor.Text = String.Format("{0:0.00}", tmpLabor)
        Dim tmpMat As Double = 0
        Dim tmpSub As Double = 0
        Dim tmpAdj As Double = 0
        '
        Dim PendRecCnt As Integer = 0
        Dim PendQBECnt As Integer = 0
        Dim PendManCnt As Integer = 0
        Dim PendRecAmount As Double = 0
        Dim PendQBEAmount As Double = 0
        Dim PendManAmount As Double = 0

        '
        JTnlc.JTnlcArrayLoad()
        JTnlc.JTnlcUnbilledNLC(tmpMat, tmpSub, tmpAdj, PendRecCnt, PendRecAmount, PendQBECnt, PendQBEAmount,
                               PendManCnt, PendManAmount)


        txtJTdbUAMat.Text = String.Format("{0:0.00}", tmpMat)
        txtJTdbUASub.Text = String.Format("{0:0.00}", tmpSub)
        txtJTdbUAAdj.Text = String.Format("{0:0.00}", tmpAdj)
        '
        Dim tmpTotal As Double = tmpLabor + tmpMat + tmpSub + tmpAdj
        txtJTdbUATotal.Text = String.Format("{0:0.00}", tmpTotal)
        ' --------------------------------------------------------------
        ' Populate fields on GroupBox4
        ' --------------------------------------------------------------
        txtJTdbHSDate5.Text = w5date
        txtJTdbHSDate4.Text = w4date
        txtJTdbHSDate3.Text = w3date
        txtJTdbHSDate2.Text = w2date
        txtJTdbHSDate1.Text = w1date
        '
        txtJTdbHSHours5.Text = String.Format("{0:0.0}", w5hours)
        txtJTdbHSHours4.Text = String.Format("{0:0.0}", w4hours)
        txtJTdbHSHours3.Text = String.Format("{0:0.0}", w3hours)
        txtJTdbHSHours2.Text = String.Format("{0:0.0}", w2hours)
        txtJTdbHSHours1.Text = String.Format("{0:0.0}", w1hours)
        ' ----------------------------------------------------------------------
        ' Following code populated NLC related fields in GroupBox5 - Activity 
        ' Pending Export.
        ' ----------------------------------------------------------------------
        txtJTdbPENLCUnRecCnt.Text = PendRecCnt
        txtJTdbPENLCUnRecAmt.Text = String.Format("{0:0.00}", PendRecAmount)
        txtJTdbPENLCQBCnt.Text = PendQBECnt
        txtJTdbPENLCQBAmt.Text = String.Format("{0:0.00}", PendQBEAmount)
        txtJTdbPENLCManCnt.Text = PendManCnt
        txtJTdbPENLCManAmt.Text = String.Format("{0:0.00}", PendManAmount)
        Return ""
    End Function
    ''' <summary>checked access to internet and posted status on Dashboard panel. Emailing directly from JIM is not available if there is no internet access.</summary>
    Private Function JTdbGroupBox3InternetAccess()
        '       If My.Computer.Network.IsAvailable Then
        If CheckForInternetConnection() = True Then
            txtJTdbIAStatus.Text = "Available"
        Else
            txtJTdbIAStatus.Text = "Not Connected"
        End If
        Return ""
    End Function
    ''' <summary>function scan exportable items (invoices, NLC, etc.) and presents the current status on the Dashboard panel ... Groupbox5.</summary>
    Private Function JTdbGroupBox5PendingExport()
        Dim tmpCount As Decimal = 0
        Dim tmpAmount As Double = 0
        Dim tmpNotEmailed As Integer = 0
        '
        JTas.JTasUNotExportedInvoices(tmpCount, tmpAmount, tmpNotEmailed)
        txtJTdbPEInvCnt.Text = tmpCount
        txtJTdbPEInvAmt.Text = String.Format("{0:0.00}", tmpAmount)
        ' populating GroupBox8 field
        txtJTdbNECount.Text = String.Format("{0:0}", tmpNotEmailed)
        Return ""
    End Function
    ''' <summary>Fills groupbox6 on the dashboard with Supplier information and statistics.</summary>
    Private Function JTdbGroupBox6SupplierRecap()
        Dim tmpSupplierCnt As Integer = 0
        Dim tmpSubConCnt As Integer = 0
        Dim tmpOutofCompCnt As Integer = 0
        JTvi.JTviSupplierRecapExtract(tmpSupplierCnt, tmpSubConCnt, tmpOutofCompCnt)
        txtJTdbSSRSupCnt.Text = tmpSupplierCnt
        txtJTdbSSRSucConCnt.Text = tmpSubConCnt
        txtJTdbSSROutofCompCnt.Text = tmpSubConCnt
        If tmpSubConCnt > 0 Then
            txtJTdbSSRCompCom.Text = "Paperwork is out of compliance. Review table on MainMenu to identify and resolve."
        End If
        Return ""
    End Function
    ''' <summary>Creates list in groupbox7 of customer/jobs with pending invoice amounts. List is in descending order.</summary>
    Private Function JTdbGroupBox7LstInvoice()
        Dim tmpJobSub As Integer = 0
        Dim tmpJobId As String = ""
        Dim tmpSubName As String = ""
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpCstMeth As String = ""
        '
        Dim NewItem As GB7Stru


        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobId, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"

            NewItem.GB7SJob = tmpJobId
            NewItem.GB7SSub = tmpSubName
            If JTas.JTasJobLstInvoice(tmpJobId, tmpSubName) = "" Then
                NewItem.GB7SLstDate = "NI"
            Else
                Dim Today As DateTime = DateTime.Now
                Dim InvDate As DateTime = CDate(JTas.JTasJobLstInvoice(tmpJobId, tmpSubName))

                Dim DaysStayed As Int32 = Today.Subtract(InvDate).Days
                NewItem.GB7SLstDate = DaysStayed

            End If
            Dim tmpLstTKDate As String = ""
            Dim tmpLstNLCDate As String = ""
            '
            NewItem.GB7SUnbilled = String.Format("{0:0.00}", JTtk.JTtkJobUnbilled(tmpJobId, tmpSubName, tmpLstTKDate) +
                                                 JTnlc.JTnlcJobUnbilled(tmpJobId, tmpSubName, tmpLstNLCDate))
            tmpJobSub += 1
            Dim slKey As String
            If NewItem.GB7SLstDate <> "NI" Then
                slKey = Str(2000 - Val(NewItem.GB7SLstDate)) & tmpJobId & tmpSubName
            Else
                slKey = Str(2000) & tmpJobId & tmpSubName
            End If
            If Val(NewItem.GB7SUnbilled) <> 0 Then
                slGB7.Add(slKey, NewItem)
            End If
        Loop
        '
        '
        Dim itmBuild(3) As String
        Dim lvLine As ListViewItem
        '
        lvJTdbLI.Items.Clear()
        Dim key As ICollection = slGB7.Keys
        Dim k As String = ""

        For Each k In key

            itmBuild(0) = slGB7(k).GB7SJob
            If slGB7(k).GB7SSub <> "" Then
                itmBuild(0) = itmBuild(0) & "_" & slGB7(k).GB7SSub
            End If

            itmBuild(1) = slGB7(k).GB7SLstDate
            itmBuild(2) = String.Format("{0:0.00}", Val(slGB7(k).GB7SUnbilled))
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTdbLI.Items.Add(lvLine)
        Next
        '
        Return ""
    End Function


End Class