﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
''' <summary>
''' This class formats and produces the Invoice Report. It creates a PDF report utilizing iTextSharp.
''' </summary>
Public Class JTir
    '
    Dim slJTir As New SortedList
    '
    Dim tmpSubTls As JTas.JTasIHStru
    Dim tmpGrdTls As JTas.JTasIHStru
    '
    ''' <summary>
    ''' Sub to prepare JTir panel for presentation. Panel sets options related to Invoice Report.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTir_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        JTMainMenu.Hide()
        JTstart.JTstartMenuOff()
        Me.MdiParent = JTstart
        '
        cboxPrimary.Checked = True
        cboxAlternate.Checked = False
        '
        rbtnJTirYTD.Checked = True
        txtJTirEndMth.Text = String.Format("{0:00}", Today.Month)
        txtJTirStMth.Text = "01"
        txtJTirEndYr.Text = Today.Year.ToString
        txtJTirStYr.Text = Today.Year.ToString
        '
        Me.Show()
    End Sub
    ''' <summary>
    ''' JTir panel controls. Cancels request for report.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub
    ''' <summary>
    ''' JTir panel controls. Queues logic to generate report.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        If IsNumeric(txtJTirStMth.Text) = False Then
            txtJTirStMth.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTirEndMth.Text) = False Then
            txtJTirEndMth.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTirStYr.Text) = False Then
            txtJTirStYr.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTirEndYr.Text) = False Then
            txtJTirEndYr.Select()
            Exit Sub
        End If
        '
        If txtJTirStMth.Text < 1 Or txtJTirStMth.Text > 12 Then
            txtJTirStMth.Select()
            Exit Sub
        End If
        If txtJTirEndMth.Text < 1 Or txtJTirEndMth.Text > 12 Then
            txtJTirEndMth.Select()
            Exit Sub
        End If
        If txtJTirStYr.Text < 2000 Or txtJTirStYr.Text > Today.Year.ToString Then
            txtJTirStYr.Select()
            Exit Sub
        End If
        If txtJTirEndYr.Text < 2000 Or txtJTirEndYr.Text > Today.Year.ToString Then
            txtJTirEndYr.Select()
            Exit Sub
        End If
        If txtJTirStYr.Text > txtJTirEndYr.Text Then
            txtJTirEndYr.Select()
            Exit Sub
        End If
        If cboxAlternate.Checked = False And cboxPrimary.Checked = False Then
            cboxPrimary.Select()
            Exit Sub
        End If
        JTirBldPrntRprt()


    End Sub
    ''' <summary>
    ''' JTir panel controls. Reformats beginning date input.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTirStMth_LostFocus(sender As Object, e As EventArgs) Handles txtJTirStMth.LostFocus
        txtJTirStMth.Text = String.Format("{0:00}", Val(txtJTirStMth.Text))
    End Sub
    ''' <summary>
    ''' JTir panel controls. Reformats end date input.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTirEndMth_LostFocus(sender As Object, e As EventArgs) Handles txtJTirEndMth.LostFocus
        txtJTirEndMth.Text = String.Format("{0:00}", Val(txtJTirEndMth.Text))
    End Sub
    ''' <summary>
    ''' JTir panel controls. Set beginning and ending date based on options selected..
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub rbtnJTirCurrent_CheckedChanged(sender As Object, e As EventArgs) _
        Handles rbtnJTirCurrent.CheckedChanged, rbtnJTirYTD.CheckedChanged, rbtnJTirCustom.CheckedChanged
        If rbtnJTirCurrent.Checked = True Then
            txtJTirEndMth.Text = String.Format("{0:00}", Today.Month)
            txtJTirStMth.Text = String.Format("{0:00}", Today.Month)
            txtJTirEndYr.Text = Today.Year.ToString
            txtJTirStYr.Text = Today.Year.ToString
        End If
        If rbtnJTirYTD.Checked = True Then
            txtJTirEndMth.Text = String.Format("{0:00}", Today.Month)
            txtJTirStMth.Text = "01"
            txtJTirEndYr.Text = Today.Year.ToString
            txtJTirStYr.Text = Today.Year.ToString
        End If
        If rbtnJTirCustom.Checked = True Then
            txtJTirEndMth.Text = String.Format("{0:00}", Today.Month)
            txtJTirEndYr.Text = Today.Year.ToString
            Dim StMonth As String = ""
            Dim StYear As String = ""
            JTas.JTasEarliestInvoiceDate(StMonth, StYear)
            txtJTirStMth.Text = String.Format("{0:00}", Val(StMonth))
            txtJTirStYr.Text = StYear
        End If
    End Sub
    ''' <summary>
    ''' Overall software manager to build Invoice Report. Called by the proceed button click.
    ''' </summary>
    Sub JTirBldPrntRprt()
        Dim SinceMidnight As TimeSpan = DateTime.Now - DateTime.Today
        Dim secs As Double = SinceMidnight.TotalSeconds
        Dim JulDate As String = Gregarian2Julian(Date.Now)
        Dim JTirFileRef As String = "JIMInvoiceRprt"
        '
        Try
            Using fs As System.IO.FileStream = New FileStream(JTVarMaint.JTvmFilePath & JTirFileRef & ".PDF", FileMode.Create)

                Dim mydoc As Document = New Document(iTextSharp.text.PageSize.LETTER.Rotate(), 30, 10, 42, 35)
                ' 30, 10, 42, 35
                Dim writer As PdfWriter = PdfWriter.GetInstance(mydoc, fs)
                Dim ev As New JTiritsEvents
                writer.PageEvent = ev


                mydoc.AddTitle("JIM Invoice History Report")
                mydoc.Open()
                '
                ' -------------------------------------------------------------------------
                ' Retrieve date from JTas.slJTas that need exporting
                ' Primary Invoice Number Series.
                ' -------------------------------------------------------------------------
                Dim tmpRprtName As String = "IR"
                Dim tmpExptMode As String = "P"
                '
                If cboxAlternate.Checked = True Then
                    tmpExptMode = "A"
                End If
                If cboxPrimary.Checked = True And cboxAlternate.Checked = True Then
                    tmpExptMode = "B"
                End If
                '
                Dim tmpStMth As String = txtJTirStMth.Text
                Dim tmpStYr As String = txtJTirStYr.Text
                Dim tmpEndMth As String = txtJTirEndMth.Text
                Dim tmpEndYr As String = txtJTirEndYr.Text
                Dim tmpStMthEndYr As String = txtJTirEndYr.Text
              '
                Dim tmpCurrRow As Integer = 0
                '
                Dim tmpSLihItem As JTas.JTasIHStru = Nothing
                '
                Do While JTas.JTasirExt(tmpRprtName, tmpExptMode, tmpCurrRow,
                                        tmpStMth, tmpStYr, tmpEndMth, tmpEndYr,
                                        tmpSLihItem) = "DATA"
                    Dim tmpSLInvKey As String = Gregarian2Julian(tmpSLihItem.ihInvDate) &
                        tmpSLihItem.ihInvNum & tmpSLihItem.ihJobID & tmpSLihItem.ihSubID
                    ' ------------------------------------------------------------------------
                    ' Code to check for duplicate records. They should not exist, but, if they
                    ' do they will be dropped.
                    ' ----------------------
                    ' A MSGBOX will display the key for the record being dropped.
                    ' ------------------------------------------------------------------------
                    '
                    If slJTir.Contains(tmpSLInvKey) = True Then
                        MsgBox("Duplicate invoice record found in History File." & vbCrLf & "Record will be dropped." & vbCrLf &
                               "Key ---> " & tmpSLInvKey & vbCrLf & "Write key down for Ed ---- JTir~200")
                    Else
                        slJTir.Add(tmpSLInvKey, tmpSLihItem)
                    End if
                Loop
                '
                If slJTir.Count > 0 Then
                    Dim phReportDateTitle As New Phrase(FP_H10("Invoices included on this report are between the following dates: "))
                    Dim phReportDateRange As New Phrase(FP_H10("Produced during or after " & tmpStMth & "/" & tmpStYr &
                                                        " and before or during " & tmpEndMth & "/" & tmpEndYr))
                    '
                    Dim phUnderscores As New Phrase("__________________________________________________________________")
                    '
                    '       mydoc.Add(Chunk.NEWLINE)
                    mydoc.Add(phUnderscores)
                    mydoc.Add(Chunk.NEWLINE)
                    mydoc.Add(phReportDateTitle)
                    mydoc.Add(Chunk.NEWLINE)
                    mydoc.Add(phReportDateRange)
                    mydoc.Add(Chunk.NEWLINE)
                    mydoc.Add(phUnderscores)
                    mydoc.Add(Chunk.NEWLINE)
                    '
                    JTirPrtRprt(mydoc)
                    slJTir.Clear()   ' clear table to free memory
                Else
                    MsgBox("No invoices extracted for report" & vbCrLf &
                        "Be sure beginning and ending Mth/Yrs are Correct" & vbCrLf &
                           "OPERATION ABORTED")
                    Me.Close()
                    JTMainMenu.JTMMinit()
                End If
                '
                '
                Dim JIMwhtFooter As New Phrase(FP_H8Blue("Information managed and reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                mydoc.Add(JIMwhtFooter)
                '
                mydoc.Close()
                '
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        JTvPDF.JTvPDFDispPDF(JTVarMaint.JTvmFilePath & JTirFileRef & ".PDF")
        ' deleting temporary pdf report file  
        '       MsgBox("Invoice Report built - Delete of PDF file omitted - JTir~237")
        My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & JTirFileRef & ".PDF")
        '
        Me.Close()
        If JTye.JTyeInProgress = True Then
            Exit Sub
        End If
        JTMainMenu.JTMMinit()
    End Sub
    ''' <summary>
    ''' Builds a printline for an invoice being printed on the Invoice Report.
    ''' </summary>
    ''' <param name="mydoc">the itext document for the entire report</param>
    ''' <returns>Nothing</returns>
    Function JTirPrtRprt(ByRef mydoc As Document) As String
        ' -------------------------------------------------------------
        ' Sub to build and print table for Invoices
        ' -------------------------------------------------------------
        Dim table As New PdfPTable(13)
        ' actual width of table in points
        table.TotalWidth = 700.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2   ' reprint first & second rows at top of each page
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single =
           {1.1F, 0.6F, 1.9F, 1.0F, 1.0F, 1.0F, 1.0F, 0.9F, 1.0F, 0.9F, 1.3F, 1.0F, 1.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
        '
        ' leave a gap before And after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim cell = New PdfPCell(New Phrase(FP_H10("Invoices")))
        '
        cell.Colspan = 13
        cell.Border = 0
        cell.HorizontalAlignment = 0
        table.AddCell(cell)
        '
        Dim TitleCell = New PdfPCell(New Phrase(FP_H10("Inv.Dt")))    ' col 1
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Inv #")))          ' col 2
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Job")))            ' col 3
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Labor")))          ' col 4
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Material")))       ' col 5
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Services")))       ' col 6
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("InHouse")))        ' col 7
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Adjust")))         ' col 8
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("AUP")))            ' col 9
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Tax")))          ' col 10
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Total")))          ' col 11
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Est. Mrgn")))      ' col 12
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        TitleCell = New PdfPCell(New Phrase(FP_H10("Tx Info")))        ' col 13
        TitleCell.HorizontalAlignment = 1
        table.AddCell(TitleCell)
        '
        Dim tmpLstItemYYYYMM As String = ""
        '

        ' clear out totals areas
        JTirClrSubTls()
        tmpGrdTls = tmpSubTls

        '
        Dim key As ICollection = slJTir.Keys
        Dim k As String = ""
        '

        For Each k In key
            Dim tmpInvDate As Date = CDate(slJTir(k).ihInvDate)

            Dim tmpInvYYYYMM As String = String.Format("{0:0000}", tmpInvDate.Year) &
                String.Format("{0:00}", tmpInvDate.Month)
            If tmpLstItemYYYYMM = "" Then
                tmpLstItemYYYYMM = tmpInvYYYYMM
            End If
            '
            If tmpLstItemYYYYMM <> tmpInvYYYYMM Then
                ' Print monthly total
                JtirPrtMthTls(tmpLstItemYYYYMM, table)
                '
                tmpLstItemYYYYMM = tmpInvYYYYMM
            End If
            'add to subtotal areas
            tmpSubTls.ihLab = Val(tmpSubTls.ihLab) + Val(slJTir(k).ihLab)
            tmpSubTls.ihMat = Val(tmpSubTls.ihMat) + Val(slJTir(k).ihMat)
            tmpSubTls.ihSub = Val(tmpSubTls.ihSub) + Val(slJTir(k).ihSub)
            tmpSubTls.ihAdj = Val(tmpSubTls.ihAdj) + Val(slJTir(k).ihAdj)
            tmpSubTls.ihIH = Val(tmpSubTls.ihIH) + Val(slJTir(k).ihIH)
            tmpSubTls.ihInvTl = Val(tmpSubTls.ihInvTl) + Val(slJTir(k).ihInvTl)
            tmpSubTls.ihEstMrgn = Val(tmpSubTls.ihEstMrgn) + Val(slJTir(k).ihEstMrgn)
            tmpSubTls.ihAUPInv = Val(tmpSubTls.ihAUPInv) + Val(slJTir(k).ihAUPInv)
            tmpSubTls.ihTaxableSales = Val(tmpSubTls.ihTaxableSales) + Val(slJTir(k).ihTaxableSales)
            tmpSubTls.ihTaxAmount = Val(tmpSubTls.ihTaxAmount) + Val(slJTir(k).ihTaxAmount)
            '
            '

            Dim ArrayData As String = slJTir(k).ihInvDate          ' Column 1 Inv Date
            Dim CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
            CellContents.HorizontalAlignment = 1
            table.AddCell(CellContents)
            '
            ArrayData = slJTir(k).ihInvNum                          ' Column 2 Inv Number
            CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
            CellContents.HorizontalAlignment = 1
            table.AddCell(CellContents)
            '
            ArrayData = slJTir(k).ihJobID                         ' Column 3 Job:Subjob
            If slJTir(k).ihSubID <> "" Then
                ArrayData = ArrayData & ":" & slJTir(k).ihSubID
            End If
            CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
            CellContents.HorizontalAlignment = 0
            table.AddCell(CellContents)
            '
            JTirFillFigures(slJTir(k), table)   ' common function to fill amount fields

            '
            ArrayData = ""                                               ' Column 13 Tax Info
            If Val(slJTir(k).ihTaxableSales) <> 0 Then
                ArrayData = slJTir(k).ihWorkLocState & " " &
                    (String.Format("{0:0.00}", Val(slJTir(k).ihTaxableSales)))
            End If
            CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
            CellContents.HorizontalAlignment = 2
            table.AddCell(CellContents)
            '
            '
        Next
        '
        JtirPrtMthTls(tmpLstItemYYYYMM, table)
        '
        tmpLstItemYYYYMM = "Grand Totals"
        JtirPrtMthTls(tmpLstItemYYYYMM, table)

        mydoc.Add(table)
        '
        Return ""
        '
    End Function
    '
    ''' <summary>
    ''' Fills the amount field on the printline of the Invoice Report.
    ''' </summary>
    ''' <param name="DataItem">Represents the data to be printed. It might be for a single invoice, a monthly total or the grand total for the report.</param>
    ''' <param name="table">This is the printline table defined in JTirPrtRprt.</param>
    ''' <returns>Nothing</returns>
    Public Function JTirFillFigures(DataItem As JTas.JTasIHStru, ByRef table As PdfPTable) As String

        Dim ArrayData As String = ""                                               ' Column 4 Labor
        If Val(DataItem.ihLab) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihLab)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 5 Material
        If Val(DataItem.ihMat) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihMat)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 6 Sub
        If Val(DataItem.ihSub) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihSub)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 7 InHouse
        If Val(DataItem.ihIH) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihIH)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 8 Adj
        If Val(DataItem.ihAdj) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihAdj)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 9 AUP
        If Val(DataItem.ihAUPInv) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihAUPInv)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 10 Tax
        If Val(DataItem.ihTaxAmount) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihTaxAmount)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 11 InvTl
        If Val(DataItem.ihInvTl) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihInvTl)))
        End If
        JTirFillCell(ArrayData, table)
        '
        ArrayData = ""                                               ' Column 12 Est. Mrgn
        If Val(DataItem.ihEstMrgn) <> 0 Then
            ArrayData = (String.Format("{0:0.00}", Val(DataItem.ihEstMrgn)))
        End If
        JTirFillCell(ArrayData, table)
        '
        Return ""
    End Function
    ''' <summary>
    ''' This is a utiliity sub used to fill a specific cell in the print table. It is call from multiple locations.
    ''' </summary>
    ''' <param name="ArrayData">This is the data to be format for the print table</param>
    ''' <param name="table">This is the printline table defined in JTirPrtRprt.</param>
    ''' <returns>Nothing</returns>
    Public Function JTirFillCell(ArrayData As String, ByRef table As PdfPTable) As String
        Dim CellContents = New PdfPCell(New Phrase(FP_H10(ArrayData)))
        If Val(ArrayData) > 99999.99 Or Val(ArrayData) < -9999.99 Then
            CellContents = New PdfPCell(New Phrase(FP_H8(ArrayData)))
        End If
        CellContents.HorizontalAlignment = 2
        table.AddCell(CellContents)
        Return ""
    End Function
    ''' <summary>
    ''' Utility to clear the total area that are used to save Monthly and grand totals.
    ''' </summary>
    ''' <returns>Nothing</returns>
    Private Function JTirClrSubTls()
        tmpSubTls.ihLab = 0
        tmpSubTls.ihMat = 0
        tmpSubTls.ihSub = 0
        tmpSubTls.ihAdj = 0
        tmpSubTls.ihIH = 0
        tmpSubTls.ihInvTl = 0
        tmpSubTls.ihEstMrgn = 0
        tmpSubTls.ihAUPInv = 0
        tmpSubTls.ihTaxableSales = 0
        tmpSubTls.ihTaxAmount = 0
        '
        Return ""
    End Function
    ''' <summary>
    ''' Sub to print Monthly Totals on Invoice Report. It also adds figure to the grand total fields for later printing.
    ''' </summary>
    ''' <param name="tmpLstItemYYYYMM">Date used to format the date of the total on the printline</param>
    ''' <param name="table">This is the printline table defined in JTirPrtRprt.</param>
    ''' <returns>Nothing</returns>
    Private Function JtirPrtMthTls(tmpLstItemYYYYMM As String, ByRef table As PdfPTable) As String
        Dim tmpCellContents As String = tmpLstItemYYYYMM
        If tmpLstItemYYYYMM <> "Grand Totals" Then
            tmpCellContents = tmpLstItemYYYYMM.Substring(4, 2) &
                            "/" & tmpLstItemYYYYMM.Substring(0, 4) & " Totals"
        End If
        Dim cell = New PdfPCell(New Phrase(FP_H10(tmpCellContents)))
        cell.Colspan = 3
        '      cell.Border = 0
        cell.HorizontalAlignment = 0
        table.AddCell(cell)
        '
        If tmpLstItemYYYYMM <> "Grand Totals" Then
            JTirFillFigures(tmpSubTls, table)
        Else
            JTirFillFigures(tmpGrdTls, table)
        End If
        JTirFillCell(" ", table) 'blank out 13th cell
        '
        If tmpLstItemYYYYMM <> "Grand Totals" Then
            cell = New PdfPCell(New Phrase(FP_H10(" ")))
            cell.Colspan = 13
            cell.Border = 0
            cell.HorizontalAlignment = 0
            table.AddCell(cell)
        End If
        ' Update Grand tl areas
        tmpGrdTls.ihLab = Val(tmpGrdTls.ihLab) + Val(tmpSubTls.ihLab)
        tmpGrdTls.ihMat = Val(tmpGrdTls.ihMat) + Val(tmpSubTls.ihMat)
        tmpGrdTls.ihSub = Val(tmpGrdTls.ihSub) + Val(tmpSubTls.ihSub)
        tmpGrdTls.ihAdj = Val(tmpGrdTls.ihAdj) + Val(tmpSubTls.ihAdj)
        tmpGrdTls.ihIH = Val(tmpGrdTls.ihIH) + Val(tmpSubTls.ihIH)
        tmpGrdTls.ihInvTl = Val(tmpGrdTls.ihInvTl) + Val(tmpSubTls.ihInvTl)
        tmpGrdTls.ihEstMrgn = Val(tmpGrdTls.ihEstMrgn) + Val(tmpSubTls.ihEstMrgn)
        tmpGrdTls.ihAUPInv = Val(tmpGrdTls.ihAUPInv) + Val(tmpSubTls.ihAUPInv)
        tmpGrdTls.ihTaxableSales = Val(tmpGrdTls.ihTaxableSales) + Val(tmpSubTls.ihTaxableSales)
        tmpGrdTls.ihTaxAmount = Val(tmpGrdTls.ihTaxAmount) + Val(tmpSubTls.ihTaxAmount)
        '
        JTirClrSubTls()
        Return ""
    End Function

End Class
