﻿Imports System.IO
Public Class JTtk
    Public Structure JTtkArrayStructure

        Dim JTtkYrWkNum As String
        Dim JTtkEmpID As String
        Dim JTtkLabCat As String
        Dim JTtkWEDate As String
        Dim JTtkWorkDate As String
        Dim JTtkJobID As String
        Dim JTtkSubID As String
        Dim JTtkActDescrpt As String
        Dim JTtkHours As String
        Dim JTtkRate As String
        Dim JTtkRateCat As String
        Dim JTtkOverTime As String
        Dim JTtkDolAmt As String
        Dim JTtkInvDate As String
        Dim JTtkInvNumber As String
    End Structure
    ' --------------------------------------------------------------
    ' For use in function to get total hours logged for JTdb
    ' --------------------------------------------------------------
    Public Structure tkHourSummary
        Dim tkHSJulDate As String
        Dim tkHSGregDate As String
        Dim tkHSHours As Double
    End Structure

    Public JTtkLastItemEntered As New JTtkArrayStructure

    '
    Public JTtkSLSession As SortedList = New SortedList
    '
    Public JTtkslReservoir As SortedList = New SortedList

    Dim JTtkSLSeq As Integer = 0
    Dim JTtkHoursLogged As Double = 0
    Dim JTtkTotlAmount As Double = 0
    Dim saveJTtkEmpID As String
    Dim JTtkWEDate As Date
    Dim SavetkYrWkNum As String
    Dim JTtkKeyCollPos As Integer = 0
    Dim JTtkChangesThisSession As Boolean = False
    '
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ' ------------------------------------------
        ' Sub to cancel timekeeping screen.
        ' ------------------------------------------
        JTtkChangesThisSession = False
        Me.Hide()
        MsgBox("Timekeeping function cancelled." & vbCrLf & "Any entries made on this screen will be cancelled.(TK.001)")
        JTMainMenu.JTMMinit()
    End Sub
    Public Sub JTtkInit(JTtkEmpID As String, JTtkWEDate As String)
        '
        SaveJTtkEmpID = JTtkEmpID
        '
        Dim tmpPOS As Integer
        Dim tmpEmpName As String = ""
        Dim tmpLabCat As String = ""
        Dim tmpPayMeth As String = ""

        If JThr.JThrFindFunc(tmpPOS, JTtkEmpID, tmpEmpName, tmpLabCat, tmpPayMeth) <> "DATA" Then
            MsgBox("Employee not found. Timekeeping aborted.")
            Exit Sub
        End If
        txtJTtkEmpID.Text = JTtkEmpID
        txtJTtkEmpName.Text = tmpEmpName
        txtJTtkLabCode.Text = tmpLabCat
        ' ------------------------------------------------------------
        ' Calculate last Saturdays date based on today's date and show
        ' on the panel as a suggestion.
        ' ------------------------------------------------------------
        Dim tmpDate As Date = Today
        Dim tmpDayofWeek As Integer = tmpDate.DayOfWeek
        Dim tmpLastSatDate As Date = tmpDate.AddDays((tmpDayofWeek + 1) * -1)
        JTtkWEDate = tmpLastSatDate
        '
        mtxtJTtkWEDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)

        JTtkRestorePastEntries()
    End Sub
    Public Sub JTtkRestorePastEntries()

        ' -------------------------------------------------------------------------
        ' Check to see if any entries have been made for this YrWk/EmpID before.
        ' If yes, restore data into JTtkSLSession.
        ' -------------------------------------------------------------------------
        '
        Dim tmpDone As String = ""
        Dim tmpFileSeq As Integer = 10
        Dim tmpYrWkNum As String = ""
        Dim tmpWEDate As Date = CDate(mtxtJTtkWEDate.Text)
        Dim tmpWeekNo As Integer = 0
        tmpWeekNo = (tmpWEDate.DayOfYear / 7) + 1
        If tmpWeekNo < 10 Then
            tmpYrWkNum = (Str(tmpWEDate.Year) & "0" & Str(tmpWeekNo).Trim(" "))
        Else
            tmpYrWkNum = (Str(tmpWEDate.Year) & Str(tmpWeekNo).Trim(" "))
        End If
        '
        JTtkSLSession.Clear()
        '
        Do Until tmpDone = "DONE"
            Dim tmpKey As String = tmpYrWkNum & SaveJTtkEmpID & Str(tmpFileSeq).Trim(" ")
            SavetkYrWkNum = tmpYrWkNum   ' save to be used when clearinf items from reservoir
            '
            If JTtkslReservoir.ContainsKey(tmpKey) Then

                Dim tmpResItmArea As New JTtkArrayStructure
                'move fields
                tmpResItmArea.JTtkYrWkNum = JTtkslReservoir(tmpKey).JTtkYrWkNum
                tmpResItmArea.JTtkEmpID = JTtkslReservoir(tmpKey).JTtkEmpID
                tmpResItmArea.JTtkLabCat = JTtkslReservoir(tmpKey).JTtkLabCat
                tmpResItmArea.JTtkWEDate = JTtkslReservoir(tmpKey).JTtkWEDate
                tmpResItmArea.JTtkWorkDate = JTtkslReservoir(tmpKey).JTtkWorkDate
                tmpResItmArea.JTtkJobID = JTtkslReservoir(tmpKey).JTtkJobID
                tmpResItmArea.JTtkSubID = JTtkslReservoir(tmpKey).JTtkSubID
                tmpResItmArea.JTtkActDescrpt = JTtkslReservoir(tmpKey).JTtkActDescrpt
                tmpResItmArea.JTtkHours = JTtkslReservoir(tmpKey).JTtkHours
                tmpResItmArea.JTtkRate = JTtkslReservoir(tmpKey).JTtkRate
                tmpResItmArea.JTtkRateCat = JTtkslReservoir(tmpKey).JTtkRateCat
                tmpResItmArea.JTtkOverTime = JTtkslReservoir(tmpKey).JTtkOverTime
                tmpResItmArea.JTtkDolAmt = JTtkslReservoir(tmpKey).JTtkDolAmt
                tmpResItmArea.JTtkInvDate = JTtkslReservoir(tmpKey).JTtkInvDate
                '
                Dim tmpSLKey As String = tmpResItmArea.JTtkWorkDate & tmpResItmArea.JTtkJobID & tmpResItmArea.JTtkSubID & Str(tmpFileSeq)
                JTtkSLSession.Add(tmpSLKey, tmpResItmArea)
                '
            Else
                tmpDone = "DONE"
            End If
            tmpFileSeq += 1
        Loop
        '
        '

        '
        ' -----------------------------------------------------------
        ' Call sub to fill lv.
        ' -----------------------------------------------------------
        FilllvJTtkData()
        txtJTtkDate.Text = ""
        txtJTtkJobID.Text = ""
        txtJTtkSubJob.Text = ""
        txtJTtkActivity.Text = ""
        txtJTtkHours.Text = ""
        radiobtnJTtkStanRate.Checked = True
        chkboxJTtkOTRate.Checked = False
        txtErrMsg.Text = ""


        txtJTtkDate.Select()
        '
        Dim JTtkJobList As New AutoCompleteStringCollection
        Dim tmpJobSub = 0
        Dim tmpJobName As String = ""
        Dim tmpSubName As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpCstMeth As String = ""
        Dim tmpLstJob As String = Nothing
        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpDeactDate = "" Then
                If tmpLstJob <> tmpJobName Then
                    JTtkJobList.Add(tmpJobName)
                    tmpLstJob = tmpJobName
                End If
            End If
                tmpJobSub += 1
        Loop
        'Then Set ComboBox AutoComplete properties
        txtJTtkJobID.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTtkJobID.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTtkJobID.AutoCompleteCustomSource = JTtkJobList
        '
        ' ----------------------------------------------------------------------
        ' Create autocompletestringcollection for  txtJTtkActivity
        ' ----------------------------------------------------------------------
        Dim slTmpActivityDescrip As New SortedList
        Dim JTtkActivityList As New AutoCompleteStringCollection
        Dim keys As ICollection = JTtkslReservoir.Keys
        Dim k As String = ""
        For Each k In keys
            If slTmpActivityDescrip.ContainsKey(JTtkslReservoir(k).JTtkActDescrpt) = False Then
                slTmpActivityDescrip.Add(JTtkslReservoir(k).JTtkActDescrpt, "X")
            End If
        Next
        keys = slTmpActivityDescrip.Keys
        For Each k In keys
            JTtkActivityList.Add(k)
        Next
        slTmpActivityDescrip.Clear()

        '
        'Then Set ComboBox AutoComplete properties
        txtJTtkActivity.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTtkActivity.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTtkActivity.AutoCompleteCustomSource = JTtkActivityList
        '

        '

        Me.Show()

    End Sub
    Private Sub txtJTtkJobID_Leave(sender As Object, e As EventArgs) Handles txtJTtkJobID.Leave

        txtJTtkJobID.Text = txtJTtkJobID.Text.ToUpper
        txtErrMsg.Text = ""
        Dim tmpPOS As String = ""
        Dim tmpJobId As String = txtJTtkJobID.Text
        Dim tmpSubName As String = ""
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpCstMeth As String = ""
        '     If JTjim.JTjimFindFunc(tmpPOS, tmpJobId, tmpSubName, tmpJobType, tmpDeactDate) <> "DATA" Then
        If JTjim.JTjimFindCI(txtJTtkJobID.Text) = False Then
            txtErrMsg.Text = "Invalidate Job Name entered ... Correct and resubmit."
            txtJTtkJobID.Focus()
            Exit Sub
        End If
        Dim tmpSub As Integer = 0
        Dim tmpJobName As String = Nothing
        Dim tmpJobType2 As String = Nothing
        If JTjim.JTjimCntCustJobs(txtJTtkJobID.Text) = 1 Then
            Do While JTjim.JTjimReadFunc(tmpSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType2, tmpCstMeth) = "DATA"
                If tmpJobName = txtJTtkJobID.Text And
                       tmpDeactDate = "" Then
                    txtJTtkSubJob.Text = tmpSubName
                    txtJTtkSubJob.ReadOnly = True
                    txtJTtkActivity.Select()
                    Exit Sub
                End If
                tmpSub += 1
            Loop
        End If
        '
        '
        Dim JTtkSubList As New AutoCompleteStringCollection
        tmpSub = 0
        '     '
        Do While JTjim.JTjimReadFunc(tmpSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpJobName = txtJTtkJobID.Text And tmpDeactDate = "" And
                tmpSubName <> "" Then
                '               MsgBox(">" & tmpSubName & "< - JTtk~247")
                JTtkSubList.Add(tmpSubName)
            End If
            tmpSub += 1
        Loop
        'Then Set ComboBox AutoComplete properties
        txtJTtkSubJob.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTtkSubJob.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTtkSubJob.AutoCompleteCustomSource = JTtkSubList
        '
        txtJTtkSubJob.ReadOnly = False
        txtJTtkSubJob.Select()
        '     End If
    End Sub
    Private Sub txtJTtkSubJob_Leave(sender As Object, e As EventArgs) Handles txtJTtkSubJob.Leave

        txtJTtkSubJob.Text = txtJTtkSubJob.Text.ToUpper
        Dim tmpPOS As String = ""

        Dim tmpJobId As String = txtJTtkJobID.Text
        Dim tmpSubName As String = txtJTtkSubJob.Text
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        If JTjim.JTjimFindFunc(tmpPOS, tmpJobId, tmpSubName, tmpJobType, tmpDeactDate) <> "DATA" Then
            txtErrMsg.Text = "Invalidate SubJob Name entered ... Correct and resubmit.(TK.002)"
            txtJTtkSubJob.Focus()
            Exit Sub
        End If
        If tmpDeactDate <> "" Then
            txtErrMsg.Text = "SubJob has been Deactivated ... Correct and resubmit."
            txtJTtkJobID.Focus()
            Exit Sub
        End If

    End Sub
    Private Sub btnJTtkRecord_Click(sender As Object, e As EventArgs) Handles btnJTtkRecord.Click
        ' ----------------------------------------------------------------------------------------------------
        ' If this btn is activated, the entry has been accepted. Logic is run in this sub to add the line to 
        ' Listview.
        ' ----------------------------------------------------------------------------------------------------
        '
        ' Do final date check.
        If txtJTtkDate.Text = "" Or
             ChkTKDate() = False Then
            ToolTip1.Show("Date entered for timekeeping is not valid or outside of work week.(TK.006)", txtJTtkDate, 20, -50, 6000)
            txtJTtkDate.Select()
        End If
        '
        JTtkChangesThisSession = True
        ' ********** Save keyed values for use by the "d" option on the next entry. **********
        '
        If Val(txtJTtkHours.Text) <> 0 Then
            JTtkLastItemEntered.JTtkJobID = txtJTtkJobID.Text
            JTtkLastItemEntered.JTtkSubID = txtJTtkSubJob.Text
            JTtkLastItemEntered.JTtkActDescrpt = txtJTtkActivity.Text.Trim()
            JTtkLastItemEntered.JTtkHours = txtJTtkHours.Text
            If radiobtnJTtkStanRate.Checked = True Then
                JTtkLastItemEntered.JTtkRateCat = "Stan"
            Else
                JTtkLastItemEntered.JTtkRateCat = "Shop"
            End If
            If chkboxJTtkOTRate.Checked = True Then
                JTtkLastItemEntered.JTtkOverTime = "X"
            Else
                JTtkLastItemEntered.JTtkOverTime = ""
            End If
            '
            '
            ' ************************ Listview update code goes here ********************
            '
            Dim tmpJustEntered As New JTtkArrayStructure
            Dim tmpWEDate As Date = CDate(mtxtJTtkWEDate.Text)
            Dim tmpWeekNo As Integer = 0
            tmpWeekNo = (tmpWEDate.DayOfYear / 7) + 1
            If tmpWeekNo <10 Then
                tmpJustEntered.JTtkYrWkNum= (Str(tmpWEDate.Year) & "0" & Str(tmpWeekNo).Trim(" "))
            Else
                tmpJustEntered.JTtkYrWkNum = (Str(tmpWEDate.Year) & Str(tmpWeekNo).Trim(" "))
            End If
            SavetkYrWkNum = tmpJustEntered.JTtkYrWkNum
            tmpJustEntered.JTtkEmpID = txtJTtkEmpID.Text
            tmpJustEntered.JTtkLabCat = txtJTtkLabCode.Text
            tmpJustEntered.JTtkWEDate = mtxtJTtkWEDate.Text.Substring(0, 10)
            tmpJustEntered.JTtkWorkDate = txtJTtkDate.Text
            tmpJustEntered.JTtkJobID = txtJTtkJobID.Text
            tmpJustEntered.JTtkSubID = txtJTtkSubJob.Text
            tmpJustEntered.JTtkActDescrpt = txtJTtkActivity.Text.Trim()
            tmpJustEntered.JTtkHours = txtJTtkHours.Text
            ' ----------------------------------------------------------
            ' go get labor rates
            ' ---- this needs to be enhanced for job specific pricing
            ' ----------------------------------------------------------
            Dim tmpPOS As Integer = 0
            Dim tmpLCKey As String = txtJTtkLabCode.Text
            Dim tmpLCName As String = ""
            Dim tmpLCStanRate As String = ""
            Dim tmpLCShopRate As String = ""
            tmpJustEntered.JTtkRate = ""
            Dim tmpJSRStan As String = ""
            Dim tmpJSRShop As String = ""
            Dim tmpJSRPricing As String = ""

            tmpJSRPricing = "N"
            If JTjim.JTjimJSRLkUp(txtJTtkJobID.Text, txtJTtkSubJob.Text, tmpLCKey, tmpJSRStan, tmpJSRShop) = "DATA" Then
                tmpJSRPricing = "Y"
            End If

            If LaborCategories.JTlcFindFunc(tmpPOS, tmpLCKey, tmpLCName, tmpLCStanRate, tmpLCShopRate) = "DATA" Then
                If radiobtnJTtkStanRate.Checked = True Then
                    If tmpJSRPricing = "Y" Then
                        tmpJustEntered.JTtkRate = tmpJSRStan
                    Else
                        tmpJustEntered.JTtkRate = tmpLCStanRate
                    End If
                    tmpJustEntered.JTtkRateCat = "Stan"
                Else
                    If tmpJSRPricing = "Y" Then
                        tmpJustEntered.JTtkRate = tmpJSRShop
                    Else
                        tmpJustEntered.JTtkRate = tmpLCShopRate
                    End If
                    tmpJustEntered.JTtkRateCat = "Shop"
                End If
            End If
            Dim tmpOTFactor As Double = 1.0
            If chkboxJTtkOTRate.Checked = True Then
                tmpOTFactor = 1.5
                tmpJustEntered.JTtkOverTime = "X"
            End If
            Dim tmpRate As Double = Val(tmpJustEntered.JTtkRate)
            Dim tmpHours As Double = Val(txtJTtkHours.Text)
            tmpJustEntered.JTtkDolAmt = Str(tmpRate * tmpHours * tmpOTFactor)
            tmpJustEntered.JTtkInvDate = ""
            ' ----------------------------------------
            ' update mainmenu lv with new TK wedate
            ' ----------------------------------------
            JTMainMenu.JTMMTKArrayDateUpdate(txtJTtkEmpID.Text, mtxtJTtkWEDate.Text.Substring(0, 10))
            ' -------------------------------------------
            ' Update Last Activity Date on jim record.
            '--------------------------------------------
            JTjim.JTjimUpdtActDate(txtJTtkJobID.Text, txtJTtkSubJob.Text)
            ' -----------------------------------------------------------
            ' call routine to update mainmenu lv with not inv'd activity
            ' -----------------------------------------------------------
            ' call deleted ... also done when total weeks timekeeping processed.
            '      JTMainMenu.JTMMNotInvAmts(txtJTtkJobID.Text, txtJTtkSubJob.Text, Val(tmpJustEntered.JTtkDolAmt), 0, 0)
            ' --------------------------
            ' build key for sorted list
            ' --------------------------
            JTtkSLSeq += 1
            Dim tmpSLKey As String = txtJTtkDate.Text & txtJTtkJobID.Text & txtJTtkSubJob.Text & Str(JTtkSLSeq)
            JTtkSLSession.Add(tmpSLKey, tmpJustEntered)
        End If
        '
        ' -----------------------------------------------------------
        ' Call sub to fill lv.
        ' -----------------------------------------------------------
        '
        FilllvJTtkData()
        '
        txtJTtkDate.Text = ""
        txtJTtkJobID.Text = ""
        txtJTtkSubJob.Text = ""
        txtJTtkActivity.Text = ""
        txtJTtkHours.Text = ""
        radiobtnJTtkStanRate.Checked = True
        radiobtnJTtkShopRate.Checked = False
        chkboxJTtkOTRate.Checked = False
        txtJTtkDate.Select()
        '
    End Sub

    Private Sub txtJTtkDate_TextChanged(sender As Object, e As EventArgs) Handles txtJTtkDate.TextChanged
        If txtJTtkDate.Text.ToUpper = "D" Then
            txtJTtkDate.Text = ""
            ' ----------------------------------------------------------------
            ' code to copy last item keyed data to current fields.
            ' ----------------------------------------------------------------
            txtJTtkJobID.Text = JTtkLastItemEntered.JTtkJobID
            txtJTtkSubJob.Text = JTtkLastItemEntered.JTtkSubID
            txtJTtkActivity.Text = JTtkLastItemEntered.JTtkActDescrpt
            txtJTtkHours.Text = JTtkLastItemEntered.JTtkHours
            If JTtkLastItemEntered.JTtkRateCat = "Stan" Then
                radiobtnJTtkStanRate.Checked = True
            Else
                radiobtnJTtkShopRate.Checked = True
            End If

            If JTtkLastItemEntered.JTtkOverTime = "X" Then
                chkboxJTtkOTRate.Checked = True
            End If
            Exit Sub
        End If
        JTtkWEDate = CDate(mtxtJTtkWEDate.Text)
        '
        Select Case txtJTtkDate.Text.ToUpper
            Case "U"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-6)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "M"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-5)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "T"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-4)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "W"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-3)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "R"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-2)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "F"
                Dim tmpwkdate As Date = JTtkWEDate.AddDays(-1)
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '
            Case "S"
                Dim tmpwkdate As Date = JTtkWEDate
                txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", tmpwkdate)
                '

        End Select
        '
    End Sub
    Private Sub txtJTtkDate_LostFocus(sender As Object, e As EventArgs) Handles txtJTtkDate.LostFocus
        If txtJTtkDate.Text <> "" Then
            If ChkTKDate() = False Then
                ToolTip1.Show("Date entered for timekeeping not valid or outside of work week.(TK.006)", txtJTtkDate, 20, -50, 6000)
                txtJTtkDate.Select()
            Else
                txtJTtkJobID.Select()
            End If
        End If
    End Sub
    '
    Private Sub mtxtJTtkWEDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTtkWEDate.LostFocus
        If IsDate(mtxtJTtkWEDate.Text) = False Then
            ToolTip1.Show("Week ending date not a valid date.(TK.003)", mtxtJTtkWEDate, 20, -50, 6000)
            mtxtJTtkWEDate.SelectAll()
            Exit Sub
        End If
        JTtkWEDate = CDate(mtxtJTtkWEDate.Text)
        If JTtkWEDate.DayOfWeek <> 6 Then
            ToolTip1.Show("Week ending date entered not a Saturday date.(TK.004)", mtxtJTtkWEDate, 20, -50, 6000)
            mtxtJTtkWEDate.SelectAll()
            Exit Sub

        End If
        JTtkRestorePastEntries()
    End Sub

    Private Sub lvJTtkData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTtkData.DoubleClick
        ' -------------------------------------------------------------------------------
        ' line in lvJTtkdata selected. Sub will move it to top for re-edit. It deletes
        ' the entry in the lv --- it will be replaced with newly edited entry.
        ' -------------------------------------------------------------------------------
        '
        ' Get a collection of the keys. 
        '
        Dim key As ICollection = JTtkSLSession.Keys
        Dim tmpIndex As Integer = lvJTtkData.SelectedItems(0).Index
        '
        If JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkInvDate <> "" Then
            ToolTip1.Show("Item selected has been invoiced --- it can no longer be modified.(TK.005)", lvJTtkData, 20, -50, 6000)
            FilllvJTtkData()
            Exit Sub
        End If
        txtJTtkDate.Text = JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkWorkDate
        txtJTtkJobID.Text = JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkJobID
        txtJTtkSubJob.Text = JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkSubID
        txtJTtkActivity.Text = JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkActDescrpt
        txtJTtkHours.Text = JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkhours
        If JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkRateCat = "Stan" Then
            radiobtnJTtkStanRate.Checked = True
        Else
            radiobtnJTtkShopRate.Checked = True
        End If
        If JTtkSLSession(key(lvJTtkData.SelectedItems(0).Index)).JTtkOverTime = "X" Then
            chkboxJTtkOTRate.Checked = True
        Else
            chkboxJTtkOTRate.Checked = False
        End If
        JTtkSLSession.Remove(key(lvJTtkData.SelectedItems(0).Index))
        FilllvJTtkData()

    End Sub
    Sub FilllvJTtkData()
        ' --------------------------------------------------------------
        ' sub to fill lvJTtkData. Called from multiple locations.
        ' --------------------------------------------------------------
        '
        ' Logic to make sure ListView is empty.
        '
        lvJTtkData.Items.Clear()

        ' clear total accumulators
        '
        JTtkHoursLogged = 0
        JTtkTotlAmount = 0
        '
        ' Constants for listview
        '
        Dim tmpSub = 0
        Dim itmBuild(10) As String
        Dim lvLine As ListViewItem
        '
        ' Get a collection of the keys. 
        Dim key As ICollection = JTtkSLSession.Keys
        Dim k As String
        For Each k In key
            ' move items to listview

            itmBuild(0) = JTtkSLSession(k).JTtkWorkDate.substring(0, 5)
            itmBuild(1) = JTtkSLSession(k).JTtkJobID
            itmBuild(2) = JTtkSLSession(k).JTtkSubID
            itmBuild(3) = JTtkSLSession(k).JTtkActDescrpt
            itmBuild(4) = String.Format("{0:0.00}", Val(JTtkSLSession(k).JTtkHours))
            itmBuild(5) = JTtkSLSession(k).JTtkRate
            itmBuild(6) = JTtkSLSession(k).JTtkRateCat
            itmBuild(7) = JTtkSLSession(k).JTtkOverTime
            itmBuild(8) = String.Format("{0:0.00}", Val(JTtkSLSession(k).JTtkDolAmt))
            itmBuild(9) = JTtkSLSession(k).JTtkInvDate
            lvLine = New ListViewItem(itmBuild)
            lvJTtkData.Items.Add(lvLine)
            '
            ' Accumulate total of all entries entered
            '
            JTtkHoursLogged = JTtkHoursLogged + Val(JTtkSLSession(k).JTtkHours)
            JTtkTotlAmount = JTtkTotlAmount + Val(JTtkSLSession(k).JTtkDolAmt)
            '
            ' set subscript for next item
            '
            tmpSub += 1
        Next k

        '
        '
        txtJTtkHrsLogged.Text = JTtkHoursLogged
        txtJTtkAmount.Text = JTtkTotlAmount

    End Sub
    ''' <summary>
    ''' This sub writes the data from the JTtkslReservoir sorted list to the JTTimeKeeping.dat file.
    ''' Whenever timekeeping changes are made (new entries, changes, invoicing, etc.) this sub
    ''' is executed to keep the file up-to-date.
    ''' </summary>
    Public Sub TKArrayFlush()
        ' ---------------------------------------------------------------------------
        ' This sub reformats time keeping data into XML sentences and write the data to a file.
        ' ---------------------------------------------------------------------------
        Dim fileRecord As String = ""
        '
        Dim tmpMissingInvDates As Short = 0
        '
        Dim tmpDoingArchive As Boolean = False
        Dim tmpRecordsToArchive As Decimal = 0
        Dim tmpSkipControl As String
        '
        If JTmmArchiveDayCalc(JTVarMaint.LstTKPurge) = "DoArchive" Then
            tmpDoingArchive = True
        End If
        '
        Dim tmpDate As Date
        Dim tmpJulianToday As String
        tmpJulianToday = Format$(Date.Today, "yyyy") & Date.Today.DayOfYear.ToString.PadLeft(3, "0")
        Dim tmpJulianTran As String = ""
        Dim tmpJulianInv As String = ""
        '
        Dim tmpLoopControl As String = "Normal"
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTTimeKeeping.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTTimeKeeping.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        '
        ' Get a collection of the keys.
        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String
        '
        Do While tmpLoopControl <> ""
            Try
                Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
                '
                key = JTtkslReservoir.Keys
                k = ""
                '
                For Each k In key
                    '
                    tmpDate = CDate(JTtkslReservoir(k).JTtkWorkDate)
                    tmpJulianTran = Format$(tmpDate, "yyyy") & tmpDate.DayOfYear.ToString.PadLeft(3, "0")
                    If JTtkslReservoir(k).JTtkInvDate = "" Then
                        tmpJulianInv = tmpJulianToday 'plug today's date into invoice date so it won't be purged.
                    Else
                        tmpDate = CDate(JTtkslReservoir(k).JTtkInvDate)
                        tmpJulianInv = Format$(tmpDate, "yyyy") & tmpDate.DayOfYear.ToString.PadLeft(3, "0")
                    End If
                    Dim tmpWorkDays As Decimal = 0
                    Dim tmpInvDays As Decimal = 0
                    If tmpJulianToday.Substring(0, 4) > tmpJulianTran.Substring(0, 4) Then
                        tmpWorkDays = Val(tmpJulianToday.Substring(4, 3)) + (365 - Val(tmpJulianTran.Substring(4, 3)))
                    Else
                        tmpWorkDays = Val(tmpJulianToday) - Val(tmpJulianTran)
                    End If
                    '
                    If tmpJulianToday.Substring(0, 4) > tmpJulianInv.Substring(0, 4) Then
                        tmpInvDays = Val(tmpJulianToday.Substring(4, 3)) + (365 - Val(tmpJulianInv.Substring(4, 3)))
                    Else
                        tmpInvDays = Val(tmpJulianToday) - Val(tmpJulianInv)
                    End If
                    Dim tmpArchiveEligible As Boolean = True
                    '
                    If tmpWorkDays < JTVarMaint.LabMinLife Or tmpInvDays < JTVarMaint.MinDaysAfterInv Then
                        tmpArchiveEligible = False
                    End If
                    '
                    tmpSkipControl = "Save"
                    '
                    If tmpDoingArchive = True Then
                        If tmpArchiveEligible = True Then
                            If tmpLoopControl = "Normal" Then
                                tmpRecordsToArchive += 1
                                tmpSkipControl = "Drop"
                            End If
                        Else
                            If tmpLoopControl = "Archive" Then
                                tmpSkipControl = "Drop"
                            End If
                        End If
                    End If
                    '
                    If tmpSkipControl = "Save" Then
                        fileRecord = "<tkRecord>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<YrWkNum>" & JTtkslReservoir(k).JTtkYrWkNum & "</YrWkNum>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<EmpID>" & JTtkslReservoir(k).JTtkEmpID & "</EmpID>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<LabCat>" & JTtkslReservoir(k).JTtkLabCat & "</LabCat>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<WEDate>" & JTtkslReservoir(k).JTtkWEDate & "</WEDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<WorkDate>" & JTtkslReservoir(k).JTtkWorkDate & "</WorkDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<JobID>" & JTtkslReservoir(k).JTtkJobID & "</JobID>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If JTtkslReservoir(k).JTtkSubID <> "" Then
                            fileRecord = "<SubID>" & JTtkslReservoir(k).JTtkSubID & "</SubID>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        ' ---------------------------------------------------------------------------
                        ' Code to break records longer than 75 character into multiple short records.
                        ' ---------------------------------------------------------------------------
                        Dim tmpFieldData As String = JTtkslReservoir(k).JTtkActDescrpt.replace(vbLf, "~!")
                        Dim tmpLength As Integer = tmpFieldData.Length
                        Dim tmpFieldPos As Integer = 0
                        Do While tmpLength > 75
                            fileRecord = "<ActDescrpt>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</ActDescrpt>"
                            writeFile.WriteLine(fileRecord)
                            tmpLength -= 75
                            tmpFieldPos += 75
                        Loop
                        If tmpLength > 0 Then
                            fileRecord = "<ActDescrpt>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</ActDescrpt>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "<Hours>" & JTtkslReservoir(k).JTtkHours & "</Hours>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<Rate>" & JTtkslReservoir(k).JTtkRate & "</Rate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<RateCat>" & JTtkslReservoir(k).JTtkRateCat & "</RateCat>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If JTtkslReservoir(k).JTtkOverTime <> "" Then
                            fileRecord = "<OverTime>" & JTtkslReservoir(k).JTtkOverTime & "</OverTime>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "<DolAmt>" & JTtkslReservoir(k).JTtkDolAmt & "</DolAmt>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If JTtkslReservoir(k).JTtkInvDate <> "" Then
                            fileRecord = "<InvDate>" & JTtkslReservoir(k).JTtkInvDate & "</InvDate>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If JTtkslReservoir(k).JTtkInvNumber <> "" Then
                            fileRecord = "<InvNumber>" & JTtkslReservoir(k).JTtkInvNumber & "</InvNumber>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "</tkRecord>"
                        writeFile.WriteLine(fileRecord)
                        '
                    End If
                    '
                Next
                '
                '
                writeFile.Flush()
                writeFile.Close()
                writeFile = Nothing
            Catch ex As IOException
                MsgBox(ex.ToString)
            End Try
            ' ------------------------------------------------------------------------
            ' Recovery logic --- after successful save
            ' ------------------------------------------------------------------------
            If File.Exists(JTVarMaint.JTvmFilePath & "JTTimeKeeping.Bkup") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTTimeKeeping.Bkup")
            End If
            ' ------------------------------------------------------------------------

            '
            If tmpDoingArchive = True And
                   tmpLoopControl = "Normal" And
                   tmpRecordsToArchive <> 0 Then
                MsgBox("Temp Message - TK Archive --- JTtk~760" & vbCrLf &
                       "Total # of records - " & JTtkslReservoir.Count & vbCrLf &
                       "Records to be archived - " & tmpRecordsToArchive)
                tmpLoopControl = "Archive"
                Dim tmpArchiveFileName As String = "JTTKArchive" & tmpJulianToday & ".dat"
                Dim tmpTodayYear As String = Date.Today.Year
                Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "DataArchive\" & tmpTodayYear
                If Not System.IO.Directory.Exists(tmpFilePath) Then
                    System.IO.Directory.CreateDirectory(tmpFilePath)
                End If
                filePath = tmpFilePath & "\" & tmpArchiveFileName
                JTVarMaint.LstTKPurge = tmpJulianToday
            Else
                tmpLoopControl = ""
            End If
            '
        Loop
        '
        ' restore JTtkslReservoir from file to reset after archive.
        '
        If tmpDoingArchive = True Then
            '    MsgBox("Restoring post-purge TK data - JTtk - 781")
            JTtkRestoreTKFile()
        End If
        ' -------------------------------------------------------------------------------------
        ' This is part of the code installed to correct missing invoice dates. It is probably
        ' not needed. I'll comment it out until theory is proven.
        ' -------------------------------------------------------------------------------------
        ' If tmpMissingInvDates > 0 Then
        '     MsgBox("************************** IMPORTANT ****************************" & vbCrLf &
        '            "An error has occurred and it is likely related to an invoice just" & vbCrLf &
        '            "produced. Take a few notes related to what you have been doing just" & vbCrLf &
        '            "prior to this error and call Ed. It's OK to keep going after this." & vbCrLf &
        '            " -------- Records repaired - " & tmpMissingInvDates & " --------")
        ' End If
    End Sub
    ''' <summary>
    ''' This sub takes changes made in a session of timekeeping and updates these changes
    ''' to the overall timekeeping SL (JTtkslReservoir). It then queues TKArrayFlush to
    ''' update the permanent timekeeping file. The sub is queued when the Update Button is
    ''' selected on the TK panel.
    ''' </summary>
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        ' -------------------------------------------------------------------------------
        ' Sub to wind up timekeeping activities for this employee. 
        ' Steps ...
        ' 1. Copy data from this employee to JTtkslReservoir. Check to make sure that 
        '    there weren't prior records for this yr/wk/emp combination.
        ' 2. Flush JTtkslReservoir to disk.
        ' -------------------------------------------------------------------------------
        JTtkChangesThisSession = False
        Dim tmpLoopSW As String = ""
        Dim tmpFileSeq As Integer = 10
        Do While tmpLoopSW <> "DONE"
            Dim tmplvKey As String = SavetkYrWkNum & txtJTtkEmpID.Text & Str(tmpFileSeq).Trim(" ")
            If JTtkslReservoir.ContainsKey(tmplvKey) Then
                If JTtkslReservoir(tmplvKey).JTtkInvDate = "" Then
                    JTMainMenu.JTMMNotInvAmts(JTtkslReservoir(tmplvKey).JTtkJobID, JTtkslReservoir(tmplvKey).JTtkSubID,
                                              Val(JTtkslReservoir(tmplvKey).JTtkDolAmt) * -1, 0, 0)
                End If
                JTtkslReservoir.Remove(tmplvKey)
                tmpFileSeq += 1
            Else
                tmpLoopSW = "DONE"
            End If
        Loop
        ' -----------------------------------------------------------------------
        ' Add elements in JTtkCLSession to JTtkslReservoir.
        ' -----------------------------------------------------------------------
        ' Get a collection of the keys. 
        Dim key As ICollection = JTtkSLSession.Keys
        Dim k As String
        '
        tmpFileSeq = 10
        For Each k In key

            Dim tmplvKey As String = SavetkYrWkNum & txtJTtkEmpID.Text & Str(tmpFileSeq).Trim(" ")
            Dim tmpOPR As New JTtkArrayStructure
            '
            tmpOPR.JTtkYrWkNum = JTtkSLSession(k).JTtkYrWkNum
            tmpOPR.JTtkEmpID = JTtkSLSession(k).JTtkEmpID
            tmpOPR.JTtkLabCat = JTtkSLSession(k).JTtkLabCat
            tmpOPR.JTtkWEDate = JTtkSLSession(k).JTtkWEDate
            tmpOPR.JTtkWorkDate = JTtkSLSession(k).JTtkWorkDate
            tmpOPR.JTtkJobID = JTtkSLSession(k).JTtkJobID
            tmpOPR.JTtkSubID = JTtkSLSession(k).JTtkSubID
            tmpOPR.JTtkActDescrpt = JTtkSLSession(k).JTtkActDescrpt
            tmpOPR.JTtkHours = JTtkSLSession(k).JTtkHours
            tmpOPR.JTtkRate = JTtkSLSession(k).JTtkRate
            tmpOPR.JTtkRateCat = JTtkSLSession(k).JTtkRateCat
            tmpOPR.JTtkOverTime = JTtkSLSession(k).JTtkOverTime
            tmpOPR.JTtkDolAmt = JTtkSLSession(k).JTtkDolAmt
            tmpOPR.JTtkInvDate = JTtkSLSession(k).JTtkInvDate
            '
            JTtkslReservoir.Add(tmplvKey, tmpOPR)
            '
            '
            '

            If tmpOPR.JTtkInvDate = "" Then
                ' update totals of unbilled on mainmenu
                JTMainMenu.JTMMNotInvAmts(tmpOPR.JTtkJobID, tmpOPR.JTtkSubID, Val(tmpOPR.JTtkDolAmt), 0, 0)
                'update last activity date
                JTjim.JTJimUpdtLstActivity(tmpOPR.JTtkJobID, tmpOPR.JTtkSubID, tmpOPR.JTtkWorkDate)
            End If
            '

            '
            tmpFileSeq += 1

            '
        Next
        '
        JTtkSLSession.Clear()
        '
        TKArrayFlush()
        '
        Me.Hide()
        JTMainMenu.JTMMinit()
    End Sub
    ''' <summary>
    ''' This SUB reads the JTTimeKeeping.dat file and populates the JTtkslReservoir sorted list.
    ''' This is the main storage for labor hours within JIM.
    ''' </summary>
    Public Sub JTtkRestoreTKFile()
        ' -----------------------------------------------------------------------------
        ' This sub read the JOBS XML data and rebuilds jimArray.
        ' -----------------------------------------------------------------------------
        Dim tmpJIarray As Integer = 0
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        Dim tmpSaveYrWk As String = ""
        Dim tmpSaveEmpID As String = ""
        '
        Dim JTtkReadFileKey As String = ""
        Dim JTtkReadFileData As String = ""
        '
        JTtkslReservoir.Clear()
        Dim tmpIPR As New JTtkArrayStructure
        '
        Dim tmpRecordType As String = ""
        Dim JThrWhichField As Integer = 1
        Dim tmpFileSeq As Integer = 10
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTTimeKeeping.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTTimeKeeping.Bkup") Then
            MsgBox("JTTimeKeeping --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (TK.007)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTTimeKeeping.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTTimeKeeping.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTTimeKeeping.Bkup", "JTTimeKeeping.dat")
        End If
        ' ------------------------------------------------------------------

        '
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTtkReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTtkReadFileKey = "tkRecord" Or JTtkReadFileKey = "/tkRecord" Then
                        JTtkReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTtkReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case JTtkReadFileKey
                        '
                        Case "YrWkNum"
                            tmpIPR.JTtkYrWkNum = JTtkReadFileData

                        Case "EmpID"
                            tmpIPR.JTtkEmpID = JTtkReadFileData

                        Case "LabCat"
                            tmpIPR.JTtkLabCat = JTtkReadFileData
                        Case "WEDate"
                            tmpIPR.JTtkWEDate = JTtkReadFileData
                        Case "WorkDate"
                            tmpIPR.JTtkWorkDate = String.Format("{0:MM/dd/yyyy}", CDate(JTtkReadFileData))

                        Case "JobID"
                            tmpIPR.JTtkJobID = JTtkReadFileData
                        Case "SubID"
                            tmpIPR.JTtkSubID = JTtkReadFileData
                        Case "ActDescrpt"
                            tmpIPR.JTtkActDescrpt = tmpIPR.JTtkActDescrpt & JTtkReadFileData

                        Case "Hours"
                            tmpIPR.JTtkHours = JTtkReadFileData
                        Case "Rate"
                            tmpIPR.JTtkRate = JTtkReadFileData
                            Dim tmprate As String = JTtkReadFileData
                        Case "RateCat"
                            tmpIPR.JTtkRateCat = JTtkReadFileData
                        Case "OverTime"
                            tmpIPR.JTtkOverTime = JTtkReadFileData
                        Case "DolAmt"
                            tmpIPR.JTtkDolAmt = JTtkReadFileData
                        Case "InvDate"
                            tmpIPR.JTtkInvDate = JTtkReadFileData
                        Case "InvNumber"
                            tmpIPR.JTtkInvNumber = JTtkReadFileData
                        '
                        '
                        Case "/tkRecord"
                            tmpIPR.JTtkActDescrpt = tmpIPR.JTtkActDescrpt.Replace("~!", vbLf)
                            ' --------------------------------------------------------------------
                            ' The following code fixes a problem where invoice numbers were left
                            ' in the workarea and carried forward to records that were not in-
                            ' cluded on that invoice.
                            ' This code should be installed with the next release but can be re-
                            ' moved from all releases after that.
                            ' --------------------------------------------------------------------
                            If tmpIPR.JTtkInvDate = "" Then
                                tmpIPR.JTtkInvNumber = ""
                            End If
                            ' --------------------------------------------------------------------
                            ' End of repair code.
                            ' --------------------------------------------------------------------
                            If tmpSaveYrWk = "" Then
                                tmpSaveYrWk = tmpIPR.JTtkYrWkNum
                                tmpSaveEmpID = tmpIPR.JTtkEmpID
                            End If
                            If tmpSaveYrWk <> tmpIPR.JTtkYrWkNum Or tmpSaveEmpID <> tmpIPR.JTtkEmpID Then
                                tmpFileSeq = 10
                                tmpSaveYrWk = tmpIPR.JTtkYrWkNum
                                tmpSaveEmpID = tmpIPR.JTtkEmpID
                            End If
                            Dim tmplvKey As String = tmpIPR.JTtkYrWkNum & tmpIPR.JTtkEmpID & Str(tmpFileSeq).Trim(" ")
                            Do While JTtkslReservoir.ContainsKey(tmplvKey) = True
                                tmpFileSeq += 1
                                tmplvKey = tmpIPR.JTtkYrWkNum & tmpIPR.JTtkEmpID & Str(tmpFileSeq).Trim(" ")
                            Loop
                            JTtkslReservoir.Add(tmplvKey, tmpIPR)
                            '
                            ' send date to mainmenu to update EmpTK SL
                            '
                            JTMainMenu.JTMMTKArrayDateUpdate(tmpIPR.JTtkEmpID, tmpIPR.JTtkWEDate)
                            '
                            ' send date to update not inv'd line items
                            '
                            If tmpIPR.JTtkInvDate = "" Then
                                JTMainMenu.JTMMNotInvAmts(tmpIPR.JTtkJobID, tmpIPR.JTtkSubID, Val(tmpIPR.JTtkDolAmt), 0, 0)
                            End If
                            '
                            ' clear fields that might not have a value in the next record.
                            '
                            tmpIPR.JTtkInvNumber = ""
                            tmpIPR.JTtkInvDate = ""
                            tmpIPR.JTtkOverTime = ""
                            tmpIPR.JTtkActDescrpt = ""
                            tmpIPR.JTtkSubID = ""
                            '
                            tmpFileSeq += 1
                    End Select
                    '
                    '
                End If
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        '
    End Sub
    ''' <summary>
    ''' This function returns the hours worked for an employee for a specific week number.
    ''' The hours returned are segregated by the location of the job (by state).
    ''' </summary>
    ''' <param name="tmpempID">Employee ID being requested.</param>
    ''' <param name="tmpweekno">Week number of data being requested.</param>
    ''' <param name="st1ID">State abbreviation where work was performed.</param>
    ''' <param name="st1hrs">Hours worked in state.</param>
    ''' <param name="st2ID">Second state ID.</param>
    ''' <param name="st2hrs">Hours for second state.</param>
    ''' <param name="st3ID">Third state ID.</param>
    ''' <param name="st3hrs">Hours for third state.</param>
    ''' <returns>Totals hours worked by this employee for the week number requested.</returns>
    Public Function JTtkPRHours(tmpempID As String, tmpweekno As String, ByRef st1ID As String,
                                ByRef st1hrs As String, ByRef st2ID As String, ByRef st2hrs As String,
                                ByRef st3ID As String, ByRef st3hrs As String) As String
        '
        Dim tkArray As New JTtkArrayStructure
        '
        Dim tmpHourAccum As Double = 0
        Dim tmpST1hrs As Double = 0
        Dim tmpST2hrs As Double = 0
        Dim tmpST3hrs As Double = 0
        '
        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String
        For Each k In key

            If JTtkslReservoir(k).JTtkEmpID = tmpempID And JTtkslReservoir(k).JTtkYrWkNum = tmpweekno Then
                tmpHourAccum = tmpHourAccum + Val(JTtkslReservoir(k).JTtkHours)

                Select Case JTjim.WorkInState(JTtkslReservoir(k).JTtkjobID, JTtkslReservoir(k).JTtksubid)
                    Case = "CT"
                        tmpST1hrs = tmpST1hrs + Val(JTtkslReservoir(k).JTtkHours)
                    Case = "RI"
                        tmpST2hrs = tmpST2hrs + Val(JTtkslReservoir(k).JTtkHours)
                    Case Else
                        tmpST3hrs = tmpST3hrs + Val(JTtkslReservoir(k).JTtkHours)
                End Select

            End If
        Next
        st1ID = "CT"
        st1hrs = Str(tmpST1hrs)
        st2ID = "RI"
        st2hrs = Str(tmpST2hrs)
        st3ID = "Other"
        st3hrs = Str(tmpST3hrs)


        Return Str(tmpHourAccum)
    End Function
    ''' <summary>
    ''' This function is used to select timekeeping entries in preparation for invoicing.
    ''' It is called from JTasinit. The data arguments are used to define the data being extracted
    ''' as well as to send the results back to the calling module.
    ''' </summary>
    ''' <param name="tmpJobID">This data is supplied by the calling module. It contains the Job Name.</param>
    ''' <param name="tmpSubID">This data is also supplied by the calling module. It will either be
    ''' blank (for invoicing a normal or master job), the sub job name or "MSCOMB" (for creation
    ''' of combined master/subjob invoice).</param>
    ''' <param name="tmpSLkey">This data is returned. It is the SL key for the item being extracted.</param>
    ''' <param name="tmpSLtkItem">This data is returned. It is a mirror image of the SL item.</param>
    ''' <returns>"DATA" is returned if a record's data is being returned.
    ''' "DONE" is returned if there are no more records to be returned.</returns>
    Public Function JTTKasExt(tmpJobID As String, tmpSubID As String, ByRef tmpSLkey As String,
                              ByRef tmpSLtkItem As JTtkArrayStructure) As String
        Dim tmpSlKeySave As String = tmpSLkey
        Dim tkkey As ICollection = JTtkslReservoir.Keys
        If tmpSlKeySave = "FIRST" Then
            JTtkKeyCollPos = 0
            tmpSlKeySave = ""
        End If
        Do While JTtkKeyCollPos < tkkey.Count
            Dim tmparrayJob As String = JTtkslReservoir(tkkey(JTtkKeyCollPos)).JTtkJobID
            Dim tmparraySub As String = JTtkslReservoir(tkkey(JTtkKeyCollPos)).JTtkSubID
            Dim tmparrayInvDate As String = JTtkslReservoir(tkkey(JTtkKeyCollPos)).JTtkInvDate
            If tmpJobID = tmparrayJob And (tmpSubID = tmparraySub Or tmpSubID = "MSCOMB") And tmparrayInvDate = "" Then
                tmpSLkey = tkkey(JTtkKeyCollPos)
                tmpSLtkItem = JTtkslReservoir.Item(tkkey(JTtkKeyCollPos))
                JTtkKeyCollPos += 1
                Return "DATA"
                '
            End If
            JTtkKeyCollPos += 1

        Loop
        Return "DONE"
    End Function
    ''' <summary>
    ''' This function checks to see if there is any activity still in the JTTimeKeeping.dat Reservoir.
    ''' It is used to determine if an ID for a terminated employee can be purged (all activity billed and archived)
    ''' </summary>
    ''' <param name="EmpID">Employee ID</param>
    ''' <returns>Returns "YES" (activity still exists) or "NO" (all activity for this emp ID has 
    ''' been archived)</returns>
    Public Function JTtkChkForActivity(EmpID As String) As String
        Dim keys As ICollection = JTtkslReservoir.Keys
        Dim k As String
        For Each k In keys
            If JTtkslReservoir(k).JTtkEmpID = EmpID Then
                Return "YES"
            End If
        Next
        Return "NO"
    End Function
    Private Sub btnJTtkModWkB_Click(sender As Object, e As EventArgs) Handles btnJTtkModWkB.Click
        If JTtkChangesThisSession = True Then
            MsgBox("Changes have been made in this session that have not been updated. Date cannot be modified.")
        End If
        Dim tmpLastSatDate As Date = CDate(mtxtJTtkWEDate.Text).AddDays(-7)
        JTtkWEDate = tmpLastSatDate
        mtxtJTtkWEDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)
        JTtkRestorePastEntries()
        mtxtJTtkWEDate.Select()
    End Sub

    Private Sub btnJTtkModWkF_Click(sender As Object, e As EventArgs) Handles btnJTtkModWkF.Click
        If JTtkChangesThisSession = True Then
            MsgBox("Changes have been made in this session that have not been updated. Date cannot be modified.")
        End If
        Dim tmpLastSatDate As Date = CDate(mtxtJTtkWEDate.Text).AddDays(+7)
        If tmpLastSatDate > Date.Today Then
            Exit Sub
        End If
        JTtkWEDate = tmpLastSatDate
        mtxtJTtkWEDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)
        JTtkRestorePastEntries()
        mtxtJTtkWEDate.Select()
    End Sub
    Public Function JTtkUnbilledLabor(ByRef w5date As String, ByRef w5hours As Double,
                                      ByRef w4date As String, ByRef w4hours As Double,
                                      ByRef w3date As String, ByRef w3hours As Double,
                                      ByRef w2date As String, ByRef w2hours As Double,
                                      ByRef w1date As String, ByRef w1hours As Double) As Double
        Dim sltkHS As New SortedList
        Dim HSkey As ICollection = sltkHS.Keys
        Dim HSk As String
        Dim tmpJulDate As String
        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String = ""
        Dim tmpTlLabor As Double = 0
        For Each k In key
            HSkey = sltkHS.Keys
            '
            Dim NewHSItem As tkHourSummary
            NewHSItem.tkHSHours = 0

            tmpJulDate = Gregarian2Julian(JTtkslReservoir(k).JTtkWEDate)

            For Each HSk In HSkey

                If tmpJulDate = sltkHS(HSk).tkHSJulDate Then
                    NewHSItem = sltkHS.Item(HSk)
                    sltkHS.Remove(HSk)
                    Exit For
                End If
            Next
            NewHSItem.tkHSJulDate = tmpJulDate
            NewHSItem.tkHSGregDate = JTtkslReservoir(k).JTtkWEDate
            NewHSItem.tkHSHours += Val(JTtkslReservoir(k).JTtkHours)
            sltkHS.Add(tmpJulDate, NewHSItem)
            HSkey = sltkHS.Keys
            '
            If JTtkslReservoir(k).JTtkInvdate = "" Then
                tmpTlLabor += Val(JTtkslReservoir(k).JTtkDolAmt)
            End If
        Next
        HSkey = sltkHS.Keys
        '
        Dim tmpCount As Integer = HSkey.Count
        Dim tmpCountEnd As Integer = HSkey.Count - 5

        If HSkey.Count < 5 Then
            tmpCountEnd = 1
        End If
        Dim tmpcount2 As Integer = 1
        For tmpCount = HSkey.Count - 1 To tmpCountEnd - 1 Step -1
            If tmpcount2 = 1 Then
                w1date = sltkHS(HSkey(tmpCount)).tkHSGregDate
                w1hours = sltkHS(HSkey(tmpCount)).tkHSHours
            End If
            If tmpcount2 = 2 Then
                w2date = sltkHS(HSkey(tmpCount)).tkHSGregDate
                w2hours = sltkHS(HSkey(tmpCount)).tkHSHours
            End If
            If tmpcount2 = 3 Then
                w3date = sltkHS(HSkey(tmpCount)).tkHSGregDate
                w3hours = sltkHS(HSkey(tmpCount)).tkHSHours
            End If
            If tmpcount2 = 4 Then
                w4date = sltkHS(HSkey(tmpCount)).tkHSGregDate
                w4hours = sltkHS(HSkey(tmpCount)).tkHSHours
            End If
            If tmpcount2 = 5 Then
                w5date = sltkHS(HSkey(tmpCount)).tkHSGregDate
                w5hours = sltkHS(HSkey(tmpCount)).tkHSHours
            End If
            tmpcount2 += 1
        Next

        Return tmpTlLabor
    End Function
    Public Function JTtkJobUnbilled(tmpJob As String, tmpSubJob As String, ByRef tmpLstDate As String) As Double

        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String = ""
        Dim tmpTlunbilled As Double = 0
        For Each k In key
            If JTtkslReservoir(k).JTtkJobID = tmpJob And JTtkslReservoir(k).JTtkSubID = tmpSubJob And
                JTtkslReservoir(k).JTtkInvDate = "" Then
                tmpTlunbilled += Val(JTtkslReservoir(k).JTtkDolAmt)
                If tmpLstDate = "" Then
                    tmpLstDate = JTtkslReservoir(k).JTtkWorkDate
                ElseIf CDate(tmplstdate) < CDate(JTtkslReservoir(k).JTtkWorkDate) Then
                    tmpLstDate = JTtkslReservoir(k).JTtkWorkDate
                End If
            End If
        Next
        '
        Return tmpTlunbilled
    End Function
    ''' <summary>
    ''' This function checks the date entered into the txtJTtkDate field on the JTtk
    ''' panel to be sure it is a date in the week currently being processed.
    ''' </summary>
    ''' <returns>"TRUE" - if the date is a day in the week being processed.
    ''' "FALSE" - If an invalid date has been entered.</returns>
    Public Function ChkTKDate() As Boolean
        JTtkWEDate = CDate(mtxtJTtkWEDate.Text)
        If txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-6)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-5)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-4)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-3)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-2)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate.AddDays(-1)) Or
            txtJTtkDate.Text = String.Format("{0:MM/dd/yyyy}", JTtkWEDate) Then
            Return True
        Else
            Return False
        End If

        '
    End Function
    ''' <summary>
    ''' This function returns unbilled TK items. It is called to accumulate data for the 
    ''' DashBoard. One record is returned at a time. The calling Sub processes the data.
    ''' </summary>
    ''' <param name="tmpsub">Record number to be queried next in the SL. On the first call
    ''' this field will be "1". It is controled by the function after that.</param>
    ''' <param name="tmpArrayITem">This is a mirror image of a TK SL item.</param>
    ''' <returns>"DATA" - If a record's data is being returned.
    ''' "DONE" - If there are no more records to return.</returns>
    Public Function JTtkReturnUnbilledTK(ByRef tmpsub As Integer, ByRef tmpArrayITem As JTtkArrayStructure) As String
        Dim key As ICollection = JTtkslReservoir.Keys
        Do While tmpsub < JTtkslReservoir.Count
            If JTtkslReservoir(key(tmpsub)).JTtkinvdate = "" Then
                tmpArrayITem = JTtkslReservoir(key(tmpsub))
                tmpsub += 1
                Return "DATA"
            End If
            tmpsub += 1
        Loop
        Return "DONE"
    End Function
    Public Function JTtkJobHasActivity(tmpJobName As String, tmpSubname As String) As Boolean
        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String = ""
        For Each k In key
            If JTtkslReservoir(k).JTtkJobID = tmpJobName Then
                If JTtkslReservoir(k).JTtkSubID = tmpSubname Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function
    ''' <summary>
    ''' Function to make customer and/or job names changes. Used by JTjm.
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTtkCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String


        ' Public Structure JTjmSettingsStru
        '      Dim ExCust As String
        '      Dim ExJob As String
        '      Dim NewCust As String
        '      Dim NewJob As String
        '      Dim CustNameChg As Boolean
        '      Dim JobNameChg As Boolean
        ' End Structure
        ' --------------------------------------------------
        ' Start of JTtkslReservoir section
        ' --------------------------------------------------
        Dim tmpKeyCollection As New Collection()
        '
        Dim key As ICollection = JTtkslReservoir.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTtkslReservoir(k).JTtkJobID Then
                    tmpKeyCollection.Add(k)
                End If
            Next
            '         MsgBox("JTtkslReservoir Records found - >" & tmpKeyCollection.Count & "< - " & JTjmSettings.ExCust & "- JTtk~1342")
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTtkslReservoir(k).JTtkJobID Then
                    If JTjmSettings.ExJob = JTtkslReservoir(k).JTtkSubID Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
            '          MsgBox("JTtkslReservoir Records found - >" & tmpKeyCollection.Count & "< - " & JTjmSettings.ExCust & "~" & JTjmSettings.ExJob & "- JTtk~1352")
        End If
        ' Modify logic for JTtkslReservoir placed here .....
        Dim tmpFileSeq As Integer = 10
        For Each k In tmpKeyCollection
            Dim tmpItem As JTtkArrayStructure = JTtkslReservoir(k)
            JTtkslReservoir.Remove(k)
            '
            tmpItem.JTtkJobID = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.JTtkSubID = JTjmSettings.NewJob
            End If
            Dim tmpKey As String = tmpItem.JTtkYrWkNum & tmpItem.JTtkEmpID & Str(tmpFileSeq).Trim(" ")
            Do While JTtkslReservoir.ContainsKey(tmpKey) = True
                tmpFileSeq += 1
                tmpKey = tmpItem.JTtkYrWkNum & tmpItem.JTtkEmpID & Str(tmpFileSeq).Trim(" ")
            Loop
            JTtkslReservoir.Add(tmpKey, tmpItem)
        Next
        ' --------------------------------------------------
        ' End of JTtkslReservoir section
        ' --------------------------------------------------
        Return ""
    End Function


End Class