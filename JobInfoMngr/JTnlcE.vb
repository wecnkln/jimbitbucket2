﻿Imports System.DirectoryServices.ActiveDirectory
Imports System.IO
Imports iTextSharp.text.pdf

Public Class JTnlcE
    ' -------------------------------------------------------------------
    ' JTnlcEMode values --- This value is sett by the calling module.
    ' "MANUAL" - - - Invoices being entered manually - - no supporting
    '                electronic data.
    ' "EDI" - - - Invoice to processed with supporting vendor invoice PDF.
    ' "REVMODNLC" - - - Invoice to be reviewed or modified. Call coming
    '                   from JTnlc panel.
    ' "REVMODREC" - - - Invoice to be reviewed or modified. Call coming
    '                   from JTrec panel.
    ' "REVMODAS" - - - Invoice to be reviewed or modified. Call coming
    '                  from JTas panel.
    ' --------------------------------------------------------------------
    Dim JTnlcEMode As String = Nothing
    Dim JTnlcEInvFileName As String = Nothing
    Dim tmpRestrictionsApplyInv As Boolean = False
    Dim tmpRestrictionsApplyRecExt As Boolean = False
    Dim tmpItem As JTnlc.JTnlcStructure
    '
    ' Keep track of the files that have been opened in webbrowser.
    Public OpenedFiles As New List(Of WebBrowser)
    '
    ''' <summary>
    ''' This sub is the entry into all JTnlsE activity. It can be called by multiple spots
    ''' and in different modes. The mode of operation is determined by the tmpMode argument.
    ''' </summary>
    ''' <param name="tmpMode">Determines the mode of operation for JTnlcE. It also
    ''' determines where the request came from.
    ''' Values: "EDI" - Came from JTnlcM. New entry with electronic image of invoice.
    '''         "REVMODNLC" - Request for rev/mod from JTnlc.
    '''         nothing - Manual entry new invoice, IS/IH, or adjustment.
    '''         "REVMODREC" - Request for rev/mod from JTrec(Statement Reconciliation).
    '''         "REVMODAS" -  Request for rev/mod from JTas(invoice production).</param>
    ''' <param name="tmpInvFileName">PDF file name or key to locate item to be reviewed.</param>
    Public Sub JtnlcEInit(tmpMode As String, tmpInvFileName As String)
        JTnlcEMode = tmpMode
        JTnlcEInvFileName = tmpInvFileName
        JTnlcEEnableAllEntryFields()   'Enable all input fields and switches.
        mtxtJTnlcEEntDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        ' Filling combo box value for Job ID.
        JTnlcEFillcboxJobID()
        '
        '
        ' Filling combo box value for NJR accounts.
        JTnlcEFillcboxNJRE()
        '
        If tmpMode = "EDI" Then
            gboxJTnlcENoImage.Visible = False
            gboxInv1PDF.Visible = True
            JTnlcEFillInvImage(tmpInvFileName)
            JTnlcEFillPanelFields(tmpInvFileName)
        Else
            '      gboxJTnlcENoImage.Location(248, 275)
            gboxJTnlcENoImage.Visible = True
            gboxInv1PDF.Visible = False
        End If
        ' ---------------------------------------------------------------------
        ' Set text on button depending upon the type of transaction.
        ' ---------------------------------------------------------------------
        btnJTnlcERecord.Visible = True
        If tmpMode = "EDI" Or tmpMode = "MANUAL" Or tmpMode = "ISRETURN" Then
            If tmpMode = "ISRETURN" Then    ' if returning from an IS/IH session, turn session back to "MANUAL"
                tmpMode = "MANUAL"
                JTnlcEMode = "MANUAL"
            End If
            btnJTnlcERecord.Text = "C A N C E L"
            btnJtnlcEExit.Text = "E X I T"
            btnJTnlcEDeleteEntry.Visible = False
        Else
            btnJTnlcERecord.Text = "R E C O R D"
            btnJtnlcEExit.Text = "C A N C E L"
            btnJTnlcEDeleteEntry.Visible = True
        End If
        '
        If tmpMode = "REVMODNLC" Or tmpMode = "REVMODREC" Or tmpMode = "REVMODAS" Then
            If JTnlc.JTnlcGetslNLCRec(tmpInvFileName, "DELETE", tmpItem) = "DATA" Then
                mtxtJTnlcEEntDate.Text = tmpItem.EntryDate
                mtxtJTnlcEDate.Text = tmpItem.InvDate
                cboxJTnlcEJobID.Text = tmpItem.JobID
                cboxJTnlcESubID.Text = tmpItem.SubID
                Select Case tmpItem.MorS
                    Case "M", "IS", "IH"
                        radiobtnJTnlcEMat.Checked = True
                    Case "S"
                        radiobtnJTnlcESer.Checked = True
                    Case "ADJ"
                        radiobtnJTnlcEAdj.Checked = True
                    Case "NJR"
                        radiobtnJTnlcENJRE.Checked = True
                End Select
                txtJTnlcEDecrpt.Text = tmpItem.Descrpt
                cboxJTnlcESupplier.Text = tmpItem.Supplier
                txtJTnlcEInvNum.Text = tmpItem.SupInvNum
                txtJTnlcEXRef.Text = tmpItem.SupCrossRefNum
                txtJTnlcEInvFileName.Text = tmpItem.SupInvImageFile
                txtJTnlcESupCost.Text = tmpItem.SupCost
                txtJTnlcEMrgnPC.Text = tmpItem.MrgnPC
                txtJTnlcEAmtTBInvd.Text = tmpItem.TBInvd

                Select Case tmpItem.PymtSwitch
                    Case "R"
                        rbtnJTnlcEviReconcile.Checked = True
                    Case "M"
                        rbtnJTnlcEviManual.Checked = True
                    Case "E"
                        rbtnJTnlcEviExport.Checked = True
                    Case "JO"
                        rbtnJTnlcEviJourOnly.Checked = True
                End Select
                '
                cboxJTnlcENJREAcct.Text = tmpItem.NJREAcct
                txtJTviAcctNum.Text = tmpItem.viAcctNum
                txtJTviVendName.Text = tmpItem.viVendName
                txtJTviStreet.Text = tmpItem.viStreet
                txtJTviCityStateZip.Text = tmpItem.viCityState
                txtJTnlcviTerms.Text = tmpItem.viTerms
                mtxtJTnlcEviDueDate.Text = tmpItem.viDueDate
                txtJTviDiscPercent.Text = tmpItem.viDiscpercent
                txtJTnlcviDiscAmt.Text = tmpItem.viDiscAmt
                '
                JTnlcEFrmtPanelDisplay()    'Call function to set correct display options for transaction type selected.
                If txtJTnlcEInvFileName.Text <> "" Then
                    gboxJTnlcENoImage.Visible = False
                    gboxInv1PDF.Visible = True
                    JTnlcEShowExistingPDF(txtJTnlcEInvFileName.Text)
                End If
            End If
        End If
        '
        JTstart.Label1.Visible = False
        JTstart.PictureBox1.Visible = False
        '
        mtxtJTnlcEDate.Select()
        '
        If JTnlcEMode <> "EDI" Then
            If JTnlcEMode <> "MANUAL" Then
                JTnlcECustomizeEntryFields(JTnlcEMode)
            End If
        End If
        '
        Me.MdiParent = JTstart
        Me.Show()
        '
    End Sub
    ''' <summary>
    ''' Object used to control SyncLock in JTnlcEValidTranCheck function.
    ''' </summary>
    Dim tmpSLObject As Object = New Object
    ''' <summary>
    ''' Function to check the values on the JTnlcE panel to see if the entries
    ''' make a valid transaction.
    ''' </summary>
    ''' <param name="tmpMode">"DISPMSG" - Display message with all error messages. Any other value - Just perform checks.</param>
    ''' <returns>true = Valid transaction
    ''' false = errors or omissions exist</returns>
    Private Function JTnlcEValidTranCheck(tmpMode As String) As Boolean
        SyncLock tmpSLObject
            '
            Dim tmpGoodTransaction As Boolean = True
            Dim tmpMsgBoxText As String = "NLC Entry - Transaction Error List" & vbCrLf & vbCrLf
            If txtJTnlcEDecrpt.Text = "" Then
                tmpGoodTransaction = False
                tmpMsgBoxText = tmpMsgBoxText & "- No transaction description entered" & vbCrLf
            End If
            '
            If txtJTnlcESupCost.Text = "" Then
                tmpGoodTransaction = False
                tmpMsgBoxText = tmpMsgBoxText & "- No Vendor cost or Adjustment Amount Entered" & vbCrLf
            End If
            '
            If radiobtnJTnlcENJRE.Checked = True Then
                Dim tmpNJREAcctDesc As String = Nothing
                If JTVarMaint.JTvmValidateUpdateNJREA("Validate", cboxJTnlcENJREAcct.Text, tmpNJREAcctDesc) = "NOF" Then
                    tmpGoodTransaction = False
                    tmpMsgBoxText = tmpMsgBoxText & "- NJR Account not valid" & vbCrLf
                End If
            Else
                Dim tmpSub As String = Nothing
                Dim tmpJobType As String = Nothing
                Dim tmpDeactDate As String = Nothing
                If JTjim.JTjimFindFunc(tmpSub, cboxJTnlcEJobID.Text, cboxJTnlcESubID.Text, tmpJobType, tmpDeactDate) <> "DATA" Then
                    tmpGoodTransaction = False
                    tmpMsgBoxText = tmpMsgBoxText & "- Customer/Job entered is not valid" & vbCrLf
                End If
            End If
            If TestForValidDate(mtxtJTnlcEDate.Text) = False Or TestForValidDate(mtxtJTnlcEEntDate.Text) = False Then
                tmpGoodTransaction = False
                tmpMsgBoxText = tmpMsgBoxText & "- The Entry and/or Invoice date is/are not valid." & vbCrLf
            End If
            Dim tmpMorS As String = Nothing
            Dim tmpLic As String = Nothing
            Dim tmpLicExpired As String = Nothing
            Dim tmpIns As String = Nothing
            Dim tmpInsExpired As String = Nothing
            If radiobtnJTnlcEAdj.Checked = False Then
                If JTvi.JTviLkupShortName(cboxJTnlcESupplier.Text, tmpMorS, tmpLic, tmpLicExpired, tmpIns, tmpInsExpired) <> "VALID" Then
                    tmpGoodTransaction = False
                    tmpMsgBoxText = tmpMsgBoxText & "- The Supplier entered is not valid." & vbCrLf
                End If

                If txtJTnlcEInvNum.Text = "" Then
                    tmpGoodTransaction = False
                    tmpMsgBoxText = tmpMsgBoxText & "- No invoice number entered." & vbCrLf
                Else
                    If JTnlc.JTnlcDupInvNumChk(cboxJTnlcESupplier.Text, txtJTnlcEInvNum.Text) = "DUP" Then
                        tmpGoodTransaction = False
                        tmpMsgBoxText = tmpMsgBoxText & "- Invoice number entry is a duplicate entry." & vbCrLf
                    End If
                End If

                If rbtnJTnlcEviExport.Checked = True Then
                    If mtxtJTnlcEviDueDate.Text = "" Then
                        tmpGoodTransaction = False
                        tmpMsgBoxText = tmpMsgBoxText & "- No due date for transaction being exported." & vbCrLf
                    End If
                End If
            End If
            If tmpGoodTransaction = False Then
                If tmpMode = "DISPMSG" Then
                    MsgBox(tmpMsgBoxText)
                End If
            End If

            Return tmpGoodTransaction
        End SyncLock
    End Function

    Private Sub btnJTnlcERecord_Click(sender As Object, e As EventArgs) Handles btnJTnlcERecord.Click
        If btnJTnlcERecord.Text = "C A N C E L" Then
            If JTnlcEMode = "EDI" Or JTnlcEMode = "MANUAL" Then
                JTnlcEClearPanel()
                Select Case JTnlcEMode
                    Case "EDI"
                        WebBrowser1.Navigate("about:blank")
                        WebBrowser1.Dispose()
                        ReleaseOpenPDFs()
                        Me.Close()
                        JTstart.EDINavigationSub()
                        Exit Sub
                    Case "MANUAL"
                        JTnlcEFrmtPanelDisplay()      'add call here to reset autofill table for vendor
                        Exit Sub
                End Select
            End If
        End If
        If JTnlcEValidTranCheck("DISPMSG") = False Then
            Exit Sub
        End If
        If JTnlcEMode = "EDI" Or JTnlcEMode = "MANUAL" Then

            If JTnlcEMode = "EDI" Then
                JTnlcERenameMovePDF()
            End If


            JTnlcEPaneltoItem()

            JTnlcESavetoNLC()
            ' Do not look for supplier name, address, etc. if this is an adjustment.
            If tmpItem.MorS <> "ADJ" Then
                JTvi.JTviUpdtActDate(tmpItem.Supplier, tmpItem.InvDate, tmpItem.PymtSwitch)
            End If
            tmpItem = Nothing
                JTnlcEClearPanel()
                Select Case JTnlcEMode
                    Case "EDI"
                        Me.Close()
                        JTstart.EDINavigationSub()
                    Case "MANUAL"
                        JTnlcEFrmtPanelDisplay()          'add call here to reset autofill table for vendor
                End Select
            Else
            JTnlcEPaneltoItem()
            ' ---------------------------------------------------------------
            ' check to see if this item has a vendor invoice pdf. If no, skip
            ' the renaming logic.
            ' ---------------------------------------------------------------
            If tmpItem.SupInvImageFile <> "" Then
                ' --------------------------------------------------------------------------------
                ' Code added here to rename the PDF file with the expanded name --- name that
                ' includes Customer and job name. It will also add the invoice amount as metadata.
                ' --------------------------------------------------------------------------------
                '           MsgBox("Orig file name - >" & tmpItem.SupInvImageFile & "< ~274/JTnlcE")
                Dim tmpNewFileName As String = JTnlc.JTnlcAddInvInfotoPDFName(tmpItem.SupInvImageFile,
                                                 tmpItem.SupInvImageFile,
                                                 cboxJTnlcEJobID.Text,
                                                 cboxJTnlcESubID.Text,
                                                 "",
                                                 "",
                                                 txtJTnlcESupCost.Text)

                '             MsgBox("New file name - >" & tmpNewFileName & "< ~282/JTnlcE")
            End If
            Dim tmpKey As String = JTnlc.JTnlcBldSLKey(tmpItem.InvDate, tmpItem.JobID, tmpItem.SubID, tmpItem.CustInvDate, tmpItem.TBInvd)
                JTnlc.JTnlcAddSLItem(tmpKey, tmpItem, "Recording reviewed item - JTnlcE~163")
                tmpItem = Nothing
                JTnlcEFrmtPanelDisplay()
                JTstart.Label1.Visible = True
                JTstart.PictureBox1.Visible = True
                ReleaseOpenPDFs()
                Select Case JTnlcEMode
                    Case "REVMODNLC"
                        Me.Close()
                        JTnlc.JTnlcInit()
                        Exit Sub
                    Case "REVMODREC"
                        If JTrec.JTrecSuspendedFlag = True Then
                            JTrec.JTrecSuspendedFlag = False
                            JTrec.JTrecInit()
                        End If
                        Me.Close()
                        Exit Sub
                    Case "REVMODAS"
                        JTas.JTasInit(JTas.JTasJobID, JTas.JTasSubID, JTas.JTasJobDesc, JTas.JTasJobPricing, JTas.JTasLayer)
                        Me.Close()
                End Select
            End If
    End Sub
    Private Sub btnJtnlcEExit_Click(sender As Object, e As EventArgs) Handles btnJtnlcEExit.Click
        JTstart.Label1.Visible = True
        JTstart.PictureBox1.Visible = True
        ' ---------------------------------------------------------------------
        ' Make sure all just entered entries have been saved to disk.
        ' ---------------------------------------------------------------------
        JTnlc.JTnlcArrayFlush("")
        '
        If JTnlcEMode = "EDI" Or JTnlcEMode = "MANUAL" Then
            Select Case JTnlcEMode
                Case "EDI"
                    ReleaseOpenPDFs()
                    JTstart.EDINavigationSub()
                Case "MANUAL"
                    JTnlcM.JTnlcMinit()
            End Select
            Me.Close()
            Me.Dispose()
            Exit Sub
        Else
            JTnlcEFrmtPanelDisplay()
            Dim tmpKey As String = JTnlc.JTnlcBldSLKey(tmpItem.InvDate, tmpItem.JobID, tmpItem.SubID, tmpItem.CustInvDate, tmpItem.TBInvd)
            JTnlc.JTnlcAddSLItem(tmpKey, tmpItem, "Re-Recording restricted item - JTnlcE~186")
            tmpItem = Nothing
            Select Case JTnlcEMode
                Case "REVMODNLC"
                    Me.Close()
                    JTnlc.JTnlcInit()
                    Exit Sub
                Case "REVMODREC"
                    If JTrec.JTrecSuspendedFlag = True Then
                        JTrec.JTrecSuspendedFlag = False
                        JTrec.JTrecInit()
                    End If
                    Me.Close()
                    Exit Sub
                Case "REVMODAS"
                    JTas.JTasInit(JTas.JTasJobID, JTas.JTasSubID, JTas.JTasJobDesc, JTas.JTasJobPricing, JTas.JTasLayer)
                    Me.Close()
                    Exit Sub
            End Select
        End If
        JTstart.Label1.Visible = True
        JTstart.PictureBox1.Visible = True
        Me.Close()
    End Sub
    ''' <summary>
    ''' Sub to handle delete button. It is only used on NLC, REC and AS requests that have not been
    ''' invoiced, reconciled or extracted to accounting. If there is an invoice image, it will be moved 
    ''' back to the EDIInputFiles folder.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTnlcEDeleteEntry_Click(sender As Object, e As EventArgs) Handles btnJTnlcEDeleteEntry.Click
        If tmpItem.CustInvDate <> "" Or tmpItem.ReconcileDate <> "" Or tmpItem.AcctExtDate <> "" Then
            MsgBox("Item cannot be deleted. It has been invoiced or reconciled" & vbCrLf &
                   "or extracted to accounting.")
            Exit Sub
        End If
        '
        ' ------------------------------------------------------------------------------
        ' Code inserted here to clear webbrowser pdf display. This has been done to make
        ' sure file has been released prior to any delete attempt.
        ' ------------------------------------------------------------------------------
        WebBrowser1.DocumentText = ""
        WebBrowser1.Navigate("about:blank")
        WebBrowser1.Navigate("about:blank")
        WebBrowser1.Dispose()
        '
        If tmpItem.SupInvImageFile <> "" Then
            JTnlcEMovePDFBack(tmpItem.SupInvImageFile)
        End If
        tmpItem = Nothing
        Select Case JTnlcEMode
            Case "REVMODNLC"
                Me.Close()
                JTnlc.JTnlcInit()
                Exit Sub
            Case "REVMODREC"
                If JTrec.JTrecSuspendedFlag = True Then
                    JTrec.JTrecSuspendedFlag = False
                    JTrec.JTrecInit()
                End If
                Me.Close()
                Exit Sub
            Case "REVMODAS"
                JTas.JTasInit(JTas.JTasJobID, JTas.JTasSubID, JTas.JTasJobDesc, JTas.JTasJobPricing, JTas.JTasLayer)
                Me.Close()
                Exit Sub
        End Select
    End Sub
    Private Sub cboxJTnlcEJobID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxJTnlcEJobID.SelectedIndexChanged
        If JTnlcEFillcboxSubID(cboxJTnlcEJobID.Text) = 1 Then
            JTnlcEPostJobNavigation()
        End If
    End Sub
    ''' <summary>
    ''' Sub to handle radio buttons to set panel for specific transactions.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub radiobtnJTnlcEMat_CheckedChanged(sender As Object, e As EventArgs) Handles radiobtnJTnlcEMat.CheckedChanged,
                                                  radiobtnJTnlcESer.CheckedChanged, radiobtnJTnlcENJRE.CheckedChanged,
                                                  radiobtnJTnlcEAdj.CheckedChanged, radiobtnJTnlcEISItems.CheckedChanged
        JTnlcEFrmtPanelDisplay()    'Call function to set correct display options for transaction type selected.
    End Sub



    ''' <summary>
    ''' Sub to populate vendor information after vendor short name has been selected.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTnlcESupplier_LostFocus(sender As Object, e As EventArgs) Handles cboxJTnlcESupplier.LostFocus
        If cboxJTnlcESupplier.Text <> "" Then
            JTnlcEFillviFields(cboxJTnlcESupplier.Text)
        End If
        If JTnlcEValidTranCheck("") = True Then
            btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
        End If
    End Sub
    ''' <summary>
    ''' Function to fill values for combo box for Customer (JobID).
    ''' </summary>
    ''' <returns>Number of customers added.</returns>
    Private Function JTnlcEFillcboxJobID() As Integer
        Dim tmpCount As Integer = 0
        cboxJTnlcEJobID.Items.Clear()
        Dim tmpJobSub As Integer = 0
        Dim tmpJobName As String = Nothing
        Dim tmpSubName As String = Nothing
        Dim tmpDeactDate As String = Nothing
        Dim tmpJobType As String = Nothing
        Dim tmpCstMeth As String = Nothing
        '
        Dim tmpLstName As String = Nothing
        '
        ' Filling combo box values for customer.
        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpDeactDate = "" Then
                If tmpJobName <> tmpLstName Then
                    cboxJTnlcEJobID.Items.Add(tmpJobName)
                    tmpLstName = tmpJobName
                    tmpCount += 1
                End If
            End If
            tmpJobSub += 1
        Loop

        Return tmpCount
    End Function
    ''' <summary>
    ''' Function to fill values for combo box for NJR Accounts.
    ''' </summary>
    ''' <returns>Number of accounts added.</returns>
    Private Function JTnlcEFillcboxNJRE() As Integer
        Dim tmpCount As Integer = 0
        cboxJTnlcENJREAcct.Items.Clear()
        Dim tmpSub As Integer = 0
        Dim tmpAccount As String = Nothing
        Dim tmpDesc As String = Nothing
        Do While JTVarMaint.JTvmReadNJREA(tmpSub, tmpAccount, tmpDesc) = "DATA"
            cboxJTnlcENJREAcct.Items.Add(tmpAccount)
            tmpCount += 1
            tmpSub += 1
        Loop
        Return tmpCount
    End Function
    ''' <summary>
    ''' Function to handle filling drop down list for cboxSubID. Extra logic included to 
    ''' check for a blank Subjob name (left over from early system methods) and also
    ''' checks the number of jobs for the customer.
    ''' </summary>
    ''' <param name="tmpJobID">Customer selected in cboxJobID. This key is used to select
    ''' the proper Subid for the drop down list.</param>
    ''' <returns># of jobs for this customer</returns>
    Private Function JTnlcEFillcboxSubID(tmpJobID As String) As Integer
        cboxJTnlcESubID.Items.Clear()
        Dim tmpCount As Integer = 0
        Dim tmpSub As Integer = 0
        Dim tmpJobName As String = ""
        Dim tmpSubName As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpCstMeth As String = ""
        '
        If JTjim.JTjimCntCustJobs(tmpJobID) = 1 Then
            Do While JTjim.JTjimReadFunc(tmpSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
                If tmpJobName = tmpJobID And
                   tmpDeactDate = "" Then
                    cboxJTnlcESubID.Text = tmpSubName
                    Dim tmpMorS As String = Nothing
                    txtJTnlcEMrgnPC.Text = Nothing
                    If radiobtnJTnlcENJRE.Checked <> True Then

                        If radiobtnJTnlcEMat.Checked = True Or radiobtnJTnlcESer.Checked = True Then
                            txtJTnlcEDecrpt.Select()
                            If radiobtnJTnlcEMat.Checked = True Then
                                tmpMorS = "M"
                            End If
                            If radiobtnJTnlcESer.Checked = True Then
                                tmpMorS = "S"
                            End If
                            txtJTnlcEMrgnPC.Text = JTjim.JTjimMrgnPC(tmpJobName, tmpSubName, tmpMorS)
                        End If
                    End If

                    Return 1    'returning from search for SubID's where customer only has one job
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        Do While JTjim.JTjimReadFunc(tmpSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpJobName = tmpJobID And tmpDeactDate = "" Then
                If tmpSubName = "" Then
                    tmpSubName = "--ValidBlankJobName--"    'This takes care of cust/job prior to mandatory job ("subjob") IDs
                End If
                cboxJTnlcESubID.Items.Add(tmpSubName)
                tmpCount += 1
            End If
            tmpSub += 1
        Loop
        '
        cboxJTnlcESubID.Select()
        '
        Return tmpCount
    End Function
    ''' <summary>
    ''' Function to open vendor supplied PDF and display it on JTnlcE. 
    ''' File to be displayed selected on JTedi panel
    ''' 
    ''' </summary>
    ''' <param name="tmpSelectedFileName"></param>
    ''' <returns></returns>
    Private Function JTnlcEFillInvImage(tmpSelectedFileName As String) As String
        ' -------------------------------------------------------------------------------
        ' code assembles a list of *.pdf files
        ' -------------------------------------------------------------------------------
        Dim JTediPDFFiles As New List(Of String)
        Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePath & "EDIInputFiles\", "*.pdf")
        Dim tmpPrefixLen As Integer = Len(JTVarMaint.JTvmFilePath & "EDIInputFiles\")
        ' Process the list of .pdf files found in the directory. '
        Dim tmpExpandedfileName As String = tmpSelectedFileName   ' portion of line commented out - - - & ".PDF"
        Dim tmpFileName As String = Nothing
        '
        Dim tmpFilePathMergeCandidate As String = Nothing
        '
        Dim tmpTryAgainFlag As Boolean = True
        Dim tmpWorkingFile As String = Nothing
        Do While tmpTryAgainFlag = True
            tmpFileName = JTVarMaint.JTvmFilePath & "EDIInputFiles/" & tmpExpandedfileName
            '
            If My.Computer.FileSystem.FileExists(tmpFileName) = False Then
                MsgBox("Can't find file." & vbCrLf &
                           "File Name - >" & tmpFileName & "<- - - JTnlcE~534")
            End If
            '
            ' ------------------------------------------------------------------
            ' Check to see if file is a candidate to re-merge. '-1' returns ---
            ' not a merge candidate.
            ' ------------------------------------------------------------------
            Dim tmpFileSerNum As Integer = CheckforMergeCandidate(tmpFileName, tmpFilePathMergeCandidate)
            '
            If tmpFileSerNum <> -1 Then
                btnJTnlceMergePDF.Visible = True
            Else
                btnJTnlceMergePDF.Visible = False
            End If
            '
            txtJTnlcEInvFileName.Text = tmpFileName

            Try
                ' 
                ' ----------------------------------------------------------------
                ' Explanation of code to display PDF in browser window.
                ' ----------------------------------------------------------------
                '    <WebBrowser x : Name="pdfWebViewer"></WebBrowser>
                ' =============================================================================
                ' Bad situation occurs when some special characters are in the filpath string.
                ' Somehow the browser changes the character and gives back a 'file not found'
                ' --- don't know what to do right now. Shouldn't happen too much when it is 
                ' just invoices.
                ' =============================================================================

                '    ToolBar = 1|0 Turns the toolbar On Or off.
                '    statusbar = 1|0 Turns the status bar On Or off.
                '    messages = 1|0 Turns the document message bar On Or off.
                '    navpanes = 1|0 Turns the navigation panes And tabs On Or off.
                '    highlight = lt,rt,top,btm Highlights a specified rectangle On the displayed 
                '
                If File.Exists(tmpFileName) Then
                    JTedi.JTediWorkFileCreate(tmpFileName, tmpWorkingFile)
                    '
                    WebBrowser1.Navigate(tmpWorkingFile)
                Else
                    MsgBox("File not found to display in web browser - JTnlcE~577")
                End If
                OpenedFiles.Add(WebBrowser1)
                tmpTryAgainFlag = False

                ' ==========================================================================
                gboxInv1PDF.Visible = True
                '
            Catch
                tmpTryAgainFlag = False
                Dim tmpYesNo As Integer = MsgBox(" Error found trying to load - " & vbCrLf &
                                   tmpWorkingFile & vbCrLf & vbCrLf &
                                    "To try again, enter 'YES', else enter'NO'" & vbCrLf &
                                    "--- JTnlcE~602",
                                    MsgBoxStyle.YesNo, "J.I.M. - nlcE")
                If tmpYesNo = DialogResult.Yes Then
                    tmpTryAgainFlag = True
                End If
            End Try
        Loop
        '
        Return ""
    End Function
    Private Function JTnlcEShowExistingPDF(ByRef tmpFileName As String) As Boolean
        Dim tmpReturnFlag As Boolean = True
        '
        Dim tmpPOS As Integer = tmpFileName.IndexOf("_", tmpFileName.IndexOf("_") + 1)
        If tmpPOS < 1 Then
            MsgBox("PDF File Name incorrect ---- cannot display file. " & tmpFileName & "JTnlcE~650")
            Return False
        End If
        Dim tmpYear As String = tmpFileName.Substring(tmpPOS + 1, 4)
        ' ---------------------------------------------------------------------------------------------
        ' Added '*' wildcard to file name. This should make it find the file even if customer
        ' invoice information has been appended to the original PDF image name.
        ' This will be used when an invoice is being reviewed.
        ' ---------------------------------------------------------------------------------------------
        Dim tmpFilePathName As String = ""
        ' ---------------------------------------------------------------------------------------------
        ' Routine to create file path that matches the file path with an abreviated file name. This is
        ' used to loacate files that may have already been invoiced --- where additional info is
        ' appended to the original file name.
        ' ---------------------------------------------------------------------------------------------
        '     Dim tmpTrimmedFilePath As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF/" & tmpYear &
        '    tmpFileName.Substring(tmpFileName.Length - 4, 4)
        '
        Dim root As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF/" & tmpYear & "/"
        If System.IO.Directory.Exists(root) Then
            Dim fileEntries As String() = Directory.GetFiles(root)
            '     MsgBox("Files found Integer folder - " & root & " --- " & fileEntries.Count & " JTnlcE~643")
            '       MsgBox("File path ---" & vbCrLf &
            '         "Path - " & root & vbCrLf &
            '        "File name to be found - " & tmpFileName & ".PDF" & vbCrLf &
            '       "~662/JTnlcE")
            For Each fileName As String In fileEntries
                If fileName.ToUpper.Contains(tmpFileName & ".PDF".ToUpper) = True Then
                    tmpFilePathName = fileName
                    '            MsgBox("Found file - >" & fileName & "< ~663/JTnlcE")
                End If
            Next
        End If
        '
        Try
            '
            If tmpFilePathName = "" Then
                MsgBox("Can't find file." & vbCrLf &
                       "File Name - >" & tmpFilePathName & "<- - - JTnlcE~688")
                tmpReturnFlag = False
                Return tmpReturnFlag
            End If
            '

            ' ========================================
            ' ----------------------------------------------------------------
            ' Explanation of code to display PDF in browser window.
            ' ----------------------------------------------------------------
            '    <WebBrowser x : Name="pdfWebViewer"></WebBrowser>
            ' =============================================================================
            ' Bad situation occurs when some special characters are in the filpath string.
            ' Somehow the browser changes the character and gives back a 'file not found'
            ' --- don't know what to do right now. Shouldn't happen too much when it is 
            ' just invoices.
            ' =============================================================================

            '    ToolBar = 1|0 Turns the toolbar On Or off.
            '    statusbar = 1|0 Turns the status bar On Or off.
            '    messages = 1|0 Turns the document message bar On Or off.
            '    navpanes = 1|0 Turns the navigation panes And tabs On Or off.
            '    highlight = lt,rt,top,btm Highlights a specified rectangle On the displayed 
            '         Dim tmpPDFFilePath As String = PDFFilePath
            '
            If File.Exists(tmpFilePathName) Then
                Dim tmpWorkingFile As String = Nothing
                JTedi.JTediWorkFileCreate(tmpFilePathName, tmpWorkingFile)
                WebBrowser1.Navigate(tmpWorkingFile)
            End If
            OpenedFiles.Add(WebBrowser1)
        Catch
            MsgBox("Error trying to display PDF - " & vbCrLf &
                   tmpFilePathName & vbCrLf & "---- JTnlcE~720")
        End Try
        '
        gboxInv1PDF.Visible = True
        '
        Return tmpReturnFlag
    End Function


    ''' <summary>
    ''' Function to fill the values in the CBoxVendShortName drop down.
    ''' </summary>
    ''' <param name="tmpnlcEtype"></param>
    ''' <returns></returns>
    Private Function JTnlcEFillCBoxVendShortName(tmpnlcEtype As String)
        ' ---------------------------------------------------------------
        ' Build list of suggested Vendors from VendInfo (slJTvi) table.
        ' ---------------------------------------------------------------
        cboxJTnlcESupplier.Items.Clear()
        Dim tmpSub As Integer = 0
        Dim tmpShortName As String = ""
        Dim tmpVendType As String = ""
        Do While JTvi.JTviShortNames(tmpSub, tmpShortName, tmpVendType) = "DATA"
            If tmpVendType = tmpnlcEtype Or (tmpVendType = "B" Or tmpnlcEtype = "NJR" Or tmpnlcEtype = "") Then
                If tmpVendType <> "STMT" Then
                    cboxJTnlcESupplier.Items.Add(tmpShortName)
                End If
            End If
            tmpSub += 1
        Loop
        Return ""
    End Function
    Private Sub rbtnVendSearch_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnVendSearch.CheckedChanged
        Dim tmpNLCEntryType As String = "B"
        If rbtnVendSearch.Checked = True Then
            rbtnVendSearch.Checked = False
            If radiobtnJTnlcEMat.Checked = True Then
                tmpNLCEntryType = "M"
            End If
            If radiobtnJTnlcESer.Checked = True Then
                tmpNLCEntryType = "S"
            End If
            JTvi.JTviReturnPanel = "NLC"
            JTvis.JTvisInit(tmpNLCEntryType)
            rbtnVendSearch.Checked = False
        End If
    End Sub
    ''' <summary>
    ''' Utility function to access supplier demographic and terms information. Info is populated on Groupbox on NLC panel.
    ''' Function is called on initial entry and also if an item is to be reviewed.
    ''' </summary>
    ''' <param name="tmpSupplier">Supplier short name</param>
    ''' <returns></returns>
    Private Function JTnlcEFillviFields(tmpSupplier As String) As String
        If radiobtnJTnlcEISItems.Checked = False Then
            If radiobtnJTnlcEAdj.Checked = False Then
                Dim viArray As JTvi.JTviArrayStructure = Nothing
                If JTvi.JTviRetrieveVendInfo(cboxJTnlcESupplier.Text, viArray) <> "DATA" Then
                    MsgBox("Supplier data not found - JTnlcE~771")
                End If
                '
                txtJTviAcctNum.Text = viArray.JTviAcctNum
                txtJTviVendName.Text = viArray.JTviVendName
                txtJTviStreet.Text = viArray.JTviStreet
                txtJTviCityStateZip.Text = viArray.JTviCityStateZip
                Select Case viArray.JTviActivityInterface
                    Case "R"
                        rbtnJTnlcEviReconcile.Checked = True
                        gboxJTnlcPymtSpecInfo.Visible = True
                    Case "E"
                        rbtnJTnlcEviExport.Checked = True
                        '
                        txtJTviDiscPercent.Text = viArray.JTviDiscPercent
                        txtJTnlcviTerms.Text = viArray.JTviPymtTerms
                        mtxtJTnlcEviDueDate.Text = JTrec.JTrecCalcDueDate(mtxtJTnlcEDate.Text, txtJTnlcviTerms.Text)
                        '
                        gboxJTnlcPymtSpecInfo.Visible = True
                    Case "M"
                        rbtnJTnlcEviManual.Checked = True
                    Case "JO"
                        rbtnJTnlcEviJourOnly.Checked = True
                End Select
                '
            End If
        End If
        Return ""
    End Function

    ''' <summary>
    ''' Sub to verify that valid customer ID has been entered or selected.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlcEJobID_LostFocus(sender As Object, e As EventArgs) Handles cboxJTnlcEJobID.LostFocus
        If cboxJTnlcEJobID.Text <> "" Then      'allow operator to skip this entry for now.
            If JTjim.JTjimCustFindFunc(cboxJTnlcEJobID.Text) <> "DATA" Then
                ToolTip1.Show("Customer entry is missing or invalid.", cboxJTnlcEJobID, 40, -55, 6000)
                cboxJTnlcEJobID.Select()
            End If
        End If
        If JTnlcEValidTranCheck("") = True Then
            btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
        End If
    End Sub
    ''' <summary>
    ''' Sub to verify that valid customer/sub ID has been entered or selected.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlcEsubid_LostFocus(sender As Object, e As EventArgs) Handles cboxJTnlcESubID.LostFocus
        Dim tmpSub As String = Nothing
        Dim tmpJobType As String = Nothing
        Dim tmpDeactDate As String = Nothing
        Dim tmpSubID As String
        If cboxJTnlcESubID.Text = "--ValidBlankJobName--" Then
            tmpSubID = ""
        Else
            tmpSubID = cboxJTnlcESubID.Text
        End If

        If cboxJTnlcESubID.Text <> "" Then
            If JTjim.JTjimFindFunc(tmpSub, cboxJTnlcEJobID.Text, tmpSubID, tmpJobType, tmpDeactDate) <> "DATA" Then
                ToolTip1.Show("Customer/Job ID combination" & vbCrLf & "entered is not valid.", cboxJTnlcESubID, 40, -55, 6000)
                cboxJTnlcEJobID.Select()
                Exit Sub
            Else
                cboxJTnlcESubID.Text = tmpSubID
                JTnlcEPostJobNavigation()
            End If
        End If
        If JTnlcEValidTranCheck("") = True Then
            btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
        End If
    End Sub
    ''' <summary>
    ''' Sub to verify that a valid NJR account has been entered or selected.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlcENJREAcct_LostFocus(sender As Object, e As EventArgs) Handles cboxJTnlcENJREAcct.LostFocus
        Dim tmpNJREAcctDesc As String = Nothing
        JTnlcEBldFileName() ' call function to build new file name for EDI invoice PDF
        If JTVarMaint.JTvmValidateUpdateNJREA("Validate", cboxJTnlcENJREAcct.Text, tmpNJREAcctDesc) <> "EXISTS" Then
            ToolTip1.Show("NJR Account is missing or invalid.", cboxJTnlcENJREAcct, 40, -55, 6000)
            cboxJTnlcENJREAcct.Select()
            Exit Sub
        End If
        If JTnlcEValidTranCheck("") = True Then
            btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
        End If
        txtJTnlcEDecrpt.Select()
    End Sub
    ''' <summary>
    ''' This sub control the panel specification and display based upon
    ''' the type of transaction being entered. It is called after a valid Customer/Job
    ''' combination has been entered. This sub is not used for NJR.
    ''' </summary>
    Sub JTnlcEPostJobNavigation()
        ' Handles adjustment transaction entry
        If radiobtnJTnlcEISItems.Checked = False Then
            txtJTnlcEDecrpt.Select()
            Exit Sub
        Else
            ' ----------------------------------------------------------------------
            ' Handles IS/IH transaction entry.
            ' Before linking out to the IS/IH logic, existance of good data in the
            ' necessary fields is validated.
            ' ----------------------------------------------------------------------
            Dim tmpGoodTransaction As Boolean = True
            Dim tmpMsgBoxText As String = "NLC Entry - Transaction Error List" & vbCrLf & vbCrLf
            '
            Dim tmpSub As String = Nothing
            Dim tmpJobType As String = Nothing
            Dim tmpDeactDate As String = Nothing
            If JTjim.JTjimFindFunc(tmpSub, cboxJTnlcEJobID.Text, cboxJTnlcESubID.Text, tmpJobType, tmpDeactDate) <> "DATA" Then
                tmpGoodTransaction = False
                tmpMsgBoxText = tmpMsgBoxText & "- Customer/Job entered is not valid" & vbCrLf
            End If
            If TestForValidDate(mtxtJTnlcEDate.Text) = False Or TestForValidDate(mtxtJTnlcEEntDate.Text) = False Then
                tmpGoodTransaction = False
                tmpMsgBoxText = tmpMsgBoxText & "- The Entry and/or Invoice date is/are not valid." & vbCrLf
            End If
            '
            If tmpGoodTransaction = False Then
                MsgBox(tmpMsgBoxText)
                Exit Sub
            End If
            '
            Dim tmpCustID As String = cboxJTnlcEJobID.Text
            Dim tmpJobID As String = cboxJTnlcESubID.Text
            Dim tmpEntDate As String = mtxtJTnlcEEntDate.Text

            JTis.JTisNLCRequest(tmpCustID, tmpJobID, tmpEntDate)
            Me.Close()
            Exit Sub
            '
        End If
        ' Handle other transactions --- Material and Services
        txtJTnlcEDecrpt.Select()
    End Sub
    ''' <summary>
    ''' sub to insure uppercase entry in cboxjtnlceJobID.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlceJobID_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboxJTnlcEJobID.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub
    ''' <summary>
    ''' sub to insure uppercase entry in cboxjtnlceSubID.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlceSubID_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboxJTnlcESubID.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub
    ''' <summary>
    ''' sub to insure uppercase entry in cboxjtnlceSupplier.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxjtnlceSupplier_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboxJTnlcESupplier.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub
    '
    '
    ''' <summary>
    ''' Sub to validate Invoice Date to be logical date.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTnlcEDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTnlcEDate.LostFocus
        If JTnlcEValidTranCheck("") = True Then
            btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
        End If
    End Sub
    ''' <summary>
    ''' Sub to reformat sup cost into xxx.xx format.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTnlcSupCost_LostFocus(sender As Object, e As EventArgs) Handles txtJTnlcESupCost.LostFocus
        txtJTnlcESupCost.Text = String.Format("{0:0.00}", Val(txtJTnlcESupCost.Text))
        '
        ' Check to see if there is a discount percent and calculate amount
        If rbtnJTnlcEviExport.Checked = True Then
            If txtJTviDiscPercent.Text <> "" Then
                Dim tmpDiscPercent As Long = Val(txtJTviDiscPercent.Text) / 100
                txtJTnlcviDiscAmt.Text = String.Format("{0:0.00}", Math.Round(Val(txtJTnlcESupCost.Text) * tmpDiscPercent, 2))
            End If
        End If
        txtJTnlcEMrgnPC.Text = ""
        If radiobtnJTnlcENJRE.Checked <> True Then
            If radiobtnJTnlcEMat.Checked = True Then
                txtJTnlcEMrgnPC.Text = JTjim.JTjimMrgnPC(cboxJTnlcEJobID.Text, cboxJTnlcESubID.Text, "M")
            End If
            If radiobtnJTnlcESer.Checked = True Then
                txtJTnlcEMrgnPC.Text = JTjim.JTjimMrgnPC(cboxJTnlcEJobID.Text, cboxJTnlcESubID.Text, "M")
            End If
        End If
        If txtJTnlcEMrgnPC.Text = "" Then
            txtJTnlcEAmtTBInvd.Text = String.Format("{0:0.00}", Val(txtJTnlcESupCost.Text))
        Else
            txtJTnlcEAmtTBInvd.Text = String.Format("{0:0.00}", Val(txtJTnlcESupCost.Text) * (1 + (Val(txtJTnlcEMrgnPC.Text / 100))))
        End If
        '
        If JTnlcEMode = "EDI" Then
            JTnlcEBldFileName()    'call function to build new file name.
        Else
            If JTnlcEValidTranCheck("DISPMSG") = False Then
                Exit Sub
            End If
        End If
        '
        txtJTnlcEXRef.Select()
        '

        btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
    End Sub
    ''' <summary>
    ''' sub to validate date in Due Date field. This field should be populated by
    ''' a combination of the vi terms and the invoice date just entered. The operator can override
    ''' the formula-generated date.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTnlcEviDueDate_LostFocus(sender As Object, e As EventArgs)
        txtJTnlcEXRef.Select()
        '
        If JTnlcEValidTranCheck("DISPMSG") = False Then
            Exit Sub
        End If
        btnJTnlcERecord.Text = "RECORD" & vbCrLf & "UPDATE"
    End Sub

    ''' <summary>
    ''' Function to fill the JTnlcStructure format with data from the panel. 
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcEPaneltoItem()
        tmpItem.EntryDate = mtxtJTnlcEEntDate.Text
        tmpItem.InvDate = mtxtJTnlcEDate.Text
        tmpItem.JobID = cboxJTnlcEJobID.Text
        tmpItem.SubID = cboxJTnlcESubID.Text
        ' ----------------------------------------------------------------------
        ' tmpItem.MorS only updated from panel on new entries. Entries being 
        ' reviewed and modified could have other codes. These codes will be left
        ' over in the tmpItem that brought the data in to populate the panel
        ' for the review/update process.
        ' ----------------------------------------------------------------------
        If JTnlcEMode = "EDI" Or JTnlcEMode = "MANUAL" Then
            tmpItem.viRcrdEntrySrce = Nothing
            If radiobtnJTnlcEMat.Checked = True Then
                tmpItem.MorS = "M"
            End If
            If radiobtnJTnlcESer.Checked = True Then
                tmpItem.MorS = "S"
            End If
            If radiobtnJTnlcEAdj.Checked = True Then
                tmpItem.MorS = "ADJ"
            End If
            If radiobtnJTnlcENJRE.Checked = True Then
                tmpItem.MorS = "NJR"
                tmpItem.viRcrdEntrySrce = "NLC"
            End If
        End If

        tmpItem.Descrpt = txtJTnlcEDecrpt.Text
        tmpItem.Supplier = cboxJTnlcESupplier.Text
        tmpItem.SupInvNum = txtJTnlcEInvNum.Text
        tmpItem.SupCrossRefNum = txtJTnlcEXRef.Text
        tmpItem.SupInvImageFile = txtJTnlcEInvFileName.Text
        tmpItem.SupCost = txtJTnlcESupCost.Text
        tmpItem.MrgnPC = txtJTnlcEMrgnPC.Text
        tmpItem.TBInvd = txtJTnlcEAmtTBInvd.Text
        '
        If rbtnJTnlcEviReconcile.Checked = True Then
            tmpItem.PymtSwitch = "R"
        End If
        If rbtnJTnlcEviManual.Checked = True Then
            tmpItem.PymtSwitch = "M"
        End If
        If rbtnJTnlcEviExport.Checked = True Then
            tmpItem.PymtSwitch = "E"
        End If
        If rbtnJTnlcEviJourOnly.Checked = True Then
            tmpItem.PymtSwitch = "JO"
        End If
        '
        tmpItem.NJREAcct = cboxJTnlcENJREAcct.Text
        tmpItem.viAcctNum = txtJTviAcctNum.Text
        tmpItem.viVendName = txtJTviVendName.Text
        tmpItem.viStreet = txtJTviStreet.Text
        tmpItem.viCityState = txtJTviCityStateZip.Text
        tmpItem.viTerms = txtJTnlcviTerms.Text
        tmpItem.viDueDate = mtxtJTnlcEviDueDate.Text
        tmpItem.viDiscpercent = txtJTviDiscPercent.Text
        tmpItem.viDiscAmt = txtJTnlcviDiscAmt.Text
        Return ""
    End Function
    ''' <summary>
    ''' Function to modify the setting of the JTnlcE display for the
    ''' transaction being processed.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcEFrmtPanelDisplay() As String
        ' -------------------------------------------------------------
        ' Resetting panel values that might have been changed.
        ' -------------------------------------------------------------
        gboxJTYnlcEMrgnInvd.Visible = True
        gboxJTnlcENJRA.Visible = False
        btnJTnlcEISITems.Visible = False
        lblJTnlcEDesc.Text = "Description of Material or Service"
        lblJTnlcECost.Text = "Cost"
        lblJTnlcEFileName.Visible = True
        lblJTnlcEInvNum.Visible = True
        lblJTnlcELookUp.Visible = True
        lblJTnlcESupplr.Visible = True
        lblJTnlcEXref.Visible = True
        txtJTnlcEInvNum.Visible = True
        '
        rbtnVendSearch.Visible = True
        ' clearing out Supplier name in case one had been entered and then option changed.
        ' ----------------------------------------------------------------
        ' Commented line out so that the Suuplier name on item reviews from
        ' invoice production and reconciliation queries does not get blanked
        ' out.
        ' ---------------------------
        ' Still might have a problem here on normal transactions if type
        ' is changed.
        ' -----------------------------------------------------------------
        '    cboxJTnlcESupplier.Text = Nothing
        cboxJTnlcESupplier.Visible = True
        txtJTnlcEInvNum.Visible = True
        txtJTnlcEXRef.Visible = True
        txtJTnlcEInvFileName.Visible = True
        '
        gboxJTnlcEInvInfo.Visible = True
        gboxJTnlcPymtSpecInfo.Visible = False
        '
        If radiobtnJTnlcENJRE.Checked = True Then
            gboxJTnlcECustJob.Visible = False
            gboxJTnlcENJRA.Visible = True
            gboxJTYnlcEMrgnInvd.Visible = False
            JTnlcEFillCBoxVendShortName("")
        End If
        '
        If radiobtnJTnlcEMat.Checked = True Then
            gboxJTnlcECustJob.Visible = True
            gboxJTnlcENJRA.Visible = False
            gboxJTYnlcEMrgnInvd.Visible = True
            JTnlcEFillCBoxVendShortName("M")
        End If
        '
        If radiobtnJTnlcESer.Checked = True Then
            gboxJTnlcECustJob.Visible = True
            gboxJTnlcENJRA.Visible = False
            gboxJTYnlcEMrgnInvd.Visible = True
            JTnlcEFillCBoxVendShortName("S")
        End If
        '
        If radiobtnJTnlcEISItems.Checked = True Then
            gboxJTnlcECustJob.Visible = True
            gboxJTnlcENJRA.Visible = False
            gboxJTYnlcEMrgnInvd.Visible = False
            gboxJTnlcEInvInfo.Visible = False
            btnJTnlcEISITems.Visible = True
        End If
        '
        If radiobtnJTnlcEAdj.Checked = True Then
            gboxJTnlcECustJob.Visible = True
            gboxJTYnlcEMrgnInvd.Visible = False
            gboxJTnlcENJRA.Visible = False
            lblJTnlcEDesc.Text = "Description of Adjustment"
            lblJTnlcECost.Text = "Amount of Adjustment"
            lblJTnlcEFileName.Visible = False
            lblJTnlcEInvNum.Visible = False
            lblJTnlcELookUp.Visible = False
            lblJTnlcESupplr.Visible = False
            lblJTnlcEXref.Visible = False
            txtJTnlcEInvNum.Visible = False
            '   
            rbtnVendSearch.Visible = False
            cboxJTnlcESupplier.Visible = False
            txtJTnlcEInvNum.Visible = False
            txtJTnlcEXRef.Visible = False
            txtJTnlcEInvFileName.Visible = False

        End If
        Return ""
    End Function

    ''' <summary>
    ''' Function to enable all entry fields. Some fields will be disabled
    ''' when items are reviewed or updated and have already been invoiced,
    ''' reconciled and/or exported. This function resets those changes.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcEEnableAllEntryFields() As String

        cboxJTnlcEJobID.Enabled = True
        cboxJTnlcESubID.Enabled = True
        cboxJTnlcENJREAcct.Enabled = True
        cboxJTnlcESupplier.Enabled = True
        '
        mtxtJTnlcEEntDate.Enabled = True
        mtxtJTnlcEDate.Enabled = True
        '
        txtJTnlcEInvNum.Enabled = True
        txtJTnlcESupCost.Enabled = True
        txtJTnlcEMrgnPC.Enabled = True
        txtJTnlcEAmtTBInvd.Enabled = True
        txtJTnlcEXRef.Enabled = True
        txtJTnlcEInvFileName.Enabled = True
        '
        txtJTviAcctNum.Enabled = True
        txtJTviVendName.Enabled = True
        txtJTviStreet.Enabled = True
        txtJTviCityStateZip.Enabled = True
        txtJTnlcviDiscAmt.Enabled = True
        mtxtJTnlcEviDueDate.Enabled = True
        txtJTnlcviTerms.Enabled = True
        txtJTviDiscPercent.Enabled = True
        txtJTviAcctNum.Enabled = True
        '
        radiobtnJTnlcENJRE.Enabled = True
        radiobtnJTnlcEMat.Enabled = True
        radiobtnJTnlcESer.Enabled = True
        radiobtnJTnlcEISItems.Enabled = True
        radiobtnJTnlcEAdj.Enabled = True
        '
        rbtnVendSearch.Enabled = True
        '
        Return ""
    End Function
    ''' <summary>
    ''' Function called on item being reviewed or modified to make sure only
    ''' field modification are allowed on fields that might contradict prior activities.
    ''' For example, if an entry has been invoiced, the jobID, subID or invoiced amounts
    ''' can no longer be changed. The operator will be informed and the fields (while
    ''' still visible) will be disabled.
    ''' </summary>
    ''' <param name="tmpMode"></param>
    ''' <returns></returns>
    Private Function JTnlcECustomizeEntryFields(tmpMode As String) As String
        tmpRestrictionsApplyInv = False
        tmpRestrictionsApplyRecExt = False
        Dim tmpMsgBoxText As String = "Invoice - " & tmpItem.Supplier & "-" & tmpItem.SupInvNum & "-" & tmpItem.InvDate &
            vbCrLf & vbCrLf
        If tmpItem.CustInvNumber <> "" Then  'entry has been invoiced.
            tmpMsgBoxText = tmpMsgBoxText & "Has been invoiced to the customer - " & tmpItem.CustInvNumber & "-" & tmpItem.CustInvDate &
                 vbCrLf & "Fields to change Customer, Job, transaction type and amounts are disabled." & vbCrLf & vbCrLf
            cboxJTnlcEJobID.Enabled = False
            cboxJTnlcESubID.Enabled = False
            cboxJTnlcENJREAcct.Enabled = False
            mtxtJTnlcEEntDate.Enabled = False
            mtxtJTnlcEDate.Enabled = False
            txtJTnlcEInvNum.Enabled = False
            txtJTnlcESupCost.Enabled = False
            txtJTnlcEMrgnPC.Enabled = False
            txtJTnlcEAmtTBInvd.Enabled = False
            radiobtnJTnlcENJRE.Enabled = False
            radiobtnJTnlcEMat.Enabled = False
            radiobtnJTnlcESer.Enabled = False
            radiobtnJTnlcEISItems.Enabled = False
            radiobtnJTnlcEAdj.Enabled = False
            tmpRestrictionsApplyInv = True
        End If
        '
        If tmpItem.ReconcileDate <> "" Or tmpItem.ReconcileDate = "PENDING" _
                                    Or tmpItem.AcctExtDoc <> "" Then     'entry has been reconciled or exported for payment.
            tmpMsgBoxText = tmpMsgBoxText & "Invoice has been reconciled or exported. Changes are no longer allowed on anything" &
                 "except Customer and Job assignment (assuming it has not been invoiced to the customer)."
            cboxJTnlcESupplier.Enabled = False
            '
            mtxtJTnlcEEntDate.Enabled = False
            mtxtJTnlcEDate.Enabled = False
            '
            txtJTnlcEInvNum.Enabled = False
            txtJTnlcESupCost.Enabled = False
            txtJTnlcEMrgnPC.Enabled = False
            txtJTnlcEAmtTBInvd.Enabled = False
            txtJTnlcEXRef.Enabled = False
            txtJTnlcEInvFileName.Enabled = False
            '
            txtJTviAcctNum.Enabled = False
            txtJTviVendName.Enabled = False
            txtJTviStreet.Enabled = False
            txtJTviCityStateZip.Enabled = False
            txtJTnlcviDiscAmt.Enabled = False
            mtxtJTnlcEviDueDate.Enabled = False
            txtJTnlcviTerms.Enabled = False
            txtJTviDiscPercent.Enabled = False
            txtJTviAcctNum.Enabled = False
            '
            radiobtnJTnlcENJRE.Enabled = False
            radiobtnJTnlcEMat.Enabled = False
            radiobtnJTnlcESer.Enabled = False
            radiobtnJTnlcEISItems.Enabled = False
            radiobtnJTnlcEAdj.Enabled = False
            '
            rbtnVendSearch.Enabled = False
            tmpRestrictionsApplyRecExt = True
            '
        End If
        If tmpRestrictionsApplyInv = True Or tmpRestrictionsApplyRecExt = True Then
            MsgBox(tmpMsgBoxText)
            btnJTnlcEDeleteEntry.Visible = False
        End If
        '      btnJtnlcEExit.Text = "E X I T"
        Return ""
    End Function
    ''' <summary>
    ''' Function handles renaming and moving the PDF invoice file that has just been entered into J.I.M.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcERenameMovePDF()
        ' Rename original file.

        ' Determine correct year of invoice for selection appropiate year folder
        ' 
        Dim tmpInvYear As String = Mid(mtxtJTnlcEDate.Text, mtxtJTnlcEDate.Text.Length - 3, 4)
        Dim tmpEDIFileName As String = JTVarMaint.JTvmFilePath & "EDIInputFiles/" & JTnlcEInvFileName
        Dim tmpEDIFileRoute As String = JTVarMaint.JTvmFilePath & "EDIInputFiles/"
        Dim tmpDestinationLocation As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF\" & tmpInvYear & "\"



        ' Replace any ":" in file name being built with "~". This will occur when NJR account like "SHOP:TOOLS"
        ' are used. Perhaps an edit should be created when creating accounts so that this would not happen. 1/2/2021
        '
        If txtJTnlcEInvFileName.Text <> "" Then
            txtJTnlcEInvFileName.Text = txtJTnlcEInvFileName.Text.Replace(":", "~")
        End If


        '  rename file in EDIInputFiles folder
        '      If File.Exists(tmpEDIFileName) = False Then
        Dim tmpRenameToName As String = txtJTnlcEInvFileName.Text & ".pdf"
        My.Computer.FileSystem.RenameFile(tmpEDIFileName, tmpRenameToName)
        '    End If
        '
        ' add meta data to file
        If txtJTnlcESupCost.Text <> "" Then
            JTnlc.AddMetaToExistingPDF(tmpEDIFileRoute,
                          tmpRenameToName,
                          txtJTnlcESupCost.Text)
        End If
        '
        ' move file


        If File.Exists(tmpDestinationLocation & tmpRenameToName) = False Then
            My.Computer.FileSystem.MoveFile(tmpEDIFileRoute & tmpRenameToName, tmpDestinationLocation & tmpRenameToName)
            tmpItem.SupInvImageFile = tmpRenameToName
        End If



        Return ""
    End Function

    Private Function JTnlcEMovePDFBack(ByRef tmpFileName As String) As Boolean
        Dim tmpPOS As Integer = tmpFileName.IndexOf("_", tmpFileName.IndexOf("_") + 1)
        If tmpPOS < 1 Then
            MsgBox("PDF File Name incorrect ---- cannot display file. " & tmpFileName & "JTnlcE~1293")
            Return False
        End If
        Dim tmpYear As String = tmpFileName.Substring(tmpPOS + 1, 4)

        Dim tmpSourceLocation As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF/" &
            tmpYear & "/" & tmpFileName & ".PDF"
        Dim tmpDestinationLocation As String = JTVarMaint.JTvmFilePath & "EDIInputFiles/" & tmpFileName & ".PDF"
        If vbYes = MsgBox("Will this invoice be reprocessed?" & vbCrLf &
                          "(If NO, invoice image will be deleted) - (NLC.018)", 4, "J.I.M. - Invoice Images") Then

            My.Computer.FileSystem.MoveFile(tmpSourceLocation, tmpDestinationLocation)
        Else
            My.Computer.FileSystem.DeleteFile(tmpSourceLocation)
        End If

        Return True
    End Function
    ''' <summary>
    ''' Function to break up incoming file name. The idea is to use the fields to identify the vendor.
    ''' 
    ''' </summary>
    ''' <param name="tmpExpandedfileName"></param>
    ''' <returns></returns>
    Private Function JTnlcEFillPanelFields(tmpExpandedfileName As String)
        Dim tmpFileNameLength As Integer = Len(tmpExpandedfileName)
        Dim tmpFileName As String = tmpExpandedfileName.Substring(0, tmpFileNameLength - 4)
        Dim tmpFileNameSave As String = tmpFileName
        Dim tmpField(6) As String
        If tmpFileName.IndexOf("_") <> -1 Then
            tmpField(1) = tmpFileName.Substring(0, tmpFileName.IndexOf("_"))
            tmpFileName = tmpFileName.Substring(tmpFileName.IndexOf("_") + 1)
        End If
        If tmpFileName.IndexOf("_") <> -1 Then
            tmpField(2) = tmpFileName.Substring(0, tmpFileName.IndexOf("_"))
            tmpFileName = tmpFileName.Substring(tmpFileName.IndexOf("_") + 1)
        End If
        If tmpFileName.IndexOf("_") <> -1 Then
            tmpField(3) = tmpFileName.Substring(0, tmpFileName.IndexOf("_"))
            tmpFileName = tmpFileName.Substring(tmpFileName.IndexOf("_") + 1)
        End If
        If tmpFileName.IndexOf("_") <> -1 Then
            tmpField(4) = tmpFileName.Substring(0, tmpFileName.IndexOf("_"))
            tmpFileName = tmpFileName.Substring(tmpFileName.IndexOf("_") + 1)
        End If
        If tmpFileName.IndexOf("_") <> -1 Then
            tmpField(5) = tmpFileName.Substring(0, tmpFileName.IndexOf("_"))
            tmpFileName = tmpFileName.Substring(tmpFileName.IndexOf("_") + 1)
        End If
        ' ------------------------------------------------------------------------------
        ' Code to try to figure out where segments can be used. Fill any available
        ' panel fields.
        ' ------------------------------------------------------------------------------
        Dim tmpSub As Integer = 1
        Do While tmpSub < 6
            If tmpField(tmpSub) <> "" Then

                If IsNumeric(tmpField(tmpSub)) = True Then
                    If tmpField(tmpSub).Length = 8 Then
                        ' Possibly the invoice date
                        If tmpField(tmpSub).Substring(0, 2) = "20" Then
                            If tmpField(tmpSub).Substring(4, 2) > "00" Then
                                If tmpField(tmpSub).Substring(4, 2) < "13" Then
                                    ' Use as invoice date --- probable a better way to do this
                                    Dim tmpMMddYYYYDate As String = tmpField(tmpSub).Substring(4, 2) & tmpField(tmpSub).Substring(6, 2) &
                                           tmpField(tmpSub).Substring(0, 4)
                                    mtxtJTnlcEDate.Text = String.Format("{0:MMddYYYY}", tmpMMddYYYYDate)
                                End If
                            End If
                        End If
                    End If
                    JTnlcELookForVendor(tmpField(tmpSub))
                Else
                    'Alpha --- may be company abbreviation
                    JTnlcELookForVendor(tmpField(tmpSub))
                End If
                '
            End If
            tmpSub += 1
        Loop
        ' ------------------------------------------------------------------
        ' Use internal text found in PDF invoice image to look for invoice
        ' number and date as well as amount of invoice.
        ' ------------------------------------------------------------------
        Dim tmpPathFileName As String = JTVarMaint.JTvmFilePath & "EDIInputFiles/" & tmpExpandedfileName
        Dim tmpPDFText As String = Nothing
        '      MsgBox("Looking for data in - " & tmpPathFileName & " - NLCe~1286")
        FindTextinPDF(tmpPathFileName, tmpPDFText)
        '      MsgBox(tmpPDFText)

        Dim tmpBiggestNumber As Decimal = 0.00
        Dim tmpInvoiceNumber As String = Nothing
        Dim tmpInvoiceDate As String = Nothing
        Dim tmpSearchLocation As Integer = 0
        Dim tmpCharGroup As String = Nothing
        '
        ' ---------------------------------------------------------------------
        ' If added to skip around 'HD' ... better code should be added to 'learn'
        ' how to get info from specific vendor invoices. 7/28/2020
        ' ---------------------------------------------------------------------
        '
        If cboxJTnlcESupplier.Text <> "HD" Then
            Do While tmpSearchLocation < tmpPDFText.Length
                If JTNLCeFindCharGroup(tmpPDFText, tmpSearchLocation, tmpCharGroup) = True Then
                    '          MsgBox(tmpCharGroup)
                    ' Look for Invoice Total
                    tmpCharGroup = tmpCharGroup.Replace("$", "")
                    tmpCharGroup = tmpCharGroup.Replace(",", "")
                    If tmpCharGroup.Contains(".") = True Then
                        If tmpCharGroup.Length - (tmpCharGroup.IndexOf(".") + 1) = 2 Then

                            ' 999.99
                            Dim tmpNumericChk As String = tmpCharGroup.Replace(".", "")

                            If IsNumeric(tmpNumericChk) = True Then
                                ' found amount
                                If Val(tmpCharGroup) > tmpBiggestNumber Then
                                    tmpBiggestNumber = Val(tmpCharGroup)
                                End If
                            End If
                        End If
                    End If
                    ' Looking for invoice number.
                    If IsNumeric(tmpCharGroup) = True Then
                        If Val(tmpCharGroup > 1000) Then    ' trying to eliminated page number entry
                            If tmpInvoiceNumber = Nothing Then
                                tmpInvoiceNumber = tmpCharGroup
                            End If
                        End If
                    End If
                    ' Looking for invoice date.
                    If CountCharacter(tmpCharGroup, "/") = 2 Then
                        If IsNumeric(tmpCharGroup.Replace("/", "")) Then
                            If tmpCharGroup.Length > 5 And tmpCharGroup.Length < 11 Then
                                ' probably found date ... now make logic to create MM/DD/YYYY

                                If tmpCharGroup.IndexOf("/") = 1 Then
                                    tmpCharGroup = "0" & tmpCharGroup
                                End If
                                If tmpCharGroup.IndexOf("/") = 4 Then
                                    tmpCharGroup = tmpCharGroup.Substring(0, 3) & "0" & tmpCharGroup.Substring(3)
                                End If
                                If tmpCharGroup.Length <> 10 Then
                                    tmpCharGroup = tmpCharGroup.Substring(0, 6) & "20" & tmpCharGroup.Substring(6)
                                    '    mm/dd/yyyy
                                End If
                                If tmpInvoiceDate = Nothing Then
                                    tmpInvoiceDate = tmpCharGroup
                                End If
                            End If
                        End If
                    End If
                End If
            Loop
            '
            txtJTnlcEInvNum.Text = tmpInvoiceNumber
            txtJTnlcESupCost.Text = tmpBiggestNumber
            mtxtJTnlcEDate.Text = tmpInvoiceDate
        End If
        Return ""
    End Function

    Private Function JTNLCeFindCharGroup(ByRef tmpPDFText As String,
                                         ByRef tmpSearchLocation As Integer,
                                         ByRef tmpCharGroup As String) As Boolean
        Dim tmpDataReturned = False
        tmpCharGroup = Nothing
        Do While tmpSearchLocation < tmpPDFText.Length
            If tmpPDFText.Substring(tmpSearchLocation, 1) <> " " And
                tmpPDFText.Substring(tmpSearchLocation, 1) <> vbLf And
                tmpPDFText.Substring(tmpSearchLocation, 1) <> vbTab And
                tmpPDFText.Substring(tmpSearchLocation, 1) <> vbCr Then
                tmpCharGroup &= tmpPDFText.Substring(tmpSearchLocation, 1)
            Else
                If tmpCharGroup <> "" Then
                    Return True
                End If
            End If
            tmpSearchLocation += 1
        Loop
        If tmpCharGroup <> "" Then
            tmpDataReturned = True
        End If
        Return tmpDataReturned
    End Function
    ''' <summary>
    ''' Function to search for match on EDI invoice file name fields ro identify vendor
    ''' and use data to partially fill fields on the JTnlcE panel.
    ''' </summary>
    ''' <param name="tmpField"></param>
    ''' <returns></returns>
    Private Function JTnlcELookForVendor(ByRef tmpField As String) As Boolean
        Dim tmpFoundVendor As Boolean = False
        Dim tmpSub As Integer = 0
        Dim tmpJTviRec As JTvi.JTviArrayStructure = Nothing
        '
        tmpField = tmpField.Trim
        '
        If tmpField <> "" Then
            Do While JTvi.JTviReadVendInfo(tmpSub, tmpJTviRec) = "DATA"
                If tmpJTviRec.JTviVendType <> "STMT" Then
                    Dim tmpAcctnum As String = Nothing
                    If tmpJTviRec.JTviAcctNum <> Nothing Then
                        tmpAcctnum = tmpJTviRec.JTviAcctNum.Trim
                    End If
                    Dim tmpShortname As String = tmpJTviRec.JTviShortName.Trim
                    If tmpShortname = tmpField Or
                     tmpField = tmpAcctnum Then
                        cboxJTnlcESupplier.Text = tmpJTviRec.JTviShortName
                        Select Case tmpJTviRec.JTviVendType
                            Case "M", "B"
                                radiobtnJTnlcEMat.Checked = True
                            Case "S"
                                radiobtnJTnlcESer.Checked = True
                        End Select
                        txtJTviVendName.Text = tmpJTviRec.JTviVendName
                        txtJTviCityStateZip.Text = tmpJTviRec.JTviStreet
                        txtJTviCityStateZip.Text = tmpJTviRec.JTviCityStateZip
                        txtJTviAcctNum.Text = tmpJTviRec.JTviAcctNum
                        txtJTnlcviTerms.Text = tmpJTviRec.JTviPymtTerms
                        txtJTviDiscPercent.Text = tmpJTviRec.JTviDiscPercent
                        Select Case tmpJTviRec.JTviActivityInterface
                            Case "R"
                                rbtnJTnlcEviReconcile.Checked = True
                            Case "E"
                                rbtnJTnlcEviExport.Checked = True
                            Case "M"
                                rbtnJTnlcEviManual.Checked = True
                        End Select
                        tmpFoundVendor = True
                        Exit Do
                    End If
                End If

            Loop
        End If
        Return tmpFoundVendor
    End Function
    ''' <summary>
    ''' Utility function to create slJTnlc key and save new item.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcESavetoNLC()
        Dim tmpKey As String = JTnlc.JTnlcBldSLKey(tmpItem.InvDate, tmpItem.JobID, tmpItem.SubID, tmpItem.CustInvDate, tmpItem.TBInvd)
        JTnlc.JTnlcAddSLItem(tmpKey, tmpItem, "Recording JTnlcE item - JTnlcE~1140")
        Return ""
    End Function

    Private Function JTnlcEClearPanel()
        '
        cboxJTnlcEJobID.Text = Nothing
        cboxJTnlcESubID.Text = Nothing
        cboxJTnlcESupplier.Text = Nothing
        cboxJTnlcENJREAcct.Text = Nothing
        '
        txtJTnlcEDecrpt.Text = Nothing
        txtJTnlcEInvNum.Text = Nothing
        txtJTnlcESupCost.Text = Nothing
        txtJTnlcEMrgnPC.Text = Nothing
        txtJTnlcEAmtTBInvd.Text = Nothing
        txtJTnlcEXRef.Text = Nothing
        txtJTnlcEInvFileName.Text = Nothing
        mtxtJTnlcEDate.Text = Nothing
        gboxJTnlcPymtSpecInfo.Visible = False
        mtxtJTnlcEDate.Select()
        Return ""
    End Function
    ''' <summary>
    ''' Sub to manage navigation after leaving description txt field.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTnlcEDecrpt_LostFocus(sender As Object, e As EventArgs) Handles txtJTnlcEDecrpt.LostFocus
        If radiobtnJTnlcEAdj.Checked = True Then
            txtJTnlcESupCost.Select()
        Else
            cboxJTnlcESupplier.Select()
        End If

    End Sub
    ''' <summary>
    ''' Sub to handle merging two PDF files --- for multipage invoices.
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTnlceMergePDF_Click(sender As Object, e As EventArgs) Handles btnJTnlceMergePDF.Click
        btnJTnlceMergePDF.Visible = False
        WebBrowser1.DocumentText = ""
        WebBrowser1.Navigate("about:blank")
        WebBrowser1.Dispose()

        Dim tmpMergeCandidate As String = Nothing
        '
        CheckforMergeCandidate(txtJTnlcEInvFileName.Text, tmpMergeCandidate)
        '
        Dim tmpFilePrefix As String = JTVarMaint.JTvmFilePath & "EDIInputFiles\"
        Dim outPutPDF As String = tmpFilePrefix & "TmpMergedPDF.PDF"
        '
        MergePDFFiles(outPutPDF, txtJTnlcEInvFileName.Text, tmpMergeCandidate)
        '

        ' Delete orignal Files
        My.Computer.FileSystem.DeleteFile(txtJTnlcEInvFileName.Text)
        My.Computer.FileSystem.DeleteFile(tmpMergeCandidate)
        ' Rename outputfile to mergecandidate name
        Dim tmpRenameFileName As String = Mid(tmpMergeCandidate, Len(tmpFilePrefix) + 1)
        My.Computer.FileSystem.RenameFile(outPutPDF, tmpRenameFileName)
        JTnlcEFillInvImage(tmpRenameFileName)
    End Sub
    ''' <summary>
    ''' Function to build new file name for EDI invoice being processed.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcEBldFileName() As Boolean
        If JTnlcEValidTranCheck("DISPMSG") = False Then
            Return False
        End If
        If radiobtnJTnlcENJRE.Checked = False Then
            txtJTnlcEInvFileName.Text = cboxJTnlcESupplier.Text & "_" & txtJTnlcEInvNum.Text &
                                        "_" & mtxtJTnlcEDate.Text.Substring(6, 4) &
                                        mtxtJTnlcEDate.Text.Substring(0, 2) &
                                        mtxtJTnlcEDate.Text.Substring(3, 2) & "_" &
                                        cboxJTnlcEJobID.Text & "_" & cboxJTnlcESubID.Text
        Else
            txtJTnlcEInvFileName.Text = cboxJTnlcESupplier.Text & "_" & txtJTnlcEInvNum.Text &
                                        "_" & mtxtJTnlcEDate.Text.Substring(6, 4) &
                                        mtxtJTnlcEDate.Text.Substring(0, 2) &
                                        mtxtJTnlcEDate.Text.Substring(3, 2) & "_" &
                                        "NJR" & "_" & cboxJTnlcENJREAcct.Text
        End If
        Return True
    End Function

    Private Function ReleaseOpenPDFs() As String
        ' ----------------------------------------------------------------------------------
        ' Clearing open files from browser --- hopefully fixed deleting - Open File problem.
        ' ----------------------------------------------------------------------------------
        If OpenedFiles.Count > 0 Then
            '        MsgBox("OpenFiles.count = " & OpenedFiles.Count & " - JTnlcE~1544")
            For Each WebWrowser1 As WebBrowser In OpenedFiles
                WebBrowser1.Dispose()
            Next
            OpenedFiles.Clear()
        End If
        Return ""
    End Function


End Class