﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTaup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtJTaupJobID = New System.Windows.Forms.TextBox()
        Me.txtJTaupJobDesc = New System.Windows.Forms.TextBox()
        Me.txtJTaupSubID = New System.Windows.Forms.TextBox()
        Me.lvJTaupJobDetails = New System.Windows.Forms.ListView()
        Me.IorC = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EntDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Description = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CustApproval = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Amount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.RefDoc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtJTaupInitAmt = New System.Windows.Forms.TextBox()
        Me.lvJTaupInvDetails = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtJTaupTlAupAmt = New System.Windows.Forms.TextBox()
        Me.txtJTaupAppvedChgs = New System.Windows.Forms.TextBox()
        Me.txtJTaupAupLessInvdTD = New System.Windows.Forms.TextBox()
        Me.txtJTaupTDQue = New System.Windows.Forms.TextBox()
        Me.txtJTaupQueAmt = New System.Windows.Forms.TextBox()
        Me.txtJTaupInvdTD = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rtxtJTaupJDesc = New System.Windows.Forms.RichTextBox()
        Me.btnJTaupJCancel = New System.Windows.Forms.Button()
        Me.rbtnJTaupChg = New System.Windows.Forms.RadioButton()
        Me.btnJTaupJDelete = New System.Windows.Forms.Button()
        Me.btnJTaupJAddUpdt = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtJTaupApproval = New System.Windows.Forms.TextBox()
        Me.txtJTaupJAmt = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtJTaupRefDoc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbtnJTaupInit = New System.Windows.Forms.RadioButton()
        Me.mtxtJTaupJDate = New System.Windows.Forms.MaskedTextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnJTaupMultiAdd = New System.Windows.Forms.Button()
        Me.rtxtJTaupIDesc = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupFinalInv = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupQueAfterTranDt = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnJTaupICancel = New System.Windows.Forms.Button()
        Me.btnJTaupIDelete = New System.Windows.Forms.Button()
        Me.btnJTaupIAddUpdt = New System.Windows.Forms.Button()
        Me.txtJTaupIAmt = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.mtxtJTaupIDate = New System.Windows.Forms.MaskedTextBox()
        Me.btnJTaupUpdtReturn = New System.Windows.Forms.Button()
        Me.btnJTaupCancel = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtJTaupCategories = New System.Windows.Forms.TextBox()
        Me.gboxJTaupMultiInv = New System.Windows.Forms.GroupBox()
        Me.txtJTaupMItotal = New System.Windows.Forms.TextBox()
        Me.rtxtJTaupMIdesc2 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi2 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd2 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt2 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate2 = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtJTaupMIdesc6 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi6 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd6 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt6 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate6 = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtJTaupMIdesc7 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi7 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd7 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt7 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate7 = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtJTaupMIdesc3 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi3 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd3 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt3 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate3 = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtJTaupMIdesc5 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi5 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd5 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt5 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate5 = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtJTaupMIdesc4 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi4 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd4 = New System.Windows.Forms.CheckBox()
        Me.txtJTaupMIamt4 = New System.Windows.Forms.TextBox()
        Me.mtxtJTaupMIdate4 = New System.Windows.Forms.MaskedTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.rtxtJTaupMIdesc1 = New System.Windows.Forms.RichTextBox()
        Me.cboxJTaupMIfi1 = New System.Windows.Forms.CheckBox()
        Me.cboxJTaupMIqatd1 = New System.Windows.Forms.CheckBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.btnJTaupMICancel = New System.Windows.Forms.Button()
        Me.btnJTaupMIAddUpdt = New System.Windows.Forms.Button()
        Me.txtJTaupMIamt1 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.mtxtJTaupMIdate1 = New System.Windows.Forms.MaskedTextBox()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gboxJTaupMultiInv.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(318, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cust."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(462, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Job"
        '
        'txtJTaupJobID
        '
        Me.txtJTaupJobID.Location = New System.Drawing.Point(357, 4)
        Me.txtJTaupJobID.Name = "txtJTaupJobID"
        Me.txtJTaupJobID.ReadOnly = True
        Me.txtJTaupJobID.Size = New System.Drawing.Size(100, 22)
        Me.txtJTaupJobID.TabIndex = 3
        '
        'txtJTaupJobDesc
        '
        Me.txtJTaupJobDesc.Location = New System.Drawing.Point(629, 6)
        Me.txtJTaupJobDesc.Name = "txtJTaupJobDesc"
        Me.txtJTaupJobDesc.ReadOnly = True
        Me.txtJTaupJobDesc.Size = New System.Drawing.Size(302, 22)
        Me.txtJTaupJobDesc.TabIndex = 4
        '
        'txtJTaupSubID
        '
        Me.txtJTaupSubID.Location = New System.Drawing.Point(499, 6)
        Me.txtJTaupSubID.Name = "txtJTaupSubID"
        Me.txtJTaupSubID.ReadOnly = True
        Me.txtJTaupSubID.Size = New System.Drawing.Size(125, 22)
        Me.txtJTaupSubID.TabIndex = 5
        '
        'lvJTaupJobDetails
        '
        Me.lvJTaupJobDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.IorC, Me.EntDate, Me.Description, Me.CustApproval, Me.Amount, Me.RefDoc})
        Me.lvJTaupJobDetails.FullRowSelect = True
        Me.lvJTaupJobDetails.HideSelection = False
        Me.lvJTaupJobDetails.Location = New System.Drawing.Point(318, 35)
        Me.lvJTaupJobDetails.MultiSelect = False
        Me.lvJTaupJobDetails.Name = "lvJTaupJobDetails"
        Me.lvJTaupJobDetails.Size = New System.Drawing.Size(895, 302)
        Me.lvJTaupJobDetails.TabIndex = 22
        Me.lvJTaupJobDetails.UseCompatibleStateImageBehavior = False
        Me.lvJTaupJobDetails.View = System.Windows.Forms.View.Details
        '
        'IorC
        '
        Me.IorC.Text = "      I/C"
        '
        'EntDate
        '
        Me.EntDate.Text = "Date"
        Me.EntDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.EntDate.Width = 90
        '
        'Description
        '
        Me.Description.Text = "Description"
        Me.Description.Width = 300
        '
        'CustApproval
        '
        Me.CustApproval.Text = "Approved By"
        Me.CustApproval.Width = 120
        '
        'Amount
        '
        Me.Amount.Text = "Amount"
        Me.Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Amount.Width = 100
        '
        'RefDoc
        '
        Me.RefDoc.Text = "Reference Document"
        Me.RefDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.RefDoc.Width = 150
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 17)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Initial AUP Amount"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(483, 7)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(124, 17)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Total AUP Amount"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(241, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 17)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Approved Changes"
        '
        'txtJTaupInitAmt
        '
        Me.txtJTaupInitAmt.Location = New System.Drawing.Point(138, 2)
        Me.txtJTaupInitAmt.Name = "txtJTaupInitAmt"
        Me.txtJTaupInitAmt.ReadOnly = True
        Me.txtJTaupInitAmt.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupInitAmt.TabIndex = 22
        '
        'lvJTaupInvDetails
        '
        Me.lvJTaupInvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader5, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader1})
        Me.lvJTaupInvDetails.FullRowSelect = True
        Me.lvJTaupInvDetails.HideSelection = False
        Me.lvJTaupInvDetails.Location = New System.Drawing.Point(318, 392)
        Me.lvJTaupInvDetails.MultiSelect = False
        Me.lvJTaupInvDetails.Name = "lvJTaupInvDetails"
        Me.lvJTaupInvDetails.Size = New System.Drawing.Size(799, 270)
        Me.lvJTaupInvDetails.TabIndex = 26
        Me.lvJTaupInvDetails.UseCompatibleStateImageBehavior = False
        Me.lvJTaupInvDetails.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Date"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader2.Width = 90
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Description"
        Me.ColumnHeader3.Width = 300
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Amount"
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader5.Width = 115
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "QATD"
        Me.ColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader7.Width = 45
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Final"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader8.Width = 45
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Inv Date"
        Me.ColumnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader9.Width = 90
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "InvNum"
        Me.ColumnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(195, 7)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 17)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "Amt Queued"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(387, 7)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 17)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "Tl Inv+Queue"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(2, 7)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 17)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "Amt Inv'd TD"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(585, 7)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 17)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "AUP-Inv'd"
        '
        'txtJTaupTlAupAmt
        '
        Me.txtJTaupTlAupAmt.Location = New System.Drawing.Point(616, 4)
        Me.txtJTaupTlAupAmt.Name = "txtJTaupTlAupAmt"
        Me.txtJTaupTlAupAmt.ReadOnly = True
        Me.txtJTaupTlAupAmt.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupTlAupAmt.TabIndex = 34
        '
        'txtJTaupAppvedChgs
        '
        Me.txtJTaupAppvedChgs.Location = New System.Drawing.Point(376, 3)
        Me.txtJTaupAppvedChgs.Name = "txtJTaupAppvedChgs"
        Me.txtJTaupAppvedChgs.ReadOnly = True
        Me.txtJTaupAppvedChgs.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupAppvedChgs.TabIndex = 35
        '
        'txtJTaupAupLessInvdTD
        '
        Me.txtJTaupAupLessInvdTD.Location = New System.Drawing.Point(661, 7)
        Me.txtJTaupAupLessInvdTD.Name = "txtJTaupAupLessInvdTD"
        Me.txtJTaupAupLessInvdTD.ReadOnly = True
        Me.txtJTaupAupLessInvdTD.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupAupLessInvdTD.TabIndex = 36
        '
        'txtJTaupTDQue
        '
        Me.txtJTaupTDQue.Location = New System.Drawing.Point(486, 7)
        Me.txtJTaupTDQue.Name = "txtJTaupTDQue"
        Me.txtJTaupTDQue.ReadOnly = True
        Me.txtJTaupTDQue.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupTDQue.TabIndex = 37
        '
        'txtJTaupQueAmt
        '
        Me.txtJTaupQueAmt.Location = New System.Drawing.Point(288, 7)
        Me.txtJTaupQueAmt.Name = "txtJTaupQueAmt"
        Me.txtJTaupQueAmt.ReadOnly = True
        Me.txtJTaupQueAmt.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupQueAmt.TabIndex = 38
        '
        'txtJTaupInvdTD
        '
        Me.txtJTaupInvdTD.Location = New System.Drawing.Point(96, 7)
        Me.txtJTaupInvdTD.Name = "txtJTaupInvdTD"
        Me.txtJTaupInvdTD.ReadOnly = True
        Me.txtJTaupInvdTD.Size = New System.Drawing.Size(93, 22)
        Me.txtJTaupInvdTD.TabIndex = 39
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightBlue
        Me.Panel3.Controls.Add(Me.txtJTaupAppvedChgs)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.txtJTaupInitAmt)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.txtJTaupTlAupAmt)
        Me.Panel3.Location = New System.Drawing.Point(319, 349)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(799, 37)
        Me.Panel3.TabIndex = 40
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.LightBlue
        Me.Panel4.Controls.Add(Me.txtJTaupInvdTD)
        Me.Panel4.Controls.Add(Me.txtJTaupQueAmt)
        Me.Panel4.Controls.Add(Me.txtJTaupTDQue)
        Me.Panel4.Controls.Add(Me.txtJTaupAupLessInvdTD)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.Label13)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Location = New System.Drawing.Point(318, 673)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(798, 40)
        Me.Panel4.TabIndex = 41
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.LightBlue
        Me.GroupBox1.Controls.Add(Me.rtxtJTaupJDesc)
        Me.GroupBox1.Controls.Add(Me.btnJTaupJCancel)
        Me.GroupBox1.Controls.Add(Me.rbtnJTaupChg)
        Me.GroupBox1.Controls.Add(Me.btnJTaupJDelete)
        Me.GroupBox1.Controls.Add(Me.btnJTaupJAddUpdt)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtJTaupApproval)
        Me.GroupBox1.Controls.Add(Me.txtJTaupJAmt)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtJTaupRefDoc)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.rbtnJTaupInit)
        Me.GroupBox1.Controls.Add(Me.mtxtJTaupJDate)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 359)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "AUP JOB/CHANGE DESCRIPTION"
        '
        'rtxtJTaupJDesc
        '
        Me.rtxtJTaupJDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupJDesc.Location = New System.Drawing.Point(12, 60)
        Me.rtxtJTaupJDesc.Name = "rtxtJTaupJDesc"
        Me.rtxtJTaupJDesc.Size = New System.Drawing.Size(259, 137)
        Me.rtxtJTaupJDesc.TabIndex = 47
        Me.rtxtJTaupJDesc.Text = ""
        '
        'btnJTaupJCancel
        '
        Me.btnJTaupJCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupJCancel.Location = New System.Drawing.Point(196, 323)
        Me.btnJTaupJCancel.Name = "btnJTaupJCancel"
        Me.btnJTaupJCancel.Size = New System.Drawing.Size(75, 32)
        Me.btnJTaupJCancel.TabIndex = 33
        Me.btnJTaupJCancel.Text = "Cancel"
        Me.btnJTaupJCancel.UseVisualStyleBackColor = True
        '
        'rbtnJTaupChg
        '
        Me.rbtnJTaupChg.AutoSize = True
        Me.rbtnJTaupChg.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTaupChg.Location = New System.Drawing.Point(199, 18)
        Me.rbtnJTaupChg.Name = "rbtnJTaupChg"
        Me.rbtnJTaupChg.Size = New System.Drawing.Size(78, 21)
        Me.rbtnJTaupChg.TabIndex = 22
        Me.rbtnJTaupChg.TabStop = True
        Me.rbtnJTaupChg.Text = "Change"
        Me.rbtnJTaupChg.UseVisualStyleBackColor = True
        '
        'btnJTaupJDelete
        '
        Me.btnJTaupJDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupJDelete.Location = New System.Drawing.Point(115, 323)
        Me.btnJTaupJDelete.Name = "btnJTaupJDelete"
        Me.btnJTaupJDelete.Size = New System.Drawing.Size(75, 32)
        Me.btnJTaupJDelete.TabIndex = 32
        Me.btnJTaupJDelete.Text = "Delete"
        Me.btnJTaupJDelete.UseVisualStyleBackColor = True
        '
        'btnJTaupJAddUpdt
        '
        Me.btnJTaupJAddUpdt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupJAddUpdt.Location = New System.Drawing.Point(16, 323)
        Me.btnJTaupJAddUpdt.Name = "btnJTaupJAddUpdt"
        Me.btnJTaupJAddUpdt.Size = New System.Drawing.Size(95, 32)
        Me.btnJTaupJAddUpdt.TabIndex = 31
        Me.btnJTaupJAddUpdt.Text = "Add/Update"
        Me.btnJTaupJAddUpdt.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(18, 271)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 17)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Approved By:"
        '
        'txtJTaupApproval
        '
        Me.txtJTaupApproval.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupApproval.Location = New System.Drawing.Point(15, 291)
        Me.txtJTaupApproval.Name = "txtJTaupApproval"
        Me.txtJTaupApproval.Size = New System.Drawing.Size(257, 24)
        Me.txtJTaupApproval.TabIndex = 29
        '
        'txtJTaupJAmt
        '
        Me.txtJTaupJAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupJAmt.Location = New System.Drawing.Point(164, 253)
        Me.txtJTaupJAmt.Name = "txtJTaupJAmt"
        Me.txtJTaupJAmt.Size = New System.Drawing.Size(109, 24)
        Me.txtJTaupJAmt.TabIndex = 28
        Me.txtJTaupJAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(19, 253)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 17)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Amount"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 205)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 17)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Reference Document"
        '
        'txtJTaupRefDoc
        '
        Me.txtJTaupRefDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupRefDoc.Location = New System.Drawing.Point(16, 224)
        Me.txtJTaupRefDoc.Name = "txtJTaupRefDoc"
        Me.txtJTaupRefDoc.Size = New System.Drawing.Size(257, 24)
        Me.txtJTaupRefDoc.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(176, 17)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Job or Change Description"
        '
        'rbtnJTaupInit
        '
        Me.rbtnJTaupInit.AutoSize = True
        Me.rbtnJTaupInit.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTaupInit.Location = New System.Drawing.Point(107, 18)
        Me.rbtnJTaupInit.Name = "rbtnJTaupInit"
        Me.rbtnJTaupInit.Size = New System.Drawing.Size(85, 21)
        Me.rbtnJTaupInit.TabIndex = 21
        Me.rbtnJTaupInit.TabStop = True
        Me.rbtnJTaupInit.Text = "Intial Job"
        Me.rbtnJTaupInit.UseVisualStyleBackColor = True
        '
        'mtxtJTaupJDate
        '
        Me.mtxtJTaupJDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupJDate.Location = New System.Drawing.Point(12, 16)
        Me.mtxtJTaupJDate.Mask = "00/00/0000"
        Me.mtxtJTaupJDate.Name = "mtxtJTaupJDate"
        Me.mtxtJTaupJDate.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupJDate.TabIndex = 20
        Me.mtxtJTaupJDate.ValidatingType = GetType(Date)
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.LightBlue
        Me.GroupBox2.Controls.Add(Me.btnJTaupMultiAdd)
        Me.GroupBox2.Controls.Add(Me.rtxtJTaupIDesc)
        Me.GroupBox2.Controls.Add(Me.cboxJTaupFinalInv)
        Me.GroupBox2.Controls.Add(Me.cboxJTaupQueAfterTranDt)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.btnJTaupICancel)
        Me.GroupBox2.Controls.Add(Me.btnJTaupIDelete)
        Me.GroupBox2.Controls.Add(Me.btnJTaupIAddUpdt)
        Me.GroupBox2.Controls.Add(Me.txtJTaupIAmt)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.mtxtJTaupIDate)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(15, 399)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(292, 314)
        Me.GroupBox2.TabIndex = 43
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "AUP INVOICING"
        '
        'btnJTaupMultiAdd
        '
        Me.btnJTaupMultiAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupMultiAdd.Location = New System.Drawing.Point(3, 228)
        Me.btnJTaupMultiAdd.Name = "btnJTaupMultiAdd"
        Me.btnJTaupMultiAdd.Size = New System.Drawing.Size(44, 69)
        Me.btnJTaupMultiAdd.TabIndex = 49
        Me.btnJTaupMultiAdd.Text = "Mult" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Add" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Opt."
        Me.btnJTaupMultiAdd.UseVisualStyleBackColor = True
        '
        'rtxtJTaupIDesc
        '
        Me.rtxtJTaupIDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupIDesc.Location = New System.Drawing.Point(13, 55)
        Me.rtxtJTaupIDesc.Name = "rtxtJTaupIDesc"
        Me.rtxtJTaupIDesc.Size = New System.Drawing.Size(259, 142)
        Me.rtxtJTaupIDesc.TabIndex = 48
        Me.rtxtJTaupIDesc.Text = ""
        '
        'cboxJTaupFinalInv
        '
        Me.cboxJTaupFinalInv.AutoSize = True
        Me.cboxJTaupFinalInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupFinalInv.Location = New System.Drawing.Point(194, 234)
        Me.cboxJTaupFinalInv.Name = "cboxJTaupFinalInv"
        Me.cboxJTaupFinalInv.Size = New System.Drawing.Size(86, 21)
        Me.cboxJTaupFinalInv.TabIndex = 33
        Me.cboxJTaupFinalInv.Text = "Final Inv."
        Me.cboxJTaupFinalInv.UseVisualStyleBackColor = True
        '
        'cboxJTaupQueAfterTranDt
        '
        Me.cboxJTaupQueAfterTranDt.AutoSize = True
        Me.cboxJTaupQueAfterTranDt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupQueAfterTranDt.Location = New System.Drawing.Point(52, 234)
        Me.cboxJTaupQueAfterTranDt.Name = "cboxJTaupQueAfterTranDt"
        Me.cboxJTaupQueAfterTranDt.Size = New System.Drawing.Size(138, 21)
        Me.cboxJTaupQueAfterTranDt.TabIndex = 32
        Me.cboxJTaupQueAfterTranDt.Text = "Inv. After Tran Dt"
        Me.cboxJTaupQueAfterTranDt.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(11, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(165, 17)
        Me.Label9.TabIndex = 31
        Me.Label9.Text = "Invoice Transaction Date"
        '
        'btnJTaupICancel
        '
        Me.btnJTaupICancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupICancel.Location = New System.Drawing.Point(201, 264)
        Me.btnJTaupICancel.Name = "btnJTaupICancel"
        Me.btnJTaupICancel.Size = New System.Drawing.Size(67, 32)
        Me.btnJTaupICancel.TabIndex = 30
        Me.btnJTaupICancel.Text = "Cancel"
        Me.btnJTaupICancel.UseVisualStyleBackColor = True
        '
        'btnJTaupIDelete
        '
        Me.btnJTaupIDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupIDelete.Location = New System.Drawing.Point(135, 264)
        Me.btnJTaupIDelete.Name = "btnJTaupIDelete"
        Me.btnJTaupIDelete.Size = New System.Drawing.Size(61, 32)
        Me.btnJTaupIDelete.TabIndex = 29
        Me.btnJTaupIDelete.Text = "Delete"
        Me.btnJTaupIDelete.UseVisualStyleBackColor = True
        '
        'btnJTaupIAddUpdt
        '
        Me.btnJTaupIAddUpdt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupIAddUpdt.Location = New System.Drawing.Point(49, 264)
        Me.btnJTaupIAddUpdt.Name = "btnJTaupIAddUpdt"
        Me.btnJTaupIAddUpdt.Size = New System.Drawing.Size(81, 32)
        Me.btnJTaupIAddUpdt.TabIndex = 28
        Me.btnJTaupIAddUpdt.Text = "Add/Updt"
        Me.btnJTaupIAddUpdt.UseVisualStyleBackColor = True
        '
        'txtJTaupIAmt
        '
        Me.txtJTaupIAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupIAmt.Location = New System.Drawing.Point(168, 205)
        Me.txtJTaupIAmt.Name = "txtJTaupIAmt"
        Me.txtJTaupIAmt.Size = New System.Drawing.Size(104, 24)
        Me.txtJTaupIAmt.TabIndex = 27
        Me.txtJTaupIAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(34, 208)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 17)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "Amt to be Invoiced"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 38)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(127, 17)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Invoice Description"
        '
        'mtxtJTaupIDate
        '
        Me.mtxtJTaupIDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupIDate.Location = New System.Drawing.Point(178, 17)
        Me.mtxtJTaupIDate.Mask = "00/00/0000"
        Me.mtxtJTaupIDate.Name = "mtxtJTaupIDate"
        Me.mtxtJTaupIDate.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupIDate.TabIndex = 23
        Me.mtxtJTaupIDate.ValidatingType = GetType(Date)
        '
        'btnJTaupUpdtReturn
        '
        Me.btnJTaupUpdtReturn.BackColor = System.Drawing.Color.LightBlue
        Me.btnJTaupUpdtReturn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupUpdtReturn.Location = New System.Drawing.Point(1123, 349)
        Me.btnJTaupUpdtReturn.Name = "btnJTaupUpdtReturn"
        Me.btnJTaupUpdtReturn.Size = New System.Drawing.Size(90, 183)
        Me.btnJTaupUpdtReturn.TabIndex = 44
        Me.btnJTaupUpdtReturn.Text = "Update/ Return"
        Me.btnJTaupUpdtReturn.UseVisualStyleBackColor = False
        '
        'btnJTaupCancel
        '
        Me.btnJTaupCancel.BackColor = System.Drawing.Color.LightBlue
        Me.btnJTaupCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupCancel.Location = New System.Drawing.Point(1123, 538)
        Me.btnJTaupCancel.Name = "btnJTaupCancel"
        Me.btnJTaupCancel.Size = New System.Drawing.Size(90, 175)
        Me.btnJTaupCancel.TabIndex = 45
        Me.btnJTaupCancel.Text = "Cancel"
        Me.btnJTaupCancel.UseVisualStyleBackColor = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(14, 4)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(303, 24)
        Me.Label17.TabIndex = 46
        Me.Label17.Text = "AUP DESCRIPTION/INVOICING"
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'txtJTaupCategories
        '
        Me.txtJTaupCategories.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupCategories.ForeColor = System.Drawing.SystemColors.Info
        Me.txtJTaupCategories.Location = New System.Drawing.Point(938, 6)
        Me.txtJTaupCategories.Name = "txtJTaupCategories"
        Me.txtJTaupCategories.ReadOnly = True
        Me.txtJTaupCategories.Size = New System.Drawing.Size(273, 22)
        Me.txtJTaupCategories.TabIndex = 47
        Me.txtJTaupCategories.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gboxJTaupMultiInv
        '
        Me.gboxJTaupMultiInv.BackColor = System.Drawing.Color.LightBlue
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMItotal)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc2)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi2)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd2)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt2)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate2)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc6)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi6)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd6)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt6)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate6)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc7)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi7)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd7)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt7)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate7)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc3)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi3)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd3)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt3)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate3)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc5)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi5)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd5)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt5)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate5)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc4)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi4)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd4)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt4)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate4)
        Me.gboxJTaupMultiInv.Controls.Add(Me.Label22)
        Me.gboxJTaupMultiInv.Controls.Add(Me.Label21)
        Me.gboxJTaupMultiInv.Controls.Add(Me.rtxtJTaupMIdesc1)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIfi1)
        Me.gboxJTaupMultiInv.Controls.Add(Me.cboxJTaupMIqatd1)
        Me.gboxJTaupMultiInv.Controls.Add(Me.Label18)
        Me.gboxJTaupMultiInv.Controls.Add(Me.btnJTaupMICancel)
        Me.gboxJTaupMultiInv.Controls.Add(Me.btnJTaupMIAddUpdt)
        Me.gboxJTaupMultiInv.Controls.Add(Me.txtJTaupMIamt1)
        Me.gboxJTaupMultiInv.Controls.Add(Me.Label19)
        Me.gboxJTaupMultiInv.Controls.Add(Me.Label20)
        Me.gboxJTaupMultiInv.Controls.Add(Me.mtxtJTaupMIdate1)
        Me.gboxJTaupMultiInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTaupMultiInv.Location = New System.Drawing.Point(333, 166)
        Me.gboxJTaupMultiInv.Name = "gboxJTaupMultiInv"
        Me.gboxJTaupMultiInv.Size = New System.Drawing.Size(838, 366)
        Me.gboxJTaupMultiInv.TabIndex = 49
        Me.gboxJTaupMultiInv.TabStop = False
        Me.gboxJTaupMultiInv.Text = "AUP MULTIPLE  INVOICE ENTRY"
        Me.gboxJTaupMultiInv.Visible = False
        '
        'txtJTaupMItotal
        '
        Me.txtJTaupMItotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMItotal.Location = New System.Drawing.Point(599, 303)
        Me.txtJTaupMItotal.Name = "txtJTaupMItotal"
        Me.txtJTaupMItotal.ReadOnly = True
        Me.txtJTaupMItotal.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMItotal.TabIndex = 81
        Me.txtJTaupMItotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'rtxtJTaupMIdesc2
        '
        Me.rtxtJTaupMIdesc2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc2.Location = New System.Drawing.Point(114, 101)
        Me.rtxtJTaupMIdesc2.Multiline = False
        Me.rtxtJTaupMIdesc2.Name = "rtxtJTaupMIdesc2"
        Me.rtxtJTaupMIdesc2.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc2.TabIndex = 3
        Me.rtxtJTaupMIdesc2.Text = ""
        '
        'cboxJTaupMIfi2
        '
        Me.cboxJTaupMIfi2.AutoSize = True
        Me.cboxJTaupMIfi2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi2.Location = New System.Drawing.Point(788, 107)
        Me.cboxJTaupMIfi2.Name = "cboxJTaupMIfi2"
        Me.cboxJTaupMIfi2.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi2.TabIndex = 79
        Me.cboxJTaupMIfi2.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd2
        '
        Me.cboxJTaupMIqatd2.AutoSize = True
        Me.cboxJTaupMIqatd2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd2.Location = New System.Drawing.Point(736, 107)
        Me.cboxJTaupMIqatd2.Name = "cboxJTaupMIqatd2"
        Me.cboxJTaupMIqatd2.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd2.TabIndex = 78
        Me.cboxJTaupMIqatd2.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt2
        '
        Me.txtJTaupMIamt2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt2.Location = New System.Drawing.Point(599, 101)
        Me.txtJTaupMIamt2.Name = "txtJTaupMIamt2"
        Me.txtJTaupMIamt2.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt2.TabIndex = 4
        Me.txtJTaupMIamt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate2
        '
        Me.mtxtJTaupMIdate2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate2.Location = New System.Drawing.Point(14, 101)
        Me.mtxtJTaupMIdate2.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate2.Name = "mtxtJTaupMIdate2"
        Me.mtxtJTaupMIdate2.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate2.TabIndex = 76
        Me.mtxtJTaupMIdate2.ValidatingType = GetType(Date)
        '
        'rtxtJTaupMIdesc6
        '
        Me.rtxtJTaupMIdesc6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc6.Location = New System.Drawing.Point(115, 229)
        Me.rtxtJTaupMIdesc6.Multiline = False
        Me.rtxtJTaupMIdesc6.Name = "rtxtJTaupMIdesc6"
        Me.rtxtJTaupMIdesc6.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc6.TabIndex = 15
        Me.rtxtJTaupMIdesc6.Text = ""
        '
        'cboxJTaupMIfi6
        '
        Me.cboxJTaupMIfi6.AutoSize = True
        Me.cboxJTaupMIfi6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi6.Location = New System.Drawing.Point(789, 235)
        Me.cboxJTaupMIfi6.Name = "cboxJTaupMIfi6"
        Me.cboxJTaupMIfi6.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi6.TabIndex = 74
        Me.cboxJTaupMIfi6.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd6
        '
        Me.cboxJTaupMIqatd6.AutoSize = True
        Me.cboxJTaupMIqatd6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd6.Location = New System.Drawing.Point(737, 235)
        Me.cboxJTaupMIqatd6.Name = "cboxJTaupMIqatd6"
        Me.cboxJTaupMIqatd6.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd6.TabIndex = 73
        Me.cboxJTaupMIqatd6.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt6
        '
        Me.txtJTaupMIamt6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt6.Location = New System.Drawing.Point(600, 229)
        Me.txtJTaupMIamt6.Name = "txtJTaupMIamt6"
        Me.txtJTaupMIamt6.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt6.TabIndex = 16
        Me.txtJTaupMIamt6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate6
        '
        Me.mtxtJTaupMIdate6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate6.Location = New System.Drawing.Point(15, 229)
        Me.mtxtJTaupMIdate6.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate6.Name = "mtxtJTaupMIdate6"
        Me.mtxtJTaupMIdate6.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate6.TabIndex = 71
        Me.mtxtJTaupMIdate6.ValidatingType = GetType(Date)
        '
        'rtxtJTaupMIdesc7
        '
        Me.rtxtJTaupMIdesc7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc7.Location = New System.Drawing.Point(114, 261)
        Me.rtxtJTaupMIdesc7.Multiline = False
        Me.rtxtJTaupMIdesc7.Name = "rtxtJTaupMIdesc7"
        Me.rtxtJTaupMIdesc7.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc7.TabIndex = 18
        Me.rtxtJTaupMIdesc7.Text = ""
        '
        'cboxJTaupMIfi7
        '
        Me.cboxJTaupMIfi7.AutoSize = True
        Me.cboxJTaupMIfi7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi7.Location = New System.Drawing.Point(788, 267)
        Me.cboxJTaupMIfi7.Name = "cboxJTaupMIfi7"
        Me.cboxJTaupMIfi7.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi7.TabIndex = 69
        Me.cboxJTaupMIfi7.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd7
        '
        Me.cboxJTaupMIqatd7.AutoSize = True
        Me.cboxJTaupMIqatd7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd7.Location = New System.Drawing.Point(736, 267)
        Me.cboxJTaupMIqatd7.Name = "cboxJTaupMIqatd7"
        Me.cboxJTaupMIqatd7.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd7.TabIndex = 68
        Me.cboxJTaupMIqatd7.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt7
        '
        Me.txtJTaupMIamt7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt7.Location = New System.Drawing.Point(599, 261)
        Me.txtJTaupMIamt7.Name = "txtJTaupMIamt7"
        Me.txtJTaupMIamt7.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt7.TabIndex = 19
        Me.txtJTaupMIamt7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate7
        '
        Me.mtxtJTaupMIdate7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate7.Location = New System.Drawing.Point(14, 261)
        Me.mtxtJTaupMIdate7.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate7.Name = "mtxtJTaupMIdate7"
        Me.mtxtJTaupMIdate7.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate7.TabIndex = 66
        Me.mtxtJTaupMIdate7.ValidatingType = GetType(Date)
        '
        'rtxtJTaupMIdesc3
        '
        Me.rtxtJTaupMIdesc3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc3.Location = New System.Drawing.Point(114, 132)
        Me.rtxtJTaupMIdesc3.Multiline = False
        Me.rtxtJTaupMIdesc3.Name = "rtxtJTaupMIdesc3"
        Me.rtxtJTaupMIdesc3.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc3.TabIndex = 6
        Me.rtxtJTaupMIdesc3.Text = ""
        '
        'cboxJTaupMIfi3
        '
        Me.cboxJTaupMIfi3.AutoSize = True
        Me.cboxJTaupMIfi3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi3.Location = New System.Drawing.Point(788, 138)
        Me.cboxJTaupMIfi3.Name = "cboxJTaupMIfi3"
        Me.cboxJTaupMIfi3.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi3.TabIndex = 64
        Me.cboxJTaupMIfi3.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd3
        '
        Me.cboxJTaupMIqatd3.AutoSize = True
        Me.cboxJTaupMIqatd3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd3.Location = New System.Drawing.Point(736, 138)
        Me.cboxJTaupMIqatd3.Name = "cboxJTaupMIqatd3"
        Me.cboxJTaupMIqatd3.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd3.TabIndex = 63
        Me.cboxJTaupMIqatd3.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt3
        '
        Me.txtJTaupMIamt3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt3.Location = New System.Drawing.Point(599, 132)
        Me.txtJTaupMIamt3.Name = "txtJTaupMIamt3"
        Me.txtJTaupMIamt3.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt3.TabIndex = 7
        Me.txtJTaupMIamt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate3
        '
        Me.mtxtJTaupMIdate3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate3.Location = New System.Drawing.Point(14, 132)
        Me.mtxtJTaupMIdate3.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate3.Name = "mtxtJTaupMIdate3"
        Me.mtxtJTaupMIdate3.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate3.TabIndex = 61
        Me.mtxtJTaupMIdate3.ValidatingType = GetType(Date)
        '
        'rtxtJTaupMIdesc5
        '
        Me.rtxtJTaupMIdesc5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc5.Location = New System.Drawing.Point(115, 198)
        Me.rtxtJTaupMIdesc5.Multiline = False
        Me.rtxtJTaupMIdesc5.Name = "rtxtJTaupMIdesc5"
        Me.rtxtJTaupMIdesc5.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc5.TabIndex = 12
        Me.rtxtJTaupMIdesc5.Text = ""
        '
        'cboxJTaupMIfi5
        '
        Me.cboxJTaupMIfi5.AutoSize = True
        Me.cboxJTaupMIfi5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi5.Location = New System.Drawing.Point(789, 204)
        Me.cboxJTaupMIfi5.Name = "cboxJTaupMIfi5"
        Me.cboxJTaupMIfi5.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi5.TabIndex = 59
        Me.cboxJTaupMIfi5.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd5
        '
        Me.cboxJTaupMIqatd5.AutoSize = True
        Me.cboxJTaupMIqatd5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd5.Location = New System.Drawing.Point(737, 204)
        Me.cboxJTaupMIqatd5.Name = "cboxJTaupMIqatd5"
        Me.cboxJTaupMIqatd5.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd5.TabIndex = 58
        Me.cboxJTaupMIqatd5.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt5
        '
        Me.txtJTaupMIamt5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt5.Location = New System.Drawing.Point(600, 198)
        Me.txtJTaupMIamt5.Name = "txtJTaupMIamt5"
        Me.txtJTaupMIamt5.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt5.TabIndex = 13
        Me.txtJTaupMIamt5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate5
        '
        Me.mtxtJTaupMIdate5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate5.Location = New System.Drawing.Point(15, 198)
        Me.mtxtJTaupMIdate5.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate5.Name = "mtxtJTaupMIdate5"
        Me.mtxtJTaupMIdate5.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate5.TabIndex = 56
        Me.mtxtJTaupMIdate5.ValidatingType = GetType(Date)
        '
        'rtxtJTaupMIdesc4
        '
        Me.rtxtJTaupMIdesc4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc4.Location = New System.Drawing.Point(114, 165)
        Me.rtxtJTaupMIdesc4.Multiline = False
        Me.rtxtJTaupMIdesc4.Name = "rtxtJTaupMIdesc4"
        Me.rtxtJTaupMIdesc4.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc4.TabIndex = 9
        Me.rtxtJTaupMIdesc4.Text = ""
        '
        'cboxJTaupMIfi4
        '
        Me.cboxJTaupMIfi4.AutoSize = True
        Me.cboxJTaupMIfi4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi4.Location = New System.Drawing.Point(788, 171)
        Me.cboxJTaupMIfi4.Name = "cboxJTaupMIfi4"
        Me.cboxJTaupMIfi4.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi4.TabIndex = 54
        Me.cboxJTaupMIfi4.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd4
        '
        Me.cboxJTaupMIqatd4.AutoSize = True
        Me.cboxJTaupMIqatd4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd4.Location = New System.Drawing.Point(736, 171)
        Me.cboxJTaupMIqatd4.Name = "cboxJTaupMIqatd4"
        Me.cboxJTaupMIqatd4.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd4.TabIndex = 53
        Me.cboxJTaupMIqatd4.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt4
        '
        Me.txtJTaupMIamt4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt4.Location = New System.Drawing.Point(599, 165)
        Me.txtJTaupMIamt4.Name = "txtJTaupMIamt4"
        Me.txtJTaupMIamt4.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt4.TabIndex = 10
        Me.txtJTaupMIamt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mtxtJTaupMIdate4
        '
        Me.mtxtJTaupMIdate4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate4.Location = New System.Drawing.Point(14, 165)
        Me.mtxtJTaupMIdate4.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate4.Name = "mtxtJTaupMIdate4"
        Me.mtxtJTaupMIdate4.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate4.TabIndex = 51
        Me.mtxtJTaupMIdate4.ValidatingType = GetType(Date)
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(779, 30)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(52, 34)
        Me.Label22.TabIndex = 50
        Me.Label22.Text = "  Final" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoice"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(713, 14)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(64, 51)
        Me.Label21.TabIndex = 49
        Me.Label21.Text = " Queue" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "   after" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Tran Dte"
        '
        'rtxtJTaupMIdesc1
        '
        Me.rtxtJTaupMIdesc1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtxtJTaupMIdesc1.Location = New System.Drawing.Point(114, 71)
        Me.rtxtJTaupMIdesc1.Multiline = False
        Me.rtxtJTaupMIdesc1.Name = "rtxtJTaupMIdesc1"
        Me.rtxtJTaupMIdesc1.Size = New System.Drawing.Size(470, 24)
        Me.rtxtJTaupMIdesc1.TabIndex = 0
        Me.rtxtJTaupMIdesc1.Text = ""
        '
        'cboxJTaupMIfi1
        '
        Me.cboxJTaupMIfi1.AutoSize = True
        Me.cboxJTaupMIfi1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIfi1.Location = New System.Drawing.Point(788, 77)
        Me.cboxJTaupMIfi1.Name = "cboxJTaupMIfi1"
        Me.cboxJTaupMIfi1.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIfi1.TabIndex = 33
        Me.cboxJTaupMIfi1.UseVisualStyleBackColor = True
        '
        'cboxJTaupMIqatd1
        '
        Me.cboxJTaupMIqatd1.AutoSize = True
        Me.cboxJTaupMIqatd1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTaupMIqatd1.Location = New System.Drawing.Point(736, 77)
        Me.cboxJTaupMIqatd1.Name = "cboxJTaupMIqatd1"
        Me.cboxJTaupMIqatd1.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTaupMIqatd1.TabIndex = 32
        Me.cboxJTaupMIqatd1.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(19, 30)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(83, 34)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Transaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "     Date"
        '
        'btnJTaupMICancel
        '
        Me.btnJTaupMICancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupMICancel.Location = New System.Drawing.Point(118, 300)
        Me.btnJTaupMICancel.Name = "btnJTaupMICancel"
        Me.btnJTaupMICancel.Size = New System.Drawing.Size(75, 32)
        Me.btnJTaupMICancel.TabIndex = 30
        Me.btnJTaupMICancel.Text = "Cancel"
        Me.btnJTaupMICancel.UseVisualStyleBackColor = True
        '
        'btnJTaupMIAddUpdt
        '
        Me.btnJTaupMIAddUpdt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTaupMIAddUpdt.Location = New System.Drawing.Point(15, 300)
        Me.btnJTaupMIAddUpdt.Name = "btnJTaupMIAddUpdt"
        Me.btnJTaupMIAddUpdt.Size = New System.Drawing.Size(95, 32)
        Me.btnJTaupMIAddUpdt.TabIndex = 20
        Me.btnJTaupMIAddUpdt.Text = "Add/Update"
        Me.btnJTaupMIAddUpdt.UseVisualStyleBackColor = True
        '
        'txtJTaupMIamt1
        '
        Me.txtJTaupMIamt1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTaupMIamt1.Location = New System.Drawing.Point(599, 71)
        Me.txtJTaupMIamt1.Name = "txtJTaupMIamt1"
        Me.txtJTaupMIamt1.Size = New System.Drawing.Size(115, 24)
        Me.txtJTaupMIamt1.TabIndex = 1
        Me.txtJTaupMIamt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(630, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 34)
        Me.Label19.TabIndex = 26
        Me.Label19.Text = "Invoice" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Amount"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(205, 42)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(285, 17)
        Me.Label20.TabIndex = 25
        Me.Label20.Text = "Invoice Description(to be printed on invoice)"
        '
        'mtxtJTaupMIdate1
        '
        Me.mtxtJTaupMIdate1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTaupMIdate1.Location = New System.Drawing.Point(14, 71)
        Me.mtxtJTaupMIdate1.Mask = "00/00/0000"
        Me.mtxtJTaupMIdate1.Name = "mtxtJTaupMIdate1"
        Me.mtxtJTaupMIdate1.Size = New System.Drawing.Size(91, 24)
        Me.mtxtJTaupMIdate1.TabIndex = 23
        Me.mtxtJTaupMIdate1.ValidatingType = GetType(Date)
        '
        'JTaup
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1225, 722)
        Me.ControlBox = False
        Me.Controls.Add(Me.gboxJTaupMultiInv)
        Me.Controls.Add(Me.txtJTaupCategories)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.btnJTaupCancel)
        Me.Controls.Add(Me.btnJTaupUpdtReturn)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.lvJTaupInvDetails)
        Me.Controls.Add(Me.lvJTaupJobDetails)
        Me.Controls.Add(Me.txtJTaupSubID)
        Me.Controls.Add(Me.txtJTaupJobDesc)
        Me.Controls.Add(Me.txtJTaupJobID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Name = "JTaup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "J.I.M. - Agreed Upon Pricing"
        Me.TopMost = True
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gboxJTaupMultiInv.ResumeLayout(False)
        Me.gboxJTaupMultiInv.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtJTaupJobID As TextBox
    Friend WithEvents txtJTaupJobDesc As TextBox
    Friend WithEvents txtJTaupSubID As TextBox
    Friend WithEvents lvJTaupJobDetails As ListView
    Friend WithEvents IorC As ColumnHeader
    Friend WithEvents EntDate As ColumnHeader
    Friend WithEvents Description As ColumnHeader
    Friend WithEvents CustApproval As ColumnHeader
    Friend WithEvents Amount As ColumnHeader
    Friend WithEvents RefDoc As ColumnHeader
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtJTaupInitAmt As TextBox
    Friend WithEvents lvJTaupInvDetails As ListView
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtJTaupTlAupAmt As TextBox
    Friend WithEvents txtJTaupAppvedChgs As TextBox
    Friend WithEvents txtJTaupAupLessInvdTD As TextBox
    Friend WithEvents txtJTaupTDQue As TextBox
    Friend WithEvents txtJTaupQueAmt As TextBox
    Friend WithEvents txtJTaupInvdTD As TextBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnJTaupJCancel As Button
    Friend WithEvents rbtnJTaupChg As RadioButton
    Friend WithEvents btnJTaupJDelete As Button
    Friend WithEvents btnJTaupJAddUpdt As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents txtJTaupApproval As TextBox
    Friend WithEvents txtJTaupJAmt As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtJTaupRefDoc As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents rbtnJTaupInit As RadioButton
    Friend WithEvents mtxtJTaupJDate As MaskedTextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cboxJTaupFinalInv As CheckBox
    Friend WithEvents cboxJTaupQueAfterTranDt As CheckBox
    Friend WithEvents Label9 As Label
    Friend WithEvents btnJTaupICancel As Button
    Friend WithEvents btnJTaupIDelete As Button
    Friend WithEvents btnJTaupIAddUpdt As Button
    Friend WithEvents txtJTaupIAmt As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents mtxtJTaupIDate As MaskedTextBox
    Friend WithEvents btnJTaupUpdtReturn As Button
    Friend WithEvents btnJTaupCancel As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents rtxtJTaupJDesc As RichTextBox
    Friend WithEvents rtxtJTaupIDesc As RichTextBox
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtJTaupCategories As TextBox
    Friend WithEvents btnJTaupMultiAdd As Button
    Friend WithEvents gboxJTaupMultiInv As GroupBox
    Friend WithEvents txtJTaupMItotal As TextBox
    Friend WithEvents rtxtJTaupMIdesc2 As RichTextBox
    Friend WithEvents cboxJTaupMIfi2 As CheckBox
    Friend WithEvents cboxJTaupMIqatd2 As CheckBox
    Friend WithEvents txtJTaupMIamt2 As TextBox
    Friend WithEvents mtxtJTaupMIdate2 As MaskedTextBox
    Friend WithEvents rtxtJTaupMIdesc6 As RichTextBox
    Friend WithEvents cboxJTaupMIfi6 As CheckBox
    Friend WithEvents cboxJTaupMIqatd6 As CheckBox
    Friend WithEvents txtJTaupMIamt6 As TextBox
    Friend WithEvents mtxtJTaupMIdate6 As MaskedTextBox
    Friend WithEvents rtxtJTaupMIdesc7 As RichTextBox
    Friend WithEvents cboxJTaupMIfi7 As CheckBox
    Friend WithEvents cboxJTaupMIqatd7 As CheckBox
    Friend WithEvents txtJTaupMIamt7 As TextBox
    Friend WithEvents mtxtJTaupMIdate7 As MaskedTextBox
    Friend WithEvents rtxtJTaupMIdesc3 As RichTextBox
    Friend WithEvents cboxJTaupMIfi3 As CheckBox
    Friend WithEvents cboxJTaupMIqatd3 As CheckBox
    Friend WithEvents txtJTaupMIamt3 As TextBox
    Friend WithEvents mtxtJTaupMIdate3 As MaskedTextBox
    Friend WithEvents rtxtJTaupMIdesc5 As RichTextBox
    Friend WithEvents cboxJTaupMIfi5 As CheckBox
    Friend WithEvents cboxJTaupMIqatd5 As CheckBox
    Friend WithEvents txtJTaupMIamt5 As TextBox
    Friend WithEvents mtxtJTaupMIdate5 As MaskedTextBox
    Friend WithEvents rtxtJTaupMIdesc4 As RichTextBox
    Friend WithEvents cboxJTaupMIfi4 As CheckBox
    Friend WithEvents cboxJTaupMIqatd4 As CheckBox
    Friend WithEvents txtJTaupMIamt4 As TextBox
    Friend WithEvents mtxtJTaupMIdate4 As MaskedTextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents rtxtJTaupMIdesc1 As RichTextBox
    Friend WithEvents cboxJTaupMIfi1 As CheckBox
    Friend WithEvents cboxJTaupMIqatd1 As CheckBox
    Friend WithEvents Label18 As Label
    Friend WithEvents btnJTaupMICancel As Button
    Friend WithEvents btnJTaupMIAddUpdt As Button
    Friend WithEvents txtJTaupMIamt1 As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents mtxtJTaupMIdate1 As MaskedTextBox
End Class
