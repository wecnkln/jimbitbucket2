﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTstart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTstart))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtUserID = New System.Windows.Forms.TextBox()
        Me.btnSignIn = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTInitStatus = New System.Windows.Forms.TextBox()
        Me.chkboxJTStartForgotPW = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.NonLaborEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HumanResourceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WorkerInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaborCategoryMaintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayrollTransmittalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JobUtilitiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JobReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NameChangeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JobDeactivationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceExtrasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceVoidMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceLookupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountingInterfaceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QBReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EDITestingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UtilitiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GlobalSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.JobPurgeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 48.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 34)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(993, 91)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Job Information Manager"
        Me.Label1.UseWaitCursor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(3, 448)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(1031, 52)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'txtUserID
        '
        Me.txtUserID.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserID.Location = New System.Drawing.Point(417, 190)
        Me.txtUserID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(161, 27)
        Me.txtUserID.TabIndex = 3
        Me.txtUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtUserID.UseWaitCursor = True
        '
        'btnSignIn
        '
        Me.btnSignIn.BackColor = System.Drawing.SystemColors.Control
        Me.btnSignIn.Location = New System.Drawing.Point(449, 271)
        Me.btnSignIn.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSignIn.Name = "btnSignIn"
        Me.btnSignIn.Size = New System.Drawing.Size(100, 28)
        Me.btnSignIn.TabIndex = 4
        Me.btnSignIn.Text = "Sign In"
        Me.btnSignIn.UseVisualStyleBackColor = False
        Me.btnSignIn.UseWaitCursor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(449, 320)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(100, 28)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        Me.btnExit.UseWaitCursor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(445, 230)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Enter User ID"
        Me.Label2.UseWaitCursor = True
        '
        'txtJTInitStatus
        '
        Me.txtJTInitStatus.Location = New System.Drawing.Point(65, 144)
        Me.txtJTInitStatus.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTInitStatus.Multiline = True
        Me.txtJTInitStatus.Name = "txtJTInitStatus"
        Me.txtJTInitStatus.ReadOnly = True
        Me.txtJTInitStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtJTInitStatus.Size = New System.Drawing.Size(220, 203)
        Me.txtJTInitStatus.TabIndex = 7
        Me.txtJTInitStatus.UseWaitCursor = True
        Me.txtJTInitStatus.Visible = False
        '
        'chkboxJTStartForgotPW
        '
        Me.chkboxJTStartForgotPW.AutoSize = True
        Me.chkboxJTStartForgotPW.Location = New System.Drawing.Point(408, 382)
        Me.chkboxJTStartForgotPW.Name = "chkboxJTStartForgotPW"
        Me.chkboxJTStartForgotPW.Size = New System.Drawing.Size(190, 20)
        Me.chkboxJTStartForgotPW.TabIndex = 8
        Me.chkboxJTStartForgotPW.Text = "Don't remember password."
        Me.chkboxJTStartForgotPW.UseVisualStyleBackColor = True
        Me.chkboxJTStartForgotPW.UseWaitCursor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NonLaborEntryToolStripMenuItem, Me.HumanResourceToolStripMenuItem, Me.JobUtilitiesToolStripMenuItem, Me.InvoiceExtrasToolStripMenuItem, Me.AccountingInterfaceToolStripMenuItem, Me.UtilitiesToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1049, 28)
        Me.MenuStrip1.TabIndex = 10
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'NonLaborEntryToolStripMenuItem
        '
        Me.NonLaborEntryToolStripMenuItem.Name = "NonLaborEntryToolStripMenuItem"
        Me.NonLaborEntryToolStripMenuItem.Size = New System.Drawing.Size(128, 24)
        Me.NonLaborEntryToolStripMenuItem.Text = "NonLabor Costs"
        '
        'HumanResourceToolStripMenuItem
        '
        Me.HumanResourceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WorkerInformationToolStripMenuItem, Me.LaborCategoryMaintToolStripMenuItem, Me.PayrollTransmittalToolStripMenuItem})
        Me.HumanResourceToolStripMenuItem.Name = "HumanResourceToolStripMenuItem"
        Me.HumanResourceToolStripMenuItem.Size = New System.Drawing.Size(135, 24)
        Me.HumanResourceToolStripMenuItem.Text = "Human Resource"
        '
        'WorkerInformationToolStripMenuItem
        '
        Me.WorkerInformationToolStripMenuItem.Name = "WorkerInformationToolStripMenuItem"
        Me.WorkerInformationToolStripMenuItem.Size = New System.Drawing.Size(236, 26)
        Me.WorkerInformationToolStripMenuItem.Text = "Worker Information"
        '
        'LaborCategoryMaintToolStripMenuItem
        '
        Me.LaborCategoryMaintToolStripMenuItem.Name = "LaborCategoryMaintToolStripMenuItem"
        Me.LaborCategoryMaintToolStripMenuItem.Size = New System.Drawing.Size(236, 26)
        Me.LaborCategoryMaintToolStripMenuItem.Text = "Labor Category Maint"
        '
        'PayrollTransmittalToolStripMenuItem
        '
        Me.PayrollTransmittalToolStripMenuItem.Name = "PayrollTransmittalToolStripMenuItem"
        Me.PayrollTransmittalToolStripMenuItem.Size = New System.Drawing.Size(236, 26)
        Me.PayrollTransmittalToolStripMenuItem.Text = "Payroll Transmittal"
        '
        'JobUtilitiesToolStripMenuItem
        '
        Me.JobUtilitiesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.JobReportToolStripMenuItem, Me.NameChangeToolStripMenuItem, Me.JobDeactivationToolStripMenuItem, Me.JobPurgeToolStripMenuItem})
        Me.JobUtilitiesToolStripMenuItem.Name = "JobUtilitiesToolStripMenuItem"
        Me.JobUtilitiesToolStripMenuItem.Size = New System.Drawing.Size(100, 24)
        Me.JobUtilitiesToolStripMenuItem.Text = "Job Utilities"
        '
        'JobReportToolStripMenuItem
        '
        Me.JobReportToolStripMenuItem.Name = "JobReportToolStripMenuItem"
        Me.JobReportToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.JobReportToolStripMenuItem.Text = "Job Report"
        '
        'NameChangeToolStripMenuItem
        '
        Me.NameChangeToolStripMenuItem.Name = "NameChangeToolStripMenuItem"
        Me.NameChangeToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.NameChangeToolStripMenuItem.Text = "Name Change"
        '
        'JobDeactivationToolStripMenuItem
        '
        Me.JobDeactivationToolStripMenuItem.Name = "JobDeactivationToolStripMenuItem"
        Me.JobDeactivationToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.JobDeactivationToolStripMenuItem.Text = "Job Deactivation"
        '
        'InvoiceExtrasToolStripMenuItem
        '
        Me.InvoiceExtrasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InvoiceVoidMenuItem, Me.InvoiceReportToolStripMenuItem, Me.InvoiceLookupToolStripMenuItem})
        Me.InvoiceExtrasToolStripMenuItem.Name = "InvoiceExtrasToolStripMenuItem"
        Me.InvoiceExtrasToolStripMenuItem.Size = New System.Drawing.Size(117, 24)
        Me.InvoiceExtrasToolStripMenuItem.Text = "Invoice  Extras"
        '
        'InvoiceVoidMenuItem
        '
        Me.InvoiceVoidMenuItem.Name = "InvoiceVoidMenuItem"
        Me.InvoiceVoidMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.InvoiceVoidMenuItem.Text = "Invoice Void"
        '
        'InvoiceReportToolStripMenuItem
        '
        Me.InvoiceReportToolStripMenuItem.Name = "InvoiceReportToolStripMenuItem"
        Me.InvoiceReportToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.InvoiceReportToolStripMenuItem.Text = "Inv. Hist Report"
        '
        'InvoiceLookupToolStripMenuItem
        '
        Me.InvoiceLookupToolStripMenuItem.Name = "InvoiceLookupToolStripMenuItem"
        Me.InvoiceLookupToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.InvoiceLookupToolStripMenuItem.Text = "Invoice Lookup"
        '
        'AccountingInterfaceToolStripMenuItem
        '
        Me.AccountingInterfaceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QBReportToolStripMenuItem, Me.EDITestingToolStripMenuItem})
        Me.AccountingInterfaceToolStripMenuItem.Name = "AccountingInterfaceToolStripMenuItem"
        Me.AccountingInterfaceToolStripMenuItem.Size = New System.Drawing.Size(160, 24)
        Me.AccountingInterfaceToolStripMenuItem.Text = "Accounting Interface"
        '
        'QBReportToolStripMenuItem
        '
        Me.QBReportToolStripMenuItem.Name = "QBReportToolStripMenuItem"
        Me.QBReportToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.QBReportToolStripMenuItem.Text = "QB Report/Extract"
        '
        'EDITestingToolStripMenuItem
        '
        Me.EDITestingToolStripMenuItem.Name = "EDITestingToolStripMenuItem"
        Me.EDITestingToolStripMenuItem.Size = New System.Drawing.Size(212, 26)
        Me.EDITestingToolStripMenuItem.Text = "EDI Testing"
        '
        'UtilitiesToolStripMenuItem
        '
        Me.UtilitiesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GlobalSettingsToolStripMenuItem})
        Me.UtilitiesToolStripMenuItem.Name = "UtilitiesToolStripMenuItem"
        Me.UtilitiesToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
        Me.UtilitiesToolStripMenuItem.Text = "Utilities"
        '
        'GlobalSettingsToolStripMenuItem
        '
        Me.GlobalSettingsToolStripMenuItem.Name = "GlobalSettingsToolStripMenuItem"
        Me.GlobalSettingsToolStripMenuItem.Size = New System.Drawing.Size(193, 26)
        Me.GlobalSettingsToolStripMenuItem.Text = "Global Settings"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(47, 24)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = Global.JobInfoMngr.My.Resources.Resources.Logo2
        Me.PictureBox1.Location = New System.Drawing.Point(796, 320)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(245, 180)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.UseWaitCursor = True
        '
        'JobPurgeToolStripMenuItem
        '
        Me.JobPurgeToolStripMenuItem.Name = "JobPurgeToolStripMenuItem"
        Me.JobPurgeToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.JobPurgeToolStripMenuItem.Text = "Job Purge"
        '
        'JTstart
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1049, 510)
        Me.Controls.Add(Me.chkboxJTStartForgotPW)
        Me.Controls.Add(Me.txtJTInitStatus)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSignIn)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTstart"
        Me.Text = " WATCH HILL TECHNOLOGY - JOB INFORMATION MANAGER"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents txtUserID As TextBox
    Friend WithEvents btnSignIn As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTInitStatus As TextBox
    Friend WithEvents chkboxJTStartForgotPW As CheckBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents NonLaborEntryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HumanResourceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WorkerInformationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LaborCategoryMaintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PayrollTransmittalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UtilitiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GlobalSettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JobUtilitiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JobReportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NameChangeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InvoiceExtrasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InvoiceReportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AccountingInterfaceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QBReportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InvoiceVoidMenuItem As ToolStripMenuItem
    Friend WithEvents InvoiceLookupToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JobDeactivationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EDITestingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents JobPurgeToolStripMenuItem As ToolStripMenuItem
End Class
