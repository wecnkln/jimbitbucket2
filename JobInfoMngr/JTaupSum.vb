﻿Public Class JTaupSum
    Public JTaupSumCameFromHere As Boolean = False
    Public Sub JTaupSumInit()
        lvJTaupSum.Items.Clear()
        Dim tmpSub As Integer = 0
        Dim tmpJTjimRec As JTjim.JTjimJobInfo = Nothing
        Do While JTjim.JTjimReadJtjim(tmpSub, tmpJTjimRec) = "DATA"
            If tmpJTjimRec.JTjimJIJobPricMeth = "AUP" Or tmpJTjimRec.JTjimJIJobPricMeth = "AUPH" Then
                Dim tmpAUPInfo As JTaup.JTaupInfoRec = Nothing
                tmpAUPInfo.JTaupCustomer = tmpJTjimRec.JTjimJIJobName
                tmpAUPInfo.JTaupJob = tmpJTjimRec.JTjimJISubName
                JTaup.JTaupJobInfo(tmpAUPInfo)
                '
                ' Go get unposted activity here
                '
                Dim itmBuild(15) As String
                Dim lvline As New ListViewItem
                '
                itmBuild(0) = tmpJTjimRec.JTjimJIJobName
                itmBuild(1) = tmpJTjimRec.JTjimJISubName
                itmBuild(2) = tmpJTjimRec.JTjimJICreateDate
                If Val(tmpAUPInfo.JTaupJobInitial) <> 0 Then
                    itmBuild(3) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupJobInitial))
                End If
                If Val(tmpAUPInfo.JTaupJobChanges) <> 0 Then
                    itmBuild(4) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupJobChanges))
                End If
                If Val(tmpAUPInfo.JTaupInvdTD) <> 0 Then
                    itmBuild(5) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupInvdTD))
                End If
                itmBuild(6) = tmpAUPInfo.JTaupLstInvDate
                If Val(tmpAUPInfo.JTaupQueuedNow) <> 0 Then
                    itmBuild(7) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupQueuedNow))
                End If
                If Val(tmpAUPInfo.JTaupQueuedFuture) <> 0 Then
                    itmBuild(8) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupQueuedFuture))
                End If
                Dim tmpTBInvoiced As Decimal = Val(tmpAUPInfo.JTaupJobInitial) + Val(tmpAUPInfo.JTaupJobChanges) -
                                            Val(tmpAUPInfo.JTaupQueuedNow) - Val(tmpAUPInfo.JTaupInvdTD) - Val(tmpAUPInfo.JTaupQueuedFuture)
                If tmpTBInvoiced <> 0 Then
                    itmBuild(9) = String.Format("{0:0.00}", tmpTBInvoiced)
                End If
                Dim tmpPostedActivity As String = ""
                JTas.JTasAUPPostedActivity(tmpJTjimRec.JTjimJIJobName, tmpJTjimRec.JTjimJISubName, tmpPostedActivity)
                If tmpPostedActivity <> 0 Then
                    itmBuild(10) = String.Format("{0:0.00}", Val(tmpPostedActivity))
                End If
                Dim tmpNotInvLab As Double = 0
                Dim tmpNotInvMat As Double = 0
                Dim tmpNotInvSub As Double = 0
                JTMainMenu.JTMMNotInvValues(tmpJTjimRec.JTjimJIJobName, tmpJTjimRec.JTjimJISubName, tmpNotInvLab, tmpNotInvMat, tmpNotInvSub)
                Dim tmpUnpostedActivity As Double = tmpNotInvLab + tmpNotInvMat + tmpNotInvSub
                If tmpUnpostedActivity <> 0 Then
                    itmBuild(11) = String.Format("{0:0.00}", tmpUnpostedActivity)
                End If
                If tmpAUPInfo.JTaupLstInvSent = True Then
                    itmBuild(12) = "X"
                End If
                If tmpJTjimRec.JTjimJIAUPLabor <> "X" Then
                    itmBuild(13) = "L"
                End If
                If tmpJTjimRec.JTjimJIAUPMat <> "X" Then
                    itmBuild(13) = itmBuild(13) & "M"
                End If
                If tmpJTjimRec.JTjimJIAUPServices <> "X" Then
                    itmBuild(13) = itmBuild(13) & "S"
                End If
                itmBuild(14) = tmpJTjimRec.JTjimJIDescName
                If itmBuild(11) = "" And itmBuild(12) = "X" Then
                    itmBuild = Nothing
                Else
                    lvline = New ListViewItem(itmBuild)
                    If Val(tmpAUPInfo.JTaupJobInitial) = 0 Then       ' no I record exists 
                        lvline.ForeColor = Color.Red
                    ElseIf tmpTBInvoiced <> 0 Then
                        If itmBuild(13) <> "LMS" Then
                            lvline.ForeColor = Color.Blue    ' color item blue if there is more to be invoiced and it is not AUPH ... likely for deposit only
                        End If
                    End If
                    lvJTaupSum.Items.Add(lvline)
                    itmBuild = Nothing
                End If
            End If
        Loop
        Me.MdiParent = JTstart
        Me.Show()
    End Sub
    '
    Private Sub JTaupSumClose_Click(sender As Object, e As EventArgs) Handles JTaupSumClose.Click
        lvJTaupSum.Items.Clear()
        JTaupSumCameFromHere = False
        Me.Hide()
        JTMainMenu.JTMMinit()
    End Sub

    Private Sub LvJTaupSum_DoubleClick(sender As Object, e As EventArgs) Handles lvJTaupSum.DoubleClick
        JTaupSumCameFromHere = True
        Dim tmpExcludes As String = ""
        If lvJTaupSum.SelectedItems(0).SubItems(13).Text <> "" Then
            tmpExcludes = "AUP Quote excludes "
            Dim tmpChar As String = lvJTaupSum.SelectedItems(0).SubItems(13).Text
            Dim tmpPos As Integer = 0
            Do While tmpPos < tmpChar.Length
                Select Case tmpChar.Substring(tmpPos, 1)
                    Case "L"
                        tmpExcludes = tmpExcludes & "Labor "
                    Case "M"
                        tmpExcludes = tmpExcludes & "Material "
                    Case "S"
                        tmpExcludes = tmpExcludes & "Services "
                End Select
                tmpPos += 1
            Loop
        End If
        JTaup.txtJTaupCategories.Text = tmpExcludes

        Me.Hide()
        JTaup.JTaupInit(lvJTaupSum.SelectedItems(0).Text, lvJTaupSum.SelectedItems(0).SubItems(1).Text,
                        lvJTaupSum.SelectedItems(0).SubItems(14).Text)
    End Sub
End Class