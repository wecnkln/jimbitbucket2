﻿
Imports System.IO
Public Class JTinv
    Public JTinvCameFromHere As Boolean = False
    ''' <summary>
    ''' Entry point for the JTinv routines. Sub gather and formats information to be presented on the
    ''' JTinv panel.
    ''' </summary>
    Public Sub JTinvInit()
        Dim tmpSub As Integer = 0
        Dim tmpJTjimRec As JTjim.JTjimJobInfo = Nothing
        Do While JTjim.JTjimReadJtjim(tmpSub, tmpJTjimRec) = "DATA"
            Dim tmpAUPInfo As JTaup.JTaupInfoRec = Nothing
            If tmpJTjimRec.JTjimJIJobPricMeth <> "CSTP" Then
                tmpAUPInfo.JTaupCustomer = tmpJTjimRec.JTjimJIJobName
                tmpAUPInfo.JTaupJob = tmpJTjimRec.JTjimJISubName
                JTaup.JTaupJobInfo(tmpAUPInfo)
            End If
            Dim tmpNotInvLab As Double = 0
            Dim tmpNotInvMat As Double = 0
            Dim tmpNotInvSub As Double = 0
            JTMainMenu.JTMMNotInvValues(tmpJTjimRec.JTjimJIJobName, tmpJTjimRec.JTjimJISubName, tmpNotInvLab, tmpNotInvMat, tmpNotInvSub)
            Dim tmpUnpostedActivity As Double = tmpNotInvLab + tmpNotInvMat + tmpNotInvSub
            '
            '
            Dim itmBuild(15) As String
            Dim lvline As New ListViewItem
            '
            itmBuild(0) = tmpJTjimRec.JTjimJIJobName
            itmBuild(1) = tmpJTjimRec.JTjimJISubName
            '
            Dim tmpCumInvTotals As Decimal = 0
            Dim tmpSub2 As Integer = 0
            Dim tmpInvDate As String = "FIRST"
            Dim tmpInvAmt As String = Nothing
            itmBuild(4) = ""
            Do While JTas.JTasReturnInvHist(tmpSub2, tmpJTjimRec.JTjimJIJobName, tmpJTjimRec.JTjimJISubName, tmpInvDate, tmpInvAmt) = "DATA"
                tmpCumInvTotals = tmpCumInvTotals + Val(tmpInvAmt)
                If itmBuild(3) = "" Then
                    itmBuild(3) = tmpInvDate
                    itmBuild(4) = String.Format("{0:0.00}", Val(tmpInvAmt))
                Else
                    Dim tmpLstDate As Date = CDate(itmBuild(3))
                    Dim tmpThisDate As Date = CDate(tmpInvDate)
                    If tmpThisDate > tmpLstDate Then
                        itmBuild(3) = tmpInvDate
                        itmBuild(4) = String.Format("{0:0.00}", Val(tmpInvAmt))
                    End If
                End If
                '      If Val(tmpInvAmt) <> 0 Then
                '     itmBuild(4) = String.Format("{0:0.00}", Val(tmpInvAmt))
                '    End If
            Loop

            If tmpCumInvTotals <> 0 Then
                itmBuild(2) = String.Format("{0:0.00}", tmpCumInvTotals)
            End If
            '


            itmBuild(5) = tmpJTjimRec.JTjimJIJobPricMeth
            ' amount queued for invoicing
            Dim tmpQueuedAmt As Decimal = 0

            If tmpJTjimRec.JTjimJIJobPricMeth = "AUP" Or tmpJTjimRec.JTjimJIJobPricMeth = "AUPH" Then
                tmpQueuedAmt = tmpAUPInfo.JTaupQueuedNow
                If tmpJTjimRec.JTjimJIJobPricMeth = "AUP" Then
                    If tmpJTjimRec.JTjimJIAUPLabor <> "X" Then
                        tmpQueuedAmt += tmpNotInvLab
                        itmBuild(11) = "Labor "
                    End If
                    If tmpJTjimRec.JTjimJIAUPMat <> "X" Then
                        tmpQueuedAmt += tmpNotInvMat
                        itmBuild(11) = itmBuild(11) & "Materials "
                    End If
                    If tmpJTjimRec.JTjimJIAUPServices <> "X" Then
                        tmpQueuedAmt += tmpNotInvSub
                        itmBuild(11) = itmBuild(11) & "Services"
                    End If
                End If
            Else
                tmpQueuedAmt = tmpUnpostedActivity
            End If
            itmBuild(6) = String.Format("{0:0.00}", tmpQueuedAmt)


            If tmpNotInvLab <> 0 Then
                itmBuild(7) = String.Format("{0:0.00}", tmpNotInvLab)

            End If
            If tmpNotInvMat <> 0 Then
                itmBuild(8) = String.Format("{0:0.00}", tmpNotInvMat)
            End If
            If tmpNotInvSub <> 0 Then
                itmBuild(9) = String.Format("{0:0.00}", tmpNotInvSub)
            End If
            If tmpAUPInfo.JTaupQueuedNow <> 0 Then
                itmBuild(10) = String.Format("{0:0.00}", Val(tmpAUPInfo.JTaupQueuedNow))
            End If
            itmBuild(14) = tmpJTjimRec.JTjimJIDescName
            If Val(itmBuild(6)) = 0 Then
                itmBuild = Nothing
            Else
                lvline = New ListViewItem(itmBuild)
                lvJTinv.Items.Add(lvline)
                itmBuild = Nothing
            End If
            '
        Loop
        rbtnJobSeq.Checked = True
        Me.Show()
    End Sub

    Private Sub BtnJTinvClose_Click(sender As Object, e As EventArgs) Handles btnJTinvClose.Click
        lvJTinv.Items.Clear()
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub
    ''' <summary>
    ''' Queued on mouse move ... recalculates the total amount from the invoices that have been selected 
    ''' for production.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LvJTinv_MouseMove(sender As Object, e As EventArgs) Handles lvJTinv.MouseMove
        Dim tmpSub As Integer = 0
        Dim tmpSelectedJobs As Decimal = 0
        Dim tmpSelectedAmounts As Decimal = 0
        Do While tmpSub < lvJTinv.CheckedItems.Count
            tmpSelectedJobs += 1
            tmpSelectedAmounts += Val(lvJTinv.CheckedItems(tmpSub).SubItems(6).Text)
            tmpSub += 1
        Loop
        txtJTinvSelJobs.Text = tmpSelectedJobs
        txtJTinvSelAmts.Text = String.Format("{0:0.00}", tmpSelectedAmounts)
    End Sub
    ''' <summary>
    ''' Set switch so that invoicing routines know to return here after producing a single invoice.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTinvProduce_Click(sender As Object, e As EventArgs) Handles btnJTinvProduce.Click
        JTinvCameFromHere = True
        Me.Hide()
        JTinvQueueInvoice()
    End Sub
    ''' <summary>
    ''' Sub checked for selected items. If one is found is founded, the information needed
    ''' for invoicing is organized and control is passed to JTas ... to do the technical part of invoicing.
    ''' </summary>
    Public Sub JTinvQueueInvoice()
        If lvJTinv.CheckedItems.Count > 0 Then
            Dim tmpCustID As String = lvJTinv.CheckedItems(0).Text
            Dim tmpJobID As String = lvJTinv.CheckedItems(0).SubItems(1).Text
            Dim tmpJobDesc As String = lvJTinv.CheckedItems(0).SubItems(14).Text
            Dim tmpJobPricing As String = Nothing
            If JTinvCntCustJobs(lvJTinv.CheckedItems(0).Text) > 1 Then
                Dim tmpAns As Integer = MsgBox("Customer selected (" & tmpCustID & ") for invoicing has multiple jobs." & vbCrLf &
                 "Do you want to invoice all jobs of this customer (Yes) or" & vbCrLf &
               "just the specific job selected (No)? (JI.006)", vbYesNoCancel, "J.I.M. - Invoice Select Options")
                If tmpAns = vbYes Then
                    tmpJobPricing = "MSCOMB"
                End If
            End If
            Dim tmpLayer As String = "Lab"
            If tmpJobPricing <> "MSCOMB" Then
                lvJTinv.CheckedItems(0).Checked = False
            Else
                Dim tmpSub As Integer = 0
                Do While tmpSub < lvJTinv.Items.Count
                    If lvJTinv.Items(tmpSub).Text = tmpCustID Then
                        lvJTinv.Items(tmpSub).Checked = False
                    End If
                    tmpSub += 1
                Loop
            End If
            JTas.JTasInit(tmpCustID, tmpJobID, tmpJobDesc, tmpJobPricing, tmpLayer)
        Else
            JTinvCameFromHere = False
            lvJTinv.Items.Clear()
            Me.Close()
            JTMainMenu.JTMMinit()
        End If
    End Sub


    Private Function JTinvCntCustJobs(tmpCustID As String) As Integer
        Dim tmpJobCount As Integer = 0
        Dim tmpSub As Integer = 0
        Do While tmpSub < lvJTinv.Items.Count
            If lvJTinv.Items(tmpSub).Text = tmpCustID Then
                tmpJobCount += 1
            End If
            tmpSub += 1
        Loop
        Return tmpJobCount
    End Function
    ''' <summary>
    ''' Structure used with tmpslJTinv. It is used for changing the sequence of the 
    ''' JTinv panel.
    ''' </summary>
    Private Structure tmpslStru    'structure used as temp holder to resort lv values ... erased after function complete
        Dim tmpslChecked As Boolean
        Dim tmpsl0 As String
        Dim tmpsl1 As String
        Dim tmpsl2 As String
        Dim tmpsl3 As String
        Dim tmpsl4 As String
        Dim tmpsl5 As String
        Dim tmpsl6 As String
        Dim tmpsl7 As String
        Dim tmpsl8 As String
        Dim tmpsl9 As String
        Dim tmpsl10 As String
        Dim tmpsl11 As String
        Dim tmpsl12 As String
        Dim tmpsl13 As String
        Dim tmpsl14 As String
    End Structure
    ''' <summary>
    ''' Sorted list used to manipulate sequence of JTinv panel. It is cleared of data as soon as 
    ''' the routines have finished.
    ''' </summary>
    Private tmpslJTinv As New SortedList
    ''' <summary>
    ''' Sub to change the seqence of the JTinv panel. Handles the clicked from all the radio buttons.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTinvRbtn_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJobSeq.CheckedChanged,
                                                                                   rbtnAmtSeq.CheckedChanged,
                                                                                   rbtnAmtSeq.CheckedChanged
        Dim tmpSeqNumber As Integer = 0
        Dim tmpslItem As tmpslStru
        Dim tmpSub As Integer = 0

        Do While tmpSub < lvJTinv.Items.Count
            tmpslItem.tmpslChecked = lvJTinv.Items(tmpSub).Checked
            tmpslItem.tmpsl0 = lvJTinv.Items(tmpSub).Text
            tmpslItem.tmpsl1 = lvJTinv.Items(tmpSub).SubItems(1).Text
            tmpslItem.tmpsl2 = lvJTinv.Items(tmpSub).SubItems(2).Text
            tmpslItem.tmpsl3 = lvJTinv.Items(tmpSub).SubItems(3).Text
            tmpslItem.tmpsl4 = lvJTinv.Items(tmpSub).SubItems(4).Text
            tmpslItem.tmpsl5 = lvJTinv.Items(tmpSub).SubItems(5).Text
            tmpslItem.tmpsl6 = lvJTinv.Items(tmpSub).SubItems(6).Text
            tmpslItem.tmpsl7 = lvJTinv.Items(tmpSub).SubItems(7).Text
            tmpslItem.tmpsl8 = lvJTinv.Items(tmpSub).SubItems(8).Text
            tmpslItem.tmpsl9 = lvJTinv.Items(tmpSub).SubItems(9).Text
            tmpslItem.tmpsl10 = lvJTinv.Items(tmpSub).SubItems(10).Text
            tmpslItem.tmpsl11 = lvJTinv.Items(tmpSub).SubItems(11).Text
            tmpslItem.tmpsl12 = lvJTinv.Items(tmpSub).SubItems(12).Text
            tmpslItem.tmpsl13 = lvJTinv.Items(tmpSub).SubItems(13).Text
            tmpslItem.tmpsl14 = lvJTinv.Items(tmpSub).SubItems(14).Text
            Dim tmpKey As String = Nothing
            If rbtnJobSeq.Checked = True Then
                Dim tmpCustName As String = FixedLengthString(tmpslItem.tmpsl0, 10, "_")
                Dim tmpJobName As String = FixedLengthString(tmpslItem.tmpsl1, 10, "_")
                tmpKey = tmpCustName & tmpJobName
            End If
            If rbtnInvDtSeq.Checked = True Then
                If tmpslItem.tmpsl3 <> "" Then
                    Dim tmpDate As Date = Date.ParseExact(tmpslItem.tmpsl3, "MM/dd/yyyy", Nothing)


                    tmpKey = Gregarian2Julian(tmpDate)
                Else
                    tmpKey = "0000000"
                End If
                Do While tmpslJTinv.ContainsKey(tmpKey)
                    tmpSeqNumber += 1
                    tmpKey = tmpKey & tmpSeqNumber.ToString
                Loop
            End If
            If rbtnAmtSeq.Checked = True Then
                tmpKey = Str(3000000 - Val(tmpslItem.tmpsl6))
                Do While tmpslJTinv.ContainsKey(tmpKey)
                    tmpSeqNumber += 1
                    tmpKey = tmpKey & tmpSeqNumber.ToString
                Loop
            End If
            tmpslJTinv.Add(tmpKey, tmpslItem)
            tmpSub += 1
        Loop
        '
        lvJTinv.Items.Clear()
        Dim itmBuild(15) As String
        Dim lvline As New ListViewItem
        tmpSub = 0
        Dim key As ICollection = tmpslJTinv.Keys
        Dim k As String = Nothing
        '
        For Each k In key
            itmBuild(0) = tmpslJTinv(k).tmpsl0
            itmBuild(1) = tmpslJTinv(k).tmpsl1
            itmBuild(2) = tmpslJTinv(k).tmpsl2
            itmBuild(3) = tmpslJTinv(k).tmpsl3
            itmBuild(4) = tmpslJTinv(k).tmpsl4
            itmBuild(5) = tmpslJTinv(k).tmpsl5
            itmBuild(6) = tmpslJTinv(k).tmpsl6
            itmBuild(7) = tmpslJTinv(k).tmpsl7
            itmBuild(8) = tmpslJTinv(k).tmpsl8
            itmBuild(9) = tmpslJTinv(k).tmpsl9
            itmBuild(10) = tmpslJTinv(k).tmpsl10
            itmBuild(11) = tmpslJTinv(k).tmpsl11
            itmBuild(12) = tmpslJTinv(k).tmpsl12
            itmBuild(13) = tmpslJTinv(k).tmpsl13
            itmBuild(14) = tmpslJTinv(k).tmpsl14
            '
            lvline = New ListViewItem(itmBuild)
            lvJTinv.Items.Add(lvline)
            lvJTinv.Items(tmpSub).Checked = tmpslJTinv(k).tmpslChecked
            tmpSub += 1
        Next
        tmpslJTinv.Clear()
    End Sub
End Class