﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LaborCategories
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.StanLabCatListView = New System.Windows.Forms.ListView()
        Me.LabCatKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LabCatName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LabCatFldRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LabCatShopRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtLabCatDesc = New System.Windows.Forms.TextBox()
        Me.txtStanRate = New System.Windows.Forms.TextBox()
        Me.btnDone = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnChange = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtLabCatKey = New System.Windows.Forms.TextBox()
        Me.txtShopRate = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtErrorMsg = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(327, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Standard Labor Categories"
        '
        'StanLabCatListView
        '
        Me.StanLabCatListView.BackColor = System.Drawing.SystemColors.Window
        Me.StanLabCatListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.LabCatKey, Me.LabCatName, Me.LabCatFldRate, Me.LabCatShopRate})
        Me.StanLabCatListView.FullRowSelect = True
        Me.StanLabCatListView.GridLines = True
        Me.StanLabCatListView.Location = New System.Drawing.Point(16, 86)
        Me.StanLabCatListView.Margin = New System.Windows.Forms.Padding(4)
        Me.StanLabCatListView.MultiSelect = False
        Me.StanLabCatListView.Name = "StanLabCatListView"
        Me.StanLabCatListView.Size = New System.Drawing.Size(531, 307)
        Me.StanLabCatListView.TabIndex = 1
        Me.StanLabCatListView.UseCompatibleStateImageBehavior = False
        Me.StanLabCatListView.View = System.Windows.Forms.View.Details
        '
        'LabCatKey
        '
        Me.LabCatKey.Text = "Lab Cat Key"
        Me.LabCatKey.Width = 90
        '
        'LabCatName
        '
        Me.LabCatName.Text = "Category Title"
        Me.LabCatName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LabCatName.Width = 150
        '
        'LabCatFldRate
        '
        Me.LabCatFldRate.Text = "Field Rate"
        Me.LabCatFldRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LabCatFldRate.Width = 70
        '
        'LabCatShopRate
        '
        Me.LabCatShopRate.Text = "Shop Rate"
        Me.LabCatShopRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LabCatShopRate.Width = 70
        '
        'txtLabCatDesc
        '
        Me.txtLabCatDesc.Location = New System.Drawing.Point(161, 85)
        Me.txtLabCatDesc.Margin = New System.Windows.Forms.Padding(4)
        Me.txtLabCatDesc.Name = "txtLabCatDesc"
        Me.txtLabCatDesc.Size = New System.Drawing.Size(191, 22)
        Me.txtLabCatDesc.TabIndex = 1
        '
        'txtStanRate
        '
        Me.txtStanRate.Location = New System.Drawing.Point(232, 133)
        Me.txtStanRate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStanRate.Name = "txtStanRate"
        Me.txtStanRate.Size = New System.Drawing.Size(75, 22)
        Me.txtStanRate.TabIndex = 2
        '
        'btnDone
        '
        Me.btnDone.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnDone.Location = New System.Drawing.Point(320, 417)
        Me.btnDone.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(100, 28)
        Me.btnDone.TabIndex = 5
        Me.btnDone.Text = "Done"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(529, 417)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(100, 28)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnAdd.Location = New System.Drawing.Point(53, 215)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(100, 28)
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnChange
        '
        Me.btnChange.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnChange.Location = New System.Drawing.Point(161, 215)
        Me.btnChange.Margin = New System.Windows.Forms.Padding(4)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(85, 28)
        Me.btnChange.TabIndex = 8
        Me.btnChange.Text = "Change"
        Me.btnChange.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnDelete.Location = New System.Drawing.Point(255, 215)
        Me.btnDelete.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(83, 27)
        Me.btnDelete.TabIndex = 9
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(65, 48)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 17)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Labor Category Key"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(69, 85)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 17)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Description"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(69, 137)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Hourly Rate"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.txtLabCatKey)
        Me.GroupBox1.Controls.Add(Me.txtShopRate)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnDelete)
        Me.GroupBox1.Controls.Add(Me.btnChange)
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.txtStanRate)
        Me.GroupBox1.Controls.Add(Me.txtLabCatDesc)
        Me.GroupBox1.Location = New System.Drawing.Point(556, 86)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(376, 249)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'txtLabCatKey
        '
        Me.txtLabCatKey.Location = New System.Drawing.Point(232, 39)
        Me.txtLabCatKey.Margin = New System.Windows.Forms.Padding(4)
        Me.txtLabCatKey.Name = "txtLabCatKey"
        Me.txtLabCatKey.Size = New System.Drawing.Size(75, 22)
        Me.txtLabCatKey.TabIndex = 0
        '
        'txtShopRate
        '
        Me.txtShopRate.Location = New System.Drawing.Point(232, 175)
        Me.txtShopRate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtShopRate.Name = "txtShopRate"
        Me.txtShopRate.Size = New System.Drawing.Size(75, 22)
        Me.txtShopRate.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(69, 183)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Shop Rate"
        '
        'txtErrorMsg
        '
        Me.txtErrorMsg.Location = New System.Drawing.Point(556, 342)
        Me.txtErrorMsg.Margin = New System.Windows.Forms.Padding(4)
        Me.txtErrorMsg.Multiline = True
        Me.txtErrorMsg.Name = "txtErrorMsg"
        Me.txtErrorMsg.ReadOnly = True
        Me.txtErrorMsg.Size = New System.Drawing.Size(375, 48)
        Me.txtErrorMsg.TabIndex = 14
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(392, 26)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(539, 43)
        Me.TextBox1.TabIndex = 15
        Me.TextBox1.Text = "Double Click entry for 'Change' or  'Delete'. Enter data and hit 'Add' for new ca" &
    "tegory. Enter 'Done' to save and exit. 'Cancel"" will erase all changes made in t" &
    "his session."
        '
        'LaborCategories
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 459)
        Me.ControlBox = False
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtErrorMsg)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.StanLabCatListView)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimizeBox = False
        Me.Name = "LaborCategories"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information manager - Labor Categories"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents StanLabCatListView As ListView
    Friend WithEvents LabCatKey As ColumnHeader
    Friend WithEvents LabCatName As ColumnHeader
    Friend WithEvents LabCatFldRate As ColumnHeader
    Friend WithEvents txtLabCatDesc As TextBox
    Friend WithEvents txtStanRate As TextBox
    Friend WithEvents btnDone As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnChange As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtErrorMsg As TextBox
    Friend WithEvents LabCatShopRate As ColumnHeader
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents txtShopRate As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtLabCatKey As TextBox
End Class
