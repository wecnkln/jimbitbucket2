﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTye
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTye))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtJTyeSystemYear = New System.Windows.Forms.TextBox()
        Me.txtJTyeCalendarYear = New System.Windows.Forms.TextBox()
        Me.btnJTyeProcessYE = New System.Windows.Forms.Button()
        Me.btnJTyeDeferYE = New System.Windows.Forms.Button()
        Me.tboxJTyeYEprocStep1 = New System.Windows.Forms.TextBox()
        Me.tboxJTyeYEprocStep2 = New System.Windows.Forms.TextBox()
        Me.tboxJTyeYEprocStep5 = New System.Windows.Forms.TextBox()
        Me.btnJTyeDone = New System.Windows.Forms.Button()
        Me.tboxJTyeYEprocStep1a = New System.Windows.Forms.TextBox()
        Me.gboxJTyeJobPurge = New System.Windows.Forms.GroupBox()
        Me.btnJTyeAbortJobPurge = New System.Windows.Forms.Button()
        Me.btnJTyePurgeJobs = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lvJTyePurgeElig = New System.Windows.Forms.ListView()
        Me.JobName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SubJobName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CostMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LstInvDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gboxJTyePurgeStatus = New System.Windows.Forms.GroupBox()
        Me.tboxJTyeJPPCComp = New System.Windows.Forms.TextBox()
        Me.tboxJTyeJPJobsPurged = New System.Windows.Forms.TextBox()
        Me.tboxJTyeJPJobsToPurge = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gboxJTyeJobPurge.SuspendLayout()
        Me.gboxJTyePurgeStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(39, 88)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(633, 376)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(41, 10)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(444, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Job Information Manager  - Year-End Processing"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(196, 22)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Current System Year"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(349, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(212, 22)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Current Calendar Year"
        '
        'txtJTyeSystemYear
        '
        Me.txtJTyeSystemYear.Location = New System.Drawing.Point(253, 50)
        Me.txtJTyeSystemYear.Name = "txtJTyeSystemYear"
        Me.txtJTyeSystemYear.Size = New System.Drawing.Size(70, 28)
        Me.txtJTyeSystemYear.TabIndex = 4
        Me.txtJTyeSystemYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTyeCalendarYear
        '
        Me.txtJTyeCalendarYear.Location = New System.Drawing.Point(601, 50)
        Me.txtJTyeCalendarYear.Name = "txtJTyeCalendarYear"
        Me.txtJTyeCalendarYear.Size = New System.Drawing.Size(70, 28)
        Me.txtJTyeCalendarYear.TabIndex = 5
        Me.txtJTyeCalendarYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnJTyeProcessYE
        '
        Me.btnJTyeProcessYE.Location = New System.Drawing.Point(39, 472)
        Me.btnJTyeProcessYE.Name = "btnJTyeProcessYE"
        Me.btnJTyeProcessYE.Size = New System.Drawing.Size(315, 38)
        Me.btnJTyeProcessYE.TabIndex = 6
        Me.btnJTyeProcessYE.Text = "Execute Year-End routines"
        Me.btnJTyeProcessYE.UseVisualStyleBackColor = True
        '
        'btnJTyeDeferYE
        '
        Me.btnJTyeDeferYE.Location = New System.Drawing.Point(360, 472)
        Me.btnJTyeDeferYE.Name = "btnJTyeDeferYE"
        Me.btnJTyeDeferYE.Size = New System.Drawing.Size(312, 38)
        Me.btnJTyeDeferYE.TabIndex = 7
        Me.btnJTyeDeferYE.Text = "Defer this processing until later"
        Me.btnJTyeDeferYE.UseVisualStyleBackColor = True
        '
        'tboxJTyeYEprocStep1
        '
        Me.tboxJTyeYEprocStep1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tboxJTyeYEprocStep1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxJTyeYEprocStep1.Location = New System.Drawing.Point(695, 8)
        Me.tboxJTyeYEprocStep1.Multiline = True
        Me.tboxJTyeYEprocStep1.Name = "tboxJTyeYEprocStep1"
        Me.tboxJTyeYEprocStep1.ReadOnly = True
        Me.tboxJTyeYEprocStep1.Size = New System.Drawing.Size(272, 52)
        Me.tboxJTyeYEprocStep1.TabIndex = 8
        Me.tboxJTyeYEprocStep1.Text = "Year-End Processing Started ..."
        '
        'tboxJTyeYEprocStep2
        '
        Me.tboxJTyeYEprocStep2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tboxJTyeYEprocStep2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxJTyeYEprocStep2.Location = New System.Drawing.Point(695, 110)
        Me.tboxJTyeYEprocStep2.Multiline = True
        Me.tboxJTyeYEprocStep2.Name = "tboxJTyeYEprocStep2"
        Me.tboxJTyeYEprocStep2.ReadOnly = True
        Me.tboxJTyeYEprocStep2.Size = New System.Drawing.Size(272, 59)
        Me.tboxJTyeYEprocStep2.TabIndex = 11
        Me.tboxJTyeYEprocStep2.Text = "Archival backup created. Store in secure location."
        '
        'tboxJTyeYEprocStep5
        '
        Me.tboxJTyeYEprocStep5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tboxJTyeYEprocStep5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxJTyeYEprocStep5.Location = New System.Drawing.Point(695, 175)
        Me.tboxJTyeYEprocStep5.Multiline = True
        Me.tboxJTyeYEprocStep5.Name = "tboxJTyeYEprocStep5"
        Me.tboxJTyeYEprocStep5.ReadOnly = True
        Me.tboxJTyeYEprocStep5.Size = New System.Drawing.Size(272, 52)
        Me.tboxJTyeYEprocStep5.TabIndex = 12
        Me.tboxJTyeYEprocStep5.Text = "Selected inactive jobs purged."
        '
        'btnJTyeDone
        '
        Me.btnJTyeDone.Location = New System.Drawing.Point(695, 373)
        Me.btnJTyeDone.Name = "btnJTyeDone"
        Me.btnJTyeDone.Size = New System.Drawing.Size(272, 141)
        Me.btnJTyeDone.TabIndex = 13
        Me.btnJTyeDone.Text = "Year-End Processing complete. 'Click' this button to continue with new year's pro" &
    "cessing."
        Me.btnJTyeDone.UseVisualStyleBackColor = True
        '
        'tboxJTyeYEprocStep1a
        '
        Me.tboxJTyeYEprocStep1a.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tboxJTyeYEprocStep1a.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxJTyeYEprocStep1a.Location = New System.Drawing.Point(695, 71)
        Me.tboxJTyeYEprocStep1a.Multiline = True
        Me.tboxJTyeYEprocStep1a.Name = "tboxJTyeYEprocStep1a"
        Me.tboxJTyeYEprocStep1a.ReadOnly = True
        Me.tboxJTyeYEprocStep1a.Size = New System.Drawing.Size(272, 32)
        Me.tboxJTyeYEprocStep1a.TabIndex = 14
        Me.tboxJTyeYEprocStep1a.Text = "Year-End Inv. Report Produced"
        '
        'gboxJTyeJobPurge
        '
        Me.gboxJTyeJobPurge.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTyeJobPurge.Controls.Add(Me.btnJTyeAbortJobPurge)
        Me.gboxJTyeJobPurge.Controls.Add(Me.btnJTyePurgeJobs)
        Me.gboxJTyeJobPurge.Controls.Add(Me.Label4)
        Me.gboxJTyeJobPurge.Controls.Add(Me.lvJTyePurgeElig)
        Me.gboxJTyeJobPurge.Location = New System.Drawing.Point(37, 87)
        Me.gboxJTyeJobPurge.Name = "gboxJTyeJobPurge"
        Me.gboxJTyeJobPurge.Size = New System.Drawing.Size(633, 376)
        Me.gboxJTyeJobPurge.TabIndex = 15
        Me.gboxJTyeJobPurge.TabStop = False
        Me.gboxJTyeJobPurge.Text = "Inactive Job Purge"
        '
        'btnJTyeAbortJobPurge
        '
        Me.btnJTyeAbortJobPurge.Location = New System.Drawing.Point(321, 329)
        Me.btnJTyeAbortJobPurge.Name = "btnJTyeAbortJobPurge"
        Me.btnJTyeAbortJobPurge.Size = New System.Drawing.Size(288, 39)
        Me.btnJTyeAbortJobPurge.TabIndex = 3
        Me.btnJTyeAbortJobPurge.Text = "Do Not Purge Any Jobs"
        Me.btnJTyeAbortJobPurge.UseVisualStyleBackColor = True
        '
        'btnJTyePurgeJobs
        '
        Me.btnJTyePurgeJobs.Location = New System.Drawing.Point(28, 328)
        Me.btnJTyePurgeJobs.Name = "btnJTyePurgeJobs"
        Me.btnJTyePurgeJobs.Size = New System.Drawing.Size(287, 39)
        Me.btnJTyePurgeJobs.TabIndex = 2
        Me.btnJTyePurgeJobs.Text = "Perform Purge"
        Me.btnJTyePurgeJobs.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label4.Location = New System.Drawing.Point(16, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(595, 36)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Jobs that are currently inactive with no invoicing in the last twelve (12) months" &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "are eligible for deletion. Uncheck jobs to stop deletion and to keep active."
        '
        'lvJTyePurgeElig
        '
        Me.lvJTyePurgeElig.CheckBoxes = True
        Me.lvJTyePurgeElig.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.JobName, Me.SubJobName, Me.Type, Me.CostMeth, Me.LstInvDt})
        Me.lvJTyePurgeElig.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvJTyePurgeElig.HideSelection = False
        Me.lvJTyePurgeElig.Location = New System.Drawing.Point(28, 75)
        Me.lvJTyePurgeElig.Name = "lvJTyePurgeElig"
        Me.lvJTyePurgeElig.Size = New System.Drawing.Size(581, 245)
        Me.lvJTyePurgeElig.TabIndex = 0
        Me.lvJTyePurgeElig.UseCompatibleStateImageBehavior = False
        Me.lvJTyePurgeElig.View = System.Windows.Forms.View.Details
        '
        'JobName
        '
        Me.JobName.Text = "Customer"
        Me.JobName.Width = 88
        '
        'SubJobName
        '
        Me.SubJobName.Text = "Job"
        Me.SubJobName.Width = 105
        '
        'Type
        '
        Me.Type.Text = "JobType"
        Me.Type.Width = 99
        '
        'CostMeth
        '
        Me.CostMeth.Text = "Cst Meth"
        Me.CostMeth.Width = 105
        '
        'LstInvDt
        '
        Me.LstInvDt.Text = "Last Invoice"
        Me.LstInvDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LstInvDt.Width = 130
        '
        'gboxJTyePurgeStatus
        '
        Me.gboxJTyePurgeStatus.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTyePurgeStatus.Controls.Add(Me.tboxJTyeJPPCComp)
        Me.gboxJTyePurgeStatus.Controls.Add(Me.tboxJTyeJPJobsPurged)
        Me.gboxJTyePurgeStatus.Controls.Add(Me.tboxJTyeJPJobsToPurge)
        Me.gboxJTyePurgeStatus.Controls.Add(Me.Label7)
        Me.gboxJTyePurgeStatus.Controls.Add(Me.Label6)
        Me.gboxJTyePurgeStatus.Controls.Add(Me.Label5)
        Me.gboxJTyePurgeStatus.Location = New System.Drawing.Point(695, 128)
        Me.gboxJTyePurgeStatus.Name = "gboxJTyePurgeStatus"
        Me.gboxJTyePurgeStatus.Size = New System.Drawing.Size(246, 336)
        Me.gboxJTyePurgeStatus.TabIndex = 16
        Me.gboxJTyePurgeStatus.TabStop = False
        Me.gboxJTyePurgeStatus.Text = "JTye Purge Status"
        Me.gboxJTyePurgeStatus.Visible = False
        '
        'tboxJTyeJPPCComp
        '
        Me.tboxJTyeJPPCComp.Location = New System.Drawing.Point(73, 274)
        Me.tboxJTyeJPPCComp.Name = "tboxJTyeJPPCComp"
        Me.tboxJTyeJPPCComp.ReadOnly = True
        Me.tboxJTyeJPPCComp.Size = New System.Drawing.Size(100, 28)
        Me.tboxJTyeJPPCComp.TabIndex = 5
        Me.tboxJTyeJPPCComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tboxJTyeJPJobsPurged
        '
        Me.tboxJTyeJPJobsPurged.Location = New System.Drawing.Point(73, 184)
        Me.tboxJTyeJPJobsPurged.Name = "tboxJTyeJPJobsPurged"
        Me.tboxJTyeJPJobsPurged.ReadOnly = True
        Me.tboxJTyeJPJobsPurged.Size = New System.Drawing.Size(100, 28)
        Me.tboxJTyeJPJobsPurged.TabIndex = 4
        Me.tboxJTyeJPJobsPurged.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tboxJTyeJPJobsToPurge
        '
        Me.tboxJTyeJPJobsToPurge.Location = New System.Drawing.Point(73, 91)
        Me.tboxJTyeJPJobsToPurge.Name = "tboxJTyeJPJobsToPurge"
        Me.tboxJTyeJPJobsToPurge.ReadOnly = True
        Me.tboxJTyeJPJobsToPurge.Size = New System.Drawing.Size(100, 28)
        Me.tboxJTyeJPJobsToPurge.TabIndex = 3
        Me.tboxJTyeJPJobsToPurge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(60, 231)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 22)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "% Complete"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(49, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 22)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = " Jobs Purged"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(186, 22)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Total Jobs to Purge"
        '
        'JTye
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 22.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 526)
        Me.ControlBox = False
        Me.Controls.Add(Me.gboxJTyePurgeStatus)
        Me.Controls.Add(Me.gboxJTyeJobPurge)
        Me.Controls.Add(Me.tboxJTyeYEprocStep1a)
        Me.Controls.Add(Me.btnJTyeDone)
        Me.Controls.Add(Me.tboxJTyeYEprocStep5)
        Me.Controls.Add(Me.tboxJTyeYEprocStep2)
        Me.Controls.Add(Me.tboxJTyeYEprocStep1)
        Me.Controls.Add(Me.btnJTyeDeferYE)
        Me.Controls.Add(Me.btnJTyeProcessYE)
        Me.Controls.Add(Me.txtJTyeCalendarYear)
        Me.Controls.Add(Me.txtJTyeSystemYear)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTye"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information System -  Year-End Processing"
        Me.gboxJTyeJobPurge.ResumeLayout(False)
        Me.gboxJTyeJobPurge.PerformLayout()
        Me.gboxJTyePurgeStatus.ResumeLayout(False)
        Me.gboxJTyePurgeStatus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtJTyeSystemYear As TextBox
    Friend WithEvents txtJTyeCalendarYear As TextBox
    Friend WithEvents btnJTyeProcessYE As Button
    Friend WithEvents btnJTyeDeferYE As Button
    Friend WithEvents tboxJTyeYEprocStep1 As TextBox
    Friend WithEvents tboxJTyeYEprocStep2 As TextBox
    Friend WithEvents tboxJTyeYEprocStep5 As TextBox
    Friend WithEvents btnJTyeDone As Button
    Friend WithEvents tboxJTyeYEprocStep1a As TextBox
    Friend WithEvents gboxJTyeJobPurge As GroupBox
    Friend WithEvents btnJTyeAbortJobPurge As Button
    Friend WithEvents btnJTyePurgeJobs As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents lvJTyePurgeElig As ListView
    Friend WithEvents JobName As ColumnHeader
    Friend WithEvents SubJobName As ColumnHeader
    Friend WithEvents Type As ColumnHeader
    Friend WithEvents CostMeth As ColumnHeader
    Friend WithEvents LstInvDt As ColumnHeader
    Friend WithEvents gboxJTyePurgeStatus As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents tboxJTyeJPPCComp As TextBox
    Friend WithEvents tboxJTyeJPJobsPurged As TextBox
    Friend WithEvents tboxJTyeJPJobsToPurge As TextBox
End Class
