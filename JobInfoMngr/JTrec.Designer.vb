﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JTrec
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lvJTrecData = New System.Windows.Forms.ListView()
        Me.lvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSpplrName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvDescrpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvoice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvCost = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMorS = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJobInvDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtJTrecRecName = New System.Windows.Forms.TextBox()
        Me.mtxtJTrecDate = New System.Windows.Forms.MaskedTextBox()
        Me.txtJTrecStatBal = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtJTrecSelTotal = New System.Windows.Forms.TextBox()
        Me.txtJTrecDiff = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.mtxtJTrecDueDate = New System.Windows.Forms.MaskedTextBox()
        Me.mtxtJTrecStmtDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTrecManual = New System.Windows.Forms.RadioButton()
        Me.rbtnJTrecExport = New System.Windows.Forms.RadioButton()
        Me.txtJTrecVendCityState = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboxJTrecAddUpdtVend = New System.Windows.Forms.CheckBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtJTrecVendDisc = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtJTrecVendTerms = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtJTrecStmtInvNumb = New System.Windows.Forms.TextBox()
        Me.txtJTrecVendorName = New System.Windows.Forms.TextBox()
        Me.txtJTrecVendAddrss = New System.Windows.Forms.TextBox()
        Me.txtJTrecAcctNum = New System.Windows.Forms.TextBox()
        Me.txtJTrecVndShrtName = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtOptMsg = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnJTrecSuspend = New System.Windows.Forms.Button()
        Me.btnJTrecCorrections = New System.Windows.Forms.Button()
        Me.rbtnJTrecVJD = New System.Windows.Forms.RadioButton()
        Me.rbtnJTrecDVJ = New System.Windows.Forms.RadioButton()
        Me.rbtnJTrecVDJ = New System.Windows.Forms.RadioButton()
        Me.rbtnJTrecVJA = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJTrecNJAccount = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtJTrecNJAmount = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtJTrecNJDesc = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtJTrecNJVendor = New System.Windows.Forms.TextBox()
        Me.btnJTrecNJRecord = New System.Windows.Forms.Button()
        Me.btnJTrecNJDelete = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.mtxtJTrecNJDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtJTrecNJInvNumber = New System.Windows.Forms.TextBox()
        Me.lvJTrecNJRInvList = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvNJRSupplrName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemSource = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JTnlcKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTrecAcctTls = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Amount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.gboxJTrecAccumTls = New System.Windows.Forms.GroupBox()
        Me.txtJTrecNJRSelTotal = New System.Windows.Forms.TextBox()
        Me.txtJTrecRecStmtTl = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gboxJTrecAccumTls.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(344, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Non-Labor Expense Reconciliation"
        '
        'lvJTrecData
        '
        Me.lvJTrecData.AllowDrop = True
        Me.lvJTrecData.CheckBoxes = True
        Me.lvJTrecData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvDate, Me.lvSpplrName, Me.lvDescrpt, Me.lvInvoice, Me.lvInvCost, Me.lvMorS, Me.lvJobID, Me.lvSubName, Me.lvJobInvDt})
        Me.lvJTrecData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvJTrecData.FullRowSelect = True
        Me.lvJTrecData.HideSelection = False
        Me.lvJTrecData.HoverSelection = True
        Me.lvJTrecData.Location = New System.Drawing.Point(8, 212)
        Me.lvJTrecData.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTrecData.MultiSelect = False
        Me.lvJTrecData.Name = "lvJTrecData"
        Me.lvJTrecData.Size = New System.Drawing.Size(633, 354)
        Me.lvJTrecData.TabIndex = 5
        Me.lvJTrecData.UseCompatibleStateImageBehavior = False
        Me.lvJTrecData.View = System.Windows.Forms.View.Details
        '
        'lvDate
        '
        Me.lvDate.Text = "             Date"
        Me.lvDate.Width = 130
        '
        'lvSpplrName
        '
        Me.lvSpplrName.Text = "Spplr/Contrct Name"
        Me.lvSpplrName.Width = 110
        '
        'lvDescrpt
        '
        Me.lvDescrpt.Text = "Material or Service Purchased"
        Me.lvDescrpt.Width = 200
        '
        'lvInvoice
        '
        Me.lvInvoice.Text = "Invoice #"
        Me.lvInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvInvoice.Width = 80
        '
        'lvInvCost
        '
        Me.lvInvCost.Text = "Cost"
        Me.lvInvCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvInvCost.Width = 85
        '
        'lvMorS
        '
        Me.lvMorS.Text = "M or S"
        Me.lvMorS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMorS.Width = 0
        '
        'lvJobID
        '
        Me.lvJobID.Text = "Customer"
        Me.lvJobID.Width = 0
        '
        'lvSubName
        '
        Me.lvSubName.Text = "Job"
        Me.lvSubName.Width = 0
        '
        'lvJobInvDt
        '
        Me.lvJobInvDt.Text = "JobInv Dt"
        Me.lvJobInvDt.Width = 0
        '
        'txtJTrecRecName
        '
        Me.txtJTrecRecName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecRecName.Location = New System.Drawing.Point(440, 683)
        Me.txtJTrecRecName.Name = "txtJTrecRecName"
        Me.txtJTrecRecName.ReadOnly = True
        Me.txtJTrecRecName.Size = New System.Drawing.Size(268, 27)
        Me.txtJTrecRecName.TabIndex = 4
        '
        'mtxtJTrecDate
        '
        Me.mtxtJTrecDate.BeepOnError = True
        Me.mtxtJTrecDate.Location = New System.Drawing.Point(1003, 13)
        Me.mtxtJTrecDate.Margin = New System.Windows.Forms.Padding(4)
        Me.mtxtJTrecDate.Mask = "00/00/0000"
        Me.mtxtJTrecDate.Name = "mtxtJTrecDate"
        Me.mtxtJTrecDate.ReadOnly = True
        Me.mtxtJTrecDate.Size = New System.Drawing.Size(100, 24)
        Me.mtxtJTrecDate.TabIndex = 66
        Me.mtxtJTrecDate.ValidatingType = GetType(Date)
        '
        'txtJTrecStatBal
        '
        Me.txtJTrecStatBal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecStatBal.Location = New System.Drawing.Point(373, 42)
        Me.txtJTrecStatBal.Name = "txtJTrecStatBal"
        Me.txtJTrecStatBal.Size = New System.Drawing.Size(120, 27)
        Me.txtJTrecStatBal.TabIndex = 1
        Me.txtJTrecStatBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(275, 686)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 18)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Reconciliation Title"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(299, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 18)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Stmt Bal:"
        '
        'txtJTrecSelTotal
        '
        Me.txtJTrecSelTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecSelTotal.Location = New System.Drawing.Point(116, 51)
        Me.txtJTrecSelTotal.Name = "txtJTrecSelTotal"
        Me.txtJTrecSelTotal.ReadOnly = True
        Me.txtJTrecSelTotal.Size = New System.Drawing.Size(105, 27)
        Me.txtJTrecSelTotal.TabIndex = 71
        Me.txtJTrecSelTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTrecDiff
        '
        Me.txtJTrecDiff.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecDiff.Location = New System.Drawing.Point(116, 109)
        Me.txtJTrecDiff.Name = "txtJTrecDiff"
        Me.txtJTrecDiff.ReadOnly = True
        Me.txtJTrecDiff.Size = New System.Drawing.Size(105, 27)
        Me.txtJTrecDiff.TabIndex = 73
        Me.txtJTrecDiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox1.Controls.Add(Me.mtxtJTrecDueDate)
        Me.GroupBox1.Controls.Add(Me.mtxtJTrecStmtDate)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVendCityState)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVendDisc)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVendTerms)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtJTrecStmtInvNumb)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVendorName)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVendAddrss)
        Me.GroupBox1.Controls.Add(Me.txtJTrecAcctNum)
        Me.GroupBox1.Controls.Add(Me.txtJTrecVndShrtName)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtJTrecStatBal)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(633, 177)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'mtxtJTrecDueDate
        '
        Me.mtxtJTrecDueDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTrecDueDate.Location = New System.Drawing.Point(373, 72)
        Me.mtxtJTrecDueDate.Mask = "00/00/0000"
        Me.mtxtJTrecDueDate.Name = "mtxtJTrecDueDate"
        Me.mtxtJTrecDueDate.Size = New System.Drawing.Size(100, 27)
        Me.mtxtJTrecDueDate.TabIndex = 106
        Me.mtxtJTrecDueDate.ValidatingType = GetType(Date)
        '
        'mtxtJTrecStmtDate
        '
        Me.mtxtJTrecStmtDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTrecStmtDate.Location = New System.Drawing.Point(351, 12)
        Me.mtxtJTrecStmtDate.Mask = "00/00/0000"
        Me.mtxtJTrecStmtDate.Name = "mtxtJTrecStmtDate"
        Me.mtxtJTrecStmtDate.Size = New System.Drawing.Size(100, 27)
        Me.mtxtJTrecStmtDate.TabIndex = 105
        Me.mtxtJTrecStmtDate.ValidatingType = GetType(Date)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 18)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Acct #"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.Window
        Me.GroupBox5.Controls.Add(Me.rbtnJTrecManual)
        Me.GroupBox5.Controls.Add(Me.rbtnJTrecExport)
        Me.GroupBox5.Location = New System.Drawing.Point(311, 104)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(301, 30)
        Me.GroupBox5.TabIndex = 94
        Me.GroupBox5.TabStop = False
        '
        'rbtnJTrecManual
        '
        Me.rbtnJTrecManual.AutoSize = True
        Me.rbtnJTrecManual.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTrecManual.Location = New System.Drawing.Point(162, 4)
        Me.rbtnJTrecManual.Name = "rbtnJTrecManual"
        Me.rbtnJTrecManual.Size = New System.Drawing.Size(134, 24)
        Me.rbtnJTrecManual.TabIndex = 0
        Me.rbtnJTrecManual.TabStop = True
        Me.rbtnJTrecManual.Text = "Paid Manually"
        Me.rbtnJTrecManual.UseVisualStyleBackColor = True
        '
        'rbtnJTrecExport
        '
        Me.rbtnJTrecExport.AutoSize = True
        Me.rbtnJTrecExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTrecExport.Location = New System.Drawing.Point(14, 4)
        Me.rbtnJTrecExport.Name = "rbtnJTrecExport"
        Me.rbtnJTrecExport.Size = New System.Drawing.Size(149, 24)
        Me.rbtnJTrecExport.TabIndex = 0
        Me.rbtnJTrecExport.TabStop = True
        Me.rbtnJTrecExport.Text = "Exprt to Acct'ng"
        Me.rbtnJTrecExport.UseVisualStyleBackColor = True
        '
        'txtJTrecVendCityState
        '
        Me.txtJTrecVendCityState.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVendCityState.Location = New System.Drawing.Point(13, 135)
        Me.txtJTrecVendCityState.Name = "txtJTrecVendCityState"
        Me.txtJTrecVendCityState.ReadOnly = True
        Me.txtJTrecVendCityState.Size = New System.Drawing.Size(277, 27)
        Me.txtJTrecVendCityState.TabIndex = 15
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.Window
        Me.GroupBox4.Controls.Add(Me.cboxJTrecAddUpdtVend)
        Me.GroupBox4.Location = New System.Drawing.Point(311, 140)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(301, 30)
        Me.GroupBox4.TabIndex = 93
        Me.GroupBox4.TabStop = False
        '
        'cboxJTrecAddUpdtVend
        '
        Me.cboxJTrecAddUpdtVend.AutoSize = True
        Me.cboxJTrecAddUpdtVend.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTrecAddUpdtVend.Location = New System.Drawing.Point(14, 5)
        Me.cboxJTrecAddUpdtVend.Name = "cboxJTrecAddUpdtVend"
        Me.cboxJTrecAddUpdtVend.Size = New System.Drawing.Size(284, 24)
        Me.cboxJTrecAddUpdtVend.TabIndex = 0
        Me.cboxJTrecAddUpdtVend.Text = "Add/Update Stmt Short Name Info"
        Me.cboxJTrecAddUpdtVend.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(494, 76)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(55, 18)
        Me.Label23.TabIndex = 91
        Me.Label23.Text = "Disc %"
        '
        'txtJTrecVendDisc
        '
        Me.txtJTrecVendDisc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVendDisc.Location = New System.Drawing.Point(551, 72)
        Me.txtJTrecVendDisc.Name = "txtJTrecVendDisc"
        Me.txtJTrecVendDisc.ReadOnly = True
        Me.txtJTrecVendDisc.Size = New System.Drawing.Size(79, 27)
        Me.txtJTrecVendDisc.TabIndex = 90
        Me.txtJTrecVendDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(299, 76)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 18)
        Me.Label21.TabIndex = 89
        Me.Label21.Text = "Due Date"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(494, 46)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(51, 18)
        Me.Label15.TabIndex = 87
        Me.Label15.Text = "Terms"
        '
        'txtJTrecVendTerms
        '
        Me.txtJTrecVendTerms.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVendTerms.Location = New System.Drawing.Point(551, 42)
        Me.txtJTrecVendTerms.Name = "txtJTrecVendTerms"
        Me.txtJTrecVendTerms.ReadOnly = True
        Me.txtJTrecVendTerms.Size = New System.Drawing.Size(79, 27)
        Me.txtJTrecVendTerms.TabIndex = 86
        Me.txtJTrecVendTerms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(276, 15)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 18)
        Me.Label5.TabIndex = 85
        Me.Label5.Text = "Stmt Date"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(452, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 18)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "Stmt Inv #"
        '
        'txtJTrecStmtInvNumb
        '
        Me.txtJTrecStmtInvNumb.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecStmtInvNumb.Location = New System.Drawing.Point(529, 11)
        Me.txtJTrecStmtInvNumb.Name = "txtJTrecStmtInvNumb"
        Me.txtJTrecStmtInvNumb.Size = New System.Drawing.Size(101, 27)
        Me.txtJTrecStmtInvNumb.TabIndex = 82
        '
        'txtJTrecVendorName
        '
        Me.txtJTrecVendorName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVendorName.Location = New System.Drawing.Point(13, 75)
        Me.txtJTrecVendorName.Name = "txtJTrecVendorName"
        Me.txtJTrecVendorName.ReadOnly = True
        Me.txtJTrecVendorName.Size = New System.Drawing.Size(277, 27)
        Me.txtJTrecVendorName.TabIndex = 81
        '
        'txtJTrecVendAddrss
        '
        Me.txtJTrecVendAddrss.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVendAddrss.Location = New System.Drawing.Point(13, 105)
        Me.txtJTrecVendAddrss.Name = "txtJTrecVendAddrss"
        Me.txtJTrecVendAddrss.ReadOnly = True
        Me.txtJTrecVendAddrss.Size = New System.Drawing.Size(277, 27)
        Me.txtJTrecVendAddrss.TabIndex = 80
        '
        'txtJTrecAcctNum
        '
        Me.txtJTrecAcctNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecAcctNum.Location = New System.Drawing.Point(67, 43)
        Me.txtJTrecAcctNum.Name = "txtJTrecAcctNum"
        Me.txtJTrecAcctNum.ReadOnly = True
        Me.txtJTrecAcctNum.Size = New System.Drawing.Size(223, 27)
        Me.txtJTrecAcctNum.TabIndex = 8
        '
        'txtJTrecVndShrtName
        '
        Me.txtJTrecVndShrtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecVndShrtName.Location = New System.Drawing.Point(141, 11)
        Me.txtJTrecVndShrtName.Name = "txtJTrecVndShrtName"
        Me.txtJTrecVndShrtName.Size = New System.Drawing.Size(127, 27)
        Me.txtJTrecVndShrtName.TabIndex = 12
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(12, 15)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(123, 18)
        Me.Label20.TabIndex = 77
        Me.Label20.Text = "Stmt Short Name"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(1145, 630)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(125, 57)
        Me.btnExit.TabIndex = 76
        Me.btnExit.Text = "Exit (Cancel)"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(1145, 465)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(126, 100)
        Me.btnUpdate.TabIndex = 0
        Me.btnUpdate.Text = "Update Records, Generate Report and Finish"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'txtOptMsg
        '
        Me.txtOptMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOptMsg.Location = New System.Drawing.Point(274, 605)
        Me.txtOptMsg.Multiline = True
        Me.txtOptMsg.Name = "txtOptMsg"
        Me.txtOptMsg.Size = New System.Drawing.Size(625, 74)
        Me.txtOptMsg.TabIndex = 78
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(274, 570)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(545, 34)
        Me.Label6.TabIndex = 79
        Me.Label6.Text = "Comment Area (optional) --- Any text entered here will be included on the " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "recon" &
    "ciliation report."
        '
        'btnJTrecSuspend
        '
        Me.btnJTrecSuspend.Location = New System.Drawing.Point(1145, 570)
        Me.btnJTrecSuspend.Name = "btnJTrecSuspend"
        Me.btnJTrecSuspend.Size = New System.Drawing.Size(126, 58)
        Me.btnJTrecSuspend.TabIndex = 80
        Me.btnJTrecSuspend.Text = "Suspend Reconciliation Session"
        Me.btnJTrecSuspend.UseVisualStyleBackColor = True
        '
        'btnJTrecCorrections
        '
        Me.btnJTrecCorrections.Location = New System.Drawing.Point(1145, 395)
        Me.btnJTrecCorrections.Name = "btnJTrecCorrections"
        Me.btnJTrecCorrections.Size = New System.Drawing.Size(126, 67)
        Me.btnJTrecCorrections.TabIndex = 81
        Me.btnJTrecCorrections.Text = "Make Line Item Corrections"
        Me.btnJTrecCorrections.UseVisualStyleBackColor = True
        '
        'rbtnJTrecVJD
        '
        Me.rbtnJTrecVJD.AutoSize = True
        Me.rbtnJTrecVJD.Location = New System.Drawing.Point(23, 22)
        Me.rbtnJTrecVJD.Name = "rbtnJTrecVJD"
        Me.rbtnJTrecVJD.Size = New System.Drawing.Size(191, 21)
        Me.rbtnJTrecVJD.TabIndex = 82
        Me.rbtnJTrecVJD.TabStop = True
        Me.rbtnJTrecVJD.Text = "Vender ... Job ... Date"
        Me.rbtnJTrecVJD.UseVisualStyleBackColor = True
        '
        'rbtnJTrecDVJ
        '
        Me.rbtnJTrecDVJ.AutoSize = True
        Me.rbtnJTrecDVJ.Location = New System.Drawing.Point(23, 103)
        Me.rbtnJTrecDVJ.Name = "rbtnJTrecDVJ"
        Me.rbtnJTrecDVJ.Size = New System.Drawing.Size(191, 21)
        Me.rbtnJTrecDVJ.TabIndex = 83
        Me.rbtnJTrecDVJ.TabStop = True
        Me.rbtnJTrecDVJ.Text = "Date ... Vender ... Job"
        Me.rbtnJTrecDVJ.UseVisualStyleBackColor = True
        '
        'rbtnJTrecVDJ
        '
        Me.rbtnJTrecVDJ.AutoSize = True
        Me.rbtnJTrecVDJ.Location = New System.Drawing.Point(23, 76)
        Me.rbtnJTrecVDJ.Name = "rbtnJTrecVDJ"
        Me.rbtnJTrecVDJ.Size = New System.Drawing.Size(191, 21)
        Me.rbtnJTrecVDJ.TabIndex = 84
        Me.rbtnJTrecVDJ.TabStop = True
        Me.rbtnJTrecVDJ.Text = "Vender ... Date ... Job"
        Me.rbtnJTrecVDJ.UseVisualStyleBackColor = True
        '
        'rbtnJTrecVJA
        '
        Me.rbtnJTrecVJA.AutoSize = True
        Me.rbtnJTrecVJA.Location = New System.Drawing.Point(23, 49)
        Me.rbtnJTrecVJA.Name = "rbtnJTrecVJA"
        Me.rbtnJTrecVJA.Size = New System.Drawing.Size(211, 21)
        Me.rbtnJTrecVJA.TabIndex = 85
        Me.rbtnJTrecVJA.TabStop = True
        Me.rbtnJTrecVJA.Text = "Vender ... Job ... Amount"
        Me.rbtnJTrecVJA.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Window
        Me.GroupBox2.Controls.Add(Me.rbtnJTrecVJA)
        Me.GroupBox2.Controls.Add(Me.rbtnJTrecVDJ)
        Me.GroupBox2.Controls.Add(Me.rbtnJTrecDVJ)
        Me.GroupBox2.Controls.Add(Me.rbtnJTrecVJD)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(9, 573)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(254, 133)
        Me.GroupBox2.TabIndex = 87
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Reconciliation Table Sequence"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(654, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(202, 18)
        Me.Label8.TabIndex = 89
        Me.Label8.Text = "Non-Job Related Invoices"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 5)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 18)
        Me.Label9.TabIndex = 75
        Me.Label9.Text = "Date"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(136, 5)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 18)
        Me.Label10.TabIndex = 91
        Me.Label10.Text = "Account"
        '
        'txtJTrecNJAccount
        '
        Me.txtJTrecNJAccount.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJAccount.Location = New System.Drawing.Point(110, 26)
        Me.txtJTrecNJAccount.Name = "txtJTrecNJAccount"
        Me.txtJTrecNJAccount.Size = New System.Drawing.Size(146, 27)
        Me.txtJTrecNJAccount.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(241, 58)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 18)
        Me.Label11.TabIndex = 93
        Me.Label11.Text = "Amount"
        '
        'txtJTrecNJAmount
        '
        Me.txtJTrecNJAmount.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJAmount.Location = New System.Drawing.Point(235, 79)
        Me.txtJTrecNJAmount.Name = "txtJTrecNJAmount"
        Me.txtJTrecNJAmount.Size = New System.Drawing.Size(96, 27)
        Me.txtJTrecNJAmount.TabIndex = 5
        Me.txtJTrecNJAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(73, 58)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(83, 18)
        Me.Label12.TabIndex = 95
        Me.Label12.Text = "Description"
        '
        'txtJTrecNJDesc
        '
        Me.txtJTrecNJDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJDesc.Location = New System.Drawing.Point(6, 79)
        Me.txtJTrecNJDesc.Name = "txtJTrecNJDesc"
        Me.txtJTrecNJDesc.Size = New System.Drawing.Size(222, 27)
        Me.txtJTrecNJDesc.TabIndex = 4
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(310, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 18)
        Me.Label13.TabIndex = 97
        Me.Label13.Text = "Vendor"
        '
        'txtJTrecNJVendor
        '
        Me.txtJTrecNJVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJVendor.Location = New System.Drawing.Point(260, 26)
        Me.txtJTrecNJVendor.Name = "txtJTrecNJVendor"
        Me.txtJTrecNJVendor.Size = New System.Drawing.Size(161, 27)
        Me.txtJTrecNJVendor.TabIndex = 2
        '
        'btnJTrecNJRecord
        '
        Me.btnJTrecNJRecord.Location = New System.Drawing.Point(344, 69)
        Me.btnJTrecNJRecord.Name = "btnJTrecNJRecord"
        Me.btnJTrecNJRecord.Size = New System.Drawing.Size(70, 33)
        Me.btnJTrecNJRecord.TabIndex = 0
        Me.btnJTrecNJRecord.Text = "R'cord"
        Me.btnJTrecNJRecord.UseVisualStyleBackColor = True
        '
        'btnJTrecNJDelete
        '
        Me.btnJTrecNJDelete.Location = New System.Drawing.Point(420, 69)
        Me.btnJTrecNJDelete.Name = "btnJTrecNJDelete"
        Me.btnJTrecNJDelete.Size = New System.Drawing.Size(70, 33)
        Me.btnJTrecNJDelete.TabIndex = 1
        Me.btnJTrecNJDelete.Text = "Delete"
        Me.btnJTrecNJDelete.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox3.Controls.Add(Me.mtxtJTrecNJDate)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txtJTrecNJInvNumber)
        Me.GroupBox3.Controls.Add(Me.btnJTrecNJDelete)
        Me.GroupBox3.Controls.Add(Me.btnJTrecNJRecord)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.txtJTrecNJVendor)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.txtJTrecNJDesc)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtJTrecNJAmount)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtJTrecNJAccount)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Location = New System.Drawing.Point(653, 55)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(514, 117)
        Me.GroupBox3.TabIndex = 101
        Me.GroupBox3.TabStop = False
        '
        'mtxtJTrecNJDate
        '
        Me.mtxtJTrecNJDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTrecNJDate.Location = New System.Drawing.Point(6, 26)
        Me.mtxtJTrecNJDate.Mask = "00/00/0000"
        Me.mtxtJTrecNJDate.Name = "mtxtJTrecNJDate"
        Me.mtxtJTrecNJDate.Size = New System.Drawing.Size(100, 27)
        Me.mtxtJTrecNJDate.TabIndex = 0
        Me.mtxtJTrecNJDate.ValidatingType = GetType(Date)
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(438, 2)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 18)
        Me.Label14.TabIndex = 101
        Me.Label14.Text = "Inv. #"
        '
        'txtJTrecNJInvNumber
        '
        Me.txtJTrecNJInvNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJInvNumber.Location = New System.Drawing.Point(428, 24)
        Me.txtJTrecNJInvNumber.Name = "txtJTrecNJInvNumber"
        Me.txtJTrecNJInvNumber.Size = New System.Drawing.Size(80, 27)
        Me.txtJTrecNJInvNumber.TabIndex = 3
        Me.txtJTrecNJInvNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvJTrecNJRInvList
        '
        Me.lvJTrecNJRInvList.AllowDrop = True
        Me.lvJTrecNJRInvList.CheckBoxes = True
        Me.lvJTrecNJRInvList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.lvNJRSupplrName, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ItemSource, Me.JTnlcKey})
        Me.lvJTrecNJRInvList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvJTrecNJRInvList.FullRowSelect = True
        Me.lvJTrecNJRInvList.HideSelection = False
        Me.lvJTrecNJRInvList.HoverSelection = True
        Me.lvJTrecNJRInvList.Location = New System.Drawing.Point(651, 184)
        Me.lvJTrecNJRInvList.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTrecNJRInvList.MultiSelect = False
        Me.lvJTrecNJRInvList.Name = "lvJTrecNJRInvList"
        Me.lvJTrecNJRInvList.Size = New System.Drawing.Size(619, 203)
        Me.lvJTrecNJRInvList.TabIndex = 3
        Me.lvJTrecNJRInvList.UseCompatibleStateImageBehavior = False
        Me.lvJTrecNJRInvList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "        Date"
        Me.ColumnHeader1.Width = 89
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Account"
        Me.ColumnHeader2.Width = 113
        '
        'lvNJRSupplrName
        '
        Me.lvNJRSupplrName.Text = "Vendor"
        Me.lvNJRSupplrName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvNJRSupplrName.Width = 85
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Invoice #"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader4.Width = 80
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Description"
        Me.ColumnHeader5.Width = 139
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Amount"
        Me.ColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader6.Width = 70
        '
        'ItemSource
        '
        Me.ItemSource.Width = 0
        '
        'JTnlcKey
        '
        Me.JTnlcKey.Width = 0
        '
        'lvJTrecAcctTls
        '
        Me.lvJTrecAcctTls.AllowDrop = True
        Me.lvJTrecAcctTls.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader9, Me.Amount})
        Me.lvJTrecAcctTls.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvJTrecAcctTls.HideSelection = False
        Me.lvJTrecAcctTls.HoverSelection = True
        Me.lvJTrecAcctTls.Location = New System.Drawing.Point(651, 395)
        Me.lvJTrecAcctTls.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTrecAcctTls.MultiSelect = False
        Me.lvJTrecAcctTls.Name = "lvJTrecAcctTls"
        Me.lvJTrecAcctTls.Size = New System.Drawing.Size(487, 170)
        Me.lvJTrecAcctTls.TabIndex = 1
        Me.lvJTrecAcctTls.UseCompatibleStateImageBehavior = False
        Me.lvJTrecAcctTls.View = System.Windows.Forms.View.Details
        Me.lvJTrecAcctTls.Visible = False
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Account"
        Me.ColumnHeader7.Width = 130
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Account Description"
        Me.ColumnHeader9.Width = 200
        '
        'Amount
        '
        Me.Amount.Text = "Amount"
        Me.Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Amount.Width = 110
        '
        'gboxJTrecAccumTls
        '
        Me.gboxJTrecAccumTls.BackColor = System.Drawing.SystemColors.Window
        Me.gboxJTrecAccumTls.Controls.Add(Me.txtJTrecNJRSelTotal)
        Me.gboxJTrecAccumTls.Controls.Add(Me.txtJTrecRecStmtTl)
        Me.gboxJTrecAccumTls.Controls.Add(Me.Label18)
        Me.gboxJTrecAccumTls.Controls.Add(Me.Label17)
        Me.gboxJTrecAccumTls.Controls.Add(Me.txtJTrecDiff)
        Me.gboxJTrecAccumTls.Controls.Add(Me.Label19)
        Me.gboxJTrecAccumTls.Controls.Add(Me.txtJTrecSelTotal)
        Me.gboxJTrecAccumTls.Controls.Add(Me.Label16)
        Me.gboxJTrecAccumTls.Location = New System.Drawing.Point(910, 572)
        Me.gboxJTrecAccumTls.Name = "gboxJTrecAccumTls"
        Me.gboxJTrecAccumTls.Size = New System.Drawing.Size(227, 142)
        Me.gboxJTrecAccumTls.TabIndex = 104
        Me.gboxJTrecAccumTls.TabStop = False
        Me.gboxJTrecAccumTls.Text = "Reconcil Recap"
        '
        'txtJTrecNJRSelTotal
        '
        Me.txtJTrecNJRSelTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecNJRSelTotal.Location = New System.Drawing.Point(116, 80)
        Me.txtJTrecNJRSelTotal.Name = "txtJTrecNJRSelTotal"
        Me.txtJTrecNJRSelTotal.ReadOnly = True
        Me.txtJTrecNJRSelTotal.Size = New System.Drawing.Size(105, 27)
        Me.txtJTrecNJRSelTotal.TabIndex = 80
        Me.txtJTrecNJRSelTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTrecRecStmtTl
        '
        Me.txtJTrecRecStmtTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTrecRecStmtTl.Location = New System.Drawing.Point(116, 21)
        Me.txtJTrecRecStmtTl.Name = "txtJTrecRecStmtTl"
        Me.txtJTrecRecStmtTl.ReadOnly = True
        Me.txtJTrecRecStmtTl.Size = New System.Drawing.Size(105, 27)
        Me.txtJTrecRecStmtTl.TabIndex = 77
        Me.txtJTrecRecStmtTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(6, 114)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(75, 18)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "Difference"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(6, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(87, 18)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Job Related"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(5, 85)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(71, 18)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "NJR Inv's"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(6, 27)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 18)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Stmt Total"
        '
        'JTrec
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(1283, 716)
        Me.ControlBox = False
        Me.Controls.Add(Me.gboxJTrecAccumTls)
        Me.Controls.Add(Me.lvJTrecAcctTls)
        Me.Controls.Add(Me.lvJTrecNJRInvList)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnJTrecCorrections)
        Me.Controls.Add(Me.btnJTrecSuspend)
        Me.Controls.Add(Me.txtJTrecRecName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtOptMsg)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.mtxtJTrecDate)
        Me.Controls.Add(Me.lvJTrecData)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "JTrec"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Non-Labor Expense Reconciliation"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gboxJTrecAccumTls.ResumeLayout(False)
        Me.gboxJTrecAccumTls.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents lvJTrecData As ListView
    Friend WithEvents lvDate As ColumnHeader
    Friend WithEvents lvSpplrName As ColumnHeader
    Friend WithEvents lvDescrpt As ColumnHeader
    Friend WithEvents lvInvoice As ColumnHeader
    Friend WithEvents lvInvCost As ColumnHeader
    Friend WithEvents lvMorS As ColumnHeader
    Friend WithEvents lvJobID As ColumnHeader
    Friend WithEvents lvSubName As ColumnHeader
    Friend WithEvents lvJobInvDt As ColumnHeader
    Friend WithEvents txtJTrecRecName As TextBox
    Friend WithEvents mtxtJTrecDate As MaskedTextBox
    Friend WithEvents txtJTrecStatBal As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtJTrecSelTotal As TextBox
    Friend WithEvents txtJTrecDiff As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnExit As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtOptMsg As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents btnJTrecSuspend As Button
    Friend WithEvents btnJTrecCorrections As Button
    Friend WithEvents rbtnJTrecVJD As RadioButton
    Friend WithEvents rbtnJTrecDVJ As RadioButton
    Friend WithEvents rbtnJTrecVDJ As RadioButton
    Friend WithEvents rbtnJTrecVJA As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtJTrecNJAccount As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtJTrecNJAmount As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtJTrecNJDesc As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtJTrecNJVendor As TextBox
    Friend WithEvents btnJTrecNJRecord As Button
    Friend WithEvents btnJTrecNJDelete As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtJTrecNJInvNumber As TextBox
    Friend WithEvents lvJTrecNJRInvList As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents lvNJRSupplrName As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents lvJTrecAcctTls As ListView
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents Amount As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents rbtnJTrecManual As RadioButton
    Friend WithEvents rbtnJTrecExport As RadioButton
    Friend WithEvents txtJTrecVendCityState As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents cboxJTrecAddUpdtVend As CheckBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtJTrecVendDisc As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents txtJTrecVendTerms As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtJTrecStmtInvNumb As TextBox
    Friend WithEvents txtJTrecVendorName As TextBox
    Friend WithEvents txtJTrecVendAddrss As TextBox
    Friend WithEvents txtJTrecAcctNum As TextBox
    Friend WithEvents txtJTrecVndShrtName As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents gboxJTrecAccumTls As GroupBox
    Friend WithEvents txtJTrecNJRSelTotal As TextBox
    Friend WithEvents txtJTrecRecStmtTl As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents mtxtJTrecDueDate As MaskedTextBox
    Friend WithEvents mtxtJTrecStmtDate As MaskedTextBox
    Friend WithEvents mtxtJTrecNJDate As MaskedTextBox
    Friend WithEvents ItemSource As ColumnHeader
    Friend WithEvents JTnlcKey As ColumnHeader
End Class
