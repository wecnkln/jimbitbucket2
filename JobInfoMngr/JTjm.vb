﻿Public Class JTjm
    Public Structure JTjmSettingsStru
        Dim ExCust As String
        Dim ExJob As String
        Dim NewCust As String
        Dim NewJob As String
        Dim CustNameChg As Boolean
        Dim JobNameChg As Boolean
    End Structure
    ''' <summary>
    ''' This sub handle the DONE button. No real business rules other than closing this class and
    ''' reinitializing the Main Menu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTjmDone_Click(sender As Object, e As EventArgs) Handles btnJTjmDone.Click
        Me.Close()
        JTMainMenu.JTMMFirstTimeThru = "FIRST"
        JTMainMenu.JTMMinit()
    End Sub
    Private Sub RbtnJTjmCustNameChg_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTjmCustNameChg.CheckedChanged
        '      JTjmExLabel.Text = "Customer Name Change"
        '   JTjmNewLabel.Visible = False
        JTjmExJobLabel.Visible = False
        JTjmNewJobLabel.Visible = False
        '     JTjmNewCustLabel.Visible = True
        txtJTjmExJob.Visible = False
        txtJTjmNewJob.Visible = False
        txtJTjmComment.Text = ""
        '
        Dim JTjmcustList As New AutoCompleteStringCollection
        Dim tmpSub As Integer = 0
        Dim tmpCustName As String = ""
        '
        Do While JTjim.JTjimReadCI(tmpSub, tmpCustName) = "DATA"
            JTjmCustList.Add(tmpCustName)
            tmpSub += 1
        Loop
        'Then Set ComboBox AutoComplete properties
        txtJTjmExCust.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTjmExCust.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTjmExCust.AutoCompleteCustomSource = JTjmcustList
        txtJTjmExCust.ReadOnly = False
        txtJTjmExCust.Text = ""
        txtJTjmExCust.Select()
        btnJTjmAction.Enabled = False
        btnJTjmAction.Text = "Action"

    End Sub
    Private Sub RbtnJTjmRename_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTjmRename.CheckedChanged
        JTjmExJobLabel.Visible = True
        JTjmNewJobLabel.Visible = True
        txtJTjmExJob.Visible = True
        txtJTjmNewJob.Visible = True
        '
        Dim JTjmCustList As New AutoCompleteStringCollection
        Dim tmpSub As Integer = 0
        Dim tmpCustName As String = ""
        '
        Do While JTjim.JTjimReadCI(tmpSub, tmpCustName) = "DATA"
            JTjmcustList.Add(tmpCustName)
            tmpSub += 1
        Loop
        'Then Set ComboBox AutoComplete properties
        txtJTjmExCust.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTjmExCust.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTjmExCust.AutoCompleteCustomSource = JTjmCustList
        txtJTjmExCust.ReadOnly = False
        txtJTjmExCust.Text = ""
        txtJTjmExCust.Select()
        btnJTjmAction.Enabled = False
        btnJTjmAction.Text = "Action"
        '
        txtJTjmExCust.Select()
    End Sub
    ''' <summary>
    ''' Validation of existing customer id entered.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub TxtJTjmExCust_Leave(sender As Object, e As EventArgs) Handles txtJTjmExCust.Leave
        txtJTjmExCust.Text = txtJTjmExCust.Text.ToUpper
        If txtJTjmExCust.Text <> "" Then

            If JTjmvalidateCust(txtJTjmExCust.Text) = False Then
                txtJTjmExCust.Select()
                ToolTip1.Show("Existing Customer ID must be entered ... Correct and resubmit.(JM.001)", txtJTjmExCust, 80, 10, 7000)
                Exit Sub
            End If
            If rbtnJTjmCustNameChg.Checked = True Then
                txtJTjmNewCust.ReadOnly = False
                txtJTjmNewCust.Select()
            Else
                Dim JTjmJobList As New AutoCompleteStringCollection
                Dim tmpSub As Integer = 0
                Dim tmpCustName As String = txtJTjmExCust.Text
                Dim tmpJobName As String = Nothing
                '
                Do While JTjim.JTjimReadJI(tmpSub, tmpCustName, tmpJobName) = "DATA"
                    JTjmJobList.Add(tmpJobName)
                Loop
                'Then Set ComboBox AutoComplete properties
                txtJTjmExJob.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                txtJTjmExJob.AutoCompleteSource = AutoCompleteSource.CustomSource
                txtJTjmExJob.AutoCompleteCustomSource = JTjmJobList
                txtJTjmExJob.ReadOnly = False
                txtJTjmExJob.Select()
            End If
        End If
    End Sub
    Private Sub TxtJTjmNewCust_Leave(sender As Object, e As EventArgs) Handles txtJTjmNewCust.Leave
        txtJTjmNewCust.Text = txtJTjmNewCust.Text.ToUpper()
        If txtJTjmNewCust.Text.Contains(" ") = True Then
            ToolTip1.ToolTipTitle = "Customer Name Error"
            ToolTip1.Show("No spaces are allowed in Customer Name." & vbCrLf & "Correct and Resubmit.", txtJTjmNewCust, 40, -90, 7000)
            txtJTjmNewCust.Select()
            Exit Sub
        End If
        If rbtnJTjmCustNameChg.Checked = True Then
            If JTjmvalidateCust(txtJTjmNewCust.Text) = True Then
                ToolTip1.Show("CustomerID already exists. In this operation it must be unique." & vbCrLf & "Correct and Resubmit.",
                              txtJTjmNewCust, 40, -90, 7000)
                txtJTjmNewCust.Select()
                Exit Sub
            End If
            btnJTjmAction.Text = "Chg Cust ID's"
            btnJTjmAction.Enabled = True
            btnJTjmAction.Select()
            Exit Sub
        Else
            If JTjmvalidateCust(txtJTjmNewCust.Text) = False Then
                ToolTip1.Show("CustomerID must be an existing CustID." & vbCrLf & "Correct and Resubmit.",
                              txtJTjmNewCust, 40, -90, 7000)
                txtJTjmNewCust.ReadOnly = False
                txtJTjmNewCust.Select()
                Exit Sub
            End If
            txtJTjmNewJob.ReadOnly = False
            txtJTjmNewJob.Select()
        End If
    End Sub
    Private Sub TxtJTjmExJob_Leave(sender As Object, e As EventArgs) Handles txtJTjmExJob.Leave
        txtJTjmExJob.Text = txtJTjmExJob.Text.ToUpper
        If txtJTjmExJob.Text <> "" Then
            If JTjmvalidateJob(txtJTjmExCust.Text, txtJTjmExJob.Text) = False Then
                ToolTip1.Show("JobID doesn't exists." & vbCrLf & "Correct and Resubmit.",
                          txtJTjmNewCust, 40, -90, 7000)
                txtJTjmExJob.Select()
                Exit Sub
            End If
        End If
        Dim JTjmCustList As New AutoCompleteStringCollection
        Dim tmpSub As Integer = 0
        Dim tmpCustName As String = ""
        '
        Do While JTjim.JTjimReadCI(tmpSub, tmpCustName) = "DATA"
            JTjmCustList.Add(tmpCustName)
            tmpSub += 1
        Loop
        'Then Set ComboBox AutoComplete properties
        txtJTjmNewCust.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTjmNewCust.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTjmNewCust.AutoCompleteCustomSource = JTjmCustList
        txtJTjmNewCust.ReadOnly = False
        txtJTjmNewCust.Select()
    End Sub
    Private Sub TxtJTjmNewJob_Leave(sender As Object, e As EventArgs) Handles txtJTjmNewJob.Leave
        txtJTjmNewJob.Text = txtJTjmNewJob.Text.ToUpper
        If JTjmvalidateJob(txtJTjmNewCust.Text, txtJTjmNewJob.Text) = True Then
            ToolTip1.Show("JobID already exists. New JobID must be unique." & vbCrLf & "Correct and Resubmit.",
                         txtJTjmNewJob, 40, -90, 7000)
            txtJTjmNewJob.Select()
            Exit Sub
        End If
        btnJTjmAction.Text = "Change Job Name"
        btnJTjmAction.Enabled = True
        btnJTjmAction.Select()
    End Sub
    Private Sub BtnJTjmAction_Click(sender As Object, e As EventArgs) Handles btnJTjmAction.Click

        If txtJTjmExCust.Text = "" Then
            ToolTip1.Show("Existing Customer Name Required." & vbCrLf & "Correct and Resubmit.",
                         txtJTjmExCust, 40, -90, 7000)
            txtJTjmExCust.Select()
            Exit Sub
        End If
        If txtJTjmNewCust.Text = "" Then
            ToolTip1.Show("New Customer Name Required." & vbCrLf & "Correct and Resubmit.",
                          txtJTjmNewCust, 40, -90, 7000)
            txtJTjmNewCust.Select()
            Exit Sub
        End If
        If rbtnJTjmRename.Checked = True Then
            If txtJTjmNewJob.Text = "" Then
                ToolTip1.Show("New Customer Name Required." & vbCrLf & "Correct and Resubmit.",
                             txtJTjmNewJob, 40, -90, 7000)
                txtJTjmNewJob.Select()
                Exit Sub
            End If
        End If
        ' ------------------------------------------------------------------------------------------------
        ' The following code will call routines to actually make the data changes. The folowing areas 
        ' and tables will be processed.
        '                    xx  JTAup
        '                    xx   - Job records
        '                    xx   - Invoice Records
        '                    xx  JTCustomers
        '                    xx   - Customer Records
        '                    xx  JTInvHist
        '                    xx  JTJobs
        '                    xx   - Jobs
        '                    xx   - Special Pricing
        '                    xx  JTif
        '                    xx   - Non Standard invoice format                                                                                                              
        '                    xx  JTNonLaborCosts
        '                    xx  JTTimeKeeping
        ' ----------------------------------------------------------------------------------------------
        '
        ' -----------------------------------------------------
        ' Set up change parameters
        ' -----------------------------------------------------
        Dim JTjmSettings As JTjmSettingsStru = Nothing
        JTjmSettings.ExCust = txtJTjmExCust.Text
        JTjmSettings.NewCust = txtJTjmNewCust.Text
        JTjmSettings.CustNameChg = rbtnJTjmCustNameChg.Checked
        JTjmSettings.JobNameChg = rbtnJTjmRename.Checked
        If JTjmSettings.JobNameChg = True Then
            JTjmSettings.ExJob = txtJTjmExJob.Text
            JTjmSettings.NewJob = txtJTjmNewJob.Text
        End If
        '
        ' ------------------------------------------------------
        ' Call various function to make changes
        ' ------------------------------------------------------
        JTjim.JTjimCustJobNameChg(JTjmSettings)
        JTif.JTifCustJobNameChg(JTjmSettings)
        JTnlc.JTnlcCustJobNameChg(JTjmSettings)
        JTtk.JTtkCustJobNameChg(JTjmSettings)
        JTas.JTasCustJobNameChg(JTjmSettings)
        JTaup.JTaupCustJobNameChg(JTjmSettings)
        '
        ' ------------------------------------------------------
        ' Flush and Restore Modified Data
        ' ------------------------------------------------------
        JTjim.JobArrayFlush()
        JTjim.CustArrayFlush()
        JTnlc.JTnlcArrayFlush("")
        JTtk.TKArrayFlush()
        JTas.JTasHistFlush()
        JTaup.JTaupFlush()
        '
        ' -----------------------------------------
        ' Clean up after changes
        ' -----------------------------------------
        txtJTjmExCust.Text = ""
        txtJTjmNewCust.Text = ""
        txtJTjmExJob.Text = ""
        txtJTjmNewJob.Text = ""
        txtJTjmExCust.ReadOnly = True
        txtJTjmNewCust.ReadOnly = True
        txtJTjmExJob.ReadOnly = True
        txtJTjmNewJob.ReadOnly = True
        btnJTjmAction.Text = "Action"
        btnJTjmAction.Enabled = False
        rbtnJTjmCustNameChg.Checked = False
        rbtnJTjmRename.Checked = False
        '
        btnJTjmDone.Select()
    End Sub
    ''' <summary>
    ''' Code in back of cancel button on JTjm panel. Reset fields back to a stable condition.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTjmCancel_Click(sender As Object, e As EventArgs) Handles btnJTjmCancel.Click
        txtJTjmExCust.Text = ""
        txtJTjmNewCust.Text = ""
        txtJTjmExJob.Text = ""
        txtJTjmNewJob.Text = ""
        txtJTjmExCust.ReadOnly = True
        txtJTjmNewCust.ReadOnly = True
        txtJTjmExJob.ReadOnly = True
        txtJTjmNewJob.ReadOnly = True
        btnJTjmAction.Text = "Action"
        btnJTjmAction.Enabled = False
        rbtnJTjmCustNameChg.Checked = False
        rbtnJTjmRename.Checked = False
        '
        btnJTjmDone.Select()
    End Sub

    ''' <summary>
    ''' Function to validate Customer ID.
    ''' </summary>
    ''' <param name="tmpCustID">The Customer ID to be validated.</param>
    ''' <returns>True --- if exists / False --- if not in existence</returns>
    Private Function JTjmvalidateCust(tmpCustID As String) As Boolean
        Dim tmpSub As Integer = 0
        Dim tmpCustName As String = ""
        '
        Do While JTjim.JTjimReadCI(tmpSub, tmpCustName) = "DATA"
            If tmpCustName = tmpCustID Then
                Return True
            End If
            tmpSub += 1
        Loop
        Return False
    End Function


    ''' <summary>
    ''' Function to validate JobID on JTjm panel during job name change.
    ''' </summary>
    ''' <param name="tmpCustID"></param>
    ''' <param name="tmpJobID"></param>
    ''' <returns></returns>
    Private Function JTjmvalidateJob(ByRef tmpCustID As String, ByRef tmpJobID As String) As Boolean
        Dim tmpSub As Integer = 0
        Dim tmpJobName As String = Nothing
        '
        Do While JTjim.JTjimReadJI(tmpSub, tmpCustID, tmpJobName) = "DATA"
            If tmpJobName = tmpJobID Then
                Return True
            End If
        Loop
        Return False
    End Function
End Class