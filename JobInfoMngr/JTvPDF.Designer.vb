﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTvPDF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTvPDF))
        Me.AxAcroPDF1 = New AxAcroPDFLib.AxAcroPDF()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txttmpSubJobID = New System.Windows.Forms.TextBox()
        Me.txtfilename = New System.Windows.Forms.TextBox()
        Me.gboxInvSettings = New System.Windows.Forms.GroupBox()
        Me.gboxInvEmail = New System.Windows.Forms.GroupBox()
        Me.rbtnEmailNever = New System.Windows.Forms.RadioButton()
        Me.rbtnEmailNotNow = New System.Windows.Forms.RadioButton()
        Me.rbtnEmailYes = New System.Windows.Forms.RadioButton()
        Me.gboxInvRecord = New System.Windows.Forms.GroupBox()
        Me.rbtnDiscard = New System.Windows.Forms.RadioButton()
        Me.rbtnRecInv = New System.Windows.Forms.RadioButton()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtJTvPDFRprtType = New System.Windows.Forms.TextBox()
        Me.gboxInvReview = New System.Windows.Forms.GroupBox()
        Me.rbtnReemailNever = New System.Windows.Forms.RadioButton()
        Me.rbtnReemailNo = New System.Windows.Forms.RadioButton()
        Me.rbtnRemailYes = New System.Windows.Forms.RadioButton()
        Me.InvRevEmailLabel = New System.Windows.Forms.Label()
        Me.txtEmailComment = New System.Windows.Forms.TextBox()
        Me.txtEmailDate = New System.Windows.Forms.TextBox()
        Me.txttmpInvNum = New System.Windows.Forms.TextBox()
        Me.txttmpJobID = New System.Windows.Forms.TextBox()
        Me.txtTmpInvDate = New System.Windows.Forms.TextBox()
        CType(Me.AxAcroPDF1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxInvSettings.SuspendLayout()
        Me.gboxInvEmail.SuspendLayout()
        Me.gboxInvRecord.SuspendLayout()
        Me.gboxInvReview.SuspendLayout()
        Me.SuspendLayout()
        '
        'AxAcroPDF1
        '
        Me.AxAcroPDF1.Enabled = True
        Me.AxAcroPDF1.Location = New System.Drawing.Point(238, 12)
        Me.AxAcroPDF1.Name = "AxAcroPDF1"
        Me.AxAcroPDF1.OcxState = CType(resources.GetObject("AxAcroPDF1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxAcroPDF1.Size = New System.Drawing.Size(1155, 727)
        Me.AxAcroPDF1.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(18, 694)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(208, 45)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Close PDF / Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txttmpSubJobID
        '
        Me.txttmpSubJobID.Location = New System.Drawing.Point(1094, 706)
        Me.txttmpSubJobID.Name = "txttmpSubJobID"
        Me.txttmpSubJobID.ReadOnly = True
        Me.txttmpSubJobID.Size = New System.Drawing.Size(125, 22)
        Me.txttmpSubJobID.TabIndex = 2
        Me.txttmpSubJobID.Visible = False
        '
        'txtfilename
        '
        Me.txtfilename.Location = New System.Drawing.Point(954, 706)
        Me.txtfilename.Name = "txtfilename"
        Me.txtfilename.ReadOnly = True
        Me.txtfilename.Size = New System.Drawing.Size(125, 22)
        Me.txtfilename.TabIndex = 3
        Me.txtfilename.Visible = False
        '
        'gboxInvSettings
        '
        Me.gboxInvSettings.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.gboxInvSettings.Controls.Add(Me.gboxInvEmail)
        Me.gboxInvSettings.Controls.Add(Me.gboxInvRecord)
        Me.gboxInvSettings.Controls.Add(Me.TextBox1)
        Me.gboxInvSettings.Location = New System.Drawing.Point(18, 388)
        Me.gboxInvSettings.Name = "gboxInvSettings"
        Me.gboxInvSettings.Size = New System.Drawing.Size(208, 285)
        Me.gboxInvSettings.TabIndex = 4
        Me.gboxInvSettings.TabStop = False
        Me.gboxInvSettings.Text = "Invoice Options"
        Me.gboxInvSettings.Visible = False
        '
        'gboxInvEmail
        '
        Me.gboxInvEmail.Controls.Add(Me.rbtnEmailNever)
        Me.gboxInvEmail.Controls.Add(Me.rbtnEmailNotNow)
        Me.gboxInvEmail.Controls.Add(Me.rbtnEmailYes)
        Me.gboxInvEmail.Location = New System.Drawing.Point(7, 171)
        Me.gboxInvEmail.Name = "gboxInvEmail"
        Me.gboxInvEmail.Size = New System.Drawing.Size(194, 102)
        Me.gboxInvEmail.TabIndex = 2
        Me.gboxInvEmail.TabStop = False
        Me.gboxInvEmail.Text = "Email  Invoice?"
        '
        'rbtnEmailNever
        '
        Me.rbtnEmailNever.AutoSize = True
        Me.rbtnEmailNever.Location = New System.Drawing.Point(9, 75)
        Me.rbtnEmailNever.Name = "rbtnEmailNever"
        Me.rbtnEmailNever.Size = New System.Drawing.Size(171, 20)
        Me.rbtnEmailNever.TabIndex = 2
        Me.rbtnEmailNever.Text = "Never Email this invoice"
        Me.rbtnEmailNever.UseVisualStyleBackColor = True
        '
        'rbtnEmailNotNow
        '
        Me.rbtnEmailNotNow.AutoSize = True
        Me.rbtnEmailNotNow.Location = New System.Drawing.Point(9, 47)
        Me.rbtnEmailNotNow.Name = "rbtnEmailNotNow"
        Me.rbtnEmailNotNow.Size = New System.Drawing.Size(79, 20)
        Me.rbtnEmailNotNow.TabIndex = 1
        Me.rbtnEmailNotNow.Text = "Not Now"
        Me.rbtnEmailNotNow.UseVisualStyleBackColor = True
        '
        'rbtnEmailYes
        '
        Me.rbtnEmailYes.AutoSize = True
        Me.rbtnEmailYes.Checked = True
        Me.rbtnEmailYes.Location = New System.Drawing.Point(9, 20)
        Me.rbtnEmailYes.Name = "rbtnEmailYes"
        Me.rbtnEmailYes.Size = New System.Drawing.Size(52, 20)
        Me.rbtnEmailYes.TabIndex = 0
        Me.rbtnEmailYes.TabStop = True
        Me.rbtnEmailYes.Text = "Yes"
        Me.rbtnEmailYes.UseVisualStyleBackColor = True
        '
        'gboxInvRecord
        '
        Me.gboxInvRecord.Controls.Add(Me.rbtnDiscard)
        Me.gboxInvRecord.Controls.Add(Me.rbtnRecInv)
        Me.gboxInvRecord.Location = New System.Drawing.Point(7, 89)
        Me.gboxInvRecord.Name = "gboxInvRecord"
        Me.gboxInvRecord.Size = New System.Drawing.Size(194, 76)
        Me.gboxInvRecord.TabIndex = 1
        Me.gboxInvRecord.TabStop = False
        Me.gboxInvRecord.Text = "Record Invoice?"
        '
        'rbtnDiscard
        '
        Me.rbtnDiscard.AutoSize = True
        Me.rbtnDiscard.Location = New System.Drawing.Point(87, 20)
        Me.rbtnDiscard.Name = "rbtnDiscard"
        Me.rbtnDiscard.Size = New System.Drawing.Size(101, 36)
        Me.rbtnDiscard.TabIndex = 1
        Me.rbtnDiscard.Text = "Discard and" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Restart"
        Me.rbtnDiscard.UseVisualStyleBackColor = True
        '
        'rbtnRecInv
        '
        Me.rbtnRecInv.AutoSize = True
        Me.rbtnRecInv.Checked = True
        Me.rbtnRecInv.Location = New System.Drawing.Point(6, 20)
        Me.rbtnRecInv.Name = "rbtnRecInv"
        Me.rbtnRecInv.Size = New System.Drawing.Size(73, 36)
        Me.rbtnRecInv.TabIndex = 0
        Me.rbtnRecInv.TabStop = True
        Me.rbtnRecInv.Text = "Record" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoice"
        Me.rbtnRecInv.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(7, 23)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(194, 60)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "Select actions for this invoice. Processes will be completed after this panel is " &
    "closed."
        '
        'txtJTvPDFRprtType
        '
        Me.txtJTvPDFRprtType.Location = New System.Drawing.Point(714, 713)
        Me.txtJTvPDFRprtType.Name = "txtJTvPDFRprtType"
        Me.txtJTvPDFRprtType.ReadOnly = True
        Me.txtJTvPDFRprtType.Size = New System.Drawing.Size(167, 22)
        Me.txtJTvPDFRprtType.TabIndex = 5
        Me.txtJTvPDFRprtType.Visible = False
        '
        'gboxInvReview
        '
        Me.gboxInvReview.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.gboxInvReview.Controls.Add(Me.rbtnReemailNever)
        Me.gboxInvReview.Controls.Add(Me.rbtnReemailNo)
        Me.gboxInvReview.Controls.Add(Me.rbtnRemailYes)
        Me.gboxInvReview.Controls.Add(Me.InvRevEmailLabel)
        Me.gboxInvReview.Controls.Add(Me.txtEmailComment)
        Me.gboxInvReview.Location = New System.Drawing.Point(18, 202)
        Me.gboxInvReview.Name = "gboxInvReview"
        Me.gboxInvReview.Size = New System.Drawing.Size(208, 169)
        Me.gboxInvReview.TabIndex = 6
        Me.gboxInvReview.TabStop = False
        Me.gboxInvReview.Text = "Invoice Review Options"
        Me.gboxInvReview.Visible = False
        '
        'rbtnReemailNever
        '
        Me.rbtnReemailNever.AutoSize = True
        Me.rbtnReemailNever.Location = New System.Drawing.Point(132, 136)
        Me.rbtnReemailNever.Name = "rbtnReemailNever"
        Me.rbtnReemailNever.Size = New System.Drawing.Size(65, 20)
        Me.rbtnReemailNever.TabIndex = 4
        Me.rbtnReemailNever.Text = "Never"
        Me.rbtnReemailNever.UseVisualStyleBackColor = True
        '
        'rbtnReemailNo
        '
        Me.rbtnReemailNo.AutoSize = True
        Me.rbtnReemailNo.Checked = True
        Me.rbtnReemailNo.Location = New System.Drawing.Point(76, 136)
        Me.rbtnReemailNo.Name = "rbtnReemailNo"
        Me.rbtnReemailNo.Size = New System.Drawing.Size(46, 20)
        Me.rbtnReemailNo.TabIndex = 3
        Me.rbtnReemailNo.TabStop = True
        Me.rbtnReemailNo.Text = "No"
        Me.rbtnReemailNo.UseVisualStyleBackColor = True
        '
        'rbtnRemailYes
        '
        Me.rbtnRemailYes.AutoSize = True
        Me.rbtnRemailYes.Location = New System.Drawing.Point(17, 136)
        Me.rbtnRemailYes.Name = "rbtnRemailYes"
        Me.rbtnRemailYes.Size = New System.Drawing.Size(52, 20)
        Me.rbtnRemailYes.TabIndex = 2
        Me.rbtnRemailYes.Text = "Yes"
        Me.rbtnRemailYes.UseVisualStyleBackColor = True
        '
        'InvRevEmailLabel
        '
        Me.InvRevEmailLabel.AutoSize = True
        Me.InvRevEmailLabel.BackColor = System.Drawing.SystemColors.Control
        Me.InvRevEmailLabel.Location = New System.Drawing.Point(25, 93)
        Me.InvRevEmailLabel.Name = "InvRevEmailLabel"
        Me.InvRevEmailLabel.Size = New System.Drawing.Size(141, 32)
        Me.InvRevEmailLabel.TabIndex = 1
        Me.InvRevEmailLabel.Text = "Do you to want Email " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(re-Email) this invoice?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtEmailComment
        '
        Me.txtEmailComment.Location = New System.Drawing.Point(10, 24)
        Me.txtEmailComment.Multiline = True
        Me.txtEmailComment.Name = "txtEmailComment"
        Me.txtEmailComment.ReadOnly = True
        Me.txtEmailComment.Size = New System.Drawing.Size(184, 56)
        Me.txtEmailComment.TabIndex = 0
        '
        'txtEmailDate
        '
        Me.txtEmailDate.Location = New System.Drawing.Point(369, 602)
        Me.txtEmailDate.Name = "txtEmailDate"
        Me.txtEmailDate.Size = New System.Drawing.Size(142, 22)
        Me.txtEmailDate.TabIndex = 7
        Me.txtEmailDate.Visible = False
        '
        'txttmpInvNum
        '
        Me.txttmpInvNum.Location = New System.Drawing.Point(777, 602)
        Me.txttmpInvNum.Name = "txttmpInvNum"
        Me.txttmpInvNum.Size = New System.Drawing.Size(142, 22)
        Me.txttmpInvNum.TabIndex = 8
        Me.txttmpInvNum.Visible = False
        '
        'txttmpJobID
        '
        Me.txttmpJobID.Location = New System.Drawing.Point(587, 602)
        Me.txttmpJobID.Name = "txttmpJobID"
        Me.txttmpJobID.Size = New System.Drawing.Size(142, 22)
        Me.txttmpJobID.TabIndex = 9
        Me.txttmpJobID.Visible = False
        '
        'txtTmpInvDate
        '
        Me.txtTmpInvDate.Location = New System.Drawing.Point(954, 602)
        Me.txtTmpInvDate.Name = "txtTmpInvDate"
        Me.txtTmpInvDate.Size = New System.Drawing.Size(142, 22)
        Me.txtTmpInvDate.TabIndex = 10
        Me.txtTmpInvDate.Visible = False
        '
        'JTvPDF
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1430, 751)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtTmpInvDate)
        Me.Controls.Add(Me.txttmpJobID)
        Me.Controls.Add(Me.txttmpInvNum)
        Me.Controls.Add(Me.txtEmailDate)
        Me.Controls.Add(Me.gboxInvReview)
        Me.Controls.Add(Me.txtJTvPDFRprtType)
        Me.Controls.Add(Me.gboxInvSettings)
        Me.Controls.Add(Me.txtfilename)
        Me.Controls.Add(Me.txttmpSubJobID)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.AxAcroPDF1)
        Me.Name = "JTvPDF"
        Me.Text = "Job Information Manager - Invoice Archive"
        Me.TopMost = True
        CType(Me.AxAcroPDF1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxInvSettings.ResumeLayout(False)
        Me.gboxInvSettings.PerformLayout()
        Me.gboxInvEmail.ResumeLayout(False)
        Me.gboxInvEmail.PerformLayout()
        Me.gboxInvRecord.ResumeLayout(False)
        Me.gboxInvRecord.PerformLayout()
        Me.gboxInvReview.ResumeLayout(False)
        Me.gboxInvReview.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents AxAcroPDF1 As AxAcroPDFLib.AxAcroPDF
    Friend WithEvents btnExit As Button
    Friend WithEvents txttmpSubJobID As TextBox
    Friend WithEvents txtfilename As TextBox
    Friend WithEvents gboxInvSettings As GroupBox
    Friend WithEvents gboxInvEmail As GroupBox
    Friend WithEvents rbtnEmailNever As RadioButton
    Friend WithEvents rbtnEmailNotNow As RadioButton
    Friend WithEvents rbtnEmailYes As RadioButton
    Friend WithEvents gboxInvRecord As GroupBox
    Friend WithEvents rbtnDiscard As RadioButton
    Friend WithEvents rbtnRecInv As RadioButton
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents txtJTvPDFRprtType As TextBox
    Friend WithEvents gboxInvReview As GroupBox
    Friend WithEvents rbtnReemailNo As RadioButton
    Friend WithEvents rbtnRemailYes As RadioButton
    Friend WithEvents InvRevEmailLabel As Label
    Friend WithEvents txtEmailComment As TextBox
    Friend WithEvents txtEmailDate As TextBox
    Friend WithEvents rbtnReemailNever As RadioButton
    Friend WithEvents txttmpInvNum As TextBox
    Friend WithEvents txttmpJobID As TextBox
    Friend WithEvents txtTmpInvDate As TextBox
End Class
