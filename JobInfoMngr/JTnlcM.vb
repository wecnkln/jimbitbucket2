﻿Public Class JTnlcM
    Dim slJTnlcM As New SortedList
    Public Structure slJTnlcMStruct
        Dim RecNum As String
        Dim KeyLetter As String
        Dim Title As String
        Dim Count As Decimal
        Dim Total As Decimal
        Dim Age0_30 As Decimal
        Dim Age31_60 As Decimal
        Dim Age61_90 As Decimal
        Dim AgeOver90 As Decimal
    End Structure

    Sub JTnlcMinit()
        slJTnlcM.Clear()
        lvJTnlcM.Items.Clear()
        ' ------------------------------------------------------
        ' Load skeleton records.
        ' ------------------------------------------------------
        Dim tmpItem As slJTnlcMStruct = Nothing
        '
        tmpItem.RecNum = "1"
        tmpItem.KeyLetter = "M"
        tmpItem.Title = "Material"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        tmpItem.RecNum = "2"
        tmpItem.KeyLetter = "S"
        tmpItem.Title = "Services"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        tmpItem.RecNum = "3"
        tmpItem.KeyLetter = "ADJ"
        tmpItem.Title = "Adjustments"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        tmpItem.RecNum = "4"
        tmpItem.KeyLetter = "IS"
        tmpItem.Title = "InStock"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        tmpItem.RecNum = "5"
        tmpItem.KeyLetter = "IH"
        tmpItem.Title = "InHouse"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        tmpItem.RecNum = "6"
        tmpItem.KeyLetter = "TOTL"
        tmpItem.Title = "Total"
        slJTnlcM.Add(tmpItem.RecNum, tmpItem)
        '
        Dim tmpSub As Integer = 0
        Dim tmpnlcItem As JTnlc.JTnlcStructure = Nothing
        Do While JTnlc.JTnlcReadslNLC(tmpSub, tmpnlcItem) = "DATA"
            '        MsgBox("tmpSub - >" & tmpSub & "<" & vbCrLf &
            '              "Invoice # - >" & tmpnlcItem.CustInvNumber & "<" & vbCrLf &
            '             "MorS - >" & tmpnlcItem.MorS & "< - - - JTnlcM~58")
            If tmpnlcItem.CustInvNumber = "" Then
                If tmpnlcItem.MorS <> "NJR" And tmpnlcItem.MorS <> "" Then
                    Dim tmpTodayJulDate As String = Gregarian2Julian(Date.Today)
                    Dim tmpInvDate As DateTime = CDate(tmpnlcItem.InvDate)
                    Dim tmpInvDateJul = Gregarian2Julian(tmpInvDate)
                    Dim tmpTodayJulDay As Integer = Val(tmpTodayJulDate.Substring(4, 3))
                    Dim tmpInvJulDay As Integer = Val(tmpInvDateJul.Substring(4, 3))
                    Dim tmpTransAmt As Decimal = Val(tmpnlcItem.TBInvd)
                    If tmpTodayJulDate.Substring(0, 4) > tmpInvDateJul.Substring(0, 4) Then
                        tmpInvJulDay += 365
                    End If
                    Dim tmpInvAge As Integer = tmpTodayJulDay - tmpInvJulDay
                    Dim tmpOver90 As Decimal = 0
                    Dim tmp61_90 As Decimal = 0
                    Dim tmp31_60 As Decimal = 0
                    Dim tmp0_30 As Decimal = 0
                    If tmpInvAge > 90 Then
                        tmpOver90 += Val(tmpnlcItem.TBInvd)
                    End If
                    If tmpInvAge > 60 And tmpInvAge < 91 Then
                        tmp61_90 += Val(tmpnlcItem.TBInvd)
                    End If
                    If tmpInvAge > 30 And tmpInvAge < 61 Then
                        tmp31_60 += Val(tmpnlcItem.TBInvd)
                    End If
                    If tmpInvAge < 31 Then
                        tmp0_30 += Val(tmpnlcItem.TBInvd)
                    End If
                    Dim key As ICollection = slJTnlcM.Keys
                    Dim k As String = Nothing
                    For Each k In key
                        If slJTnlcM(k).keyLetter = tmpnlcItem.MorS Then
                            tmpItem = slJTnlcM(k)
                            slJTnlcM.Remove(k)
                            Exit For
                        End If
                    Next
                    If tmpItem.KeyLetter <> tmpnlcItem.MorS Then
                        MsgBox("Didn't find key in slJTnlcM" & vbCrLf &
                              "NLC Key - >" & tmpnlcItem.MorS & "< -- JTnlcM~127")
                    End If
                    tmpItem.Count += 1
                    tmpItem.Total += Val(tmpnlcItem.TBInvd)
                    tmpItem.Age0_30 += tmp0_30
                    tmpItem.Age31_60 += tmp31_60
                    tmpItem.Age61_90 += tmp61_90
                    tmpItem.AgeOver90 += tmpOver90
                    slJTnlcM.Add(tmpItem.RecNum, tmpItem)
                    '
                    key = slJTnlcM.Keys
                    tmpItem = slJTnlcM(key(5))
                    slJTnlcM.Remove(key(5))
                    tmpItem.Count += 1
                    tmpItem.Total += Val(tmpnlcItem.TBInvd)
                    tmpItem.Age0_30 += tmp0_30
                    tmpItem.Age31_60 += tmp31_60
                    tmpItem.Age61_90 += tmp61_90
                    tmpItem.AgeOver90 += tmpOver90
                    slJTnlcM.Add(tmpItem.RecNum, tmpItem)
                End If
            End If
            tmpSub += 1     ' Set index to get next item.
        Loop
        '
        Dim itmBuild(7) As String
        Dim lvLine As New ListViewItem
        '
        Dim Key2 As ICollection = slJTnlcM.Keys
        Dim k2 As String = Nothing
        For Each k2 In Key2
            itmBuild(0) = slJTnlcM(k2).Title
            itmBuild(1) = String.Format("{0:#}", slJTnlcM(k2).Count)
            itmBuild(2) = String.Format("{0:#,###}", slJTnlcM(k2).Total)
            itmBuild(3) = String.Format("{0:#,###}", slJTnlcM(k2).Age0_30)
            itmBuild(4) = String.Format("{0:#,###}", slJTnlcM(k2).Age31_60)
            itmBuild(5) = String.Format("{0:#,###}", slJTnlcM(k2).Age61_90)
            itmBuild(6) = String.Format("{0:#,###}", slJTnlcM(k2).AgeOver90)
            lvLine = New ListViewItem(itmBuild)
            lvJTnlcM.Items.Add(lvLine)
            '
        Next
        '
        Me.MdiParent = JTstart
        Me.Show()
        '
    End Sub

    Private Sub btnJTnlcMExit_Click(sender As Object, e As EventArgs) Handles btnJTnlcMExit.Click
        JTnlc.JTnlcArrayFlush("")
        JTMainMenu.JTMMFirstTimeThru = "FIRST"
        LogFileWrite("JTnlcM", "JTnlcM Closed - JTnlc.JTnlcArrayFlush("") successful")
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub

    Private Sub btnJTnlcMInvEntMan_Click(sender As Object, e As EventArgs) Handles btnJTnlcMInvEntMan.Click
        Me.Close()
        JTnlcE.JtnlcEInit("MANUAL", "")
    End Sub

    Private Sub btnJTnlcMInvEntEDI_Click(sender As Object, e As EventArgs) Handles btnJTnlcMInvEntEDI.Click
        Me.Close()
        JTedi.JTediDispPDF()
    End Sub
    Public Sub JTnlcMBounceBack()
        JTedi.JTediDispPDF()
    End Sub

    Private Sub btnJTnlcMRevMod_Click(sender As Object, e As EventArgs) Handles btnJTnlcMRevMod.Click
        JTstart.JTstartMenuOff()
        JTnlc.MdiParent = JTstart
        JTnlc.JTnlcInit()
        Me.Close()
    End Sub

    Private Sub btnJTnlcMInStk_Click(sender As Object, e As EventArgs) Handles btnJTnlcMInStk.Click
        JTis.JTisMaintMode = True
        JTis.JTisNLCRequest("", "", "")
        Me.Close()
    End Sub

    Private Sub btnJTnlcMRecon_Click(sender As Object, e As EventArgs) Handles btnJTnlcMRecon.Click
        JTstart.JTstartHideTitles()
        JTstart.JTstartMenuOff()
        JTrec.MdiParent = JTstart
        'Set initial JTrec lv sequence
        JTrec.rbtnJTrecVJD.Checked = True
        JTrec.JTrecInit()
        Me.Close()
    End Sub
    ''' <summary>
    ''' Sub called from delete functions in JTedi to delete files. Working on problem of file
    ''' being open by another function.
    ''' </summary>
    ''' <param name="tmpFileToDelete"></param>
    Public Sub JTnlcMDelFile(tmpFileToDelete As String)
        Try
            My.Computer.FileSystem.DeleteFile(tmpFileToDelete)
            MsgBox("File Deleted - " & vbCrLf &
              tmpFileToDelete)
        Catch
            MsgBox("File Deleted Failed - " & vbCrLf &
             tmpFileToDelete)
        End Try
        JTedi.JTediTopInvNum = 0
        JTedi.JTediFillInvImages()
        Me.Close()
    End Sub
End Class