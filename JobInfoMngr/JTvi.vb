﻿
Imports System.IO
Public Class JTvi

    Public Structure JTviArrayStructure

        Dim JTviShortName As String
        Dim JTviVendName As String
        Dim JTviStreet As String
        Dim JTviCityStateZip As String
        Dim JTviEstDate As String
        Dim JTviLstActDate As String
        Dim JTviVendType As String
        Dim JTviActivityInterface As String
        Dim JTviContName1 As String
        Dim JTviContTel1 As String
        Dim JTviContEmail1 As String
        Dim JTviContName2 As String
        Dim JTviContTel2 As String
        Dim JTviContEmail2 As String
        Dim JTviContName3 As String
        Dim JTviContTel3 As String
        Dim JTviContEmail3 As String
        Dim JTviLicCopy As String
        Dim JTviLicExpDate As String
        Dim JTviInsCopy As String
        Dim JTviInsExpDate As String
        Dim JTvi1099 As String
        ' New fields added for reconciliation and payment interface.
        Dim JTviScanning As Boolean
        '
        Dim JTviAcctNum As String
        Dim JTviPymtTerms As String
        Dim JTviDiscPercent As String
        '
        Dim JTviReconSuggest As String
    End Structure

    Public JTviNewItem As New JTviArrayStructure

    '
    Public slJTvi As SortedList = New SortedList
    '
    ' This field is populated when the JTvi panel is started to know where to go back to.
    Public JTviReturnPanel As String = ""
    ' -----------------------------------------------------------------------
    ' Following two elements control events related to checkedchanges event
    ' in JTvi panel. These controls are programatically changed that seemed to 
    ' queue the execution of the underlying Sub. Using these tags (False - meaning
    ' first time through ... exit sub; True - subsequent time through ... allow 
    ' processing to contine) the execution of the sib's code is controlled.
    ' ---------------------------------------------------------------------------
    '
    ''' <summary>Sub loads the xml vend info file into slJTvi sorted list.</summary>
    Public Sub JTviLoad()
        ' ------------------------------------------------------
        ' Code to load vi data from file into slJTvi.
        ' ------------------------------------------------------
        Dim tmpJIarray As Integer = 0
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        Dim tmpSaveYrWk As String = ""
        Dim tmpSaveEmpID As String = ""
        '
        Dim JTviReadFileKey As String = ""
        Dim JTviReadFileData As String = ""
        '
        slJTvi.Clear()

        '
        Dim tmpRecordType As String = ""

        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTVendInfo.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTVendInfo.Bkup") Then
            MsgBox("JTVendInfo --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (VI.004)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTVendInfo.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTVendInfo.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTVendInfo.Bkup", "JTVendInfo.dat")
        End If
        ' ------------------------------------------------------------------

        '
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTviReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTviReadFileKey = "viRecord" Or JTviReadFileKey = "/viRecord" Or
                        JTviReadFileKey = "VendInfo" Or JTviReadFileKey = "/VendInfo" Then
                        JTviReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTviReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case JTviReadFileKey
                        '
                        Case "ShortName"
                            JTviNewItem.JTviShortName = JTviReadFileData.Trim()

                        Case "VendName"
                            JTviNewItem.JTviVendName = JTviReadFileData

                        Case "Street"
                            JTviNewItem.JTviStreet = JTviReadFileData
                        Case "CityStateZip"
                            JTviNewItem.JTviCityStateZip = JTviReadFileData
                        Case "EstDate"
                            JTviNewItem.JTviEstDate = String.Format("{0:MM/dd/yyyy}", CDate(JTviReadFileData))
                        Case "LstActDate"
                            JTviNewItem.JTviLstActDate = JTviReadFileData
                        Case "VendType"
                            JTviNewItem.JTviVendType = JTviReadFileData
                        Case "ActivityInterface"
                            JTviNewItem.JTviActivityInterface = JTviReadFileData
                        Case "ContName1"
                            JTviNewItem.JTviContName1 = JTviReadFileData
                        Case "ContTel1"
                            JTviNewItem.JTviContTel1 = JTviReadFileData
                        Case "ContEmail1"
                            JTviNewItem.JTviContEmail1 = JTviReadFileData
                        Case "ContName2"
                            JTviNewItem.JTviContName2 = JTviReadFileData
                        Case "ContTel2"
                            JTviNewItem.JTviContTel2 = JTviReadFileData
                        Case "ContEmail2"
                            JTviNewItem.JTviContEmail2 = JTviReadFileData
                        Case "ContName3"
                            JTviNewItem.JTviContName3 = JTviReadFileData
                        Case "ContTel3"
                            JTviNewItem.JTviContTel3 = JTviReadFileData
                        Case "ContEmail3"
                            JTviNewItem.JTviContEmail3 = JTviReadFileData
                        Case "LicCopy"
                            JTviNewItem.JTviLicCopy = JTviReadFileData
                        Case "LicExpDate"
                            JTviNewItem.JTviLicExpDate = JTviReadFileData
                        Case "InsCopy"
                            JTviNewItem.JTviInsCopy = JTviReadFileData
                        Case "InsExpDate"
                            JTviNewItem.JTviInsExpDate = JTviReadFileData
                        Case "1099"
                            JTviNewItem.JTvi1099 = JTviReadFileData
                        Case "Scanning"
                            JTviNewItem.JTviScanning = JTviReadFileData
                        Case "AcctNum"
                            JTviNewItem.JTviAcctNum = JTviReadFileData
                        Case "PymtTerms"
                            JTviNewItem.JTviPymtTerms = JTviReadFileData
                        Case "DiscPercent"
                            JTviNewItem.JTviDiscPercent = JTviReadFileData
                        Case "ReconSuggest"
                            JTviNewItem.JTviReconSuggest = JTviReadFileData
                        Case "/viRecord"
                            Dim slKey As String = JTviNewItem.JTviShortName
                            If JTviNewItem.JTviVendType = "STMT" Then
                                slKey = slKey & "_STMT"
                            End If
                            If slJTvi.ContainsKey(slKey) Then
                                slJTvi.Remove(slKey)
                            End If
                            slJTvi.Add(slKey, JTviNewItem)
                            '
                            ' clear fields that might not have a value in the next record.
                            '
                            JTviNewItem = Nothing
                            '
                    End Select
                    '
                    '
                End If
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        '
    End Sub
    '
    ''' <summary>This sub takes the slJTvi sorted list and saved it in XML format to disk.</summary>
    Public Sub JTviFlush()
        ' ------------------------------------------------------
        ' Code to unload slJTvi data to the JTVendInfo.dat file.
        ' ------------------------------------------------------
        Dim fileRecord As String = ""
        '
        '
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTVendInfo.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTVendInfo.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            '
            fileRecord = "<VendInfo>"
            writeFile.WriteLine(fileRecord)
            ' Get a collection of the keys. 
            Dim key As ICollection = slJTvi.Keys
            Dim k As String
            For Each k In key
                '
                fileRecord = "<viRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<ShortName>" & slJTvi(k).JTviShortName & "</ShortName>"
                writeFile.WriteLine(fileRecord)
                '

                If slJTvi(k).JTviVendName <> "" Then
                    fileRecord = "<VendName>" & slJTvi(k).JTviVendName & "</VendName>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviStreet <> "" Then
                    fileRecord = "<Street>" & slJTvi(k).JTviStreet & "</Street>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviCityStateZip <> "" Then
                    fileRecord = "<CityStateZip>" & slJTvi(k).JTviCityStateZip & "</CityStateZip>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviEstDate <> "" Then
                    fileRecord = "<EstDate>" & slJTvi(k).JTviEstDate & "</EstDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviLstActDate <> "" Then
                    fileRecord = "<LstActDate>" & slJTvi(k).JTviLstActDate & "</LstActDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviVendType <> "" Then
                    fileRecord = "<VendType>" & slJTvi(k).JTviVendType & "</VendType>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviActivityInterface <> "" Then
                    fileRecord = "<ActivityInterface>" & slJTvi(k).JTviActivityInterface & "</ActivityInterface>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContName1 <> "" Then
                    fileRecord = "<ContName1>" & slJTvi(k).JTviContName1 & "</ContName1>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContTel1 <> "" Then
                    fileRecord = "<ContTel1>" & slJTvi(k).JTviContTel1 & "</ContTel1>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContEmail1 <> "" Then
                    fileRecord = "<ContEmail1>" & slJTvi(k).JTviContEmail1 & "</ContEmail1>"
                    writeFile.WriteLine(fileRecord)
                End If

                If slJTvi(k).JTviContName2 <> "" Then
                    fileRecord = "<ContName2>" & slJTvi(k).JTviContName2 & "</ContName2>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContTel2 <> "" Then
                    fileRecord = "<ContTel2>" & slJTvi(k).JTviContTel2 & "</ContTel2>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContEmail2 <> "" Then
                    fileRecord = "<ContEmail2>" & slJTvi(k).JTviContEmail2 & "</ContEmail2>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContName3 <> "" Then
                    fileRecord = "<ContName3>" & slJTvi(k).JTviContName3 & "</ContName3>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContTel3 <> "" Then
                    fileRecord = "<ContTel3>" & slJTvi(k).JTviContTel3 & "</ContTel3>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviContEmail3 <> "" Then
                    fileRecord = "<ContEmail3>" & slJTvi(k).JTviContEmail3 & "</ContEmail3>"
                    writeFile.WriteLine(fileRecord)
                End If
                If slJTvi(k).JTviLicCopy <> "" Then
                    fileRecord = "<LicCopy>" & slJTvi(k).JTviLicCopy & "</LicCopy>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviLicExpDate <> "" Then
                    fileRecord = "<LicExpDate>" & slJTvi(k).JTviLicExpDate & "</LicExpDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviInsCopy <> "" Then
                    fileRecord = "<InsCopy>" & slJTvi(k).JTviInsCopy & "</InsCopy>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviInsExpDate <> "" Then
                    fileRecord = "<InsExpDate>" & slJTvi(k).JTviInsExpDate & "</InsExpDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTvi1099 <> "" Then
                    fileRecord = "<1099>" & slJTvi(k).JTvi1099 & "</1099>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviAcctNum <> "" Then
                    fileRecord = "<AcctNum>" & slJTvi(k).JTviAcctNum & "</AcctNum>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviPymtTerms <> "" Then
                    fileRecord = "<PymtTerms>" & slJTvi(k).JTviPymtTerms & "</PymtTerms>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTvi(k).JTviDiscPercent <> "" Then
                    fileRecord = "<DiscPercent>" & slJTvi(k).JTviDiscPercent & "</DiscPercent>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "<Scanning>" & slJTvi(k).JTviScanning & "</DiscScanning>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<ReconSuggest>" & slJTvi(k).JTviReconSuggest & "</ReconSuggest>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "</viRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            fileRecord = "</VendInfo>"
            writeFile.WriteLine(fileRecord)
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try

        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTVendInfo.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTVendInfo.Bkup")
        End If
        ' ------------------------------------------------------------------------


    End Sub
    '
    Public Sub JTviUpdateEntry(tmpShortName As String, tmpVendType As String)
        JTviPopPanel(tmpShortName, tmpVendType)
        Me.Show()
    End Sub
    Public Function JTviPopPanel(tmpShortName As String, tmpVendType As String) As String
        ' ---------------------------------------------------------
        ' Code to populate fields on the JTvi panel from a specific
        ' record of the slJTvi table. Before populating screen, sub
        ' checked to make sure this is not a new vendor.
        ' ---------------------------------------------------------
        If tmpShortName <> "" Then

            txtJTviShortName.Text = tmpShortName
            txtJTviShortName.ReadOnly = True
            If tmpVendType = "STMT" Then
                tmpShortName = tmpShortName & "_STMT"
            End If
        Else
            txtJTviShortName.Text = ""
            txtJTviShortName.ReadOnly = False
            txtJTviShortName.Select()
        End If
        '
        If slJTvi.ContainsKey(tmpShortName) = False Then
            txtJTviVendName.Text = ""
            txtJTviStreet.Text = ""
            txtJTviCityStateZip.Text = ""
            txtJTviEstDate.Text = Date.Today
            txtJTviLstActDate.Text = ""
            Me.gboxJTviSerProvDoc.Visible = True
            rbtnJTviMat.Checked = False
            rbtnJTviSer.Checked = False
            rbtnJTviBoth.Checked = False
            rbtnJTviNJR.Checked = False
            rbtnJTviStmt.Checked = False

            '
            rbtnJTviReconcile.Checked = False
            If tmpVendType = "STMT" Then
                rbtnJTviReconcile.Enabled = False
                '
            End If
            rbtnJTviExport.Enabled = True
            rbtnJTviManual.Enabled = True
            rbtnJTviExport.Checked = False
            rbtnJTviManual.Checked = False
            '
            txtJTviContName1.Text = ""
            txtJTviContTel1.Text = ""
            txtJTviContEmail1.Text = ""
            txtJTviContName2.Text = ""
            txtJTviContTel2.Text = ""
            txtJTviContEmail2.Text = ""
            txtJTviContName3.Text = ""
            txtJTviContTel3.Text = ""
            txtJTviContEmail3.Text = ""
            cboxJTviLicCopy.Checked = False
            mtxtJTviLicExpDate.Text = ""
            cboxJTviInsCopy.Checked = False
            mtxtJTviInsExpDate.Text = ""
            cboxJTvi1099.Checked = False
        Else
            Dim k As String = tmpShortName
            txtJTviVendName.Text = slJTvi(k).JTviVendName
            txtJTviStreet.Text = slJTvi(k).JTviStreet
            txtJTviCityStateZip.Text = slJTvi(k).JTviCityStateZip
            txtJTviEstDate.Text = slJTvi(k).JTviEstDate
            txtJTviLstActDate.Text = slJTvi(k).JTviLstActDate
            Me.gboxJTviSerProvDoc.Visible = False
            rbtnJTviStmt.Enabled = True
            rbtnJTviMat.Enabled = True
            rbtnJTviSer.Enabled = True
            rbtnJTviBoth.Enabled = True
            rbtnJTviNJR.Enabled = True
            Select Case slJTvi(k).JTviVendType
                Case "M"
                    rbtnJTviMat.Checked = True
                Case "S"
                    rbtnJTviSer.Checked = True
                    Me.gboxJTviSerProvDoc.Visible = True
                Case "B"
                    rbtnJTviBoth.Checked = True
                    Me.gboxJTviSerProvDoc.Visible = True
                Case "NJR"
                    rbtnJTviNJR.Checked = True
                Case "STMT"
                    rbtnJTviStmt.Checked = True
                    rbtnJTviMat.Enabled = False
                    rbtnJTviSer.Enabled = False
                    rbtnJTviBoth.Enabled = False
                    rbtnJTviNJR.Enabled = False
            End Select
            '
            rbtnJTviReconcile.Enabled = True
            rbtnJTviExport.Enabled = True
            rbtnJTviManual.Enabled = True
            Select Case slJTvi(k).JTviActivityInterface
                Case "R"
                    rbtnJTviReconcile.Checked = True
                    gboxJTviPaymntTerms.Visible = True
                Case "E"
                    rbtnJTviExport.Checked = True
                    gboxJTviPaymntTerms.Visible = True
                Case "M"
                    rbtnJTviManual.Checked = True
            End Select
            If tmpVendType = "STMT" Then
                rbtnJTviReconcile.Enabled = False
            End If
            '
            txtJTviContName1.Text = slJTvi(k).JTviContName1
            txtJTviContTel1.Text = slJTvi(k).JTviContTel1
            txtJTviContEmail1.Text = slJTvi(k).JTviContEmail1
            txtJTviContName2.Text = slJTvi(k).JTviContName2
            txtJTviContTel2.Text = slJTvi(k).JTviContTel2
            txtJTviContEmail2.Text = slJTvi(k).JTviContEmail2
            txtJTviContName3.Text = slJTvi(k).JTviContName3
            txtJTviContTel3.Text = slJTvi(k).JTviContTel3
            txtJTviContEmail3.Text = slJTvi(k).JTviContEmail3
            '
            txtJTviAcctNum.Text = slJTvi(k).JTviAcctNum
            txtJTviDiscPercent.Text = slJTvi(k).JTviDiscPercent
            '
            Select Case slJTvi(k).JTviPymtTerms
                Case "DUR"
                    rbtnJTviDUR.Checked = True
                Case "10Days"
                    rbtnJTvi10Days.Checked = True
                Case "30Days"
                    rbtnJTvi30Days.Checked = True
                Case "EOM"
                    rbtnJTviEOM.Checked = True
                Case "Manual"
                    rbtnJTviManEntry.Checked = True
            End Select

            ' 
            If slJTvi(k).JTviLicCopy = "X" Then
                cboxJTviLicCopy.Checked = True
            End If
            mtxtJTviLicExpDate.Text = slJTvi(k).JTviLicExpDate
            If slJTvi(k).JTviInsCopy = "X" Then
                cboxJTviInsCopy.Checked = True
            End If
            mtxtJTviInsExpDate.Text = slJTvi(k).JTviInsExpDate
            If slJTvi(k).JTvi1099 = "X" Then
                cboxJTvi1099.Checked = True
            End If
            Return "PANELFILLED"
        End If
        '         Next
        '    End If
        Return "NEWITEM"
    End Function
    '
    Public Sub JTviUpdtSL()
        ' ---------------------------------------------------------
        ' Code to update slJTvi fields from data entered or updated
        ' on the JTvi panel.
        ' ---------------------------------------------------------
        JTviNewItem.JTviShortName = txtJTviShortName.Text
        JTviNewItem.JTviVendName = txtJTviVendName.Text
        JTviNewItem.JTviStreet = txtJTviStreet.Text
        JTviNewItem.JTviCityStateZip = txtJTviCityStateZip.Text
        JTviNewItem.JTviEstDate = txtJTviEstDate.Text
        JTviNewItem.JTviLstActDate = txtJTviLstActDate.Text
        If rbtnJTviMat.Checked = True Then
            JTviNewItem.JTviVendType = "M"
        End If
        If rbtnJTviSer.Checked = True Then
            JTviNewItem.JTviVendType = "S"
        End If
        If rbtnJTviBoth.Checked = True Then
            JTviNewItem.JTviVendType = "B"
        End If
        If rbtnJTviStmt.Checked = True Then
            JTviNewItem.JTviVendType = "STMT"
        End If
        If rbtnJTviNJR.Checked = True Then
            JTviNewItem.JTviVendType = "NJR"
        End If
        '
        If rbtnJTviReconcile.Checked = True Then
            JTviNewItem.JTviActivityInterface = "R"
            If JTnlc.JTnlcChkForNonRecActiveItems(txtJTviShortName.Text) = True Then
                Dim tmpAnswer2 As Integer = MsgBox("Items exist for this vendor not categorized for Reconciliation." & vbCrLf &
                                                   "Would you like to convert these items so they can be reconciled?",
                                                    MsgBoxStyle.YesNo, "J.I.M. - Vendor Information")
                If tmpAnswer2 = DialogResult.Yes Then
                    JTnlc.JTnlcMakeReconcilable(txtJTviShortName.Text)
                    JTnlc.JTnlcArrayFlush("")
                End If
            End If
        End If
        If rbtnJTviExport.Checked = True Then
            JTviNewItem.JTviActivityInterface = "E"
        End If
        If rbtnJTviManual.Checked = True Then
            JTviNewItem.JTviActivityInterface = "M"
        End If
        '
        JTviNewItem.JTviContName1 = txtJTviContName1.Text
        JTviNewItem.JTviContTel1 = txtJTviContTel1.Text
        JTviNewItem.JTviContEmail1 = txtJTviContEmail1.Text
        JTviNewItem.JTviContName2 = txtJTviContName2.Text
        JTviNewItem.JTviContTel2 = txtJTviContTel2.Text
        JTviNewItem.JTviContEmail2 = txtJTviContEmail2.Text
        JTviNewItem.JTviContName3 = txtJTviContName3.Text
        JTviNewItem.JTviContTel2 = txtJTviContTel3.Text
        JTviNewItem.JTviContEmail3 = txtJTviContEmail3.Text
        If cboxJTviLicCopy.Checked = True Then
            JTviNewItem.JTviLicCopy = "X"
        End If
        If mtxtJTviLicExpDate.Text <> "  /  /" Then
            JTviNewItem.JTviLicExpDate = mtxtJTviLicExpDate.Text
        Else
            JTviNewItem.JTviLicExpDate = ""
        End If
        If cboxJTviInsCopy.Checked = True Then
            JTviNewItem.JTviInsCopy = "X"
        End If
        If mtxtJTviInsExpDate.Text <> "  /  /" Then
            JTviNewItem.JTviInsExpDate = mtxtJTviInsExpDate.Text
        Else
            JTviNewItem.JTviInsExpDate = ""
        End If
        If cboxJTvi1099.Checked = True Then
            JTviNewItem.JTvi1099 = "X"
        End If
        ' New Fields 3/25/2019 --- to suppport STMT, payment interface, Reconciliation, Etc.
        JTviNewItem.JTviScanning = cboxJTviScanOpt.Checked
        JTviNewItem.JTviAcctNum = txtJTviAcctNum.Text
        JTviNewItem.JTviDiscPercent = txtJTviDiscPercent.Text
        ' clear fields after update
        cboxJTviScanOpt.Checked = False
        txtJTviAcctNum.Text = ""
        txtJTviDiscPercent.Text = ""
        '
        If rbtnJTviDUR.Checked = True Then
            JTviNewItem.JTviPymtTerms = "DUR"
        End If
        If rbtnJTvi10Days.Checked = True Then
            JTviNewItem.JTviPymtTerms = "10Days"
        End If
        If rbtnJTvi30Days.Checked = True Then
            JTviNewItem.JTviPymtTerms = "30Days"
        End If
        If rbtnJTviEOM.Checked = True Then
            JTviNewItem.JTviPymtTerms = "EOM"
        End If
        If rbtnJTviManEntry.Checked = True Then
            JTviNewItem.JTviPymtTerms = "Manual"
        End If
        '
        Dim tmpKey As String = JTviNewItem.JTviShortName
        If JTviNewItem.JTviVendType = "STMT" Then
            tmpKey = tmpKey & "_STMT"
        End If
        If slJTvi.ContainsKey(tmpKey) Then
            slJTvi.Remove(tmpKey)
        End If
        slJTvi.Add(tmpKey, JTviNewItem)
    End Sub

    Private Sub btnJTviCancel_Click(sender As Object, e As EventArgs) Handles btnJTviCancel.Click
        ' ------------------------------------------------
        ' Cancel button hit ... window closed.
        ' ------------------------------------------------
        Me.Hide()
        Select Case JTviReturnPanel
            Case "NLC"    'return to NLC panel
                JTviReturnPanel = ""
                Exit Sub
            Case "VIS", "REC"   'return to VIS or JTrec panel (already visible)
                JTviReturnPanel = ""
                Exit Sub
            Case "REC"
                JTviReturnPanel = ""
                Me.Hide()
                JTrec.Show()
                Exit Sub
        End Select
        JTMainMenu.JTMMinit()              'return to MM
        '
    End Sub

    Private Sub btnJTviDelete_Click(sender As Object, e As EventArgs) Handles btnJTviDelete.Click
        ' ------------------------------------------------------------
        ' Sub to delete vendor. Called when Delete button is hit.
        ' ------------------------------------------------------------
        '
        ' Code to make sure a STMT record is not delated while there is still
        ' a normal account record categorized as a reconciled ('R') account. 
        If Me.rbtnJTviStmt.Checked = True Then
            If slJTvi.ContainsKey(Me.txtJTviShortName.Text.Trim) = True Then
                If slJTvi(Me.txtJTviShortName.Text).JTviActivityInterface = "R" Then
                    MsgBox("WARNING ---" & vbCrLf &
                           "A request has been submitted to delete a Vendor STMT account." & vbCrLf &
                           "This vendor still has a record to process invoices" & vbCrLf &
                           "categorized as a 'Reconciled Account'. This category" & vbCrLf &
                           "must be changed or the non-statement record must be" & vbCrLf &
                           "deleted prior to deleting the STMT record.")
                    Exit Sub
                End If
            End If
        End If
        '
        Dim tmpAnswer As Integer = MsgBox("Vendor " & Me.txtJTviShortName.Text & " is about to be deleted." & vbCrLf &
                                          "This action cannot be undone. Do you want to continue?",
                       MsgBoxStyle.YesNo, "J.I.M. - Vendor Information")
        If tmpAnswer = DialogResult.Yes Then
            If slJTvi.ContainsKey(Me.txtJTviShortName.Text) Or
                slJTvi.ContainsKey(txtJTviShortName.Text & "_STMT") Then
                Dim tmpviKey As String = Me.txtJTviShortName.Text
                If slJTvi.ContainsKey(Me.txtJTviShortName.Text & "_STMT") Then
                    tmpviKey = Me.txtJTviShortName.Text & "_STMT"
                End If
                slJTvi.Remove(tmpviKey)
                JTviFlush()
            End If
            Me.Hide()
            Select Case JTviReturnPanel
                Case "NLC"
                    JTviReturnPanel = ""
                    Exit Sub
                Case "VIS"
                    JTviReturnPanel = ""
                    JTvis.Hide()
                    Dim tmpEntryType As String = JTvis.JTvisNLCEntrySave
                    JTvis.JTvisInit(tmpEntryType)
                    Exit Sub
                Case "REC"
                    JTrec.JTrecFillShortNameAutoTable()
                    Me.Hide()
                    JTrec.Show()
                    Exit Sub
            End Select
            JTMainMenu.JTMMinit()
            '
        End If
    End Sub

    Private Sub btnJTviUpdtSave_Click(sender As Object, e As EventArgs) Handles btnJTviUpdtSave.Click
        ' ------------------------------------------------------------
        ' Sub called when Add/Update hit...updates slJTvi and also
        ' flushes to disk file.
        ' ------------------------------------------------------------
        '
        ' ===================================================================
        ' Add editing logic for panel fields................
        ' ===================================================================
        '
        ' Edit to checks fields for stmt and export to QB options
        If rbtnJTviExport.Checked = True Or rbtnJTviStmt.Checked = True Then
            Dim tmpErrorFlag As Boolean = True
            If txtJTviVendName.Text = Nothing Then
                tmpErrorFlag = False
            End If
            If txtJTviStreet.Text = Nothing Then
                tmpErrorFlag = False
            End If
            If txtJTviCityStateZip.Text = Nothing Then
                tmpErrorFlag = False
            End If
            If tmpErrorFlag = False Then
                MsgBox("For Vendor or Vendor Statements being exported to accounting," & vbCrLf &
                       "a full name and complete address is required. If an account" & vbCrLf &
                       "number for this vendor is available, that should be entered" & vbCrLf &
                       "also. (VI.005)")
                txtJTviVendName.Select()
                Exit Sub
            End If
        End If
        '

        ' Validate dates
        If mtxtJTviLicExpDate.Text <> "  /  /" Then
            If IsDate(mtxtJTviLicExpDate.Text) = False Then
                MsgBox("Enter Valid License Expiration Date. (VI.001)")
                mtxtJTviLicExpDate.Select()
                Exit Sub
            End If
        End If
        If mtxtJTviInsExpDate.Text <> "  /  /" Then
            If IsDate(mtxtJTviInsExpDate.Text) = False Then
                MsgBox("Enter Valid Insurance Expiration Date. (VI.001)")
                mtxtJTviInsExpDate.Select()
                Exit Sub
            End If
        End If
        JTviUpdtSL()
        Me.Hide()
        JTviFlush()
        Dim tmpNLCtype As String = "M"
        '    If JTnlc.radiobtnJTnlcSub.Checked = True Then
        '   tmpNLCtype = "S"
        '  End If
        If JTviReturnPanel <> "MM" Then
            '           JTnlc.JTnlcBldAutoCompleteTables(tmpNLCtype)
        Else
            JTMainMenu.JTMMinit()
            Exit Sub
        End If
        '
        Select Case JTviReturnPanel
            Case "NLC"
                JTviReturnPanel = ""
                Exit Sub
            Case "VIS"
                JTvis.JTvisInit(tmpNLCtype)
                Exit Sub
            Case "REC"
                JTviReturnPanel = ""
                JTrec.JTrecFillShortNameAutoTable()
                Me.Hide()
                JTrec.Show()
                Exit Sub
        End Select
        '
    End Sub
    Public Function JTviUpdtActDate(VendShortName As String, ActivityDate As String, ByRef PayMeth As String) As String
        ' ----------------------------------------------------------------
        ' Update Activity Date ... queued by NLC Activity.
        ' ----------------------------------------------------------------
        Dim keys As ICollection = slJTvi.Keys
        Dim k As String = ""


        For Each k In keys
            If slJTvi(k).JTviShortName = VendShortName Then
                JTviNewItem = slJTvi.Item(k)
                slJTvi.Remove(VendShortName)
                JTviNewItem.JTviLstActDate = ActivityDate
                PayMeth = JTviNewItem.JTviActivityInterface
                slJTvi.Add(VendShortName, JTviNewItem)
                JTviFlush()
                Return "DONE"
            End If
        Next
        MsgBox("ShortName key doesn't seem to exist - >" & VendShortName & "<" & vbCrLf &
               "JTviUpdtActDate - called from JTnlcUpdtLineItem")
        '
        Return "DONE"
    End Function
    Public Function JTviShortNames(ByRef tmpSub As Integer, ByRef tmpShortName As String, ByRef tmpVendType As String) As String
        Dim keys As ICollection = slJTvi.Keys
        Dim k As String = ""
        If tmpSub < keys.Count Then
            tmpShortName = slJTvi(keys(tmpSub)).JTviShortName
            tmpVendType = slJTvi(keys(tmpSub)).JTviVendType
            Return "DATA"
        Else
            Return "DONE"
        End If
    End Function
    Public Function JTviLkupShortName(ByRef tmpSupplier As String, ByRef tmpMorS As String, ByRef tmpLic As String,
                                      ByRef tmpLicExpired As String, ByRef tmpIns As String,
                                      ByRef tmpInsExpired As String) As String
        Dim keys As ICollection = slJTvi.Keys
        Dim k As String = ""
        If slJTvi.ContainsKey(tmpSupplier) = False Then
            Return "INVALID"
        Else
            For Each k In keys
                If slJTvi(k).JTviShortName = tmpSupplier Then
                    tmpMorS = slJTvi(k).JTviVendType
                    If tmpMorS = "S" Or tmpMorS = "B" Then
                        tmpLic = slJTvi(k).JTviLicCopy
                        If slJTvi(k).JTviLicExpDate = "" Then
                            tmpLicExpired = "X"
                        Else
                            If CDate(slJTvi(k).JTviLicExpDate) < Date.Today Then
                                tmpLicExpired = "X"
                            End If
                        End If
                        tmpIns = slJTvi(k).JTviInsCopy
                        If slJTvi(k).JTviInsExpDate = "" Then
                            tmpInsExpired = "X"
                        Else
                            If CDate(slJTvi(k).JTviInsExpDate) < Date.Today Then
                                tmpInsExpired = "X"
                            End If
                        End If
                    End If
                End If
            Next
        End If
        Return "VALID"
    End Function
    Public Function JTviSupplierRecapExtract(ByRef tmpSupplierCnt As Integer, ByRef tmpSubConCnt As Integer,
                                         ByRef tmpOutofCompCnt As Integer) As String
        Dim key As ICollection = slJTvi.Keys
        Dim k As String = ""
        '
        For Each k In key
            If slJTvi(k).JTviVendType = "M" Then
                tmpSupplierCnt += 1
            End If
            If slJTvi(k).JTviVendType = "S" Then
                tmpSubConCnt += 1
                If slJTvi(k).JTviLicCopy = "" Or slJTvi(k).JTviInsCopy = "" Then
                    tmpOutofCompCnt += 1
                End If
                If slJTvi(k).JTviLicExpDate = "" Then
                    tmpOutofCompCnt += 1
                ElseIf Date.Today < CDate(slJTvi(k).JTviLicExpDate) Then
                    tmpOutofCompCnt += 1
                End If
                If slJTvi(k).JTviInsExpDate = "" Then
                    tmpOutofCompCnt += 1
                ElseIf Date.Today < CDate(slJTvi(k).JTviInsExpDate) Then
                    tmpOutofCompCnt += 1
                End If
            End If
        Next
        '
        Return ""
        '
    End Function
    Public Function JTviReadJTviforMM(tmpSub As Integer, ByRef ShortName As String, ByRef VendName As String,
                                      ByRef VendType As String,
                                      ByRef LstActDate As String, ByRef ActivityInterface As String,
                                      ByRef Issues As String) As String
        Dim key As ICollection = slJTvi.Keys
        If tmpSub < key.Count Then
            ShortName = slJTvi(key(tmpSub)).JTviShortName
            VendName = slJTvi(key(tmpSub)).JTviVendName
            VendType = slJTvi(key(tmpSub)).JTviVendType
            LstActDate = slJTvi(key(tmpSub)).JTviLstActDate
            ActivityInterface = slJTvi(key(tmpSub)).JTviActivityInterface
            Issues = ""
            If VendType = "S" Or VendType = "B" Then
                If slJTvi(key(tmpSub)).JTviInsCopy = "" Then
                    Issues = "INSMiss"
                End If
                If slJTvi(key(tmpSub)).JTviLicCopy = "" Then
                    Issues = "LICMiss"
                End If
                If slJTvi(key(tmpSub)).JTviInsCopy <> "" Then
                    If slJTvi(key(tmpSub)).JTviInsExpDate = "" Then
                        Issues = "INSExp"
                    ElseIf Convert.ToDateTime(slJTvi(key(tmpSub)).JTviInsExpDate) < Date.Today _
                    Then
                        Issues = "INSExp"
                    End If
                End If

                If slJTvi(key(tmpSub)).JTviLicCopy <> "" Then
                    If slJTvi(key(tmpSub)).JTviLicExpDate = "" Then
                        Issues = "LicExp"
                    ElseIf Convert.ToDateTime(slJTvi(key(tmpSub)).JTviLicExpDate) < Date.Today _
                    Then
                        Issues = "LicExp"
                    End If
                End If
            End If
            Return "DATA"
        End If
        '
        Return "EMPTY"
    End Function

    Private Sub txtJTviContEmail1_LostFocus(sender As Object, e As EventArgs) Handles txtJTviContEmail1.LostFocus
        If txtJTviContEmail1.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTviContEmail1.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Delete or correct and resubmit.(VI.002)", txtJTviContEmail1, 35, -55, 7000)
                txtJTviContEmail1.Select()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtJTviContEmail2_LostFocus(sender As Object, e As EventArgs) Handles txtJTviContEmail2.LostFocus
        If txtJTviContEmail2.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTviContEmail2.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Delete or correct and resubmit.(VI.002)", txtJTviContEmail2, 35, -55, 7000)
                txtJTviContEmail2.Select()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtJTviContEmail3_LostFocus(sender As Object, e As EventArgs) Handles txtJTviContEmail3.LostFocus
        If txtJTviContEmail3.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTviContEmail3.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Delete or correct and resubmit.(VI.002)", txtJTviContEmail3, 35, -55, 7000)
                txtJTviContEmail3.Select()
                Exit Sub
            End If
        End If
    End Sub
    ''' <summary>This sub displays the JTvi panel when new vendors are being added.</summary>
    ''' <param name="tmpVendShrtName"></param>
    Public Sub JTviDisplayVendor(tmpVendShrtName As String)
        If tmpVendShrtName = "NewVendor" Then
            '      JTviGroupBox4_Ctrl = False
            '     gboxJTviInterfaceOpt_Ctrl = False
            ' set all job type options as available
            rbtnJTviMat.Enabled = False
            rbtnJTviSer.Enabled = False
            rbtnJTviBoth.Enabled = False
            rbtnJTviStmt.Enabled = False
            ' hide unused gboxes
            '    rbtnJTviMat.Checked = True
            GroupBox2.Visible = False
            GroupBox3.Visible = False
            GroupBox8.Visible = False
            gboxJTviInterfaceOpt.Visible = False
            gboxJTviSerProvDoc.Visible = False
            gboxJTviPaymntTerms.Visible = False
            gboxJTviScanOpt.Visible = False
            '
            txtJTviShortName.Text = ""
            txtJTviShortName.ReadOnly = False
            txtJTviShortName.Focus()
            ' 
            btnJTviDelete.Visible = False
            btnJTviUpdtSave.Visible = False
            Me.Show()
        End If
    End Sub

    Private Sub txtJTviShortName_TextChanged(sender As Object, e As EventArgs) Handles txtJTviShortName.LostFocus
        If txtJTviShortName.Text = "" Then
            txtJTviShortName.Select()
            Exit Sub
        End If
        txtJTviShortName.Text = txtJTviShortName.Text.ToUpper
        If slJTvi.ContainsKey(txtJTviShortName.Text) = False And
           slJTvi.ContainsKey(txtJTviShortName.Text & "_STMT") = False Then
            '
            rbtnJTviStmt.Enabled = True
            rbtnJTviMat.Enabled = True
            rbtnJTviSer.Enabled = True
            rbtnJTviBoth.Enabled = True
            rbtnJTviNJR.Enabled = True
            rbtnJTviStmt.Checked = False
            rbtnJTviMat.Checked = False
            rbtnJTviSer.Checked = False
            rbtnJTviBoth.Checked = False
            rbtnJTviNJR.Checked = False
            txtJTviShortName.ReadOnly = True
            txtJTviEstDate.Text = Date.Today
            GroupBox8.Visible = True
            gboxJTviInterfaceOpt.Visible = False
            Exit Sub
        Else
            If slJTvi.ContainsKey(txtJTviShortName.Text & "_STMT") = True Then
                rbtnJTviStmt.Enabled = False
                rbtnJTviMat.Enabled = False
                rbtnJTviSer.Enabled = False
                rbtnJTviBoth.Enabled = False
                rbtnJTviStmt.Checked = True
                '       rbtnJTviStmt.Enabled = True
                '
                rbtnJTviManual.Enabled = False
                rbtnJTviExport.Enabled = False
                rbtnJTviReconcile.Checked = True
                Exit Sub
            End If
            If slJTvi.ContainsKey(txtJTviShortName.Text) = True Then
                If slJTvi(txtJTviShortName.Text).JTviActivityInterface = "R" Then
                    rbtnJTviStmt.Enabled = True
                    rbtnJTviMat.Enabled = False
                    rbtnJTviSer.Enabled = False
                    rbtnJTviBoth.Enabled = False
                    '
                    rbtnJTviStmt.Checked = True
                    ' --------------------------------------------------------
                    ' Following code not working. Trying to hide radiobuttons.
                    ' --------------------------------------------------------
                    rbtnJTviReconcile.Enabled = False

                    rbtnJTviManual.Enabled = True
                    rbtnJTviExport.Enabled = True
                    rbtnJTviExport.Checked = True
                    '
                    JTviPopPanel(txtJTviShortName.Text, "STMT")

                    Exit Sub
                Else
                    ToolTip1.Show("Vendor Short Name exists as a non-reconciled vendor." & vbCrLf &
                                  "Statement cannot be added with duplicate short name unless" & vbCrLf &
                                  "it is a reconciled vendor. Delete or correct and resubmit.(VI.002)",
                                  txtJTviShortName, 35, -55, 7000)
                    txtJTviShortName.Select()

                End If
            End If
        End If
    End Sub

    Private Sub rbtnJTviMat_CheckedChanged(sender As Object, e As EventArgs) _
        Handles rbtnJTviMat.CheckedChanged, rbtnJTviSer.CheckedChanged, rbtnJTviBoth.CheckedChanged, rbtnJTviStmt.CheckedChanged
        gboxJTviSerProvDoc.Visible = False
        gboxJTviInterfaceOpt.Visible = True
        If JTVarMaint.JTgvScanNLC = True Then
            gboxJTviScanOpt.Visible = True
        End If
        If rbtnJTviSer.Checked = True Or rbtnJTviBoth.Checked = True Then
            gboxJTviSerProvDoc.Visible = True
        End If
        '
        ' Logic to duplicate vendor name and address if STMT account being created has an already existing
        ' M or S account entry. Can be overriden if necessary.
        '
        If rbtnJTviStmt.Checked = True Then
            If slJTvi.ContainsKey(txtJTviShortName.Text) = True Then
                If slJTvi.ContainsKey(txtJTviShortName.Text & "_STMT") = False Then
                    txtJTviVendName.Text = slJTvi(txtJTviShortName.Text).JTviVendName
                    txtJTviStreet.Text = slJTvi(txtJTviShortName.Text).JTviStreet
                    txtJTviCityStateZip.Text = slJTvi(txtJTviShortName.Text).JTviCityStateZip
                End If
            End If
        End If
    End Sub

    Private Sub rbtnJTviReconcile_CheckedChanged(sender As Object, e As EventArgs) _
        Handles rbtnJTviReconcile.CheckedChanged, rbtnJTviExport.CheckedChanged, rbtnJTviManual.CheckedChanged
        '      If gboxJTviInterfaceOpt_Ctrl = False Then
        '     gboxJTviInterfaceOpt_Ctrl = True
        '    Exit Sub
        '   End If
        If rbtnJTviExport.Checked = True Then
            '      rbtnJTviManEntry.Checked = True
            gboxJTviPaymntTerms.Visible = True
        Else
            gboxJTviPaymntTerms.Visible = False
        End If
        GroupBox2.Visible = True
        GroupBox3.Visible = True
        btnJTviDelete.Visible = True
        btnJTviUpdtSave.Visible = True
    End Sub
    ''' <summary>Function written to return the entire vi sl record back. Written specifically for JTrec. If vendor short name found ... "DATA" returned ...  else "NOTFOUND" returned.</summary>
    ''' <param name="tmpVendShrtName">Vendor short name ... calling sub must add "_STMT" if a statement short name is being searched for.</param>
    ''' <param name="tmpJTviRec">All info stored for this vendor in the slJTvi.</param>
    Public Function JTviRetrieveVendInfo(tmpVendShrtName As String,
                                         ByRef tmpJTviRec As JTviArrayStructure) As String
        If slJTvi.ContainsKey(tmpVendShrtName) = False Then
            Return "NOTFOUND"
        End If
        tmpJTviRec = slJTvi(tmpVendShrtName)
        Return "DATA"
    End Function
    ''' <summary>
    ''' Read vi record in sequence and return entire record to calling sub or function.
    ''' </summary>
    ''' <param name="tmpSub"></param>
    ''' <param name="tmpJTviRec"></param>
    ''' <returns></returns>
    Public Function JTviReadVendInfo(ByRef tmpSub As Integer, ByRef tmpJTviRec As JTviArrayStructure) As String
        Dim keys As ICollection = slJTvi.Keys
        '
        If tmpSub < keys.Count Then
            tmpJTviRec = slJTvi(keys(tmpSub))
            tmpSub += 1
            Return "DATA"
        End If
        '
        Return "END"
    End Function
    ''' <summary>
    ''' Function to update and look up recon suggestion for each item being reconciled.
    ''' If mode = "UPDATE" --- add current stmtshortname to JTvireconsuggest
    ''' If mode = "LOOKUP"(or anything else) --- return TRUE if this VendShortName's Recon Suggestion = StmtShortName
    ''' </summary>
    ''' <param name="tmpmode">Function currently being request ... UPDATE or LOOKUP</param>
    ''' <param name="tmpVendShortName">name of item being reconciled</param>
    ''' <param name="tmpStmtShortName">name of stmt items are included in</param>
    ''' <returns></returns>
    Public Function JTviReconSuggest(tmpMode As String, ByRef tmpVendShortName As String, ByRef tmpStmtShortName As String) As Boolean
        If tmpMode = "UPDATE" Then
            ' update stmtshortname in JTviReconSuggest for this VendShortName
            If slJTvi.ContainsKey(tmpVendShortName) Then
                Dim tmpslItem As JTviArrayStructure
                tmpslItem = slJTvi(tmpVendShortName)
                tmpslItem.JTviReconSuggest = tmpStmtShortName
                slJTvi.Remove(tmpVendShortName)
                slJTvi.Add(tmpVendShortName, tmpslItem)
                Return True
            End If
        Else
            ' lookup mode ... used for passing data back to JTrec to check appropriate lines based on rec history
            ' get VendShortName ... if JTviReconSuggest =  StmtShortName = return True
            If slJTvi.ContainsKey(tmpVendShortName) Then
                If slJTvi(tmpVendShortName).JTviReconSuggest = tmpStmtShortName Then
                    Return True
                End If
            End If
        End If
        '
        Return False
    End Function
    ''' <summary>
    ''' Function to find vendor short name from the vendor account number.
    ''' Function used by EDI functions within NLC.
    ''' </summary>
    ''' <param name="tmpAcctNum">account number to look up</param>
    ''' <returns>Vendor short name --- if account number found. "NOTFOUND" if acct number did not match existing files.</returns>
    Public Function JTviFindVendAcctNum(tmpAcctNum As String) As String
        Dim key As ICollection = slJTvi.Keys
        Dim k As String = Nothing
        For Each k In key
            If slJTvi(k).JTviAcctNum = tmpAcctNum Then
                Return slJTvi(k).JTviShortName
            End If
        Next
        Return "NOTFOUND"
    End Function
End Class