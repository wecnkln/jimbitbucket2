﻿''' <summary>
''' Class to display long error message and/or explanations for certain events. The panel
''' will be called from certain other points in the program. The calling points will determine
''' the text to be displayed. 
''' 
''' That text will be stored in the JTEHinit Sub. This may change to a
''' file somtime in the future to make maintenance easier.
''' 
''' When the panel is exited, the program will be right where it left off.
''' </summary>
Public Class JTErrHandle
    ''' <summary>
    ''' Logic to determine the text to populate the JTErrHandle panel and display panel.
    ''' </summary>
    ''' <param name="tmpCalledFrom">The is argument passed to identify where sub was called from.</param>
    Public Sub JTEHinit(tmpCalledFrom As String)
        If tmpCalledFrom = "JTEmailSend" Then
            JTEHtext.Text =
               "Email server refused the connection request from Job Information Manager. " &
               "The most likely cause is an incorrect password (Utilities/Global Settings). " &
               "This will happen if the email password has been changed. When the email password is changed, " &
               "Google voids any other passwords for this account. To request and install a new password follow the steps listed below." &
               vbCrLf & vbCrLf &
               "    1. Go to gmail on computer" & vbCrLf &
               "    2. Hit 9 - dot image in upper right corner." & vbCrLf &
               "    3. Select -  go To your account" & vbCrLf &
               "    4. Once at your Google account … select security." & vbCrLf &
               "    5. Scroll down And select App passwords." & vbCrLf &
               "    6. Click Select App … select custom app … key in 'JobInfoMngr'" & vbCrLf &
               "    7. Hit Generate … the New key will be created. (16 character apha numeric key)" & vbCrLf &
               "    8. Copy that key so you can paste it into JIM" & vbCrLf &
               "    9. Back in JobInfoMngr, Go to Utilities/Global Settings" & vbCrLf &
               "   10. Paste the New key into the 'Google 2-step Validation code' field. JIM will automatically save the code." & vbCrLf &
               "   11. Email should be operational again."
        End If
        '
        Me.Show()


    End Sub
    '
    ''' <summary>
    ''' Logic to exit and close panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTEHexit_Click(sender As Object, e As EventArgs) Handles JTEHexit.Click
        Me.Hide()
        Me.Close()
    End Sub
End Class