﻿Public Class JTpr
    Public Structure JTprDataStructure
        Dim JTprDSID As String
        Dim JTprDSName As String
        Dim JTprDSPayCat As String
        Public JTprDStkHours As String
        Dim JTprDSprHours As String
        Dim JTprDSState1Name As String
        Dim JTprDSState1Hours As String
        Dim JTprDSState2Name As String
        Dim JTprDSState2Hours As String
        Dim JTprDSState3Name As String
        Dim JTprDSState3Hours As String
        Dim JTprDSPRID As String
        Dim JTprDSHrlyRate As String
        Dim JTprDSEmail As String
    End Structure
    '
    Public slJTpr As SortedList = New SortedList
    '
    ''' <summary>
    ''' This tag is used to check to see if the date on the panel has been changed.
    ''' If it hasn't not, the panel will not be refreshed.
    ''' </summary>
    Dim LstDateDisplayed As String = Nothing
    ''' <summary>
    ''' Entry point into payroll summary functions. This sub calculates last Saturday's date based on today's 
    ''' date and shows it on the panel as a suggestion. It also retrieves the data and builds the LV for the
    ''' panel.
    ''' </summary>
    Public Sub JTprInit()
        Dim tmpDate As Date = Today
        Dim tmpDayofWeek As Integer = tmpDate.DayOfWeek
        Dim tmpLastSatDate As Date
        If tmpDayofWeek <> 6 Then
            tmpLastSatDate = tmpDate.AddDays((tmpDayofWeek + 1) * -1)
        Else
            tmpLastSatDate = tmpDate
        End If

        mtxtJTprWeekEndDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)
        JTprLoadPanel()
        Me.Show()
    End Sub
    ''' <summary>
    ''' Function to fill payroll data on the JTpr panel. It is call when the JTpr functions
    ''' are originally called and any time the date is changed on the JTpr panel.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTprLoadPanel()

        '
        Dim tmpDate As Date = Date.ParseExact(mtxtJTprWeekEndDate.Text, "MM/dd/yyyy",
            System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim tmpDayofWeek As Integer = tmpDate.DayOfWeek
        Dim tmpLastSatDate As Date
        If tmpDayofWeek <> 6 Then
            tmpLastSatDate = tmpDate.AddDays((tmpDayofWeek + 1) * -1)
        Else
            tmpLastSatDate = tmpDate
        End If
        '
        mtxtJTprWeekEndDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)
        '
        Dim tmpFileSeq As Integer = 10
        Dim tmpYrWkNum As String
        Dim tmpWEDate As Date = tmpLastSatDate
        Dim tmpWeekNo As Integer
        tmpWeekNo = (tmpWEDate.DayOfYear / 7) + 1
        If tmpWeekNo < 10 Then
            tmpYrWkNum = (Str(tmpWEDate.Year) & "0" & Str(tmpWeekNo).Trim(" "))
        Else
            tmpYrWkNum = (Str(tmpWEDate.Year) & Str(tmpWeekNo).Trim(" "))
        End If
        '
        ' -------------------------------------------------------------------
        ' Retrieve and organize HR data into PR array
        ' -------------------------------------------------------------------
        Dim tmpData As New JTprDataStructure
        '
        Dim tmpPOS As Integer = 0
        Dim tmpID As String = Nothing
        Dim tmpName As String = Nothing
        Dim tmpLabCat As String = Nothing
        Dim tmpPayMeth As String = Nothing
        Dim tmpPRID As String = Nothing
        Dim tmpPayRate As String = Nothing
        Dim tmpEmail As String = Nothing
        Dim tmpTermDate As String = Nothing
        '
        Dim alEmpID As New ArrayList
        '      Dim EIASub As Integer
        '
        slJTpr.Clear()
        alEmpID.Clear()
        '
        Do While JThr.JThrNewReadFunc(tmpPOS, tmpID, tmpName, tmpLabCat, tmpPayMeth, tmpPRID, tmpPayRate, tmpEmail, tmpTermDate) = "DATA"
            '
            If tmpTermDate = "" Then
                tmpData.JTprDSID = tmpID
                tmpData.JTprDSName = tmpName
                tmpData.JTprDSPayCat = tmpPayMeth
                tmpData.JTprDStkHours = "0"
                tmpData.JTprDSprHours = "0"
                tmpData.JTprDSPRID = tmpPRID
                tmpData.JTprDSHrlyRate = tmpPayRate
                tmpData.JTprDSEmail = tmpEmail
                slJTpr.Add(tmpID, tmpData)
                alEmpID.Add(tmpID)
            End If
            tmpPOS += 1
        Loop
        '
        ' ------------------------------------------------------------------------------------
        ' Retrieve sum of TK data for each empID for PR array.
        ' ------------------------------------------------------------------------------------
        '
        Dim tmpHrsReturned As String = 0
        Dim tmpEmpID As String
        Dim tmpst1ID As String = Nothing
        Dim tmpst1hrs As String = Nothing
        Dim tmpst2ID As String = Nothing
        Dim tmpst2hrs As String = Nothing
        Dim tmpst3ID As String = Nothing
        Dim tmpst3hrs As String = Nothing
        '
        Dim alEmp As String
        For Each alEmp In alEmpID
            tmpEmpID = slJTpr(alEmp).JTprDSID
            '    MsgBox("JTpr~99  cjecking the user ID and week # sent" & vbCrLf &
            '       "Employee ID - >" & tmpEmpID & "<" & vbCrLf &
            '       "YrWkNum - >" & tmpYrWkNum & "<")
            tmpHrsReturned = JTtk.JTtkPRHours(tmpEmpID, tmpYrWkNum, tmpst1ID, tmpst1hrs, tmpst2ID, tmpst2hrs, tmpst3ID, tmpst3hrs)
            Dim tmpItem As JTprDataStructure = slJTpr(alEmp)
            tmpItem.JTprDStkHours = tmpHrsReturned
            tmpItem.JTprDSprHours = tmpHrsReturned
            tmpItem.JTprDSState1Name = tmpst1ID
            tmpItem.JTprDSState1Hours = tmpst1hrs
            tmpItem.JTprDSState2Name = tmpst2ID
            tmpItem.JTprDSState2Hours = tmpst2hrs
            tmpItem.JTprDSState3Name = tmpst3ID
            tmpItem.JTprDSState3Hours = tmpst3hrs
            '
            slJTpr.Remove(alEmp)
            slJTpr.Add(alEmp, tmpItem)
            '
        Next
        ' ----------------------------------------------------------------------------------
        ' Build lv from array values
        ' ----------------------------------------------------------------------------------
        ' 
        lvJTpr.Items.Clear()
        Dim TlW2Liab As Double = 0
        Dim Tl1099Liab As Double = 0
        Dim itmBuild(8) As String
        Dim lvLine As ListViewItem
        '
        Dim key As ICollection = slJTpr.Keys
        Dim k As String
        For Each k In key
            itmBuild(0) = slJTpr(k).JTprDSID
            itmBuild(1) = slJTpr(k).JTprDSName
            itmBuild(2) = slJTpr(k).JTprDSPayCat
            itmBuild(3) = String.Format("{0:#0.0}", Val(slJTpr(k).JTprDStkHours))
            itmBuild(4) = String.Format("{0:#0.0}", Val(slJTpr(k).JTprDSprHours))
            itmBuild(5) = slJTpr(k).JTprDSState1Name & "/" & String.Format("{0:#0.0}", Val(slJTpr(k).JTprDSState1Hours))
            itmBuild(6) = slJTpr(k).JTprDSState2Name & "/" & String.Format("{0:#0.0}", Val(slJTpr(k).JTprDSState2Hours))
            itmBuild(7) = slJTpr(k).JTprDSState3Name & "/" & String.Format("{0:#0.0}", Val(slJTpr(k).JTprDSState3Hours))
            If slJTpr(k).JTprDSPayCat = "W2" Then
                TlW2Liab = TlW2Liab + (Val(slJTpr(k).JTprDStkHours) * Val(slJTpr(k).JTprDShrlyrate))
            Else
                Tl1099Liab = Tl1099Liab + (Val(slJTpr(k).JTprDStkHours) * Val(slJTpr(k).JTprDShrlyrate))
            End If
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTpr.Items.Add(lvLine)
        Next
        txtW2PRLiab.Text = String.Format("{0:0.00}", TlW2Liab)
        txt1099PRLiab.Text = String.Format("{0:0.00}", Tl1099Liab)
        btnPREmail.Select()
        Return ""
    End Function


    ''' <summary>
    ''' Exit point for the payroll subsystem. Closes down p/r panel and transfer control to mainmenu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Hide()
        JTMainMenu.JTMMinit()
        Me.Close()
    End Sub
    ''' <summary>
    ''' Sub handles double click on p/r form listview. It is used to identify an item so it can be modified.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvJTpr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTpr.DoubleClick
        '
        Dim tmpIndex As Integer = lvJTpr.SelectedItems(0).Index
        '
        txtJTprHrAdj.Text = lvJTpr.SelectedItems(0).SubItems(4).Text
    End Sub
    ''' <summary>
    ''' Sub to update adjustment changes to a listview value.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTprHrAdj_TextChanged(sender As Object, e As EventArgs) Handles txtJTprHrAdj.TextChanged
        '
        If txtJTprHrAdj.Text <> "" Then
            lvJTpr.SelectedItems(0).SubItems(4).Text = txtJTprHrAdj.Text
        End If
    End Sub
    Private Sub txtJTprHrAdj_LostFocus(sender As Object, e As EventArgs) Handles txtJTprHrAdj.LostFocus
        ' -----------------------------------------------------------------------
        ' Sub to record the changes made to the lv p/r hours item.
        ' --- Values are changed in slJTpr sortedlist.
        ' -----------------------------------------------------------------------
        If lvJTpr.SelectedItems.Count = 0 Then
            Exit Sub
        End If
        Dim tmpItem As JTprDataStructure = slJTpr(lvJTpr.SelectedItems(0).Text)
        '
        Dim tmpHrChg As Double = Val(tmpItem.JTprDSprHours) - Val(txtJTprHrAdj.Text)
        tmpItem.JTprDSprHours = txtJTprHrAdj.Text
        If Val(tmpItem.JTprDSState1Hours) > Val(tmpItem.JTprDSState2Hours) Then
            tmpItem.JTprDSState1Hours = Str(Val(tmpItem.JTprDSState1Hours) - tmpHrChg)
        Else
            tmpItem.JTprDSState2Hours = Str(Val(tmpItem.JTprDSState2Hours) - tmpHrChg)
        End If
        '
        slJTpr(lvJTpr.SelectedItems(0).Text) = tmpItem
        txtJTprHrAdj.Text = ""
        If tmpItem.JTprDSPayCat = "W2" Then
            txtW2PRLiab.Text = Str(Val(txtW2PRLiab.Text) - (tmpHrChg * Val(tmpItem.JTprDSHrlyRate)))
        Else
            txt1099PRLiab.Text = Str(Val(txt1099PRLiab.Text) - (tmpHrChg * Val(tmpItem.JTprDSHrlyRate)))
        End If
        '
        btnPREmail.Select()
    End Sub
    ''' <summary>
    ''' Extract data from sl and format the email message to be sent to the payroll service.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnPREmail_Click(sender As Object, e As EventArgs) Handles btnPREmail.Click

        Dim tmpTextArea As String = ""
        Dim tmpFixedName As String = ""
        Dim tmpRegHrs As Double = 0
        Dim tmpOThrs As Double = 0
        '
        Dim key As ICollection = slJTpr.Keys
        Dim k As String
        For Each k In key
            tmpOThrs = 0
            If Val(slJTpr(k).JTprDSprHours) > 40 Then
                tmpRegHrs = 40
                tmpOThrs = Val(slJTpr(k).JTprDSprHours) - 40
            Else
                tmpRegHrs = Val(slJTpr(k).JTprDSprHours)
            End If
            If slJTpr(k).JTprDSPayCat = "W2" And slJTpr(k).JTprDSprHours <> "" Then
                Dim tmpst1 As String = slJTpr(k).JTprDSState1Name & " - " & String.Format("{0:00.0}", Val(slJTpr(k).JTprDSState1Hours))
                Dim tmpst2 As String = slJTpr(k).JTprDSState2Name & " - " & String.Format("{0:00.0}", Val(slJTpr(k).JTprDSState2Hours))
                Dim tmpst3 As String = slJTpr(k).JTprDSState3Name & " - " & String.Format("{0:00.0}", Val(slJTpr(k).JTprDSState3Hours))
                '
                Dim tmpFormat As String = "{0, 11} {1, 13} {2, 12} {3, 11} {4, 11}"
                Dim tmpEmpName As String = slJTpr(k).JTprDSName.padright(22, ".")
                '
                Dim tmpPrintLineName As String = tmpEmpName & vbTab
                '
                Dim tmpPrintLine As String = tmpPrintLineName & String.Format(tmpFormat, "Reg - " & String.Format("{0:00.0}", tmpRegHrs),
                                     "O/T - " & String.Format("{0:00.0}", tmpOThrs),
                                     tmpst1, tmpst2, tmpst3) & vbCrLf
                '
                tmpTextArea = tmpTextArea & tmpPrintLine
            End If
        Next
        Dim tmpEmailSub As String = JTVarMaint.CompanyNme & " - Payroll input for W/E " & mtxtJTprWeekEndDate.Text
        JTEmail.JTSndEmail(JTVarMaint.PayrollEmailAddressW2, "", "", JTVarMaint.JTFromEmailAddress,
                              tmpEmailSub, JTVarMaint.EmailMsgTextW2 & vbCrLf & tmpTextArea, "")
        Me.Close()
    End Sub
    ''' <summary>
    ''' sub to handle date changes on the JTpr panel. The date entered is checked for
    ''' validity and then converted to the w/e/ date of the date entered.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTprWeekEndDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTprWeekEndDate.LostFocus
        If TestForValidDate(mtxtJTprWeekEndDate.Text) = False Then
            mtxtJTprWeekEndDate.Text = ""
            mtxtJTprWeekEndDate.Select()
            Exit Sub
        End If
        If LstDateDisplayed = mtxtJTprWeekEndDate.Text Then
            Exit Sub
        End If
        ' -------------------------------------------------------------------------------
        ' The following code only executes if the date on the panel has been changed.
        ' -------------------------------------------------------------------------------
        LstDateDisplayed = mtxtJTprWeekEndDate.Text
        Dim tmpDate As Date = Date.ParseExact(mtxtJTprWeekEndDate.Text, "MM/dd/yyyy",
            System.Globalization.DateTimeFormatInfo.InvariantInfo)
        Dim tmpDayofWeek As Integer = tmpDate.DayOfWeek
        Dim tmpLastSatDate As Date
        If tmpDayofWeek <> 6 Then
            tmpLastSatDate = tmpDate.AddDays((tmpDayofWeek + 1) * -1)
        Else
            tmpLastSatDate = tmpDate
        End If
        '
        mtxtJTprWeekEndDate.Text = String.Format("{0:MM/dd/yyyy}", tmpLastSatDate)
        '
        Dim tmpDone As String = ""
        Dim tmpFileSeq As Integer = 10
        Dim tmpYrWkNum As String = ""
        Dim tmpWEDate As Date = Date.ParseExact(mtxtJTprWeekEndDate.Text, "MM/dd/yyyy",
            System.Globalization.DateTimeFormatInfo.InvariantInfo)
        '
        JTprLoadPanel()        'reload data on panel reflecting date entered
    End Sub

End Class