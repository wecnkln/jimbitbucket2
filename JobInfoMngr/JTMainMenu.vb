﻿Imports System.IO
Public Class JTMainMenu
    Public Structure JTlaborcats
        Dim JTlcKey As String
        Dim JTlcName As String
        Dim JTlcStanRate As String
        Dim JTlcShopRate As String
    End Structure
    '
    ' ---------------------------------------
    ' Array to support emp/timekeeping LV
    ' Key = EmpID
    ' ---------------------------------------
    Public Structure JTMMEmpTKStructure
        Dim JTMMEmpID As String
        Dim JTMMLstTK As String
    End Structure

    Dim JTMMEmpTK As SortedList = New SortedList
    '
    ' ------------------------------------------------
    ' Array to support job selection
    ' Key = Tl of t-b-inv'd activity + 1000000 & JobID & SubID
    ' (Intent is to have jobs with highest unbilled on top.
    ' ------------------------------------------------
    Public Structure JTMMJobActStructure
        Dim JTMMjaJobID As String
        Dim JTMMjaSubID As String
        Dim JTMMjaLab As Double
        Dim JTMMjaMat As Double
        Dim JTMMjaSub As Double
        Dim JTMMjaJobType As String
        Dim JTMMjaCstMeth As String
        Dim JTMMjaLstInvDt As String
        Dim JTMMjaTotlInvd As String
        Dim JTMMjaAUPQueued As String
        Dim JTMMjaAUPQuoted As String
    End Structure
    '
    Dim JTMMJobAct As SortedList = New SortedList
    '

    Dim JTMMjaarea As New JTMMJobActStructure
    Dim JTMMEmpTkArea As New JTMMEmpTKStructure
    '
    Public JTMMFirstTimeThru As String = "FIRST"
    '
    Public Sub JTMMinit()
        ' ----------------------------------------------------------------------------
        'Sub to prepare JTMainMenu for presentation.
        ' ----------------------------------------------------------------------------
        '
        ' Load data into 'lvMMJobs' listview. 
        '
        '
        '
        ' Logic to clear items from ListView.
        '


        If lvMMJobs.Items.Count > 0 Then
            lvMMJobs.Items.Clear()
            lvMMAUP.Items.Clear()
            lvMMCstP.Items.Clear()
        End If

        Dim tmpJTPOS As Integer = 0
        Dim tmpJTJobName As String = ""
        Dim tmpJTSubname As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpPayMeth As String = ""
        '
        If JTMMFirstTimeThru = "FIRST" Then
            JTMMJobAct.Clear()

            Do While JTjim.JTjimReadFunc(tmpJTPOS, tmpJTJobName, tmpJTSubname, tmpDeactDate, tmpJobType, tmpPayMeth) = "DATA"
                JTMMjaarea.JTMMjaJobID = tmpJTJobName
                JTMMjaarea.JTMMjaSubID = tmpJTSubname
                JTMMjaarea.JTMMjaJobType = tmpJobType
                JTMMjaarea.JTMMjaCstMeth = tmpPayMeth
                If tmpDeactDate <> "" And cboxJTmmShowDeact.Checked = True Then
                    ' plug with value to mark as deactivated
                    JTMMjaarea.JTMMjaLab = 99999999
                    Dim tmpKey As String
                    If JTVarMaint.JTgvMMJobSeq = "J" Then

                        tmpKey = JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
                    Else

                        tmpKey = Str(2000000) & JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
                    End If

                    JTMMJobAct.Add(tmpKey, JTMMjaarea)
                    tmpJTPOS += 1
                End If
                If tmpDeactDate <> "" And cboxJTmmShowDeact.Checked = False Then
                    tmpJTPOS += 1
                End If
                If tmpDeactDate = "" Then

                    JTMMjaarea.JTMMjaLab = 0
                    JTMMjaarea.JTMMjaMat = 0
                    JTMMjaarea.JTMMjaSub = 0
                    JTMMjaarea.JTMMjaAUPQueued = ""
                    JTMMjaarea.JTMMjaAUPQuoted = ""

                    Dim tmpKey As String
                    If JTVarMaint.JTgvMMJobSeq = "J" Then

                        tmpKey = JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
                    Else

                        tmpKey = Str(1999999 - Int(JTMMjaarea.JTMMjaLab + JTMMjaarea.JTMMjaMat + JTMMjaarea.JTMMjaSub)) _
                            & JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
                    End If
                    '
                    JTMMJobAct.Add(tmpKey, JTMMjaarea)
                    tmpJTPOS += 1
                End If
                tmpJobType = ""
            Loop
            '
            ' Insert calls here for the initial load of JRInvHist & JTAUP. The code is located
            ' here to make sure the JTMMJobAct sl has been populated with job prior to these calls.
            ' The original location of these calls was JTstart ... DBStart_buttonclick.
            '
            ' -------------------------------------------
            ' Load AUP Records
            ' -------------------------------------------
            JTaup.JTaupLoad()
            ' Run the following function at initial AUP file load. It is possible ... if there was a system crash ...
            ' that a (or some) "Deposit Reversal" entries got left on the file. This will remove them.
            JTaup.JTaupRemoveDanglingDepositReversals()
            '
            ' -------------------------------------------
            ' Load Invoice History
            ' -------------------------------------------
            JTas.JTasHistLoad()

            Dim tmpJTEmpID As String = ""
            Dim tmpJTEmpName As String = ""
            Dim tmpJTLabCat As String = ""
            Dim tmpJTPayMeth As String = ""
            Dim tmpJTTermDate As String = ""

            tmpJTPOS = 0
            '
            JTMMEmpTK.Clear()

            Do While JThr.JThrReadFunc(tmpJTPOS, tmpJTEmpID, tmpJTEmpName, tmpJTLabCat, tmpJTPayMeth, tmpJTTermDate) = "DATA"

                If tmpJTTermDate = "" Then
                    JTMMEmpTkArea.JTMMEmpID = tmpJTEmpID
                    JTMMEmpTkArea.JTMMLstTK = ""
                    Dim tmpKEY As String = JTMMEmpTkArea.JTMMEmpID
                    JTMMEmpTK.Add(tmpKEY, JTMMEmpTkArea)
                End If
                tmpJTPOS += 1
            Loop
            JTMMFirstTimeThru = "InitialLoadDone"

            JTtk.JTtkRestoreTKFile()

            JTnlc.JTnlcArrayLoad()

        End If
        Dim itmBuild(5) As String
        Dim lvLine As ListViewItem
        '
        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String
        For Each k In key

            itmBuild(0) = JTMMJobAct(k).JTMMjaJobID
            ' -----------------------------------------------------
            ' code being added to concatinate Job~Subjob on display
            '
            ' Additional code must be added
            ' - to fill two new panels
            ' - To decide what panels to display or hide
            ' - To break job~sub apart so that other code can access it
            '
            ' -----------------------------------------------------
            If JTMMJobAct(k).JTMMjaSubID <> "" Then
                itmBuild(0) = itmBuild(0) & "~" & JTMMJobAct(k).JTMMjaSubID
            End If

            itmBuild(2) = JTMMJobAct(k).JTmmjaJobType
            itmBuild(3) = JTMMJobAct(k).JTmmjaCstMeth
            If JTMMJobAct(k).JTMMjalab > 9999999 Then
                itmBuild(1) = "DEACT"
            Else
                itmBuild(1) = String.Format("{0:0.00}", JTMMJobAct(k).JTMMjalab + JTMMJobAct(k).jtmmjamat + JTMMJobAct(k).jtmmjasub)
            End If
            '
            lvLine = New ListViewItem(itmBuild)
            lvMMJobs.Items.Add(lvLine)
            '
            itmBuild(0) = ""
            itmBuild(1) = ""
            itmBuild(2) = ""
            itmBuild(3) = ""
            itmBuild(4) = ""
            '
            '
            If JTMMJobAct(k).JTMMjaCstMeth = "CST+" Then
                Dim itmBuild4(4) As String
                itmBuild4(0) = JTMMJobAct(k).JTMMjaJobID
                '
                If JTMMJobAct(k).JTMMjaSubID <> "" Then
                    itmBuild4(0) = itmBuild4(0) & "~" & JTMMJobAct(k).JTMMjaSubID
                End If
                If JTMMJobAct(k).JTMMjalab > 9999999 Then
                    itmBuild4(1) = "DEACT"
                Else
                    itmBuild4(1) = String.Format("{0:0.00}", JTMMJobAct(k).JTMMjalab + JTMMJobAct(k).jtmmjamat + JTMMJobAct(k).jtmmjasub)
                End If
                itmBuild4(2) = JTMMJobAct(k).JTMMjaLstInvDt
                itmBuild4(3) = JTMMJobAct(k).JTMMjaTotlInvd
                lvLine = New ListViewItem(itmBuild4)

                lvMMCstP.Items.Add(lvLine)
            End If
            '

            '
            If JTMMJobAct(k).JTMMjaCstMeth = "AUP" Or
                JTMMJobAct(k).JTMMjaCstMeth = "AUPH" Then
                Dim itmBuild4(4) As String
                itmBuild4(0) = JTMMJobAct(k).JTMMjaJobID
                '
                If JTMMJobAct(k).JTMMjaSubID <> "" Then
                    itmBuild4(0) = itmBuild4(0) & "~" & JTMMJobAct(k).JTMMjaSubID
                End If
                If JTMMJobAct(k).JTMMjalab > 9999999 Then
                    itmBuild4(1) = "DEACT"
                Else
                    itmBuild4(1) = JTMMJobAct(k).JTMMjaAUPQueued
                End If
                itmBuild4(2) = JTMMJobAct(k).JTMMjaTotlInvd
                itmBuild4(3) = JTMMJobAct(k).JTMMjaAUPQuoted
                lvLine = New ListViewItem(itmBuild4)
                lvMMAUP.Items.Add(lvLine)
            End If
        Next
        '

        ' ----------------------------------
        ' load data into lvMMtk listview.
        ' ----------------------------------
        '
        '
        ' Logic to make sure ListView is empty.
        '
        If lvMMtk.Items.Count > 0 Then
            lvMMtk.Items.Clear()
        End If

        tmpJTPOS = 0
        '
        Dim itmBuildtk(2) As String
        Dim lvLinetk As ListViewItem
        '
        Dim key2 As ICollection = JTMMEmpTK.Keys
        Dim k2 As String
        For Each k2 In key2
            '
            itmBuildtk(0) = JTMMEmpTK(k2).JTMMEmpID
            itmBuildtk(1) = JTMMEmpTK(k2).JTMMLstTK
            '
            lvLinetk = New ListViewItem(itmBuildtk)
            lvMMtk.Items.Add(lvLinetk)
        Next
        '
        ' -----------------------------------------------------------------
        ' load data into lvMMNLC listview.
        ' -----------------------------------------------------------------
        '
        Dim tmpSub As Integer = 0
        Dim itmBuildvi(5) As String
        Dim lvLinevi As ListViewItem
        Dim ShortName As String = ""
        Dim VendName As String = ""
        Dim VendType As String = ""
        Dim LstActDate As String = ""
        Dim ActivityInterface As String = ""
        Dim Issues As String = ""
        '
        lvMMNLC.Items.Clear()
        tmpSub = 0
        '
        ' -----------------------------------------------------------
        ' Code to populate the vendor lv on the MainMenu
        ' -----------------------------------------------------------
        Do While JTvi.JTviReadJTviforMM(tmpSub, ShortName, VendName, VendType,
                                        LstActDate, ActivityInterface, Issues) _
                                        = "DATA" Or tmpSub = 0
            '
            itmBuildvi(0) = ShortName
            itmBuildvi(1) = VendType
            itmBuildvi(2) = LstActDate
            itmBuildvi(3) = ActivityInterface
            itmBuildvi(4) = Issues
            '
            lvLinevi = New ListViewItem(itmBuildvi)
            lvMMNLC.Items.Add(lvLinevi)
            '
            tmpSub += 1
        Loop
        '
        ' -------------------------------------------------------
        ' Fill last NLC entry date on MM
        ' -------------------------------------------------------
        Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePath & "EDIInputFiles\", "*.pdf")
        JTmmNLCLabel.Text = "Last NLC" & vbCrLf & "Entry Date" & vbCrLf & JTnlc.JTnlcLstEntryDate() & vbCrLf &
                 "= = = = = = =" & vbCrLf & fileEntries.Count & " Unprocessed" & vbCrLf & "PDF Invoice" & vbCrLf & "files"
        ' 
        Erase fileEntries

        '
        cboxJTmmNewJob.Checked = False
        cboxJTmmNewVendor.Checked = False
        cboxJTmmEmpMnt.Checked = False
        '
        lvMMtk.Select()

        Me.MdiParent = JTstart
        JTstart.JTstartMenuOn()
        JTstart.JTstartShowTitles()
        JTstart.JTstPictureBox1("SHOW")
        '
        Me.Show()
    End Sub


    Private Sub lvMMJobs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvMMJobs.DoubleClick
        Dim tmplvSub As Double = lvMMJobs.FocusedItem.Index
        Dim lvLine As ListViewItem
        Me.Hide()
        lvLine = lvMMJobs.SelectedItems(0)
        Dim TmpJob As String = lvLine.SubItems(0).Text
        Dim TmpSubJob As String = ""
        JTMMSplitJobSubjob(TmpJob, TmpSubJob)
        If lvLine.SubItems(1).Text = "DEACT" Then
            JTMMQuestionDeact(TmpJob, TmpSubJob)

        End If
        '
        '
        JTjim.JTjimPopPanel(TmpJob, TmpSubJob)
        '
    End Sub
    Private Sub lvMMCstP_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvMMCstP.DoubleClick
        Dim tmplvSub As Double = lvMMCstP.FocusedItem.Index
        Dim lvLine As ListViewItem
        Me.Hide()
        lvLine = lvMMCstP.SelectedItems(0)
        Dim TmpJob As String = lvLine.SubItems(0).Text
        Dim TmpSubJob As String = ""
        JTMMSplitJobSubjob(TmpJob, TmpSubJob)
        '
        If lvLine.SubItems(1).Text = "DEACT" Then
            JTMMQuestionDeact(TmpJob, TmpSubJob)
        End If
        '
        JTjim.JTjimPopPanel(TmpJob, TmpSubJob)
        '
    End Sub

    Private Sub lvMMAUP_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvMMAUP.DoubleClick
        Dim tmplvSub As Double = lvMMAUP.FocusedItem.Index
        Dim lvLine As ListViewItem
        Me.Hide()
        lvLine = lvMMAUP.SelectedItems(0)
        Dim TmpJob As String = lvLine.SubItems(0).Text
        Dim TmpSubJob As String = ""
        JTMMSplitJobSubjob(TmpJob, TmpSubJob)
        '
        If lvLine.SubItems(1).Text = "DEACT" Then
            JTMMQuestionDeact(TmpJob, TmpSubJob)
        End If
        '
        JTjim.JTjimPopPanel(TmpJob, TmpSubJob)
        '
    End Sub
    Private Sub lvMMtk_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvMMtk.DoubleClick
        ' ------------------------------------------------------------------------------------------
        ' Sub to kick off timekeeping. Selected employee and last date recorded passed as arguments.
        ' ------------------------------------------------------------------------------------------
        Dim tmpEmpID As String = lvMMtk.SelectedItems.Item(0).Text
        Dim tmpLastDate As String = ""
        '
        Me.Hide()
        JTtk.MdiParent = JTstart
        JTtk.JTtkInit(tmpEmpID, tmpLastDate)

    End Sub
    Private Sub lvMMNLC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvMMNLC.DoubleClick
        ' ------------------------------------------------------------------------------------------
        ' Sub to kick off timekeeping. Selected employee and last date recorded passed as arguments.
        ' ------------------------------------------------------------------------------------------
        Dim tmpShortName As String = lvMMNLC.SelectedItems.Item(0).Text

        '
        Me.Hide()
        JTstart.JTstartMenuOff()
        JTvi.JTviReturnPanel = "MM"
        '       If tmpShortName = "New Vendor" Then
        '      JTviNewVendor.JTviNVGetNewShortName(tmpShortName)
        '     Else
        JTvi.MdiParent = JTstart

            JTvi.JTviPopPanel(tmpShortName, lvMMNLC.SelectedItems(0).SubItems(1).Text)
            JTvi.Show()
        '    End If
    End Sub
    Public Sub JTMMTKArrayDateUpdate(tmpEmpID As String, tmpWEDate As String)

        If JTMMEmpTK.ContainsKey(tmpEmpID) = True Then
            JTMMEmpTkArea.JTMMEmpID = tmpEmpID
            JTMMEmpTkArea.JTMMLstTK = tmpWEDate
            If JTMMEmpTK(JTMMEmpTkArea.JTMMEmpID).JTMMLstTK <> "" Then
                Dim tmpExtngDate As Date = JTMMEmpTK(JTMMEmpTkArea.JTMMEmpID).JTMMLstTK
                Dim tmpNewDate As Date = tmpWEDate
                If tmpNewDate > tmpExtngDate Then
                    JTMMEmpTK.Remove(tmpEmpID)
                    JTMMEmpTK.Add(JTMMEmpTkArea.JTMMEmpID, JTMMEmpTkArea)
                End If
            Else
                JTMMEmpTK.Remove(tmpEmpID)
                JTMMEmpTK.Add(JTMMEmpTkArea.JTMMEmpID, JTMMEmpTkArea)
            End If
        End If
    End Sub
    Public Function JTMMNotInvAmts(tmpJobID As String, tmpSubID As String, tmpLabDol As Double, tmpMatDol As Double, tmpSubDol As Double) As String
        '
        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String
        '
        For Each k In key
            If JTMMJobAct(k).JTMMjaJobID = tmpJobID And JTMMJobAct(k).JTMMjasubID = tmpSubID Then
                Dim tmpSLarea As New JTMMJobActStructure
                tmpSLarea = JTMMJobAct(k)
                JTMMJobAct.Remove(k)
                tmpSLarea.JTMMjaLab += tmpLabDol
                tmpSLarea.JTMMjaMat += tmpMatDol
                tmpSLarea.JTMMjaSub += tmpSubDol
                '

                Dim tmpKey As String = Str(1999999 - Int(tmpSLarea.JTMMjaLab + tmpSLarea.JTMMjaMat + tmpSLarea.JTMMjaSub)) & tmpSLarea.JTMMjaJobID & tmpSLarea.JTMMjaSubID
                If JTVarMaint.JTgvMMJobSeq <> "J" Then
                    JTMMJobAct.Add(tmpKey, tmpSLarea)
                Else
                    JTMMJobAct.Add(k, tmpSLarea)
                End If
                Return ""
            End If
        Next
        Return ""
    End Function
    Public Function JTMMNotInvValues(tmpJobID As String, tmpSubID As String,
                                     ByRef tmpLabAmt As Double, ByRef tmpMatAmt As Double, ByRef tmpSubAmt As Double) As String

        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String
        '
        tmpLabAmt = 0
        tmpMatAmt = 0
        tmpSubAmt = 0
        For Each k In key

            If JTMMJobAct(k).JTMMjaJobID = tmpJobID And JTMMJobAct(k).JTMMjaSubID = tmpSubID Then

                tmpLabAmt = JTMMJobAct(k).JTMMjaLab
                tmpMatAmt = JTMMJobAct(k).JTMMjaMat
                tmpSubAmt = JTMMJobAct(k).JTMMjaSub
                '
                Return "DATA"
            End If
        Next
        '      MsgBox(tmpJobID & " " & tmpSubID & " Not Found JTMainMenu~474")
        Return "NOTFOUND"
    End Function

    Public Sub JTMMClrMorSNotInvValues()
        ' -------------------------------------------------------------------
        ' Clear to 0 the unbilled activity for Mat and Subs.
        ' Is used if nlc session (entire session, not line item is cancelled.
        ' -------------------------------------------------------------------
        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String = ""
        Dim tmpKeyCnt As Integer = key.Count
        Dim tmpLoopPOS As Integer = 0
        Do While tmpLoopPOS < tmpKeyCnt
            JTMMjaarea.JTMMjaJobID = JTMMJobAct(key(tmpLoopPOS)).JTMMjaJobID
            JTMMjaarea.JTMMjaSubID = JTMMJobAct(key(tmpLoopPOS)).JTMMjasubID
            JTMMjaarea.JTMMjaJobType = JTMMJobAct(key(tmpLoopPOS)).JTMMjaJobType
            JTMMjaarea.JTMMjaCstMeth = JTMMJobAct(key(tmpLoopPOS)).JTMMjaCstMeth
            JTMMjaarea.JTMMjaLab = JTMMJobAct(key(tmpLoopPOS)).JTMMjaLab
            JTMMjaarea.JTMMjaMat = 0
            JTMMjaarea.JTMMjaSub = 0
            '
            JTMMJobAct.Remove(key(tmpLoopPOS))
            Dim tmpKey As String = Str(1999999 - Int(JTMMjaarea.JTMMjaLab + JTMMjaarea.JTMMjaMat + JTMMjaarea.JTMMjaSub)) & JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
            JTMMJobAct.Add(tmpKey, JTMMjaarea)
            tmpLoopPOS += 1
        Loop
    End Sub
    Public Function JTMMAddNewJobtoJTMMJobAct(tmpJTJobName As String, tmpJTSubname As String) As String
        ' -------------------------------------------------------------------------
        ' sub to newly added job to JTMMJobAct SL
        ' -------------------------------------------------------------------------
        JTMMjaarea.JTMMjaJobID = tmpJTJobName
        JTMMjaarea.JTMMjaSubID = tmpJTSubname
        JTMMjaarea.JTMMjaLab = 0
        JTMMjaarea.JTMMjaMat = 0
        JTMMjaarea.JTMMjaSub = 0
        Dim tmpKey As String = Str(1999999 - Int(JTMMjaarea.JTMMjaLab + JTMMjaarea.JTMMjaMat + JTMMjaarea.JTMMjaSub)) & JTMMjaarea.JTMMjaJobID & JTMMjaarea.JTMMjaSubID
        JTMMJobAct.Add(tmpKey, JTMMjaarea)
        Return "JobAdded"
    End Function

    Private Sub cboxJTmmNewJob_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmNewJob.CheckedChanged
        If cboxJTmmNewJob.Checked = True Then
            Me.Hide()
            JTstart.JTstartMenuOff()
            JTvi.JTviReturnPanel = "MM"
            JTnj.MdiParent = JTstart
            JTnj.JTnjInit()
        End If
    End Sub

    Private Sub cboxJTmmNewVendor_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmNewVendor.CheckedChanged
        If cboxJTmmNewVendor.Checked = True Then
            cboxJTmmNewVendor.Checked = False
            Me.Hide()
            JTstart.JTstartMenuOff()
            JTvi.JTviReturnPanel = "MM"
            ' Modified to use JTvi panel for new vendors
            '      JTviNewVendor.MdiParent = JTstart
            '      JTviNewVendor.JTviNVGetNewShortName("New Vendor")
            JTvi.JTviDisplayVendor("NewVendor")
        End If
    End Sub

    Private Sub cboxJTmmEmpMnt_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmEmpMnt.CheckedChanged
        If cboxJTmmEmpMnt.Checked = True Then
            cboxJTmmEmpMnt.Checked = False
            JTstart.JTstartMenuOff()
            JTvi.JTviReturnPanel = "MM"
            Me.Hide()
            JThr.MdiParent = JTstart
            JThr.JThrInit()
        End If
    End Sub

    Private Sub cboxJTmmShowDeact_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmShowDeact.CheckedChanged
        Me.Hide()
        JTMMFirstTimeThru = "FIRST"
        JTMMinit()
    End Sub

    Private Sub rbtnJTMMJSAmt_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTMMJSAmt.CheckedChanged, rbtnJTmmJSAlpha.CheckedChanged
    
        If JTMMFirstTimeThru = "FIRST" Then
            Exit Sub
        End if
        If rbtnJTMMJSAmt.Checked = True Then
            JTVarMaint.JTgvMMJobSeq = "A"
        End If
        If rbtnJTmmJSAlpha.Checked = True Then
            JTVarMaint.JTgvMMJobSeq = "J"
        End If
        Me.Hide()
        JTMMFirstTimeThru = "FIRST"
        JTMMinit()
    End Sub

    Private Sub btnJTmmNLC_Click(sender As Object, e As EventArgs) Handles btnJTmmNLC.Click
        LogFileWrite("JTMainMenu", "JTnlcM session Started")
        Me.Hide()
        JTstart.JTstartMenuOff()
        JTnlc.MdiParent = JTstart
        JTnlcM.JTnlcMinit()
    End Sub

    Private Sub btnJTmmExit_Click(sender As Object, e As EventArgs) Handles btnJTmmExit.Click
        ' -----------------------------------------
        ' Call common system closing logic
        ' -----------------------------------------
        JTstart.JTstartExitSys()
    End Sub
    ''' <summary>
    ''' Function to split job and subjob names returned from the MM panel.
    ''' Jobs with subjobs are separated with "~". This function removes the "~"
    ''' and puts the job name and subjob name into segregated fields.
    ''' </summary>
    ''' <param name="JobName"></param>
    ''' <param name="SubJobName"></param>
    ''' <returns></returns>
    Private Function JTMMSplitJobSubjob(ByRef JobName As String, ByRef SubJobName As String) As String
        Dim FirstCharacter As Integer = JobName.IndexOf("~")
        If FirstCharacter = -1 Then 'value of '-1' is when a "~" does not exist in JobName.
            SubJobName = ""
            Return ""
        End If
        SubJobName = Mid(JobName, FirstCharacter + 2)
        JobName = Mid(JobName, 1, FirstCharacter)
        Return ""
    End Function
    ''' <summary>
    ''' This function is called when a deactivated job is selected from the MainMenu.
    ''' It gives the option to reactivate the job. A deactiviated job cannot have any 
    ''' new activity charged to it. If the job is not reactivated, it can only be
    ''' displayed.
    ''' </summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubJob"></param>
    ''' <returns></returns>
    Private Function JTMMQuestionDeact(tmpJob As String, tmpSubJob As String) As String
        Dim tmpAnswer As Integer = MsgBox("Reactivate Job?" & vbCrLf &
                          tmpJob & " " & tmpSubJob & " is a Deactivasted Job." & vbCrLf &
                          "Would you like to Reactivate it?",
                          MsgBoxStyle.YesNo, "J.I.M. - Main Menu")
        If tmpAnswer = DialogResult.Yes Then
            ' code to reactivate a job.
            '       lvLine.SubItems(1).Text = String.Format("{0:0.00}", 0)
            Dim key As ICollection = JTMMJobAct.Keys
            Dim k As String
            For Each k In key
                If JTMMJobAct(k).JTMMjaJobID = tmpJob And
                           JTMMJobAct(k).JTMMjaSubID = tmpSubJob Then
                    Dim tmpNewItem As New JTMMJobActStructure
                    tmpNewItem = JTMMJobAct(k)
                    tmpNewItem.JTMMjaLab = 0
                    JTMMJobAct.Remove(k)
                    JTMMJobAct.Add(k, tmpNewItem)
                    Exit For
                End If
            Next
            JTjim.JTjimReactivateJob(tmpJob, tmpSubJob)
        End If
        Return ""
    End Function
    ''' <summary>
    ''' This function updates a total of all previous invoices for this job into the
    ''' JTMMJobAct sl. It also updates the last invoice date.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <param name="tmpInvTotalsStr"></param>
    ''' <param name="tmpLstInvDate"></param>
    ''' <returns>Return values are not used.</returns>
    Public Function JTmmUpdtInvTls(tmpJobID As String, tmpSubID As String,
                                   tmpInvTotalsStr As String, tmpLstInvDate As String) As String
        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String
        For Each k In key
            If tmpJobID = JTMMJobAct(k).JTMMjaJobID And
                tmpSubID = JTMMJobAct(k).JTMMjaSubID Then
                Dim tmpSLArea As New JTMMJobActStructure
                tmpSLArea = JTMMJobAct(k)
                JTMMJobAct.Remove(k)
                tmpSLArea.JTMMjaTotlInvd = tmpInvTotalsStr
                tmpSLArea.JTMMjaLstInvDt = tmpLstInvDate
                JTMMJobAct.Add(k, tmpSLArea)
                Return "Updated"
            End If
        Next
        Return "NoRecordFound"
    End Function
    ''' <summary>
    ''' Function to update amounts from AUP jobs into the JTMMJobAct sl.
    ''' 
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <param name="tmpJorQ">This parameter determines what type of amount is being presented.
    ''' "J" = The agreed upon amount for the job
    ''' "Q" = stands for amount that are queued for invoicing for this job.</param>
    ''' <param name="tmpAUPStrAmt"></param>
    ''' <returns>Values in the return are not used.</returns>
    Public Function JTmmUpdtAUPTls(tmpJobID As String, tmpSubID As String,
                                   tmpJorQ As String, tmpAUPStrAmt As String) As String
        Dim key As ICollection = JTMMJobAct.Keys
        Dim k As String
        For Each k In key
            If tmpJobID = JTMMJobAct(k).JTMMjaJobID And
                    tmpSubID = JTMMJobAct(k).JTMMjaSubID Then
                JTMMjaarea = JTMMJobAct(k)
                JTMMJobAct.Remove(k)
                If tmpJorQ = "J" Then
                    JTMMjaarea.JTMMjaAUPQuoted = tmpAUPStrAmt
                Else
                    JTMMjaarea.JTMMjaAUPQueued = tmpAUPStrAmt
                End If

                JTMMJobAct.Add(k, JTMMjaarea)
                Return "Updated"
            End If
        Next
        Return "NoRecordFound"
    End Function
    ''' <summary>
    ''' This Sub control the Chkbox on the MainMenu to display or shut off the display
    ''' of the Cost+ listview ... displayed in the center window of the MainMenu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTmmDispCostP_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmDispCostP.CheckedChanged
        If cboxJTmmDispCostP.Checked = True Then
            If cboxJTmmDispAUP.Checked = True Then
                cboxJTmmDispAUP.Checked = False
            End If
            lvMMCstP.Visible = True
            JTmmJobPanelLabel.Text = "Job Entry, Maintenance , Invoicing, Etc.(Cst+ Jobs)"
            lvMMAUP.Visible = False
            cboxJTmmDispAUP.Checked = False
            lvMMJobs.Visible = False
        Else
            lvMMCstP.Visible = False
            lvMMAUP.Visible = False
            ' cboxJTmmDispAUP.Checked = False
            lvMMJobs.Visible = True
            JTmmJobPanelLabel.Text = "Job Entry, Maintenance , Invoicing, Etc.(All Jobs)"
        End If
    End Sub
    ''' <summary>
    ''' This Sub control the Chkbox on the MainMenu to display or shut off the display
    ''' of the AUP listview ... displayed in the center window of the MainMenu.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTmmDispAUP_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmDispAUP.CheckedChanged
        If cboxJTmmDispAUP.Checked = True Then
            If cboxJTmmDispCostP.Checked = True Then
                cboxJTmmDispCostP.Checked = False
            End If
            lvMMCstP.Visible = False
            lvMMAUP.Visible = True
            JTmmJobPanelLabel.Text = "Job Entry, Maintenance , Invoicing, Etc.(AUP Jobs)"
            cboxJTmmDispCostP.Checked = False
            lvMMJobs.Visible = False
        Else
            lvMMCstP.Visible = False
            lvMMAUP.Visible = False
            ' cboxJTmmDispCostP.Checked = False
            lvMMJobs.Visible = True
            JTmmJobPanelLabel.Text = "Job Entry, Maintenance , Invoicing, Etc.(All Jobs)"
        End If
    End Sub

    Private Sub CboxJTmmJobList_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmJobList.CheckedChanged
        If cboxJTmmJobList.Checked = True Then
            cboxJTmmJobList.Checked = False
            JTmr.JTmrInit("CUSTJOB")
        End If
    End Sub

    Private Sub CboxJTmmPrntVendList_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTmmPrntVendList.CheckedChanged
        If cboxJTmmPrntVendList.Checked = True Then
            cboxJTmmPrntVendList.Checked = False
            JTmr.JTmrInit("VENDOR")
        End If
    End Sub

    Private Sub BtnJTmmAUPSum_Click(sender As Object, e As EventArgs) Handles btnJTmmAUPSum.Click
        Me.Hide()
        JTaupSum.JTaupSumInit()
    End Sub

    Private Sub BtnJTmmInvng_Click(sender As Object, e As EventArgs) Handles btnJTmmInvng.Click
        Me.Hide()
        JTinv.JTinvInit()
    End Sub





    '
End Class