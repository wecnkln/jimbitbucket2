﻿Imports System.IO
Public Class JTaup
    ' ----------------------------------------------------------------------------------------------
    ' Logic to manage JTaup panel. This panel is used to set up and invoice AUP job and/or sub jobs.
    ' ----------------------------------------------------------------------------------------------
    Structure JTaupJOBStru
        Dim JTaupJJob As String
        Dim JTaupJSubJob As String
        Dim JTaupJDate As String
        Dim JTaupJType As String
        Dim JTaupJDesc As String
        Dim JTaupJAmt As String
        Dim JTaupJApprvd As String
        Dim JTaupJRefDoc As String
    End Structure
    '
    Structure JTaupInvStru
        Dim JTaupIJob As String
        Dim JTaupISubJob As String
        Dim JTaupIDate As String
        Dim JTaupIDesc As String
        Dim JTaupIAmt As String
        Dim JTaupIAfterDt As String
        Dim JTaupIFinal As String
        Dim JTaupIInvDate As String
        Dim JTaupIInvNum As String
    End Structure
    '
    ' --------------------------------------------------------------------------------------
    ' Structure used to pass job related figures back to help populate control panel for AUP
    ' and general invoicing panel.
    ' --------------------------------------------------------------------------------------
    Structure JTaupInfoRec
        Dim JTaupCustomer As String
        Dim JTaupJob As String
        Dim JTaupJobInitial As String
        Dim JTaupJobChanges As String
        Dim JTaupInvdTD As String
        Dim JTaupQueuedNow As String
        Dim JTaupQueuedFuture As String
        Dim JTaupLstInvDate As String
        Dim JTaupLstInvSent As Boolean
    End Structure
    '
    Dim slJTaupJob As SortedList = New SortedList
    '
    Public slJTaupInv As SortedList = New SortedList
    '
    Dim JTaupKeyCollPos As Integer = 0
    Dim tmpEmptyDate As String = Nothing
    '

    Public Sub JTaupLoad()
        ' ----------------------------------------------------------------------
        ' Load JTAUP.dat file into Sortedlists. Done on initial system start up.
        ' ----------------------------------------------------------------------
        '
        ' Clear sorted list to make sure it is empty before load.
        '
        Dim tmpadditemJ As New JTaupJOBStru
        Dim tmpadditemI As New JTaupInvStru
        slJTaupJob.Clear()
        slJTaupInv.Clear()

        Dim fileRecord As String = ""
        Dim ReadFileData As String = ""
        Dim tmpSeq As Integer = 10

        '

        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTAUP.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTAUP.Bkup") Then
            MsgBox("JTAUP --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (AUP.011)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTAUP.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTAUP.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTAUP.Bkup", "JTAUP.dat")
        End If
        ' ------------------------------------------------------------------

        '
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    Dim tmpLoc As Integer = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    Dim ReadFileKey As String = fileRecord.Substring(1, tmpLoc - 1)
                    If ReadFileKey = "AUPJobRecord" Or ReadFileKey = "/AUPJobRecord" Or ReadFileKey = "AUPInvRecord" _
                                                    Or ReadFileKey = "/AUPInvRecord" Then
                        ReadFileData = ""
                    Else
                        tmpLoc += 1
                        Dim tmpLocKey As Integer = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        ReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case ReadFileKey
                        '
                        Case "aupJJob"
                            tmpadditemJ.JTaupJJob = ReadFileData
                        Case "aupJSubJob"
                            tmpadditemJ.JTaupJSubJob = ReadFileData
                        Case "aupJDate"
                            tmpadditemJ.JTaupJDate = ReadFileData
                        Case "aupJType"
                            tmpadditemJ.JTaupJType = ReadFileData
                        Case "aupJDesc"
                            tmpadditemJ.JTaupJDesc = tmpadditemJ.JTaupJDesc & ReadFileData
                        Case "aupJAmt"
                            tmpadditemJ.JTaupJAmt = ReadFileData
                        Case "aupJApprvd"
                            tmpadditemJ.JTaupJApprvd = ReadFileData
                        Case "aupJRefDoc"
                            tmpadditemJ.JTaupJRefDoc = ReadFileData
                        Case "/AUPJobRecord"
                            tmpadditemJ.JTaupJDesc = tmpadditemJ.JTaupJDesc.Replace("~!", vbLf)
                            Dim tmpkey As String = tmpadditemJ.JTaupJJob & tmpadditemJ.JTaupJSubJob & tmpadditemJ.JTaupJDate.Substring(6, 4) _
                               & tmpadditemJ.JTaupJDate.Substring(0, 2) & tmpadditemJ.JTaupJDate.Substring(3, 2) & tmpSeq
                            Do While slJTaupJob.ContainsKey(tmpkey)
                                tmpSeq += 1
                                tmpkey = tmpadditemJ.JTaupJJob & tmpadditemJ.JTaupJSubJob & tmpadditemJ.JTaupJDate.Substring(6, 4) _
                                & tmpadditemJ.JTaupJDate.Substring(0, 2) & tmpadditemJ.JTaupJDate.Substring(3, 2) & tmpSeq
                            Loop
                            slJTaupJob.Add(tmpkey, tmpadditemJ)
                            '                     MsgBox("AUP Job Record Added" & tmpadditemJ.JTaupJJob & "JTaup - 111")
                            tmpadditemJ.JTaupJDesc = ""
'
                        Case "aupIJob"
                            tmpadditemI.JTaupIJob = ReadFileData
                        Case "aupISubJob"
                            tmpadditemI.JTaupISubJob = ReadFileData
                        Case "aupIDate"
                            tmpadditemI.JTaupIDate = ReadFileData
                        Case "aupIDesc"
                            tmpadditemI.JTaupIDesc = tmpadditemI.JTaupIDesc & ReadFileData
                        Case "aupIAmt"
                            tmpadditemI.JTaupIAmt = ReadFileData
                        Case "aupIAfterDt"
                            tmpadditemI.JTaupIAfterDt = ReadFileData
                        Case "aupIFinal"
                            tmpadditemI.JTaupIFinal = ReadFileData
                        Case "aupIInvDate"
                            tmpadditemI.JTaupIInvDate = ReadFileData
                        Case "aupIInvNum"
                            tmpadditemI.JTaupIInvNum = ReadFileData
                        Case "/AUPInvRecord"
                            tmpadditemI.JTaupIDesc = tmpadditemI.JTaupIDesc.Replace("~!", vbLf)
                            JTaupslJTaupInvAddRecord(tmpadditemI, "Called from JTaupLoad")
                            tmpadditemI.JTaupIDesc = ""
                            '
                    End Select
                    '
                End If
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' -----------------------------------------------------------
        ' If this load is for YE processing, exit sub here.
        ' -----------------------------------------------------------
        '     If JTye.JTyeInProgress = True Then
        '    Exit Sub
        '   End If
        '
        '
        ' -----------------------------------------------------------
        ' Add code to create totals invoiced field in JTMMJobAct SL.
        ' Last invoice date will also be updated. Data will be used
        ' on MM panels.
        ' -----------------------------------------------------------
        Dim tmpJobID As String = ""
        Dim tmpSubID As String = ""
        Dim tmpAUPQuoted As Decimal = 0
        Dim tmpAUPQueued As Decimal = 0
        Dim tmpAUPStrAmt As String = ""
        '
        ' Accumulate AUP quoted amounts ... Job totals
        '
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String
        For Each k In key
            If tmpJobID = "" And tmpSubID = "" Then
                tmpJobID = slJTaupJob(k).JTaupJJob
                tmpSubID = slJTaupJob(k).JTaupJSubJob
                tmpAUPQuoted = Val(slJTaupJob(k).JTaupJAmt)

            Else
                If tmpJobID = slJTaupJob(k).JTaupJJob And tmpSubID = slJTaupJob(k).JTaupJSubJob Then
                    tmpAUPQuoted += Val(slJTaupJob(k).JTaupJAmt)
                Else
                    tmpAUPStrAmt = String.Format("{0:0.00}", tmpAUPQuoted)
                    JTMainMenu.JTmmUpdtAUPTls(tmpJobID, tmpSubID, "J", tmpAUPStrAmt)
                    tmpJobID = slJTaupJob(k).JTaupJJob
                    tmpSubID = slJTaupJob(k).JTaupJSubJob
                    tmpAUPQuoted = Val(slJTaupJob(k).JTaupJAmt)
                End If
            End If
        Next
        ' process last key
        If tmpAUPQuoted <> 0 Then
            tmpAUPStrAmt = String.Format("{0:0.00}", tmpAUPQuoted)
            JTMainMenu.JTmmUpdtAUPTls(tmpJobID, tmpSubID, "J", tmpAUPStrAmt)
        End If
        tmpJobID = ""
        tmpSubID = ""
        tmpAUPQueued = 0
        ' 
        '
        ' Accumulate AUP queued amounts ... 
        '
        key = slJTaupInv.Keys
        '
        For Each k In key
            If slJTaupInv(k).JTaupIInvNum = "" Then   ' check to make sure item has not been invoiced.
                '
                If tmpJobID = "" And tmpSubID = "" Then
                    tmpJobID = slJTaupInv(k).JTaupIJob
                    tmpSubID = slJTaupInv(k).JTaupISubJob
                    tmpAUPQueued = Val(slJTaupInv(k).JTaupIAmt)
                Else
                    If tmpJobID = slJTaupInv(k).JTaupIJob And
                        tmpSubID = slJTaupInv(k).JTaupISubJob Then
                        tmpAUPQueued += Val(slJTaupInv(k).JTaupIAmt)
                    Else
                        tmpAUPStrAmt = String.Format("{0:0.00}", tmpAUPQueued)
                        JTMainMenu.JTmmUpdtAUPTls(tmpJobID, tmpSubID, "Q", tmpAUPStrAmt)
                        tmpJobID = slJTaupInv(k).JTaupIJob
                        tmpSubID = slJTaupInv(k).JTaupISubJob
                        tmpAUPQueued = Val(slJTaupInv(k).JTaupIAmt)
                    End If
                End If
            End If
        Next
        ' process last key
        If tmpAUPQueued <> 0 Then
            tmpAUPStrAmt = String.Format("{0:0.00}", tmpAUPQueued)
            JTMainMenu.JTmmUpdtAUPTls(tmpJobID, tmpSubID, "Q", tmpAUPStrAmt)
        End If
        '
        '
    End Sub
    Public Sub JTaupFlush()
        ' ----------------------------------------------------------------------
        ' Flush values in sortedlists to JTAUP.dat file. This sub can an should
        ' be run everytime values have been changed in the sortedlists.
        ' ----------------------------------------------------------------------
        Dim fileRecord As String = ""
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTAUP.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTAUP.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            '

            ' Get a collection of the keys. 
            Dim keyJ As ICollection = slJTaupJob.Keys
            Dim k As String
            For Each k In keyJ
                '
                fileRecord = "<AUPJobRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJJob>" & slJTaupJob(k).JTaupJJob & "</aupJJob>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJSubJob>" & slJTaupJob(k).JTaupJSubJob & "</aupJSubJob>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJDate>" & slJTaupJob(k).JTaupJDate & "</aupJDate>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJType>" & slJTaupJob(k).JTaupJType & "</aupJType>"
                writeFile.WriteLine(fileRecord)
                ' ---------------------------------------------------------------------------
                ' Code to break records longer than 75 character into multiple short records.
                ' ---------------------------------------------------------------------------
                Dim tmpFieldData As String = slJTaupJob(k).JTaupJDesc.replace(vbLf, "~!")
                Dim tmpLength As Short = tmpFieldData.Length
                Dim tmpFieldPos As Short = 0
                Do While tmpLength > 75
                    fileRecord = "<aupJDesc>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</aupJDesc>"
                    writeFile.WriteLine(fileRecord)
                    tmpLength -= 75
                    tmpFieldPos += 75
                Loop
                If tmpLength > 0 Then
                    fileRecord = "<aupJDesc>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</aupJDesc>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "<aupJAmt>" & slJTaupJob(k).JTaupJAmt & "</aupJAmt>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJApprvd>" & slJTaupJob(k).JTaupJApprvd & "</aupJApprvd>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupJRefDoc>" & slJTaupJob(k).JTaupJRefDoc & "</aupJRefDoc>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "</AUPJobRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            ' Get a collection of the keys. 
            Dim keyI As ICollection = slJTaupInv.Keys
            '
            For Each k In keyI
                '
                fileRecord = "<AUPInvRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIJob>" & slJTaupInv(k).JTaupIJob & "</aupIJob>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupISubJob>" & slJTaupInv(k).JTaupISubJob & "</aupISubJob>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIDate>" & slJTaupInv(k).JTaupIDate & "</aupIDate>"
                writeFile.WriteLine(fileRecord)
                '
                ' ---------------------------------------------------------------------------
                ' Code to break records longer than 75 character into multiple short records.
                ' ---------------------------------------------------------------------------
                Dim tmpFieldData As String = slJTaupInv(k).JTaupIDesc.replace(vbLf, "~!")
                Dim tmpLength As Short = tmpFieldData.Length
                Dim tmpFieldPos As Short = 0
                Do While tmpLength > 75
                    fileRecord = "<aupIDesc>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</aupIDesc>"
                    writeFile.WriteLine(fileRecord)
                    tmpLength -= 75
                    tmpFieldPos += 75
                Loop
                If tmpLength > 0 Then
                    fileRecord = "<aupIDesc>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</aupIDesc>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "<aupIAmt>" & slJTaupInv(k).JTaupIAmt & "</aupIAmt>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIAfterDt>" & slJTaupInv(k).JTaupIAfterDt & "</aupIAfterDt>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIFinal>" & slJTaupInv(k).JTaupIFinal & "</aupIFinal>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIInvDate>" & slJTaupInv(k).JTaupIInvDate & "</aupIInvDate>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<aupIInvNum>" & slJTaupInv(k).JTaupIInvNum & "</aupIInvNum>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "</AUPInvRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTAUP.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTAUP.Bkup")
        End If
        ' ------------------------------------------------------------------------

    End Sub
    Public Sub JTaupInit(tmpJob As String, tmpSubJob As String, tmpJobDesc As String)
        ' ----------------------------------------------------------------------
        ' This sub sets the values to prepare the JTaup panel for display.
        ' ----------------------------------------------------------------------
        txtJTaupJobID.Text = tmpJob
        txtJTaupSubID.Text = tmpSubJob
        txtJTaupJobDesc.Text = tmpJobDesc
        mtxtJTaupJDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTaupIDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        ' Disababling Amount field for CPWD jobs.
        If txtJTaupCategories.Text.Substring(0, 5) = "Cost+" Then
            txtJTaupJAmt.Text = "N/A"
            txtJTaupJAmt.ReadOnly = True
        Else
            txtJTaupJAmt.Text = ""
            txtJTaupJAmt.ReadOnly = False
        End If
        Dim keysJ As ICollection = slJTaupJob.Keys
        Dim k As String
        '
        For Each k In keysJ
            If slJTaupJob(k).JTaupJJob = tmpJob Then
                If slJTaupJob(k).JTaupJSubJob = tmpSubJob Then
                    ' add item to listview
                    Dim itmBuild(6) As String
                    Dim lvLine As ListViewItem
                    '
                    ' move items to listview

                    itmBuild(0) = slJTaupJob(k).JTaupJType
                    itmBuild(1) = slJTaupJob(k).JTaupJDate
                    itmBuild(2) = slJTaupJob(k).JTaupJDesc
                    itmBuild(3) = slJTaupJob(k).JTaupJApprvd
                    itmBuild(4) = slJTaupJob(k).JTaupJAmt
                    itmBuild(5) = slJTaupJob(k).JTaupJRefDoc
                    '
                    lvLine = New ListViewItem(itmBuild)
                    lvJTaupJobDetails.Items.Add(lvLine)
                    '
                End If
            End If
        Next
        '
        Dim keysI As ICollection = slJTaupInv.Keys
        '
        For Each k In keysI
            If slJTaupInv(k).JTaupIJob = tmpJob Then
                If slJTaupInv(k).JTaupISubJob = tmpSubJob Then
                    ' add item to listview
                    Dim itmBuild(7) As String
                    Dim lvLine As ListViewItem
                    '
                    ' move items to listview
                    '
                    itmBuild(0) = slJTaupInv(k).JTaupIDate
                    itmBuild(1) = slJTaupInv(k).JTaupIDesc
                    itmBuild(2) = slJTaupInv(k).JTaupIAmt
                    itmBuild(3) = slJTaupInv(k).JTaupIAfterDt
                    itmBuild(4) = slJTaupInv(k).JTaupIFinal
                    itmBuild(5) = slJTaupInv(k).JTaupIInvDate
                    itmBuild(6) = slJTaupInv(k).JTaupIInvNum
                    '
                    lvLine = New ListViewItem(itmBuild)
                    lvJTaupInvDetails.Items.Add(lvLine)
                    '
                End If
            End If
        Next
        JTaupUpdtPanelTotals()
        JTstart.JTstartMenuOff()
        JTstart.JTstartHideTitles()
        ' clearing out panel fields to prevent data being displayed from a previous job being displayed mistakenly again.
        txtJTaupApproval.Text = ""
        txtJTaupJAmt.Text = ""
        txtJTaupRefDoc.Text = ""
        rtxtJTaupJDesc.Text = ""
        rtxtJTaupIDesc.Text = ""
        txtJTaupIAmt.Text = ""
        '
        Me.MdiParent = JTstart
        Me.Show()
    End Sub


    Sub JTaupUpdtPanelTotals()
        ' --------------------------------------------------------------------------------
        ' This sub passed the LVs and recalculates all the totals for the panel.
        ' It will be called everytime the listviews are modified.
        ' --------------------------------------------------------------------------------
        Dim tmpInitAmt As Decimal = 0
        Dim tmpChgAmt As Decimal = 0
        Dim tmpJobTotal As Decimal = 0
        '
        Dim tmpInvdTD As Decimal = 0
        Dim tmpInvQueue As Decimal = 0
        Dim tmpInvTotal As Decimal = 0
        '
        Dim tmpJobMinusInv As Decimal = 0
        '
        Dim tmpIdx As Integer = 0
        Do While tmpIdx < lvJTaupJobDetails.Items.Count
            If lvJTaupJobDetails.Items(tmpIdx).Text = "I" Then
                tmpInitAmt = tmpInitAmt + Val(lvJTaupJobDetails.Items(tmpIdx).SubItems(4).Text)
            Else
                tmpChgAmt = tmpChgAmt + Val(lvJTaupJobDetails.Items(tmpIdx).SubItems(4).Text)
            End If
            tmpIdx += 1
        Loop
        tmpJobTotal = tmpInitAmt + tmpChgAmt
        '
        tmpIdx = 0
        Do While tmpIdx < lvJTaupInvDetails.Items.Count
            If lvJTaupInvDetails.Items(tmpIdx).SubItems(5).Text <> "" Then
                tmpInvdTD = tmpInvdTD + Val(lvJTaupInvDetails.Items(tmpIdx).SubItems(2).Text)
            Else
                tmpInvQueue = tmpInvQueue + Val(lvJTaupInvDetails.Items(tmpIdx).SubItems(2).Text)
            End If
            tmpIdx += 1
        Loop
        tmpInvTotal = tmpInvdTD + tmpInvQueue
        tmpJobMinusInv = tmpJobTotal - tmpInvdTD
        ' move totals to panel
        '
        txtJTaupInitAmt.Text = String.Format("{0:0.00}", tmpInitAmt)
        txtJTaupAppvedChgs.Text = String.Format("{0:0.00}", tmpChgAmt)
        txtJTaupTlAupAmt.Text = String.Format("{0:0.00}", tmpJobTotal)
        '
        txtJTaupInvdTD.Text = String.Format("{0:0.00}", tmpInvdTD)
        txtJTaupQueAmt.Text = String.Format("{0:0.00}", tmpInvQueue)
        txtJTaupTDQue.Text = String.Format("{0:0.00}", tmpInvTotal)
        txtJTaupAupLessInvdTD.Text = String.Format("{0:0.00}", tmpJobMinusInv)
    End Sub
    Private Sub lvJTaupJobDetails_DoubleClick(sender As Object, e As EventArgs) Handles lvJTaupJobDetails.DoubleClick
        ' ----------------------------------------------------------------------------------------
        ' Populates editing area with selected line and then deletes the line ... Jobs lv
        ' ----------------------------------------------------------------------------------------
        If txtJTaupJAmt.Text <> "" Then
            ToolTip1.Show("Data exists in editing area. Clear before selecting an item to edit.(AUP.002)", lvJTaupJobDetails, 80, 10, 7000)
            Exit Sub
        End If
        mtxtJTaupJDate.Text = lvJTaupJobDetails.SelectedItems(0).SubItems(1).Text
        If lvJTaupJobDetails.SelectedItems(0).Text = "I" Then
            rbtnJTaupInit.Checked = True
        Else
            rbtnJTaupChg.Checked = True
        End If
        rtxtJTaupJDesc.Text = lvJTaupJobDetails.SelectedItems(0).SubItems(2).Text
        txtJTaupRefDoc.Text = lvJTaupJobDetails.SelectedItems(0).SubItems(5).Text
        txtJTaupJAmt.Text = lvJTaupJobDetails.SelectedItems(0).SubItems(4).Text
        txtJTaupApproval.Text = lvJTaupJobDetails.SelectedItems(0).SubItems(3).Text
        '
        lvJTaupJobDetails.SelectedItems(0).Remove()
        JTaupUpdtPanelTotals()
        Me.Hide()
        Me.Show()
        mtxtJTaupJDate.Select()
    End Sub

    Private Sub lvJTaupInvDetails_DoubleClick(sender As Object, e As EventArgs) Handles lvJTaupInvDetails.DoubleClick
        ' ----------------------------------------------------------------------------------------
        ' Populates editing area with selected line and then deletes the line ... Inv lv. If
        ' the selected entry has already been invoiced, error message is presented.
        ' ----------------------------------------------------------------------------------------
        If txtJTaupIAmt.Text <> "" Then
            ToolTip1.Show("Data exists in editing area. Clear before selecting an item to edit.(AUP.003)", lvJTaupInvDetails, 80, 10, 7000)
            Exit Sub
        End If
        If lvJTaupInvDetails.SelectedItems(0).SubItems(5).Text <> "" Then
            ToolTip1.Show("Item selected has been invoiced --- it can no longer be modified.(AUP.001)", lvJTaupInvDetails, 80, 10, 7000)
            Exit Sub
        End If
        mtxtJTaupIDate.Text = lvJTaupInvDetails.SelectedItems(0).Text
        rtxtJTaupIDesc.Text = lvJTaupInvDetails.SelectedItems(0).SubItems(1).Text
        txtJTaupIAmt.Text = lvJTaupInvDetails.SelectedItems(0).SubItems(2).Text
        If lvJTaupInvDetails.SelectedItems(0).SubItems(3).Text = "X" Then
            cboxJTaupQueAfterTranDt.Checked = True
        End If
        If lvJTaupInvDetails.SelectedItems(0).SubItems(4).Text = "X" Then
            cboxJTaupFinalInv.Checked = True
        End If
        '
        lvJTaupInvDetails.SelectedItems(0).Remove()
        '
        JTaupUpdtPanelTotals()
        Me.Hide()
        Me.Show()
        mtxtJTaupJDate.Select()
    End Sub

    Private Sub btnJTaupJCancel_Click(sender As Object, e As EventArgs) Handles btnJTaupJCancel.Click, btnJTaupJDelete.Click
        ' -----------------------------------------------------------------------------
        ' Init Job and Chg desc --- Cancel and Delete --- Clear fields in editing area.
        ' For deleted records, lv entry has already been cleared.
        ' -----------------------------------------------------------------------------
        mtxtJTaupJDate.Text = ""
        rbtnJTaupInit.Checked = False
        rbtnJTaupChg.Checked = False
        rtxtJTaupJDesc.Text = ""
        txtJTaupRefDoc.Text = ""
        txtJTaupJAmt.Text = ""
        txtJTaupApproval.Text = ""
        mtxtJTaupJDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTaupIDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        Me.Hide()
        Me.Show()
        mtxtJTaupJDate.Select()
    End Sub

    Private Sub btnJTaupICancel_Click(sender As Object, e As EventArgs) Handles btnJTaupICancel.Click, btnJTaupIDelete.Click
        ' ---------------------------------------------------------------------------------
        ' Invoicing Items --- Cancel and Delete --- Clear fields in editing area. 
        ' For deleted records, lv entry has already been cleared.
        ' ---------------------------------------------------------------------------------
        mtxtJTaupIDate.Text = ""
        rtxtJTaupIDesc.Text = ""
        txtJTaupIAmt.Text = ""
        cboxJTaupFinalInv.Checked = False
        cboxJTaupQueAfterTranDt.Checked = False
        mtxtJTaupJDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTaupIDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        Me.Hide()
        Me.Show()
        mtxtJTaupIDate.Select()
    End Sub

    Private Sub btnJTaupJAddUpdt_Click(sender As Object, e As EventArgs) Handles btnJTaupJAddUpdt.Click
        ' -----------------------------------------------------------------------------
        ' Moves data from entry area into LV for job def and chg entries.
        ' -----------------------------------------------------------------------------
        If rtxtJTaupJDesc.Text = "" Then
            ToolTip1.Show("A description is required for this entry.(AUP.006)", rtxtJTaupJDesc, 20, -55, 6000)
            rtxtJTaupJDesc.Select()
            Exit Sub
        End If
        If rbtnJTaupInit.Checked = True Then
            Dim tmpIndex As Integer = 0
            Do While tmpIndex < lvJTaupJobDetails.Items.Count
                If lvJTaupJobDetails.Items(tmpIndex).Text = "I" Then
                    ToolTip1.Show("Only one (I)nitial job item allowed.(AUP.007)", rbtnJTaupInit, 20, -55, 6000)
                    Exit Sub
                End If
                tmpIndex += 1
            Loop
        End If
        ' add item to listview
        Dim itmBuild(6) As String
        Dim lvLine As ListViewItem
        '
        ' move items to listview
        If rbtnJTaupInit.Checked = True Then
            itmBuild(0) = "I"
        Else
            itmBuild(0) = "C"
        End If
        itmBuild(1) = mtxtJTaupJDate.Text
        itmBuild(2) = rtxtJTaupJDesc.Text
        itmBuild(3) = txtJTaupApproval.Text
        If txtJTaupCategories.Text.Substring(0, 5) = "Cost+" Then
            itmBuild(4) = ""
        Else
            itmBuild(4) = txtJTaupJAmt.Text
        End If
        itmBuild(5) = txtJTaupRefDoc.Text
        '
        lvLine = New ListViewItem(itmBuild)
        lvJTaupJobDetails.Items.Add(lvLine)
        '
        mtxtJTaupJDate.Text = ""
        rbtnJTaupInit.Checked = False
        rbtnJTaupChg.Checked = False
        rtxtJTaupJDesc.Text = ""
        txtJTaupRefDoc.Text = ""
        txtJTaupJAmt.Text = ""
        txtJTaupApproval.Text = ""
        mtxtJTaupJDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTaupIDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        JTaupUpdtPanelTotals()
        Me.Hide()
        Me.Show()
        mtxtJTaupJDate.Select()
    End Sub

    Private Sub btnJTaupIAddUpdt_Click(sender As Object, e As EventArgs) Handles btnJTaupIAddUpdt.Click
        ' -----------------------------------------------------------------------------
        ' Moves data from entry area into LV for invoicing entries.
        ' -----------------------------------------------------------------------------
        If rtxtJTaupIDesc.Text = "" Then
            ToolTip1.Show("A description is required for this entry.(AUP.006)", rtxtJTaupIDesc, 20, -55, 6000)
            rtxtJTaupIDesc.Select()
            Exit Sub
        End If
        ' add item to listview
        Dim itmBuild(7) As String
        Dim lvLine As ListViewItem
        '
        ' move items to listview

        itmBuild(0) = mtxtJTaupIDate.Text
        itmBuild(1) = rtxtJTaupIDesc.Text
        itmBuild(2) = txtJTaupIAmt.Text
        If cboxJTaupQueAfterTranDt.Checked = True Then
            itmBuild(3) = "X"
        Else
            itmBuild(3) = ""
        End If
        If cboxJTaupFinalInv.Checked = True Then
            itmBuild(4) = "X"
        Else
            itmBuild(4) = ""
        End If
        itmBuild(5) = ""
        itmBuild(6) = ""
        '
        lvLine = New ListViewItem(itmBuild)
        lvJTaupInvDetails.Items.Add(lvLine)
        '
        mtxtJTaupIDate.Text = ""
        rtxtJTaupIDesc.Text = ""
        txtJTaupIAmt.Text = ""
        cboxJTaupFinalInv.Checked = False
        cboxJTaupQueAfterTranDt.Checked = False
        mtxtJTaupJDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTaupIDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        JTaupUpdtPanelTotals()
        Me.Hide()
        Me.Show()
        mtxtJTaupIDate.Select()
    End Sub

    Private Sub mtxtJTaupJDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTaupJDate.LostFocus
        ' -------------------------------------------------------------------------------
        '  mtxt date validation ... Job and chg entry section.
        ' -------------------------------------------------------------------------------
        '
        ' check for entry with no year ... append year prior to validation
        '
        If mtxtJTaupJDate.Text <> "  /  /" Then
            mtxtJTaupJDate.Text = mtxtJTaupJDate.Text.Replace(" ", "0")
            '
            If mtxtJTaupJDate.Text.Length = 6 Then
                If mtxtJTaupJDate.Text.Substring(0, 2) > Today.Month Then
                    mtxtJTaupJDate.Text = mtxtJTaupJDate.Text & Today.Year - 1

                Else
                    mtxtJTaupJDate.Text = mtxtJTaupJDate.Text & Today.Year
                End If
            End If
        End If

        If IsDate(mtxtJTaupJDate.Text) = False Then
            ToolTip1.Show("Invalid Date entered not valid. Correct and resubmit.(AUP.004)", mtxtJTaupJDate, 20, -55, 6000)
            mtxtJTaupJDate.Select()
            mtxtJTaupJDate.SelectionStart = 0

            Exit Sub
        End If
        rtxtJTaupJDesc.Select()
    End Sub
    Private Sub mtxtJTaugIDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTaupIDate.LostFocus
        ' -------------------------------------------------------------------------------
        '  mtxt date validation ... Job and chg entry section.
        ' -------------------------------------------------------------------------------
        '
        ' check for entry with no year ... append year prior to validation
        '
        If mtxtJTaupIDate.Text <> "  /  /" Then
            mtxtJTaupIDate.Text = mtxtJTaupIDate.Text.Replace(" ", "0")
            '
            If mtxtJTaupIDate.Text.Length = 6 Then
                If mtxtJTaupIDate.Text.Substring(0, 2) > Today.Month Then
                    mtxtJTaupIDate.Text = mtxtJTaupIDate.Text & Today.Year - 1

                Else
                    mtxtJTaupIDate.Text = mtxtJTaupIDate.Text & Today.Year
                End If
            End If
        End If

        If IsDate(mtxtJTaupIDate.Text) = False Then
            ToolTip1.Show("Invalid Date entered not valid. Correct and resubmit.(AUP.005)", mtxtJTaupIDate, 20, -55, 6000)
            mtxtJTaupIDate.Select()
            mtxtJTaupIDate.SelectionStart = 0

            Exit Sub
        End If
        rtxtJTaupIDesc.Select()
    End Sub
    Private Sub txtJTaupJAmt_LostFocus(sender As Object, e As EventArgs) Handles txtJTaupJAmt.LostFocus
        txtJTaupJAmt.Text = String.Format("{0:0.00}", Val(txtJTaupJAmt.Text))
    End Sub
    Private Sub txtJTaupIAmt_LostFocus(sender As Object, e As EventArgs) Handles txtJTaupIAmt.LostFocus
        txtJTaupIAmt.Text = String.Format("{0:0.00}", Val(txtJTaupIAmt.Text))
    End Sub

    Private Sub btnJTaupUpdtReturn_Click(sender As Object, e As EventArgs) Handles btnJTaupUpdtReturn.Click
        ' -----------------------------------------------------------------------
        ' Sub to update all items in the JTaup listviews into the sortedlists. It
        ' will also flush the values to Disk(JTAUP.dat).
        ' -----------------------------------------------------------------------
        '
        ' ----------------------------------------------------------------------------------
        ' Prior to updating records, make sure that no transaction were left hanging ---
        ' data left in fields. If something found, go back and clean it up prior to exiting.
        ' ----------------------------------------------------------------------------------
        If rtxtJTaupIDesc.Text <> "" Or
        rtxtJTaupJDesc.Text <> "" Or
        txtJTaupIAmt.Text <> "" Or
        txtJTaupJAmt.Text <> "" Then
            MsgBox("Data left in the transaction editing fields. it must be" & vbCrLf &
                   "deleted or updated prior to exiting the AUP panel. (AUP.008)")
        End If

        Dim keysInv As ICollection = slJTaupInv.Keys
        ' delete items for this job in slJTaupInv. New values will be added from LvJTaupInv.
        Dim k As Integer = 0

        Do While k <keysInv.Count
            If txtJTaupJobID.Text = slJTaupInv(keysInv(k)).JTaupIJob And
                txtJTaupSubID.Text = slJTaupInv(keysInv(k)).JTaupISubJob Then
                slJTaupInv.Remove(keysInv(k))
            Else
                k += 1
            End If
        Loop
        ' delete items for this job in slJTaupJob. New values will be added from LvJTaupJob.
        Dim keysJob As IList = slJTaupJob.Keys
        k = 0
        '
        Do While k < keysJob.Count
            If txtJTaupJobID.Text = slJTaupJob(keysJob(k)).JTaupJJob And
                txtJTaupSubID.Text = slJTaupJob(keysJob(k)).JTaupJSubJob Then
                slJTaupJob.Remove(keysJob(k))
            Else
                k += 1
            End If
        Loop
        ' Add items in LVs to SLs.
        Dim tmpSeq As Integer = 10
        Dim tmpAddItemI As JTaupInvStru
        Dim tmpIndex As Integer = 0
        Do While tmpIndex < lvJTaupInvDetails.Items.Count
            tmpAddItemI.JTaupIJob = txtJTaupJobID.Text
            tmpAddItemI.JTaupISubJob = txtJTaupSubID.Text
            tmpAddItemI.JTaupIDate = lvJTaupInvDetails.Items(tmpIndex).Text
            tmpAddItemI.JTaupIDesc = lvJTaupInvDetails.Items(tmpIndex).SubItems(1).Text
            tmpAddItemI.JTaupIAmt = lvJTaupInvDetails.Items(tmpIndex).SubItems(2).Text
            tmpAddItemI.JTaupIAfterDt = lvJTaupInvDetails.Items(tmpIndex).SubItems(3).Text
            tmpAddItemI.JTaupIFinal = lvJTaupInvDetails.Items(tmpIndex).SubItems(4).Text
            tmpAddItemI.JTaupIInvDate = lvJTaupInvDetails.Items(tmpIndex).SubItems(5).Text
            tmpAddItemI.JTaupIInvNum = lvJTaupInvDetails.Items(tmpIndex).SubItems(6).Text
            JTaupslJTaupInvAddRecord(tmpAddItemI, "Called from btnJTaupUpdtReturn")
            tmpIndex += 1
        Loop
        '
        tmpSeq = 10
        Dim tmpAddItemJ As JTaupJOBStru
        tmpIndex = 0
        Do While tmpIndex < lvJTaupJobDetails.Items.Count
            tmpAddItemJ.JTaupJJob = txtJTaupJobID.Text
            tmpAddItemJ.JTaupJSubJob = txtJTaupSubID.Text
            tmpAddItemJ.JTaupJType = lvJTaupJobDetails.Items(tmpIndex).Text
            tmpAddItemJ.JTaupJDate = lvJTaupJobDetails.Items(tmpIndex).SubItems(1).Text
            tmpAddItemJ.JTaupJDesc = lvJTaupJobDetails.Items(tmpIndex).SubItems(2).Text
            tmpAddItemJ.JTaupJAmt = lvJTaupJobDetails.Items(tmpIndex).SubItems(4).Text
            tmpAddItemJ.JTaupJApprvd = lvJTaupJobDetails.Items(tmpIndex).SubItems(3).Text
            tmpAddItemJ.JTaupJRefDoc = lvJTaupJobDetails.Items(tmpIndex).SubItems(5).Text
            Dim tmpkey As String = tmpAddItemJ.JTaupJJob & tmpAddItemJ.JTaupJSubJob & tmpAddItemJ.JTaupJDate.Substring(6, 4) _
                & tmpAddItemJ.JTaupJDate.Substring(0, 2) & tmpAddItemJ.JTaupJDate.Substring(3, 2) & tmpSeq
            Do While slJTaupJob.ContainsKey(tmpkey)
                tmpSeq += 1
                tmpkey = tmpAddItemJ.JTaupJJob & tmpAddItemJ.JTaupJSubJob & tmpAddItemJ.JTaupJDate.Substring(6, 4) _
                & tmpAddItemJ.JTaupJDate.Substring(0, 2) & tmpAddItemJ.JTaupJDate.Substring(3, 2) & tmpSeq
            Loop
            slJTaupJob.Add(tmpkey, tmpAddItemJ)
            tmpIndex += 1
        Loop
        '
        lvJTaupJobDetails.Items.Clear()
        lvJTaupInvDetails.Items.Clear()
        JTaupFlush()
        Me.Hide()
        ' ------------------------------------------------------------------
        ' Check to see if this job has an I record. If not give msg and 
        ' circle back to panel.
        ' ------------------------------------------------------------------
        If JTaupIentryChk(txtJTaupJobID.Text, txtJTaupSubID.Text) <> True Then
            Dim tmpMsgBoxText As String =
                "All versions of AUP Jobs require an ""I"" entry that defines the initial" & vbCrLf &
                "scope of the job as well as an Agreed Upon Price quote (unless this job" & vbCrLf &
                "is a COST+ with Deposits job). The system will navigate back to the AUP" & vbCrLf &
                "screen to allow the ""I"" record entry." & vbCrLf & vbCrLf &
                "While it Is Not mandatory, it Is also recommended that Job Invoicing Or" & vbCrLf &
                "Deposit payments are scheduled. They will Not be queued For invoicing" & vbCrLf &
                "until the Date entered has past. (AUP.010)"
            MsgBox(tmpMsgBoxText)
            JTaupInit(txtJTaupJobID.Text, txtJTaupSubID.Text, txtJTaupJobDesc.Text)
            Exit Sub
        End If
        '
        If JTaupSum.JTaupSumCameFromHere = True Then
            JTaupSum.JTaupSumInit()
            Exit Sub
        End If
        JTaupFillJTjim(txtJTaupJobID.Text, txtJTaupSubID.Text)
        JTstart.JTstartShowTitles()
        JTjim.Show()
    End Sub

    Private Sub btnJTaupCancel_Click(sender As Object, e As EventArgs) Handles btnJTaupCancel.Click
        ' -------------------------------------------------------------------------
        ' Erase any activity and/or changes made in this session. Clear values from 
        ' panels listviews.
        ' -------------------------------------------------------------------------
        rtxtJTaupIDesc.Text = Nothing
        rtxtJTaupJDesc.Text = Nothing
        txtJTaupIAmt.Text = Nothing
        txtJTaupJAmt.Text = Nothing
        lvJTaupJobDetails.Items.Clear()
        lvJTaupInvDetails.Items.Clear()
        Me.Hide()
        If JTaupSum.JTaupSumCameFromHere = True Then
            JTaupSum.JTaupSumInit()
            Exit Sub
        End If
        JTstart.JTstartShowTitles()
        JTjim.Show()
    End Sub
    ''' <summary>
    ''' Sub to fill fields on the CPWD and AUP groupboxes on JTjim. It filled them both
    ''' no matter how the job is coded. The JTjim logic will determine which groupbox
    ''' will be visible.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    Public Sub JTaupFillJTjim(tmpJobID As String, tmpSubID As String)
        Dim tmpQuotedAmt As Decimal = 0
        Dim tmpInvdTD As Decimal = 0
        Dim tmpNetRemain As Decimal = 0
        Dim tmpQueueAmt As Decimal = 0
        '
        ' ----------------------------------------------------------------
        ' Fields for CPWD groupbox on JTjim
        ' ----------------------------------------------------------------
        Dim tmpInvdDep As Decimal = 0
        Dim tmpInvdAct As Decimal = 0
        Dim tmpUnbilldAct As Decimal = 0
        Dim tmpSchedDep As Decimal = 0
        '
        Dim keysJ As ICollection = slJTaupJob.Keys
        Dim k As String
        For Each k In keysJ
            If slJTaupJob(k).JTaupJJob = tmpJobID And slJTaupJob(k).JTaupJSubJob = tmpSubID Then
                tmpQuotedAmt = tmpQuotedAmt + Val(slJTaupJob(k).JTaupJamt)
            End If
        Next
        Dim keysI As ICollection = slJTaupInv.Keys
        For Each k In keysI
            If slJTaupInv(k).JTaupIJob = tmpJobID And slJTaupInv(k).JTaupISubJob = tmpSubID Then
                ' Accumulate amounts for AUP JTjim groupbox
                If slJTaupInv(k).JTaupIInvDate = "" Then
                    tmpQueueAmt = tmpQueueAmt + Val(slJTaupInv(k).JTaupIamt)
                Else
                    tmpInvdTD = tmpInvdTD + Val(slJTaupInv(k).JTaupIamt)
                End If
                ' Accumulate amount for CPWD JTjim groupbox
                If slJTaupInv(k).JTaupIInvDate = "" Then
                    tmpSchedDep += Val(slJTaupInv(k).JTaupIamt)
                Else
                    If Val(slJTaupInv(k).JTaupIamt) > 0 Then
                        tmpInvdDep += Val(slJTaupInv(k).JTaupIamt)
                    Else
                        tmpInvdAct += (Val(slJTaupInv(k).JTaupIamt) * -1)
                    End If
                End If
            End If
        Next
        ' Fill JTjim AUP groupbox fields
        tmpNetRemain = tmpQuotedAmt - tmpInvdTD
        JTjim.txtJTjimAUPQuoted.Text = String.Format("{0:0.00}", tmpQuotedAmt)
        JTjim.txtJTjimAUPInvdTD.Text = String.Format("{0:0.00}", tmpInvdTD)
        JTjim.txtJTjimAUPNetRemain.Text = String.Format("{0:0.00}", tmpNetRemain)
        JTjim.txtJTjimAUPQueueAmt.Text = String.Format("{0:0.00}", tmpQueueAmt)
        '
        ' Fill JTjim CPWD groupbox fields.
        JTjim.txtJTjimCPWDInvdDep.Text = String.Format("{0:0.00}", tmpInvdDep)
        JTjim.txtJTjimCPWDInvdAct.Text = String.Format("{0:0.00}", tmpInvdAct)
        JTjim.txtJTjimCPWDDepBal.Text = String.Format("{0:0.00}", tmpInvdDep - tmpInvdAct)
        '
        Dim tmpNotInvLab As Decimal = 0
        Dim tmpNotInvMat As Decimal = 0
        Dim tmpNotInvSub As Decimal = 0
        JTMainMenu.JTMMNotInvValues(tmpJobID, tmpSubID, tmpNotInvLab, tmpNotInvMat, tmpNotInvSub)
        '
        JTjim.txtJTjimCPWDUnbilldAct.Text = String.Format("{0:0.00}", tmpNotInvLab + tmpNotInvMat + tmpNotInvSub)
        JTjim.txtJTjimCPWDSchedDep.Text = String.Format("{0:0.00}", tmpSchedDep)
    End Sub
    Public Function JTAUPasExt(tmpJobID As String, tmpSubID As String, ByRef tmpSLkey As String, ByRef tmpSLaupItem As JTaupInvStru) As String

        Dim tmpSLKeySave As String = tmpSLkey
        Dim aupkey As ICollection = slJTaupInv.Keys
        If tmpSLKeySave = "FIRST" Then
            JTaupKeyCollPos = 0
            tmpSLKeySave = ""
        End If
        Do While JTaupKeyCollPos < aupkey.Count
            If tmpJobID = slJTaupInv(aupkey(JTaupKeyCollPos)).JTaupIJob _
                And (tmpSubID = slJTaupInv(aupkey(JTaupKeyCollPos)).JTaupISubJob Or tmpSubID = "MSCOMB") _
                And slJTaupInv(aupkey(JTaupKeyCollPos)).JTaupIInvDate = "" Then
                Dim tmpInvDate As Date = CDate(slJTaupInv(aupkey(JTaupKeyCollPos)).JTaupIDate)
                If slJTaupInv(aupkey(JTaupKeyCollPos)).JTaupIAfterDt = "X" Then
                    If Date.Today >= tmpInvDate Then
                        tmpSLkey = aupkey(JTaupKeyCollPos)
                        tmpSLaupItem = slJTaupInv.Item(aupkey(JTaupKeyCollPos))
                        JTaupKeyCollPos += 1
                        Return "DATA"
                        '
                    End If
                Else
                    tmpSLkey = aupkey(JTaupKeyCollPos)
                    tmpSLaupItem = slJTaupInv.Item(aupkey(JTaupKeyCollPos))
                    JTaupKeyCollPos += 1
                    Return "DATA"
                End If
            End If
            JTaupKeyCollPos += 1
        Loop
        '
        Return "DONE"
    End Function
    ''' <summary>
    ''' Function checks to see if final invoice flag is set. It also checked to make sure that that invoice has been issued.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <returns></returns>
    Public Function JTaupFinalFlagSet(tmpJobID As String, tmpSubID As String) As Boolean
        Dim FlagSet As Boolean = False
        Dim key As ICollection = slJTaupInv.Keys
        Dim k As String = ""
        For Each k In key
            If slJTaupInv(k).JTaupIJob = tmpJobID Then
                If slJTaupInv(k).JTaupISubJob = tmpSubID Then
                    If slJTaupInv(k).JTaupIFinal <> "" Then
                        If slJTaupInv(k).JTaupIInvNum <> "" Then
                            FlagSet = True
                        End If
                    End If
                End If
            End If
        Next
        Return FlagSet
    End Function
    ''' <summary>
    ''' Function to set final flag. Flag will be set on first invoice found ... I
    ''' think that is the late one ... but not sure. Shouldn't make a difference.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <returns></returns>
    Public Function JTaupSetFinalFlag(tmpJobID As String, tmpSubID As String) As Boolean
        Dim FlagSet As Boolean = False
        Dim key As ICollection = slJTaupInv.Keys
        Dim k As String = ""
        For Each k In key
            If slJTaupInv(k).JTaupIJob = tmpJobID Then
                If slJTaupInv(k).JTaupISubJob = tmpSubID Then
                    slJTaupInv(k).JTaupIFinal = "X"
                    FlagSet = True
                End If
            End If
        Next
        Return FlagSet
    End Function
    Public Function JTaupPurgeAUPInfo(tmppjob As String, tmppsubjob As String) As String
        Dim tmpJPurgeCnt As Integer = 0
        Dim tmpIPurgeCnt As Integer = 0
        '       Dim tmpCount As Integer = 0
        '
        ' Search for Job Records
        '
        Dim tmpSaveKeys As New List(Of String)
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String = Nothing
        For Each k In key
            If slJTaupJob(k).JTaupJJob = tmppjob Then
                If slJTaupJob(k).JTaupJSubJob = tmppsubjob Then
                    tmpSaveKeys.Add(k)
                End If
            End If
        Next
        If tmpSaveKeys.Count > 0 Then
            For Each k In tmpSaveKeys
                slJTaupJob.Remove(k)
                tmpJPurgeCnt += 1
            Next
        End If
        Dim tmpSub As Integer = 0
        '       Do While tmpSub < tmpSaveKeys.Count

        '     Loop
        '
        ' Search for invoice records
        '
        key = slJTaupInv.Keys
        tmpSaveKeys = New List(Of String)
        For Each k In key

            If slJTaupInv(k).JTaupIJob = tmppjob Then
                If slJTaupInv(k).JTaupISubJob = tmppsubjob Then
                    tmpSaveKeys.Add(k)
                    '               slJTaupInv(k).remove
                    '              tmpIPurgeCnt += 1
                End If
            End If
        Next
        If tmpSaveKeys.Count > 0 Then

            For Each k In tmpSaveKeys
                slJTaupInv.Remove(k)
                tmpIPurgeCnt += 1
            Next
        End If
        '
        If tmpIPurgeCnt + tmpJPurgeCnt > 0 Then
            JTaupFlush()
        End If
        Return ""
    End Function
    ''' <summary>
    ''' Function to check to see if this job has AUP records. Used to warn operator if the 
    ''' pricing method is being charged from AUP to Cost+.
    ''' </summary>
    ''' <param name="tmpCust"></param>
    ''' <param name="tmpJob"></param>
    ''' <returns></returns>
    Public Function JTaupChkforRecords(tmpCust As String, tmpJob As String) As Boolean
        ' Search for Job Records
        '
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String = ""
        For tmpCount = 0 To key.Count - 1
            k = key(tmpCount)
            If slJTaupJob(k).JTaupJJob = tmpCust Then
                If slJTaupJob(k).JTaupJSubJob = tmpJob Then
                    Return True
                End If
            End If
        Next
        '
        ' Search for invoice records
        '
        key = slJTaupInv.Keys

        For tmpCount = 0 To key.Count - 1
            k = key(tmpCount)
            If slJTaupInv(k).JTaupIJob = tmpCust Then
                If slJTaupInv(k).JTaupISubJob = tmpJob Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function

    Private Sub MtxtJTaupMIdate1_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate1.Leave
        If mtxtJTaupMIdate1.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate1.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate1, 80, 10, 7000)
            mtxtJTaupMIdate1.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd1.Checked = True
        rtxtJTaupMIdesc1.Select()
    End Sub

    Private Sub MtxtJTaupMIdate2_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate2.Leave
        If mtxtJTaupMIdate2.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate2.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate2, 80, 10, 7000)
            mtxtJTaupMIdate2.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd2.Checked = True
        rtxtJTaupMIdesc2.Select()
    End Sub

    Private Sub MtxtJTaupMIdate3_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate3.Leave
        If mtxtJTaupMIdate3.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate3.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate3, 80, 10, 7000)
            mtxtJTaupMIdate3.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd3.Checked = True
        rtxtJTaupMIdesc3.Select()
    End Sub

    Private Sub MtxtJTaupMIdate4_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate4.Leave
        If mtxtJTaupMIdate4.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate4.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate4, 80, 10, 7000)
            mtxtJTaupMIdate4.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd4.Checked = True
        rtxtJTaupMIdesc4.Select()
    End Sub

    Private Sub MtxtJTaupMIdate5_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate5.Leave
        If mtxtJTaupMIdate5.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate5.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate5, 80, 10, 7000)
            mtxtJTaupMIdate5.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd5.Checked = True
        rtxtJTaupMIdesc5.Select()
    End Sub

    Private Sub MtxtJTaupMIdate6_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate6.Leave
        If mtxtJTaupMIdate6.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate6.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate6, 80, 10, 7000)
            mtxtJTaupMIdate6.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd6.Checked = True
        rtxtJTaupMIdesc6.Select()
    End Sub

    Private Sub MtxtJTaupMIdate7_Leave(sender As Object, e As EventArgs) Handles mtxtJTaupMIdate7.Leave
        If mtxtJTaupMIdate7.Text = tmpEmptyDate Then
            Exit Sub
        End If
        If TestForValidDate(mtxtJTaupMIdate7.Text) = False Then
            ToolTip1.Show("Invalid Date entered ... Correct And resubmit.(AUP.009)", mtxtJTaupMIdate7, 80, 10, 7000)
            mtxtJTaupMIdate7.Focus()
            Exit Sub
        End If
        cboxJTaupMIqatd7.Checked = True
        rtxtJTaupMIdesc7.Select()
    End Sub

    Private Sub txtJTaupMIamt1_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt1.Leave
        txtJTaupMIamt1.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt1.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt2_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt2.Leave
        txtJTaupMIamt2.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt2.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt3_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt3.Leave
        txtJTaupMIamt3.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt3.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt4_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt4.Leave
        txtJTaupMIamt4.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt4.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt5_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt5.Leave
        txtJTaupMIamt5.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt5.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt6_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt6.Leave
        txtJTaupMIamt6.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt6.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub
    Private Sub txtJTaupMIamt7_Leave(sender As Object, e As EventArgs) Handles txtJTaupMIamt7.Leave
        txtJTaupMIamt7.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt7.Text))
        JTaupMIUpdtTotl()
        btnJTaupMIAddUpdt.Select()
    End Sub


    Private Function JTaupMIUpdtTotl() As String
        txtJTaupMItotal.Text = String.Format("{0:0.00}", Val(txtJTaupMIamt1.Text) + Val(txtJTaupMIamt2.Text) +
                                                         Val(txtJTaupMIamt3.Text) + Val(txtJTaupMIamt4.Text) +
                                                         Val(txtJTaupMIamt5.Text) + Val(txtJTaupMIamt6.Text) +
                                                         Val(txtJTaupMIamt7.Text))
        Return ""
    End Function

    Private Sub BtnJTaupMIAddUpdt_Click(sender As Object, e As EventArgs) Handles btnJTaupMIAddUpdt.Click
        If TestForValidDate(mtxtJTaupMIdate1.Text) = True Then
            ' Disable button to avaoid possible double-clicks
            btnJTaupMIAddUpdt.Enabled = False
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate1.Text
            itmBuild(1) = rtxtJTaupMIdesc1.Text
            itmBuild(2) = txtJTaupMIamt1.Text
            If cboxJTaupMIqatd1.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi1.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate1.Text = Nothing
            rtxtJTaupMIdesc1.Text = Nothing
            txtJTaupMIamt1.Text = Nothing
            cboxJTaupMIqatd1.Checked = False
            cboxJTaupMIfi1.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate2.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate2.Text
            itmBuild(1) = rtxtJTaupMIdesc2.Text
            itmBuild(2) = txtJTaupMIamt2.Text
            If cboxJTaupMIqatd2.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi2.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate2.Text = Nothing
            rtxtJTaupMIdesc2.Text = Nothing
            txtJTaupMIamt2.Text = Nothing
            cboxJTaupMIqatd2.Checked = False
            cboxJTaupMIfi2.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate3.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate3.Text
            itmBuild(1) = rtxtJTaupMIdesc3.Text
            itmBuild(2) = txtJTaupMIamt3.Text
            If cboxJTaupMIqatd3.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi3.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate3.Text = Nothing
            rtxtJTaupMIdesc3.Text = Nothing
            txtJTaupMIamt3.Text = Nothing
            cboxJTaupMIqatd3.Checked = False
            cboxJTaupMIfi3.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate4.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate4.Text
            itmBuild(1) = rtxtJTaupMIdesc4.Text
            itmBuild(2) = txtJTaupMIamt4.Text
            If cboxJTaupMIqatd4.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi4.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate4.Text = Nothing
            rtxtJTaupMIdesc4.Text = Nothing
            txtJTaupMIamt4.Text = Nothing
            cboxJTaupMIqatd4.Checked = False
            cboxJTaupMIfi4.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate5.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate5.Text
            itmBuild(1) = rtxtJTaupMIdesc5.Text
            itmBuild(2) = txtJTaupMIamt5.Text
            If cboxJTaupMIqatd5.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi5.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate5.Text = Nothing
            rtxtJTaupMIdesc5.Text = Nothing
            txtJTaupMIamt5.Text = Nothing
            cboxJTaupMIqatd5.Checked = False
            cboxJTaupMIfi5.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate6.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate6.Text
            itmBuild(1) = rtxtJTaupMIdesc6.Text
            itmBuild(2) = txtJTaupMIamt6.Text
            If cboxJTaupMIqatd6.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi6.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate6.Text = Nothing
            rtxtJTaupMIdesc6.Text = Nothing
            txtJTaupMIamt6.Text = Nothing
            cboxJTaupMIqatd6.Checked = False
            cboxJTaupMIfi6.Checked = False
        End If
        '
        If TestForValidDate(mtxtJTaupMIdate7.Text) = True Then
            ' add item to listview
            Dim itmBuild(7) As String
            Dim lvLine As ListViewItem
            ' --------------------------------
            ' move items to listview
            ' --------------------------------
            itmBuild(0) = mtxtJTaupMIdate7.Text
            itmBuild(1) = rtxtJTaupMIdesc7.Text
            itmBuild(2) = txtJTaupMIamt7.Text
            If cboxJTaupMIqatd7.Checked = True Then
                itmBuild(3) = "X"
            Else
                itmBuild(3) = ""
            End If
            If cboxJTaupMIfi7.Checked = True Then
                itmBuild(4) = "X"
            Else
                itmBuild(4) = ""
            End If
            itmBuild(5) = ""
            itmBuild(6) = ""
            '
            lvLine = New ListViewItem(itmBuild)
            lvJTaupInvDetails.Items.Add(lvLine)
            ' Clear fields on panel.
            mtxtJTaupMIdate7.Text = Nothing
            rtxtJTaupMIdesc7.Text = Nothing
            txtJTaupMIamt7.Text = Nothing
            cboxJTaupMIqatd7.Checked = False
            cboxJTaupMIfi7.Checked = False
        End If
        '
        '     gboxJTaupMultiInv = Nothing
        gboxJTaupMultiInv.Visible = False
    End Sub


    Private Sub BtnJTaupMICancel_Click(sender As Object, e As EventArgs) Handles btnJTaupMICancel.Click
        gboxJTaupMultiInv.Visible = False
    End Sub

    Private Sub BtnJTaupMultiAdd_Click(sender As Object, e As EventArgs) Handles btnJTaupMultiAdd.Click
        gboxJTaupMultiInv.Visible = True
        btnJTaupMIAddUpdt.Enabled = True
        tmpEmptyDate = mtxtJTaupMIdate1.Text
        mtxtJTaupMIdate1.Select()
    End Sub
    '
    ''' <summary>
    ''' Function fills in fields of the structure passed. This data will be used on the AUP control Panel 
    ''' as well as the invoicing panel.
    ''' </summary>
    ''' <param name="tmpInfoRec">This is a structure to pass data back and forth between subs.</param>
    ''' <returns></returns>
    Public Function JTaupJobInfo(ByRef tmpInfoRec As JTaupInfoRec) As String
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String = Nothing
        For Each k In key
            If slJTaupJob(k).JTaupJJob = tmpInfoRec.JTaupCustomer Then
                If slJTaupJob(k).JTaupJSubJob = tmpInfoRec.JTaupJob Then
                    If slJTaupJob(k).JTaupJType = "I" Then
                        tmpInfoRec.JTaupJobInitial = Val(tmpInfoRec.JTaupJobInitial) + Val(slJTaupJob(k).JTaupJAmt)
                    Else
                        tmpInfoRec.JTaupJobChanges = Val(tmpInfoRec.JTaupJobChanges) + Val(slJTaupJob(k).JTaupJAmt)
                    End If
                End If
            End If
        Next
        '
        key = slJTaupInv.Keys
        For Each k In key
            If slJTaupInv(k).JTaupIJob = tmpInfoRec.JTaupCustomer Then
                If slJTaupInv(k).JTaupISubJob = tmpInfoRec.JTaupJob Then
                    If slJTaupInv(k).JTaupIInvNum <> "" Then
                        tmpInfoRec.JTaupInvdTD = Val(tmpInfoRec.JTaupInvdTD) + slJTaupInv(k).JTaupIAmt
                        If slJTaupInv(k).JTaupIFinal = "X" Then
                            tmpInfoRec.JTaupLstInvSent = True
                        End If
                        If tmpInfoRec.JTaupLstInvDate = "" Then
                            tmpInfoRec.JTaupLstInvDate = slJTaupInv(k).JTaupIInvDate
                        Else
                            Dim tmpLstInvDate As Date = CDate(tmpInfoRec.JTaupLstInvDate)
                            Dim tmpThisInvDate As Date = CDate(slJTaupInv(k).JTaupIInvDate)
                            If tmpThisInvDate > tmpLstInvDate Then
                                tmpInfoRec.JTaupLstInvDate = slJTaupInv(k).JTaupIInvDate
                            End If
                        End If
                    Else
                            Dim tmpInvDate As Date = CDate(slJTaupInv(k).JTaupIDate)
                        If tmpInvDate > Date.Today Then
                            tmpInfoRec.JTaupQueuedFuture = Val(tmpInfoRec.JTaupQueuedFuture) + slJTaupInv(k).JTaupIAmt
                        Else
                            tmpInfoRec.JTaupQueuedNow = Val(tmpInfoRec.JTaupQueuedNow) + slJTaupInv(k).JTaupIAmt
                        End If
                    End If
                End If
            End If
        Next
        Return ""
    End Function
    ''' <summary>
    ''' Function to make customer and/or job names changes. Used by JTjm.
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTaupCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String


        ' --------------------------------------------------
        ' Start of slJTaupJob section
        ' --------------------------------------------------

        '
        Dim tmpKeyCollection As New Collection()
        '
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTaupJob(k).JTaupJJob Then
                    tmpKeyCollection.Add(k)
                End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTaupJob(k).JTaupJJob Then
                    If JTjmSettings.ExJob = slJTaupJob(k).JTaupJSubJob Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
        End If
        ' Modify logic for slJTaupJob placed here .....  
        For Each k In tmpKeyCollection
            Dim tmpItem As JTaupJOBStru = slJTaupJob(k)
            slJTaupJob.Remove(k)
            '
            tmpItem.JTaupJJob = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.JTaupJSubJob = JTjmSettings.NewJob
            End If
            Dim tmpSeq As Integer = 10
            Dim tmpkey As String = tmpItem.JTaupJJob & tmpItem.JTaupJSubJob & tmpItem.JTaupJDate.Substring(6, 4) _
                               & tmpItem.JTaupJDate.Substring(0, 2) & tmpItem.JTaupJDate.Substring(3, 2) & tmpSeq
            Do While slJTaupJob.ContainsKey(tmpkey)
                tmpSeq += 1
                tmpkey = tmpItem.JTaupJJob & tmpItem.JTaupJSubJob & tmpItem.JTaupJDate.Substring(6, 4) _
                                & tmpItem.JTaupJDate.Substring(0, 2) & tmpItem.JTaupJDate.Substring(3, 2) & tmpSeq
            Loop
            '
            slJTaupJob.Add(tmpkey, tmpItem)
        Next
        ' --------------------------------------------------
        ' End of slJTaupJob section
        ' --------------------------------------------------
        '
        ' --------------------------------------------------
        ' Start of slJTaupInv section
        ' --------------------------------------------------

        '
        tmpKeyCollection.Clear()

        '
        key = slJTaupInv.Keys
        k = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTaupInv(k).JTaupIJob Then
                    tmpKeyCollection.Add(k)
                End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTaupInv(k).JTaupIJob Then
                    If JTjmSettings.ExJob = slJTaupInv(k).JTaupISubJob Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
        End If
        ' Modify logic for slJTaupInv placed here .....
        For Each k In tmpKeyCollection
            Dim tmpItem As JTaupInvStru = slJTaupInv(k)
            slJTaupInv.Remove(k)
            '
            tmpItem.JTaupIJob = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.JTaupISubJob = JTjmSettings.NewJob
            End If
            JTaupslJTaupInvAddRecord(tmpItem, "Called from JTaupCustJobNameChg")
        Next
        ' --------------------------------------------------
        ' End of slJTaupInv section
        ' --------------------------------------------------
        Return ""
    End Function
    ''' <summary>
    ''' Sub checks that AUP job has an initial "I" record that defines the job. If it does not, a message is 
    ''' displayed and the operator is redirected to put in an initial description.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub RtxtJTaupIDesc_TextChanged(sender As Object, e As EventArgs) Handles rtxtJTaupIDesc.TextChanged, rtxtJTaupMIdesc1.TextChanged
        Dim tmpSub As Integer = 0
        Do While tmpSub < lvJTaupJobDetails.Items.Count
            If lvJTaupJobDetails.Items(tmpSub).Text = "I" Then
                Exit Sub
            End If
            tmpSub += 1
        Loop
        MsgBox("AUP job must have an original Job Description prior to invoicing." & vbCrLf &
               "Create an 'I' entry and then proceed with invoicing.(AUP.009)")
        rtxtJTaupIDesc.Text = ""
        rtxtJTaupMIdesc1.Text = ""
        rbtnJTaupInit.Checked = True
        rtxtJTaupJDesc.Select()
    End Sub

    ''' <summary>
    ''' Function to check to see if an 'I' record exists for this job.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <returns></returns>
    Public Function JTaupIentryChk(tmpJobID As String, tmpSubID As String) As Boolean
        Dim tmpReturnValue As Boolean = False
        Dim key As ICollection = slJTaupJob.Keys
        Dim k As String = Nothing
        For Each k In key
            If tmpJobID = slJTaupJob(k).JTaupJjob Then
                If tmpSubID = slJTaupJob(k).JTaupJSubJob Then
                    If slJTaupJob(k).JTaupJType = "I" Then
                        tmpReturnValue = True
                        Exit For
                    End If
                End If
            End If
        Next
        '
        Return tmpReturnValue
    End Function
    '

    ''' <summary>
    ''' Function to check status of deposits made on CPWD jobs that are about
    ''' to be invoiced. If that job category job is found, the activity is gathered
    ''' and the operator is queried to see if a reversal of some or all of the
    ''' deposit amount should be made. After completion, the function will exit back
    ''' into the invoicing stream of code.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <returns></returns>
    Public Function JTaupDepositChk(tmpJobID As String, tmpSubID As String) As Boolean
        Dim tmpReturnFlag As Boolean = False
        Dim tmpJobList As New List(Of String)
        Dim tmpSub As Integer = 0
        Dim tmpJimjobID As String = tmpJobID
        Dim tmpJimSubID As String = Nothing
        Dim tmpAUPDepositBal As Decimal = 0
        Dim tmpUnbilled As Decimal = 0
        Dim tmpDepReversal As Decimal = 0
        If tmpSubID <> "MSCOMB" Then
            tmpJobList.Add(tmpSubID)
        Else

            Do While JTjim.JTjimReadJI(tmpsub, tmpJimjobID, tmpJimSubID) = "DATA"
                If tmpJimjobID = tmpJobID Then
                    tmpJobList.Add(tmpJimSubID)
                End If
                tmpsub += 1
            Loop
        End If
        Dim JItmpSub As Integer = 0
        tmpSub = 0
        Do While tmpSub < tmpJobList.Count
            Dim tmpJobCount As Integer = 0
            Dim tmpJTjimRec As JTjim.JTjimJobInfo = Nothing
            Do While JTjim.JTjimReadJtjim(JItmpSub, tmpJTjimRec) = "DATA"
                If tmpJTjimRec.JTjimJIJobName = tmpJimjobID Then
                    If tmpJTjimRec.JTjimJISubName = tmpJobList(tmpSub) Then
                        If tmpJTjimRec.JTjimJIJobPricMeth = "AUP" Then
                            If tmpJTjimRec.JTjimJIDeactDate = "" Then
                                If tmpJTjimRec.JTjimJIAUPLabor <> "X" Then
                                    If tmpJTjimRec.JTjimJIAUPMat <> "X" Then
                                        If tmpJTjimRec.JTjimJIAUPServices <> "X" Then
                                            ' Found a job - CPWD - AUP job with no categories checked.
                                            tmpAUPDepositBal = 0
                                            Dim key As ICollection = slJTaupInv.Keys
                                            Dim k As String = Nothing
                                            For Each k In key
                                                If slJTaupInv(k).JTaupIJob = tmpJobID Then
                                                    If slJTaupInv(k).JTaupISubJob = tmpJobList(tmpSub) Then
                                                        If slJTaupInv(k).JTaupIInvDate <> "" Then
                                                            '    MsgBox("Found billed AUP invoice line item --- JTaup~1746")
                                                            tmpAUPDepositBal += Val(slJTaupInv(k).JTaupIAmt)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                            If tmpAUPDepositBal > 0 Then
                                                Dim tmpNotInvLab As Decimal = 0
                                                Dim tmpNotInvMat As Decimal = 0
                                                Dim tmpNotInvSub As Decimal = 0
                                                '
                                                JTMainMenu.JTMMNotInvValues(tmpJobID, tmpSubID, tmpNotInvLab, tmpNotInvMat, tmpNotInvSub)
                                                tmpUnbilled = tmpNotInvLab + tmpNotInvMat + tmpNotInvSub
                                                If tmpUnbilled > 0 Then
                                                    If tmpAUPDepositBal > tmpUnbilled Then
                                                        tmpDepReversal = tmpUnbilled
                                                    Else
                                                        tmpDepReversal = tmpAUPDepositBal
                                                    End If
                                                    Dim tmpFormat As String = "{0:-33}{1:9}"
                                                    Dim MsgBoxText As String = tmpJobID & "~" & tmpSubID & "Is about to invoiced." & vbCrLf &
                                                    "- - - - This is a 'Cost+ with Deposits' Job - - - -" & vbCrLf & vbCrLf &
                                                    String.Format(tmpFormat, "Current Net Deposit balance --->" & vbTab, String.Format("{0:0.00}", tmpAUPDepositBal)) & vbCrLf &
                                                    String.Format(tmpFormat, "Current Unbilled Activity   --->" & vbTab, String.Format("{0:0.00}", tmpUnbilled)) & vbCrLf &
                                                    vbCrLf &
                                                    String.Format(tmpFormat, "Suggested Deposit Reversal  --->" & vbTab, String.Format("{0:0.00}", tmpDepReversal)) & vbCrLf &
                                                    vbCrLf &
                                                    "Enter 'Yes' to create the deposit reversal transaction." & vbCrLf &
                                                    "      'No' to proceed wihtout adjusting deposit balance."
                                                    Dim tmpAns As Integer = MsgBox(MsgBoxText, vbYesNo, "J.I.M. - CPWD - Deposit Adjustments")
                                                    If tmpAns = vbYes Then
                                                        ' Create a deposit reversal transaction.
                                                        Dim tmpslJTaupInvItem As JTaupInvStru = Nothing
                                                        tmpslJTaupInvItem.JTaupIJob = tmpJobID
                                                        tmpslJTaupInvItem.JTaupISubJob = tmpSubID
                                                        tmpslJTaupInvItem.JTaupIDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
                                                        tmpslJTaupInvItem.JTaupIDesc = "Deposit Reversal"
                                                        tmpslJTaupInvItem.JTaupIAmt = Val(tmpDepReversal) * -1
                                                        JTaupslJTaupInvAddRecord(tmpslJTaupInvItem, "Called from JTaupDepositChk")
                                                        '
                                                        '    MsgBox("Deposit Reversal Record built & saved - - - JTaup~1764" & vbCrLf &
                                                        '       tmpslJTaupInvItem.JTaupIJob & vbCrLf &
                                                        '       tmpslJTaupInvItem.JTaupISubJob & vbCrLf &
                                                        '       tmpslJTaupInvItem.JTaupIDate & vbCrLf &
                                                        '       tmpslJTaupInvItem.JTaupIDesc & vbCrLf &
                                                        '       tmpslJTaupInvItem.JTaupIAmt)
                                                        '
                                                        tmpReturnFlag = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                tmpJobCount += 1
            Loop
            tmpSub += 1
        Loop
        If tmpReturnFlag = True Then
            JTaupFlush()
        End If
        Return tmpReturnFlag
    End Function
    ''' <summary>
    ''' Function to clear any 'dangling' Deposit Reversal entries. These entries are generated
    ''' as part of the invoicing process on a CPWD job. If must be used on an invoice during
    ''' that session. If the operator leave the invoicing session. The system will clear any left
    ''' over items by calling this function.
    ''' </summary>
    ''' <returns>Returns # of items deleted.</returns>
    Public Function JTaupRemoveDanglingDepositReversals() As Integer
        Dim tmpDeleteCount As Integer = 0
        Dim tmpRecordNumber As Integer = 0

        Dim tmpkey As ICollection = slJTaupInv.Keys
        Dim key As ICollection = tmpkey
        tmpkey = Nothing
        '
        '     MsgBox("Running RemoveDangling ... JTaup~1805")
        Do While tmpRecordNumber < key.Count
            If slJTaupInv(key(tmpRecordNumber)).JTaupIInvNum = "" Then
                '            MsgBox("Found blank invoice #" & vbCrLf & slJTaupInv(key(tmpRecordNumber)).JTaupIAmt & vbCrLf &
                '                ">" & slJTaupInv(key(tmpRecordNumber)).JTaupIDesc & "<")
                If (slJTaupInv(key(tmpRecordNumber)).JTaupIDesc).trim = "Deposit Reversal" Then
                    Dim tmpAmt As Decimal = Val(slJTaupInv(key(tmpRecordNumber)).JTaupIAmt)
                    If tmpAmt < 0 Then
                        '                MsgBox("Found amount less than 0")

                        slJTaupInv.Remove(key(tmpRecordNumber))
                        '                MsgBox("Removing record - - - JTaup~1811")
                        tmpDeleteCount += 1
                    End If
                End If
            End If
            tmpRecordNumber += 1
        Loop
        If tmpDeleteCount > 0 Then
            '       MsgBox("Dangling Records Removed - - - " & tmpDeleteCount)
            JTaupFlush()
        End If
        Return tmpDeleteCount
    End Function
    '
    ''' <summary>
    ''' This function is a common function for adding an item to slJTaupInv.
    ''' </summary>
    ''' <param name="tmpslJTaupInvItem"></param>
    ''' <param name="tmpWhoCalledthisFunction"></param>
    ''' <returns></returns>
    Private Function JTaupslJTaupInvAddRecord(ByRef tmpslJTaupInvItem As JTaupInvStru, tmpWhoCalledthisFunction As String) As Boolean
        Dim tmpSeq As Integer = 10
        Dim tmpkey As String = tmpslJTaupInvItem.JTaupIJob & tmpslJTaupInvItem.JTaupISubJob & tmpslJTaupInvItem.JTaupIDate.Substring(6, 4) _
                       & tmpslJTaupInvItem.JTaupIDate.Substring(0, 2) & tmpslJTaupInvItem.JTaupIDate.Substring(3, 2) & tmpSeq
        Do While slJTaupInv.ContainsKey(tmpkey)
            tmpSeq += 1
            tmpkey = tmpslJTaupInvItem.JTaupIJob & tmpslJTaupInvItem.JTaupISubJob & tmpslJTaupInvItem.JTaupIDate.Substring(6, 4) _
                                & tmpslJTaupInvItem.JTaupIDate.Substring(0, 2) & tmpslJTaupInvItem.JTaupIDate.Substring(3, 2) & tmpSeq
        Loop
        '
        slJTaupInv.Add(tmpkey, tmpslJTaupInvItem)

        Return True
    End Function

End Class