﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTInvSeq
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnJTInvSeqExit = New System.Windows.Forms.Button()
        Me.txtJTInvSeqList = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnJTInvSeqExit
        '
        Me.btnJTInvSeqExit.Location = New System.Drawing.Point(135, 399)
        Me.btnJTInvSeqExit.Name = "btnJTInvSeqExit"
        Me.btnJTInvSeqExit.Size = New System.Drawing.Size(130, 37)
        Me.btnJTInvSeqExit.TabIndex = 0
        Me.btnJTInvSeqExit.Text = "Proceed"
        Me.btnJTInvSeqExit.UseVisualStyleBackColor = True
        '
        'txtJTInvSeqList
        '
        Me.txtJTInvSeqList.Location = New System.Drawing.Point(48, 12)
        Me.txtJTInvSeqList.Multiline = True
        Me.txtJTInvSeqList.Name = "txtJTInvSeqList"
        Me.txtJTInvSeqList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtJTInvSeqList.Size = New System.Drawing.Size(315, 368)
        Me.txtJTInvSeqList.TabIndex = 1
        '
        'JTInvSeq
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(417, 450)
        Me.Controls.Add(Me.txtJTInvSeqList)
        Me.Controls.Add(Me.btnJTInvSeqExit)
        Me.Name = "JTInvSeq"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Invoice File Sequence Check"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnJTInvSeqExit As Button
    Friend WithEvents txtJTInvSeqList As TextBox
End Class
