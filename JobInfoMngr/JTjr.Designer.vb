﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTjr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboxShowMrgn = New System.Windows.Forms.CheckBox()
        Me.lblSubjob = New System.Windows.Forms.Label()
        Me.cboxConsolidate = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnLife = New System.Windows.Forms.RadioButton()
        Me.rbtnTY = New System.Windows.Forms.RadioButton()
        Me.btnGenerate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.comboJTjrJob = New System.Windows.Forms.ComboBox()
        Me.comboJTjrSubjob = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Job Report"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label2.Location = New System.Drawing.Point(10, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Customer:"
        '
        'cboxShowMrgn
        '
        Me.cboxShowMrgn.AutoSize = True
        Me.cboxShowMrgn.Checked = True
        Me.cboxShowMrgn.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cboxShowMrgn.Location = New System.Drawing.Point(13, 21)
        Me.cboxShowMrgn.Name = "cboxShowMrgn"
        Me.cboxShowMrgn.Size = New System.Drawing.Size(177, 21)
        Me.cboxShowMrgn.TabIndex = 2
        Me.cboxShowMrgn.Text = "Show Estimated Margin"
        Me.cboxShowMrgn.UseVisualStyleBackColor = True
        '
        'lblSubjob
        '
        Me.lblSubjob.AutoSize = True
        Me.lblSubjob.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.lblSubjob.Location = New System.Drawing.Point(243, 23)
        Me.lblSubjob.Name = "lblSubjob"
        Me.lblSubjob.Size = New System.Drawing.Size(35, 17)
        Me.lblSubjob.TabIndex = 4
        Me.lblSubjob.Text = "Job:"
        Me.lblSubjob.Visible = False
        '
        'cboxConsolidate
        '
        Me.cboxConsolidate.AutoSize = True
        Me.cboxConsolidate.Location = New System.Drawing.Point(13, 48)
        Me.cboxConsolidate.Name = "cboxConsolidate"
        Me.cboxConsolidate.Size = New System.Drawing.Size(177, 21)
        Me.cboxConsolidate.TabIndex = 6
        Me.cboxConsolidate.Text = "Consolidate all Subjobs"
        Me.cboxConsolidate.UseVisualStyleBackColor = True
        Me.cboxConsolidate.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.rbtnLife)
        Me.GroupBox1.Controls.Add(Me.rbtnTY)
        Me.GroupBox1.Location = New System.Drawing.Point(270, 122)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(130, 84)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Activity Range"
        '
        'rbtnLife
        '
        Me.rbtnLife.AutoSize = True
        Me.rbtnLife.Checked = True
        Me.rbtnLife.Location = New System.Drawing.Point(11, 30)
        Me.rbtnLife.Name = "rbtnLife"
        Me.rbtnLife.Size = New System.Drawing.Size(95, 21)
        Me.rbtnLife.TabIndex = 1
        Me.rbtnLife.TabStop = True
        Me.rbtnLife.Text = "Life of Job"
        Me.rbtnLife.UseVisualStyleBackColor = True
        '
        'rbtnTY
        '
        Me.rbtnTY.AutoSize = True
        Me.rbtnTY.Location = New System.Drawing.Point(11, 57)
        Me.rbtnTY.Name = "rbtnTY"
        Me.rbtnTY.Size = New System.Drawing.Size(90, 21)
        Me.rbtnTY.TabIndex = 0
        Me.rbtnTY.Text = "This Year"
        Me.rbtnTY.UseVisualStyleBackColor = True
        '
        'btnGenerate
        '
        Me.btnGenerate.Location = New System.Drawing.Point(98, 222)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(138, 40)
        Me.btnGenerate.TabIndex = 8
        Me.btnGenerate.Text = "Generate Report"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(242, 222)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(138, 40)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'comboJTjrJob
        '
        Me.comboJTjrJob.FormattingEnabled = True
        Me.comboJTjrJob.Location = New System.Drawing.Point(94, 21)
        Me.comboJTjrJob.Name = "comboJTjrJob"
        Me.comboJTjrJob.Size = New System.Drawing.Size(135, 24)
        Me.comboJTjrJob.TabIndex = 10
        '
        'comboJTjrSubjob
        '
        Me.comboJTjrSubjob.FormattingEnabled = True
        Me.comboJTjrSubjob.Location = New System.Drawing.Point(301, 21)
        Me.comboJTjrSubjob.Name = "comboJTjrSubjob"
        Me.comboJTjrSubjob.Size = New System.Drawing.Size(137, 24)
        Me.comboJTjrSubjob.TabIndex = 11
        Me.comboJTjrSubjob.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.comboJTjrSubjob)
        Me.GroupBox2.Controls.Add(Me.comboJTjrJob)
        Me.GroupBox2.Controls.Add(Me.lblSubjob)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 53)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(454, 57)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.cboxConsolidate)
        Me.GroupBox3.Controls.Add(Me.cboxShowMrgn)
        Me.GroupBox3.Location = New System.Drawing.Point(51, 122)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 84)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        '
        'JTjr
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(478, 277)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnGenerate)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "JTjr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "J.I.M - Job Report"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cboxShowMrgn As CheckBox
    Friend WithEvents lblSubjob As Label
    Friend WithEvents cboxConsolidate As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbtnLife As RadioButton
    Friend WithEvents rbtnTY As RadioButton
    Friend WithEvents btnGenerate As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents comboJTjrJob As ComboBox
    Friend WithEvents comboJTjrSubjob As ComboBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
End Class
