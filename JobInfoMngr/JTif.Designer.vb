﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTif
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTif))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTifJob = New System.Windows.Forms.TextBox()
        Me.txtJTifSubJob = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTifLabTlsOnly = New System.Windows.Forms.RadioButton()
        Me.rbtnJTifLabTask = New System.Windows.Forms.RadioButton()
        Me.rbtnJTifLabDetails = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboxJTifMatShowCost = New System.Windows.Forms.CheckBox()
        Me.rbtnJTifMatTlsOnly = New System.Windows.Forms.RadioButton()
        Me.rbtnJTifMatDetails = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboxJTifSubShowCost = New System.Windows.Forms.CheckBox()
        Me.rbtnJTifSubTlsOnly = New System.Windows.Forms.RadioButton()
        Me.rbtnJTifSubDetails = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnJTifDone = New System.Windows.Forms.Button()
        Me.btnJTifTurnOff = New System.Windows.Forms.Button()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 4)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(374, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Non-Standard Invoice  Settings"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Customer"
        '
        'txtJTifJob
        '
        Me.txtJTifJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTifJob.Location = New System.Drawing.Point(113, 15)
        Me.txtJTifJob.Name = "txtJTifJob"
        Me.txtJTifJob.ReadOnly = True
        Me.txtJTifJob.Size = New System.Drawing.Size(142, 27)
        Me.txtJTifJob.TabIndex = 3
        '
        'txtJTifSubJob
        '
        Me.txtJTifSubJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTifSubJob.Location = New System.Drawing.Point(113, 55)
        Me.txtJTifSubJob.Name = "txtJTifSubJob"
        Me.txtJTifSubJob.ReadOnly = True
        Me.txtJTifSubJob.Size = New System.Drawing.Size(142, 27)
        Me.txtJTifSubJob.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoEllipsis = True
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Job"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.rbtnJTifLabTlsOnly)
        Me.GroupBox1.Controls.Add(Me.rbtnJTifLabTask)
        Me.GroupBox1.Controls.Add(Me.rbtnJTifLabDetails)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(54, 154)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(413, 56)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Labor"
        '
        'rbtnJTifLabTlsOnly
        '
        Me.rbtnJTifLabTlsOnly.AutoSize = True
        Me.rbtnJTifLabTlsOnly.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifLabTlsOnly.Location = New System.Drawing.Point(265, 25)
        Me.rbtnJTifLabTlsOnly.Name = "rbtnJTifLabTlsOnly"
        Me.rbtnJTifLabTlsOnly.Size = New System.Drawing.Size(115, 24)
        Me.rbtnJTifLabTlsOnly.TabIndex = 2
        Me.rbtnJTifLabTlsOnly.TabStop = True
        Me.rbtnJTifLabTlsOnly.Text = "Totals Only"
        Me.rbtnJTifLabTlsOnly.UseVisualStyleBackColor = True
        '
        'rbtnJTifLabTask
        '
        Me.rbtnJTifLabTask.AutoSize = True
        Me.rbtnJTifLabTask.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifLabTask.Location = New System.Drawing.Point(123, 25)
        Me.rbtnJTifLabTask.Name = "rbtnJTifLabTask"
        Me.rbtnJTifLabTask.Size = New System.Drawing.Size(91, 24)
        Me.rbtnJTifLabTask.TabIndex = 1
        Me.rbtnJTifLabTask.TabStop = True
        Me.rbtnJTifLabTask.Text = "By Task"
        Me.rbtnJTifLabTask.UseVisualStyleBackColor = True
        '
        'rbtnJTifLabDetails
        '
        Me.rbtnJTifLabDetails.AutoSize = True
        Me.rbtnJTifLabDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifLabDetails.Location = New System.Drawing.Point(13, 25)
        Me.rbtnJTifLabDetails.Name = "rbtnJTifLabDetails"
        Me.rbtnJTifLabDetails.Size = New System.Drawing.Size(83, 24)
        Me.rbtnJTifLabDetails.TabIndex = 0
        Me.rbtnJTifLabDetails.TabStop = True
        Me.rbtnJTifLabDetails.Text = "Details"
        Me.rbtnJTifLabDetails.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.cboxJTifMatShowCost)
        Me.GroupBox2.Controls.Add(Me.rbtnJTifMatTlsOnly)
        Me.GroupBox2.Controls.Add(Me.rbtnJTifMatDetails)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(54, 293)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(413, 58)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Materials"
        '
        'cboxJTifMatShowCost
        '
        Me.cboxJTifMatShowCost.AutoSize = True
        Me.cboxJTifMatShowCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTifMatShowCost.Location = New System.Drawing.Point(269, 26)
        Me.cboxJTifMatShowCost.Name = "cboxJTifMatShowCost"
        Me.cboxJTifMatShowCost.Size = New System.Drawing.Size(112, 24)
        Me.cboxJTifMatShowCost.TabIndex = 5
        Me.cboxJTifMatShowCost.Text = "Show Cost"
        Me.cboxJTifMatShowCost.UseVisualStyleBackColor = True
        '
        'rbtnJTifMatTlsOnly
        '
        Me.rbtnJTifMatTlsOnly.AutoSize = True
        Me.rbtnJTifMatTlsOnly.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifMatTlsOnly.Location = New System.Drawing.Point(125, 26)
        Me.rbtnJTifMatTlsOnly.Name = "rbtnJTifMatTlsOnly"
        Me.rbtnJTifMatTlsOnly.Size = New System.Drawing.Size(115, 24)
        Me.rbtnJTifMatTlsOnly.TabIndex = 4
        Me.rbtnJTifMatTlsOnly.TabStop = True
        Me.rbtnJTifMatTlsOnly.Text = "Totals Only"
        Me.rbtnJTifMatTlsOnly.UseVisualStyleBackColor = True
        '
        'rbtnJTifMatDetails
        '
        Me.rbtnJTifMatDetails.AutoSize = True
        Me.rbtnJTifMatDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifMatDetails.Location = New System.Drawing.Point(14, 26)
        Me.rbtnJTifMatDetails.Name = "rbtnJTifMatDetails"
        Me.rbtnJTifMatDetails.Size = New System.Drawing.Size(83, 24)
        Me.rbtnJTifMatDetails.TabIndex = 5
        Me.rbtnJTifMatDetails.TabStop = True
        Me.rbtnJTifMatDetails.Text = "Details"
        Me.rbtnJTifMatDetails.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.cboxJTifSubShowCost)
        Me.GroupBox3.Controls.Add(Me.rbtnJTifSubTlsOnly)
        Me.GroupBox3.Controls.Add(Me.rbtnJTifSubDetails)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(54, 220)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(413, 62)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Subcontractors"
        '
        'cboxJTifSubShowCost
        '
        Me.cboxJTifSubShowCost.AutoSize = True
        Me.cboxJTifSubShowCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTifSubShowCost.Location = New System.Drawing.Point(268, 26)
        Me.cboxJTifSubShowCost.Name = "cboxJTifSubShowCost"
        Me.cboxJTifSubShowCost.Size = New System.Drawing.Size(112, 24)
        Me.cboxJTifSubShowCost.TabIndex = 4
        Me.cboxJTifSubShowCost.Text = "Show Cost"
        Me.cboxJTifSubShowCost.UseVisualStyleBackColor = True
        '
        'rbtnJTifSubTlsOnly
        '
        Me.rbtnJTifSubTlsOnly.AutoSize = True
        Me.rbtnJTifSubTlsOnly.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifSubTlsOnly.Location = New System.Drawing.Point(125, 26)
        Me.rbtnJTifSubTlsOnly.Name = "rbtnJTifSubTlsOnly"
        Me.rbtnJTifSubTlsOnly.Size = New System.Drawing.Size(115, 24)
        Me.rbtnJTifSubTlsOnly.TabIndex = 3
        Me.rbtnJTifSubTlsOnly.TabStop = True
        Me.rbtnJTifSubTlsOnly.Text = "Totals Only"
        Me.rbtnJTifSubTlsOnly.UseVisualStyleBackColor = True
        '
        'rbtnJTifSubDetails
        '
        Me.rbtnJTifSubDetails.AutoSize = True
        Me.rbtnJTifSubDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJTifSubDetails.Location = New System.Drawing.Point(14, 26)
        Me.rbtnJTifSubDetails.Name = "rbtnJTifSubDetails"
        Me.rbtnJTifSubDetails.Size = New System.Drawing.Size(83, 24)
        Me.rbtnJTifSubDetails.TabIndex = 3
        Me.rbtnJTifSubDetails.TabStop = True
        Me.rbtnJTifSubDetails.Text = "Details"
        Me.rbtnJTifSubDetails.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.txtJTifJob)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.txtJTifSubJob)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(54, 44)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(276, 100)
        Me.GroupBox4.TabIndex = 8
        Me.GroupBox4.TabStop = False
        '
        'btnJTifDone
        '
        Me.btnJTifDone.Location = New System.Drawing.Point(54, 358)
        Me.btnJTifDone.Name = "btnJTifDone"
        Me.btnJTifDone.Size = New System.Drawing.Size(112, 38)
        Me.btnJTifDone.TabIndex = 9
        Me.btnJTifDone.Text = "DONE"
        Me.btnJTifDone.UseVisualStyleBackColor = True
        '
        'btnJTifTurnOff
        '
        Me.btnJTifTurnOff.Location = New System.Drawing.Point(177, 358)
        Me.btnJTifTurnOff.Name = "btnJTifTurnOff"
        Me.btnJTifTurnOff.Size = New System.Drawing.Size(289, 38)
        Me.btnJTifTurnOff.TabIndex = 10
        Me.btnJTifTurnOff.Text = "Turn Off Non-Std Frmt"
        Me.btnJTifTurnOff.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(41, 404)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(487, 178)
        Me.TextBox3.TabIndex = 11
        Me.TextBox3.Text = resources.GetString("TextBox3.Text")
        '
        'JTif
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(579, 592)
        Me.ControlBox = False
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.btnJTifTurnOff)
        Me.Controls.Add(Me.btnJTifDone)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.Name = "JTif"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Non-Standard Invoice Format Settings"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTifJob As TextBox
    Friend WithEvents txtJTifSubJob As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbtnJTifLabTlsOnly As RadioButton
    Friend WithEvents rbtnJTifLabTask As RadioButton
    Friend WithEvents rbtnJTifLabDetails As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents rbtnJTifMatTlsOnly As RadioButton
    Friend WithEvents rbtnJTifMatDetails As RadioButton
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cboxJTifSubShowCost As CheckBox
    Friend WithEvents rbtnJTifSubTlsOnly As RadioButton
    Friend WithEvents rbtnJTifSubDetails As RadioButton
    Friend WithEvents cboxJTifMatShowCost As CheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btnJTifDone As Button
    Friend WithEvents btnJTifTurnOff As Button
    Friend WithEvents TextBox3 As TextBox
End Class
