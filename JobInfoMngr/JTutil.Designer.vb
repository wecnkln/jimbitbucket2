﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTutil
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTutil))
        Me.tabJTutil = New System.Windows.Forms.TabControl()
        Me.tabJTutilSelections = New System.Windows.Forms.TabPage()
        Me.rbtnEmail = New System.Windows.Forms.RadioButton()
        Me.rbtnInvVoid = New System.Windows.Forms.RadioButton()
        Me.rbtnJobDeact = New System.Windows.Forms.RadioButton()
        Me.rbtnPrtDoc = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tabJTutilPFA = New System.Windows.Forms.TabPage()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioBtnJTutilQB = New System.Windows.Forms.RadioButton()
        Me.RadioBtnJTutilRec = New System.Windows.Forms.RadioButton()
        Me.RadioBtnJTutilInv = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lvJTutilPDFNames = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabJTutilIV = New System.Windows.Forms.TabPage()
        Me.btnJTutilLookUp = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnJTutilVoid = New System.Windows.Forms.Button()
        Me.txtJTutilInvDate = New System.Windows.Forms.MaskedTextBox()
        Me.txtJTutilSubJob = New System.Windows.Forms.TextBox()
        Me.txtJTutilInvNum = New System.Windows.Forms.TextBox()
        Me.txtJTutilJobID = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tabJTutilJDeact = New System.Windows.Forms.TabPage()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.btnJTutilDCStartProcess = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lvJTutilDeactCandidates = New System.Windows.Forms.ListView()
        Me.DCCustName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DCJobName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DCCstMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DCCreateDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DCLstInvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabJTutilIL = New System.Windows.Forms.TabPage()
        Me.txtJTutilILInvPDF = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.btnJTutilILInvHist = New System.Windows.Forms.Button()
        Me.btnJTutilILDisplay = New System.Windows.Forms.Button()
        Me.txtJTutilILInvAmt = New System.Windows.Forms.MaskedTextBox()
        Me.txtJTutilILInvDate = New System.Windows.Forms.MaskedTextBox()
        Me.txtJTutilILSubjob = New System.Windows.Forms.TextBox()
        Me.txtJTutilILInvNum = New System.Windows.Forms.TextBox()
        Me.txtJTutilILJob = New System.Windows.Forms.TextBox()
        Me.btnJTutilILLookUp = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnJtutilDone = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tabJTutil.SuspendLayout()
        Me.tabJTutilSelections.SuspendLayout()
        Me.tabJTutilPFA.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tabJTutilIV.SuspendLayout()
        Me.tabJTutilJDeact.SuspendLayout()
        Me.tabJTutilIL.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabJTutil
        '
        Me.tabJTutil.Controls.Add(Me.tabJTutilSelections)
        Me.tabJTutil.Controls.Add(Me.tabJTutilPFA)
        Me.tabJTutil.Controls.Add(Me.tabJTutilIV)
        Me.tabJTutil.Controls.Add(Me.tabJTutilJDeact)
        Me.tabJTutil.Controls.Add(Me.tabJTutilIL)
        Me.tabJTutil.Location = New System.Drawing.Point(12, 67)
        Me.tabJTutil.Name = "tabJTutil"
        Me.tabJTutil.SelectedIndex = 0
        Me.tabJTutil.Size = New System.Drawing.Size(756, 429)
        Me.tabJTutil.TabIndex = 0
        '
        'tabJTutilSelections
        '
        Me.tabJTutilSelections.Controls.Add(Me.rbtnEmail)
        Me.tabJTutilSelections.Controls.Add(Me.rbtnInvVoid)
        Me.tabJTutilSelections.Controls.Add(Me.rbtnJobDeact)
        Me.tabJTutilSelections.Controls.Add(Me.rbtnPrtDoc)
        Me.tabJTutilSelections.Controls.Add(Me.Label8)
        Me.tabJTutilSelections.Location = New System.Drawing.Point(4, 25)
        Me.tabJTutilSelections.Name = "tabJTutilSelections"
        Me.tabJTutilSelections.Padding = New System.Windows.Forms.Padding(3)
        Me.tabJTutilSelections.Size = New System.Drawing.Size(748, 400)
        Me.tabJTutilSelections.TabIndex = 3
        Me.tabJTutilSelections.Text = "Selections"
        Me.tabJTutilSelections.UseVisualStyleBackColor = True
        '
        'rbtnEmail
        '
        Me.rbtnEmail.AutoSize = True
        Me.rbtnEmail.Location = New System.Drawing.Point(100, 220)
        Me.rbtnEmail.Name = "rbtnEmail"
        Me.rbtnEmail.Size = New System.Drawing.Size(62, 20)
        Me.rbtnEmail.TabIndex = 11
        Me.rbtnEmail.Text = "Email"
        Me.rbtnEmail.UseVisualStyleBackColor = True
        '
        'rbtnInvVoid
        '
        Me.rbtnInvVoid.AutoSize = True
        Me.rbtnInvVoid.Location = New System.Drawing.Point(100, 133)
        Me.rbtnInvVoid.Name = "rbtnInvVoid"
        Me.rbtnInvVoid.Size = New System.Drawing.Size(102, 20)
        Me.rbtnInvVoid.TabIndex = 9
        Me.rbtnInvVoid.Text = "Invoice Void"
        Me.rbtnInvVoid.UseVisualStyleBackColor = True
        '
        'rbtnJobDeact
        '
        Me.rbtnJobDeact.AutoSize = True
        Me.rbtnJobDeact.Location = New System.Drawing.Point(100, 175)
        Me.rbtnJobDeact.Name = "rbtnJobDeact"
        Me.rbtnJobDeact.Size = New System.Drawing.Size(129, 20)
        Me.rbtnJobDeact.TabIndex = 8
        Me.rbtnJobDeact.Text = "Job Deactivation"
        Me.rbtnJobDeact.UseVisualStyleBackColor = True
        '
        'rbtnPrtDoc
        '
        Me.rbtnPrtDoc.AutoSize = True
        Me.rbtnPrtDoc.Location = New System.Drawing.Point(100, 92)
        Me.rbtnPrtDoc.Name = "rbtnPrtDoc"
        Me.rbtnPrtDoc.Size = New System.Drawing.Size(182, 20)
        Me.rbtnPrtDoc.TabIndex = 7
        Me.rbtnPrtDoc.Text = "Printed Document Archive"
        Me.rbtnPrtDoc.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(27, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(134, 18)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Utility Selections"
        '
        'tabJTutilPFA
        '
        Me.tabJTutilPFA.Controls.Add(Me.RichTextBox1)
        Me.tabJTutilPFA.Controls.Add(Me.GroupBox1)
        Me.tabJTutilPFA.Controls.Add(Me.Label2)
        Me.tabJTutilPFA.Controls.Add(Me.lvJTutilPDFNames)
        Me.tabJTutilPFA.Location = New System.Drawing.Point(4, 25)
        Me.tabJTutilPFA.Name = "tabJTutilPFA"
        Me.tabJTutilPFA.Padding = New System.Windows.Forms.Padding(3)
        Me.tabJTutilPFA.Size = New System.Drawing.Size(748, 400)
        Me.tabJTutilPFA.TabIndex = 0
        Me.tabJTutilPFA.Text = "Print File Archive"
        Me.tabJTutilPFA.UseVisualStyleBackColor = True
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(53, 168)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(260, 186)
        Me.RichTextBox1.TabIndex = 6
        Me.RichTextBox1.Text = "Activate a Button to select the type of report desired." & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "Double Click on the repo" &
    "rt name in the table." & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "The report will be displayed on the panel. It can be revi" &
    "ewed online or printed."
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.PowderBlue
        Me.GroupBox1.Controls.Add(Me.RadioBtnJTutilQB)
        Me.GroupBox1.Controls.Add(Me.RadioBtnJTutilRec)
        Me.GroupBox1.Controls.Add(Me.RadioBtnJTutilInv)
        Me.GroupBox1.Location = New System.Drawing.Point(76, 48)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(208, 106)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'RadioBtnJTutilQB
        '
        Me.RadioBtnJTutilQB.AutoSize = True
        Me.RadioBtnJTutilQB.Location = New System.Drawing.Point(13, 64)
        Me.RadioBtnJTutilQB.Name = "RadioBtnJTutilQB"
        Me.RadioBtnJTutilQB.Size = New System.Drawing.Size(122, 20)
        Me.RadioBtnJTutilQB.TabIndex = 3
        Me.RadioBtnJTutilQB.TabStop = True
        Me.RadioBtnJTutilQB.Text = "QB Input Report"
        Me.RadioBtnJTutilQB.UseVisualStyleBackColor = True
        '
        'RadioBtnJTutilRec
        '
        Me.RadioBtnJTutilRec.AutoSize = True
        Me.RadioBtnJTutilRec.Location = New System.Drawing.Point(13, 37)
        Me.RadioBtnJTutilRec.Name = "RadioBtnJTutilRec"
        Me.RadioBtnJTutilRec.Size = New System.Drawing.Size(164, 20)
        Me.RadioBtnJTutilRec.TabIndex = 2
        Me.RadioBtnJTutilRec.TabStop = True
        Me.RadioBtnJTutilRec.Text = "Reconciliation Reports"
        Me.RadioBtnJTutilRec.UseVisualStyleBackColor = True
        '
        'RadioBtnJTutilInv
        '
        Me.RadioBtnJTutilInv.AutoSize = True
        Me.RadioBtnJTutilInv.Location = New System.Drawing.Point(13, 10)
        Me.RadioBtnJTutilInv.Name = "RadioBtnJTutilInv"
        Me.RadioBtnJTutilInv.Size = New System.Drawing.Size(78, 20)
        Me.RadioBtnJTutilInv.TabIndex = 1
        Me.RadioBtnJTutilInv.TabStop = True
        Me.RadioBtnJTutilInv.Text = "Invoices"
        Me.RadioBtnJTutilInv.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(238, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Report Archive Display Reprint"
        '
        'lvJTutilPDFNames
        '
        Me.lvJTutilPDFNames.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lvJTutilPDFNames.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.lvJTutilPDFNames.FullRowSelect = True
        Me.lvJTutilPDFNames.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvJTutilPDFNames.HideSelection = False
        Me.lvJTutilPDFNames.Location = New System.Drawing.Point(346, 48)
        Me.lvJTutilPDFNames.MultiSelect = False
        Me.lvJTutilPDFNames.Name = "lvJTutilPDFNames"
        Me.lvJTutilPDFNames.Size = New System.Drawing.Size(312, 306)
        Me.lvJTutilPDFNames.TabIndex = 0
        Me.lvJTutilPDFNames.UseCompatibleStateImageBehavior = False
        Me.lvJTutilPDFNames.View = System.Windows.Forms.View.List
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "File Names"
        Me.ColumnHeader1.Width = 250
        '
        'tabJTutilIV
        '
        Me.tabJTutilIV.Controls.Add(Me.btnJTutilLookUp)
        Me.tabJTutilIV.Controls.Add(Me.Label11)
        Me.tabJTutilIV.Controls.Add(Me.btnJTutilVoid)
        Me.tabJTutilIV.Controls.Add(Me.txtJTutilInvDate)
        Me.tabJTutilIV.Controls.Add(Me.txtJTutilSubJob)
        Me.tabJTutilIV.Controls.Add(Me.txtJTutilInvNum)
        Me.tabJTutilIV.Controls.Add(Me.txtJTutilJobID)
        Me.tabJTutilIV.Controls.Add(Me.Label7)
        Me.tabJTutilIV.Controls.Add(Me.Label6)
        Me.tabJTutilIV.Controls.Add(Me.Label5)
        Me.tabJTutilIV.Controls.Add(Me.Label4)
        Me.tabJTutilIV.Controls.Add(Me.RichTextBox2)
        Me.tabJTutilIV.Controls.Add(Me.Label3)
        Me.tabJTutilIV.Location = New System.Drawing.Point(4, 25)
        Me.tabJTutilIV.Name = "tabJTutilIV"
        Me.tabJTutilIV.Padding = New System.Windows.Forms.Padding(3)
        Me.tabJTutilIV.Size = New System.Drawing.Size(748, 400)
        Me.tabJTutilIV.TabIndex = 1
        Me.tabJTutilIV.Text = "Invoice Void"
        Me.tabJTutilIV.UseVisualStyleBackColor = True
        '
        'btnJTutilLookUp
        '
        Me.btnJTutilLookUp.Location = New System.Drawing.Point(285, 90)
        Me.btnJTutilLookUp.Name = "btnJTutilLookUp"
        Me.btnJTutilLookUp.Size = New System.Drawing.Size(96, 37)
        Me.btnJTutilLookUp.TabIndex = 14
        Me.btnJTutilLookUp.Text = "Look Up"
        Me.btnJTutilLookUp.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(35, 122)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(215, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "----------------------------------------------------"
        '
        'btnJTutilVoid
        '
        Me.btnJTutilVoid.Location = New System.Drawing.Point(146, 257)
        Me.btnJTutilVoid.Name = "btnJTutilVoid"
        Me.btnJTutilVoid.Size = New System.Drawing.Size(131, 39)
        Me.btnJTutilVoid.TabIndex = 12
        Me.btnJTutilVoid.Text = "Void Invoice"
        Me.btnJTutilVoid.UseVisualStyleBackColor = True
        '
        'txtJTutilInvDate
        '
        Me.txtJTutilInvDate.Location = New System.Drawing.Point(165, 216)
        Me.txtJTutilInvDate.Name = "txtJTutilInvDate"
        Me.txtJTutilInvDate.ReadOnly = True
        Me.txtJTutilInvDate.Size = New System.Drawing.Size(100, 22)
        Me.txtJTutilInvDate.TabIndex = 2
        '
        'txtJTutilSubJob
        '
        Me.txtJTutilSubJob.Location = New System.Drawing.Point(165, 182)
        Me.txtJTutilSubJob.Name = "txtJTutilSubJob"
        Me.txtJTutilSubJob.ReadOnly = True
        Me.txtJTutilSubJob.Size = New System.Drawing.Size(113, 22)
        Me.txtJTutilSubJob.TabIndex = 1
        '
        'txtJTutilInvNum
        '
        Me.txtJTutilInvNum.Location = New System.Drawing.Point(165, 96)
        Me.txtJTutilInvNum.Name = "txtJTutilInvNum"
        Me.txtJTutilInvNum.Size = New System.Drawing.Size(91, 22)
        Me.txtJTutilInvNum.TabIndex = 3
        '
        'txtJTutilJobID
        '
        Me.txtJTutilJobID.Location = New System.Drawing.Point(165, 148)
        Me.txtJTutilJobID.Name = "txtJTutilJobID"
        Me.txtJTutilJobID.ReadOnly = True
        Me.txtJTutilJobID.Size = New System.Drawing.Size(113, 22)
        Me.txtJTutilJobID.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label7.Location = New System.Drawing.Point(40, 181)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 17)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Job"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(40, 216)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 18)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Invoice Date"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(40, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 18)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Invoice Number"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(40, 143)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 18)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Customer"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.Location = New System.Drawing.Point(418, 58)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.Size = New System.Drawing.Size(243, 320)
        Me.RichTextBox2.TabIndex = 7
        Me.RichTextBox2.Text = resources.GetString("RichTextBox2.Text")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(18, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Invoice Void"
        '
        'tabJTutilJDeact
        '
        Me.tabJTutilJDeact.Controls.Add(Me.TextBox2)
        Me.tabJTutilJDeact.Controls.Add(Me.btnJTutilDCStartProcess)
        Me.tabJTutilJDeact.Controls.Add(Me.Label9)
        Me.tabJTutilJDeact.Controls.Add(Me.lvJTutilDeactCandidates)
        Me.tabJTutilJDeact.Location = New System.Drawing.Point(4, 25)
        Me.tabJTutilJDeact.Name = "tabJTutilJDeact"
        Me.tabJTutilJDeact.Size = New System.Drawing.Size(748, 400)
        Me.tabJTutilJDeact.TabIndex = 2
        Me.tabJTutilJDeact.Text = "Job Deactivation"
        Me.tabJTutilJDeact.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(499, 14)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(239, 378)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        '
        'btnJTutilDCStartProcess
        '
        Me.btnJTutilDCStartProcess.Location = New System.Drawing.Point(286, 356)
        Me.btnJTutilDCStartProcess.Name = "btnJTutilDCStartProcess"
        Me.btnJTutilDCStartProcess.Size = New System.Drawing.Size(201, 36)
        Me.btnJTutilDCStartProcess.TabIndex = 2
        Me.btnJTutilDCStartProcess.Text = "Deactivated Selected Jobs"
        Me.btnJTutilDCStartProcess.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(15, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(164, 18)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "JOB DEACTIVATION"
        '
        'lvJTutilDeactCandidates
        '
        Me.lvJTutilDeactCandidates.CheckBoxes = True
        Me.lvJTutilDeactCandidates.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.DCCustName, Me.DCJobName, Me.DCCstMeth, Me.DCCreateDate, Me.DCLstInvDate})
        Me.lvJTutilDeactCandidates.FullRowSelect = True
        Me.lvJTutilDeactCandidates.HideSelection = False
        Me.lvJTutilDeactCandidates.Location = New System.Drawing.Point(8, 50)
        Me.lvJTutilDeactCandidates.Name = "lvJTutilDeactCandidates"
        Me.lvJTutilDeactCandidates.RightToLeftLayout = True
        Me.lvJTutilDeactCandidates.Size = New System.Drawing.Size(479, 300)
        Me.lvJTutilDeactCandidates.TabIndex = 0
        Me.lvJTutilDeactCandidates.UseCompatibleStateImageBehavior = False
        Me.lvJTutilDeactCandidates.View = System.Windows.Forms.View.Details
        '
        'DCCustName
        '
        Me.DCCustName.Text = "Customer"
        Me.DCCustName.Width = 100
        '
        'DCJobName
        '
        Me.DCJobName.Text = "Job"
        Me.DCJobName.Width = 100
        '
        'DCCstMeth
        '
        Me.DCCstMeth.Text = "CMeth"
        Me.DCCstMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.DCCstMeth.Width = 50
        '
        'DCCreateDate
        '
        Me.DCCreateDate.Text = "Created"
        Me.DCCreateDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.DCCreateDate.Width = 85
        '
        'DCLstInvDate
        '
        Me.DCLstInvDate.Text = "Last Invoice"
        Me.DCLstInvDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.DCLstInvDate.Width = 85
        '
        'tabJTutilIL
        '
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILInvPDF)
        Me.tabJTutilIL.Controls.Add(Me.Label18)
        Me.tabJTutilIL.Controls.Add(Me.btnJTutilILInvHist)
        Me.tabJTutilIL.Controls.Add(Me.btnJTutilILDisplay)
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILInvAmt)
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILInvDate)
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILSubjob)
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILInvNum)
        Me.tabJTutilIL.Controls.Add(Me.txtJTutilILJob)
        Me.tabJTutilIL.Controls.Add(Me.btnJTutilILLookUp)
        Me.tabJTutilIL.Controls.Add(Me.Label17)
        Me.tabJTutilIL.Controls.Add(Me.Label12)
        Me.tabJTutilIL.Controls.Add(Me.Label13)
        Me.tabJTutilIL.Controls.Add(Me.Label14)
        Me.tabJTutilIL.Controls.Add(Me.Label15)
        Me.tabJTutilIL.Controls.Add(Me.Label16)
        Me.tabJTutilIL.Controls.Add(Me.TextBox1)
        Me.tabJTutilIL.Location = New System.Drawing.Point(4, 25)
        Me.tabJTutilIL.Name = "tabJTutilIL"
        Me.tabJTutilIL.Padding = New System.Windows.Forms.Padding(3)
        Me.tabJTutilIL.Size = New System.Drawing.Size(748, 400)
        Me.tabJTutilIL.TabIndex = 5
        Me.tabJTutilIL.Text = "Invoice Lookup"
        Me.tabJTutilIL.UseVisualStyleBackColor = True
        '
        'txtJTutilILInvPDF
        '
        Me.txtJTutilILInvPDF.Location = New System.Drawing.Point(445, 345)
        Me.txtJTutilILInvPDF.Name = "txtJTutilILInvPDF"
        Me.txtJTutilILInvPDF.ReadOnly = True
        Me.txtJTutilILInvPDF.Size = New System.Drawing.Size(194, 22)
        Me.txtJTutilILInvPDF.TabIndex = 29
        Me.txtJTutilILInvPDF.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(31, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(189, 29)
        Me.Label18.TabIndex = 28
        Me.Label18.Text = "Invoice Lookup"
        '
        'btnJTutilILInvHist
        '
        Me.btnJTutilILInvHist.Location = New System.Drawing.Point(207, 311)
        Me.btnJTutilILInvHist.Name = "btnJTutilILInvHist"
        Me.btnJTutilILInvHist.Size = New System.Drawing.Size(190, 32)
        Me.btnJTutilILInvHist.TabIndex = 27
        Me.btnJTutilILInvHist.Text = "Go to Job Invoice History"
        Me.btnJTutilILInvHist.UseVisualStyleBackColor = True
        '
        'btnJTutilILDisplay
        '
        Me.btnJTutilILDisplay.Location = New System.Drawing.Point(36, 311)
        Me.btnJTutilILDisplay.Name = "btnJTutilILDisplay"
        Me.btnJTutilILDisplay.Size = New System.Drawing.Size(149, 32)
        Me.btnJTutilILDisplay.TabIndex = 26
        Me.btnJTutilILDisplay.Text = "Display/Print Invoice"
        Me.btnJTutilILDisplay.UseVisualStyleBackColor = True
        '
        'txtJTutilILInvAmt
        '
        Me.txtJTutilILInvAmt.Location = New System.Drawing.Point(184, 253)
        Me.txtJTutilILInvAmt.Name = "txtJTutilILInvAmt"
        Me.txtJTutilILInvAmt.ReadOnly = True
        Me.txtJTutilILInvAmt.Size = New System.Drawing.Size(100, 22)
        Me.txtJTutilILInvAmt.TabIndex = 25
        '
        'txtJTutilILInvDate
        '
        Me.txtJTutilILInvDate.Location = New System.Drawing.Point(184, 220)
        Me.txtJTutilILInvDate.Name = "txtJTutilILInvDate"
        Me.txtJTutilILInvDate.ReadOnly = True
        Me.txtJTutilILInvDate.Size = New System.Drawing.Size(100, 22)
        Me.txtJTutilILInvDate.TabIndex = 23
        '
        'txtJTutilILSubjob
        '
        Me.txtJTutilILSubjob.Location = New System.Drawing.Point(184, 186)
        Me.txtJTutilILSubjob.Name = "txtJTutilILSubjob"
        Me.txtJTutilILSubjob.ReadOnly = True
        Me.txtJTutilILSubjob.Size = New System.Drawing.Size(113, 22)
        Me.txtJTutilILSubjob.TabIndex = 22
        '
        'txtJTutilILInvNum
        '
        Me.txtJTutilILInvNum.Location = New System.Drawing.Point(184, 100)
        Me.txtJTutilILInvNum.Name = "txtJTutilILInvNum"
        Me.txtJTutilILInvNum.Size = New System.Drawing.Size(91, 22)
        Me.txtJTutilILInvNum.TabIndex = 24
        '
        'txtJTutilILJob
        '
        Me.txtJTutilILJob.Location = New System.Drawing.Point(184, 152)
        Me.txtJTutilILJob.Name = "txtJTutilILJob"
        Me.txtJTutilILJob.ReadOnly = True
        Me.txtJTutilILJob.Size = New System.Drawing.Size(113, 22)
        Me.txtJTutilILJob.TabIndex = 21
        '
        'btnJTutilILLookUp
        '
        Me.btnJTutilILLookUp.Location = New System.Drawing.Point(315, 85)
        Me.btnJTutilILLookUp.Name = "btnJTutilILLookUp"
        Me.btnJTutilILLookUp.Size = New System.Drawing.Size(96, 37)
        Me.btnJTutilILLookUp.TabIndex = 20
        Me.btnJTutilILLookUp.Text = "Look Up"
        Me.btnJTutilILLookUp.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(55, 256)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(109, 18)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "Invoice Amount"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(50, 126)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(215, 16)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "----------------------------------------------------"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label13.Location = New System.Drawing.Point(55, 185)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 17)
        Me.Label13.TabIndex = 17
        Me.Label13.Text = "Job"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(55, 220)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 18)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Invoice Date"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(55, 104)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(111, 18)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Invoice Number"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(55, 147)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(74, 18)
        Me.Label16.TabIndex = 14
        Me.Label16.Text = "Customer"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(445, 75)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(270, 242)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(30, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 29)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Utilities"
        '
        'btnJtutilDone
        '
        Me.btnJtutilDone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJtutilDone.Location = New System.Drawing.Point(312, 513)
        Me.btnJtutilDone.Name = "btnJtutilDone"
        Me.btnJtutilDone.Size = New System.Drawing.Size(141, 42)
        Me.btnJtutilDone.TabIndex = 2
        Me.btnJtutilDone.Text = "Done/Exit"
        Me.btnJtutilDone.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'JTutil
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.LightBlue
        Me.ClientSize = New System.Drawing.Size(780, 571)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJtutilDone)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tabJTutil)
        Me.Name = "JTutil"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Utilities"
        Me.tabJTutil.ResumeLayout(False)
        Me.tabJTutilSelections.ResumeLayout(False)
        Me.tabJTutilSelections.PerformLayout()
        Me.tabJTutilPFA.ResumeLayout(False)
        Me.tabJTutilPFA.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tabJTutilIV.ResumeLayout(False)
        Me.tabJTutilIV.PerformLayout()
        Me.tabJTutilJDeact.ResumeLayout(False)
        Me.tabJTutilJDeact.PerformLayout()
        Me.tabJTutilIL.ResumeLayout(False)
        Me.tabJTutilIL.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tabJTutil As TabControl
    Friend WithEvents tabJTutilPFA As TabPage
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RadioBtnJTutilQB As RadioButton
    Friend WithEvents RadioBtnJTutilRec As RadioButton
    Friend WithEvents RadioBtnJTutilInv As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents lvJTutilPDFNames As ListView
    Friend WithEvents tabJTutilIV As TabPage
    Friend WithEvents tabJTutilJDeact As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents btnJtutilDone As Button
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents txtJTutilInvDate As MaskedTextBox
    Friend WithEvents txtJTutilSubJob As TextBox
    Friend WithEvents txtJTutilInvNum As TextBox
    Friend WithEvents txtJTutilJobID As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents RichTextBox2 As RichTextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents tabJTutilSelections As TabPage
    Friend WithEvents rbtnInvVoid As RadioButton
    Friend WithEvents rbtnJobDeact As RadioButton
    Friend WithEvents rbtnPrtDoc As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents btnJTutilVoid As Button
    Friend WithEvents rbtnEmail As RadioButton
    Friend WithEvents btnJTutilLookUp As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents tabJTutilIL As TabPage
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents btnJTutilILInvHist As Button
    Friend WithEvents btnJTutilILDisplay As Button
    Friend WithEvents txtJTutilILInvAmt As MaskedTextBox
    Friend WithEvents txtJTutilILInvDate As MaskedTextBox
    Friend WithEvents txtJTutilILSubjob As TextBox
    Friend WithEvents txtJTutilILInvNum As TextBox
    Friend WithEvents txtJTutilILJob As TextBox
    Friend WithEvents btnJTutilILLookUp As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtJTutilILInvPDF As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents btnJTutilDCStartProcess As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents lvJTutilDeactCandidates As ListView
    Friend WithEvents DCCustName As ColumnHeader
    Friend WithEvents DCJobName As ColumnHeader
    Friend WithEvents DCCreateDate As ColumnHeader
    Friend WithEvents DCLstInvDate As ColumnHeader
    Friend WithEvents DCCstMeth As ColumnHeader
End Class
