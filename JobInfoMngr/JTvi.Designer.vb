﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTvi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTvi))
        Me.gboxJTviInterfaceOpt = New System.Windows.Forms.GroupBox()
        Me.rbtnJTviManual = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviExport = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviReconcile = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtJTviStreet = New System.Windows.Forms.TextBox()
        Me.txtJTviCityStateZip = New System.Windows.Forms.TextBox()
        Me.txtJTviVendName = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtJTviContTel3 = New System.Windows.Forms.TextBox()
        Me.txtJTviContEmail3 = New System.Windows.Forms.TextBox()
        Me.txtJTviContName3 = New System.Windows.Forms.TextBox()
        Me.txtJTviContTel2 = New System.Windows.Forms.TextBox()
        Me.txtJTviContEmail2 = New System.Windows.Forms.TextBox()
        Me.txtJTviContName2 = New System.Windows.Forms.TextBox()
        Me.txtJTviContTel1 = New System.Windows.Forms.TextBox()
        Me.txtJTviContEmail1 = New System.Windows.Forms.TextBox()
        Me.txtJTviContName1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTviNJR = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviStmt = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviBoth = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviSer = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviMat = New System.Windows.Forms.RadioButton()
        Me.gboxJTviSerProvDoc = New System.Windows.Forms.GroupBox()
        Me.cboxJTvi1099 = New System.Windows.Forms.CheckBox()
        Me.mtxtJTviInsExpDate = New System.Windows.Forms.MaskedTextBox()
        Me.mtxtJTviLicExpDate = New System.Windows.Forms.MaskedTextBox()
        Me.cboxJTviInsCopy = New System.Windows.Forms.CheckBox()
        Me.cboxJTviLicCopy = New System.Windows.Forms.CheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtJTviShortName = New System.Windows.Forms.TextBox()
        Me.btnJTviUpdtSave = New System.Windows.Forms.Button()
        Me.btnJTviCancel = New System.Windows.Forms.Button()
        Me.btnJTviDelete = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.txtJTviLstActDate = New System.Windows.Forms.TextBox()
        Me.txtJTviEstDate = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gboxJTviPaymntTerms = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTviManEntry = New System.Windows.Forms.RadioButton()
        Me.rbtnJTvi10Days = New System.Windows.Forms.RadioButton()
        Me.rbtnJTvi30Days = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviEOM = New System.Windows.Forms.RadioButton()
        Me.rbtnJTviDUR = New System.Windows.Forms.RadioButton()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtJTviDiscPercent = New System.Windows.Forms.TextBox()
        Me.txtJTviAcctNum = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.gboxJTviScanOpt = New System.Windows.Forms.GroupBox()
        Me.cboxJTviScanOpt = New System.Windows.Forms.CheckBox()
        Me.gboxJTviInterfaceOpt.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.gboxJTviSerProvDoc.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.gboxJTviPaymntTerms.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gboxJTviScanOpt.SuspendLayout()
        Me.SuspendLayout()
        '
        'gboxJTviInterfaceOpt
        '
        Me.gboxJTviInterfaceOpt.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTviManual)
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTviExport)
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTviReconcile)
        Me.gboxJTviInterfaceOpt.Location = New System.Drawing.Point(496, 82)
        Me.gboxJTviInterfaceOpt.Name = "gboxJTviInterfaceOpt"
        Me.gboxJTviInterfaceOpt.Size = New System.Drawing.Size(173, 125)
        Me.gboxJTviInterfaceOpt.TabIndex = 0
        Me.gboxJTviInterfaceOpt.TabStop = False
        Me.gboxJTviInterfaceOpt.Text = "Activity from this Vendor should be :"
        '
        'rbtnJTviManual
        '
        Me.rbtnJTviManual.AutoSize = True
        Me.rbtnJTviManual.Location = New System.Drawing.Point(17, 90)
        Me.rbtnJTviManual.Name = "rbtnJTviManual"
        Me.rbtnJTviManual.Size = New System.Drawing.Size(156, 21)
        Me.rbtnJTviManual.TabIndex = 2
        Me.rbtnJTviManual.TabStop = True
        Me.rbtnJTviManual.Text = "Processed Manually"
        Me.rbtnJTviManual.UseVisualStyleBackColor = True
        '
        'rbtnJTviExport
        '
        Me.rbtnJTviExport.AutoSize = True
        Me.rbtnJTviExport.Location = New System.Drawing.Point(17, 63)
        Me.rbtnJTviExport.Name = "rbtnJTviExport"
        Me.rbtnJTviExport.Size = New System.Drawing.Size(125, 21)
        Me.rbtnJTviExport.TabIndex = 1
        Me.rbtnJTviExport.TabStop = True
        Me.rbtnJTviExport.Text = "Exported to QB"
        Me.rbtnJTviExport.UseVisualStyleBackColor = True
        '
        'rbtnJTviReconcile
        '
        Me.rbtnJTviReconcile.AutoSize = True
        Me.rbtnJTviReconcile.Location = New System.Drawing.Point(17, 36)
        Me.rbtnJTviReconcile.Name = "rbtnJTviReconcile"
        Me.rbtnJTviReconcile.Size = New System.Drawing.Size(99, 21)
        Me.rbtnJTviReconcile.TabIndex = 0
        Me.rbtnJTviReconcile.TabStop = True
        Me.rbtnJTviReconcile.Text = "Reconciled"
        Me.rbtnJTviReconcile.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.txtJTviStreet)
        Me.GroupBox2.Controls.Add(Me.txtJTviCityStateZip)
        Me.GroupBox2.Controls.Add(Me.txtJTviVendName)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 82)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(481, 125)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Vendor Addresses/Contacts"
        '
        'txtJTviStreet
        '
        Me.txtJTviStreet.Location = New System.Drawing.Point(142, 63)
        Me.txtJTviStreet.Name = "txtJTviStreet"
        Me.txtJTviStreet.Size = New System.Drawing.Size(314, 22)
        Me.txtJTviStreet.TabIndex = 1
        '
        'txtJTviCityStateZip
        '
        Me.txtJTviCityStateZip.Location = New System.Drawing.Point(142, 91)
        Me.txtJTviCityStateZip.Name = "txtJTviCityStateZip"
        Me.txtJTviCityStateZip.Size = New System.Drawing.Size(314, 22)
        Me.txtJTviCityStateZip.TabIndex = 2
        '
        'txtJTviVendName
        '
        Me.txtJTviVendName.Location = New System.Drawing.Point(142, 35)
        Me.txtJTviVendName.Name = "txtJTviVendName"
        Me.txtJTviVendName.Size = New System.Drawing.Size(314, 22)
        Me.txtJTviVendName.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 17)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Street Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "City, State, Zip"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Vendor Full Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(205, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Telphone "
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.txtJTviContTel3)
        Me.GroupBox3.Controls.Add(Me.txtJTviContEmail3)
        Me.GroupBox3.Controls.Add(Me.txtJTviContName3)
        Me.GroupBox3.Controls.Add(Me.txtJTviContTel2)
        Me.GroupBox3.Controls.Add(Me.txtJTviContEmail2)
        Me.GroupBox3.Controls.Add(Me.txtJTviContName2)
        Me.GroupBox3.Controls.Add(Me.txtJTviContTel1)
        Me.GroupBox3.Controls.Add(Me.txtJTviContEmail1)
        Me.GroupBox3.Controls.Add(Me.txtJTviContName1)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 213)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(550, 150)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Vendor Contacts"
        '
        'txtJTviContTel3
        '
        Me.txtJTviContTel3.Location = New System.Drawing.Point(169, 107)
        Me.txtJTviContTel3.Name = "txtJTviContTel3"
        Me.txtJTviContTel3.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContTel3.TabIndex = 7
        '
        'txtJTviContEmail3
        '
        Me.txtJTviContEmail3.Location = New System.Drawing.Point(331, 107)
        Me.txtJTviContEmail3.Name = "txtJTviContEmail3"
        Me.txtJTviContEmail3.Size = New System.Drawing.Size(210, 22)
        Me.txtJTviContEmail3.TabIndex = 8
        '
        'txtJTviContName3
        '
        Me.txtJTviContName3.Location = New System.Drawing.Point(7, 107)
        Me.txtJTviContName3.Name = "txtJTviContName3"
        Me.txtJTviContName3.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContName3.TabIndex = 6
        '
        'txtJTviContTel2
        '
        Me.txtJTviContTel2.Location = New System.Drawing.Point(169, 80)
        Me.txtJTviContTel2.Name = "txtJTviContTel2"
        Me.txtJTviContTel2.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContTel2.TabIndex = 4
        '
        'txtJTviContEmail2
        '
        Me.txtJTviContEmail2.Location = New System.Drawing.Point(331, 80)
        Me.txtJTviContEmail2.Name = "txtJTviContEmail2"
        Me.txtJTviContEmail2.Size = New System.Drawing.Size(210, 22)
        Me.txtJTviContEmail2.TabIndex = 5
        '
        'txtJTviContName2
        '
        Me.txtJTviContName2.Location = New System.Drawing.Point(7, 80)
        Me.txtJTviContName2.Name = "txtJTviContName2"
        Me.txtJTviContName2.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContName2.TabIndex = 3
        '
        'txtJTviContTel1
        '
        Me.txtJTviContTel1.Location = New System.Drawing.Point(169, 53)
        Me.txtJTviContTel1.Name = "txtJTviContTel1"
        Me.txtJTviContTel1.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContTel1.TabIndex = 1
        '
        'txtJTviContEmail1
        '
        Me.txtJTviContEmail1.Location = New System.Drawing.Point(331, 53)
        Me.txtJTviContEmail1.Name = "txtJTviContEmail1"
        Me.txtJTviContEmail1.Size = New System.Drawing.Size(210, 22)
        Me.txtJTviContEmail1.TabIndex = 2
        '
        'txtJTviContName1
        '
        Me.txtJTviContName1.Location = New System.Drawing.Point(7, 53)
        Me.txtJTviContName1.Name = "txtJTviContName1"
        Me.txtJTviContName1.Size = New System.Drawing.Size(156, 22)
        Me.txtJTviContName1.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(406, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 17)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Email"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(65, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 17)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Name"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.rbtnJTviNJR)
        Me.GroupBox4.Controls.Add(Me.rbtnJTviStmt)
        Me.GroupBox4.Controls.Add(Me.rbtnJTviBoth)
        Me.GroupBox4.Controls.Add(Me.rbtnJTviSer)
        Me.GroupBox4.Controls.Add(Me.rbtnJTviMat)
        Me.GroupBox4.Location = New System.Drawing.Point(351, 7)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(428, 69)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Vendor Type"
        '
        'rbtnJTviNJR
        '
        Me.rbtnJTviNJR.AutoSize = True
        Me.rbtnJTviNJR.Location = New System.Drawing.Point(253, 21)
        Me.rbtnJTviNJR.Name = "rbtnJTviNJR"
        Me.rbtnJTviNJR.Size = New System.Drawing.Size(137, 38)
        Me.rbtnJTviNJR.TabIndex = 5
        Me.rbtnJTviNJR.TabStop = True
        Me.rbtnJTviNJR.Text = "Non-Job-Related" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Expenses"
        Me.rbtnJTviNJR.UseVisualStyleBackColor = True
        '
        'rbtnJTviStmt
        '
        Me.rbtnJTviStmt.AutoSize = True
        Me.rbtnJTviStmt.Location = New System.Drawing.Point(145, 43)
        Me.rbtnJTviStmt.Name = "rbtnJTviStmt"
        Me.rbtnJTviStmt.Size = New System.Drawing.Size(93, 21)
        Me.rbtnJTviStmt.TabIndex = 4
        Me.rbtnJTviStmt.TabStop = True
        Me.rbtnJTviStmt.Text = "Statement"
        Me.rbtnJTviStmt.UseVisualStyleBackColor = True
        '
        'rbtnJTviBoth
        '
        Me.rbtnJTviBoth.AutoSize = True
        Me.rbtnJTviBoth.Location = New System.Drawing.Point(145, 21)
        Me.rbtnJTviBoth.Name = "rbtnJTviBoth"
        Me.rbtnJTviBoth.Size = New System.Drawing.Size(58, 21)
        Me.rbtnJTviBoth.TabIndex = 3
        Me.rbtnJTviBoth.TabStop = True
        Me.rbtnJTviBoth.Text = "Both"
        Me.rbtnJTviBoth.UseVisualStyleBackColor = True
        '
        'rbtnJTviSer
        '
        Me.rbtnJTviSer.AutoSize = True
        Me.rbtnJTviSer.Location = New System.Drawing.Point(43, 43)
        Me.rbtnJTviSer.Name = "rbtnJTviSer"
        Me.rbtnJTviSer.Size = New System.Drawing.Size(83, 21)
        Me.rbtnJTviSer.TabIndex = 2
        Me.rbtnJTviSer.TabStop = True
        Me.rbtnJTviSer.Text = "Services"
        Me.rbtnJTviSer.UseVisualStyleBackColor = True
        '
        'rbtnJTviMat
        '
        Me.rbtnJTviMat.AutoSize = True
        Me.rbtnJTviMat.Location = New System.Drawing.Point(43, 21)
        Me.rbtnJTviMat.Name = "rbtnJTviMat"
        Me.rbtnJTviMat.Size = New System.Drawing.Size(86, 21)
        Me.rbtnJTviMat.TabIndex = 0
        Me.rbtnJTviMat.TabStop = True
        Me.rbtnJTviMat.Text = "Materials"
        Me.rbtnJTviMat.UseVisualStyleBackColor = True
        '
        'gboxJTviSerProvDoc
        '
        Me.gboxJTviSerProvDoc.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTviSerProvDoc.Controls.Add(Me.cboxJTvi1099)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.mtxtJTviInsExpDate)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.mtxtJTviLicExpDate)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.cboxJTviInsCopy)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.cboxJTviLicCopy)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.Label10)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.Label9)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.Label8)
        Me.gboxJTviSerProvDoc.Controls.Add(Me.Label2)
        Me.gboxJTviSerProvDoc.Location = New System.Drawing.Point(799, 82)
        Me.gboxJTviSerProvDoc.Name = "gboxJTviSerProvDoc"
        Me.gboxJTviSerProvDoc.Size = New System.Drawing.Size(269, 125)
        Me.gboxJTviSerProvDoc.TabIndex = 2
        Me.gboxJTviSerProvDoc.TabStop = False
        Me.gboxJTviSerProvDoc.Text = "Service Provider Documentation"
        '
        'cboxJTvi1099
        '
        Me.cboxJTvi1099.AutoSize = True
        Me.cboxJTvi1099.Location = New System.Drawing.Point(9, 98)
        Me.cboxJTvi1099.Name = "cboxJTvi1099"
        Me.cboxJTvi1099.Size = New System.Drawing.Size(242, 21)
        Me.cboxJTvi1099.TabIndex = 8
        Me.cboxJTvi1099.Text = "Check if Year-End 1099 Required"
        Me.cboxJTvi1099.UseVisualStyleBackColor = True
        '
        'mtxtJTviInsExpDate
        '
        Me.mtxtJTviInsExpDate.Location = New System.Drawing.Point(160, 67)
        Me.mtxtJTviInsExpDate.Mask = "00/00/0000"
        Me.mtxtJTviInsExpDate.Name = "mtxtJTviInsExpDate"
        Me.mtxtJTviInsExpDate.Size = New System.Drawing.Size(86, 22)
        Me.mtxtJTviInsExpDate.TabIndex = 7
        Me.mtxtJTviInsExpDate.ValidatingType = GetType(Date)
        '
        'mtxtJTviLicExpDate
        '
        Me.mtxtJTviLicExpDate.Location = New System.Drawing.Point(160, 43)
        Me.mtxtJTviLicExpDate.Mask = "00/00/0000"
        Me.mtxtJTviLicExpDate.Name = "mtxtJTviLicExpDate"
        Me.mtxtJTviLicExpDate.Size = New System.Drawing.Size(86, 22)
        Me.mtxtJTviLicExpDate.TabIndex = 6
        Me.mtxtJTviLicExpDate.ValidatingType = GetType(Date)
        '
        'cboxJTviInsCopy
        '
        Me.cboxJTviInsCopy.AutoSize = True
        Me.cboxJTviInsCopy.Location = New System.Drawing.Point(106, 72)
        Me.cboxJTviInsCopy.Name = "cboxJTviInsCopy"
        Me.cboxJTviInsCopy.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTviInsCopy.TabIndex = 5
        Me.cboxJTviInsCopy.UseVisualStyleBackColor = True
        '
        'cboxJTviLicCopy
        '
        Me.cboxJTviLicCopy.AutoSize = True
        Me.cboxJTviLicCopy.Location = New System.Drawing.Point(106, 47)
        Me.cboxJTviLicCopy.Name = "cboxJTviLicCopy"
        Me.cboxJTviLicCopy.Size = New System.Drawing.Size(18, 17)
        Me.cboxJTviLicCopy.TabIndex = 4
        Me.cboxJTviLicCopy.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(79, 21)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 17)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Have Copy"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(157, 21)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(104, 17)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Expiration Date"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 73)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 17)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Insurance"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "License"
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox6.Controls.Add(Me.txtJTviShortName)
        Me.GroupBox6.Location = New System.Drawing.Point(145, 7)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(200, 67)
        Me.GroupBox6.TabIndex = 5
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Vendor Short Name"
        '
        'txtJTviShortName
        '
        Me.txtJTviShortName.Location = New System.Drawing.Point(5, 30)
        Me.txtJTviShortName.Name = "txtJTviShortName"
        Me.txtJTviShortName.Size = New System.Drawing.Size(188, 22)
        Me.txtJTviShortName.TabIndex = 0
        '
        'btnJTviUpdtSave
        '
        Me.btnJTviUpdtSave.Location = New System.Drawing.Point(16, 20)
        Me.btnJTviUpdtSave.Name = "btnJTviUpdtSave"
        Me.btnJTviUpdtSave.Size = New System.Drawing.Size(113, 40)
        Me.btnJTviUpdtSave.TabIndex = 0
        Me.btnJTviUpdtSave.Text = "Update/Save"
        Me.btnJTviUpdtSave.UseVisualStyleBackColor = True
        '
        'btnJTviCancel
        '
        Me.btnJTviCancel.Location = New System.Drawing.Point(270, 20)
        Me.btnJTviCancel.Name = "btnJTviCancel"
        Me.btnJTviCancel.Size = New System.Drawing.Size(113, 40)
        Me.btnJTviCancel.TabIndex = 7
        Me.btnJTviCancel.Text = "Cancel"
        Me.btnJTviCancel.UseVisualStyleBackColor = True
        '
        'btnJTviDelete
        '
        Me.btnJTviDelete.Location = New System.Drawing.Point(141, 20)
        Me.btnJTviDelete.Name = "btnJTviDelete"
        Me.btnJTviDelete.Size = New System.Drawing.Size(113, 40)
        Me.btnJTviDelete.TabIndex = 8
        Me.btnJTviDelete.Text = "Delete"
        Me.btnJTviDelete.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox7.Controls.Add(Me.btnJTviDelete)
        Me.GroupBox7.Controls.Add(Me.btnJTviUpdtSave)
        Me.GroupBox7.Controls.Add(Me.btnJTviCancel)
        Me.GroupBox7.Location = New System.Drawing.Point(9, 369)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(400, 78)
        Me.GroupBox7.TabIndex = 9
        Me.GroupBox7.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(4, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(119, 50)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Vendor " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Information"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(415, 370)
        Me.TextBox14.Multiline = True
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(653, 75)
        Me.TextBox14.TabIndex = 11
        Me.TextBox14.Text = resources.GetString("TextBox14.Text")
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox8.Controls.Add(Me.txtJTviLstActDate)
        Me.GroupBox8.Controls.Add(Me.txtJTviEstDate)
        Me.GroupBox8.Controls.Add(Me.Label13)
        Me.GroupBox8.Controls.Add(Me.Label12)
        Me.GroupBox8.Location = New System.Drawing.Point(785, 7)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(283, 67)
        Me.GroupBox8.TabIndex = 12
        Me.GroupBox8.TabStop = False
        '
        'txtJTviLstActDate
        '
        Me.txtJTviLstActDate.Location = New System.Drawing.Point(177, 35)
        Me.txtJTviLstActDate.Name = "txtJTviLstActDate"
        Me.txtJTviLstActDate.ReadOnly = True
        Me.txtJTviLstActDate.Size = New System.Drawing.Size(100, 22)
        Me.txtJTviLstActDate.TabIndex = 3
        '
        'txtJTviEstDate
        '
        Me.txtJTviEstDate.Location = New System.Drawing.Point(178, 9)
        Me.txtJTviEstDate.Name = "txtJTviEstDate"
        Me.txtJTviEstDate.ReadOnly = True
        Me.txtJTviEstDate.Size = New System.Drawing.Size(100, 22)
        Me.txtJTviEstDate.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 35)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(133, 17)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Date of Last Activity"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 11)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(165, 17)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Date Vendor Established"
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'gboxJTviPaymntTerms
        '
        Me.gboxJTviPaymntTerms.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTviPaymntTerms.Controls.Add(Me.GroupBox1)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.TextBox3)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.Label16)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.txtJTviDiscPercent)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.txtJTviAcctNum)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.Label15)
        Me.gboxJTviPaymntTerms.Controls.Add(Me.Label14)
        Me.gboxJTviPaymntTerms.Location = New System.Drawing.Point(565, 213)
        Me.gboxJTviPaymntTerms.Name = "gboxJTviPaymntTerms"
        Me.gboxJTviPaymntTerms.Size = New System.Drawing.Size(503, 150)
        Me.gboxJTviPaymntTerms.TabIndex = 9
        Me.gboxJTviPaymntTerms.TabStop = False
        Me.gboxJTviPaymntTerms.Text = "Account Information and Payment Terms"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbtnJTviManEntry)
        Me.GroupBox1.Controls.Add(Me.rbtnJTvi10Days)
        Me.GroupBox1.Controls.Add(Me.rbtnJTvi30Days)
        Me.GroupBox1.Controls.Add(Me.rbtnJTviEOM)
        Me.GroupBox1.Controls.Add(Me.rbtnJTviDUR)
        Me.GroupBox1.Location = New System.Drawing.Point(286, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(179, 130)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Terms"
        '
        'rbtnJTviManEntry
        '
        Me.rbtnJTviManEntry.AutoSize = True
        Me.rbtnJTviManEntry.Location = New System.Drawing.Point(4, 101)
        Me.rbtnJTviManEntry.Name = "rbtnJTviManEntry"
        Me.rbtnJTviManEntry.Size = New System.Drawing.Size(176, 21)
        Me.rbtnJTviManEntry.TabIndex = 8
        Me.rbtnJTviManEntry.TabStop = True
        Me.rbtnJTviManEntry.Text = "Manual Due Date Entry"
        Me.rbtnJTviManEntry.UseVisualStyleBackColor = True
        '
        'rbtnJTvi10Days
        '
        Me.rbtnJTvi10Days.AutoSize = True
        Me.rbtnJTvi10Days.Location = New System.Drawing.Point(4, 41)
        Me.rbtnJTvi10Days.Name = "rbtnJTvi10Days"
        Me.rbtnJTvi10Days.Size = New System.Drawing.Size(81, 21)
        Me.rbtnJTvi10Days.TabIndex = 7
        Me.rbtnJTvi10Days.TabStop = True
        Me.rbtnJTvi10Days.Text = "10 Days"
        Me.rbtnJTvi10Days.UseVisualStyleBackColor = True
        '
        'rbtnJTvi30Days
        '
        Me.rbtnJTvi30Days.AutoSize = True
        Me.rbtnJTvi30Days.Location = New System.Drawing.Point(4, 61)
        Me.rbtnJTvi30Days.Name = "rbtnJTvi30Days"
        Me.rbtnJTvi30Days.Size = New System.Drawing.Size(81, 21)
        Me.rbtnJTvi30Days.TabIndex = 6
        Me.rbtnJTvi30Days.TabStop = True
        Me.rbtnJTvi30Days.Text = "30 Days"
        Me.rbtnJTvi30Days.UseVisualStyleBackColor = True
        '
        'rbtnJTviEOM
        '
        Me.rbtnJTviEOM.AutoSize = True
        Me.rbtnJTviEOM.Location = New System.Drawing.Point(4, 80)
        Me.rbtnJTviEOM.Name = "rbtnJTviEOM"
        Me.rbtnJTviEOM.Size = New System.Drawing.Size(60, 21)
        Me.rbtnJTviEOM.TabIndex = 5
        Me.rbtnJTviEOM.TabStop = True
        Me.rbtnJTviEOM.Text = "EOM"
        Me.rbtnJTviEOM.UseVisualStyleBackColor = True
        '
        'rbtnJTviDUR
        '
        Me.rbtnJTviDUR.AutoSize = True
        Me.rbtnJTviDUR.Location = New System.Drawing.Point(4, 22)
        Me.rbtnJTviDUR.Name = "rbtnJTviDUR"
        Me.rbtnJTviDUR.Size = New System.Drawing.Size(134, 21)
        Me.rbtnJTviDUR.TabIndex = 4
        Me.rbtnJTviDUR.TabStop = True
        Me.rbtnJTviDUR.Text = "Due Upon Recpt"
        Me.rbtnJTviDUR.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(6, 76)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(272, 68)
        Me.TextBox3.TabIndex = 12
        Me.TextBox3.Text = "This panel enables input of information to sup-port the accounting system interfa" &
    "ce. It is required for Statements, and individual vendors whose invoices will be" &
    " exported for payment."
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(138, 53)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(139, 17)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "(If paid by Due Date)"
        '
        'txtJTviDiscPercent
        '
        Me.txtJTviDiscPercent.Location = New System.Drawing.Point(102, 50)
        Me.txtJTviDiscPercent.Name = "txtJTviDiscPercent"
        Me.txtJTviDiscPercent.Size = New System.Drawing.Size(31, 22)
        Me.txtJTviDiscPercent.TabIndex = 10
        '
        'txtJTviAcctNum
        '
        Me.txtJTviAcctNum.Location = New System.Drawing.Point(102, 24)
        Me.txtJTviAcctNum.Name = "txtJTviAcctNum"
        Me.txtJTviAcctNum.Size = New System.Drawing.Size(166, 22)
        Me.txtJTviAcctNum.TabIndex = 9
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(21, 53)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(79, 17)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Discount %"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(21, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 17)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Account #"
        '
        'gboxJTviScanOpt
        '
        Me.gboxJTviScanOpt.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTviScanOpt.Controls.Add(Me.cboxJTviScanOpt)
        Me.gboxJTviScanOpt.Location = New System.Drawing.Point(675, 82)
        Me.gboxJTviScanOpt.Name = "gboxJTviScanOpt"
        Me.gboxJTviScanOpt.Size = New System.Drawing.Size(118, 125)
        Me.gboxJTviScanOpt.TabIndex = 3
        Me.gboxJTviScanOpt.TabStop = False
        Me.gboxJTviScanOpt.Text = "Document Retention"
        '
        'cboxJTviScanOpt
        '
        Me.cboxJTviScanOpt.AutoSize = True
        Me.cboxJTviScanOpt.Location = New System.Drawing.Point(12, 56)
        Me.cboxJTviScanOpt.Name = "cboxJTviScanOpt"
        Me.cboxJTviScanOpt.Size = New System.Drawing.Size(89, 38)
        Me.cboxJTviScanOpt.TabIndex = 0
        Me.cboxJTviScanOpt.Text = "Suggest" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Scanning"
        Me.cboxJTviScanOpt.UseVisualStyleBackColor = True
        '
        'JTvi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1078, 454)
        Me.ControlBox = False
        Me.Controls.Add(Me.gboxJTviScanOpt)
        Me.Controls.Add(Me.gboxJTviPaymntTerms)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.TextBox14)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.gboxJTviSerProvDoc)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gboxJTviInterfaceOpt)
        Me.Name = "JTvi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Vendor Information"
        Me.TopMost = True
        Me.gboxJTviInterfaceOpt.ResumeLayout(False)
        Me.gboxJTviInterfaceOpt.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.gboxJTviSerProvDoc.ResumeLayout(False)
        Me.gboxJTviSerProvDoc.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.gboxJTviPaymntTerms.ResumeLayout(False)
        Me.gboxJTviPaymntTerms.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gboxJTviScanOpt.ResumeLayout(False)
        Me.gboxJTviScanOpt.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gboxJTviInterfaceOpt As GroupBox
    Friend WithEvents rbtnJTviManual As RadioButton
    Friend WithEvents rbtnJTviExport As RadioButton
    Friend WithEvents rbtnJTviReconcile As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtJTviStreet As TextBox
    Friend WithEvents txtJTviCityStateZip As TextBox
    Friend WithEvents txtJTviVendName As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtJTviContTel3 As TextBox
    Friend WithEvents txtJTviContEmail3 As TextBox
    Friend WithEvents txtJTviContName3 As TextBox
    Friend WithEvents txtJTviContTel2 As TextBox
    Friend WithEvents txtJTviContEmail2 As TextBox
    Friend WithEvents txtJTviContName2 As TextBox
    Friend WithEvents txtJTviContTel1 As TextBox
    Friend WithEvents txtJTviContEmail1 As TextBox
    Friend WithEvents txtJTviContName1 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents rbtnJTviMat As RadioButton
    Friend WithEvents gboxJTviSerProvDoc As GroupBox
    Friend WithEvents mtxtJTviLicExpDate As MaskedTextBox
    Friend WithEvents cboxJTviInsCopy As CheckBox
    Friend WithEvents cboxJTviLicCopy As CheckBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cboxJTvi1099 As CheckBox
    Friend WithEvents mtxtJTviInsExpDate As MaskedTextBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txtJTviShortName As TextBox
    Friend WithEvents btnJTviUpdtSave As Button
    Friend WithEvents btnJTviCancel As Button
    Friend WithEvents btnJTviDelete As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents rbtnJTviBoth As RadioButton
    Friend WithEvents rbtnJTviSer As RadioButton
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents txtJTviLstActDate As TextBox
    Friend WithEvents txtJTviEstDate As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents gboxJTviPaymntTerms As GroupBox
    Friend WithEvents rbtnJTviStmt As RadioButton
    Friend WithEvents gboxJTviScanOpt As GroupBox
    Friend WithEvents cboxJTviScanOpt As CheckBox
    Friend WithEvents rbtnJTviDUR As RadioButton
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtJTviDiscPercent As TextBox
    Friend WithEvents txtJTviAcctNum As TextBox
    Friend WithEvents rbtnJTviManEntry As RadioButton
    Friend WithEvents rbtnJTvi10Days As RadioButton
    Friend WithEvents rbtnJTvi30Days As RadioButton
    Friend WithEvents rbtnJTviEOM As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents rbtnJTviNJR As RadioButton
End Class
