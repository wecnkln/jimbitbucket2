﻿Imports System.IO
''' <summary>J.I.M. has a large list of operator adjustable system parameters. Method within this class handles that maintenance.</summary>
Public Class JTVarMaint
    ' ------------------------------------------------------
    ' Signon related vars
    ' ------------------------------------------------------
    ''' <summary>
    ''' This tag is the first item set when the system is initiatied and the 
    ''' last thing reset when the system set down using an orderly method. If its 
    ''' value "is not "SAFE" when the system starts, it means that the last J.I.M. 
    ''' did not end normally.
    ''' </summary>
    Public JTvmSafeClose As String = "Safe"
    ''' <summary>
    ''' The following fields relate to validating software release and 
    ''' also keeps track of whoo, if anyone is logged in and the
    ''' date and time the session was started.
    ''' </summary>
    Public JTvmSWRelease As String
    Public JTvmSignedInUser As String
    Public JTvmSignInTime As String
    ''' <summary>
    ''' This variable keeps track of the system year. When the calendar year 
    ''' changes and is different from the system year. J.I.M. senses that year-end 
    ''' processes need to be run. These routines can be deferred, but the operator 
    ''' will reminded each time the system starts until they are completed.
    ''' </summary>
    Public JTvmSystemYear As String = "2021"
    ''' <summary>
    ''' This tag is for the one time conversion of the NLC PDF file names that have already been invoiced.
    ''' It will only be used to convert JRV files. It can be deleted at a later date. It will be used in this
    ''' class in the load and unload of the variable. It will also be used in JTnlcArrayLoad. It the code
    ''' is to be taken out. It must be deleted in all locations. 10/9/2020
    ''' </summary>
    Public JTvmNLCNameConverionComplete As Boolean = False
    '
    ''' <summary>
    ''' Following tag determines whether the dashboard will be presented at Startup.
    ''' True = it will be displayed
    ''' False = it will be skipped
    ''' </summary>
    Public JTvmShowDashBoard As Boolean = False
    '
    ' ------------------------------------------------------
    ' Signon related vars
    ' ------------------------------------------------------

    ''' <summary>
    ''' This is the file path to get to J.I.M. data. If is created by the system. 
    ''' It is stored in the JTVars so that the system can check to make sure the 
    ''' file were created on this computer. It is possible the the files have been 
    ''' transferred from another computer on purpose ... this check makes sure that 
    ''' operator knows that this has happened.
    ''' </summary>
    Public JTvmFilePath As String = "\Users\wecnk\Documents\JobInfoMgr\"
    Public JTvmFilePathDoc As String
    Public JTvmUsePassWord As Boolean = True
    Public JTvmPassWord As String
    Public JTvmPWResetEmail As String = ""
    Public JTvmDemoBypass As String = Nothing
    Public JTvmEventLogEnabled As Boolean = False
    '
    ' -------------------------------------------------------------------------
    ' Company Related Constants, Logo, Etc.
    ' -------------------------------------------------------------------------
    Public CompanyNme As String = "Company Name"
    Public CompanyMailingAddressL1 As String = "Company Street Address"
    Public CompanyMailingAddressL2 As String = "Company City, State Zip"
    Public CompanyMailingAddressL3 As String = ""

    Public CompanyContactName As String = "Company Contact Name"
    Public CompanyContactEmail As String = "CompanyContact@gmail.com"
    Public GoogleValidation As String = "Google 2-step Validation Code"
    Public CompanyContactPhone As String = "555-555-5555"
    Public CompanyContactFax As String = ""
    Public CompanyLogo = ""
    ' -------------------------------------------------------------------------
    ' Alternate Company Name, Address, Logo, Etc.
    ' -------------------------------------------------------------------------
    Public AltCompanyNme As String = ""
    Public AltCompanyMailingAddressL1 As String = ""
    Public AltCompanyMailingAddressL2 As String = ""
    Public AltCompanyMailingAddressL3 As String = ""
    '
    Public AltCompanyLogo = ""
    ' ------------------------------------------------------
    ' Job specific Parameter defaults
    ' ------------------------------------------------------
    Public JTvmMatMargin As String = 15
    Public JTvmSubMargin As String = 15
    Public JTvmINHOUSEMargin As String = 25
    '
    ' ---------------------------------------------------------
    ' Payroll Email Notification Paramters/Other variables
    ' ---------------------------------------------------------
    Public SendW2PREmail As Boolean = True
    Public Send1099PREmail As Boolean = True
    '
    Public PayrollTaxLoadPercent As Double = 20
    ' Email Settings

    Public JTFromEmailAddress As String = "generalemailaddress@gmail.com"
    Public JTCCEmailAddress As String = ""
    ' W2 Payroll Service Email
    Public PayrollEmailAddressW2 As String = "payrollcompany@gmail.com"

    Public EmailMsgTextW2 As String = "Below Is the timecard information For the week ending Date In the message's subject. Please call if you have any question." & "vbCrLf" & "vbCrLf" _
       & "John Vickerman" & "vbCrLf" & "860.389.7797" & "vbCrLf" & "vbCrLf" & "--------------------------------------------------------------------------------------------"
    ' 1099 Email Variables
    Public EmailMsgText1099 As String = "Below is the timecard information for the week ending date in the message's subject. " _
       & "When you send the invoice that represents the hours documented in this email, please make reference to this Email. Please call if you have any questions." & "vbCrLf" & "vbCrLf" _
       & "John Vickerman" & "vbCrLf" & "860.389.7797" & "vbCrLf" & "vbCrLf" & "--------------------------------------------------------------------------------------------"
    '

    ' -----------------------------------------------
    ' Reconciliation Switches
    ' -----------------------------------------------
    Public ReconcilMat As Boolean = True ' switch for Materials
    Public ReconcilSub As Boolean = True ' switch for Subcontractors
    Public Reconcil1099PR As Boolean = False 'switch for 1099 PR expenses
    Public ReconcilNJR As Boolean = True ' switch for Non-Job Related.
    Public ReconMakeSuggestions As Boolean = True  ' make suggestions for reconciliation based on past activity.
    '
    ' -----------------------------------------------------------------------------------
    ' Invoicing Variables/Options, Etc.
    ' -----------------------------------------------------------------------------------
    '
    ' Labor Section Option
    Public InvLabTaskSummary As Boolean = True
    Public InvLabWorkerDetail As Boolean = True
    Public InvLabTotals As Boolean = True
    '
    Public InvSubSerDetails As Boolean = True
    Public InvSubSerTotals As Boolean = True
    '
    Public InvMatDetails As Boolean = True
    Public InvMatTotals As Boolean = True
    '
    '
    ' Material/Subcontractor Options
    Public InvMatShowCost As Boolean = True
    Public InvSubShowCost As Boolean = True
    '
    Public InvFootertext As String = "This area contains company specific text to be printed as the footer on the invoice."
    '
    Public InvNumber As String = 1000
    Public AltInvNumber As String = 8000
    '
    ' --------------------------------------------------------
    ' The following fields comtrol Email Extract options in
    ' JIMGmailExtrct.
    ' --------------------------------------------------------
    Public WebImageExt As Boolean = False
    Public WebEmailExt As Boolean = False
    '
    ' -----------------------------------------------------
    ' This number is to make keys unique when AUP Finalize 
    ' data is stored in slJTasHist. It will be incremented 
    ' and reset back to zero programatically.
    ' -----------------------------------------------------
    Public AUPFinalizeNumber As String = 0
    ' -------------------------------------------------------
    ' Variables, Etc. for state sales tax management
    ' -------------------------------------------------------
    Public Structure SalesTaxStruct
        Dim STSState As String
        Dim STSStHasTax As Boolean
        Dim STSStRate As String
        Dim STSLabHasTax As Boolean
        Dim STSLabRate As String
        Dim STSSubHasTax As Boolean
        Dim STSSubRate As String
        Dim STSMatHasTax As Boolean
        Dim STSMatRate As String
    End Structure
    '
    Public slJTvmTax As New SortedList
    '
    ' ---------------------------------------------------------
    ' Variables, SL, etc. for NJR Expense Accounts
    ' ---------------------------------------------------------
    Public Structure NJRExpAcctStruct
        Dim JTvmNJREAName As String
        Dim JTvmNJREADescription As String
    End Structure
    '
    Public slJTvmNJREA As New SortedList
    '
    Dim tmpNJREAItem As NJRExpAcctStruct

    ' 
    ' 
    '
    ' -------------------------------------------------------
    ' Variables related to Data Purging
    ' -------------------------------------------------------
    Public LabMinLife As String = "30"
    Public NLCMinLife As String = "30"
    Public MinDaysAfterInv As String = "10"
    '
    Public PurgeTiming As String = "45"
    Public LstTKPurge As String = "2017320"
    Public LstNLCPurge As String = "2017320"
    '
    Public DataPurgeJobNorm As String
    Public DataPurgeJobMstr As String
    Public DataPurgeJobSub As String
    '  
    ' -----------------------------------------------------------
    ' Variables related to File Quiet and Achival Backup Creation
    ' -----------------------------------------------------------

    Public JTAchBkupSwitch As Boolean
    Public JTAchBkupFreq As String
    Public JTAchBkupGen As String
    Public JTAchBkupPath As String
    Public JTAchBkupDate As String = "2017001"
    Public JTAchBkupSerNum As String = "0001"
    '
    Public JTLstQP As Date
    Public JTLstAchBkup As Date
    '
    ' -------------------------------------------------------------
    ' General System Variables
    ' -------------------------------------------------------------
    Public JTgvMMJobSeq As String = "J"    'J - Alpha Job Sequence ... A - Seq. by open amounts (decending)
    Public JTgvNLCSeq As String = "J"    'J - Alpha sequence by job .... D - Seq. by entry date (descending)
    ' Scanning Option Switches
    Public JTgvScanNLC As Boolean = False
    Public JTgvScanReconcil As Boolean = False
    Public JTgvScanAUP As Boolean = False

    '
    ' -----------------------------------------------------------------------------------
    ' Variables for internal program use only ... will not appear on the VarMaint panels.
    ' (They will be part of the JTGlobSettings.dat file)
    ' -----------------------------------------------------------------------------------
    ' Variables to support suspended reconciliation
    Public JTGVrecName As String = ""
    Public JTGVrecStmtAmt As String = ""
    Public JTGVrecOptMsg As String = ""
    Public JTGVrecVndShrtName As String = ""          'field added to support new reconcilation (4/18/2019)
    Public JTGVrecVndInvDate As String = ""            'field added to support new reconcilation (4/18/2019)
    Public JTGVrecVndInvNumber As String = ""            'field added to support new reconcilation (4/18/2019)
    Public JTGVrecVndInvDueDate As String = ""            'field added to support new reconcilation (4/18/2019)
    '
    ' ------------------------------------------------------------------
    ' Variables for last used Main Menu job section format.
    ' --- will be saved to file but not modifiable on Glob Var panel ---
    ' -----------------
    ' --- values of these fields are updated when the desired display of
    ' --- the MainMenu is modified by the operator.
    ' ------------------------------------------------------------------
    '
    Public JTGVShowCstP As Boolean = True
    Public JTGVShowAUP As Boolean = True
    Public JTGVShowDeactJobs As Boolean = True
    '
    '

    Private Sub btnDone_Click(sender As Object, e As EventArgs) Handles btnDone.Click
        JTvmUnloadPanel()
        Me.Hide()
        JTvmSafeClose = "Active"
        JTvmFlush()

        JTMainMenu.JTMMinit()
    End Sub
    Sub JTvmInit()
        ' --------------------------------------------
        ' Load values onto JTVarMaint panel
        ' --------------------------------------------
        cboxJTvmUsePW.Checked = JTvmUsePassWord
        cboxJTgvDashBoard.Checked = JTvmShowDashBoard
        '
        txtJTvmCompName.Text = CompanyNme
        txtJTvmCompAddL1.Text = CompanyMailingAddressL1
        txtJTvmCompAddL2.Text = CompanyMailingAddressL2
        txtJTVMCompAddL3.Text = CompanyMailingAddressL3
        '
        txtJTvmAltCompName.Text = AltCompanyNme
        txtJTvmAltCompAddL1.Text = AltCompanyMailingAddressL1
        txtJTvmAltCompAddL2.Text = AltCompanyMailingAddressL2
        txtJTvmAltCompAddL3.Text = AltCompanyMailingAddressL3
        '
        txtJTvmCompContact.Text = CompanyContactName
        txtJTvmCompEmail.Text = CompanyContactEmail
        txtJTvmGoogleValidation.Text = GoogleValidation
        txtJTvmCompPhone.Text = CompanyContactPhone
        txtJTvmCompFax.Text = CompanyContactFax
        txtJTvmCompanyLogo.Text = CompanyLogo
        txtJTvmAltCompanyLogo.Text = AltCompanyLogo
        txtJTvmInvNumber.Text = InvNumber
        txtJTvmAltInvNumber.Text = AltInvNumber
        '
        txtJTvmMatMargin.Text = JTvmMatMargin
        txtJTvmSubMargin.Text = JTvmSubMargin
        txtJTvmINHOUSEMargin.Text = JTvmINHOUSEMargin
        '
        ' Payroll Associated Variables
        '
        cboxJTvmW2Email.Checked = SendW2PREmail
        cboxJTvm1099Email.Checked = Send1099PREmail
        '
        txtJTvmPRTaxLoad.Text = PayrollTaxLoadPercent
        txtJTvmPREmailFrom.Text = JTFromEmailAddress
        txtJTvmInvCCEmail.Text = JTCCEmailAddress
        txtJTvmPREmailTo.Text = PayrollEmailAddressW2
        rtxtJTvmW2EmailText.Text = EmailMsgTextW2
        txtJTvm1099EmailText.Text = EmailMsgText1099
        '
        ' Reconciliation related variables
        '
        cboxReconMat.Checked = ReconcilMat
        cboxReconSub.Checked = ReconcilSub
        cboxRecon1099.Checked = Reconcil1099PR
        cboxReconNJR.Checked = ReconcilNJR
        cboxReconSuggest.Checked = ReconMakeSuggestions

        '
        ' Invoice related variables
        '
        rbtnInvLabTskSum.Checked = InvLabTaskSummary
        rbtnInvWrkrDet.Checked = InvLabWorkerDetail
        rbtnInvLaborTotals.Checked = InvLabTotals
        rbtnInvSubSerDetails.Checked = InvSubSerDetails
        rbtnInvSubSerTotals.Checked = InvSubSerTotals
        rbtnInvMatDetail.Checked = InvMatDetails
        rbtnInvMatTotals.Checked = InvMatTotals
        '
        cboxCstPrntMat.Checked = InvMatShowCost
        cboxCstPrntSub.Checked = InvSubShowCost
        '
        txtInvFooter.Text = InvFootertext
        ' ---------------------------------------------------
        ' Email Extract Options --- for JIMGmailExtrct
        ' ---------------------------------------------------
        If WebImageExt = True Then
            rbtnJTvmImageExtOn.Checked = True
        Else
            rbtnJTvmImageExtOff.Checked = True
        End If
        If WebEmailExt = True Then
            rbtnJTvmEmailExtOn.Checked = True
        Else
            rbtnJTvmEmailExtOff.Checked = True
        End If
        ' ----------------------------------------------------
        ' Scanner Options --- currently not being used
        ' ----------------------------------------------------
        cboxJTvmScanNLC.Checked = JTgvScanNLC
        cboxJTvmScanReconcil.Checked = JTgvScanReconcil
        cboxJTvmScanAUP.Checked = JTgvScanAUP
        ' ---------------------------------------------
        ' Sales Tax Table Load
        ' ---------------------------------------------
        JTvmSTSslTolv()
        '
        ' 
        ' NJREA sl load.
        '
        JTvmLoadlvJTvmNJRAccounts()

        '
        txtJTvmLabMinLife.Text = LabMinLife
        txtJTvmNLCMinLife.Text = NLCMinLife
        txtJTvmMinDaysAfterInv.Text = MinDaysAfterInv
        '
        txtJTvmPurgeTiming.Text = PurgeTiming
        txtJTvmLstTKPurge.Text = LstTKPurge
        txtJTvmLstNLCPurge.Text = LstNLCPurge
        '
        txtJTvmDPJNorm.Text = DataPurgeJobNorm
        txtJTvmDPJMstr.Text = DataPurgeJobMstr
        txtJTvmDPJSub.Text = DataPurgeJobSub
        '
        ' Backup Related Variables
        '
        cboxJTvmBkupAch.Checked = JTAchBkupSwitch
        '
        txtJTvmBkupAchFreq.Text = JTAchBkupFreq
        txtJTvmBkupAchGen.Text = JTAchBkupGen
        txtJTvmBkupAchPath.Text = JTAchBkupPath
        txtJTvmBkupAchDate.Text = JTAchBkupDate
        txtJTvmBkupAchSerNum.Text = JTAchBkupSerNum
        '
        If JTgvMMJobSeq = "J" Then
            rbtnJTgvMMJobAlpha.Checked = True
        Else
            rbtnJTgvMMJobAmt.Checked = True
        End If
        '
        If JTgvNLCSeq = "J" Then
            rbtnJTgvNLCSeqJob.Checked = True
        Else
            rbtnJTgvNLCSeqDate.Checked = True
        End If
        '
        cboxJTgvEventLogEnabled.Checked = JTvmEventLogEnabled
        Me.Show()
    End Sub
    Sub JTvmUnloadPanel()
        ' --------------------------------------------
        ' Unload values from JTVarMaint panel
        ' --------------------------------------------
        JTvmUsePassWord = cboxJTvmUsePW.Checked
        JTvmShowDashBoard = cboxJTgvDashBoard.Checked
        '
        CompanyNme = txtJTvmCompName.Text
        CompanyMailingAddressL1 = txtJTvmCompAddL1.Text
        CompanyMailingAddressL2 = txtJTvmCompAddL2.Text
        CompanyMailingAddressL3 = txtJTVMCompAddL3.Text
        '
        AltCompanyNme = txtJTvmAltCompName.Text
        AltCompanyMailingAddressL1 = txtJTvmAltCompAddL1.Text
        AltCompanyMailingAddressL2 = txtJTvmAltCompAddL2.Text
        AltCompanyMailingAddressL3 = txtJTvmAltCompAddL3.Text
        '
        CompanyContactName = txtJTvmCompContact.Text
        CompanyContactEmail = txtJTvmCompEmail.Text
        GoogleValidation = txtJTvmGoogleValidation.Text
        CompanyContactPhone = txtJTvmCompPhone.Text
        CompanyContactFax = txtJTvmCompFax.Text
        CompanyLogo = txtJTvmCompanyLogo.Text
        AltCompanyLogo = txtJTvmAltCompanyLogo.Text
        InvNumber = txtJTvmInvNumber.Text
        AltInvNumber = txtJTvmAltInvNumber.Text
        '
        JTvmMatMargin = txtJTvmMatMargin.Text
        JTvmSubMargin = txtJTvmSubMargin.Text
        JTvmINHOUSEMargin = txtJTvmINHOUSEMargin.Text
        '
        ' Payroll Associated Variables
        '
        SendW2PREmail = cboxJTvmW2Email.Checked
        Send1099PREmail = cboxJTvm1099Email.Checked
        '
        PayrollTaxLoadPercent = txtJTvmPRTaxLoad.Text
        JTFromEmailAddress = txtJTvmPREmailFrom.Text
        JTCCEmailAddress = txtJTvmInvCCEmail.Text
        PayrollEmailAddressW2 = txtJTvmPREmailTo.Text
        EmailMsgTextW2 = rtxtJTvmW2EmailText.Text
        EmailMsgText1099 = txtJTvm1099EmailText.Text
        '
        ' Reconciliation related variables
        '
        ReconcilMat = cboxReconMat.Checked
        ReconcilSub = cboxReconSub.Checked
        Reconcil1099PR = cboxRecon1099.Checked
        ReconcilNJR = cboxReconNJR.Checked
        ReconMakeSuggestions = cboxReconSuggest.Checked
        '
        ' Web Data Extract Switches --- For JIMGmailExtrct
        '
        If rbtnJTvmImageExtOn.Checked = True Then
            WebImageExt = True
        Else
            WebImageExt = False
        End If
        If rbtnJTvmEmailExtOn.Checked = True Then
            WebEmailExt = True
        Else
            WebEmailExt = False
        End If
        '
        ' Invoice related variables
        '
        InvLabTaskSummary = rbtnInvLabTskSum.Checked
        InvLabWorkerDetail = rbtnInvWrkrDet.Checked
        InvLabTotals = rbtnInvLaborTotals.Checked
        InvSubSerDetails = rbtnInvSubSerDetails.Checked
        InvSubSerTotals = rbtnInvSubSerTotals.Checked
        InvMatDetails = rbtnInvMatDetail.Checked
        InvMatTotals = rbtnInvMatTotals.Checked
        InvMatShowCost = cboxCstPrntMat.Checked
        InvSubShowCost = cboxCstPrntSub.Checked
        '
        InvFootertext = txtInvFooter.Text
        '
        ' Scanner Option Settings
        JTgvScanNLC = cboxJTvmScanNLC.Checked
        JTgvScanReconcil = cboxJTvmScanReconcil.Checked
        JTgvScanAUP = cboxJTvmScanAUP.Checked
        ' 
        ' Purge Settings
        '
        LabMinLife = txtJTvmLabMinLife.Text
        NLCMinLife = txtJTvmNLCMinLife.Text
        MinDaysAfterInv = txtJTvmMinDaysAfterInv.Text
        '
        PurgeTiming = txtJTvmPurgeTiming.Text
        LstTKPurge = txtJTvmLstTKPurge.Text
        LstNLCPurge = txtJTvmLstNLCPurge.Text
        '
        DataPurgeJobNorm = txtJTvmDPJNorm.Text
        DataPurgeJobMstr = txtJTvmDPJMstr.Text
        DataPurgeJobSub = txtJTvmDPJSub.Text
        '
        ' Backup Related Vaiables
        '
        JTAchBkupSwitch = cboxJTvmBkupAch.Checked
        '
        JTAchBkupFreq = txtJTvmBkupAchFreq.Text
        JTAchBkupGen = txtJTvmBkupAchGen.Text
        JTAchBkupPath = txtJTvmBkupAchPath.Text
        JTAchBkupDate = txtJTvmBkupAchDate.Text
        JTAchBkupSerNum = txtJTvmBkupAchSerNum.Text
        '
        If rbtnJTgvMMJobAlpha.Checked = True Then
            JTgvMMJobSeq = "J"
        Else
            JTgvMMJobSeq = "A"
        End If
        '
        If rbtnJTgvNLCSeqJob.Checked = True Then
            JTgvNLCSeq = "J"
        Else
            JTgvNLCSeq = "D"
        End If
        '
        JTvmEventLogEnabled = cboxJTgvEventLogEnabled.Checked
    End Sub
    Sub JTvmFlush()
        Dim fileRecord As String = ""
        Dim filePath As String = JTvmFilePath & "JTGlobalSettings.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTGlobalSettings.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            '
            '
            writeFile.WriteLine("<SafeClose>" & JTvmSafeClose & "</SafeClose>")
            writeFile.WriteLine("<SystemYear>" & JTvmSystemYear & "</SystemYear>")
            writeFile.WriteLine("<FilePath>" & JTvmFilePath & "</FilePath>")
            writeFile.WriteLine("<FilePathDoc>" & JTvmFilePathDoc & "</FilePathDoc>")
            writeFile.WriteLine("<PassWord>" & JTvmPassWord & "</PassWord>")
            writeFile.WriteLine("<UsePassWord>" & JTvmUsePassWord & "</UsePassWord>")
            writeFile.WriteLine("<ShowDashBoard>" & JTvmShowDashBoard & "</ShowDashBoard>")
            writeFile.WriteLine("<PWResetEmail>" & JTvmPWResetEmail & "</PWResetEmail>")
            '
            writeFile.WriteLine("<SWRelease>" & JTvmSWRelease & "</SWRelease>")
            writeFile.WriteLine("<SignedInUser>" & JTvmSignedInUser & "</SignedInUser>")
            writeFile.WriteLine("<SignInTime>" & JTvmSignInTime & "</SignInTime>")
            '
            ' ------------------------------------------------------------------------------
            ' Temp tag to trigger renaming N:C files with invoice information 10/9/2020
            ' ------------------------------------------------------------------------------
            writeFile.WriteLine("<NLCNameConverionComplete>" & JTvmNLCNameConverionComplete & "</NLCNameConverionComplete>")

            '
            writeFile.WriteLine("<CompanyNme>" & CompanyNme & "</CompanyNme>")
            writeFile.WriteLine("<CompanyMailingAddressL1>" & CompanyMailingAddressL1 & "</CompanyMailingAddressL1>")
            writeFile.WriteLine("<CompanyMailingAddressL2>" & CompanyMailingAddressL2 & "</CompanyMailingAddressL2>")
            writeFile.WriteLine("<CompanyMailingAddressL3>" & CompanyMailingAddressL3 & "</CompanyMailingAddressL3>")
            writeFile.WriteLine("<AltCompanyNme>" & AltCompanyNme & "</AltCompanyNme>")
            writeFile.WriteLine("<AltCompanyMailingAddressL1>" & AltCompanyMailingAddressL1 & "</AltCompanyMailingAddressL1>")
            writeFile.WriteLine("<AltCompanyMailingAddressL2>" & AltCompanyMailingAddressL2 & "</AltCompanyMailingAddressL2>")
            writeFile.WriteLine("<AltCompanyMailingAddressL3>" & AltCompanyMailingAddressL3 & "</AltCompanyMailingAddressL3>")
            writeFile.WriteLine("<CompanyContactName>" & CompanyContactName & "</CompanyContactName>")
            writeFile.WriteLine("<CompanyContactEmail>" & CompanyContactEmail & "</CompanyContactEmail>")
            writeFile.WriteLine("<GoogleValidation>" & GoogleValidation & "</GoogleValidation>")
            writeFile.WriteLine("<CompanyContactPhone>" & CompanyContactPhone & "</CompanyContactPhone>")
            writeFile.WriteLine("<CompanyContactFax>" & CompanyContactFax & "</CompanyContactFax>")
            writeFile.WriteLine("<CompanyLogo>" & CompanyLogo & "</CompanyLogo>")
            writeFile.WriteLine("<AltCompanyLogo>" & AltCompanyLogo & "</AltCompanyLogo>")
            writeFile.WriteLine("<InvNumber>" & InvNumber & "</InvNumber>")
            writeFile.WriteLine("<AltInvNumber>" & AltInvNumber & "</AltInvNumber>")
            writeFile.WriteLine("<AUPFinalizeNumber>" & AUPFinalizeNumber & "</AUPFinalizeNumber>")
            writeFile.WriteLine("<MatMargin>" & JTvmMatMargin & "</MatMargin>")
            writeFile.WriteLine("<SubMargin>" & JTvmSubMargin & "</SubMargin>")
            writeFile.WriteLine("<INHOUSEMargin>" & JTvmINHOUSEMargin & "</INHOUSEMargin>")
            writeFile.WriteLine("<SendW2PREmail>" & SendW2PREmail & "</SendW2PREmail>")
            writeFile.WriteLine("<Send1099PREmail>" & Send1099PREmail & "</Send1099PREmail>")
            writeFile.WriteLine("<PayrollTaxLoadPercent>" & PayrollTaxLoadPercent & "</PayrollTaxLoadPercent>")
            writeFile.WriteLine("<FromEmailAddress>" & JTFromEmailAddress & "</FromEmailAddress>")
            writeFile.WriteLine("<CCEmailAddress>" & JTCCEmailAddress & "</CCEmailAddress>")
            writeFile.WriteLine("<PayrollEmailAddressW2>" & PayrollEmailAddressW2 & "</PayrollEmailAddressW2>")
            writeFile.WriteLine("<DemoBypass>" & JTvmDemoBypass & "</DemoBypass>")
            writeFile.WriteLine("<EventLogEnabled>" & JTvmEventLogEnabled & "</EventLogEnabled>")
            writeFile.WriteLine("<WebImageExt>" & WebImageExt & "</WebImageExt>")
            writeFile.WriteLine("<WebEmailExt>" & WebEmailExt & "</WebEmailExt>")
            ' ---------------------------------------------------------------------------
            ' Code to break records longer than 75 character into multiple short records.
            ' ---------------------------------------------------------------------------
            Dim tmpFieldData As String = EmailMsgTextW2.Replace(vbLf, "~!")
            tmpFieldData = tmpFieldData.Replace(vbCr, "~@")
            Dim tmpLength As Integer = tmpFieldData.Length
            Dim tmpFieldPos As Integer = 0
            Do While tmpLength > 75
                fileRecord = "<EmailMsgTextW2>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</EmailMsgTextW2>"
                writeFile.WriteLine(fileRecord)
                tmpLength -= 75
                tmpFieldPos += 75
            Loop
            If tmpLength > 0 Then
                fileRecord = "<EmailMsgTextW2>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</EmailMsgTextW2>"
                writeFile.WriteLine(fileRecord)
            End If
            '          writeFile.WriteLine("<EmailMsgTextW2>" & EmailMsgTextW2 & "</EmailMsgTextW2>")
            ' ---------------------------------------------------------------------------
            ' Code to break records longer than 75 character into multiple short records.
            ' ---------------------------------------------------------------------------
            tmpFieldData = EmailMsgText1099.Replace(vbLf, "~!")
            tmpFieldData = tmpFieldData.Replace(vbCr, "~@")
            tmpLength = tmpFieldData.Length
            tmpFieldPos = 0
            Do While tmpLength > 75
                fileRecord = "<EmailMsgText1099>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</EmailMsgText1099>"
                writeFile.WriteLine(fileRecord)
                tmpLength -= 75
                tmpFieldPos += 75
            Loop
            If tmpLength > 0 Then
                fileRecord = "<EmailMsgText1099>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</EmailMsgText1099>"
                writeFile.WriteLine(fileRecord)
            End If
            '           writeFile.WriteLine("<EmailMsgText1099>" & EmailMsgText1099 & "</EmailMsgText1099>")
            writeFile.WriteLine("<ReconcilMat>" & ReconcilMat & "</ReconcilMat>")
            writeFile.WriteLine("<ReconcilSub>" & ReconcilSub & "</ReconcilSub>")
            writeFile.WriteLine("<Reconcil1099PR>" & Reconcil1099PR & "</Reconcil1099PR>")
            writeFile.WriteLine("<ReconcilNJR>" & ReconcilNJR & "</ReconcilNJR>")
            writeFile.WriteLine("<ReconMakeSuggestions>" & ReconMakeSuggestions & "</ReconMakeSuggestions>")
            '
            writeFile.WriteLine("<InvLabTaskSummary>" & InvLabTaskSummary & "</InvLabTaskSummary>")
            writeFile.WriteLine("<InvLabWorkerDetail>" & InvLabWorkerDetail & "</InvLabWorkerDetail>")
            writeFile.WriteLine("<InvLabTotals>" & InvLabTotals & "</InvLabTotals>")
            writeFile.WriteLine("<InvMatDetails>" & InvMatDetails & "</InvMatDetails>")
            writeFile.WriteLine("<InvMatTotals>" & InvMatTotals & "</InvMatTotals>")
            writeFile.WriteLine("<InvSubSerDetails>" & InvSubSerDetails & "</InvSubSerDetails>")
            writeFile.WriteLine("<InvSubSerTotals>" & InvSubSerTotals & "</InvSubSerTotals>")
            '
            writeFile.WriteLine("<InvMatShowCost>" & InvMatShowCost & "</InvMatShowCost>")
            writeFile.WriteLine("<InvSubShowCost>" & InvSubShowCost & "</InvSubShowCost>")
            ' ---------------------------------------------------------------------------
            ' Code to break records longer than 75 character into multiple short records.
            ' ---------------------------------------------------------------------------
            tmpFieldData = InvFootertext.Replace(vbLf, "~!")
            tmpFieldData = tmpFieldData.Replace(vbCr, "~@")
            tmpLength = tmpFieldData.Length
            tmpFieldPos = 0
            Do While tmpLength > 75
                fileRecord = "<InvFootertext>" & tmpFieldData.Substring(tmpFieldPos, 75) & "</InvFootertext>"
                writeFile.WriteLine(fileRecord)
                tmpLength -= 75
                tmpFieldPos += 75
            Loop
            If tmpLength > 0 Then
                fileRecord = "<InvFootertext>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</InvFootertext>"
                writeFile.WriteLine(fileRecord)
            End If
            '       writeFile.WriteLine("<InvFootertext>" & InvFootertext & "</InvFootertext>")
            writeFile.WriteLine("<LabMinLife>" & LabMinLife & "</LabMinLife>")
            writeFile.WriteLine("<NLCMinLife>" & NLCMinLife & "</NLCMinLife>")
            writeFile.WriteLine("<MinDaysAfterInv>" & MinDaysAfterInv & "</MinDaysAfterInv>")
            '
            writeFile.WriteLine("<PurgeTiming>" & PurgeTiming & "</PurgeTiming>")
            writeFile.WriteLine("<LstTKPurge>" & LstTKPurge & "</LstTKPurge>")
            writeFile.WriteLine("<LstNLCPurge>" & LstNLCPurge & "</LstNLCPurge>")
            '
            writeFile.WriteLine("<DataPurgeJobNorm>" & DataPurgeJobNorm & "</DataPurgeJobNorm>")
            writeFile.WriteLine("<DataPurgeJobMstr>" & DataPurgeJobMstr & "</DataPurgeJobMstr>")
            writeFile.WriteLine("<DataPurgeJobSub>" & DataPurgeJobSub & "</DataPurgeJobSub>")
            writeFile.WriteLine("<AchBkupSwitch>" & JTAchBkupSwitch & "</AchBkupSwitch>")
            writeFile.WriteLine("<AchBkupFreq>" & JTAchBkupFreq & "</AchBkupFreq>")
            writeFile.WriteLine("<AchBkupGen>" & JTAchBkupGen & "</AchBkupGen>")
            writeFile.WriteLine("<AchBkupPath>" & JTAchBkupPath & "</AchBkupPath>")
            writeFile.WriteLine("<AchBkupDate>" & JTAchBkupDate & "</AchBkupDate>")
            writeFile.WriteLine("<AchBkupSerNum>" & JTAchBkupSerNum & "</AchBkupSerNum>")
            If JTGVrecName <> "" Then
                writeFile.WriteLine("<GVrecName>" & JTGVrecName & "</GVrecName>")
            End If
            If JTGVrecStmtAmt <> "" Then
                writeFile.WriteLine("<GVrecStmtAmt>" & JTGVrecStmtAmt & "</GVrecSTMTAmt>")
            End If
            If JTGVrecOptMsg <> "" Then
                writeFile.WriteLine("<GVrecOptMsg>" & JTGVrecOptMsg & "</GVrecOptMsg>")
            End If
            If JTGVrecVndShrtName <> "" Then
                writeFile.WriteLine("<GVrecVndShrtName>" & JTGVrecVndShrtName & "</GVrecVndShrtName>")
            End If
            If JTGVrecVndInvDate <> "" Then
                writeFile.WriteLine("<GVrecVndInvDate>" & JTGVrecVndInvDate & "</GVrecVndInvDate>")
            End If
            If JTGVrecVndInvNumber <> "" Then
                writeFile.WriteLine("<GVrecVndInvNumber>" & JTGVrecVndInvNumber & "</GVrecVndInvNumber>")
            End If
            If JTGVrecVndInvDueDate <> "" Then
                writeFile.WriteLine("<GVrecVndInvDueDate>" & JTGVrecVndInvDueDate & "</GVrecVndInvDueDate>")
            End If
            '
            '
            writeFile.WriteLine("<MMJobSeq>" & JTgvMMJobSeq & "</MMJobSeq>")
            writeFile.WriteLine("<NLCSeq>" & JTgvNLCSeq & "</NLCSeq>")
            '
            ' write last used Main Menu display settings
            '
            writeFile.WriteLine("<ShowCstP>" & JTGVShowCstP & "</ShowCstP>")
            writeFile.WriteLine("<ShowAUP>" & JTGVShowAUP & "</ShowAUP>")
            writeFile.WriteLine("<ShowDeactJobs>" & JTGVShowDeactJobs & "</ShowDeactJobs>")
            '
            ' Scanner option settings
            writeFile.WriteLine("<ScanNLC>" & JTgvScanNLC & "</ScanNLC>")
            writeFile.WriteLine("<ScanReconcil>" & JTgvScanReconcil & "</ScanReconcil>")
            writeFile.WriteLine("<ScanAUP>" & JTgvScanAUP & "</ScanAUP>")
            ' 
            ' write sales tax settingsw from slJTvmTax to file.
            '
            If slJTvmTax.Count > 0 Then  'check to see if there are tax parameter records.
                Dim key As ICollection = slJTvmTax.Keys
                Dim k As String = ""
                '
                For Each k In key
                    writeFile.WriteLine("<STState>" & slJTvmTax(k).STSState & "</StState>")
                    writeFile.WriteLine("<STStHasTax>" & slJTvmTax(k).STSStHasTax & "</StStHasTax>")
                    If slJTvmTax(k).STSStRate <> "" Then
                        writeFile.WriteLine("<STStRate>" & slJTvmTax(k).STSStRate & "</StStRate>")
                    End If
                    writeFile.WriteLine("<STLabHasTax>" & slJTvmTax(k).STSLabHasTax & "</StLabHasTax>")
                    If slJTvmTax(k).STSLabRate <> "" Then
                        writeFile.WriteLine("<STLabRate>" & slJTvmTax(k).STSLabRate & "</StLabRate>")
                    End If
                    writeFile.WriteLine("<STSubHasTax>" & slJTvmTax(k).STSSubHasTax & "</StSubHasTax>")
                    If slJTvmTax(k).STSSubRate <> "" Then
                        writeFile.WriteLine("<STSubRate>" & slJTvmTax(k).STSSubRate & "</StSubRate>")
                    End If
                    writeFile.WriteLine("<STMatHasTax>" & slJTvmTax(k).STSMatHasTax & "</StMatHasTax>")
                    If slJTvmTax(k).STSMatRate <> "" Then
                        writeFile.WriteLine("<STMatRate>" & slJTvmTax(k).STSMatRate & "</StMatRate>")
                    End If
                    writeFile.WriteLine("<STEnd>" & slJTvmTax(k).STSState & "</StEnd>")
                Next
            End If
            '
            If slJTvmNJREA.Count > 0 Then  'check to see if there are tax parameter records.
                Dim key As ICollection = slJTvmNJREA.Keys
                Dim k As String = ""
                '
                For Each k In key
                    writeFile.WriteLine("<NJREAAccount>" & slJTvmNJREA(k).JTvmNJREAName & "</NJREAAccount>")
                    writeFile.WriteLine("<NJREADescription>" & slJTvmNJREA(k).JTvmNJREADescription & "</NJREADescription>")
                Next
            End If
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try

        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTvmFilePath & "JTGlobalSettings.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTvmFilePath & "JTGlobalSettings.Bkup")
        End If
        ' ------------------------------------------------------------------------

    End Sub
    '
    Sub JTvmVarLoad()
        Dim fileRecord As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpChar As String = ""
        Dim tmpLocKey As Integer = 0
        '
        ' Initialize Sax Tax SL
        '
        slJTvmTax.Clear()
        slJTvmNJREA.Clear()
        Dim TmpSTItem As SalesTaxStruct = Nothing
        '
        Dim tmpInvFootTextFirst As Boolean = True
        Dim tmpPRW2TextFirst As Boolean = True
        Dim tmpPR1099TextFirst As Boolean = True
        '
        Dim ReadFileKey As String = ""
        Dim ReadFileData As String = ""
        '
        JTvmFilePath = Environ("OneDrive") & "/JobInfoMngr/"
        JTvmFilePathDoc = Environ("OneDrive") & "/JobInfoMngrDocs/"
        Dim filePath As String = JTvmFilePath & "JTGlobalSettings.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTvmFilePath & "JTGlobalSettings.Bkup") Then
            MsgBox("JTGlobalSettings --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (VM.007")
            If File.Exists(JTvmFilePath & "JTGlobalSettings.dat") Then
                My.Computer.FileSystem.DeleteFile(JTvmFilePath & "JTGlobalSettings.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTvmFilePath & "JTGlobalSettings.Bkup", "JTGlobalSettings.dat")
        End If
        ' ------------------------------------------------------------------

        '
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    ReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If ReadFileKey = "GlobSettingsRecord" Or ReadFileKey = "/GlobSettingsRecord" Then
                        ReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '          tmpLoc = fileRecord.Length - 1
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        ReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case ReadFileKey
                        '
                        Case "SafeClose"
                            JTvmSafeClose = ReadFileData
                        Case "FilePath"
                            '
                            ' no need to move this data --- new value already in field --- JTvmFilePath = ReadFileData
                            ' this is true for JTvmFilePathDoc also.
                            '
                        Case "PassWord"
                            JTvmPassWord = ReadFileData
                        Case "UsePassWord"
                            JTvmUsePassWord = ReadFileData
                        Case "ShowDashBoard"
                            JTvmShowDashBoard = ReadFileData
                            '
                        Case "PWResetEmail"
                            JTvmPWResetEmail = ReadFileData
                            '
                        Case "SWRelease"
                            JTvmSWRelease = ReadFileData
                        Case "SignedInUser"
                            JTvmSignedInUser = ReadFileData
                        Case "SignInTime"
                            JTvmSignInTime = ReadFileData
                            ' ------------------------------------------------------------------------------
                            ' Single use tag for converting NLC PDf names to include invoice info. 10/9/2020
                            ' ------------------------------------------------------------------------------
                        Case "NLCNameConverionComplete"
                            JTvmNLCNameConverionComplete = ReadFileData
                            '
                        Case "DemoBypass"
                            JTvmDemoBypass = ReadFileData
                        Case "EventLogEnabled"
                            JTvmEventLogEnabled = ReadFileData
                        Case "CompanyNme"
                            CompanyNme = ReadFileData
                        Case "CompanyMailingAddressL1"
                            CompanyMailingAddressL1 = ReadFileData
                        Case "CompanyMailingAddressL2"
                            CompanyMailingAddressL2 = ReadFileData
                        Case "CompanyMailingAddressL3"
                            CompanyMailingAddressL3 = ReadFileData
                        Case "AltCompanyNme"
                            AltCompanyNme = ReadFileData
                        Case "AltCompanyMailingAddressL1"
                            AltCompanyMailingAddressL1 = ReadFileData
                        Case "AltCompanyMailingAddressL2"
                            AltCompanyMailingAddressL2 = ReadFileData
                        Case "AltCompanyMailingAddressL3"
                            AltCompanyMailingAddressL3 = ReadFileData
                        Case "CompanyContactName"
                            CompanyContactName = ReadFileData
                        Case "CompanyContactEmail"
                            CompanyContactEmail = ReadFileData
                        Case "GoogleValidation"
                            GoogleValidation = ReadFileData
                        Case "CompanyContactPhone"
                            CompanyContactPhone = ReadFileData
                        Case "CompanyContactFax"
                            CompanyContactFax = ReadFileData
                        Case "CompanyLogo"
                            CompanyLogo = ReadFileData
                        Case "AltCompanyLogo"
                            AltCompanyLogo = ReadFileData
                        Case "AltCompanyLogo"
                            AltCompanyLogo = ReadFileData
                        Case "InvNumber"
                            InvNumber = ReadFileData
                        Case "AltInvNumber"
                            AltInvNumber = ReadFileData
                        Case "AUPFinalizeNumber"
                            AUPFinalizeNumber = ReadFileData
                        Case "MatMargin"
                            JTvmMatMargin = ReadFileData
                        Case "SubMargin"
                            JTvmSubMargin = ReadFileData
                        Case "INHOUSEMargin"
                            JTvmINHOUSEMargin = ReadFileData
                        Case "SendW2PREmail"
                            SendW2PREmail = ReadFileData
                        Case "Send1099PREmail"
                            Send1099PREmail = ReadFileData
                        Case "PayrollTaxLoadPercent"
                            PayrollTaxLoadPercent = ReadFileData
                        Case "FromEmailAddress"
                            JTFromEmailAddress = ReadFileData
                        Case "CCEmailAddress"
                            JTCCEmailAddress = ReadFileData
                        Case "PayrollEmailAddressW2"
                            PayrollEmailAddressW2 = ReadFileData
                        Case "EmailMsgTextW2"
                            If tmpPRW2TextFirst = True Then
                                EmailMsgTextW2 = ""
                                tmpPRW2TextFirst = False
                            End If
                            EmailMsgTextW2 &= ReadFileData
                        Case "EmailMsgText1099"
                            If tmpPR1099TextFirst = True Then
                                EmailMsgText1099 = ""
                                tmpPR1099TextFirst = False
                            End If
                            EmailMsgText1099 &= ReadFileData
                        Case "ReconcilMat"
                            ReconcilMat = ReadFileData
                        Case "ReconcilSub"
                            ReconcilSub = ReadFileData
                        Case "Reconcil1099PR"
                            Reconcil1099PR = ReadFileData
                        Case "ReconcilNJR"
                            ReconcilNJR = ReadFileData
                        Case "ReconMakeSuggestions"
                            ReconMakeSuggestions = ReadFileData
                            '
                        Case "InvLabTaskSummary"
                            InvLabTaskSummary = ReadFileData
                        Case "InvLabWorkerDetail"
                            InvLabWorkerDetail = ReadFileData
                        Case "InvLabTotals"
                            InvLabTotals = ReadFileData
                        Case "InvMatDetails"
                            InvMatDetails = ReadFileData
                        Case "InvMatTotals"
                            InvMatTotals = ReadFileData
                        Case "InvSubserDetails"
                            InvSubSerDetails = ReadFileData
                        Case "InvSubSerTotals"
                            InvSubSerTotals = ReadFileData
                        '
                        Case "InvMatShowCost"
                            InvMatShowCost = ReadFileData
                        Case "InvSubShowCost"
                            InvSubShowCost = ReadFileData
                        Case "InvFootertext"
                            If tmpInvFootTextFirst = True Then
                                InvFootertext = ""
                                tmpInvFootTextFirst = False
                            End If
                            InvFootertext &= ReadFileData
                        Case "WebImageExt"
                            WebImageExt = ReadFileData
                        Case "WebEmailExt"
                            WebEmailExt = ReadFileData
                        Case "LabMinLife"
                            LabMinLife = ReadFileData
                        Case "NLCMinLife"
                            NLCMinLife = ReadFileData
                        Case "MinDaysAfterInv"
                            MinDaysAfterInv = ReadFileData
                        Case "PurgeTiming"
                            PurgeTiming = ReadFileData
                        Case "LstTKPurge"
                            LstTKPurge = ReadFileData
                        Case "LstNLCPurge"
                            LstNLCPurge = ReadFileData
                        Case "DataPurgeJobNorm "
                            DataPurgeJobNorm = ReadFileData
                        Case "DataPurgeJobMstr"
                            DataPurgeJobMstr = ReadFileData
                        Case "DataPurgeJobSub"
                            DataPurgeJobSub = ReadFileData
                        Case "AchBkupSwitch"
                            JTAchBkupSwitch = ReadFileData
                        Case "AchBkupFreq"
                            JTAchBkupFreq = ReadFileData
                        Case "AchBkupGen"
                            JTAchBkupGen = ReadFileData
                        ' The following to line were commented out .... I don't know why.
                        ' The field to change the variable on the utility was also mark 'read only' ????
                        ' Putting them back in service 12/17/2021
                        Case "AchBkupPath"
                            JTAchBkupPath = ReadFileData
                        Case "AchBkupDate"
                            JTAchBkupDate = ReadFileData
                        Case "AchBkupSerNum"
                            JTAchBkupSerNum = ReadFileData
                        Case "GVrecName"
                            JTGVrecName = ReadFileData
                        Case "GVrecStmtAmt"
                            JTGVrecStmtAmt = ReadFileData
                        Case "GVrecOptMsg"
                            JTGVrecOptMsg = ReadFileData
                        Case "GVrecVndShrtName"
                            JTGVrecVndShrtName = ReadFileData
                        Case "GVrecVndInvDate"
                            JTGVrecVndInvDate = ReadFileData
                        Case "GVrecVndInvNumber"
                            JTGVrecVndInvNumber = ReadFileData
                        Case "GVrecVndInvDueDate"
                            JTGVrecVndInvDueDate = ReadFileData
                        Case "MMJobSeq"
                            JTgvMMJobSeq = ReadFileData
                        Case "NLCSeq"
                            JTgvNLCSeq = ReadFileData
                            '
                        Case "ScanNLC"
                            JTgvScanNLC = ReadFileData
                        Case "ScanReconcil"
                            JTgvScanReconcil = ReadFileData
                        Case "ScanAUP"
                            JTgvScanAUP = ReadFileData
                            '
                        Case "STState"
                            TmpSTItem.STSState = ReadFileData
                            TmpSTItem.STSStRate = ""
                            TmpSTItem.STSLabRate = ""
                            TmpSTItem.STSSubRate = ""
                            TmpSTItem.STSMatRate = ""
                        Case "STStHasTax"
                            TmpSTItem.STSStHasTax = ReadFileData
                        Case "STStRate"
                            TmpSTItem.STSStRate = ReadFileData
                        Case "STLabHasTax"
                            TmpSTItem.STSLabHasTax = ReadFileData
                        Case "STLabRate"
                            TmpSTItem.STSLabRate = ReadFileData
                        Case "STSubHasTax"
                            TmpSTItem.STSSubHasTax = ReadFileData
                        Case "STSubRate"
                            TmpSTItem.STSSubRate = ReadFileData
                        Case "STMatHasTax"
                            TmpSTItem.STSMatHasTax = ReadFileData
                        Case "STMatRate"
                            TmpSTItem.STSMatRate = ReadFileData
                        Case "STEnd"
                            slJTvmTax.Add(TmpSTItem.STSState, TmpSTItem)
                            '
                        Case "NJREAAccount"
                            tmpNJREAItem.JTvmNJREAName = ReadFileData
                        Case "NJREADescription"
                            TmpNJREAItem.JTvmNJREADescription = ReadFileData
                            slJTvmNJREA.Add(tmpNJREAItem.JTvmNJREAName, tmpNJREAItem)
'     
                        Case "ShowCstP"
                            JTGVShowCstP = ReadFileData
                        Case "ShowAUP"
                            JTGVShowAUP = ReadFileData
                        Case "ShowDeactJobs"
                            JTGVShowDeactJobs = ReadFileData
                        Case "SystemYear"
                            JTvmSystemYear = ReadFileData
                            '
                    End Select
                    '
                End If
            End While
            '
            ' -----------------------------------------------------------------------------------
            ' The following two lines of code are being commented out. Don't know why they were
            ' put in to begin with. Assuming no problems, code can be deleted after a short time.
            ' 12/16/2021.
            ' -----------------------------------------------------------------------------------
            '     JTAchBkupPath = "C:\Users\johnr\OneDrive - JRV Woodworks LLC" & "\JIMBkups"
            '     JTAchBkupPath = JTAchBkupPath.Replace("Documents", "OneDrive")
            '
            InvFootertext = InvFootertext.Replace("~!", vbLf)
            InvFootertext = InvFootertext.Replace("~@", vbCr)
            EmailMsgTextW2 = EmailMsgTextW2.Replace("~!", vbLf)
            EmailMsgTextW2 = EmailMsgTextW2.Replace("~@", vbCr)
            EmailMsgText1099 = EmailMsgText1099.Replace("~!", vbLf)
            EmailMsgText1099 = EmailMsgText1099.Replace("~@", vbCr)
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub txtJTvmCompEmail_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmCompEmail.LostFocus
        If txtJTvmCompEmail.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTvmCompEmail.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(VM.003)", txtJTvmCompEmail, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtJTvmPREmailTo_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmPREmailTo.LostFocus
        If txtJTvmPREmailTo.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTvmPREmailTo.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(VM.003)", txtJTvmPREmailTo, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtJTvmPREmailCC_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmPREmailCC.LostFocus
        If txtJTvmPREmailCC.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTvmPREmailCC.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(VM.003)", txtJTvmPREmailCC, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtJTvmPREmailFrom_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmPREmailFrom.LostFocus
        If txtJTvmPREmailFrom.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTvmPREmailFrom.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(VM.003)", txtJTvmPREmailFrom, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub
    Private Sub txtJTvmInvCCEmail_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmInvCCEmail.LostFocus
        If txtJTvmInvCCEmail.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTvmInvCCEmail.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(VM.003)", txtJTvmInvCCEmail, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub
    Public Function JTvmSTChkForRec(tmpState As String) As String
        If slJTvmTax.ContainsKey(tmpState) = False Then
            Return "NORECORD"
        End If
        Return "HASRECORD"
    End Function
    Public Function JTvmSTAddSkelRecord(tmpState As String) As String '
        ' -------------------------------------------------------------------
        ' Insert tax record for state --- formatted for no tax
        ' -------------------------------------------------------------------
        Dim tmpNewITem As New SalesTaxStruct
        tmpNewITem.STSState = tmpState
        tmpNewITem.STSStHasTax = False
        tmpNewITem.STSStRate = ""
        tmpNewITem.STSLabHasTax = False
        tmpNewITem.STSLabRate = ""
        tmpNewITem.STSSubHasTax = False
        tmpNewITem.STSSubRate = ""
        tmpNewITem.STSMatHasTax = False
        tmpNewITem.STSMatRate = ""
        '
        If slJTvmTax.ContainsKey(tmpState) = False Then
            slJTvmTax.Add(tmpState, tmpNewITem)
            Return "ADDED"
        End If
        Return "DUPLICATE"
    End Function
    Sub JTvmSTSslTolv()
        Dim itmBuild(9) As String
        Dim lvLine As ListViewItem
        lvJtvmTax.Items.Clear()
        '
        Dim key As ICollection = slJTvmTax.Keys
        Dim k As String = ""
        '
        For Each k In key
            itmBuild(0) = slJTvmTax(k).STSState
            If slJTvmTax(k).STSStHasTax = True Then
                itmBuild(1) = "Y"
            Else
                itmBuild(1) = ""
            End If
            itmBuild(2) = slJTvmTax(k).STSStRate
            If slJTvmTax(k).STSLabHasTax = True Then
                itmBuild(3) = "Y"
            Else
                itmBuild(3) = ""
            End If
            itmBuild(4) = slJTvmTax(k).STSLabRate
            If slJTvmTax(k).STSSubHasTax = True Then
                itmBuild(5) = "Y"
            Else
                itmBuild(5) = ""
            End If
            itmBuild(6) = slJTvmTax(k).STSSubRate
            If slJTvmTax(k).STSMatHasTax = True Then
                itmBuild(7) = "Y"
            Else
                itmBuild(7) = ""
            End If
            itmBuild(8) = slJTvmTax(k).STSMatRate
            '
            lvLine = New ListViewItem(itmBuild)
            lvJtvmTax.Items.Add(lvLine)
        Next
        '
        '
    End Sub

    Private Sub lvJtvmTax_DoubleClick(sender As Object, e As EventArgs) Handles lvJtvmTax.DoubleClick
        Dim tmpState As String = lvJtvmTax.SelectedItems(0).Text
        txtJTvmSTState.Text = tmpState
        txtJTvmSTState.ReadOnly = True
        CboxJTvmSTStHasTax.Checked = slJTvmTax(tmpState).STSStHasTax
        txtJTvmSTStRate.Text = slJTvmTax(tmpState).STSStRate
        cboxJTvmSTLabHasTax.Checked = slJTvmTax(tmpState).STSLabHasTax
        txtJTvmStLabRate.Text = slJTvmTax(tmpState).STSLabRate
        cboxJTvmSTSubHasTax.Checked = slJTvmTax(tmpState).STSSubHasTax
        txtJTvmStSubRate.Text = slJTvmTax(tmpState).STSSubRate
        cboxJTvmSTMatHasTax.Checked = slJTvmTax(tmpState).STSMatHasTax
        txtJTvmSTMatRate.Text = slJTvmTax(tmpState).STSMatRate
        '
        slJTvmTax.Remove(tmpState)
    End Sub

    Private Sub btnJTvmSTUpdtAdd_Click(sender As Object, e As EventArgs) Handles btnJTvmSTUpdtAdd.Click
        ' --------------------------------------------------------------------
        ' Update or add state sales tax rate record.
        ' --------------------------------------------------------------------
        Dim tmpSTNewItem As SalesTaxStruct
        tmpSTNewItem.STSState = txtJTvmSTState.Text
        tmpSTNewItem.STSStHasTax = CboxJTvmSTStHasTax.Checked
        tmpSTNewItem.STSStRate = txtJTvmSTStRate.Text
        tmpSTNewItem.STSLabHasTax = cboxJTvmSTLabHasTax.Checked
        tmpSTNewItem.STSLabRate = txtJTvmStLabRate.Text
        tmpSTNewItem.STSSubHasTax = cboxJTvmSTSubHasTax.Checked
        tmpSTNewItem.STSSubRate = txtJTvmStSubRate.Text
        tmpSTNewItem.STSMatHasTax = cboxJTvmSTMatHasTax.Checked
        tmpSTNewItem.STSMatRate = txtJTvmSTMatRate.Text
        '
        slJTvmTax.Add(tmpSTNewItem.STSState, tmpSTNewItem)
        JTvmSTClrPanel()
        JTvmSTSslTolv()
    End Sub
    Sub JTvmSTClrPanel()
        txtJTvmSTState.ReadOnly = False
        txtJTvmSTState.Text = ""
        CboxJTvmSTStHasTax.Checked = False
        txtJTvmSTStRate.Text = ""
        cboxJTvmSTLabHasTax.Checked = False
        txtJTvmStLabRate.Text = ""
        cboxJTvmSTSubHasTax.Checked = False
        txtJTvmStSubRate.Text = ""
        cboxJTvmSTMatHasTax.Checked = False
        txtJTvmSTMatRate.Text = ""
    End Sub

    Private Sub btnJTvmSTCancel_Click(sender As Object, e As EventArgs) Handles btnJTvmSTCancel.Click
        JTvmSTClrPanel()
        JTvmSTSslTolv()
    End Sub

    Private Sub txtJTvmSTStRate_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmSTStRate.LostFocus
        If CboxJTvmSTStHasTax.Checked = True And txtJTvmSTStRate.Text = "" Then
            ToolTip1.Show("Rate entry required if taxable category is checked(VM.004)", txtJTvmSTStRate, -30, -50, 7000)
            txtJTvmSTStRate.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTvmSTStRate.Text) = False Then
            ToolTip1.Show("Entry is not numeric. Correct and resubmit.(VM.005)", txtJTvmSTStRate, -30, -50, 7000)
            txtJTvmSTStRate.Select()
            Exit Sub
        End If
        txtJTvmSTStRate.Text = String.Format("{0:0.000}", Val(txtJTvmSTStRate.Text))
    End Sub

    Private Sub txtJTvmStLabRate_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmStLabRate.LostFocus
        If cboxJTvmSTLabHasTax.Checked = True And txtJTvmStLabRate.Text = "" Then
            ToolTip1.Show("Rate entry required if taxable category is checked(VM.004)", txtJTvmStLabRate, -30, -50, 7000)
            txtJTvmStLabRate.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTvmStLabRate.Text) = False Then
            ToolTip1.Show("Entry is not numeric. Correct and resubmit.(VM.005)", txtJTvmStLabRate, -30, -50, 7000)
            txtJTvmStLabRate.Select()
            Exit Sub
        End If
        txtJTvmStLabRate.Text = String.Format("{0:0.000}", Val(txtJTvmStLabRate.Text))
    End Sub

    Private Sub txtJTvmStSubRate_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmStSubRate.LostFocus
        If cboxJTvmSTSubHasTax.Checked = True And txtJTvmStSubRate.Text = "" Then
            ToolTip1.Show("Rate entry required if taxable category is checked(VM.004)", txtJTvmStSubRate, -30, -50, 7000)
            txtJTvmStSubRate.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTvmStSubRate.Text) = False Then
            ToolTip1.Show("Entry is not numeric. Correct and resubmit.(VM.005)", txtJTvmStSubRate, -30, -50, 7000)
            txtJTvmStSubRate.Select()
            Exit Sub
        End If
        txtJTvmStSubRate.Text = String.Format("{0:0.000}", Val(txtJTvmStSubRate.Text))
    End Sub

    Private Sub txtJTvmSTMatRate_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmSTMatRate.LostFocus
        If cboxJTvmSTMatHasTax.Checked = True And txtJTvmSTMatRate.Text = "" Then
            ToolTip1.Show("Rate entry required if taxable category is checked(VM.004)", txtJTvmSTMatRate, -30, -50, 7000)
            txtJTvmSTMatRate.Select()
            Exit Sub
        End If
        If IsNumeric(txtJTvmSTMatRate.Text) = False Then
            ToolTip1.Show("Entry is not numeric. Correct and resubmit.(VM.005)", txtJTvmSTMatRate, -30, -50, 7000)
            txtJTvmSTMatRate.Select()
            Exit Sub
        End If
        txtJTvmSTMatRate.Text = String.Format("{0:0.000}", Val(txtJTvmSTMatRate.Text))
    End Sub

    Private Sub CboxJTvmSTStHasTax_CheckedChanged(sender As Object, e As EventArgs) Handles CboxJTvmSTStHasTax.CheckedChanged
        If CboxJTvmSTStHasTax.Checked = False Then
            txtJTvmSTStRate.Text = ""
            txtJTvmStLabRate.Text = ""
            txtJTvmStSubRate.Text = ""
            txtJTvmSTMatRate.Text = ""
            '
            cboxJTvmSTLabHasTax.Checked = False
            cboxJTvmSTSubHasTax.Checked = False
            cboxJTvmSTMatHasTax.Checked = False
        End If
    End Sub

    Private Sub cboxJTvmSTLabHasTax_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmSTLabHasTax.CheckedChanged
        If cboxJTvmSTLabHasTax.Checked = True Then
            txtJTvmStLabRate.Text = txtJTvmSTStRate.Text
        Else
            txtJTvmStLabRate.Text = ""
        End If
    End Sub

    Private Sub cboxJTvmSTSubHasTax_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmSTSubHasTax.CheckedChanged
        If cboxJTvmSTSubHasTax.Checked = True Then
            txtJTvmStSubRate.Text = txtJTvmSTStRate.Text
        Else
            txtJTvmStSubRate.Text = ""
        End If
    End Sub

    Private Sub cboxJTvmSTMatHasTax_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmSTMatHasTax.CheckedChanged
        If cboxJTvmSTMatHasTax.Checked = True Then
            txtJTvmSTMatRate.Text = txtJTvmSTStRate.Text
        Else
            txtJTvmSTMatRate.Text = ""
        End If
    End Sub

    ''' <summary>Validates that the state abbreviation entered in sales tax entry and maintenance is valid. Sub is called when state abbreviation field (txtJTvmSTState) loses focus.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTvmSTState_LostFocus(sender As Object, e As EventArgs) Handles txtJTvmSTState.LostFocus
        If txtJTvmSTState.Text = "" Then
            Exit Sub
        End If
        txtJTvmSTState.Text = txtJTvmSTState.Text.ToUpper
        If convertState(txtJTvmSTState.Text) = "NOTFOUND" Then
            ToolTip1.Show("Invalid state abbreviation. Correct and resubmit.(VM.006)", txtJTvmSTState, -30, -50, 7000)
            txtJTvmSTState.Select()
        End If
    End Sub

    ''' <summary>Sub called when cbox checked to delete an account from the non-job-related 
    '''          account list (slJTvmNJREA). If it is checked and an account is selected on 
    '''          the lv, it is deleted.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTvmNJRAccountChgDel_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmNJRAccountChgDel.CheckedChanged
        If cboxJTvmNJRAccountChgDel.Checked = True Then
            cboxJTvmNJRAccountChgDel.Checked = False
            If lvJTvmNJRAccounts.SelectedItems.Count = 0 Then    'nothing selected for delete
                Exit Sub
            End If
            txtJTvmNJRAMAcct.Text = lvJTvmNJRAccounts.SelectedItems(0).Text
            txtJTvmNJRAMDesc.Text = lvJTvmNJRAccounts.SelectedItems(0).SubItems(1).Text
            btnJTgvNJRAMAddChg.Text = "Change"
            btnJTgvNJRAMDelete.Visible = True
            btnJTgvNJRAMAddChg.Select()
            gboxJTvmNJRAMaint.Visible = True
        End If
    End Sub
    Private Sub BtnJTgvNJRAMAddChg_Click(sender As Object, e As EventArgs) Handles btnJTgvNJRAMAddChg.Click
        txtJTvmNJRAMAcct.Text = txtJTvmNJRAMAcct.Text.ToUpper
        If txtJTvmNJRAMAcct.Text = "" Then
            ToolTip1.Show("No NJR Acct ID entered. Correct and resubmit.(VM.007)", txtJTvmNJRAMAcct, -30, -50, 7000)
            Exit Sub
        End If
        If btnJTgvNJRAMAddChg.Text = "Change" Then
            If slJTvmNJREA.ContainsKey(lvJTvmNJRAccounts.SelectedItems(0).Text) Then
                slJTvmNJREA.Remove(lvJTvmNJRAccounts.SelectedItems(0).Text)
            End If
        End If
        Dim tmpSLItem As NJRExpAcctStruct
        tmpSLItem.JTvmNJREAName = txtJTvmNJRAMAcct.Text
        tmpSLItem.JTvmNJREADescription = txtJTvmNJRAMDesc.Text
        slJTvmNJREA.Add(tmpSLItem.JTvmNJREAName, tmpSLItem)
        '
        gboxJTvmNJRAMaint.Visible = False
        JTvmLoadlvJTvmNJRAccounts()
    End Sub


    Private Sub BtnJTgvNJRAMDelete_Click(sender As Object, e As EventArgs) Handles btnJTgvNJRAMDelete.Click
        If slJTvmNJREA.ContainsKey(lvJTvmNJRAccounts.SelectedItems(0).Text) Then
            slJTvmNJREA.Remove(lvJTvmNJRAccounts.SelectedItems(0).Text)
            JTvmLoadlvJTvmNJRAccounts()
        End If
        gboxJTvmNJRAMaint.Visible = False
    End Sub
    Private Sub BtnJTgvNJRAMCancel_Click(sender As Object, e As EventArgs) Handles btnJTgvNJRAMCancel.Click
        gboxJTvmNJRAMaint.Visible = False
    End Sub
    Private Sub CboxJTvmNJRAccountAdd_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmNJRAccountAdd.CheckedChanged

        If cboxJTvmNJRAccountAdd.Checked = True Then
            cboxJTvmNJRAccountAdd.Checked = False
            gboxJTvmNJRAMaint.Visible = True
            btnJTgvNJRAMAddChg.Text = "Add"
            btnJTgvNJRAMDelete.Visible = False
            txtJTvmNJRAMAcct.Text = ""
            txtJTvmNJRAMDesc.Text = ""
        End If
    End Sub
    Private Sub CboxJTvmNJRAccountPrint_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvmNJRAccountPrint.CheckedChanged
        If cboxJTvmNJRAccountPrint.Checked = True Then
            cboxJTvmNJRAccountPrint.Checked = False
            JTmrInit("NJRA")
        End If
    End Sub

    ''' <summary>Function to load lv for non-job-related expense on JTvm panel. Function 
    '''          is call on initial screen load and also to reload it if accounts are deleted.</summary>
    Private Function JTvmLoadlvJTvmNJRAccounts() As String
        lvJTvmNJRAccounts.Items.Clear()
        '
        Dim itmBuild(2) As String
        Dim lvLine As ListViewItem
        '
        Dim key As ICollection = slJTvmNJREA.Keys
        Dim k As String = ""
        For Each k In key
            itmBuild(0) = slJTvmNJREA(k).JTvmNJREAName
            itmBuild(1) = slJTvmNJREA(k).JTvmNJREADescription
            lvLine = New ListViewItem(itmBuild)
            lvJTvmNJRAccounts.Items.Add(lvLine)
        Next
        Return ""
    End Function
    ''' <summary>Function called to check for existence and/or to retrieve description and 
    '''          update entire account. Function called from Reconciliation process.</summary>
    ''' <param name="tmpAction">
    ''' "Validate" queues a check to see if account name exists.If it exists, the description 
    ''' is returned. " Update" will create a new account/description entry or will update description 
    ''' on an existing entry.
    ''' </param>
    ''' <param name="tmpNJREAAccount">Account key ... always sent with the call.</param>
    ''' <param name="tmpNJREADescription">Description is returned on "Validate" calls if account exists. 
    '''                                   Description is sent from the calling sub when the account is 
    '''                                   being added or updated.</param>
    Public Function JTvmValidateUpdateNJREA(tmpAction As String,
                                            ByRef tmpNJREAAccount As String,
                                            ByRef tmpNJREADescription As String) As String
        If tmpAction = "Validate" Then

            If slJTvmNJREA.ContainsKey(tmpNJREAAccount) Then
                tmpNJREADescription = slJTvmNJREA(tmpNJREAAccount).JTvmNJREADescription
                Return "EXISTS"
            Else
                Return "NOF"
            End If
        End If
        If tmpAction = "Update" Then
            Dim tmpItem As NJRExpAcctStruct
            tmpItem.JTvmNJREAName = tmpNJREAAccount
            tmpItem.JTvmNJREADescription = tmpNJREADescription
            If slJTvmNJREA.ContainsKey(tmpItem.JTvmNJREAName) Then
                slJTvmNJREA.Remove(tmpItem.JTvmNJREAName)
            End If
            slJTvmNJREA.Add(tmpItem.JTvmNJREAName, tmpItem)
        End If
        Return "UPDATED"
    End Function
    ''' <summary>
    '''   <para>Function to return account names from sljtvmNJREA, one at a time. tmpSub controls
    '''         which record is returned. Value of "DATA" is return if a record has been retrieved. 
    '''         "END" is returned, if there are no more records. </para>
    '''   <para>This sub is called from the reconcilaition process to prepare edit for the account 
    '''         names in NJR transaction entry.</para>
    ''' </summary>
    ''' <param name="tmpsub">Relative number of the sl item being returned.</param>
    ''' <param name="tmpAccount">The account name is returned.</param>
    Public Function JTvmReadNJREA(ByRef tmpsub As Integer, ByRef tmpAccount As String, ByRef tmpDesc As String) As String

        Dim key As ICollection = slJTvmNJREA.Keys
        If tmpsub >= key.Count Then
            Return "END"
        End If
        tmpAccount = slJTvmNJREA(key(tmpsub)).JTvmNJREAName
        tmpDesc = slJTvmNJREA(key(tmpsub)).JTvmNJREADescription
        tmpsub += 1
        Return "DATA"
    End Function



    '
End Class