﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTnlc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.mtxtJTnlcWEDate = New System.Windows.Forms.MaskedTextBox()
        Me.lvJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvDescrpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSpplrName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvoice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvCost = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMargin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvCstToCust = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtErrMsg = New System.Windows.Forms.TextBox()
        Me.btnExittoMenu = New System.Windows.Forms.Button()
        Me.lvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTnlcData = New System.Windows.Forms.ListView()
        Me.lvMorS = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PymtMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJobInvDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.slJTnlcKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtJTnlcMatCst = New System.Windows.Forms.TextBox()
        Me.txtJTnlcTlInvd = New System.Windows.Forms.TextBox()
        Me.txtJTnlcSubInvd = New System.Windows.Forms.TextBox()
        Me.txtJTnlcTlMrgn = New System.Windows.Forms.TextBox()
        Me.txtJTnlcSubMrgn = New System.Windows.Forms.TextBox()
        Me.txtJTnlcTlCst = New System.Windows.Forms.TextBox()
        Me.txtJTnlcSubCst = New System.Windows.Forms.TextBox()
        Me.txtJTnlcMatInvd = New System.Windows.Forms.TextBox()
        Me.txtJTnlcMatMrgn = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gboxJTnlcSeq = New System.Windows.Forms.GroupBox()
        Me.rbtnJTnlcseqSpplr = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnlcSeqDate = New System.Windows.Forms.RadioButton()
        Me.rbtnJtnlcSeqJob = New System.Windows.Forms.RadioButton()
        Me.gboxJTnlcFocus = New System.Windows.Forms.GroupBox()
        Me.cboxJTnlcDataFocus = New System.Windows.Forms.ComboBox()
        Me.lblJTnlcFocusTitle = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.gboxJTnlcSeq.SuspendLayout()
        Me.gboxJTnlcFocus.SuspendLayout()
        Me.SuspendLayout()
        '
        'mtxtJTnlcWEDate
        '
        Me.mtxtJTnlcWEDate.BeepOnError = True
        Me.mtxtJTnlcWEDate.Location = New System.Drawing.Point(1437, 13)
        Me.mtxtJTnlcWEDate.Margin = New System.Windows.Forms.Padding(4)
        Me.mtxtJTnlcWEDate.Mask = "00/00/0000"
        Me.mtxtJTnlcWEDate.Name = "mtxtJTnlcWEDate"
        Me.mtxtJTnlcWEDate.Size = New System.Drawing.Size(100, 22)
        Me.mtxtJTnlcWEDate.TabIndex = 65
        Me.mtxtJTnlcWEDate.ValidatingType = GetType(Date)
        '
        'lvJobID
        '
        Me.lvJobID.Text = "Customer"
        Me.lvJobID.Width = 100
        '
        'lvSubName
        '
        Me.lvSubName.Text = "Job"
        Me.lvSubName.Width = 150
        '
        'lvDescrpt
        '
        Me.lvDescrpt.Text = "Material or Service Purchased"
        Me.lvDescrpt.Width = 230
        '
        'lvSpplrName
        '
        Me.lvSpplrName.Text = "Spplr/Contrct Name"
        Me.lvSpplrName.Width = 120
        '
        'lvInvoice
        '
        Me.lvInvoice.Text = "Invoice #"
        Me.lvInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvInvCost
        '
        Me.lvInvCost.Text = "Cost"
        Me.lvInvCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvInvCost.Width = 75
        '
        'lvMargin
        '
        Me.lvMargin.Text = "Mrgn %"
        Me.lvMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMargin.Width = 50
        '
        'lvCstToCust
        '
        Me.lvCstToCust.Text = "Cst to Cust"
        Me.lvCstToCust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvCstToCust.Width = 75
        '
        'txtErrMsg
        '
        Me.txtErrMsg.Location = New System.Drawing.Point(350, 593)
        Me.txtErrMsg.Margin = New System.Windows.Forms.Padding(4)
        Me.txtErrMsg.Multiline = True
        Me.txtErrMsg.Name = "txtErrMsg"
        Me.txtErrMsg.ReadOnly = True
        Me.txtErrMsg.Size = New System.Drawing.Size(471, 149)
        Me.txtErrMsg.TabIndex = 63
        '
        'btnExittoMenu
        '
        Me.btnExittoMenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExittoMenu.Location = New System.Drawing.Point(53, 605)
        Me.btnExittoMenu.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExittoMenu.Name = "btnExittoMenu"
        Me.btnExittoMenu.Size = New System.Drawing.Size(211, 127)
        Me.btnExittoMenu.TabIndex = 5
        Me.btnExittoMenu.Text = "Exit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "to" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Menu"
        Me.btnExittoMenu.UseVisualStyleBackColor = True
        '
        'lvDate
        '
        Me.lvDate.Text = "Date"
        Me.lvDate.Width = 80
        '
        'lvJTnlcData
        '
        Me.lvJTnlcData.AllowDrop = True
        Me.lvJTnlcData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvDate, Me.lvJobID, Me.lvSubName, Me.lvMorS, Me.lvDescrpt, Me.lvSpplrName, Me.lvInvoice, Me.lvInvCost, Me.lvMargin, Me.lvCstToCust, Me.PymtMeth, Me.lvJobInvDt, Me.slJTnlcKey})
        Me.lvJTnlcData.FullRowSelect = True
        Me.lvJTnlcData.HideSelection = False
        Me.lvJTnlcData.Location = New System.Drawing.Point(13, 72)
        Me.lvJTnlcData.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTnlcData.MultiSelect = False
        Me.lvJTnlcData.Name = "lvJTnlcData"
        Me.lvJTnlcData.Size = New System.Drawing.Size(1524, 498)
        Me.lvJTnlcData.TabIndex = 5
        Me.lvJTnlcData.UseCompatibleStateImageBehavior = False
        Me.lvJTnlcData.View = System.Windows.Forms.View.Details
        '
        'lvMorS
        '
        Me.lvMorS.Text = "M or S"
        Me.lvMorS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMorS.Width = 50
        '
        'PymtMeth
        '
        Me.PymtMeth.Text = "P.M."
        Me.PymtMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.PymtMeth.Width = 35
        '
        'lvJobInvDt
        '
        Me.lvJobInvDt.Text = "JobInv Dt"
        Me.lvJobInvDt.Width = 80
        '
        'slJTnlcKey
        '
        Me.slJTnlcKey.Width = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1378, 14)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 16)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Date"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(49, 107)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 17)
        Me.Label22.TabIndex = 81
        Me.Label22.Text = "Total"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(49, 79)
        Me.Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(86, 17)
        Me.Label23.TabIndex = 80
        Me.Label23.Text = "SubContract"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(49, 46)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(65, 17)
        Me.Label24.TabIndex = 79
        Me.Label24.Text = "Materials"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(157, 23)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(36, 17)
        Me.Label19.TabIndex = 75
        Me.Label19.Text = "Cost"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(233, 23)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(63, 17)
        Me.Label20.TabIndex = 76
        Me.Label20.Text = "Margin $"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(327, 23)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(61, 17)
        Me.Label21.TabIndex = 77
        Me.Label21.Text = "JobInv $"
        '
        'txtJTnlcMatCst
        '
        Me.txtJTnlcMatCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcMatCst.Location = New System.Drawing.Point(144, 43)
        Me.txtJTnlcMatCst.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcMatCst.Name = "txtJTnlcMatCst"
        Me.txtJTnlcMatCst.ReadOnly = True
        Me.txtJTnlcMatCst.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcMatCst.TabIndex = 90
        Me.txtJTnlcMatCst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcTlInvd
        '
        Me.txtJTnlcTlInvd.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcTlInvd.Location = New System.Drawing.Point(320, 107)
        Me.txtJTnlcTlInvd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcTlInvd.Name = "txtJTnlcTlInvd"
        Me.txtJTnlcTlInvd.ReadOnly = True
        Me.txtJTnlcTlInvd.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcTlInvd.TabIndex = 91
        Me.txtJTnlcTlInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcSubInvd
        '
        Me.txtJTnlcSubInvd.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcSubInvd.Location = New System.Drawing.Point(320, 75)
        Me.txtJTnlcSubInvd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcSubInvd.Name = "txtJTnlcSubInvd"
        Me.txtJTnlcSubInvd.ReadOnly = True
        Me.txtJTnlcSubInvd.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcSubInvd.TabIndex = 92
        Me.txtJTnlcSubInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcTlMrgn
        '
        Me.txtJTnlcTlMrgn.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcTlMrgn.Location = New System.Drawing.Point(232, 107)
        Me.txtJTnlcTlMrgn.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcTlMrgn.Name = "txtJTnlcTlMrgn"
        Me.txtJTnlcTlMrgn.ReadOnly = True
        Me.txtJTnlcTlMrgn.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcTlMrgn.TabIndex = 93
        Me.txtJTnlcTlMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcSubMrgn
        '
        Me.txtJTnlcSubMrgn.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcSubMrgn.Location = New System.Drawing.Point(232, 75)
        Me.txtJTnlcSubMrgn.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcSubMrgn.Name = "txtJTnlcSubMrgn"
        Me.txtJTnlcSubMrgn.ReadOnly = True
        Me.txtJTnlcSubMrgn.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcSubMrgn.TabIndex = 94
        Me.txtJTnlcSubMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcTlCst
        '
        Me.txtJTnlcTlCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcTlCst.Location = New System.Drawing.Point(144, 107)
        Me.txtJTnlcTlCst.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcTlCst.Name = "txtJTnlcTlCst"
        Me.txtJTnlcTlCst.ReadOnly = True
        Me.txtJTnlcTlCst.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcTlCst.TabIndex = 95
        Me.txtJTnlcTlCst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcSubCst
        '
        Me.txtJTnlcSubCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcSubCst.Location = New System.Drawing.Point(144, 75)
        Me.txtJTnlcSubCst.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcSubCst.Name = "txtJTnlcSubCst"
        Me.txtJTnlcSubCst.ReadOnly = True
        Me.txtJTnlcSubCst.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcSubCst.TabIndex = 96
        Me.txtJTnlcSubCst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcMatInvd
        '
        Me.txtJTnlcMatInvd.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcMatInvd.Location = New System.Drawing.Point(320, 43)
        Me.txtJTnlcMatInvd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcMatInvd.Name = "txtJTnlcMatInvd"
        Me.txtJTnlcMatInvd.ReadOnly = True
        Me.txtJTnlcMatInvd.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcMatInvd.TabIndex = 97
        Me.txtJTnlcMatInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcMatMrgn
        '
        Me.txtJTnlcMatMrgn.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcMatMrgn.Location = New System.Drawing.Point(232, 42)
        Me.txtJTnlcMatMrgn.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcMatMrgn.Name = "txtJTnlcMatMrgn"
        Me.txtJTnlcMatMrgn.ReadOnly = True
        Me.txtJTnlcMatMrgn.Size = New System.Drawing.Size(79, 22)
        Me.txtJTnlcMatMrgn.TabIndex = 98
        Me.txtJTnlcMatMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcMatMrgn)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcMatInvd)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcSubCst)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcTlCst)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcSubMrgn)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcTlMrgn)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcSubInvd)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcTlInvd)
        Me.GroupBox1.Controls.Add(Me.txtJTnlcMatCst)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(1051, 593)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(420, 151)
        Me.GroupBox1.TabIndex = 99
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Unbilled Non-Labor Activity Status"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(36, 8)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(219, 48)
        Me.Label11.TabIndex = 100
        Me.Label11.Text = "Active Non-Labor Cost" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Transactions"
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'gboxJTnlcSeq
        '
        Me.gboxJTnlcSeq.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gboxJTnlcSeq.Controls.Add(Me.rbtnJTnlcseqSpplr)
        Me.gboxJTnlcSeq.Controls.Add(Me.rbtnJTnlcSeqDate)
        Me.gboxJTnlcSeq.Controls.Add(Me.rbtnJtnlcSeqJob)
        Me.gboxJTnlcSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTnlcSeq.Location = New System.Drawing.Point(323, 10)
        Me.gboxJTnlcSeq.Name = "gboxJTnlcSeq"
        Me.gboxJTnlcSeq.Size = New System.Drawing.Size(320, 55)
        Me.gboxJTnlcSeq.TabIndex = 107
        Me.gboxJTnlcSeq.TabStop = False
        Me.gboxJTnlcSeq.Text = "Panel Sequence"
        '
        'rbtnJTnlcseqSpplr
        '
        Me.rbtnJTnlcseqSpplr.AutoSize = True
        Me.rbtnJTnlcseqSpplr.Location = New System.Drawing.Point(204, 21)
        Me.rbtnJTnlcseqSpplr.Name = "rbtnJTnlcseqSpplr"
        Me.rbtnJTnlcseqSpplr.Size = New System.Drawing.Size(90, 22)
        Me.rbtnJTnlcseqSpplr.TabIndex = 2
        Me.rbtnJTnlcseqSpplr.TabStop = True
        Me.rbtnJTnlcseqSpplr.Text = "Supplier"
        Me.rbtnJTnlcseqSpplr.UseVisualStyleBackColor = True
        '
        'rbtnJTnlcSeqDate
        '
        Me.rbtnJTnlcSeqDate.AutoSize = True
        Me.rbtnJTnlcSeqDate.Location = New System.Drawing.Point(106, 21)
        Me.rbtnJTnlcSeqDate.Name = "rbtnJTnlcSeqDate"
        Me.rbtnJTnlcSeqDate.Size = New System.Drawing.Size(64, 22)
        Me.rbtnJTnlcSeqDate.TabIndex = 1
        Me.rbtnJTnlcSeqDate.TabStop = True
        Me.rbtnJTnlcSeqDate.Text = "Date"
        Me.rbtnJTnlcSeqDate.UseVisualStyleBackColor = True
        '
        'rbtnJtnlcSeqJob
        '
        Me.rbtnJtnlcSeqJob.AutoSize = True
        Me.rbtnJtnlcSeqJob.Location = New System.Drawing.Point(13, 22)
        Me.rbtnJtnlcSeqJob.Name = "rbtnJtnlcSeqJob"
        Me.rbtnJtnlcSeqJob.Size = New System.Drawing.Size(57, 22)
        Me.rbtnJtnlcSeqJob.TabIndex = 0
        Me.rbtnJtnlcSeqJob.TabStop = True
        Me.rbtnJtnlcSeqJob.Text = "Job"
        Me.rbtnJtnlcSeqJob.UseVisualStyleBackColor = True
        '
        'gboxJTnlcFocus
        '
        Me.gboxJTnlcFocus.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gboxJTnlcFocus.Controls.Add(Me.cboxJTnlcDataFocus)
        Me.gboxJTnlcFocus.Controls.Add(Me.lblJTnlcFocusTitle)
        Me.gboxJTnlcFocus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTnlcFocus.Location = New System.Drawing.Point(662, 10)
        Me.gboxJTnlcFocus.Name = "gboxJTnlcFocus"
        Me.gboxJTnlcFocus.Size = New System.Drawing.Size(478, 55)
        Me.gboxJTnlcFocus.TabIndex = 108
        Me.gboxJTnlcFocus.TabStop = False
        Me.gboxJTnlcFocus.Text = "Data Focus"
        '
        'cboxJTnlcDataFocus
        '
        Me.cboxJTnlcDataFocus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnlcDataFocus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnlcDataFocus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxJTnlcDataFocus.FormattingEnabled = True
        Me.cboxJTnlcDataFocus.Location = New System.Drawing.Point(159, 19)
        Me.cboxJTnlcDataFocus.Name = "cboxJTnlcDataFocus"
        Me.cboxJTnlcDataFocus.Size = New System.Drawing.Size(299, 26)
        Me.cboxJTnlcDataFocus.TabIndex = 109
        '
        'lblJTnlcFocusTitle
        '
        Me.lblJTnlcFocusTitle.AutoSize = True
        Me.lblJTnlcFocusTitle.Location = New System.Drawing.Point(37, 25)
        Me.lblJTnlcFocusTitle.Name = "lblJTnlcFocusTitle"
        Me.lblJTnlcFocusTitle.Size = New System.Drawing.Size(87, 18)
        Me.lblJTnlcFocusTitle.TabIndex = 0
        Me.lblJTnlcFocusTitle.Text = "Entry Date"
        '
        'JTnlc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(1550, 752)
        Me.ControlBox = False
        Me.Controls.Add(Me.gboxJTnlcFocus)
        Me.Controls.Add(Me.gboxJTnlcSeq)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.mtxtJTnlcWEDate)
        Me.Controls.Add(Me.txtErrMsg)
        Me.Controls.Add(Me.btnExittoMenu)
        Me.Controls.Add(Me.lvJTnlcData)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTnlc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Non-Labor Costs"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gboxJTnlcSeq.ResumeLayout(False)
        Me.gboxJTnlcSeq.PerformLayout()
        Me.gboxJTnlcFocus.ResumeLayout(False)
        Me.gboxJTnlcFocus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mtxtJTnlcWEDate As MaskedTextBox
    Friend WithEvents lvJobID As ColumnHeader
    Friend WithEvents lvSubName As ColumnHeader
    Friend WithEvents lvDescrpt As ColumnHeader
    Friend WithEvents lvSpplrName As ColumnHeader
    Friend WithEvents lvInvoice As ColumnHeader
    Friend WithEvents lvInvCost As ColumnHeader
    Friend WithEvents lvMargin As ColumnHeader
    Friend WithEvents lvCstToCust As ColumnHeader
    Friend WithEvents txtErrMsg As TextBox
    Friend WithEvents btnExittoMenu As Button
    Friend WithEvents lvDate As ColumnHeader
    Friend WithEvents lvJTnlcData As ListView
    Friend WithEvents lvMorS As ColumnHeader
    Friend WithEvents Label3 As Label
    Friend WithEvents lvJobInvDt As ColumnHeader
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents txtJTnlcMatCst As TextBox
    Friend WithEvents txtJTnlcTlInvd As TextBox
    Friend WithEvents txtJTnlcSubInvd As TextBox
    Friend WithEvents txtJTnlcTlMrgn As TextBox
    Friend WithEvents txtJTnlcSubMrgn As TextBox
    Friend WithEvents txtJTnlcTlCst As TextBox
    Friend WithEvents txtJTnlcSubCst As TextBox
    Friend WithEvents txtJTnlcMatInvd As TextBox
    Friend WithEvents txtJTnlcMatMrgn As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label11 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents gboxJTnlcSeq As GroupBox
    Friend WithEvents rbtnJTnlcSeqDate As RadioButton
    Friend WithEvents rbtnJtnlcSeqJob As RadioButton
    Friend WithEvents slJTnlcKey As ColumnHeader
    Friend WithEvents PymtMeth As ColumnHeader
    Friend WithEvents rbtnJTnlcseqSpplr As RadioButton
    Friend WithEvents gboxJTnlcFocus As GroupBox
    Friend WithEvents cboxJTnlcDataFocus As ComboBox
    Friend WithEvents lblJTnlcFocusTitle As Label
End Class
