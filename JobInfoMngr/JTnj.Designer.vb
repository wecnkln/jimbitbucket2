﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTnj
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTnj))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTnjSubName = New System.Windows.Forms.TextBox()
        Me.btnJTnjContinue = New System.Windows.Forms.Button()
        Me.btnJTnjCancel = New System.Windows.Forms.Button()
        Me.txtErrMessage = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lvJTnjJobs = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.gboxJTnjJobPricing = New System.Windows.Forms.GroupBox()
        Me.rbtnJTnjPricingAUP = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnjPricingCostPlusDeposits = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnjPricingCostPlus = New System.Windows.Forms.RadioButton()
        Me.gboxJTnjAUPsw = New System.Windows.Forms.GroupBox()
        Me.chkbJTnjAUPSer = New System.Windows.Forms.CheckBox()
        Me.chkbJTnjAUPMat = New System.Windows.Forms.CheckBox()
        Me.chkbJTnjAUPLab = New System.Windows.Forms.CheckBox()
        Me.cboxJTnjJobName = New System.Windows.Forms.ComboBox()
        Me.gboxJTnjJobPricing.SuspendLayout()
        Me.gboxJTnjAUPsw.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(186, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Enter Job's Customer name."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(39, 85)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 17)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Enter New Job name."
        '
        'txtJTnjSubName
        '
        Me.txtJTnjSubName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTnjSubName.Location = New System.Drawing.Point(115, 110)
        Me.txtJTnjSubName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnjSubName.Name = "txtJTnjSubName"
        Me.txtJTnjSubName.Size = New System.Drawing.Size(153, 22)
        Me.txtJTnjSubName.TabIndex = 3
        '
        'btnJTnjContinue
        '
        Me.btnJTnjContinue.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnjContinue.Location = New System.Drawing.Point(37, 330)
        Me.btnJTnjContinue.Margin = New System.Windows.Forms.Padding(4)
        Me.btnJTnjContinue.Name = "btnJTnjContinue"
        Me.btnJTnjContinue.Size = New System.Drawing.Size(140, 43)
        Me.btnJTnjContinue.TabIndex = 4
        Me.btnJTnjContinue.Text = "C O N T I N U E"
        Me.btnJTnjContinue.UseVisualStyleBackColor = True
        '
        'btnJTnjCancel
        '
        Me.btnJTnjCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnjCancel.Location = New System.Drawing.Point(185, 330)
        Me.btnJTnjCancel.Margin = New System.Windows.Forms.Padding(4)
        Me.btnJTnjCancel.Name = "btnJTnjCancel"
        Me.btnJTnjCancel.Size = New System.Drawing.Size(140, 43)
        Me.btnJTnjCancel.TabIndex = 5
        Me.btnJTnjCancel.Text = "C A N C E L"
        Me.btnJTnjCancel.UseVisualStyleBackColor = True
        '
        'txtErrMessage
        '
        Me.txtErrMessage.Location = New System.Drawing.Point(23, 388)
        Me.txtErrMessage.Margin = New System.Windows.Forms.Padding(4)
        Me.txtErrMessage.Multiline = True
        Me.txtErrMessage.Name = "txtErrMessage"
        Me.txtErrMessage.ReadOnly = True
        Me.txtErrMessage.Size = New System.Drawing.Size(316, 54)
        Me.txtErrMessage.TabIndex = 6
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'lvJTnjJobs
        '
        Me.lvJTnjJobs.CheckBoxes = True
        Me.lvJTnjJobs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7})
        Me.lvJTnjJobs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvJTnjJobs.HideSelection = False
        Me.lvJTnjJobs.Location = New System.Drawing.Point(355, 89)
        Me.lvJTnjJobs.MultiSelect = False
        Me.lvJTnjJobs.Name = "lvJTnjJobs"
        Me.lvJTnjJobs.Size = New System.Drawing.Size(931, 261)
        Me.lvJTnjJobs.TabIndex = 7
        Me.lvJTnjJobs.UseCompatibleStateImageBehavior = False
        Me.lvJTnjJobs.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "     Job"
        Me.ColumnHeader1.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Job Description"
        Me.ColumnHeader2.Width = 200
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Date Created"
        Me.ColumnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader3.Width = 80
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Lst Activity"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader4.Width = 80
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Deactivated"
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader5.Width = 80
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Work Street Address"
        Me.ColumnHeader6.Width = 200
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Work City, State"
        Me.ColumnHeader7.Width = 200
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(367, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(412, 20)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Current Jobs for this customer. -------------------"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(355, 371)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(564, 71)
        Me.TextBox1.TabIndex = 9
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'gboxJTnjJobPricing
        '
        Me.gboxJTnjJobPricing.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gboxJTnjJobPricing.Controls.Add(Me.rbtnJTnjPricingAUP)
        Me.gboxJTnjJobPricing.Controls.Add(Me.rbtnJTnjPricingCostPlusDeposits)
        Me.gboxJTnjJobPricing.Controls.Add(Me.rbtnJTnjPricingCostPlus)
        Me.gboxJTnjJobPricing.Location = New System.Drawing.Point(37, 146)
        Me.gboxJTnjJobPricing.Name = "gboxJTnjJobPricing"
        Me.gboxJTnjJobPricing.Size = New System.Drawing.Size(287, 112)
        Me.gboxJTnjJobPricing.TabIndex = 10
        Me.gboxJTnjJobPricing.TabStop = False
        Me.gboxJTnjJobPricing.Text = "Job Pricing"
        '
        'rbtnJTnjPricingAUP
        '
        Me.rbtnJTnjPricingAUP.AutoSize = True
        Me.rbtnJTnjPricingAUP.Location = New System.Drawing.Point(23, 78)
        Me.rbtnJTnjPricingAUP.Name = "rbtnJTnjPricingAUP"
        Me.rbtnJTnjPricingAUP.Size = New System.Drawing.Size(202, 21)
        Me.rbtnJTnjPricingAUP.TabIndex = 2
        Me.rbtnJTnjPricingAUP.Text = "Agreed Upon Pricing (AUP)"
        Me.rbtnJTnjPricingAUP.UseVisualStyleBackColor = True
        '
        'rbtnJTnjPricingCostPlusDeposits
        '
        Me.rbtnJTnjPricingCostPlusDeposits.AutoSize = True
        Me.rbtnJTnjPricingCostPlusDeposits.Location = New System.Drawing.Point(23, 51)
        Me.rbtnJTnjPricingCostPlusDeposits.Name = "rbtnJTnjPricingCostPlusDeposits"
        Me.rbtnJTnjPricingCostPlusDeposits.Size = New System.Drawing.Size(156, 21)
        Me.rbtnJTnjPricingCostPlusDeposits.TabIndex = 1
        Me.rbtnJTnjPricingCostPlusDeposits.Text = "Cost + with Deposits"
        Me.rbtnJTnjPricingCostPlusDeposits.UseVisualStyleBackColor = True
        '
        'rbtnJTnjPricingCostPlus
        '
        Me.rbtnJTnjPricingCostPlus.AutoSize = True
        Me.rbtnJTnjPricingCostPlus.Checked = True
        Me.rbtnJTnjPricingCostPlus.Location = New System.Drawing.Point(23, 25)
        Me.rbtnJTnjPricingCostPlus.Name = "rbtnJTnjPricingCostPlus"
        Me.rbtnJTnjPricingCostPlus.Size = New System.Drawing.Size(69, 21)
        Me.rbtnJTnjPricingCostPlus.TabIndex = 0
        Me.rbtnJTnjPricingCostPlus.TabStop = True
        Me.rbtnJTnjPricingCostPlus.Text = "Cost +"
        Me.rbtnJTnjPricingCostPlus.UseVisualStyleBackColor = True
        '
        'gboxJTnjAUPsw
        '
        Me.gboxJTnjAUPsw.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gboxJTnjAUPsw.Controls.Add(Me.chkbJTnjAUPSer)
        Me.gboxJTnjAUPsw.Controls.Add(Me.chkbJTnjAUPMat)
        Me.gboxJTnjAUPsw.Controls.Add(Me.chkbJTnjAUPLab)
        Me.gboxJTnjAUPsw.Location = New System.Drawing.Point(36, 264)
        Me.gboxJTnjAUPsw.Name = "gboxJTnjAUPsw"
        Me.gboxJTnjAUPsw.Size = New System.Drawing.Size(288, 59)
        Me.gboxJTnjAUPsw.TabIndex = 11
        Me.gboxJTnjAUPsw.TabStop = False
        Me.gboxJTnjAUPsw.Text = "AUP Job Charges Cover ..."
        '
        'chkbJTnjAUPSer
        '
        Me.chkbJTnjAUPSer.AutoSize = True
        Me.chkbJTnjAUPSer.Checked = True
        Me.chkbJTnjAUPSer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkbJTnjAUPSer.Location = New System.Drawing.Point(189, 28)
        Me.chkbJTnjAUPSer.Name = "chkbJTnjAUPSer"
        Me.chkbJTnjAUPSer.Size = New System.Drawing.Size(84, 21)
        Me.chkbJTnjAUPSer.TabIndex = 2
        Me.chkbJTnjAUPSer.Text = "Services"
        Me.chkbJTnjAUPSer.UseVisualStyleBackColor = True
        '
        'chkbJTnjAUPMat
        '
        Me.chkbJTnjAUPMat.AutoSize = True
        Me.chkbJTnjAUPMat.Checked = True
        Me.chkbJTnjAUPMat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkbJTnjAUPMat.Location = New System.Drawing.Point(96, 28)
        Me.chkbJTnjAUPMat.Name = "chkbJTnjAUPMat"
        Me.chkbJTnjAUPMat.Size = New System.Drawing.Size(80, 21)
        Me.chkbJTnjAUPMat.TabIndex = 1
        Me.chkbJTnjAUPMat.Text = "Material"
        Me.chkbJTnjAUPMat.UseVisualStyleBackColor = True
        '
        'chkbJTnjAUPLab
        '
        Me.chkbJTnjAUPLab.AutoSize = True
        Me.chkbJTnjAUPLab.Checked = True
        Me.chkbJTnjAUPLab.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkbJTnjAUPLab.Location = New System.Drawing.Point(13, 28)
        Me.chkbJTnjAUPLab.Name = "chkbJTnjAUPLab"
        Me.chkbJTnjAUPLab.Size = New System.Drawing.Size(67, 21)
        Me.chkbJTnjAUPLab.TabIndex = 0
        Me.chkbJTnjAUPLab.Text = "Labor"
        Me.chkbJTnjAUPLab.UseVisualStyleBackColor = True
        '
        'cboxJTnjJobName
        '
        Me.cboxJTnjJobName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnjJobName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnjJobName.FormattingEnabled = True
        Me.cboxJTnjJobName.Location = New System.Drawing.Point(115, 54)
        Me.cboxJTnjJobName.Name = "cboxJTnjJobName"
        Me.cboxJTnjJobName.Size = New System.Drawing.Size(169, 24)
        Me.cboxJTnjJobName.TabIndex = 12
        '
        'JTnj
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1308, 470)
        Me.ControlBox = False
        Me.Controls.Add(Me.cboxJTnjJobName)
        Me.Controls.Add(Me.gboxJTnjAUPsw)
        Me.Controls.Add(Me.gboxJTnjJobPricing)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lvJTnjJobs)
        Me.Controls.Add(Me.txtErrMessage)
        Me.Controls.Add(Me.btnJTnjCancel)
        Me.Controls.Add(Me.btnJTnjContinue)
        Me.Controls.Add(Me.txtJTnjSubName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTnj"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager  - New Job"
        Me.TopMost = True
        Me.gboxJTnjJobPricing.ResumeLayout(False)
        Me.gboxJTnjJobPricing.PerformLayout()
        Me.gboxJTnjAUPsw.ResumeLayout(False)
        Me.gboxJTnjAUPsw.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTnjSubName As TextBox
    Friend WithEvents btnJTnjContinue As Button
    Friend WithEvents btnJTnjCancel As Button
    Friend WithEvents txtErrMessage As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents lvJTnjJobs As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents Label3 As Label
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents gboxJTnjJobPricing As GroupBox
    Friend WithEvents rbtnJTnjPricingAUP As RadioButton
    Friend WithEvents rbtnJTnjPricingCostPlusDeposits As RadioButton
    Friend WithEvents rbtnJTnjPricingCostPlus As RadioButton
    Friend WithEvents gboxJTnjAUPsw As GroupBox
    Friend WithEvents chkbJTnjAUPSer As CheckBox
    Friend WithEvents chkbJTnjAUPMat As CheckBox
    Friend WithEvents chkbJTnjAUPLab As CheckBox
    Friend WithEvents cboxJTnjJobName As ComboBox
End Class
