﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.Diagnostics


''' <summary>
''' Invoice history and invoice production are handled within this class. It has been 
''' recently modified to use iTextPDF to output invoices. It creates and maintains the 
''' invoice history file.
''' </summary>
Public Class JTas
    '
    ' -----------------------------------------------------------------
    ' Variable set up to support line item corrections. The first is a
    ' key to be passed. The rest are values saved so the INIT sub can be
    ' called after line item correction is complete (JTnlcE).
    ' ------------------------------------------------------------------
    Dim JTasItemCorrectionNLCKey As String = Nothing
    Public JTasJobID As String
    Public JTasSubID As String
    Public JTasJobDesc As String
    Public JTasJobPricing As String
    Public JTasLayer As String
    ' -----------------------------------------------------------------
    Public Structure JTasIHStru
        Dim ihJobID As String
        Dim ihSubID As String
        Dim ihInvDate As String
        Dim ihInvNum As String
        Dim ihLab As String
        Dim ihMat As String
        Dim ihSub As String
        Dim ihAdj As String
        Dim ihIH As String
        Dim ihInvTl As String
        Dim ihEstMrgn As String
        Dim ihAUPInv As String
        ' -----------------------------------------------------------------
        ' Following four fields relate to sales tax and taxable sales.
        ' -----------------------------------------------------------------
        Dim ihWorkLocState As String      ' added 2/7/2018
        Dim ihTaxExempt As Boolean        ' added 2/7/2018
        Dim ihTaxableSales As String      ' added 2/7/2018
        Dim ihTaxAmount As String         ' added 2/7/2018
        Dim ihFile As String
        ' -----------------------------------------------------------------
        ' ihFileDetails - PDF file that shows detailed invoice elements for
        ' AUP, No Cost display or totals only invoicing. This report is
        ' for internal use.
        ' -----------------------------------------------------------------
        Dim ihFileDetails As String      ' added 2/7/2018
        Dim ihCostMeth As String
        Dim ihAUPTKsw As String
        Dim ihAUPMATsw As String
        Dim ihAUPSERsw As String
        Dim ihDoNotExpt As String
        Dim ihAcctExptDate As String
        Dim ihEmailedDate As String
    End Structure
    '
    Public JTastkStru As JTtk.JTtkArrayStructure
    Public JTasnlcStru As JTnlc.JTnlcStructure
    Public JTasAUPStru As JTaup.JTaupInvStru
    Public slJTasTK As New SortedList
    Public slJTasTKKeys As New SortedList
    Public slJTasNLCMat As New SortedList
    Public slJTasNLCMatKeys As New SortedList
    Public slJtasNLCSub As New SortedList
    Public slJTasNLCSubKeys As New SortedList
    Public slJTasAUP As New SortedList
    Public slJTasAUPKeys As New SortedList
    '
    Public slJTasIH As New SortedList
    Public JTasSub As Integer = 0
    '
    ' sl to keep track of all jobs and subjobs to be printed on the requested invoice
    '
    Public slJTasInvJobs As New SortedList
    '
    Public Structure JTasIJstru
        Dim JTasIJJobID As String
        Dim JTasIJSubJobID As String
        Dim JTasIJDescrip As String
        Dim JTasIJCostMeth As String
        Dim JtasIJTKsw As String
        Dim JTasIJMATsw As String
        Dim JTasIJSERsw As String
    End Structure
    '
    Public slJTasIJTls As New SortedList
    '
    Public Structure JTasIJTstru
        Dim JTasIJAUPTl As Double       ' fields to keep totals for each job/subjob on the invoice.
        Dim JTasIJLabTl As Double
        Dim JTasIJSubTl As Double
        Dim JTasIJMatTl As Double
        Dim JTasIJIHTl As Double
        Dim JTasIJAdjTl As Double
        Dim JTasIJGrdTl As Double
        Dim JTasIJAUPMrgn As Double
        Dim JTasIJLabMrgn As Double
        Dim JTasIJSubMrgn As Double
        Dim JTasIJMatMrgn As Double
        Dim jtasIJIHMrgn As Double
        Dim JTasIJAdjMrgn As Double
        Dim JTasIJMrgnTl As Double
        ' -----------------------------------------------------------------------
        ' Following fields temporarilly hold amounts of detailed charges
        ' that were covered by the AUP amount. They will be added back into
        ' the appropriate IH data fields prior to saving the JTInv Hist.dat file.
        ' -----------------------------------------------------------------------
        Dim JTasIJLabNPTl As Double
        Dim JTasIJSubNPTl As Double
        Dim JTasIJMatNPTl As Double
        Dim JTasIJIHNPTl As Double
        Dim JTasIJAdjNPTl As Double
        Dim JTasIJLabNPMrgn As Double
        Dim JTasIJSubNPMrgn As Double
        Dim JTasIJMatNPMrgn As Double
        Dim jtasIJIHNPMrgn As Double
        Dim JTasIJAdjNPMrgn As Double
    End Structure
    '
    Dim IJK As String = ""  'used as index for slJTasInvJobs

    '
    Dim CurrentInvJobTitle As String = Nothing   'used to control job title printing on invoice.
    '
    '
    Public slJTasLTS As New SortedList

    Public Structure JTasLTSitem
        Dim JTasLTSiHours As String
        Dim JTasLTSiDollars As String
    End Structure
    '
    '
    ' Invoice Totals
    '
    Public JTasInvAUPTl As Double = 0    ' added 8/12 for AUP Category
    Public JTasInvLabTl As Double = 0
    Public JTasInvSubTl As Double = 0
    Public JTasInvMatTl As Double = 0
    Public JTasInvIHTl As Double = 0     ' added 8/12 for Inhouse Category
    Public JTasInvAdjTl As Double = 0
    Public JTasInvGrdTl As Double = 0
    '
    Public JTasInvAUPMrgn As Double = 0    ' added 8/12 for AUP Category
    Public JTasInvLabMrgn As Double = 0
    Public JTasInvSubMrgn As Double = 0
    Public JTasInvMatMrgn As Double = 0
    Public JTasInvIHMrgn As Double = 0    ' added 8/12 for Inhouse Category
    Public JTasInvAdjMrgn As Double = 0
    Public JTasInvMrgnTl As Double = 0
    '
    Public JTasInvSectionsPrtd As Integer = 0    ' count of sections printed on an invoice.
    Public Yposition As Integer = 0              ' counter to control location on page
    Public JTasInvNumber As String
    Public JTasPageNumber As Integer = 0
    '
    Public tmpTlHours As Double = 0
    Public tmpTlDollars As Double = 0
    Public tmpTLDollarsMrgn As Double = 0
    '
    Public JTasNeedNewTable As Boolean = False
    Public AUPFinalizeFlag As Boolean = False
    Public SectionDone As Boolean = False
    Public ItemsToPrintFlag As Boolean = False
    Public tmpSub As Integer = 0
    Public tmpNumOfItems As Integer = 0
    '
    ' Invoice format settings
    ' -----------------------------------------------------------------------------------------
    ' Switch values copied from Global Variables. It the job has NonStdFrmt checked,
    ' specific values, if they exist, overlay the standard settings. A panel is presented to
    ' verify that these settings are still what is wanted. If nonstdfrmt settings are created,
    ' they will be stored in SLNonStdFrmt and written out at the end of the JTJobs file.
    ' -----------------------------------------------------------------------------------------
    ' Labor
    Public JTasInvLabTaskSummary As Boolean = True
    Public JtasInvLabWorkerDetail As Boolean = True
    Public JTasInvLabTotals As Boolean = True
    ' Services
    Public JTasInvSubSerDetails As Boolean = True
    Public JTasInvSubSerTotals As Boolean = True
    ' Materials
    Public JTasInvMatDetails As Boolean = True
    Public JTasInvMatTotals As Boolean = True
    '
    ' Material/Subcontractor Cost Options
    Public JTasInvMatShowCost As Boolean = True
    Public JTasInvSubShowCost As Boolean = True
    '
    ''' <summary>This sub loads or reloads the invoice history array (slJTasIH).</summary>
    Public Sub JTasHistLoad()
        ' -----------------------------------------------------------------------------
        ' This sub read the JOBS XML data and rebuilds jimArray.
        ' -----------------------------------------------------------------------------
        '
        ' Clear sorted list to make sure it is empty before load.
        '
        slJTasIH.Clear()
        '
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        '
        Dim ReadFileKey As String = ""
        Dim ReadFileData As String = ""
        '
        Dim tmpIHR As New JTasIHStru
        '
        Dim tmpRecordType As String = ""
        Dim tmpFileSeq As Integer = 10
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTInvHist.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTInvHist.Bkup") Then
            MsgBox("JTInvHist --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (AS.006)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTInvHist.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTInvHist.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTInvHist.Bkup", "JTInvHist.dat")
        End If
        ' ------------------------------------------------------------------

        '
        '
        ' --------------------------------------------------------------------------
        ' Counters to document if file names are being changed for invoice PDF files
        ' --------------------------------------------------------------------------
        Dim tmpInvFileNamePDFChgd As Integer = 0
        Dim tmpInvFileHistPDFChgd As Integer = 0
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    ReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If ReadFileKey = "ihRecord" Or ReadFileKey = "/ihRecord" Or ReadFileKey = "InvHistory" Or ReadFileKey = "/InvHistory" Then
                        ReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        ReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case ReadFileKey
                        '
                        Case "ihJobID"
                            tmpIHR.ihJobID = ReadFileData
                        Case "ihSubID"
                            tmpIHR.ihSubID = ReadFileData
                        Case "ihInvDate"
                            tmpIHR.ihInvDate = ReadFileData
                        Case "ihInvNum"
                            tmpIHR.ihInvNum = ReadFileData
                        Case "ihLab"
                            tmpIHR.ihLab = ReadFileData
                        Case "ihMat"
                            tmpIHR.ihMat = ReadFileData
                        Case "ihSub"
                            tmpIHR.ihSub = ReadFileData
                        Case "ihIH"
                            tmpIHR.ihIH = ReadFileData
                        Case "ihAdj"
                            tmpIHR.ihAdj = ReadFileData
                        Case "ihInvTl"
                            tmpIHR.ihInvTl = ReadFileData
                        Case "ihEstMrgn"
                            tmpIHR.ihEstMrgn = ReadFileData
                        Case "ihAUPInv"
                            tmpIHR.ihAUPInv = ReadFileData
                        Case "ihWorkLocState"
                            tmpIHR.ihWorkLocState = ReadFileData
                        Case "ihTaxExempt"
                            tmpIHR.ihTaxExempt = ReadFileData
                        Case "ihTaxableSales"
                            tmpIHR.ihTaxableSales = ReadFileData
                        Case "ihTaxAmount"
                            tmpIHR.ihTaxAmount = ReadFileData
                        Case "ihFile"
                            tmpIHR.ihFile = ReadFileData
                        Case "ihFileDetails"
                            tmpIHR.ihFileDetails = ReadFileData
                        Case "ihCostMeth"
                            tmpIHR.ihCostMeth = ReadFileData
                        Case "ihAUPTKsw"
                            tmpIHR.ihAUPTKsw = ReadFileData
                        Case "ihAUPMATsw"
                            tmpIHR.ihAUPMATsw = ReadFileData
                        Case "ihAUPSERsw"
                            tmpIHR.ihAUPSERsw = ReadFileData
                        Case "ihDoNotExpt"
                            tmpIHR.ihDoNotExpt = ReadFileData
                        Case "ihAcctExptDate"
                            tmpIHR.ihAcctExptDate = ReadFileData
                        Case "ihEmailedDate"
                            tmpIHR.ihEmailedDate = ReadFileData
                            '
                        '
                        Case "/ihRecord"
                            '
                            Dim tmpdate As Date = CDate(tmpIHR.ihInvDate)
                            '
                            Dim tmpKeyDate As String = 30000000 - Gregarian2Julian(tmpdate)
                            '
                            tmpFileSeq = 0
                            '
                            Dim tmpihKey As String = tmpIHR.ihJobID & tmpIHR.ihSubID & tmpKeyDate & Str(tmpFileSeq)
                            ' -------------------------------------------------------------------------------------
                            ' Loop inserted to check for duplicate keys. This could happen if a job is invoiced 
                            ' twice in one day. Adding to the sequence number is used to create a unique key.
                            ' -------------------------------------------------------------------------------------
                            Do While slJTasIH.ContainsKey(tmpihKey) = True
                                tmpFileSeq += 1
                                tmpihKey = tmpIHR.ihJobID & tmpIHR.ihSubID & tmpKeyDate & Str(tmpFileSeq)
                            Loop
                            slJTasIH.Add(tmpihKey, tmpIHR)
                            '
                            tmpIHR = Nothing
                    End Select
                    '
                    '
                End If
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' -----------------------------------------------------------
        ' Add code to create totals invoiced field in JTMMJobAct SL.
        ' Last invoice date will also be updated. Data will be used
        ' on MM panels.
        ' -----------------------------------------------------------
        Dim tmpJobID As String = ""
        Dim tmpSubID As String = ""
        Dim tmpInvTotals As Decimal = 0
        Dim tmpInvTotalsStr As String = ""
        Dim tmpLstInvDate As String = ""
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String
        For Each k In key
            If tmpJobID = "" And tmpSubID = "" Then
                tmpJobID = slJTasIH(k).ihJobID
                tmpSubID = slJTasIH(k).ihSubID
                tmpInvTotals = Val(slJTasIH(k).ihInvTl)
                tmpLstInvDate = slJTasIH(k).ihInvDate
            Else
                If tmpJobID = slJTasIH(k).ihJobID And tmpSubID = slJTasIH(k).ihSubID Then
                    tmpInvTotals += Val(slJTasIH(k).ihInvTl)
                    '    tmpLstInvDate = slJTasIH(k).ihInvDate
                Else
                    tmpInvTotalsStr = String.Format("{0:0.00}", tmpInvTotals)
                    JTMainMenu.JTmmUpdtInvTls(tmpJobID, tmpSubID, tmpInvTotalsStr, tmpLstInvDate)
                    tmpJobID = slJTasIH(k).ihJobID
                    tmpSubID = slJTasIH(k).ihSubID
                    tmpInvTotals = Val(slJTasIH(k).ihInvTl)
                    tmpLstInvDate = slJTasIH(k).ihInvDate
                End If
            End If
        Next
        ' process last key
        If tmpInvTotals <> 0 Then
            tmpInvTotalsStr = String.Format("{0:0.00}", tmpInvTotals)
            JTMainMenu.JTmmUpdtInvTls(tmpJobID, tmpSubID, tmpInvTotalsStr, tmpLstInvDate)
        End If
        If tmpInvFileNamePDFChgd <> 0 Or tmpInvFileHistPDFChgd <> 0 Then
            MsgBox("Invoice PDF file conversion ---" & vbCrLf &
                   "Invoice PDF files renamed - " & tmpInvFileNamePDFChgd & vbCrLf &
                   "Invoice Hist Record - file name changed - " & tmpInvFileHistPDFChgd & vbCrLf &
                   "Invoice History file will be flushed to disk.")
            JTasHistFlush()
        End If


    End Sub
    ''' <summary>
    ''' This is the matching sub to JTasHistLoad. This sub safes all the history data to 
    ''' disk. It is called from various locations within JTas. It is used to create quiet point 
    ''' to insure the dependability of the data.
    ''' </summary>
    Public Sub JTasHistFlush()
        ' ---------------------------------------------------------------------------
        ' This sub reformats time keeping data into XML sentences and write the data to a file.
        ' ---------------------------------------------------------------------------
        Dim fileRecord As String = ""
        '
        '
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTInvHist.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTInvHist.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            '
            fileRecord = "<InvHistory>"
            writeFile.WriteLine(fileRecord)
            ' Get a collection of the keys. 
            Dim key As ICollection = slJTasIH.Keys
            Dim k As String
            For Each k In key
                '
                fileRecord = "<ihRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<ihJobID>" & slJTasIH(k).ihJobID & "</ihJobID>"
                writeFile.WriteLine(fileRecord)
                '

                If slJTasIH(k).ihSubID <> "" Then
                    fileRecord = "<ihSubID>" & slJTasIH(k).ihSubID & "</ihSubID>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihInvDate <> "" Then
                    fileRecord = "<ihInvDate>" & slJTasIH(k).ihInvDate & "</ihInvDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihInvNum <> "" Then
                    fileRecord = "<ihInvNum>" & slJTasIH(k).ihInvNum & "</ihInvNum>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihLab <> "0" Then
                    fileRecord = "<ihLab>" & slJTasIH(k).ihLab & "</ihLab>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihMat <> "0" Then
                    fileRecord = "<ihMat>" & slJTasIH(k).ihMat & "</ihMat>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihSub <> "0" Then
                    fileRecord = "<ihSub>" & slJTasIH(k).ihSub & "</ihSub>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihIH <> "0" Then
                    fileRecord = "<ihIH>" & slJTasIH(k).ihIH & "</ihIH>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAdj <> "0" Then
                    fileRecord = "<ihAdj>" & slJTasIH(k).ihAdj & "</ihAdj>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihInvTl <> "" Then
                    fileRecord = "<ihInvTl>" & slJTasIH(k).ihInvTl & "</ihInvTl>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihEstMrgn <> "" Then
                    fileRecord = "<ihEstMrgn>" & slJTasIH(k).ihEstmrgn & "</ihEstMrgn>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAUPInv <> "0" Then
                    fileRecord = "<ihAUPInv>" & slJTasIH(k).ihAUPInv & "</ihAUPInv>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihWorkLocState <> "" Then
                    fileRecord = "<ihWorkLocState>" & slJTasIH(k).ihWorkLocState & "</ihWorkLocState>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihTaxExempt <> True Then
                    fileRecord = "<ihTaxExempt>" & slJTasIH(k).ihTaxExempt & "</ihTaxExempt>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihTaxableSales <> "" Then
                    fileRecord = "<ihTaxableSales>" & slJTasIH(k).ihTaxableSales & "</ihTaxableSales>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihTaxAmount <> "" Then
                    fileRecord = "<ihTaxAmount>" & slJTasIH(k).ihTaxAmount & "</ihTaxAmount>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihFile <> "" Then
                    fileRecord = "<ihFile>" & slJTasIH(k).ihFile & "</ihFile>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihFiledetails <> "" Then
                    fileRecord = "<ihFiledetails>" & slJTasIH(k).ihFiledetails & "</ihFiledetails>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihCostMeth <> "" Then
                    fileRecord = "<ihCostMeth>" & slJTasIH(k).ihCostMeth & "</ihCostMeth>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAUPTKsw <> "" Then
                    fileRecord = "<ihAUPTKsw>" & slJTasIH(k).ihAUPTKsw & "</ihAUPTKsw>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAUPMATsw <> "" Then
                    fileRecord = "<ihAUPMATsw>" & slJTasIH(k).ihAUPMATsw & "</ihAUPMATsw>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAUPSERsw <> "" Then
                    fileRecord = "<ihAUPSERsw>" & slJTasIH(k).ihAUPSERsw & "</ihAUPSERsw>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihDoNotExpt <> "" Then
                    fileRecord = "<ihDoNotExpt>" & slJTasIH(k).ihDoNotExpt & "</ihDoNotExpt>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihAcctExptDate <> "" Then
                    fileRecord = "<ihAcctExptDate>" & slJTasIH(k).ihAcctExptDate & "</ihAcctExptDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If slJTasIH(k).ihEmailedDate <> "" Then
                    fileRecord = "<ihEmailedDate>" & slJTasIH(k).ihEmailedDate & "</ihEmailedDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "</ihRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            fileRecord = "</InvHistory>"
            writeFile.WriteLine(fileRecord)
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTInvHist.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTInvHist.Bkup")
        End If
        ' ------------------------------------------------------------------------

    End Sub
    ''' <summary>
    ''' This sub loads all data necessary  into sl's and prepare the JTas panel for presentation. This sub is requested for a single customer/job normally. If combined invoicing is chosen, all the items pending invoicing for this customer will be loaded. It is also used to review invoice history.
    ''' </summary>
    ''' <param name="tmpJobID">customer that will go through invoice process.</param>
    ''' <param name="tmpSubID">job that is to be invoiced</param>
    ''' <param name="tmpJobDesc">job description of job to be invoiced.</param>
    ''' <param name="tmpJobPricing">pricing method</param>
    ''' <param name="tmpLayer">JYTas is a tabbed panel. This element contains the name of the tab layer 
    '''                        that should be initially displayed when JTas panel is displayed.</param>
    Public Sub JTasInit(tmpJobID As String, tmpSubID As String, tmpJobDesc As String,
                        tmpJobPricing As String, tmpLayer As String)
        ' -----------------------------------------------------------------------------------
        ' Save arguments - - - necessary if line item corrections are made.
        ' -----------------------------------------------------------------------------------
        JTasJobID = tmpJobID
        JTasSubID = tmpSubID
        JTasJobDesc = tmpJobDesc
        JtasJobPricing = tmpJobPricing
        JTasLayer = tmpLayer
        '
        btnJTasLineItemCorr.Visible = False
        '
        ' -----------------------------------------------------------------------------------
        ' init sub will retrieve all data necessary for detailed activity review. It
        ' desired, invoicing routines can be kicked off.
        ' -----------------------------------------------------------------------------------
        txtJTasJobID.Text = tmpJobID
        txtJTasSubID.Text = tmpSubID
        txtJTasJobDesc.Text = tmpJobDesc
        '
        mtxtJTasCutOffDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        mtxtJTasInvDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)

        If tmpJobPricing = "AUP" Then
            txtJTasPricing.Text = "Agreed Upon Price"
        Else
            If tmpJobPricing = "CPWD" Then
                txtJTasPricing.Text = "Cost+ with Deposits"
            Else
                txtJTasPricing.Text = "Cost+"
            End If
        End If
        '
        ' Loop to get unbilled activity from specific classes.
        '
        slJTasTK.Clear()
        slJTasNLCMat.Clear()
        slJtasNLCSub.Clear()
        slJTasTKKeys.Clear()
        slJTasNLCMatKeys.Clear()
        slJTasNLCSubKeys.Clear()
        slJTasAUP.Clear()
        slJTasAUPKeys.Clear()
        slJTasInvJobs.Clear()
        slJTasIJTls.Clear()

        '
        Dim tmpJobID2 As String = tmpJobID
        Dim tmpSubID2 As String = tmpSubID
        cboxJTasDeact.Checked = False
        If tmpJobPricing = "MSCOMB" Then
            tmpSubID2 = "MSCOMB"
            ' no option to DEACT if this is combined invoice.
            cboxJTasDeact.Visible = False
            cboxJTasDeact.Checked = False
        Else
            cboxJTasDeact.Visible = True
            cboxJTasDeact.Checked = False
            Dim tmpjimfPOS As String = Nothing
            Dim tmpjimJOBID As String = tmpJobID
            Dim tmpjimSubID As String = tmpSubID
            Dim tmpjimJobType As String = Nothing
            Dim tmpjimDeactDate As String = Nothing
            JTjim.JTjimFindFunc(tmpjimfPOS, tmpjimJOBID, tmpjimSubID, tmpjimJobType, tmpjimDeactDate)
        End If
        '
        ' AUP
        '
        Dim tmpSLkey As String = "FIRST"
        Dim tmpSLaupItem As JTaup.JTaupInvStru = Nothing
        ' Sequence # to append to work date to keep key unique
        Dim tmpslKeySeq As Integer = 10
        Do While JTaup.JTAUPasExt(tmpJobID2, tmpSubID2, tmpSLkey, tmpSLaupItem) = "DATA"
            Dim tmpKeyDate As String = tmpSLaupItem.JTaupIDate.Substring(6, 4) &
                                      tmpSLaupItem.JTaupIDate.Substring(0, 2) &
                                      tmpSLaupItem.JTaupIDate.Substring(3, 2)

            Dim tmpJTasKey As String = tmpKeyDate & Str(tmpslKeySeq).Trim(" ")

            slJTasAUP.Add(tmpJTasKey, tmpSLaupItem)
            slJTasAUPKeys.Add(tmpJTasKey, tmpSLkey)
            tmpslKeySeq += 1
            ' ------------------------------------------------------------------
            ' call func to make sure this item included in sl for this invoice
            ' In an MSCOMB scenario, each job/subjob that has activity must have
            ' an entry in this SL.
            ' ------------------------------------------------------------------
            JTasIJUpdt(tmpSLaupItem.JTaupIJob, tmpSLaupItem.JTaupISubJob)
        Loop
        '
        ' TK
        '
        tmpSLkey = "FIRST"
        Dim tmpSLtkItem As JTtk.JTtkArrayStructure = Nothing
        ' Sequence # to append to work date to keep key unique
        tmpslKeySeq = 10
        Do While JTtk.JTTKasExt(tmpJobID2, tmpSubID2, tmpSLkey, tmpSLtkItem) = "DATA"
            Dim tmpKeyDate As String = tmpSLtkItem.JTtkWorkDate.Substring(6, 4) &
                                      tmpSLtkItem.JTtkWorkDate.Substring(0, 2) &
                                      tmpSLtkItem.JTtkWorkDate.Substring(3, 2)

            Dim tmpJTasKey As String = tmpKeyDate & Str(tmpslKeySeq).Trim(" ")

            slJTasTK.Add(tmpJTasKey, tmpSLtkItem)
            slJTasTKKeys.Add(tmpJTasKey, tmpSLkey)
            tmpslKeySeq += 1
            '
            ' call func to make sure this item included in sl for this invoice
            '
            JTasIJUpdt(tmpSLtkItem.JTtkJobID, tmpSLtkItem.JTtkSubID)
        Loop
        '
        ' NLC Mat & Sub
        '
        tmpSLkey = "FIRST"
        Dim tmpSLnlcItem As JTnlc.JTnlcStructure = Nothing
        ' Sequence # to append to work date to keep key unique
        tmpslKeySeq = 10

        Do While JTnlc.JTNLCasExt(tmpJobID2, tmpSubID2, tmpSLkey, tmpSLnlcItem) = "DATA"

            Dim tmpKeyDate As String = tmpSLnlcItem.InvDate.Substring(6, 4) &
                                      tmpSLnlcItem.InvDate.Substring(0, 2) &
                                      tmpSLnlcItem.InvDate.Substring(3, 2)
            Dim tmpJTasKey = tmpKeyDate & Str(tmpslKeySeq)
            If tmpSLnlcItem.MorS = "M" Or tmpSLnlcItem.MorS = "IS" Or tmpSLnlcItem.MorS = "IH" Then
                slJTasNLCMat.Add(tmpJTasKey, tmpSLnlcItem)
                slJTasNLCMatKeys.Add(tmpJTasKey, tmpSLkey)
            Else
                slJtasNLCSub.Add(tmpJTasKey, tmpSLnlcItem)
                slJTasNLCSubKeys.Add(tmpJTasKey, tmpSLkey)
            End If
            tmpslKeySeq += 1
            '
            ' call func to make sure this item included in sl for this invoice
            '
            JTasIJUpdt(tmpSLnlcItem.JobID, tmpSLnlcItem.SubID)
        Loop
        '
        '
        '
        JTasFilllvs()
        '
        Select Case tmpLayer
            Case "Lab"
                tabJTas.SelectedTab = tabLabor
            Case "Mat"
                tabJTas.SelectedTab = tabMaterial
            Case "Sub"
                tabJTas.SelectedTab = tabSubContrctr
            Case "Hist"
                tabJTas.SelectedTab = tabJobPandL
            Case "AUP"
                tabJTas.SelectedTab = tabAUP
        End Select

        Me.TopMost = True
        ' code to change width to make sub job visible on lv's
        If tmpJobPricing = "MSCOMB" Then
            JTaslvLab.Columns(8).Width = 80
            JTaslvMat.Columns(8).Width = 80
            JTaslvSub.Columns(8).Width = 80
            JTaslvAUP.Columns(5).Width = 80
            lvJTasInvHist.Columns(10).Width = 80
        Else
            JTaslvLab.Columns(8).Width = 0
            JTaslvMat.Columns(8).Width = 0
            JTaslvSub.Columns(8).Width = 0
            JTaslvAUP.Columns(5).Width = 0
            lvJTasInvHist.Columns(10).Width = 0
        End If
        '
        JTstart.JTstartMenuOff()
        Me.MdiParent = JTstart
        BtnJTasInvoice.Enabled = True
        Me.Show()
    End Sub
    ''' <summary>This sub is queued by clicking the cancel button on JTas. It clears all sl's and return control to JTMainMenu panel.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.TopMost = True
        Me.Hide()
        '
        slJTasTKKeys.Clear()
        slJTasNLCMat.Clear()
        slJTasNLCMatKeys.Clear()
        slJtasNLCSub.Clear()
        slJTasNLCSubKeys.Clear()
        '
        JTaup.JTaupRemoveDanglingDepositReversals()
        '
        If JTinv.JTinvCameFromHere = True Then
            JTinv.JTinvQueueInvoice()
        Else
            JTMainMenu.JTMMinit()
        End If

    End Sub
    ''' <summary>
    ''' This sub fill all the lv's associated with the process and the presentation of JTas. 
    ''' It calls TK, NLC, AUP routines and asks for data appropriate for this customer/job. 
    ''' It also creates tables of totals.
    ''' </summary>
    Sub JTasFilllvs()
        ' ------------------------------------------------------------------------------
        ' Load Data from Arrays into LVs
        ' ------------------------------------------------------------------------------
        '
        ' Labor item load
        '
        '
        Dim tmpTotHours As Double = 0
        Dim tmpTotDol As Double = 0
        Dim tmpTotMrgn As Double = 0
        '
        JTaslvLab.Items.Clear()
        Dim itmBuild(9) As String
        Dim lvLine As New ListViewItem
        Dim key As ICollection = slJTasTK.Keys
        Dim k As String
        '
        For Each k In key
            '
            Dim tmpKey As String = slJTasTK(k).JTtkJobID & slJTasTK(k).JTtkSubID
            Dim tmpExcludeFromTotals As Boolean = False
            ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
            If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJtksw = "X" Then
                tmpExcludeFromTotals = True
            End If
            '
            itmBuild(0) = slJTasTK(k).JTtkWorkDate
            itmBuild(1) = slJTasTK(k).JTtkLabCat
            itmBuild(2) = slJTasTK(k).JTtkActDescrpt
            itmBuild(3) = String.Format("{0:0.00}", Val(slJTasTK(k).JTtkHours))

            itmBuild(4) = String.Format("{0:0.00}", Val(slJTasTK(k).JTtkDolAmt))
            If tmpExcludeFromTotals = False Then
                tmpTotHours += Val(itmBuild(3))
                tmpTotDol += Val(itmBuild(4))
            End If
            itmBuild(5) = slJTasTK(k).JTtkEmpID

            Dim tmpPOS As Integer = 0
            Dim tmpID As String = ""
            Dim tmpName As String = ""
            Dim tmpLabCat As String = ""
            Dim tmpPayMeth As String = ""
            Dim tmpPRID As String = ""
            Dim tmpPayRate As String = ""
            Dim tmpEmail As String = ""
            Dim tmpTermDate As String = ""
            Dim tmpMargin As Double = 0
            Do While JThr.JThrNewReadFunc(tmpPOS, tmpID, tmpName, tmpLabCat, tmpPayMeth, tmpPRID,
                                          tmpPayRate, tmpEmail, tmpTermDate) = "DATA"

                If tmpID = slJTasTK(k).JTtkEmpID Then
                    If tmpPayMeth = "W2" Then
                        tmpMargin = Val(slJTasTK(k).JTtkDolAmt) - (Val(tmpPayRate) * Val(slJTasTK(k).JTtkHours) *
                            (Val(JTVarMaint.PayrollTaxLoadPercent) + 100) / 100)
                    Else
                        tmpMargin = Val(slJTasTK(k).JTtkDolAmt) - (Val(tmpPayRate) * Val(slJTasTK(k).JTtkHours))
                    End If
                    itmBuild(6) = String.Format("{0:0.00}", Val(tmpMargin))
                    If tmpExcludeFromTotals = False Then
                        tmpTotMrgn += tmpMargin
                    End If
                    Exit Do
                End If

                tmpPOS += 1

            Loop
            '      
            itmBuild(7) = slJTasTK(k).JTtkJobID
            itmBuild(8) = slJTasTK(k).JTtkSubID
            '
            lvLine = New ListViewItem(itmBuild)
            JTaslvLab.Items.Add(lvLine)
            '
        Next

        txtJTasLabHrs.Text = String.Format("{0:0.00}", tmpTotHours)
        txtJtasLabTlDol.Text = String.Format("{0:0.00}", tmpTotDol)
        txtJTasLabMrgnDol.Text = String.Format("{0:0.00}", tmpTotMrgn)
        '
        txtJTasASLabUnb.Text = String.Format("{0:0.00}", tmpTotDol)
        txtJTasASLabSel.Text = String.Format("{0:0.00}", tmpTotDol)

        '
        ' Sub item load
        '
        Dim tmpSubtlCost As Double = 0
        Dim tmpSubtlInv As Double = 0
        Dim tmpSubtlMrgn As Double = 0

        '
        JTaslvSub.Items.Clear()
        Dim itmBuild2(9) As String
        Dim lvLine2 As New ListViewItem
        Dim key2 As ICollection = slJtasNLCSub.Keys

        For Each k In key2
            '
            Dim tmpKey As String = slJtasNLCSub(k).JobID & slJtasNLCSub(k).SubID
            Dim tmpExcludeFromTotals As Boolean = False
            ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
            '
            ' check to see if there is a supplier name is to identifiy adjustments ... no supplier = adjustment.
            '
            If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJSERsw = "X" _
                And slJtasNLCSub(k).Supplier <> "" Then
                tmpExcludeFromTotals = True
            End If
            '
            itmBuild2(0) = slJtasNLCSub(k).InvDate
            itmBuild2(1) = slJtasNLCSub(k).Supplier
            itmBuild2(2) = slJtasNLCSub(k).Descrpt
            itmBuild2(3) = String.Format("{0:0.00}", Val(slJtasNLCSub(k).SupCost))

            itmBuild2(4) = String.Format("{0:0.00}", Val(slJtasNLCSub(k).TBInvd))

            itmBuild2(5) = slJtasNLCSub(k).SupInvNum
            itmBuild2(6) = String.Format("{0:0.00}", Val(slJtasNLCSub(k).TBInvd) - Val(slJtasNLCSub(k).SupCost))
            If tmpExcludeFromTotals = False Then
                tmpSubtlCost += Val(slJtasNLCSub(k).SupCost)
                tmpSubtlInv += Val(slJtasNLCSub(k).TBInvd)
                tmpSubtlMrgn += Val(slJtasNLCSub(k).TBInvd) - Val(slJtasNLCSub(k).SupCost)
            End If
            '
            itmBuild2(7) = slJtasNLCSub(k).JobID
            itmBuild2(8) = slJtasNLCSub(k).SubID
            '
            lvLine2 = New ListViewItem(itmBuild2)
            JTaslvSub.Items.Add(lvLine2)
            '
        Next
        '  
        txtJTasSubCstDol.Text = String.Format("{0:0.00}", tmpSubtlCost)
        txtJTasSubCstInvDol.Text = String.Format("{0:0.00}", tmpSubtlInv)
        txtJTasSubMrgnDol.Text = String.Format("{0:0.00}", tmpSubtlMrgn)
        '
        txtJTasASSubUnb.Text = String.Format("{0:0.00}", tmpSubtlInv)
        txtJTasASSubSel.Text = String.Format("{0:0.00}", tmpSubtlInv)
        '
        ' Mat item load
        '
        '
        Dim tmpMattlCost As Double = 0
        Dim tmpMattlInv As Double = 0
        Dim tmpMattlMrgn As Double = 0
        '
        JTaslvMat.Items.Clear()
        Dim itmBuild3(9) As String
        Dim lvLine3 As New ListViewItem
        Dim key3 As ICollection = slJTasNLCMat.Keys

        For Each k In key3
            '
            Dim tmpKey As String = slJTasNLCMat(k).JobID & slJTasNLCMat(k).SubID
            Dim tmpExcludeFromTotals As Boolean = False
            ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
            If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJMATsw = "X" Then
                tmpExcludeFromTotals = True
            End If
            '
            itmBuild3(0) = slJTasNLCMat(k).InvDate
            itmBuild3(1) = slJTasNLCMat(k).Supplier
            itmBuild3(2) = slJTasNLCMat(k).Descrpt
            itmBuild3(3) = String.Format("{0:0.00}", Val(slJTasNLCMat(k).SupCost))
            tmpMattlCost += Val(slJTasNLCMat(k).SupCost)
            itmBuild3(4) = String.Format("{0:0.00}", Val(slJTasNLCMat(k).TBInvd))

            itmBuild3(5) = slJTasNLCMat(k).SupInvNum
            itmBuild3(6) = String.Format("{0:0.00}", Val(slJTasNLCMat(k).TBInvd) - Val(slJTasNLCMat(k).SupCost))
            If tmpExcludeFromTotals = False Then
                tmpMattlInv += Val(slJTasNLCMat(k).TBInvd)
                tmpMattlMrgn = tmpMattlMrgn + Val(slJTasNLCMat(k).TBInvd) - Val(slJTasNLCMat(k).SupCost)
            End If
            '
            itmBuild3(7) = slJTasNLCMat(k).JobID
            itmBuild3(8) = slJTasNLCMat(k).SubID
            '
            lvLine3 = New ListViewItem(itmBuild3)
            JTaslvMat.Items.Add(lvLine3)
            '
        Next
        '
        txtJTasMatCostDol.Text = String.Format("{0:0.00}", tmpMattlCost)
        txtJTasMatCstInvDol.Text = String.Format("{0:0.00}", tmpMattlInv)
        txtJTasMatMrgnDol.Text = String.Format("{0:0.00}", tmpMattlMrgn)
        '
        txtJTasASMatUnb.Text = String.Format("{0:0.00}", tmpMattlInv)
        txtJTasASMatSel.Text = String.Format("{0:0.00}", tmpMattlInv)
        '
        '
        ' AUP item load
        '
        '
        Dim tmpAUPtlInv As Double = 0
        '
        JTaslvAUP.Items.Clear()
        Dim itmBuild5(6) As String
        Dim lvLine5 As New ListViewItem
        Dim key5 As ICollection = slJTasAUP.Keys
        '
        For Each k In key5
            itmBuild5(0) = slJTasAUP(k).JTaupIDate
            itmBuild5(1) = slJTasAUP(k).JTaupIDesc
            itmBuild5(2) = String.Format("{0:0.00}", Val(slJTasAUP(k).JTaupIAmt))
            tmpAUPtlInv += Val(slJTasAUP(k).JTaupIAmt)
            itmBuild5(3) = slJTasAUP(k).JTaupIFinal
            itmBuild5(4) = slJTasAUP(k).JTaupIJob
            itmBuild5(5) = slJTasAUP(k).JTaupISubJob
            '
            lvLine5 = New ListViewItem(itmBuild5)
            JTaslvAUP.Items.Add(lvLine5)
            '
        Next
        '
        txtJTasAUPInvDol.Text = String.Format("{0:0.00}", tmpAUPtlInv)
        '
        txtJTasASaupUnb.Text = String.Format("{0:0.00}", tmpAUPtlInv)
        txtJTasASaupSel.Text = String.Format("{0:0.00}", tmpAUPtlInv)

        '

        txtJTasASTlUnb.Text = String.Format("{0:0.00}", tmpMattlInv + tmpSubtlInv + tmpTotDol + tmpAUPtlInv)
        txtJTasASTlSel.Text = String.Format("{0:0.00}", tmpMattlInv + tmpSubtlInv + tmpTotDol + tmpAUPtlInv)

        '
        '
        ' Load Invoice History
        '
        Dim tmpIHLab As Double = 0
        Dim tmpIHMat As Double = 0
        Dim tmpIHSub As Double = 0
        Dim tmpIHInvTl As Double = 0
        Dim tmpIHEstMrgn As Double = 0
        Dim tmpIHAUPInvTl As Double = 0
        '
        Dim itmBuild4(11) As String
        lvJTasInvHist.Items.Clear()
        Dim lvLine4 As New ListViewItem
        Dim key4 As ICollection = slJTasIH.Keys
        For Each k In key4
            If slJTasIH(k).ihJobID = txtJTasJobID.Text And
                (slJTasIH(k).ihSubID = txtJTasSubID.Text Or txtJTasSubID.Text = "MSCOMB") Then
                '
                itmBuild4(0) = slJTasIH(k).ihInvDate
                itmBuild4(1) = slJTasIH(k).ihINVNum
                If slJTasIH(k).ihInvTl = "VOIDED" Then
                    itmBuild4(2) = Nothing
                    itmBuild4(3) = Nothing
                    itmBuild4(4) = Nothing
                    itmBuild4(5) = "VOIDED"
                Else

                    itmBuild4(2) = String.Format("{0:0.00}", Val(slJTasIH(k).ihLab))
                    tmpIHLab += Val(slJTasIH(k).ihLab)
                    itmBuild4(3) = String.Format("{0:0.00}", Val(slJTasIH(k).ihMat) + Val(slJTasIH(k).ihIH))
                    tmpIHMat += Val(slJTasIH(k).ihMat) + Val(slJTasIH(k).ihIH)
                    itmBuild4(4) = String.Format("{0:0.00}", Val(slJTasIH(k).ihSub) + Val(slJTasIH(k).ihAdj))
                    tmpIHSub = tmpIHSub + Val(slJTasIH(k).ihSub) + Val(slJTasIH(k).ihAdj)
                    itmBuild4(5) = String.Format("{0:0.00}", Val(slJTasIH(k).ihInvTl))
                    tmpIHInvTl += Val(slJTasIH(k).ihInvTl)
                    If slJTasIH(k).ihEstMrgn <> "" Then
                        itmBuild4(6) = String.Format("{0:0.00}", Val(slJTasIH(k).ihEstMrgn))
                        tmpIHEstMrgn += Val(slJTasIH(k).ihEstMrgn)
                    End If
                    If Val(slJTasIH(k).ihAUPInv) <> 0 Then
                        itmBuild4(7) = String.Format("{0:0.00}", Val(slJTasIH(k).ihAUPInv))
                        tmpIHAUPInvTl += Val(slJTasIH(k).ihAUPInv)
                    End If
                    If Val(slJTasIH(k).ihAUPInv) <> 0 Then
                        '
                        Dim tmpAUPNet As Double = Val(slJTasIH(k).ihInvTl) _
                        - (Val(slJTasIH(k).ihLab) + Val(slJTasIH(k).ihMat) + Val(slJTasIH(k).ihSub) +
                        Val(slJTasIH(k).ihAdj) + Val(slJTasIH(k).ihIH) - Val(slJTasIH(k).ihEstMrgn))
                        '
                        itmBuild4(8) = String.Format("{0:0.00}", tmpAUPNet)
                    End If
                End If
                '
                itmBuild4(9) = slJTasIH(k).ihJobID
                itmBuild4(10) = slJTasIH(k).ihSubID
                '
                lvLine4 = New ListViewItem(itmBuild4)
                lvJTasInvHist.Items.Add(lvLine4)
            End If

        Next
        txtJTasIHLabTl.Text = String.Format("{0:0.00}", tmpIHLab)
        txtJTasIHMatTl.Text = String.Format("{0:0.00}", tmpIHMat)
        txtJTasIHSubTl.Text = String.Format("{0:0.00}", tmpIHSub)
        txtJTasIHInvTl.Text = String.Format("{0:0.00}", tmpIHInvTl)
        txtJTasIHEstMrgnTl.Text = String.Format("{0:0.00}", tmpIHEstMrgn)
        txtJTasIHAUPInvTl.Text = String.Format("{0:0.00}", tmpIHAUPInvTl)
        '
    End Sub
    '
    ''' <summary>
    ''' This is one of a series of subs that enables realtime updating of totals. This one
    '''        is for subcontractors. It is activated by mouse movement (if the mouse has been over
    '''        the area and leaves, it is initiatied). It call a routine to recalculate totals.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvSub_MouseLeave(sender As Object, e As EventArgs) Handles JTaslvSub.MouseLeave
        JTaslvSub_Update_Totals()
    End Sub
    ''' <summary>This sub updates the totals for subcontractors on JTas.</summary>
    Private Sub JTaslvSub_Update_Totals()
        ' --------------------------------------------------------------------
        ' Recalculates items totals for subcontractor items pending invoicing.
        ' --------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpTlAmt As Double = 0
        Dim tmpSelAmt As Double = 0
        '
        If JTaslvSub.Items.Count > 0 Then
            Do While tmpSub < JTaslvSub.Items.Count
                '
                Dim tmpKey As String = JTaslvSub.Items(tmpSub).SubItems(7).Text & JTaslvSub.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJSERsw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpTlAmt += Val(JTaslvSub.Items(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvSub.CheckedItems.Count > 0 Then
            Do While tmpSub < JTaslvSub.CheckedItems.Count
                '
                Dim tmpKey As String = JTaslvSub.Items(tmpSub).SubItems(7).Text & JTaslvSub.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJSERsw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpSelAmt += Val(JTaslvSub.CheckedItems(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If

        txtJTasASSubSel.Text = String.Format("{0:0.00}", tmpTlAmt - tmpSelAmt)
        txtJTasASSubUnb.Text = String.Format("{0:0.00}", tmpTlAmt)

        JTasReCalcTls()
    End Sub
    '
    ''' <summary>Same as JTaslvSub_MouseLeave but for material.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvMat_MouseLeave(sender As Object, e As EventArgs) Handles JTaslvMat.MouseLeave
        JTaslvMat_Update_Totals()
    End Sub
    ''' <summary>Recalc and update material totals on JTas.</summary>
    Private Sub JTaslvMat_Update_Totals()
        ' --------------------------------------------------------------------
        ' Recalculates items totals for Material items pending invoicing.
        ' --------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpTlAmt As Double = 0
        Dim tmpSelAmt As Double = 0
        '
        If JTaslvMat.Items.Count > 0 Then
            Do While tmpSub < JTaslvMat.Items.Count
                '
                Dim tmpKey As String = JTaslvMat.Items(tmpSub).SubItems(7).Text & JTaslvMat.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJMATsw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpTlAmt += Val(JTaslvMat.Items(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvMat.CheckedItems.Count > 0 Then
            Do While tmpSub < JTaslvMat.CheckedItems.Count
                '
                Dim tmpKey As String = JTaslvMat.Items(tmpSub).SubItems(7).Text & JTaslvMat.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJMATsw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpSelAmt += Val(JTaslvMat.CheckedItems(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If

        txtJTasASMatSel.Text = String.Format("{0:0.00}", tmpTlAmt - tmpSelAmt)
        txtJTasASMatUnb.Text = String.Format("{0:0.00}", tmpTlAmt)

        JTasReCalcTls()
    End Sub
    '
    ''' <summary>Same as JTaslvSub but for labor.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvLab_MouseLeave(sender As Object, e As EventArgs) Handles JTaslvLab.MouseLeave
        JTaslvLab_Update_Totals()
    End Sub
    ''' <summary>Recalc and update labor totals for JTas.</summary>
    Private Sub JTaslvLab_Update_Totals()
        ' --------------------------------------------------------------------
        ' Recalculates items totals for Labor items pending invoicing.
        ' --------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpTlAmt As Double = 0
        Dim tmpSelAmt As Double = 0
        '


        If JTaslvLab.Items.Count > 0 Then
            Do While tmpSub < JTaslvLab.Items.Count
                '
                Dim tmpKey As String = JTaslvLab.Items(tmpSub).SubItems(7).Text & JTaslvLab.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJtksw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpTlAmt += Val(JTaslvLab.Items(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvLab.CheckedItems.Count > 0 Then
            Do While tmpSub < JTaslvLab.CheckedItems.Count
                '
                Dim tmpKey As String = JTaslvLab.Items(tmpSub).SubItems(7).Text & JTaslvLab.Items(tmpSub).SubItems(8).Text
                Dim tmpExcludeFromTotals As Boolean = False
                ' set switch to queue inclusion of exclusion from totals. For Aup jobs.
                If slJTasInvJobs(tmpKey).JTasIJCostMeth = "AUP" And slJTasInvJobs(tmpKey).JTasIJtksw = "X" Then
                    tmpExcludeFromTotals = True
                End If
                '
                If tmpExcludeFromTotals = False Then
                    tmpSelAmt += Val(JTaslvLab.CheckedItems(tmpSub).SubItems(4).Text)
                End If
                tmpSub += 1
            Loop
        End If

        txtJTasASLabSel.Text = String.Format("{0:0.00}", tmpTlAmt - tmpSelAmt)
        txtJTasASLabUnb.Text = String.Format("{0:0.00}", tmpTlAmt)
        JTasReCalcTls()

    End Sub
    '
    ''' <summary>Same as JTaslvSub_MouseLeave but for AUP.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvAUP_MouseLeave(sender As Object, e As EventArgs) Handles JTaslvAUP.MouseLeave
        JTaslvAUP_Update_Totals()
    End Sub
    ''' <summary>Recalc and updates AUP totals for JTas.</summary>
    Private Sub JTaslvAUP_Update_Totals()
        ' --------------------------------------------------------------------
        ' Recalculates items totals for AUP items pending invoicing.
        ' --------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpTlAmt As Double = 0
        Dim tmpSelAmt As Double = 0
        '
        If JTaslvAUP.Items.Count > 0 Then
            Do While tmpSub < JTaslvAUP.Items.Count
                '
                tmpTlAmt += Val(JTaslvAUP.Items(tmpSub).SubItems(2).Text)
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvLab.CheckedItems.Count > 0 Then
            Do While tmpSub < JTaslvLab.CheckedItems.Count
                tmpSelAmt += Val(JTaslvAUP.CheckedItems(tmpSub).SubItems(2).Text)
                tmpSub += 1
            Loop
        End If
        '
        txtJTasASaupSel.Text = String.Format("{0:0.00}", tmpTlAmt - tmpSelAmt)
        txtJTasASaupUnb.Text = String.Format("{0:0.00}", tmpTlAmt)
        JTasReCalcTls()
        '
    End Sub
    '
    ''' <summary>This sub recalc and updates the grand totals on JTas. It is called by whatever component has just been recalculated.</summary>
    Private Sub JTasReCalcTls()
        txtJTasASTlSel.Text = String.Format("{0:0.00}", Val(txtJTasASLabSel.Text) + Val(txtJTasASMatSel.Text) _
                                            + Val(txtJTasASSubSel.Text) + Val(txtJTasASaupSel.Text))
        txtJTasASTlUnb.Text = String.Format("{0:0.00}", Val(txtJTasASLabUnb.Text) + Val(txtJTasASMatUnb.Text) _
                                            + Val(txtJTasASSubUnb.Text) + Val(txtJTasASaupUnb.Text))
        ' ---------------------------------------------------------------------------------
        ' Check to see if some of the unbilled activity is being excluded from the invoice.
        ' If true, eliminate the option to deactivate the job.
        ' ---------------------------------------------------------------------------------
        If txtJTasASTlSel.Text <> txtJTasASTlUnb.Text Then
            cboxJTasDeact.Checked = False
            cboxJTasDeact.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' This sub is called by JTmmInit as part of the logic to build an invoice history lv for the customer~job. The sub is called multiple times returning the information for one invoice at a time until no more invoices exist ("END" is returned).
    ''' </summary>
    ''' <param name="tmpJobID">Customer</param>
    ''' <param name="tmpSubID">Job</param>
    ''' <param name="tmpInvDate">Invoice Date is returned ... on first call, "FIRST" is sent as a value to tell sub that this is the beginning of a new series of invoice queries.</param>
    ''' <param name="tmpInvAmt">Amount of invoice is returned.</param>
    Public Function JTasReturnInvHist(ByRef tmpSub As Integer, ByRef tmpJobID As String, ByRef tmpSubID As String, ByRef tmpInvDate As String, ByRef tmpInvAmt As String) As String
        Dim key As ICollection = slJTasIH.Keys
        If tmpInvDate = "FIRST" Then
            tmpSub = 0
            tmpInvDate = ""
        End If
        Do While tmpSub < key.Count
            If slJTasIH(key(tmpSub)).ihJobID = tmpJobID And slJTasIH(key(tmpSub)).ihSubID = tmpSubID Then
                tmpInvDate = slJTasIH(key(tmpSub)).ihInvDate
                tmpInvAmt = slJTasIH(key(tmpSub)).ihInvTl
                tmpSub += 1
                Return "DATA"
            Else
                tmpSub += 1
            End If
        Loop
        Return "END"
    End Function
    ''' <summary>
    ''' This subroutine calls up an image of the PDF invoice for review.
    ''' If the invoice had not been previously emailed it can be now.
    ''' It can also be re-emailed if necessary.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvJTasInvHist_DoubleClick(sender As Object, e As EventArgs) Handles lvJTasInvHist.DoubleClick
        Dim tmpdate As String = lvJTasInvHist.SelectedItems(0).Text
        Dim tmpTlAmt As String = lvJTasInvHist.SelectedItems(0).SubItems(5).Text

        Dim key As ICollection = slJTasIH.Keys
        Dim k As String
        For Each k In key
            If slJTasIH(k).ihJobID = txtJTasJobID.Text And slJTasIH(k).ihSubID = txtJTasSubID.Text And slJTasIH(k).ihINVDate = tmpdate And
                    (Val(slJTasIH(k).ihInvTl) = Val(tmpTlAmt) Or slJTasIH(k).ihInvTl = "VOIDED") Then
                Me.TopMost = False
                ' ------------------------------------------------------------
                ' Code changed for new directory structure. 12/5/2021
                ' ------------------------------------------------------------
                '
                ' ----------------------------------------------------------------
                ' Code inserted to create new directory if a new year is starting.
                ' ----------------------------------------------------------------
                '
                Dim tmpDirChkStub As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" &
                    slJTasIH(k).ihInvDate.substring(slJTasIH(k).ihInvDate.length - 4, 4)
                If Directory.Exists(tmpDirChkStub) = False Then
                    Directory.CreateDirectory(tmpDirChkStub)
                End If
                Dim InvFileName As String = slJTasIH(k).ihInvDate.substring(slJTasIH(k).ihInvDate.length - 4, 4) &
                    "\" & slJTasIH(k).ihFile
                '
                Dim filename As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & InvFileName
                Dim JTvPDFRprtType As String = "RevInv"
                Dim tmpJobID As String = slJTasIH(k).ihJobID
                Dim tmpSubJobID As String = slJTasIH(k).ihSubID
                Dim tmpInvNum As String = slJTasIH(k).ihInvNum
                Dim tmpInvDate As String = slJTasIH(k).ihInvDate
                Dim tmpEmailDate As String = slJTasIH(k).ihEmailedDate
                ' 
                '
                JTvPDF.JTvPDFDispPDFInv(JTvPDFRprtType, filename, tmpSubJobID, tmpEmailDate,
                                        tmpJobID, tmpInvNum, tmpInvDate)
                Exit Sub
            End If
            '
        Next
        '
    End Sub

    ''' <summary>
    ''' A cutoff for transaction to be included on an invoice is presented on the JTas panel. 
    ''' By default it is the actual date. If this is changed by the operator, transactions after 
    ''' the cutoff date entered will be excluded from this invoice. The sub is initiated when changes 
    ''' have been made to the cutoff date field.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTasCutOffDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTasCutOffDate.LostFocus
        If Not IsDate(mtxtJTasCutOffDate.Text) Then
            ToolTip1.Show("Cutoff Date entered Not valid. Correct And resubmit.", mtxtJTasCutOffDate, 20, -55, 6000)
            Exit Sub
        End If
        Dim tmpCutOffDate As Date = CDate(mtxtJTasCutOffDate.Text)
        Dim tmpSub As Integer = 0
        If JTaslvLab.Items.Count > 0 Then
            Do While tmpSub < JTaslvLab.Items.Count
                Dim tmpActDate As Date = JTaslvLab.Items(tmpSub).SubItems(0).Text
                If tmpActDate > tmpCutOffDate Then
                    JTaslvLab.Items(tmpSub).Checked = True
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvMat.Items.Count > 0 Then
            Do While tmpSub < JTaslvMat.Items.Count
                Dim tmpActDate As Date = JTaslvMat.Items(tmpSub).SubItems(0).Text
                If tmpActDate > tmpCutOffDate Then
                    JTaslvMat.Items(tmpSub).Checked = True
                End If
                tmpSub += 1
            Loop
        End If
        '
        tmpSub = 0
        If JTaslvSub.Items.Count > 0 Then
            Do While tmpSub < JTaslvSub.Items.Count
                Dim tmpActDate As Date = JTaslvSub.Items(tmpSub).SubItems(0).Text
                If tmpActDate > tmpCutOffDate Then
                    JTaslvSub.Items(tmpSub).Checked = True
                End If
                tmpSub += 1
            Loop
        End If
        '
        JTaslvSub_Update_Totals()
        JTaslvLab_Update_Totals()
        JTaslvMat_Update_Totals()

        '
    End Sub

    ''' <summary>This sub is initiated clicking the "produce invoice" button on the JTas panel. 
    '''          It queues the process of building a pdf invoice image.Before starting, it checks 
    '''          to make sure there is activity to be invoiced.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTasInvoice_Click(sender As Object, e As EventArgs) Handles BtnJTasInvoice.Click
        '
        ' Disable button to block any double clicks
        '
        BtnJTasInvoice.Enabled = False
        ' -----------------------------------------------------------------------------
        ' Check to make sure there is activity to print on this invoice.
        ' -----------------------------------------------------------------------------
        If txtJTasPricing.Text.Substring(0, 4) <> "Agre" Then
            ' check to make sure there is activity to invoice --- CST+ job
            If Val(txtJTasASLabSel.Text) = 0 And
                     Val(txtJTasASMatSel.Text) = 0 And
                    Val(txtJTasASSubSel.Text) = 0 And
                    Val(txtJTasASaupSel.Text) = 0 Then
                MsgBox("- - - Invoice cannot be produced. - - -" & vbCrLf &
                       "There are no transactions selected To be invoiced." & vbCrLf &
                       "Post transactions And Then re-Select invoicing For this job." & vbCrLf & vbCrLf &
                       "Invoicing Aborted (As.002)")
                BtnJTasInvoice.Enabled = True
                Exit Sub
            End If
            '
        End If
        '        End If 
        ' ------------------------------------------------------------------------------
        ' Produce Invoice PDF document ... allow review ... and then email the document.
        ' ------------------------------------------------------------------------------
        Me.TopMost = False
        ' ------------------------------------------------------
        ' Setting standard invoice format settings
        ' ------------------------------------------------------
        ' Labor
        JTasInvLabTaskSummary = JTVarMaint.InvLabTaskSummary
        JtasInvLabWorkerDetail = JTVarMaint.InvLabWorkerDetail
        JTasInvLabTotals = JTVarMaint.InvLabTotals
        ' Services
        JTasInvSubSerDetails = JTVarMaint.InvSubSerDetails
        JTasInvSubSerTotals = JTVarMaint.InvSubSerTotals
        JTasInvMatDetails = JTVarMaint.InvMatDetails
        JTasInvMatTotals = JTVarMaint.InvMatTotals
        ' Material/Subcontractor Cost Options
        JTasInvMatShowCost = JTVarMaint.InvMatShowCost
        JTasInvSubShowCost = JTVarMaint.InvSubShowCost
        If JTjim.chkboxJTjimNonStdFrmt.Checked = True Then
            Me.Hide()
            JTif.JTifUpdate(txtJTasJobID.Text, txtJTasSubID.Text)
        Else
            BtnJTasInvoice_Continue()
        End If
    End Sub

    Public Sub BtnJTasInvoice_Continue()
        Me.Show()
        '
        If JTjim.JTjimSendToAcctng(txtJTasJobID.Text, txtJTasSubID.Text) = True Then
            JTasInvNumber = Val(JTVarMaint.InvNumber) + 1
        Else
            JTasInvNumber = Val(JTVarMaint.AltInvNumber) + 1
        End If
        ' go to function to check for duplicate invoice number check
        If JTasChkforDupInvoiceNumber(JTasInvNumber) = True Then
            ' ----------------------------------------------------------------------
            ' Recorded invoice with requested number already exists. ... Trouble ...
            ' Force system back to Main Menu where problem can be resolved.
            ' ----------------------------------------------------------------------
            Me.TopMost = True
            Me.Hide()
            '
            slJTasTKKeys.Clear()
            slJTasNLCMat.Clear()
            slJTasNLCMatKeys.Clear()
            slJtasNLCSub.Clear()
            slJTasNLCSubKeys.Clear()
            '
            JTMainMenu.JTMMinit()
            Exit Sub
        End If
        '
        ' ----------------------------------------------------------------------------------------------
        ' Code changed for new doc directory structure. 12/5/2021
        ' ----------------------------------------------------------------------------------------------
        '
        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & mtxtJTasInvDate.Text.Substring(6, 4)
        If Not System.IO.Directory.Exists(tmpFilePath) Then
            System.IO.Directory.CreateDirectory(tmpFilePath)
        End If
        Dim tmpFileName As String = tmpFilePath & "\" & txtJTasJobID.Text &
        "_" & mtxtJTasInvDate.Text.Substring(6, 4) & mtxtJTasInvDate.Text.Substring(0, 2) & mtxtJTasInvDate.Text.Substring(3, 2) &
        "_" & JTasInvNumber & "D" &
        ".PDF"
        Try
            Using fs As System.IO.FileStream = New FileStream(tmpFileName, FileMode.Create)
                '
                Dim myDoc As Document =
                    New Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 72)
                Dim writer As PdfWriter = PdfWriter.GetInstance(myDoc, fs)
                Dim ev As New JTasitsEvents
                writer.PageEvent = ev
                '
                myDoc.AddTitle("I N V O I C E")
                myDoc.Open()


                ' Build Invoice Header
                JTasBldInvHeader(myDoc)
                ' -------------------------------------------------------------------------------------
                ' Add logic here to loop through slJTasInvJobs to print all Jobs / Sub-Jobs that
                ' have been selected for this invoice.
                ' -------------------------------------------------------------------------------------
                Dim InvJobsKeys As ICollection = slJTasInvJobs.Keys
                '

                '
                For Each IJK In InvJobsKeys
                    '
                    JTasInvSectionsPrtd = 0
                    '
                    ' ------------------------------------------------------------------------
                    ' Print AUP invoice entries
                    ' ------------------------------------------------------------------------
                    If slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" Then
                        JTasBldAUPSection(myDoc)
                    End If
                    '
                    ' ----------------------------------------------------------------------------
                    ' Print Labor Summary or Detail Sections.
                    ' ----------------------------------------------------------------------------
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                        (slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And slJTasInvJobs(IJK).JtasIJTKsw <> "X") Then
                        ' ------------------------------------------------------------------------
                        ' Build Labor Summary section.
                        ' ------------------------------------------------------------------------
                        If JTasInvLabTaskSummary = True Then
                            JTasBldLabTskSummarySection(myDoc)
                        End If
                        '
                        ' ---------------------------------------------------
                        ' Build Detailed labor by date.
                        ' ---------------------------------------------------
                        If JtasInvLabWorkerDetail = True Or
                        JTasInvLabTotals = True Then
                            tmpSub = 0
                            SectionDone = False
                            JTasBldLabSection(myDoc, tmpSub)
                        End If
                    Else
                        ' code added to update unposted amounts
                        Dim tmpChgAmt As Double = 0
                        Dim tmpMrgnAmt As Double = 0
                        tmpSub = 0
                        Do While tmpSub < JTaslvLab.Items.Count
                            If JTaslvLab.Items(tmpSub).Checked = False Then
                                If (slJTasInvJobs(IJK).JTasIJJobID = JTaslvLab.Items(tmpSub).SubItems(7).Text And
                                    slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvLab.Items(tmpSub).SubItems(8).Text) Then
                                    tmpChgAmt += Val(JTaslvLab.Items(tmpSub).SubItems(4).Text)
                                    tmpMrgnAmt += Val(JTaslvLab.Items(tmpSub).SubItems(6).Text)
                                End If
                            End If
                            tmpSub += 1
                        Loop
                        If tmpChgAmt <> 0 Or tmpMrgnAmt <> 0 Then
                            Dim tmpNewItem As JTasIJTstru
                            If slJTasIJTls.ContainsKey(IJK) Then
                                tmpNewItem = slJTasIJTls(IJK)
                                slJTasIJTls.Remove(IJK)
                            Else
                                JTasIJTlsClr(tmpNewItem)
                            End If
                            '
                            tmpNewItem.JTasIJLabNPTl = tmpChgAmt
                            tmpNewItem.JTasIJLabNPMrgn = tmpMrgnAmt
                            '
                            slJTasIJTls.Add(IJK, tmpNewItem)
                            '
                        End If
                    End If
                    ' ---------------------------------------------------
                    ' Build Detailed Subcontrctor Section.
                    ' ---------------------------------------------------
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                        (slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And
                        slJTasInvJobs(IJK).JtasIJSERsw <> "X") Then
                        If JTVarMaint.InvSubSerDetails = True Then
                            '
                            tmpSub = 0
                            SectionDone = False
                            JTasBldSubSection(myDoc, tmpSub, tmpNumOfItems)
                        End If
                    Else
                        ' code added to update unposted amounts
                        Dim tmpChgAmt As Double = 0
                        Dim tmpMrgnAmt As Double = 0
                        tmpSub = 0
                        Do While tmpSub < JTaslvSub.Items.Count
                            If JTaslvSub.Items(tmpSub).Checked = False Then
                                If (slJTasInvJobs(IJK).JTasIJJobID = JTaslvSub.Items(tmpSub).SubItems(7).Text And
                             slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvSub.Items(tmpSub).SubItems(8).Text) Then
                                    tmpChgAmt += Val(JTaslvSub.Items(tmpSub).SubItems(4).Text)
                                    tmpMrgnAmt += Val(JTaslvSub.Items(tmpSub).SubItems(4).Text) _
                                    - Val(JTaslvSub.Items(tmpSub).SubItems(3).Text)
                                End If
                            End If
                            tmpSub += 1
                        Loop
                        If tmpChgAmt <> 0 Or tmpMrgnAmt <> 0 Then
                            Dim tmpNewItem As JTasIJTstru
                            If slJTasIJTls.ContainsKey(IJK) Then
                                tmpNewItem = slJTasIJTls(IJK)
                                slJTasIJTls.Remove(IJK)
                            Else
                                JTasIJTlsClr(tmpNewItem)
                            End If
                            '
                            tmpNewItem.JTasIJSubNPTl = tmpChgAmt
                            tmpNewItem.JTasIJSubNPMrgn = tmpMrgnAmt
                            '
                            slJTasIJTls.Add(IJK, tmpNewItem)
                            '
                        End If
                    End If
                    ' ---------------------------------------------------
                    ' Build Detailed Material Section.
                    ' ---------------------------------------------------
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                        (slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And slJTasInvJobs(IJK).JtasIJMATsw <> "X") Then
                        If JTVarMaint.InvMatDetails = True Then
                            '
                            tmpSub = 0
                            SectionDone = False
                            JTasBldMatSection(myDoc, tmpSub, tmpNumOfItems)
                        End If
                    Else
                        ' code added to update unposted amounts ... for AUP invoices.
                        Dim tmpChgAmt As Double = 0
                        Dim tmpMrgnAmt As Double = 0
                        tmpSub = 0
                        Do While tmpSub < JTaslvMat.Items.Count
                            If JTaslvMat.Items(tmpSub).Checked = False Then
                                If (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpSub).SubItems(7).Text And
                             slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpSub).SubItems(8).Text) Then
                                    tmpChgAmt += Val(JTaslvMat.Items(tmpSub).SubItems(4).Text)
                                    tmpMrgnAmt += Val(JTaslvMat.Items(tmpSub).SubItems(4).Text) _
                                    - Val(JTaslvMat.Items(tmpSub).SubItems(3).Text)
                                End If
                            End If
                            tmpSub += 1
                        Loop
                        If tmpChgAmt <> 0 Or tmpMrgnAmt <> 0 Then
                            Dim tmpNewItem As JTasIJTstru
                            If slJTasIJTls.ContainsKey(IJK) Then
                                tmpNewItem = slJTasIJTls(IJK)
                                slJTasIJTls.Remove(IJK)
                            Else
                                JTasIJTlsClr(tmpNewItem)
                            End If
                            '
                            tmpNewItem.JTasIJMatNPTl = tmpChgAmt
                            tmpNewItem.JTasIJMatNPMrgn = tmpMrgnAmt
                            '
                            slJTasIJTls.Add(IJK, tmpNewItem)
                            '
                        End If

                    End If
                    ' 
                    ' ======================================================================
                    ' Code to print 'IH' items
                    ' ======================================================================
                    '
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                        (slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And slJTasInvJobs(IJK).JtasIJMATsw <> "X") Then
                        If JTVarMaint.InvMatDetails = True Then
                            '
                            tmpSub = 0
                            SectionDone = False
                            JTasBldIHSection(myDoc, tmpSub)
                        End If
                    Else
                        ' code added to update unposted amounts ... for AUP invoices.
                        Dim tmpChgAmt As Double = 0
                        Dim tmpMrgnAmt As Double = 0
                        tmpSub = 0
                        Do While tmpSub < JTaslvMat.Items.Count
                            If JTaslvMat.Items(tmpSub).Checked = False Then
                                If (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpSub).SubItems(7).Text And
                                    slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpSub).SubItems(8).Text) And
                                    JTaslvMat.Items(tmpSub).SubItems(1).Text = "" And
                                    JTaslvMat.Items(tmpSub).SubItems(5).Text = "" Then
                                    '
                                    tmpChgAmt += Val(JTaslvMat.Items(tmpSub).SubItems(4).Text)
                                    tmpMrgnAmt += Val(JTaslvMat.Items(tmpSub).SubItems(4).Text) _
                                    - Val(JTaslvMat.Items(tmpSub).SubItems(3).Text)
                                End If
                            End If
                            tmpSub += 1
                        Loop
                        If tmpChgAmt <> 0 Or tmpMrgnAmt <> 0 Then
                            Dim tmpNewItem As JTasIJTstru
                            If slJTasIJTls.ContainsKey(IJK) Then
                                tmpNewItem = slJTasIJTls(IJK)
                                slJTasIJTls.Remove(IJK)
                            Else
                                JTasIJTlsClr(tmpNewItem)
                            End If
                            '
                            tmpNewItem.JTasIJIHNPTl = tmpChgAmt
                            tmpNewItem.jtasIJIHNPMrgn = tmpMrgnAmt
                            '
                            slJTasIJTls.Add(IJK, tmpNewItem)
                            '
                        End If
                    End If
                    '
                    JTasBldAdjSection(myDoc)
                    '
                    ' ------------------------------------------------------------------------------
                    ' Check to see if invoice subtotals are needed (MSCOMB invoice ... this job /subjob
                    ' Section with more than one category of activity). If yes, prepare and print
                    ' section totals.
                    ' ------------------------------------------------------------------------------
                    ' 
                    If slJTasInvJobs.Keys.Count > 1 Then
                        If JTasInvSectionsPrtd > 1 Then
                            '
                            JTasInvAUPTl = slJTasIJTls(IJK).JTasIJAUPTl
                            '
                            If slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And
                            slJTasInvJobs(IJK).JTasIJTKsw = "X" Then
                                JTasInvLabTl = 0
                            Else
                                JTasInvLabTl = slJTasIJTls(IJK).JTasIJLabTl
                            End If
                            '
                            If slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And
                            slJTasInvJobs(IJK).JTasIJSERsw = "X" Then
                                JTasInvSubTl = 0
                            Else
                                JTasInvSubTl = slJTasIJTls(IJK).JTasIJSubTl
                            End If
                            '
                            If slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" And
                            slJTasInvJobs(IJK).JTasIJMATsw = "X" Then
                                JTasInvMatTl = 0
                                JTasInvIHTl = 0
                            Else
                                JTasInvMatTl = slJTasIJTls(IJK).JTasIJMatTl
                                JTasInvIHTl = slJTasIJTls(IJK).JTasIJIHTl
                                '
                                JTasInvAdjTl = slJTasIJTls(IJK).JTasIJAdjTl
                                '
                            End If
                            JTasBldInvSum(myDoc, "SECTION")
                            '
                        End If
                    End If
                    '
                Next IJK
                '
                JTasInvAUPTl = 0
                JTasInvLabTl = 0
                JTasInvSubTl = 0
                JTasInvMatTl = 0
                JTasInvIHTl = 0
                JTasInvAdjTl = 0
                '
                Dim tmpIJTlsKeys As ICollection = slJTasIJTls.Keys
                For Each IJK In tmpIJTlsKeys
                    JTasInvAUPTl += slJTasIJTls(IJK).JTasIJAUPTl
                    JTasInvAUPMrgn = 0
                    '

                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                            slJTasInvJobs(IJK).JTasIJTKsw <> "X" Then
                        JTasInvLabTl += slJTasIJTls(IJK).JTasIJLabTl
                        JTasInvLabMrgn += slJTasIJTls(IJK).JTasIJLabMrgn
                    End If
                    '
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                            slJTasInvJobs(IJK).JTasIJSERsw <> "X" Then
                        JTasInvSubTl += slJTasIJTls(IJK).JTasIJSubTl
                        JTasInvSubMrgn += slJTasIJTls(IJK).JTasIJSubMrgn
                    End If

                    '
                    If slJTasInvJobs(IJK).JTasIJCostMeth <> "AUP" Or
                            slJTasInvJobs(IJK).JTasIJMATsw <> "X" Then
                        JTasInvMatTl += slJTasIJTls(IJK).JTasIJMatTl
                        JTasInvMatMrgn += slJTasIJTls(IJK).JTasIJMatMrgn
                        JTasInvIHTl += slJTasIJTls(IJK).JTasIJIHTl
                        JTasInvIHMrgn += slJTasIJTls(IJK).JTasIJIHMrgn
                    End If
                    '
                    JTasInvAdjTl += slJTasIJTls(IJK).JTasIJAdjTl
                    JTasInvAdjMrgn = 0
                Next

                JTasInvMrgnTl = JTasInvAUPMrgn + JTasInvLabMrgn + JTasInvSubMrgn + JTasInvMatMrgn + JTasInvIHMrgn + JTasInvAdjMrgn
                '
                ' ---------------------------------------------------
                ' Build Grand Total Section.
                ' ---------------------------------------------------
                JTasBldInvSum(myDoc, "GRAND")
                '
                Dim JIMwhtFooter As New Phrase(FP_H6Blue(vbCrLf & "Information managed And reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                myDoc.Add(JIMwhtFooter)

                myDoc.Close()
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        '
        '

        Dim tmpSubJobID As String = txtJTasSubID.Text
        If slJTasInvJobs.Count > 1 Then
            tmpSubJobID = ""
        End If
        AUPFinalizeFlag = False
        If slJTasInvJobs.Count = 1 Then
            ' --------------------------------------------------------------------------------
            ' Added check of AUP invoice amount --- this takes care of CPWD job where unbilled
            ' activity is less than the remaining deposit balance. 3/12/2020
            ' --------------------------------------------------------------------------------
            If (JTasInvGrdTl = 0 And JTasInvAUPTl = 0) And
            (slJTasInvJobs(IJK).JTasIJCostMeth = "AUP" Or
            slJTasInvJobs(IJK).JTasIJCostMeth = "AUPH") Then
                Dim tmpMsgTxt As String =
                   "Invoicing has been selected For an AUP Or AUPH job where there are no " &
                   "queued AUP amounts Or chargeable category items pending invoicing. " &
                   "This process Is normally used To finalize an AUP (Or AUPH) job where bills have been " &
                   "posted after the final AUP invoice was produced." & vbCrLf & vbCrLf &
                   "Enter 'YES' to proceed …" & vbCrLf &
                   "   •	No invoice is generated." & vbCrLf &
                   "   •	All unposted charges to the job will be recorded as part" & vbCrLf &
                   "                 of the job’s expenses." & vbCrLf &
                   "   •	The AUP job is marked final (if it isn’t already marked that way" & vbCrLf &
                   "                 and the job will be deactivated." & vbCrLf &
                   "   •	After this process, a Job Report can be produced for this job " & vbCrLf &
                   "                 to analyze the overall profitability of the job." & vbCrLf & vbCrLf &
                   "Enter 'NO' to cancel …" & vbCrLf &
                   "   •	All invoicing activities are cancelled."

                Select Case MsgBox(tmpMsgTxt, MsgBoxStyle.YesNo, "JIM - AUP Job Finalization - (AS.003)")
                    Case MsgBoxResult.Yes
                        JTVarMaint.AUPFinalizeNumber += 1
                        If JTVarMaint.AUPFinalizeNumber > 999 Then
                            JTVarMaint.AUPFinalizeNumber = 1
                        End If
                        ' Set the final invoice flag on an invoice record for this job.
                        JTaup.JTaupSetFinalFlag(tmpFileName, tmpSubJobID)
                        AUPFinalizeFlag = True
                        cboxJTasDeact.Checked = True
                        ' Delete .pdf file that was just created.
                        My.Computer.FileSystem.DeleteFile(tmpFileName)
                        BtnJTasInvoice_Continue2(tmpFileName, tmpSubJobID, "Y", "N")
                    Case MsgBoxResult.No
                        ' delete dummy invoice file and return to main menu.
                        BtnJTasInvoice_Continue2(tmpFileName, tmpSubJobID, "N", "N")
                End Select
            End If
        End If
        ' ---------------------------------------------------------------------------------
        ' The following code calls the module to display the invoice that was just created.
        ' The tmpFileName field was modified for the new directory structure when the file
        ' created so no modifications should be necessary here for the change in directory
        ' structure. 12/5/2021
        ' ---------------------------------------------------------------------------------
        Dim JTvPDFRprtType As String = "NewInv"
        Dim tmpEmailDate As String = ""
        JTvPDF.JTvPDFDispPDFInv(JTvPDFRprtType, tmpFileName, tmpSubJobID, tmpEmailDate, "", "", "")
        '
        ' --------------------------------------------------------------------
        ' Last two arguments above are left blank ... used only in InvRev mode.
        ' They are JobId and InvNum.
        ' --------------------------------------------------------------------
        '
    End Sub
    '
    Public Sub BtnJTasInvoice_Continue2(filename As String, tmpSubJobID As String,
                                        tmpRecordSW As String, tmpEmailSW As String)
        '
        ' ---------------------------------------------------------------------------
        ' Invoices have been displayed. Now it is time to abort the invoice or record
        ' it as an executed invoice.
        ' ---------------------------------------------------------------------------
        '
        If tmpRecordSW = "Y" Then
            ' --------------------------------------------------------
            ' Update slJTasIH with information from this invoice.
            ' Mark all line items invoiced with invoice date.
            ' --------------------------------------------------------
            JTasRecordInvoice(tmpEmailSW)
            ' --------------------------------------------------------------------------------------
            ' Deactivate job if option set with rbtnJTasDeact on JTas panel.
            ' --------------------------------------------------------------------------------------
            If cboxJTasDeact.Checked = True Then
                JTjim.JTjimPurgeJob(txtJTasJobID.Text, tmpSubJobID, "DEACT")
                JTMainMenu.JTMMFirstTimeThru = "FIRST"
            End If
            '
            ' ---------------------------------------------------------------------------------------
            ' If invoice is being recorded (now it's real), do you want to Email it?
            ' ---------------------------------------------------------------------------------------
            If AUPFinalizeFlag = True Then   'If AUPFinalize, skip email stuff
                If JTinv.JTinvCameFromHere = True Then
                    JTinv.JTinvQueueInvoice()
                Else
                    JTMainMenu.JTMMinit()
                End If
            Else
                If JTjim.chkboxJTjimInvPDF.Checked = True Then
                    Dim tmpToAdd As String = JTasBldEmailToAdd()
                    Me.Hide()

                    If CheckForInternetConnection() = False Then
                        MsgBox("Computer currently does not have Internet access. Invoice" & vbCrLf &
                               "can be emailed later when the computer is connected.")

                        If JTinv.JTinvCameFromHere = True Then
                            JTinv.JTinvQueueInvoice()
                        Else
                            JTMainMenu.JTMMinit()
                        End If
                    Else

                        If tmpEmailSW = "Y" Then
                            JTasEmailInvoice(tmpToAdd, filename)
                        Else
                            If JTinv.JTinvCameFromHere = True Then
                                JTinv.JTinvQueueInvoice()
                            Else
                                JTMainMenu.JTMMinit()
                            End If
                        End If
                    End If
                Else
                    If JTinv.JTinvCameFromHere = True Then
                        JTinv.JTinvQueueInvoice()
                    Else
                        JTMainMenu.JTMMinit()
                    End If
                End If
            End If
            '
        Else
            ' Delete .pdf file that were just created.
            My.Computer.FileSystem.DeleteFile(filename)
            '
            ' Reactivate btnJTasInvoice button after cancelled invoice
            '
            BtnJTasInvoice.Enabled = True

        End If
        '
        ' -----------------------------------------------------------------------
        ' Inserting check for invoice number gaps here. It will run after each
        ' invoice is either recorded or deleted (not recorded). Nothing should be
        ' displayed unless a gap had occurred.
        ' -----------------------------------------------------------------------
        '
        '       JTInvSeq.JTInvSeqCheckInvoicePDFSeq()
        '
    End Sub
    Sub JTasBldInvHeader(ByRef mydoc As Document)
        ' ----------------------------------------------------------------
        ' Generate PDF Invoice Header section..
        ' ----------------------------------------------------------------

        ' ----------------------------------------------------------
        ' table structure to hold logo and date
        ' ----------------------------------------------------------
        Dim table As New PdfPTable(2)
        ' Actual width of table in points
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {8.5F, 6.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        '
        Dim tmpCustinfo As JTjim.JTjimCustInfo = Nothing
        JTjim.JTjimGetBillingInfo(txtJTasJobID.Text, tmpCustinfo)
        '
        '
        Dim logolocation As String = JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.CompanyLogo
        If JTjim.JTjimSendToAcctng(txtJTasJobID.Text, txtJTasSubID.Text) = False Then
            If JTVarMaint.AltCompanyLogo <> "" Then
                logolocation = JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.AltCompanyLogo
            End If
        End If
        '
        If Not File.Exists(logolocation) Then
            ToolTip1.Show("Logo file Not found.(As.001)", BtnJTasInvoice, 40, -30, 7000)
            JTasFillCell("-------- No logo file found -------", table, 0, 0, 0, "RED")
        Else
            '
            '
            Dim tmpImage As Image = Image.GetInstance(logolocation)
            tmpImage.ScaleAbsolute(140.0F, 50.0F)
            Dim CellContents = New PdfPCell(tmpImage)
            CellContents.Border = 0
            table.AddCell(CellContents)
        End If
        ' -----------------------------------------------------------------
        ' Add invoice Date.
        ' -----------------------------------------------------------------
        Dim tmpHeadingInfo As String = "Invoice Date: " & String.Format("{0:MM/ dd / yyyy}",
                             mtxtJTasInvDate.Text) & vbCrLf & vbCrLf &
                             "Invoice # " & JTasInvNumber & vbCrLf & vbCrLf & "Invoice To:"
        JTasFillCell(tmpHeadingInfo, table, 0, 1, 0, "")
        '
        ' ---------------------------------------------------------------
        ' Add company name and address here ... to be printed under logo.
        ' ---------------------------------------------------------------
        '
        Dim tmpCompNameAddress As String = vbCrLf & JTVarMaint.CompanyNme & vbCrLf &
                          JTVarMaint.CompanyMailingAddressL1 & vbCrLf &
                          JTVarMaint.CompanyMailingAddressL2 & vbCrLf &
                          JTVarMaint.CompanyMailingAddressL3 & vbCrLf &
                          JTVarMaint.CompanyContactEmail & " --- " & JTVarMaint.CompanyContactPhone
        If JTjim.chkboxJTjimSndToAcct.Checked = False Then
            If JTVarMaint.AltCompanyNme <> "" Then
                tmpCompNameAddress = vbCrLf & JTVarMaint.AltCompanyNme & vbCrLf &
                          JTVarMaint.AltCompanyMailingAddressL1 & vbCrLf &
                          JTVarMaint.AltCompanyMailingAddressL2 & vbCrLf &
                          JTVarMaint.AltCompanyMailingAddressL3 & vbCrLf &
                          JTVarMaint.CompanyContactEmail & " --- " & JTVarMaint.CompanyContactPhone
            End If
        End If
        JTasFillCell(tmpCompNameAddress, table, 0, 1, 0, "")
        '

        ' ----------------------------------------------------------------
        ' Add Bill to Address
        ' ----------------------------------------------------------------


        Dim tmpBillToAddress As String = vbCrLf & tmpCustinfo.JTjimCIContact & vbCrLf &
                          tmpCustinfo.JTjimCIBillStreet & vbCrLf &
                          tmpCustinfo.JTjimCIBillCity & vbCrLf & vbCrLf &
                          tmpCustinfo.JTjimCIEmail

        tmpCustinfo = Nothing
        '
        JTasFillCell(tmpBillToAddress, table, 0, 1, 0, "")
        '
        mydoc.Add(table)
        '
    End Sub

    Sub JTasBldJobHeader(ByRef mydoc As Document)
        ' ----------------------------------------------------------------
        ' Generate PDF Job / SubJob description line.
        ' ----------------------------------------------------------------  '
        '
        Dim table As New PdfPTable(1)
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        Dim widths() As Single = {20.0F}
        table.SetWidths(widths)


        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        '
        ' leave a gap before and after the table
        '
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 4.0F
        Dim tmpText As String = ""
        Dim tmpJobKey As String = slJTasInvJobs(IJK).JTasIJJobID
        If slJTasInvJobs(IJK).JTasIJSubJobID <> "" Then
            tmpJobKey = tmpJobKey & "~" & slJTasInvJobs(IJK).JTasIJSubJobID
        End If
        If slJTasInvJobs(IJK).JTasIJDescrip <> "" Then
            tmpText = "Job Name - " & slJTasInvJobs(IJK).JTasIJDescrip & " (" & tmpJobKey & ")"
        Else
            tmpText = "Job Name - " & tmpJobKey
        End If
        '
        JTasFillCell(tmpText, table, 0, 1, 2.5, "PALEBLUE")
        '
        mydoc.Add(table)
        CurrentInvJobTitle = slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID
        '
    End Sub
    ''' <summary>
    ''' Subroutine builds a table containg the AUP invoice date. It is added 
    ''' to 'mydoc' ... the pdf image of the invoice.
    ''' </summary>
    ''' <param name="myDoc"></param>

    Sub JTasBldAUPSection(ByRef myDoc As Document)
        ' -----------------------------------------------------------------------------------
        ' Code to print AUP invoice items.
        ' -----------------------------------------------------------------------------------
        '
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpPrintFlag As Boolean = False

        '
        Do While tmpSub < JTaslvAUP.Items.Count
            If JTaslvAUP.Items(tmpSub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvAUP.Items(tmpSub).SubItems(4).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvAUP.Items(tmpSub).SubItems(5).Text) Then
                tmpPrintFlag = True
            End If
            tmpSub += 1
        Loop
        '
        If tmpPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(myDoc)
            End If
            '
            Dim table As New PdfPTable(3)
                ' actual width of table in points
                table.TotalWidth = 468.0F   ' fix the absolute width of the table
                table.LockedWidth = True
                table.HeaderRows = 1   ' reprint first & second rows at top of each page

                table.SpacingBefore = 10.0F
                table.SpacingAfter = 4.0F '
                ' Column widths are proportional to other columns and the table width.
                Dim widths() As Single =
               {1.75F, 12.2F, 1.75F}
                table.SetWidths(widths)
                '
                table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
                '
                ' leave a gap before And after the table
                table.SpacingBefore = 10.0F
                table.SpacingAfter = 10.0F

                Dim tmpTlDollars As Double = 0
                JTasFillCell("Invoiced Contracted Amounts", table, 1, 3, 0, "")
                JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Explanation", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Amount", table, 2, 1, 2.5, "PALEBLUE")
                tmpSub = 0
                '
                Do While tmpSub < JTaslvAUP.Items.Count
                    If JTaslvAUP.Items(tmpSub).Checked = False And
                        (slJTasInvJobs(IJK).JTasIJJobID = JTaslvAUP.Items(tmpSub).SubItems(4).Text And
                         slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvAUP.Items(tmpSub).SubItems(5).Text) Then
                        JTasFillCell(JTaslvAUP.Items(tmpSub).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvAUP.Items(tmpSub).SubItems(1).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvAUP.Items(tmpSub).SubItems(2).Text, table, 2, 1, 0, "")
                    '
                    ' accum total $$$
                    tmpTlDollars += Val(JTaslvAUP.Items(tmpSub).SubItems(2).Text)
                End If
                    tmpSub += 1
                Loop
                '
                JTasFillCell("Totals ---->", table, 2, 2, 2.5, "PALEBLUE")
                JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
                JTasInvSectionsPrtd += 1
                '
                Dim tmpNewItem As JTasIJTstru
                If slJTasIJTls.ContainsKey(IJK) Then
                    tmpNewItem = slJTasIJTls(IJK)
                    slJTasIJTls.Remove(IJK)
                Else
                    JTasIJTlsClr(tmpNewItem)
                End If
                '
                tmpNewItem.JTasIJAUPTl = tmpTlDollars
                tmpNewItem.JTasIJAUPMrgn = tmpTLDollarsMrgn
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
                '
                myDoc.Add(table)
                '
            End If

    End Sub
    Sub JTasBldLabTskSummarySection(ByRef myDoc As Document)
        ' -----------------------------------------------------------------
        ' Build SL of labor tasks. Create section for invoice printouts.
        ' Clear SL at end.
        ' -----------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpPrintFlag As Boolean = False
        Dim tmpLTSitem As New JTasLTSitem
        Dim tmpNumOfSumLines As Integer = 0

        '
        tmpLTSitem.JTasLTSiHours = "0"
        tmpLTSitem.JTasLTSiDollars = "0"
        '
        Dim InvJobsKeys As ICollection = slJTasInvJobs.Keys
        '
        Do While tmpSub < JTaslvLab.Items.Count
            '
            ' eroor on this if statement

            If JTaslvLab.Items(tmpSub).Checked = False Then
                If slJTasInvJobs(IJK).JTasIJJobID = JTaslvLab.Items(tmpSub).SubItems(7).Text And
                    slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvLab.Items(tmpSub).SubItems(8).Text Then
                    '
                    tmpPrintFlag = True
                    tmpNumOfSumLines += 1
                    tmpTLDollarsMrgn += Val(JTaslvLab.Items(tmpSub).SubItems(6).Text)
                    If slJTasLTS.ContainsKey(JTaslvLab.Items(tmpSub).SubItems(2).Text) Then
                        tmpLTSitem.JTasLTSiHours = slJTasLTS(JTaslvLab.Items(tmpSub).SubItems(2).Text).JTasLTSiHours
                        tmpLTSitem.JTasLTSiDollars = slJTasLTS(JTaslvLab.Items(tmpSub).SubItems(2).Text).JTasLTSiDollars
                        slJTasLTS.Remove(JTaslvLab.Items(tmpSub).SubItems(2).Text)

                        tmpLTSitem.JTasLTSiHours = Str(Val(tmpLTSitem.JTasLTSiHours) + Val(JTaslvLab.Items(tmpSub).SubItems(3).Text))
                        tmpLTSitem.JTasLTSiDollars = Str(Val(tmpLTSitem.JTasLTSiDollars) + Val(JTaslvLab.Items(tmpSub).SubItems(4).Text))
                    Else
                        tmpLTSitem.JTasLTSiHours = Val(JTaslvLab.Items(tmpSub).SubItems(3).Text)
                        tmpLTSitem.JTasLTSiDollars = Val(JTaslvLab.Items(tmpSub).SubItems(4).Text)
                    End If
                    slJTasLTS.Add(JTaslvLab.Items(tmpSub).SubItems(2).Text, tmpLTSitem)
                    tmpLTSitem.JTasLTSiHours = "0"
                    tmpLTSitem.JTasLTSiDollars = "0"
                End If
            End If
            tmpSub += 1
        Loop
        If tmpPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(myDoc)
            End If
            '
            Dim tmpTlHours As Double = 0
            Dim tmpTlDollars As Double = 0
            '
            ' ---------------------------------------------------------------------------
            ' Table to provide structure for the detailed items to be printed
            ' ---------------------------------------------------------------------------
            '
            Dim table As New PdfPTable(3)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            table.HeaderRows = 2   ' reprint first & second rows at top of each page

            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F '
            ' Column widths are proportional to other columns and the table width.
            Dim widths() As Single =
           {12.75F, 1.75F, 1.75F}
            table.SetWidths(widths)
            '
            table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
            '
            ' leave a gap before And after the table
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 10.0F
            JTasFillCell("Invoiced LABOR Task Summary", table, 1, 3, 0, "")
            JTasFillCell("Task Description", table, 1, 1, 2.5, "PALEBLUE")
            JTasFillCell("Hours", table, 2, 1, 2.5, "PALEBLUE")
            JTasFillCell("$$$$", table, 2, 1, 2.5, "PALEBLUE")
            '

            tmpSub = 0
            Dim numoflines As Integer = 0
            '
            Dim keys As ICollection = slJTasLTS.Keys
            Dim k As String
            For Each k In keys

                ' --------------------------------------------------------
                ' Logic to fill data fields in table
                ' --------------------------------------------------------
                '
                JTasFillCell(k, table, 1, 1, 0, "") 'Task Description
                tmpTlHours += Val(slJTasLTS(k).JTasLTSiHours)
                JTasFillCell(String.Format("{0:0.00}", Val(slJTasLTS(k).JTasLTSiHours)), table, 2, 1, 0, "")
                '
                JTasFillCell(String.Format("{0:0.00}", Val(slJTasLTS(k).JTasLTSiDollars)), table, 2, 1, 0, "")
                tmpTlDollars += Val(slJTasLTS(k).JTasLTSiDollars)
                tmpNumOfSumLines -= 1    ' decrimenting item counter
            Next
            slJTasLTS.Clear()        ' empty sorted list
            '
            JTasFillCell("Labor Totals ---->", table, 2, 1, 2.5, "PALEBLUE")
            JTasFillCell(String.Format("{0:0.00}", tmpTlHours), table, 2, 1, 2.5, "PALEBLUE")
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            '
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJLabTl = tmpTlDollars
            tmpNewItem.JTasIJLabMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            myDoc.Add(table)

        Else
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                '
                tmpNewItem.JTasIJLabTl = 0
                tmpNewItem.JTasIJLabMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
            End If
        End If
    End Sub
    Sub JTasBldLabSection(ByRef mydoc As Document, ByRef tmpSub As Integer)
        ' -----------------------------------------------------------------------------------
        ' Code to print a list of invoice LABOR items.
        ' -----------------------------------------------------------------------------------
        '
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------


        ItemsToPrintFlag = False
        tmpTlHours = 0
        tmpTlDollars = 0
        tmpTLDollarsMrgn = 0
        '
        tmpNumOfItems = 0
        Do While tmpSub < JTaslvLab.Items.Count
            If JTaslvLab.Items(tmpSub).Checked = False Then
                If (slJTasInvJobs(IJK).JTasIJJobID = JTaslvLab.Items(tmpSub).SubItems(7).Text And
                 slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvLab.Items(tmpSub).SubItems(8).Text) Then
                    ItemsToPrintFlag = True
                    tmpNumOfItems += 1
                    ' accum total hours, dollars and margin
                    tmpTlHours += Val(JTaslvLab.Items(tmpSub).SubItems(3).Text)
                    tmpTlDollars += Val(JTaslvLab.Items(tmpSub).SubItems(4).Text)
                    tmpTLDollarsMrgn += Val(JTaslvLab.Items(tmpSub).SubItems(6).Text)
                End If
            End If
            tmpSub += 1
        Loop
        tmpSub = 0
        '
        If ItemsToPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(mydoc)
            End If
            '
            ' ---------------------------------------------------------------------------
            ' Table to provide structure for the detailed items to be printed
            ' ---------------------------------------------------------------------------
            '
            Dim table As New PdfPTable(7)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            table.HeaderRows = 2   ' reprint first & second rows at top of each page
            '
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F
            '
            ' Column widths are proportional to other columns and the table width.
            Dim DetailWidths() As Single =
               {1.9F, 1.6F, 6.6F, 1.5F, 1.5F, 1.2F, 1.75F}
            Dim TlOnlyWidths() As Single =
                {0F, 0F, 11.6F, 0F, 1.5F, 1.2F, 1.75F}
            If JTasInvLabTotals = True Then
                table.SetWidths(TlOnlyWidths)
                JTasFillCell("--------------- LABOR TOTAL ---------------", table, 1, 7, 0, "")
            Else
                table.SetWidths(DetailWidths)
                JTasFillCell("Invoiced LABOR Items", table, 1, 7, 0, "")
            End If
            '
            table.HorizontalAlignment = 0     ' Determines the location of the table on the page.
            '
            ' leave a gap before And after the table
            '
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 10.0F
            If JTasInvLabTotals = False Then
                JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Worker", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Description", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("LabCat", table, 1, 1, 2.5, "PALEBLUE")
            Else
                JTasFillCell("", table, 2, 4, 2.5, "PALEBLUE")
            End If
            JTasFillCell("Hours", table, 2, 1, 2.5, "PALEBLUE")
            If JTasInvLabTotals = False Then
                JTasFillCell("Rate", table, 2, 1, 2.5, "PALEBLUE")
            Else
                JTasFillCell("", table, 2, 1, 2.5, "PALEBLUE")
            End If
            JTasFillCell("Ext'd $$$", table, 2, 1, 2.5, "PALEBLUE")
            '
            Dim numoflines As Integer = 0

            '
            If JTasInvLabTotals = False Then
                Do While tmpSub < JTaslvLab.Items.Count    ' Or JTasInvLabTotals = False
                    If JTaslvLab.Items(tmpSub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvLab.Items(tmpSub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvLab.Items(tmpSub).SubItems(8).Text) Then
                        '
                        ' --------------------------------------------------------
                        ' Logic to fill data fields in table
                        ' --------------------------------------------------------
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).Text, table, 1, 1, 0, "")
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).SubItems(5).Text, table, 1, 1, 0, "")
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).SubItems(2).Text, table, 1, 1, 0, "")
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).SubItems(1).Text, table, 1, 1, 0, "")
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).SubItems(3).Text, table, 2, 1, 0, "")
                        Dim tmpHrlyRate As Double = Val(JTaslvLab.Items(tmpSub).SubItems(4).Text) / Val(JTaslvLab.Items(tmpSub).SubItems(3).Text)
                        JTasFillCell(String.Format("{0:0.00}", tmpHrlyRate), table, 2, 1, 0, "")
                        '
                        JTasFillCell(JTaslvLab.Items(tmpSub).SubItems(4).Text, table, 2, 1, 0, "")
                        '
                        tmpNumOfItems -= 1
                    End If
                    tmpSub += 1
                Loop
            End If
            '
            JTasFillCell("Labor Totals ---->", table, 2, 4, 2.5, "PALEBLUE")
            '
            JTasFillCell(String.Format("{0:0.00}", tmpTlHours), table, 2, 1, 2.5, "PALEBLUE")
            JTasFillCell("", table, 2, 1, 2.5, "PALEBLUE")
            '
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            '
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJLabTl = tmpTlDollars
            tmpNewItem.JTasIJLabMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            SectionDone = True
            '
            mydoc.Add(table)
            '
        Else
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                '
                tmpNewItem.JTasIJLabTl = 0
                tmpNewItem.JTasIJLabMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
            End If
            SectionDone = True
        End If
    End Sub
    '
    Sub JTasBldSubSection(ByRef mydoc As Document, ByRef tmpsub As Integer, ByRef tmpNumOfItems As Integer)
        ' -----------------------------------------------------------------------------------
        ' Code to print a list of invoice Subcontractor items.
        ' -----------------------------------------------------------------------------------
        '
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------

        '
        tmpNumOfItems = 0
        ItemsToPrintFlag = False
        tmpTlDollars = 0
        tmpTLDollarsMrgn = 0
        tmpsub = 0
        Do While tmpsub < JTaslvSub.Items.Count
            If JTaslvSub.Items(tmpsub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvSub.Items(tmpsub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvSub.Items(tmpsub).SubItems(8).Text) Then
                If JTaslvSub.Items(tmpsub).SubItems(1).Text <> "" Then
                    ItemsToPrintFlag = True
                    tmpNumOfItems += 1
                    tmpTlDollars += Val(JTaslvSub.Items(tmpsub).SubItems(4).Text)
                    tmpTLDollarsMrgn += Val(JTaslvSub.Items(tmpsub).SubItems(4).Text) _
                                        - Val(JTaslvSub.Items(tmpsub).SubItems(3).Text)
                End If
            End If
            tmpsub += 1
        Loop
        tmpsub = 0
        '
        If ItemsToPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(mydoc)
            End If
            '
            Dim table As New PdfPTable(6)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            If JTasInvSubSerTotals = True Then
                table.HeaderRows = 1
            Else
                table.HeaderRows = 2   ' reprint first & second rows at top of each page
            End If
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F
            '
            ' Column widths are proportional to other columns and the table width.
            Dim DetailWidths() As Single =
           {1.9F, 2.8F, 6.35F, 1.5F, 1.75F, 1.75F}
            Dim DetailNoCostsWidths() As Single =
                {2.1F, 3.6F, 8.6F, 0F, 0F, 1.75F}
            Dim TlOnlyWidths() As Single =
            {0F, 0F, 14.3F, 0F, 0F, 1.75F}
            If JTasInvSubSerTotals = True Then
                table.SetWidths(DetailWidths)
                JTasFillCell("-------------- SUBCONTRACTED TOTAL --------------", table, 1, 6, 0, "")
            Else
                If JTasInvSubShowCost = True Then
                    table.SetWidths(DetailWidths)
                Else
                    table.SetWidths(DetailNoCostsWidths)
                End If
                JTasFillCell("Invoiced Subcontracted Costs", table, 1, 6, 0, "")
            End If
            '
            tmpsub = 0
            If JTasInvSubSerTotals = False Then
                JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("SubContractor", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Work Description", table, 1, 1, 2.5, "PALEBLUE")
                If JTasInvSubShowCost = True Then       ' show costs
                    JTasFillCell("Inv #", table, 1, 1, 2.5, "PALEBLUE")
                    JTasFillCell("SC Inv.", table, 2, 1, 2.5, "PALEBLUE")
                Else
                    JTasFillCell("", table, 2, 1, 2.5, "PALEBLUE")
                    JTasFillCell("", table, 2, 1, 2.5, "PALEBLUE")
                End If
                JTasFillCell("Job Chg", table, 2, 1, 2.5, "PALEBLUE")
            End If
            '
            If JTasInvSubSerTotals = False Then
                Do While tmpsub < JTaslvSub.Items.Count
                    If JTaslvSub.Items(tmpsub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvSub.Items(tmpsub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvSub.Items(tmpsub).SubItems(8).Text) Then
                        If JTaslvSub.Items(tmpsub).SubItems(1).Text <> "" Then
                            JTasFillCell(JTaslvSub.Items(tmpsub).Text, table, 1, 1, 0, "")
                            JTasFillCell(JTaslvSub.Items(tmpsub).SubItems(1).Text, table, 1, 1, 0, "")
                            JTasFillCell(JTaslvSub.Items(tmpsub).SubItems(2).Text, table, 1, 1, 0, "")
                            If JTasInvSubShowCost = True Then       ' show costs
                                JTasFillCell(JTaslvSub.Items(tmpsub).SubItems(5).Text, table, 1, 1, 0, "")
                                JTasFillCell(JTaslvSub.Items(tmpsub).SubItems(3).Text, table, 2, 1, 0, "")
                            Else
                                JTasFillCell("", table, 2, 1, 0, "")
                                JTasFillCell("", table, 2, 1, 0, "")
                            End If
                            '
                            JTasFillCell(JTaslvSub.Items(tmpsub).SubItems(4).Text, table, 2, 1, 0, "")
                            '
                        End If
                    End If
                    tmpsub += 1
                Loop
            End If
            '
            JTasFillCell("Subcontractor Totals ---->", table, 2, 5, 2.5, "PALEBLUE")
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            '
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJSubTl = tmpTlDollars
            tmpNewItem.JTasIJSubMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            mydoc.Add(table)
            '
            SectionDone = True

        Else
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                '
                tmpNewItem.JTasIJSubTl = 0
                tmpNewItem.JTasIJSubMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
            End If
            SectionDone = True
        End If
    End Sub
    '
    Sub JTasBldMatSection(ByRef mydoc As Document, ByRef tmpsub As Integer, ByRef tmpNumOfITems As Integer)
        ' -----------------------------------------------------------------------------------
        ' Code to print a list of invoice MATERIAL items. Exclude "IH" items.
        ' ----------------------------------------------------------------------------------- 
        '
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------
        tmpsub = 0
        tmpNumOfITems = 0
        ItemsToPrintFlag = False
        tmpTlDollars = 0
        tmpTLDollarsMrgn = 0
        '
        Do While tmpsub < JTaslvMat.Items.Count
            If JTaslvMat.Items(tmpsub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpsub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpsub).SubItems(8).Text) Then
                If JTaslvMat.Items(tmpsub).SubItems(1).Text <> "" Then
                    ItemsToPrintFlag = True
                    tmpNumOfITems += 1
                    ' accum total $$$
                    tmpTlDollars += Val(JTaslvMat.Items(tmpsub).SubItems(4).Text)
                    '
                    tmpTLDollarsMrgn += Val(JTaslvMat.Items(tmpsub).SubItems(4).Text) _
                               - Val(JTaslvMat.Items(tmpsub).SubItems(3).Text)
                    '
                End If
            End If
            tmpsub += 1
        Loop
        tmpsub = 0
        '
        If ItemsToPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(mydoc)
            End If
            Dim table As New PdfPTable(6)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            If JTasInvMatTotals = True Then
                table.HeaderRows = 1
            Else
                table.HeaderRows = 2   ' reprint first & second rows at top of each page
            End If
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F
            '
            ' Column widths are proportional to other columns and the table width.
            Dim DetailWidths() As Single =
                {1.9F, 2.8F, 6.35F, 1.5F, 1.75F, 1.75F}
            Dim DetailNoCostsWidths() As Single =
                {1.9F, 3.8F, 8.6F, 0F, 0F, 1.75F}
            Dim TlOnlyWidths() As Single =
                {0F, 0F, 14.3F, 0F, 0F, 1.75F}
            If JTasInvMatTotals = True Then
                table.SetWidths(TlOnlyWidths)
                JTasFillCell("---------- MATERIAL COST TOTALS ----------", table, 1, 6, 0, "")
            Else
                If JTasInvMatShowCost = True Then
                    table.SetWidths(DetailWidths)
                Else
                    table.SetWidths(DetailNoCostsWidths)
                End If
                JTasFillCell("Invoiced Material Costs", table, 1, 6, 0, "")
            End If
            tmpsub = 0
            '
            If JTasInvMatTotals = False Then
                JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Supplier", table, 1, 1, 2.5, "PALEBLUE")
                JTasFillCell("Material Description", table, 1, 1, 2.5, "PALEBLUE")
                If JTasInvMatShowCost = True Then
                    JTasFillCell("Inv #", table, 1, 1, 2.5, "PALEBLUE")
                    JTasFillCell("Sup. Inv.", table, 1, 1, 2.5, "PALEBLUE")
                Else
                    JTasFillCell("", table, 0, 1, 0, "PALEBLUE")
                    JTasFillCell("", table, 0, 1, 0, "PALEBLUE")
                End If
                JTasFillCell("Job Chg", table, 1, 1, 2.5, "PALEBLUE")
                '
            End If

            If JTasInvMatTotals = False Then
                Do While tmpsub < JTaslvMat.Items.Count
                    If JTaslvMat.Items(tmpsub).Checked = False And
                        (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpsub).SubItems(7).Text And
                         slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpsub).SubItems(8).Text) Then
                        If JTaslvMat.Items(tmpsub).SubItems(1).Text <> "" Then
                            JTasFillCell(JTaslvMat.Items(tmpsub).Text, table, 1, 1, 0, "")
                            JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(1).Text, table, 1, 1, 0, "")
                            JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(2).Text, table, 1, 1, 0, "")
                            If JTasInvMatShowCost = True Then
                                JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(5).Text, table, 1, 1, 0, "")
                                JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(3).Text, table, 2, 1, 0, "")
                            Else
                                JTasFillCell("", table, 2, 1, 0, "")
                                JTasFillCell("", table, 2, 1, 0, "")
                            End If
                            JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(4).Text, table, 2, 1, 0, "")
                            tmpNumOfITems -= 1
                        End If
                    End If
                    tmpsub += 1
                    '
                Loop
            End If
            JTasFillCell("Material Totals ---->", table, 2, 5, 2.5, "PALEBLUE")
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            '
            mydoc.Add(table)
            '
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJMatTl = tmpTlDollars
            tmpNewItem.JTasIJMatMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            SectionDone = True
        Else
            SectionDone = True
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                tmpNewItem.JTasIJMatTl = 0
                tmpNewItem.JTasIJMatMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)

            End If
            '

        End If
    End Sub
    '
    Sub JTasBldIHSection(ByRef mydoc As Document, ByRef tmpsub As Integer)
        ' -----------------------------------------------------------------------------------
        ' Code to print a list of invoice IH items. 
        ' ----------------------------------------------------------------------------------- 
        '
        Dim tmpNumOfItems As Integer = 0
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------
        tmpsub = 0
        ItemsToPrintFlag = False
        '
        Do While tmpsub < JTaslvMat.Items.Count
            If JTaslvMat.Items(tmpsub).Checked = False And
                (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpsub).SubItems(7).Text And
                 slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpsub).SubItems(8).Text) Then
                If JTaslvMat.Items(tmpsub).SubItems(1).Text = "" And
                   JTaslvMat.Items(tmpsub).SubItems(5).Text = "" Then
                    ItemsToPrintFlag = True
                    tmpNumOfItems += 1
                End If
            End If
            tmpsub += 1
        Loop
        tmpsub = 0

        '
        If ItemsToPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(mydoc)
            End If
            '
            Dim table As New PdfPTable(3)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            table.HeaderRows = 2   ' reprint first & second rows at top of each page
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F
            '
            ' Column widths are proportional to other columns and the table width.
            Dim DetailWidths() As Single =
                 {1.9F, 12.4F, 1.75F}
            table.SetWidths(DetailWidths)
            JTasFillCell("Manufactured Assemblies, Equipment, Etc.", table, 1, 3, 0, "")
            tmpsub = 0
            tmpTlDollars = 0
            tmpTLDollarsMrgn = 0
            '
            JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
            JTasFillCell("Item Description", table, 1, 1, 2.5, "PALEBLUE")
            JTasFillCell("Amount", table, 2, 1, 2.5, "PALEBLUE")
            '
            Do While tmpsub < JTaslvMat.Items.Count
                If JTaslvMat.Items(tmpsub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvMat.Items(tmpsub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvMat.Items(tmpsub).SubItems(8).Text) Then
                    If JTaslvMat.Items(tmpsub).SubItems(1).Text = "" And
                       JTaslvMat.Items(tmpsub).SubItems(5).Text = "" Then
                        JTasFillCell(JTaslvMat.Items(tmpsub).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(2).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvMat.Items(tmpsub).SubItems(4).Text, table, 2, 1, 0, "")
                        ' accum total $$$
                        tmpTlDollars += Val(JTaslvMat.Items(tmpsub).SubItems(4).Text)
                        '
                        tmpTLDollarsMrgn += Val(JTaslvMat.Items(tmpsub).SubItems(4).Text) _
                        - Val(JTaslvMat.Items(tmpsub).SubItems(3).Text)
                        tmpNumOfItems -= 1
                    End If
                End If
                tmpsub += 1
            Loop
            '
            JTasFillCell("Manufactured Assemblies, Equipment, Etc. Totals ------------------>", table, 2, 2, 2.5, "PALEBLUE")
            '
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJIHTl = tmpTlDollars
            tmpNewItem.jtasIJIHMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            mydoc.Add(table)
            SectionDone = True
        Else
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                '
                tmpNewItem.JTasIJIHTl = 0
                tmpNewItem.jtasIJIHMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
            End If
            SectionDone = True
        End If
    End Sub
    '
    Sub JTasBldAdjSection(ByRef mydoc As Document)
        ' -----------------------------------------------------------------------------------
        ' Code to print a list of invoice Adjustment items.
        ' -----------------------------------------------------------------------------------
        '
        ' ----------------------------------------------------------------------------------
        ' Check to see if there are any items to print on invoice in this category
        ' ----------------------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpPrintFlag As Boolean = False
        '
        Do While tmpSub < JTaslvSub.Items.Count
            If JTaslvSub.Items(tmpSub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvSub.Items(tmpSub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvSub.Items(tmpSub).SubItems(8).Text) Then
                If JTaslvSub.Items(tmpSub).SubItems(1).Text = "" And
                    JTaslvSub.Items(tmpSub).SubItems(2).Text <> "" Then
                    tmpPrintFlag = True
                End If
            End If
            tmpSub += 1
        Loop
        '
        If tmpPrintFlag = True Then  'True means there are items to print
            If CurrentInvJobTitle <> slJTasInvJobs(IJK).JTasIJJobID & slJTasInvJobs(IJK).JTasIJSubJobID Then
                JTasBldJobHeader(mydoc)
            End If
            '
            Dim table As New PdfPTable(3)
            ' actual width of table in points
            table.TotalWidth = 468.0F   ' fix the absolute width of the table
            table.LockedWidth = True
            table.HeaderRows = 2   ' reprint first & second rows at top of each page
            table.SpacingBefore = 10.0F
            table.SpacingAfter = 4.0F
            '
            ' Column widths are proportional to other columns and the table width.
            Dim DetailWidths() As Single =
                 {1.9F, 12.4F, 1.75F}

            table.SetWidths(DetailWidths)
            JTasFillCell("Invoice Adjustments", table, 1, 3, 0, "")
            Dim tmpTlDollars As Double = 0
            JTasFillCell("Date", table, 1, 1, 2.5, "PALEBLUE")
            JTasFillCell("Adjustment Explanation", table, 1, 1, 2.5, "PALEBLUE")
            JTasFillCell("Adj. Amt", table, 2, 1, 2.5, "PALEBLUE")
            tmpSub = 0
            '
            Do While tmpSub < JTaslvSub.Items.Count
                If JTaslvSub.Items(tmpSub).Checked = False And
                    (slJTasInvJobs(IJK).JTasIJJobID = JTaslvSub.Items(tmpSub).SubItems(7).Text And
                     slJTasInvJobs(IJK).JTasIJSubJobID = JTaslvSub.Items(tmpSub).SubItems(8).Text) Then
                    If JTaslvSub.Items(tmpSub).SubItems(1).Text = "" And
                       JTaslvSub.Items(tmpSub).SubItems(2).Text <> "" Then
                        JTasFillCell(JTaslvSub.Items(tmpSub).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvSub.Items(tmpSub).SubItems(2).Text, table, 1, 1, 0, "")
                        JTasFillCell(JTaslvSub.Items(tmpSub).SubItems(3).Text, table, 2, 1, 0, "")
                        '
                        ' accum total $$$
                        tmpTlDollars += Val(JTaslvSub.Items(tmpSub).SubItems(3).Text)
                        '
                    End If
                End If
                tmpSub += 1
            Loop
            '
            JTasFillCell("Adjustment Totals ---->", table, 2, 2, 2.5, "PALEBLUE")
            JTasFillCell(String.Format("{0:0.00}", tmpTlDollars), table, 2, 1, 2.5, "PALEBLUE")
            '
            JTasInvSectionsPrtd += 1
            '
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
            Else
                JTasIJTlsClr(tmpNewItem)
            End If
            '
            tmpNewItem.JTasIJAdjTl = tmpTlDollars
            tmpNewItem.JTasIJAdjMrgn = tmpTLDollarsMrgn
            '
            slJTasIJTls.Add(IJK, tmpNewItem)
            '
            mydoc.Add(table)
            '
        Else
            Dim tmpNewItem As JTasIJTstru
            If slJTasIJTls.ContainsKey(IJK) Then
                tmpNewItem = slJTasIJTls(IJK)
                slJTasIJTls.Remove(IJK)
                '
                tmpNewItem.JTasIJAdjTl = 0
                tmpNewItem.JTasIJAdjMrgn = 0
                '
                slJTasIJTls.Add(IJK, tmpNewItem)
            End If
        End If
    End Sub
    Sub JTasBldInvSum(ByRef myDoc As Document, tmpSumType As String)
        ' -----------------------------------------------------------------------------------
        ' Code to print a summary of the totals for the invoice
        ' -----------------------------------------------------------------------------------
        '
        JTasInvGrdTl = JTasInvAUPTl + JTasInvLabTl + JTasInvSubTl + JTasInvMatTl + JTasInvIHTl + JTasInvAdjTl
        ' -------------------------------------------------------------------------
        ' Check to see if there are transactions to print in this section. If not,
        ' Exit the sub.
        ' -------------------------------------------------------------------------
        '
        ' ========================================================================================
        ' Added a check for every financial component on invoice. This will cause summary to print
        ' on invoices where there are offseting entries causing the generation of a zero
        ' balance invoice. - - - 3/12/2020
        ' ========================================================================================
        If JTasInvGrdTl = 0 Then
            If JTasInvAUPTl = 0 Then
                If JTasInvLabTl = 0 Then
                    If JTasInvSubTl = 0 Then
                        If JTasInvMatTl = 0 Then
                            If JTasInvIHTl = 0 Then
                                If JTasInvAdjTl = 0 Then
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        '
        Dim table As New PdfPTable(3)
        ' actual width of table in points
        table.TotalWidth = 468.0F   ' fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2   ' reprint first & second rows at top of each page

        table.SpacingBefore = 10.0F
        table.SpacingAfter = 4.0F '
        ' Column widths are proportional to other columns and the table width.
        Dim DetailWidths() As Single =
                 {1.9F, 12.4F, 1.75F}

        table.SetWidths(DetailWidths)
        '
        If tmpSumType = "GRAND" Then
            CurrentInvJobTitle = Nothing    ' clear out current job title at end of invoice.
            JTasFillCell(vbCrLf & "Invoice Summary", table, 1, 3, 0, "")
            '
            ' --------------------------------------------------------
            ' Code to add invoice total into keyword metadata field.
            ' --------------------------------------------------------
            myDoc.AddKeywords(JTasInvGrdTl.ToString)
            '
        Else
            JTasFillCell(vbCrLf & slJTasInvJobs(IJK).JTasIJDescrip & " Summary", table, 1, 3, 0, "")
        End If
        '

        JTasFillCell("", table, 2, 1, 2.5, "PALEBLUE")
        JTasFillCell("----- Category -----", table, 1, 1, 2.5, "PALEBLUE")
        JTasFillCell("Amount", table, 2, 1, 2.5, "PALEBLUE")
        '
        If JTasInvAUPTl <> 0 Then
            '
            JTasFillCell("", table, 2, 1, 0, "")
            JTasFillCell("Contracted Amount Total ----------->", table, 2, 1, 0, "")
            '
            JTasFillCell(String.Format("{0:0.00}", JTasInvAUPTl), table, 2, 1, 0, "")
        End If
        '
        If JTasInvLabTl <> 0 Then
            '
            JTasFillCell("", table, 2, 0, 0, "")
            JTasFillCell("Labor Total ----------------------->", table, 2, 1, 0, "")
            JTasFillCell(String.Format("{0:0.00}", JTasInvLabTl), table, 2, 1, 0, "")
        End If
        '
        If JTasInvSubTl <> 0 Then
            '
            JTasFillCell("", table, 2, 0, 0, "")
            JTasFillCell("SubContractor Total ------------>", table, 2, 1, 0, "")
            JTasFillCell(String.Format("{0:0.00}", JTasInvSubTl), table, 2, 1, 0, "")
        End If
        '
        If JTasInvMatTl <> 0 Then
            '
            JTasFillCell("", table, 2, 1, 0, "")
            JTasFillCell("Material Total -------------------->", table, 2, 1, 0, "")
            JTasFillCell(String.Format("{0:0.00}", JTasInvMatTl), table, 2, 1, 0, "")
        End If
        '
        If JTasInvIHTl <> 0 Then
            '
            JTasFillCell("", table, 2, 1, 0, "")
            JTasFillCell("Manufactured Assemblies, Equipment, Etc. Totals --------->", table, 2, 1, 0, "")
            JTasFillCell(String.Format("{0:0.00}", JTasInvIHTl), table, 2, 1, 0, "")
        End If
        '
        If JTasInvAdjTl <> 0 Then
            JTasFillCell("", table, 2, 0, 0, "")
            JTasFillCell("Adjustment Total ---------------->", table, 2, 1, 0, "")
            JTasFillCell(String.Format("{0:0.00}", JTasInvAdjTl), table, 2, 1, 0, "")
        End If
        If tmpSumType = "GRAND" Then

            JTasFillCell("GRAND TOTAL ---------------------->", table, 2, 2, 2.5, "PALEBLUE")
        Else

            JTasFillCell(slJTasInvJobs(IJK).JTasIJDescrip & " TOTAL -->", table, 2, 2, 2.5, "PALEBLUE")
        End If
        '
        JTasFillCell(String.Format("{0:0.00}", JTasInvGrdTl), table, 2, 1, 2.5, "PALEBLUE")
        '
        ' --------------------------------------------------------------------
        ' Following code 'nests' the data to be printed in another table
        ' so that total and subtotals summary sections will not be potentially 
        ' be split between pages on the invoice.
        ' --------------------------------------------------------------------
        '
        Dim NestTable As New PdfPTable(1)
        NestTable.TotalWidth = 468.0F   ' fix the absolute width of the table
        NestTable.LockedWidth = True
        Dim NestCell As New PdfPCell(table)
        NestCell.Border = 0
        NestTable.AddCell(NestCell)
        myDoc.Add(NestTable)

    End Sub

    Private Function JTasRecordInvoice(tmpEmailSW As String) As String
        ' ---------------------------------------------------------------------------
        ' update invoice number in varmaintenance.
        ' 12/6/2021 --- this code moved to beginning of function from the end. For
        ' some reason the invoice number was not incremented on an invoice where the 
        ' only activity was an AUP deposit. Hope this fixes that.
        ' ---------------------------------------------------------------------------
        If JTjim.JTjimSendToAcctng(txtJTasJobID.Text, txtJTasSubID.Text) = True Then

            JTVarMaint.InvNumber = JTasInvNumber
        Else
            JTVarMaint.AltInvNumber = JTasInvNumber
        End If
        ' =======================================================================
        ' Create loop to update IH records ... especially for 'MSCOMB' invoicing.
        ' =======================================================================
        '
        Dim tmpSub As Integer = 0
        Dim tmpIHR As New JTasIHStru
        Dim InvJobCnt As ICollection = slJTasInvJobs.Keys
        Do While tmpSub < InvJobCnt.Count

            '
            tmpIHR.ihJobID = slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJJobID
            tmpIHR.ihSubID = slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJSubJobID
            '
            tmpIHR.ihCostMeth = slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJCostMeth
            tmpIHR.ihAUPTKsw = slJTasInvJobs(InvJobCnt(tmpSub)).JtasIJTKsw
            tmpIHR.ihAUPMATsw = slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJMATsw
            tmpIHR.ihAUPSERsw = slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJSERsw
            '
            tmpIHR.ihInvDate = mtxtJTasInvDate.Text
            If AUPFinalizeFlag = True Then
                tmpIHR.ihInvNum = "AUPF" & String.Format("{0:000}", Val(JTVarMaint.AUPFinalizeNumber))
                tmpIHR.ihDoNotExpt = "X"
                tmpIHR.ihAcctExptDate = "NA"
            Else
                tmpIHR.ihInvNum = JTasInvNumber
            End If
            tmpIHR.ihAUPInv = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJAUPTl
            tmpIHR.ihLab = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJLabTl + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJLabNPTl
            tmpIHR.ihSub = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJSubTl + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJSubNPTl
            tmpIHR.ihMat = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJMatTl + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJMatNPTl
            tmpIHR.ihIH = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJIHTl + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJIHNPTl
            tmpIHR.ihAdj = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJAdjTl
            '
            tmpIHR.ihEstMrgn = slJTasIJTls(InvJobCnt(tmpSub)).JTasIJLabMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJSubMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJMatMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJIHMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJAdjMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJLabNPMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJSubNPMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJMatNPMrgn _
                                        + slJTasIJTls(InvJobCnt(tmpSub)).JTasIJIHNPMrgn
            '
            ' -------------------------------------------------------------------
            ' Gross up AUP amount by any category excluded from the AUP deal.
            ' -------------------------------------------------------------------
            If slJTasInvJobs(InvJobCnt(tmpSub)).JTasIJCostMeth = "AUP" Then
                '
                tmpIHR.ihInvTl = Val(tmpIHR.ihAUPInv) + Val(tmpIHR.ihAdj)
                If slJTasInvJobs(InvJobCnt(tmpSub)).JtasIJTKsw <> "X" Then
                    tmpIHR.ihInvTl = Val(tmpIHR.ihInvTl) + Val(tmpIHR.ihLab)
                End If
                If slJTasInvJobs(InvJobCnt(tmpSub)).JtasIJMatsw <> "X" Then
                    tmpIHR.ihInvTl = Val(tmpIHR.ihInvTl) + Val(tmpIHR.ihMat) + Val(tmpIHR.ihIH)
                End If
                If slJTasInvJobs(InvJobCnt(tmpSub)).JtasIJSersw <> "X" Then
                    tmpIHR.ihInvTl = Val(tmpIHR.ihInvTl) + Val(tmpIHR.ihSub)
                End If
            Else
                tmpIHR.ihInvTl = Val(tmpIHR.ihLab) + Val(tmpIHR.ihSub) + Val(tmpIHR.ihMat) + Val(tmpIHR.ihIH) + Val(tmpIHR.ihAdj)
            End If
            '
            '  Space out subjob id in invoice file name for MSCOMB invoicing.
            Dim tmpSubJob As String = tmpIHR.ihSubID
            If InvJobCnt.Count > 1 Then
                tmpSubJob = ""
            End If
            If AUPFinalizeFlag = False Then
                ' ----------------------------------------------------------------------------
                ' Code modified to eliminate Job Name for invoice file name. It will only
                ' include the customer name.
                ' Old code below ----
                '        tmpIHR.ihFile = txtJTasJobID.Text & tmpSubJob &
                ' ----------------------------------------------------------------------------
                tmpIHR.ihFile = txtJTasJobID.Text &
                   "_" & mtxtJTasInvDate.Text.Substring(6, 4) & mtxtJTasInvDate.Text.Substring(0, 2) &
                   mtxtJTasInvDate.Text.Substring(3, 2) &
                   "_" & JTasInvNumber & "D.PDF"
                If JTjim.chkboxJTjimSndToAcct.Checked = False Then
                    tmpIHR.ihDoNotExpt = "X"
                End If
                If tmpEmailSW = "Y" Then
                    If CheckForInternetConnection() = True Then
                        tmpIHR.ihEmailedDate = DateTime.Now.ToString("MM/dd/yyyy")
                    End If
                End If
                If tmpEmailSW = "C" Then
                    tmpIHR.ihEmailedDate = "Canceled"
                End If
            Else
                tmpIHR.ihFile = ""
                tmpIHR.ihEmailedDate = "NA"
                tmpIHR.ihDoNotExpt = "X"
            End If
            '
            ' -----------------------------------------------------------------------------------
            ' Code put here to check to see if duplicate job~subjobinv# record are being added. If
            ' this situation happens a message box is displayed to capture the key information. 
            ' The operator can continue after storing the key information for Ed to analyze.
            ' ------------ Code install 3/22/2019 -------------
            ' -----------------------------------------------------------------------------------
            '
            Dim key As ICollection = slJTasIH.Keys
            Dim k As String = ""
            For Each k In key
                If slJTasIH(k).ihJobID = tmpIHR.ihJobID Then
                    If slJTasIH(k).ihSubID = tmpIHR.ihSubID Then
                        If slJTasIH(k).ihInvNum = tmpIHR.ihInvNum Then
                            MsgBox("Invoice being recorded has a duplicate key already on the Inv Hist file." _
                                   & vbCrLf & "Key = " & slJTasIH(k).ihJobID & " ~ " & slJTasIH(k).ihSubID &
                                   " ~ " & slJTasIH(k).ihInvNum & vbCrLf &
                                   "Write down key, give to Ed and continue processing. JTas~3271")
                        End If
                    End If
                End If
            Next
            ' ---------------------------------------------------------
            ' End of special code.
            ' ---------------------------------------------------------

            Dim tmpFileSeq As Integer = 10
            Dim tmpdate As DateTime = CDate(tmpIHR.ihInvDate)
            Dim tmpKeyDate As String = 30000000 - Gregarian2Julian(tmpdate)
            Dim tmpihKey As String = tmpIHR.ihJobID & tmpIHR.ihSubID & tmpKeyDate & Str(tmpFileSeq)
            Do While slJTasIH.ContainsKey(tmpihKey) = True
                tmpFileSeq = +1
                tmpihKey = tmpIHR.ihJobID & tmpIHR.ihSubID & tmpKeyDate & Str(tmpFileSeq)
            Loop
            slJTasIH.Add(tmpihKey, tmpIHR)
            ' Update uninvoiced totals on MM
            Dim tmpLabSignedTl As Double = Val(tmpIHR.ihLab) * -1
            Dim tmpMatSignedTl As Double = (Val(tmpIHR.ihMat) + Val(tmpIHR.ihIH)) * -1
            Dim tmpSubSignedTl As Double = (Val(tmpIHR.ihSub) + Val(tmpIHR.ihAdj)) * -1
            JTMainMenu.JTMMNotInvAmts(tmpIHR.ihJobID, tmpIHR.ihSubID, tmpLabSignedTl, tmpMatSignedTl, tmpSubSignedTl)
            tmpSub += 1
        Loop

        ' ======================================================================
        ' End of loop to update IH records for each job/sub job invoiced.
        ' ======================================================================


        JTVarMaint.JTvmFlush()
        JTasHistFlush()
        ' --------------------------------------------------------------------------------
        ' Update invoice date and inv. # on all activity items that were included on the invoice.
        ' --------------------------------------------------------------------------------
        ' Labor
        tmpSub = 0
        Dim keys As ICollection = slJTasTKKeys.Keys
        '
        Do While tmpSub < JTaslvLab.Items.Count
            If JTaslvLab.Items(tmpSub).Checked = False Then
                Dim tmpItem As New JTtk.JTtkArrayStructure
                tmpItem = JTtk.JTtkslReservoir(slJTasTKKeys(keys(tmpSub)))
                tmpItem.JTtkInvDate = tmpIHR.ihInvDate
                tmpItem.JTtkInvNumber = tmpIHR.ihInvNum
                JTtk.JTtkslReservoir.Remove(slJTasTKKeys(keys(tmpSub)))
                JTtk.JTtkslReservoir.Add((slJTasTKKeys(keys(tmpSub))), tmpItem)
            End If
            tmpSub += 1
        Loop

        ' Materials --- includes IS and IH items
        tmpSub = 0
        Dim keysm As ICollection = slJTasNLCMatKeys.Keys

        Do While tmpSub < JTaslvMat.Items.Count
            Dim tmpItem As New JTnlc.JTnlcStructure
            If JTaslvMat.Items(tmpSub).Checked = False Then


                tmpItem = JTnlc.slJTnlc(slJTasNLCMatKeys(keysm(tmpSub)))
                tmpItem.CustInvDate = tmpIHR.ihInvDate
                tmpItem.CustInvNumber = tmpIHR.ihInvNum
                '
                ' Call to update vendor invoice pdf file name to include cust invoice info ...
                If tmpItem.SupInvImageFile <> "" Then
                    Dim tmpOrigInvFileName As String = tmpItem.Supplier & "_" & tmpItem.SupInvNum & "_" &
                                    tmpItem.InvDate.Substring(6, 4) & tmpItem.InvDate.Substring(0, 2) &
                                    tmpItem.InvDate.Substring(3, 2)
                    JTnlc.JTnlcAddInvInfotoPDFName(tmpItem.SupInvImageFile,
                                                   tmpOrigInvFileName,
                                                   tmpItem.JobID,
                                                   tmpItem.SubID,
                                                   tmpItem.CustInvNumber,
                                                   tmpItem.CustInvDate,
                                                   "")
                End If
                '
                Dim tmpKey As String = slJTasNLCMatKeys(keysm(tmpSub))
                JTnlc.slJTnlc.Remove(slJTasNLCMatKeys(keysm(tmpSub)))
                JTnlc.JTnlcAddSLItem(tmpKey, tmpItem, "JTasRecordInvoice - JTas~3185")
            End If
            tmpSub += 1

        Loop
        ' SubContractors and Adjustments
        Dim keyss As ICollection = slJTasNLCSubKeys.Keys
        tmpSub = 0

        Do While tmpSub < JTaslvSub.Items.Count
            Dim tmpItem As New JTnlc.JTnlcStructure
            If JTaslvSub.Items(tmpSub).Checked = False Then
                tmpItem = JTnlc.slJTnlc(slJTasNLCSubKeys(keyss(tmpSub)))
                tmpItem.CustInvDate = tmpIHR.ihInvDate
                tmpItem.CustInvNumber = tmpIHR.ihInvNum
                '
                ' Call to update vendor invoice pdf file name to include cust invoice info ...
                If tmpItem.SupInvImageFile <> "" Then
                    Dim tmpOrigInvFileName As String = tmpItem.Supplier & "_" & tmpItem.SupInvNum & "_" &
                                    tmpItem.InvDate.Substring(6, 4) & tmpItem.InvDate.Substring(0, 2) & tmpItem.InvDate.Substring(3, 2)
                    JTnlc.JTnlcAddInvInfotoPDFName(tmpItem.SupInvImageFile,
                                                   tmpOrigInvFileName,
                                                   tmpItem.JobID,
                                                   tmpItem.SubID,
                                                   tmpItem.CustInvNumber,
                                                   tmpItem.CustInvDate,
                                                   "")
                End If
                '
                JTnlc.slJTnlc.Remove(slJTasNLCSubKeys(keyss(tmpSub)))
                JTnlc.JTnlcAddSLItem(slJTasNLCSubKeys(keyss(tmpSub)), tmpItem, "JtasRecordInvoice - JTas~3201")
            End If
            tmpSub += 1
        Loop
        '
        ' Marking AUP items as invoiced
        '
        tmpSub = 0
        Dim keysa As ICollection = slJTasAUPKeys.Keys

        Do While tmpSub < JTaslvAUP.Items.Count
            Dim tmpItem As New JTaup.JTaupInvStru
            If JTaslvAUP.Items(tmpSub).Checked = False Then


                tmpItem = JTaup.slJTaupInv(slJTasAUPKeys(keysa(tmpSub)))
                tmpItem.JTaupIInvDate = tmpIHR.ihInvDate
                tmpItem.JTaupIInvNum = tmpIHR.ihInvNum
                JTaup.slJTaupInv.Remove(slJTasAUPKeys(keysa(tmpSub)))
                JTaup.slJTaupInv.Add(slJTasAUPKeys(keysa(tmpSub)), tmpItem)
            End If
            tmpSub += 1

        Loop
        ' clear out arrays from this invoice
        slJTasTK.Clear()
        slJTasNLCMat.Clear()
        slJtasNLCSub.Clear()
        slJTasTKKeys.Clear()
        slJTasNLCMatKeys.Clear()
        slJTasNLCSubKeys.Clear()
        slJTasAUP.Clear()
        slJTasAUPKeys.Clear()
        slJTasInvJobs.Clear()
        slJTasIJTls.Clear()
        'clear out lv arrays
        JTaslvAUP.Items.Clear()
        JTaslvLab.Items.Clear()
        JTaslvMat.Items.Clear()
        JTaslvSub.Items.Clear()
        lvJTasInvHist.Items.Clear()
        Me.Hide()

        ' Write changes to file.
        JTtk.TKArrayFlush()
        JTnlc.JTnlcArrayFlush("")
        JTaup.JTaupFlush()
        Return ""
    End Function
    Sub JTasEmailInvoice(tmpToAdd As String, filename As String)
        Me.Hide()
        Dim tmpCC As String = JTVarMaint.JTCCEmailAddress
        Dim tmpBCC As String = ""
        Dim tmpFromAdd As String = JTVarMaint.JTFromEmailAddress
        Dim tmpSubject As String = JTVarMaint.CompanyNme & " - Invoice # " & JTasInvNumber
        Dim tmpMsg As String = "Attached is Invoice # " & JTasInvNumber & "  " & String.Format("{0:MM/ dd / yyyy}", mtxtJTasInvDate.Text)
        Dim tmpAttach As String = filename
        JTEmail.JTSndEmail(tmpToAdd, tmpCC, tmpBCC, tmpFromAdd, tmpSubject, tmpMsg, tmpAttach)
    End Sub
    ''' <summary>
    ''' This func create a sl that contains all the job/subjob that are included on the invoice.
    '''  The sl will be used to organize and sequence the invoice.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <returns></returns>
    Function JTasIJUpdt(tmpJobID As String, tmpSubID As String) As String
        '
        Dim tmpkey As String = tmpJobID & tmpSubID
        If slJTasInvJobs.ContainsKey(tmpkey) Then
            Return "Duplicate"
        End If

        Dim tmpNewItem As JTasIJstru = Nothing
        tmpNewItem.JTasIJJobID = tmpJobID
        tmpNewItem.JTasIJSubJobID = tmpSubID
        ' func to get information from JTjim sl
        JTjim.JTjimInvIDUpdt(tmpNewItem)
        slJTasInvJobs.Add(tmpkey, tmpNewItem)
        Return "New"
    End Function
    Function JTasIJTlsClr(tmpNewItem As JTasIJTstru) As String
        tmpNewItem.JTasIJAUPTl = 0
        tmpNewItem.JTasIJLabTl = 0
        tmpNewItem.JTasIJSubTl = 0
        tmpNewItem.JTasIJMatTl = 0
        tmpNewItem.JTasIJIHTl = 0
        tmpNewItem.JTasIJAdjTl = 0
        tmpNewItem.JTasIJGrdTl = 0
        tmpNewItem.JTasIJAUPMrgn = 0
        tmpNewItem.JTasIJLabMrgn = 0
        tmpNewItem.JTasIJSubMrgn = 0
        tmpNewItem.JTasIJMatMrgn = 0
        tmpNewItem.jtasIJIHMrgn = 0
        tmpNewItem.JTasIJAdjMrgn = 0
        tmpNewItem.JTasIJMrgnTl = 0
        ' Fields used to store detail amounts covered by AUP charge.
        tmpNewItem.JTasIJLabNPTl = 0
        tmpNewItem.JTasIJSubNPTl = 0
        tmpNewItem.JTasIJMatNPTl = 0
        tmpNewItem.JTasIJIHNPTl = 0
        tmpNewItem.JTasIJAdjNPTl = 0
        tmpNewItem.JTasIJLabNPMrgn = 0
        tmpNewItem.JTasIJSubNPMrgn = 0
        tmpNewItem.JTasIJMatNPMrgn = 0
        tmpNewItem.jtasIJIHNPMrgn = 0
        tmpNewItem.JTasIJAdjNPMrgn = 0
        Return "DONE"
    End Function
    Public Function JTasUNotExportedInvoices(ByRef tmpCount As Decimal, ByRef tmpAmount As Double,
                                             ByRef tmpNotEmailed As Integer) As String
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihAcctExptDate = "" Then
                tmpCount += 1
                tmpAmount += Val(slJTasIH(k).ihInvTl)
            End If
            If slJTasIH(k).ihEmailedDate = "" Then
                tmpNotEmailed += 1
            End If
        Next
        Return ""
    End Function
    Public Function JTasJobLstInvoice(tmpJob As String, tmpSub As String) As String
        Dim tmpLstDate As String = ""
        If slJTasIH.Count = 0 Then
            JTasHistLoad()
        End If
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihJobID = tmpJob And slJTasIH(k).ihSubID = tmpSub Then
                If tmpLstDate = "" Then
                    tmpLstDate = slJTasIH(k).ihInvDate
                ElseIf CDate(tmpLstDate) < CDate(slJTasIH(k).ihInvDate) Then
                    tmpLstDate = slJTasIH(k).ihInvDate
                End If
            End If
        Next
        Return tmpLstDate
    End Function
    Public Function JTasChkEmail(tmpPDFName As String) As Boolean
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihfile = tmpPDFName Then
                If slJTasIH(k).ihemaileddate = "" Then
                    Return False
                Else
                    Return True
                End If
            End If
        Next
        Return True
    End Function
    Public Function JTasInvEmailInfo(InvPDFName As String,
                                     ByRef tmpJob As String,
                                     ByRef tmpSubJob As String,
                                     ByRef tmpInvNumber As String,
                                     ByRef tmpInvDate As String) As String
        ' ------------------------------------------------------------------------------
        ' Function gets elements of data from slJTasIH to support UTIL Invoice emailing.
        ' ------------------------------------------------------------------------------
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihfile = InvPDFName Then
                tmpJob = slJTasIH(k).ihJobID
                tmpSubJob = slJTasIH(k).ihSubID
                tmpInvNumber = slJTasIH(k).ihInvNum
                tmpInvDate = slJTasIH(k).ihInvDate
                Return ""
            End If
        Next
        Return ""
    End Function
    Public Function JTasUpdtInvEmailDt(InvPDFName As String, tmpEmailDate As String)
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihfile = InvPDFName Then
                Dim tmpJTasIHITem As JTasIHStru = slJTasIH(k)
                tmpJTasIHITem.ihEmailedDate = tmpEmailDate
                slJTasIH.Remove(k)
                slJTasIH.Add(k, tmpJTasIHITem)
                Return ""
            End If
        Next
        Return ""
    End Function
    Public Function JTasHasBeenInvd(tmpJob As String, tmpSubJob As String,
                                    ByRef tmpLstInvDate As String) As Boolean
        ' ---------------------------------------------------------------------------
        ' Function checks to see if a job has been invoiced. Used by Job Maintenance.
        ' ---------------------------------------------------------------------------
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihJobid = tmpJob And slJTasIH(k).ihSubid = tmpSubJob Then
                tmpLstInvDate = slJTasIH(k).ihInvDate
                Return True
            End If
        Next
        Return False
    End Function
    Public Function JTasINVExt(ByRef tmpRprtName As String, ByRef tmpExptMode As String, ByRef tmpCurrRow As Integer,
                               ByRef tmpSLkey As String, ByRef tmpSLihItem As JTasIHStru) As String
        ' ---------------------------------------------------------------------
        ' Function to extract data for invoices being exported to acctng. Logic
        ' to export normal series ("1") and alt series ("8") based on switch.
        ' ---------------------------------------------------------------------
        Dim ihkey As ICollection = slJTasIH.Keys

        Do While tmpCurrRow < ihkey.Count
            Dim tmpinvNum As String = slJTasIH(ihkey(tmpCurrRow)).ihinvnum
            Dim tmpAcctExptDate As String = slJTasIH(ihkey(tmpCurrRow)).ihAcctExptDate
            '
            If slJTasIH(ihkey(tmpCurrRow)).ihAcctExptDate = "" Then
                Select Case tmpExptMode
                    Case "1"
                        If Val(tmpinvNum) < 8000 Then
                            tmpSLkey = ihkey(tmpCurrRow)
                            tmpSLihItem = slJTasIH(ihkey(tmpCurrRow))
                            tmpCurrRow += 1
                            Return "DATA"
                        End If
                    Case "8"
                        If Val(tmpinvNum) > 7999 Then
                            tmpSLkey = ihkey(tmpCurrRow)
                            tmpSLihItem = slJTasIH(ihkey(tmpCurrRow))
                            tmpCurrRow += 1
                            Return "DATA"
                        End If
                End Select
            End If
            tmpCurrRow += 1
        Loop
        Return "DONE"
    End Function
    Public Function JTasInvExptDateUpdt(tmpKey As String, tmpExptDate As String) As String
        ' ---------------------------------------------------------------------
        ' Function to update Acct Expt Date for exported invoices.
        ' ---------------------------------------------------------------------
        If slJTasIH.ContainsKey(tmpKey) Then
            Dim tmpItem As JTasIHStru
            tmpItem = slJTasIH(tmpKey)
            slJTasIH.Remove(tmpKey)
            tmpItem.ihAcctExptDate = tmpExptDate
            slJTasIH.Add(tmpKey, tmpItem)
        Else
            MsgBox("Error updating invoice acctng extract date --- JTas ~3693")
        End If
        Return ""
    End Function
    Public Function JTasEarliestInvoiceDate(ByRef StMonth As String, ByRef StYear As String) As String
        ' ------------------------------------------------------------
        ' Searches slJTasIH to find the earliest invoice date.
        ' ------------------------------------------------------------
        Dim tmpEarliestJul As String = "9999999"
        Dim tmpEarliestGreg As String = ""
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If Gregarian2Julian(slJTasIH(k).ihInvDate) < tmpEarliestJul Then
                tmpEarliestJul = Gregarian2Julian(slJTasIH(k).ihInvDate)
                tmpEarliestGreg = slJTasIH(k).ihInvDate
            End If
        Next
        Dim tmpDate As Date = CDate(tmpEarliestGreg)
        StMonth = tmpDate.Month
        StYear = tmpDate.Year
        Return ""
    End Function
    Public Function JTasirExt(tmpRprtName As String, tmpExptMode As String,
                              ByRef tmpCurrRow As Integer, tmpStMth As String,
                              tmpStYr As String, tmpEndMth As String, tmpendyr As String,
                              ByRef tmpSLihItem As JTasIHStru) As String
        ' ---------------------------------------------------------------------
        ' Function to extract data for invoices being exported for Invoice History
        ' Report. Invoices are selected by invoice number and a beginning and
        ' ending date range.
        ' ---------------------------------------------------------------------
        Dim ihkey As ICollection = slJTasIH.Keys
        '
        Dim tmpStYrMth As String = tmpStYr & tmpStMth
        Dim tmpEndYrMth As String = tmpendyr & tmpEndMth
        '
        Do While tmpCurrRow < ihkey.Count
            Dim tmpinvNum As String = slJTasIH(ihkey(tmpCurrRow)).ihInvNum
            Dim tmpInvDate As Date = CDate(slJTasIH(ihkey(tmpCurrRow)).ihInvDate)
            Dim tmpInvMth As String = String.Format("{0:00}", tmpInvDate.Month)
            Dim tmpInvYr As String = String.Format("{0:0000}", tmpInvDate.Year)
            Dim tmpInvYrMth As String = tmpInvYr & tmpInvMth
            If tmpInvYrMth >= tmpStYrMth And tmpInvYrMth <= tmpEndYrMth Then
                '
                Select Case tmpExptMode
                    Case "P"
                        If Val(tmpinvNum) < 8000 Then
                            tmpSLihItem = slJTasIH(ihkey(tmpCurrRow))
                            tmpCurrRow += 1
                            Return "DATA"
                        End If
                    Case "A"
                        If Val(tmpinvNum) > 7999 Then
                            tmpSLihItem = slJTasIH(ihkey(tmpCurrRow))
                            tmpCurrRow += 1
                            Return "DATA"
                        End If
                    Case Else
                        tmpSLihItem = slJTasIH(ihkey(tmpCurrRow))
                        tmpCurrRow += 1
                        Return "DATA"
                End Select
            End If
            tmpCurrRow += 1
        Loop
        Return "DONE"
    End Function
    ''' <summary>
    ''' This function retrieves records from the slJtisIH table. It is used to load the invoice history for a single job 
    ''' on the JTas panel. It is also used to extract invoices for the JTae routines.
    ''' </summary>
    ''' <param name="tmpSub">Control the subscript for reading the table</param>
    ''' <param name="tmpJob">Specifies the job to be extracted. If " ", all jobs are returned.</param>
    ''' <param name="tmpJTasItem">This is the complete slJTasIN record.</param>
    ''' <param name="tmpJTasKey">This variable contains the slJTasIH record key.</param>
    ''' <returns></returns>
    Public Function JTasJRExt(ByRef tmpSub As Integer, tmpJob As String, ByRef tmpJTasItem As JTasIHStru, ByRef tmpJTasKey As String)
        Dim key As ICollection = slJTasIH.Keys
        Do While tmpSub < slJTasIH.Count
            If tmpJob = "" Then    ' no tmpJob sent --- return all records
                tmpJTasItem = slJTasIH(key(tmpSub))
                tmpJTasKey = key(tmpSub)
                tmpSub += 1
                Return "DATA"
            Else
                If slJTasIH(key(tmpSub)).ihjobid = tmpJob Then
                tmpJTasItem = slJTasIH(key(tmpSub))
                tmpJTasKey = key(tmpSub)
                tmpSub += 1
                    Return "DATA"
                End If
            End If
            tmpSub += 1
        Loop
        Return "DONE"
    End Function
    Public Function JTasUpdtEmailDate(tmpJob As String, tmpInvNo As String,
                                      tmpEmailDate As String) As String

        Dim tmpSearchSW As String = "Looking"
        Do While tmpSearchSW = "Looking"
            Dim keys As ICollection = slJTasIH.Keys
            Dim k As String = ""
            For Each k In keys
                If slJTasIH(k).ihJobID = tmpJob And slJTasIH(k).ihInvNum = tmpInvNo Then
                    Dim tmpIHR As JTasIHStru = slJTasIH(k)
                    slJTasIH.Remove(k)
                    tmpIHR.ihEmailedDate = tmpEmailDate
                    slJTasIH.Add(k, tmpIHR)
                    k = ""
                    Exit For
                End If
            Next
            If k = "" Then
                tmpSearchSW = "DoneLooking"
            End If
        Loop
        JTasHistFlush()
        Return "DONE"
    End Function
    ''' <summary>
    ''' This function builds the "to" email address for outgoing invoice emails.
    ''' It is used by the email function executed when the invoice is being recorded
    ''' and also in the invoice review section where it can be re-emailed.
    ''' </summary>
    ''' <returns>The "to" email address is returned formatted for the message.</returns>
    Public Function JTasBldEmailToAdd() As String
        ' --------------------------------------------------------------------------------
        ' Retrieve customer information
        ' --------------------------------------------------------------------------------
        Dim tmpCustinfo As JTjim.JTjimCustInfo = Nothing
        JTjim.JTjimGetBillingInfo(txtJTasJobID.Text, tmpCustinfo)
        '
        Dim tmpToAdd As String = ""
        If tmpCustinfo.JTjimCIInvEmailPrim = "X" Then
            tmpToAdd = tmpCustinfo.JTjimCIEmail
        End If
        If tmpCustinfo.JTjimCIInvEmailAlt = "X" Then
            If tmpCustinfo.JTjimCISecEmail <> "" Then
                If tmpToAdd <> "" Then
                    tmpToAdd = tmpToAdd & ", " & tmpCustinfo.JTjimCISecEmail
                Else
                    tmpToAdd = tmpCustinfo.JTjimCISecEmail
                End If
            End If
        End If
        tmpCustinfo = Nothing
        Return tmpToAdd
    End Function
    ''' <summary>
    ''' Function to count current number of Invoices pending accounting extract.
    ''' </summary>
    ''' <returns>Returns an integer representing # pending extract.</returns>
    Public Function JTasQBExtractchk() As Integer
        Dim tmpCount As Integer = 0
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihAcctExptDate = "" Then
                '
                tmpCount += 1
            End If
        Next
        Return tmpCount
    End Function
    Public Function JtasPurgeJobInvHistory(tmpjob As String, tmpsubjob As String) As String
        Dim tmpPurgeStatus As String = "NOTDONE"
        Dim tmpPurgeCnt As Integer = 0
        Do While tmpPurgeStatus <> "DONE"
            tmpPurgeStatus = JtasPurgeJobInvHistoryLoop(tmpjob, tmpsubjob, tmpPurgeCnt)
        Loop
        '
        JTasHistFlush()
        '
        Return "PURGED"
    End Function
    Private Function JTasPurgeJobInvHistoryLoop(tmpjob As String, tmpsubjob As String,
                                                ByRef tmpPurgeCnt As Integer) As String
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = ""
        For Each k In key
            If slJTasIH(k).ihJobID = tmpjob Then
                If slJTasIH(k).ihSubID = tmpsubjob Then
                    slJTasIH.Remove(k)
                    tmpPurgeCnt += 1
                    Return "NOTDONE"
                End If
            End If
        Next
        '
        Return "DONE"
    End Function
    Public Function JTasFillCell(ArrayData As String, ByRef table As PdfPTable,
                                Position As Integer, Span As Integer,
                                Border As Integer, Color As String) As String
        If ArrayData = "" Then
            ArrayData = " "
        End If
        Dim CellContents = New PdfPCell(New Phrase(FP_H8(ArrayData)))
        CellContents.HorizontalAlignment = Position
        CellContents.Colspan = Span
        '
        If Border = 0 Then
            CellContents.Border = Border
        End If
        If Color <> "" Then
            Select Case Color.ToUpper
                Case "YELLOW"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(249, 231, 159)  ' Yellow
                Case "BLUE"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(0, 0, 255)
                Case "PALEBLUE"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(222, 234, 244)
                Case "GREEN"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(143, 188, 139)  ' Yellow
                Case "RED"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(255, 0, 0)
            End Select
        End If
        '

        table.AddCell(CellContents)
        Return ""
    End Function
    Public Function JTasAUPPostedActivity(tmpCustomer As String, tmpJob As String, ByRef tmpPostedActivity As String) As String
        Dim tmpPostedAmounts As Decimal = 0
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = Nothing
        For Each k In key
            If tmpCustomer = slJTasIH(k).ihJobID Then
                If tmpJob = slJTasIH(k).ihSubID Then
                    tmpPostedAmounts = tmpPostedAmounts + Val(slJTasIH(k).ihLab) + Val(slJTasIH(k).ihMat) + Val(slJTasIH(k).ihSub) +
                        Val(slJTasIH(k).ihAdj) + Val(slJTasIH(k).ihIH)
                End If
            End If
        Next
        tmpPostedActivity = String.Format("{0:0.00}", tmpPostedAmounts)
        Return ""
    End Function
    ''' <summary>
    ''' Function to make customer and/or job names changes. Used by JTjm.
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTasCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String
        ' --------------------------------------------------
        ' Start of slJTasIH section
        ' --------------------------------------------------
        '
        Dim tmpKeyCollection As New List(Of String)
        '
        Dim key As ICollection = slJTasIH.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTasIH(k).ihJobID Then
                    tmpKeyCollection.Add(k)
                End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTasIH(k).ihJobID Then
                    If JTjmSettings.ExJob = slJTasIH(k).ihSubID Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
        End If
        ' Modify logic for slJTasIH placed here .....
        Dim tmpFileSeq As Integer = 10
        Dim tmpSub As Integer = 0
        '
        Do While tmpSub < tmpKeyCollection.Count
            k = tmpKeyCollection(tmpSub)
            Dim tmpItem As JTasIHStru = slJTasIH(k)
            slJTasIH.Remove(k)
            '
            tmpItem.ihJobID = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.ihSubID = JTjmSettings.NewJob
            End If
            '
            Dim tmpdate As Date = CDate(tmpItem.ihInvDate)
            Dim tmpKeyDate As String = 30000000 - Gregarian2Julian(tmpdate)
            '
            Dim tmpKey As String = tmpItem.ihJobID & tmpItem.ihSubID & tmpKeyDate & Str(tmpFileSeq)
            Do While slJTasIH.ContainsKey(tmpKey) = True
                tmpFileSeq += 1
                tmpKey = tmpItem.ihJobID & tmpItem.ihSubID & tmpKeyDate & Str(tmpFileSeq)
            Loop
            slJTasIH.Add(tmpKey, tmpItem)
            '
            tmpSub += 1
        Loop
        ' --------------------------------------------------
        ' End of slJTasIH section
        ' --------------------------------------------------
        Return ""
    End Function
    ''' <summary>
    ''' Function to update the accounting extract date on slJtasIH. Called from JTae.
    ''' </summary>
    ''' <param name="tmpKey">slJTasIH key of record being modified.</param>
    ''' <returns></returns>
    Public Function JTasUpdtExtrctDate(tmpKey As String) As Boolean
        If slJTasIH.ContainsKey(tmpKey) Then
            Dim tmpSLItem As JTasIHStru
            tmpSLItem = slJTasIH(tmpKey)
            slJTasIH.Remove(tmpKey)
            tmpSLItem.ihAcctExptDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
            slJTasIH.Add(tmpKey, tmpSLItem)
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Function to pass InvHist file to find out what file name was actually used
    ''' when a combined invoice was produced. This routine is necessary to display
    ''' the PDF from a job other than the job named in the file name.
    ''' </summary>
    ''' <param name="tmpInvNum"></param>
    ''' <param name="tmpfilename"></param>
    ''' <returns></returns>
    Public Function JTasFindFileName(tmpInvNum As String, ByRef tmpfilename As String) As Boolean
        Dim tmpInvoiceYear As String = ""
        If tmpfilename.Contains("_") Then
            Dim tmpCharPos As Short = tmpfilename.IndexOf("_")
            tmpInvoiceYear = tmpfilename.Substring(tmpCharPos, 4)
        Else
            Return False
        End If
        For Each fileName As String In Directory.GetFiles(JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & tmpInvoiceYear & "\", "*.PDF")
            If fileName.Contains(tmpInvNum) Then
                tmpfilename = fileName
                Return True
            End If

        Next
        tmpfilename = ""
        Return False
        '
    End Function
    ''' <summary>
    ''' Function to check to see if invoice pdf file exists with invoice number about to be used.
    ''' This situation could occur if the system crashed during invoice production. If a file is found,
    ''' the number is checked against the InvHist DB. If a record exists, the operator is notified to 
    ''' stop and resolve issue. If no record exists ... likely prior system crash ... the file is deleted.
    ''' </summary>
    ''' <param name="tmpInvNumberQuery">Invoice number system want to assign for next invoice</param>
    ''' <returns></returns>
    Public Function JTasChkforDupInvoiceNumber(tmpInvNumberQuery As String) As Boolean
        Dim tmpYear As String = Date.Today.Year
        Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & tmpYear & "\", "*.PDF")
        '    Process the list of .PDF files found in the directory. 
        Dim tmpInvNumberFound As String = Nothing
        Dim fileName As String
        Dim tmpFoundFile As Boolean = False
        For Each fileName In fileEntries
            If fileName.Substring(fileName.Length - 11, 11) = "_Voided.PDF" Then
                tmpInvNumberFound = fileName.Substring(fileName.Length - 16, 4)
            Else
                tmpInvNumberFound = fileName.Substring(fileName.Length - 9, 4)
            End If
            If tmpInvNumberFound = tmpInvNumberQuery Then
                ' code to check on duplicate number found. Code trims filename path information.
                Dim tmpJustFileName As String = fileName.Substring(fileName.IndexOf("CustInvoicePDF\") + 20)
                Dim key As ICollection = slJTasIH.Keys
                Dim k As String = Nothing
                For Each k In key
                    If slJTasIH(k).ihFile = tmpJustFileName Then
                        MsgBox("Invoice number reserved for next invoice appears to have already" & vbCrLf &
                               "been used. An invoice PDF file exists and an Invoice History Record" & vbCrLf &
                               "exists. This would signal that an invoice was produced and recorded." & vbCrLf &
                               "Resolve the issue before proceeding with new invoicing." & vbCrLf & vbCrLf &
                               "Invoice number with duplicate entry - >" & tmpInvNumberFound & "<" & vbCrLf &
                               "File name found - >" & tmpJustFileName & "<" & vbCrLf & vbCrLf &
                               "See system manual for further explanation. AS.005")
                        Return True     'Found a problem.
                    End If
                Next
                ' --------------------------------------------------------------------------------
                ' Delete junk pdf file here.
                ' This situation will occur if an invoice file is created for review and the
                ' review process to accept or abort this invoice was not completed. It could
                ' also be due to an application or system failure.
                ' --- Not a big deal because PDF was never recorded as an active invoice. ---
                ' --------------------------------------------------------------------------------
                My.Computer.FileSystem.DeleteFile(fileName)
                Return False
            End If
        Next
        Return False
    End Function
    ' ------------------------------------------------------------------------------------
    ' Sub deleted ... can't see what its doing ... looking for why accounts are getting
    ' deactivated after invoicing 3/3/2020.
    '    Private Sub rbtnJTasDeact_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTasDeact.CheckedChanged
    '       rbtnJTasDeact.Enabled = True
    '  End Sub
    ''' <summary>
    ''' Sub to set up key for line item correction ---> JTnlcE --- Mat
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvmat_Click(sender As Object, e As EventArgs) Handles JTaslvMat.Click
        ' ------------------------------------------------------------------
        ' This sub is triggered when a check is changed on a line entry.
        ' The checks are used to excluded items from the invoice. THe logic
        ' will be wrapped in an 'if' to make sure the program enter this sub
        ' for the intended reason.
        ' ------------------------------------------------------------------
        If JTaslvMat.SelectedItems.Count > 0 Then
            Dim tmpKeys As ICollection = slJTasNLCMatKeys.Keys
            Dim tmpLookUpKey As String = tmpKeys(JTaslvMat.SelectedItems(0).Index)
            JTasItemCorrectionNLCKey = slJTasNLCMatKeys(tmpLookUpKey)
            btnJTasLineItemCorr.Visible = True
        End If
    End Sub
    ''' <summary>
    ''' Sub to set up key for line item correction ---> JTnlcE --- Services
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub JTaslvSub_Click(sender As Object, e As EventArgs) Handles JTaslvSub.Click
        ' ------------------------------------------------------------------
        ' This sub is triggered when a check is changed on a line entry.
        ' The checks are used to excluded items from the invoice. THe logic
        ' will be wrapped in an 'if' to make sure the program enter this sub
        ' for the intended reason.
        ' ------------------------------------------------------------------
        If JTaslvSub.SelectedItems.Count > 0 Then
            Dim tmpKeys As ICollection = slJTasNLCSubKeys.Keys
            Dim tmpLookUpKey As String = tmpKeys(JTaslvSub.SelectedItems(0).Index)
            JTasItemCorrectionNLCKey = slJTasNLCSubKeys(tmpLookUpKey)
            btnJTasLineItemCorr.Visible = True
        End If
    End Sub
    ''' <summary>
    ''' Sub to send key to JTnlcE for line item correction.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTasLineItemCorr_Click(sender As Object, e As EventArgs) Handles btnJTasLineItemCorr.Click
        If JTasItemCorrectionNLCKey <> "" Then
            Me.Hide()
            JTnlcE.JtnlcEInit("REVMODAS", JTasItemCorrectionNLCKey)
        End If
    End Sub




    '
End Class