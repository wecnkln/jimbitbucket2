﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JTMainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.lvMMJobs = New System.Windows.Forms.ListView()
        Me.lvMMJobsJobName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMJobsPndAmt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMJobsType = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMJobsCstMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMtk = New System.Windows.Forms.ListView()
        Me.lvJTjimEmpID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvlJTTKDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.JTmmJobPanelLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.lvMMNLC = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LastAct = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AcctOpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DocProb = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cboxJTmmEmpMnt = New System.Windows.Forms.CheckBox()
        Me.cboxJTmmNewJob = New System.Windows.Forms.CheckBox()
        Me.cboxJTmmNewVendor = New System.Windows.Forms.CheckBox()
        Me.cboxJTmmShowDeact = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbtnJTmmJSAlpha = New System.Windows.Forms.RadioButton()
        Me.rbtnJTMMJSAmt = New System.Windows.Forms.RadioButton()
        Me.btnJTmmNLC = New System.Windows.Forms.Button()
        Me.btnJTmmExit = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboxJTmmJobList = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cboxJTmmDispAUP = New System.Windows.Forms.CheckBox()
        Me.cboxJTmmDispCostP = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboxJTmmPrntVendList = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.JTmmNLCLabel = New System.Windows.Forms.Label()
        Me.lvMMCstP = New System.Windows.Forms.ListView()
        Me.lvMMCstPJob = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMCstPPendInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMCstPLstInvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMCstPPrevInvd = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMAUP = New System.Windows.Forms.ListView()
        Me.lvMMAUPJob = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMAUPPendInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMAUPrevInvd = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMMAUPAupAmt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnJTmmInvng = New System.Windows.Forms.Button()
        Me.btnJTmmAUPSum = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.TextBox1.Location = New System.Drawing.Point(12, 4)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(176, 38)
        Me.TextBox1.TabIndex = 10
        Me.TextBox1.Text = "Main Menu"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvMMJobs
        '
        Me.lvMMJobs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvMMJobsJobName, Me.lvMMJobsPndAmt, Me.lvMMJobsType, Me.lvMMJobsCstMeth})
        Me.lvMMJobs.FullRowSelect = True
        Me.lvMMJobs.HideSelection = False
        Me.lvMMJobs.Location = New System.Drawing.Point(328, 66)
        Me.lvMMJobs.Margin = New System.Windows.Forms.Padding(4)
        Me.lvMMJobs.Name = "lvMMJobs"
        Me.lvMMJobs.Size = New System.Drawing.Size(418, 250)
        Me.lvMMJobs.TabIndex = 15
        Me.lvMMJobs.UseCompatibleStateImageBehavior = False
        Me.lvMMJobs.View = System.Windows.Forms.View.Details
        '
        'lvMMJobsJobName
        '
        Me.lvMMJobsJobName.Text = "Job[~SubJob]"
        Me.lvMMJobsJobName.Width = 170
        '
        'lvMMJobsPndAmt
        '
        Me.lvMMJobsPndAmt.Text = "Pend.Inv."
        Me.lvMMJobsPndAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMMJobsPndAmt.Width = 70
        '
        'lvMMJobsType
        '
        Me.lvMMJobsType.Text = ""
        Me.lvMMJobsType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMMJobsType.Width = 30
        '
        'lvMMJobsCstMeth
        '
        Me.lvMMJobsCstMeth.Text = ""
        Me.lvMMJobsCstMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMMJobsCstMeth.Width = 48
        '
        'lvMMtk
        '
        Me.lvMMtk.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvJTjimEmpID, Me.lvlJTTKDate})
        Me.lvMMtk.FullRowSelect = True
        Me.lvMMtk.HideSelection = False
        Me.lvMMtk.Location = New System.Drawing.Point(119, 65)
        Me.lvMMtk.Margin = New System.Windows.Forms.Padding(4)
        Me.lvMMtk.Name = "lvMMtk"
        Me.lvMMtk.Size = New System.Drawing.Size(201, 250)
        Me.lvMMtk.TabIndex = 20
        Me.lvMMtk.UseCompatibleStateImageBehavior = False
        Me.lvMMtk.View = System.Windows.Forms.View.Details
        '
        'lvJTjimEmpID
        '
        Me.lvJTjimEmpID.Text = "Emp. ID"
        Me.lvJTjimEmpID.Width = 70
        '
        'lvlJTTKDate
        '
        Me.lvlJTTKDate.Text = "Last TK Recd'd"
        Me.lvlJTTKDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvlJTTKDate.Width = 90
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(119, 423)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(201, 60)
        Me.TextBox3.TabIndex = 21
        Me.TextBox3.Text = "Double Click Employee to enter or review  timekeeping."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(120, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 20)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Time Keeping"
        '
        'JTmmJobPanelLabel
        '
        Me.JTmmJobPanelLabel.AutoSize = True
        Me.JTmmJobPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JTmmJobPanelLabel.Location = New System.Drawing.Point(327, 41)
        Me.JTmmJobPanelLabel.Name = "JTmmJobPanelLabel"
        Me.JTmmJobPanelLabel.Size = New System.Drawing.Size(429, 20)
        Me.JTmmJobPanelLabel.TabIndex = 30
        Me.JTmmJobPanelLabel.Text = "Job Entry, Maintenance , Invoicing, Etc. (All Jobs)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(760, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(244, 20)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "NLC - Vendors, Contractors"
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(754, 423)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(407, 60)
        Me.TextBox4.TabIndex = 36
        Me.TextBox4.Text = "Double Click  to Review, Update or Delete Vendor. Check 'New Vendor' to add a new" &
    " supplier. Entry in Issues column denoted 'Lic' or 'Ins' issues. This should be " &
    "resolved."
        '
        'lvMMNLC
        '
        Me.lvMMNLC.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.Type, Me.LastAct, Me.AcctOpt, Me.DocProb})
        Me.lvMMNLC.FullRowSelect = True
        Me.lvMMNLC.HideSelection = False
        Me.lvMMNLC.Location = New System.Drawing.Point(753, 66)
        Me.lvMMNLC.Name = "lvMMNLC"
        Me.lvMMNLC.Size = New System.Drawing.Size(408, 250)
        Me.lvMMNLC.TabIndex = 37
        Me.lvMMNLC.UseCompatibleStateImageBehavior = False
        Me.lvMMNLC.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Name"
        Me.ColumnHeader2.Width = 110
        '
        'Type
        '
        Me.Type.Text = "Type"
        Me.Type.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LastAct
        '
        Me.LastAct.Text = "Lst Act"
        Me.LastAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LastAct.Width = 79
        '
        'AcctOpt
        '
        Me.AcctOpt.Text = "Acct Opt"
        Me.AcctOpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DocProb
        '
        Me.DocProb.Text = "Issues"
        Me.DocProb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cboxJTmmEmpMnt
        '
        Me.cboxJTmmEmpMnt.AutoSize = True
        Me.cboxJTmmEmpMnt.Location = New System.Drawing.Point(34, 27)
        Me.cboxJTmmEmpMnt.Name = "cboxJTmmEmpMnt"
        Me.cboxJTmmEmpMnt.Size = New System.Drawing.Size(106, 36)
        Me.cboxJTmmEmpMnt.TabIndex = 41
        Me.cboxJTmmEmpMnt.Text = "Employee" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Maintenance"
        Me.cboxJTmmEmpMnt.UseVisualStyleBackColor = True
        '
        'cboxJTmmNewJob
        '
        Me.cboxJTmmNewJob.AutoSize = True
        Me.cboxJTmmNewJob.Location = New System.Drawing.Point(6, 9)
        Me.cboxJTmmNewJob.Name = "cboxJTmmNewJob"
        Me.cboxJTmmNewJob.Size = New System.Drawing.Size(59, 36)
        Me.cboxJTmmNewJob.TabIndex = 42
        Me.cboxJTmmNewJob.Text = "New " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Job"
        Me.cboxJTmmNewJob.UseVisualStyleBackColor = True
        '
        'cboxJTmmNewVendor
        '
        Me.cboxJTmmNewVendor.AutoSize = True
        Me.cboxJTmmNewVendor.Location = New System.Drawing.Point(68, 39)
        Me.cboxJTmmNewVendor.Name = "cboxJTmmNewVendor"
        Me.cboxJTmmNewVendor.Size = New System.Drawing.Size(103, 20)
        Me.cboxJTmmNewVendor.TabIndex = 43
        Me.cboxJTmmNewVendor.Text = "New Vendor"
        Me.cboxJTmmNewVendor.UseVisualStyleBackColor = True
        '
        'cboxJTmmShowDeact
        '
        Me.cboxJTmmShowDeact.AutoSize = True
        Me.cboxJTmmShowDeact.Location = New System.Drawing.Point(7, 57)
        Me.cboxJTmmShowDeact.Name = "cboxJTmmShowDeact"
        Me.cboxJTmmShowDeact.Size = New System.Drawing.Size(171, 20)
        Me.cboxJTmmShowDeact.TabIndex = 44
        Me.cboxJTmmShowDeact.Text = "Show Deactivated Jobs"
        Me.cboxJTmmShowDeact.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox1.Controls.Add(Me.rbtnJTmmJSAlpha)
        Me.GroupBox1.Controls.Add(Me.rbtnJTMMJSAmt)
        Me.GroupBox1.Location = New System.Drawing.Point(261, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(147, 78)
        Me.GroupBox1.TabIndex = 45
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Display Sequence"
        '
        'rbtnJTmmJSAlpha
        '
        Me.rbtnJTmmJSAlpha.AutoSize = True
        Me.rbtnJTmmJSAlpha.Location = New System.Drawing.Point(6, 17)
        Me.rbtnJTmmJSAlpha.Name = "rbtnJTmmJSAlpha"
        Me.rbtnJTmmJSAlpha.Size = New System.Drawing.Size(63, 20)
        Me.rbtnJTmmJSAlpha.TabIndex = 0
        Me.rbtnJTmmJSAlpha.TabStop = True
        Me.rbtnJTmmJSAlpha.Text = "Alpha"
        Me.rbtnJTmmJSAlpha.UseVisualStyleBackColor = True
        '
        'rbtnJTMMJSAmt
        '
        Me.rbtnJTMMJSAmt.AutoSize = True
        Me.rbtnJTMMJSAmt.Location = New System.Drawing.Point(6, 38)
        Me.rbtnJTMMJSAmt.Name = "rbtnJTMMJSAmt"
        Me.rbtnJTMMJSAmt.Size = New System.Drawing.Size(104, 20)
        Me.rbtnJTMMJSAmt.TabIndex = 1
        Me.rbtnJTMMJSAmt.TabStop = True
        Me.rbtnJTMMJSAmt.Text = "Unbilled Amt"
        Me.rbtnJTMMJSAmt.UseVisualStyleBackColor = True
        '
        'btnJTmmNLC
        '
        Me.btnJTmmNLC.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTmmNLC.Location = New System.Drawing.Point(12, 66)
        Me.btnJTmmNLC.Name = "btnJTmmNLC"
        Me.btnJTmmNLC.Size = New System.Drawing.Size(101, 249)
        Me.btnJTmmNLC.TabIndex = 46
        Me.btnJTmmNLC.Text = "Non" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Labor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Costs"
        Me.btnJTmmNLC.UseVisualStyleBackColor = True
        '
        'btnJTmmExit
        '
        Me.btnJTmmExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTmmExit.Location = New System.Drawing.Point(1167, 66)
        Me.btnJTmmExit.Name = "btnJTmmExit"
        Me.btnJTmmExit.Size = New System.Drawing.Size(86, 249)
        Me.btnJTmmExit.TabIndex = 47
        Me.btnJTmmExit.Text = "EXIT"
        Me.btnJTmmExit.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.cboxJTmmEmpMnt)
        Me.GroupBox2.Location = New System.Drawing.Point(119, 324)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(201, 92)
        Me.GroupBox2.TabIndex = 48
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox3.Controls.Add(Me.cboxJTmmJobList)
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.cboxJTmmNewJob)
        Me.GroupBox3.Controls.Add(Me.GroupBox1)
        Me.GroupBox3.Location = New System.Drawing.Point(326, 324)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(422, 92)
        Me.GroupBox3.TabIndex = 49
        Me.GroupBox3.TabStop = False
        '
        'cboxJTmmJobList
        '
        Me.cboxJTmmJobList.AutoSize = True
        Me.cboxJTmmJobList.Location = New System.Drawing.Point(6, 48)
        Me.cboxJTmmJobList.Name = "cboxJTmmJobList"
        Me.cboxJTmmJobList.Size = New System.Drawing.Size(67, 36)
        Me.cboxJTmmJobList.TabIndex = 47
        Me.cboxJTmmJobList.Text = "Print" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Listing"
        Me.cboxJTmmJobList.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox6.Controls.Add(Me.cboxJTmmDispAUP)
        Me.GroupBox6.Controls.Add(Me.cboxJTmmShowDeact)
        Me.GroupBox6.Controls.Add(Me.cboxJTmmDispCostP)
        Me.GroupBox6.Location = New System.Drawing.Point(84, 9)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(167, 77)
        Me.GroupBox6.TabIndex = 46
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Display"
        '
        'cboxJTmmDispAUP
        '
        Me.cboxJTmmDispAUP.AutoSize = True
        Me.cboxJTmmDispAUP.Location = New System.Drawing.Point(7, 37)
        Me.cboxJTmmDispAUP.Name = "cboxJTmmDispAUP"
        Me.cboxJTmmDispAUP.Size = New System.Drawing.Size(57, 20)
        Me.cboxJTmmDispAUP.TabIndex = 1
        Me.cboxJTmmDispAUP.Text = "AUP"
        Me.cboxJTmmDispAUP.UseVisualStyleBackColor = True
        '
        'cboxJTmmDispCostP
        '
        Me.cboxJTmmDispCostP.AutoSize = True
        Me.cboxJTmmDispCostP.Location = New System.Drawing.Point(7, 17)
        Me.cboxJTmmDispCostP.Name = "cboxJTmmDispCostP"
        Me.cboxJTmmDispCostP.Size = New System.Drawing.Size(63, 20)
        Me.cboxJTmmDispCostP.TabIndex = 0
        Me.cboxJTmmDispCostP.Text = "Cost+"
        Me.cboxJTmmDispCostP.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.cboxJTmmPrntVendList)
        Me.GroupBox4.Controls.Add(Me.cboxJTmmNewVendor)
        Me.GroupBox4.Location = New System.Drawing.Point(753, 322)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(407, 94)
        Me.GroupBox4.TabIndex = 50
        Me.GroupBox4.TabStop = False
        '
        'cboxJTmmPrntVendList
        '
        Me.cboxJTmmPrntVendList.AutoSize = True
        Me.cboxJTmmPrntVendList.Location = New System.Drawing.Point(200, 39)
        Me.cboxJTmmPrntVendList.Name = "cboxJTmmPrntVendList"
        Me.cboxJTmmPrntVendList.Size = New System.Drawing.Size(125, 20)
        Me.cboxJTmmPrntVendList.TabIndex = 44
        Me.cboxJTmmPrntVendList.Text = "Print Vendor List"
        Me.cboxJTmmPrntVendList.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Controls.Add(Me.JTmmNLCLabel)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 321)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(97, 162)
        Me.GroupBox5.TabIndex = 51
        Me.GroupBox5.TabStop = False
        '
        'JTmmNLCLabel
        '
        Me.JTmmNLCLabel.AutoSize = True
        Me.JTmmNLCLabel.Location = New System.Drawing.Point(6, 23)
        Me.JTmmNLCLabel.Name = "JTmmNLCLabel"
        Me.JTmmNLCLabel.Size = New System.Drawing.Size(93, 48)
        Me.JTmmNLCLabel.TabIndex = 0
        Me.JTmmNLCLabel.Text = "Last NLC" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Entry date:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MM/DD/YYYY"
        Me.JTmmNLCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvMMCstP
        '
        Me.lvMMCstP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvMMCstPJob, Me.lvMMCstPPendInv, Me.lvMMCstPLstInvDate, Me.lvMMCstPPrevInvd})
        Me.lvMMCstP.FullRowSelect = True
        Me.lvMMCstP.HideSelection = False
        Me.lvMMCstP.Location = New System.Drawing.Point(328, 66)
        Me.lvMMCstP.Margin = New System.Windows.Forms.Padding(4)
        Me.lvMMCstP.Name = "lvMMCstP"
        Me.lvMMCstP.Size = New System.Drawing.Size(418, 250)
        Me.lvMMCstP.TabIndex = 52
        Me.lvMMCstP.UseCompatibleStateImageBehavior = False
        Me.lvMMCstP.View = System.Windows.Forms.View.Details
        Me.lvMMCstP.Visible = False
        '
        'lvMMCstPJob
        '
        Me.lvMMCstPJob.Text = "Job[~SubJob]"
        Me.lvMMCstPJob.Width = 170
        '
        'lvMMCstPPendInv
        '
        Me.lvMMCstPPendInv.Text = "Pend.Inv."
        Me.lvMMCstPPendInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMMCstPPendInv.Width = 70
        '
        'lvMMCstPLstInvDate
        '
        Me.lvMMCstPLstInvDate.Text = "Lst Inv Date"
        Me.lvMMCstPLstInvDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMMCstPLstInvDate.Width = 80
        '
        'lvMMCstPPrevInvd
        '
        Me.lvMMCstPPrevInvd.Text = "Prev.Inv'd"
        Me.lvMMCstPPrevInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMMCstPPrevInvd.Width = 70
        '
        'lvMMAUP
        '
        Me.lvMMAUP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvMMAUPJob, Me.lvMMAUPPendInv, Me.lvMMAUPrevInvd, Me.lvMMAUPAupAmt})
        Me.lvMMAUP.FullRowSelect = True
        Me.lvMMAUP.HideSelection = False
        Me.lvMMAUP.Location = New System.Drawing.Point(328, 66)
        Me.lvMMAUP.Margin = New System.Windows.Forms.Padding(4)
        Me.lvMMAUP.Name = "lvMMAUP"
        Me.lvMMAUP.Size = New System.Drawing.Size(418, 250)
        Me.lvMMAUP.TabIndex = 53
        Me.lvMMAUP.UseCompatibleStateImageBehavior = False
        Me.lvMMAUP.View = System.Windows.Forms.View.Details
        Me.lvMMAUP.Visible = False
        '
        'lvMMAUPJob
        '
        Me.lvMMAUPJob.Text = "Customer~Job"
        Me.lvMMAUPJob.Width = 170
        '
        'lvMMAUPPendInv
        '
        Me.lvMMAUPPendInv.Text = "Queued"
        Me.lvMMAUPPendInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMMAUPPendInv.Width = 70
        '
        'lvMMAUPrevInvd
        '
        Me.lvMMAUPrevInvd.Text = "Prev.Inv'd"
        Me.lvMMAUPrevInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMMAUPrevInvd.Width = 70
        '
        'lvMMAUPAupAmt
        '
        Me.lvMMAUPAupAmt.Text = "AUP Quote"
        Me.lvMMAUPAupAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMMAUPAupAmt.Width = 70
        '
        'btnJTmmInvng
        '
        Me.btnJTmmInvng.Location = New System.Drawing.Point(340, 424)
        Me.btnJTmmInvng.Name = "btnJTmmInvng"
        Me.btnJTmmInvng.Size = New System.Drawing.Size(191, 55)
        Me.btnJTmmInvng.TabIndex = 54
        Me.btnJTmmInvng.Text = "Invoicing"
        Me.btnJTmmInvng.UseVisualStyleBackColor = True
        '
        'btnJTmmAUPSum
        '
        Me.btnJTmmAUPSum.Location = New System.Drawing.Point(543, 423)
        Me.btnJTmmAUPSum.Name = "btnJTmmAUPSum"
        Me.btnJTmmAUPSum.Size = New System.Drawing.Size(191, 55)
        Me.btnJTmmAUPSum.TabIndex = 55
        Me.btnJTmmAUPSum.Text = "AUP Summary"
        Me.btnJTmmAUPSum.UseVisualStyleBackColor = True
        '
        'JTMainMenu
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(1261, 481)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJTmmAUPSum)
        Me.Controls.Add(Me.btnJTmmInvng)
        Me.Controls.Add(Me.lvMMAUP)
        Me.Controls.Add(Me.lvMMCstP)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.JTmmJobPanelLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.lvMMNLC)
        Me.Controls.Add(Me.lvMMJobs)
        Me.Controls.Add(Me.lvMMtk)
        Me.Controls.Add(Me.btnJTmmExit)
        Me.Controls.Add(Me.btnJTmmNLC)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Location = New System.Drawing.Point(100, 150)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTMainMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Main Menu"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents lvMMJobs As ListView
    Friend WithEvents lvMMJobsJobName As ColumnHeader
    Friend WithEvents lvMMtk As ListView
    Friend WithEvents lvJTjimEmpID As ColumnHeader
    Friend WithEvents lvlJTTKDate As ColumnHeader
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents lvMMJobsPndAmt As ColumnHeader
    Friend WithEvents Label1 As Label
    Friend WithEvents JTmmJobPanelLabel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents lvMMNLC As ListView
    Friend WithEvents Type As ColumnHeader
    Friend WithEvents LastAct As ColumnHeader
    Friend WithEvents AcctOpt As ColumnHeader
    Friend WithEvents DocProb As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents cboxJTmmEmpMnt As CheckBox
    Friend WithEvents cboxJTmmNewJob As CheckBox
    Friend WithEvents cboxJTmmNewVendor As CheckBox
    Friend WithEvents cboxJTmmShowDeact As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbtnJTMMJSAmt As RadioButton
    Friend WithEvents rbtnJTmmJSAlpha As RadioButton
    Friend WithEvents btnJTmmNLC As Button
    Friend WithEvents btnJTmmExit As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents lvMMJobsType As ColumnHeader
    Friend WithEvents lvMMJobsCstMeth As ColumnHeader
    Friend WithEvents JTmmNLCLabel As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents cboxJTmmDispAUP As CheckBox
    Friend WithEvents cboxJTmmDispCostP As CheckBox
    Friend WithEvents lvMMCstP As ListView
    Friend WithEvents lvMMCstPJob As ColumnHeader
    Friend WithEvents lvMMCstPPendInv As ColumnHeader
    Friend WithEvents lvMMCstPLstInvDate As ColumnHeader
    Friend WithEvents lvMMAUP As ListView
    Friend WithEvents lvMMAUPJob As ColumnHeader
    Friend WithEvents lvMMAUPPendInv As ColumnHeader
    Friend WithEvents lvMMAUPrevInvd As ColumnHeader
    Friend WithEvents lvMMAUPAupAmt As ColumnHeader
    Friend WithEvents lvMMCstPPrevInvd As ColumnHeader
    Friend WithEvents cboxJTmmJobList As CheckBox
    Friend WithEvents cboxJTmmPrntVendList As CheckBox
    Friend WithEvents btnJTmmInvng As Button
    Friend WithEvents btnJTmmAUPSum As Button
End Class
