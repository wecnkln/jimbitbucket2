﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTpr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mtxtJTprWeekEndDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lvJTpr = New System.Windows.Forms.ListView()
        Me.chEmpID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chEmpName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chCat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chTKHours = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chPRSubmit = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chState1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chState2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.chState3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnPREmail = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.txtW2PRLiab = New System.Windows.Forms.TextBox()
        Me.txt1099PRLiab = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.txtJTprHrAdj = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'mtxtJTprWeekEndDate
        '
        Me.mtxtJTprWeekEndDate.CausesValidation = False
        Me.mtxtJTprWeekEndDate.Location = New System.Drawing.Point(653, 24)
        Me.mtxtJTprWeekEndDate.Margin = New System.Windows.Forms.Padding(4)
        Me.mtxtJTprWeekEndDate.Mask = "00/00/0000"
        Me.mtxtJTprWeekEndDate.Name = "mtxtJTprWeekEndDate"
        Me.mtxtJTprWeekEndDate.Size = New System.Drawing.Size(89, 22)
        Me.mtxtJTprWeekEndDate.TabIndex = 0
        Me.mtxtJTprWeekEndDate.ValidatingType = GetType(Date)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(502, 24)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Week Ending Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(27, 18)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(253, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Payroll Data Consolidation"
        '
        'lvJTpr
        '
        Me.lvJTpr.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chEmpID, Me.chEmpName, Me.chCat, Me.chTKHours, Me.chPRSubmit, Me.chState1, Me.chState2, Me.chState3})
        Me.lvJTpr.FullRowSelect = True
        Me.lvJTpr.Location = New System.Drawing.Point(16, 65)
        Me.lvJTpr.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTpr.MultiSelect = False
        Me.lvJTpr.Name = "lvJTpr"
        Me.lvJTpr.Size = New System.Drawing.Size(813, 349)
        Me.lvJTpr.TabIndex = 3
        Me.lvJTpr.UseCompatibleStateImageBehavior = False
        Me.lvJTpr.View = System.Windows.Forms.View.Details
        '
        'chEmpID
        '
        Me.chEmpID.Text = "Emp ID"
        Me.chEmpID.Width = 70
        '
        'chEmpName
        '
        Me.chEmpName.Text = "Name"
        Me.chEmpName.Width = 125
        '
        'chCat
        '
        Me.chCat.Text = "Cat."
        Me.chCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chTKHours
        '
        Me.chTKHours.Text = "Time Keeping"
        Me.chTKHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chTKHours.Width = 110
        '
        'chPRSubmit
        '
        Me.chPRSubmit.Text = "PR Submit"
        Me.chPRSubmit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chPRSubmit.Width = 110
        '
        'chState1
        '
        Me.chState1.Text = "St 1 Hrs"
        Me.chState1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chState1.Width = 100
        '
        'chState2
        '
        Me.chState2.Text = "St 2 Hrs"
        Me.chState2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chState2.Width = 100
        '
        'chState3
        '
        Me.chState3.Text = "St 3 Hrs"
        Me.chState3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.chState3.Width = 100
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(847, 287)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(152, 139)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Text = "By selecting the email option, a document will be sent to the payroll company sub" &
    "mitting hours to be paid. This only applies to employees in the W2 category."
        '
        'btnPREmail
        '
        Me.btnPREmail.Location = New System.Drawing.Point(847, 231)
        Me.btnPREmail.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPREmail.Name = "btnPREmail"
        Me.btnPREmail.Size = New System.Drawing.Size(153, 48)
        Me.btnPREmail.TabIndex = 5
        Me.btnPREmail.Text = "Send PR Email"
        Me.btnPREmail.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(16, 421)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(813, 27)
        Me.TextBox2.TabIndex = 6
        Me.TextBox2.Text = "Highlight line in table to modify employee's payroll hours. Ajustments to payroll" &
    " hours will be adjusted in state with most hours."
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(847, 435)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(153, 46)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtW2PRLiab
        '
        Me.txtW2PRLiab.Location = New System.Drawing.Point(256, 466)
        Me.txtW2PRLiab.Margin = New System.Windows.Forms.Padding(4)
        Me.txtW2PRLiab.Name = "txtW2PRLiab"
        Me.txtW2PRLiab.ReadOnly = True
        Me.txtW2PRLiab.Size = New System.Drawing.Size(124, 22)
        Me.txtW2PRLiab.TabIndex = 8
        Me.txtW2PRLiab.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt1099PRLiab
        '
        Me.txt1099PRLiab.Location = New System.Drawing.Point(653, 466)
        Me.txt1099PRLiab.Margin = New System.Windows.Forms.Padding(4)
        Me.txt1099PRLiab.Name = "txt1099PRLiab"
        Me.txt1099PRLiab.ReadOnly = True
        Me.txt1099PRLiab.Size = New System.Drawing.Size(124, 22)
        Me.txt1099PRLiab.TabIndex = 9
        Me.txt1099PRLiab.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 470)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(193, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Estimated W2 Payroll Liability"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(424, 470)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(204, 17)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Estimated 1099 Payroll Liability"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(864, 73)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Hour Adjustment"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(847, 133)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(152, 90)
        Me.TextBox4.TabIndex = 14
        Me.TextBox4.Text = "Keyed corrected pay-roll hours into above field. it will update the selected line" &
    " item in the table."
        '
        'txtJTprHrAdj
        '
        Me.txtJTprHrAdj.Location = New System.Drawing.Point(880, 102)
        Me.txtJTprHrAdj.Name = "txtJTprHrAdj"
        Me.txtJTprHrAdj.Size = New System.Drawing.Size(80, 22)
        Me.txtJTprHrAdj.TabIndex = 15
        Me.txtJTprHrAdj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'JTpr
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1023, 512)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtJTprHrAdj)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt1099PRLiab)
        Me.Controls.Add(Me.txtW2PRLiab)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.btnPREmail)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lvJTpr)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.mtxtJTprWeekEndDate)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTpr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Payroll"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mtxtJTprWeekEndDate As MaskedTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lvJTpr As ListView
    Friend WithEvents chEmpID As ColumnHeader
    Friend WithEvents chEmpName As ColumnHeader
    Friend WithEvents chTKHours As ColumnHeader
    Friend WithEvents chPRSubmit As ColumnHeader
    Friend WithEvents chState1 As ColumnHeader
    Friend WithEvents chState2 As ColumnHeader
    Friend WithEvents chState3 As ColumnHeader
    Friend WithEvents chCat As ColumnHeader
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btnPREmail As Button
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents txtW2PRLiab As TextBox
    Friend WithEvents txt1099PRLiab As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents txtJTprHrAdj As TextBox
End Class
