﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTinv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTinv))
        Me.JobDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnJTinvClose = New System.Windows.Forms.Button()
        Me.Spare2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Spare1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AUPHDef = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AUP = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Services = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Material = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.QuedAmt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.InvdTD = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PrcMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LstInvAmt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LstInvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Job = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Customer = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Labor = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTinv = New System.Windows.Forms.ListView()
        Me.gboxJTinvSeq = New System.Windows.Forms.GroupBox()
        Me.rbtnInvDtSeq = New System.Windows.Forms.RadioButton()
        Me.rbtnAmtSeq = New System.Windows.Forms.RadioButton()
        Me.rbtnJobSeq = New System.Windows.Forms.RadioButton()
        Me.btnJTinvProduce = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtJTinvSelAmts = New System.Windows.Forms.TextBox()
        Me.txtJTinvSelJobs = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gboxJTinvSeq.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'JobDesc
        '
        Me.JobDesc.Text = "Job Description"
        Me.JobDesc.Width = 0
        '
        'btnJTinvClose
        '
        Me.btnJTinvClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTinvClose.Location = New System.Drawing.Point(16, 521)
        Me.btnJTinvClose.Name = "btnJTinvClose"
        Me.btnJTinvClose.Size = New System.Drawing.Size(167, 51)
        Me.btnJTinvClose.TabIndex = 6
        Me.btnJTinvClose.Text = "Close/Cancel"
        Me.btnJTinvClose.UseVisualStyleBackColor = True
        '
        'Spare2
        '
        Me.Spare2.Text = "Spare2"
        Me.Spare2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Spare2.Width = 0
        '
        'Spare1
        '
        Me.Spare1.Text = "Spare1"
        Me.Spare1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Spare1.Width = 0
        '
        'AUPHDef
        '
        Me.AUPHDef.Text = "AUPH Criteria Excludes"
        Me.AUPHDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.AUPHDef.Width = 155
        '
        'AUP
        '
        Me.AUP.Text = "AUP"
        Me.AUP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.AUP.Width = 80
        '
        'Services
        '
        Me.Services.Text = "Services"
        Me.Services.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Services.Width = 80
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(241, 20)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "INVOICE LAUNCHING PAD"
        '
        'Material
        '
        Me.Material.Text = "Material"
        Me.Material.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Material.Width = 80
        '
        'QuedAmt
        '
        Me.QuedAmt.Text = "Que'd Amt"
        Me.QuedAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.QuedAmt.Width = 70
        '
        'InvdTD
        '
        Me.InvdTD.Text = "Inv'd TD"
        Me.InvdTD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.InvdTD.Width = 80
        '
        'PrcMeth
        '
        Me.PrcMeth.Text = "Prc Meth"
        Me.PrcMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.PrcMeth.Width = 80
        '
        'LstInvAmt
        '
        Me.LstInvAmt.Text = "Lst Inv Amt"
        Me.LstInvAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.LstInvAmt.Width = 80
        '
        'LstInvDate
        '
        Me.LstInvDate.Text = "Lst Inv Dt"
        Me.LstInvDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LstInvDate.Width = 70
        '
        'Job
        '
        Me.Job.Text = "Job"
        Me.Job.Width = 120
        '
        'Customer
        '
        Me.Customer.Text = "Customer"
        Me.Customer.Width = 120
        '
        'Labor
        '
        Me.Labor.Text = "Labor"
        Me.Labor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Labor.Width = 80
        '
        'lvJTinv
        '
        Me.lvJTinv.CheckBoxes = True
        Me.lvJTinv.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Customer, Me.Job, Me.InvdTD, Me.LstInvDate, Me.LstInvAmt, Me.PrcMeth, Me.QuedAmt, Me.Labor, Me.Material, Me.Services, Me.AUP, Me.AUPHDef, Me.Spare1, Me.Spare2, Me.JobDesc})
        Me.lvJTinv.Location = New System.Drawing.Point(9, 58)
        Me.lvJTinv.Name = "lvJTinv"
        Me.lvJTinv.Size = New System.Drawing.Size(1172, 379)
        Me.lvJTinv.TabIndex = 4
        Me.lvJTinv.UseCompatibleStateImageBehavior = False
        Me.lvJTinv.View = System.Windows.Forms.View.Details
        '
        'gboxJTinvSeq
        '
        Me.gboxJTinvSeq.Controls.Add(Me.rbtnInvDtSeq)
        Me.gboxJTinvSeq.Controls.Add(Me.rbtnAmtSeq)
        Me.gboxJTinvSeq.Controls.Add(Me.rbtnJobSeq)
        Me.gboxJTinvSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTinvSeq.Location = New System.Drawing.Point(888, 454)
        Me.gboxJTinvSeq.Name = "gboxJTinvSeq"
        Me.gboxJTinvSeq.Size = New System.Drawing.Size(270, 64)
        Me.gboxJTinvSeq.TabIndex = 8
        Me.gboxJTinvSeq.TabStop = False
        Me.gboxJTinvSeq.Text = "Panel Sequence"
        '
        'rbtnInvDtSeq
        '
        Me.rbtnInvDtSeq.AutoSize = True
        Me.rbtnInvDtSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnInvDtSeq.Location = New System.Drawing.Point(167, 27)
        Me.rbtnInvDtSeq.Name = "rbtnInvDtSeq"
        Me.rbtnInvDtSeq.Size = New System.Drawing.Size(88, 21)
        Me.rbtnInvDtSeq.TabIndex = 2
        Me.rbtnInvDtSeq.TabStop = True
        Me.rbtnInvDtSeq.Text = "Lst Inv Dt"
        Me.rbtnInvDtSeq.UseVisualStyleBackColor = True
        '
        'rbtnAmtSeq
        '
        Me.rbtnAmtSeq.AutoSize = True
        Me.rbtnAmtSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnAmtSeq.Location = New System.Drawing.Point(78, 27)
        Me.rbtnAmtSeq.Name = "rbtnAmtSeq"
        Me.rbtnAmtSeq.Size = New System.Drawing.Size(83, 21)
        Me.rbtnAmtSeq.TabIndex = 1
        Me.rbtnAmtSeq.TabStop = True
        Me.rbtnAmtSeq.Text = "Inv. Amt."
        Me.rbtnAmtSeq.UseVisualStyleBackColor = True
        '
        'rbtnJobSeq
        '
        Me.rbtnJobSeq.AutoSize = True
        Me.rbtnJobSeq.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnJobSeq.Location = New System.Drawing.Point(17, 27)
        Me.rbtnJobSeq.Name = "rbtnJobSeq"
        Me.rbtnJobSeq.Size = New System.Drawing.Size(52, 21)
        Me.rbtnJobSeq.TabIndex = 0
        Me.rbtnJobSeq.TabStop = True
        Me.rbtnJobSeq.Text = "Job"
        Me.rbtnJobSeq.UseVisualStyleBackColor = True
        '
        'btnJTinvProduce
        '
        Me.btnJTinvProduce.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTinvProduce.Location = New System.Drawing.Point(16, 454)
        Me.btnJTinvProduce.Name = "btnJTinvProduce"
        Me.btnJTinvProduce.Size = New System.Drawing.Size(167, 51)
        Me.btnJTinvProduce.TabIndex = 9
        Me.btnJTinvProduce.Text = "Produce Invoices"
        Me.btnJTinvProduce.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtJTinvSelAmts)
        Me.GroupBox1.Controls.Add(Me.txtJTinvSelJobs)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(218, 454)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(339, 137)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selected for Invoicing"
        '
        'txtJTinvSelAmts
        '
        Me.txtJTinvSelAmts.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTinvSelAmts.Location = New System.Drawing.Point(188, 49)
        Me.txtJTinvSelAmts.Name = "txtJTinvSelAmts"
        Me.txtJTinvSelAmts.ReadOnly = True
        Me.txtJTinvSelAmts.Size = New System.Drawing.Size(100, 22)
        Me.txtJTinvSelAmts.TabIndex = 5
        Me.txtJTinvSelAmts.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTinvSelJobs
        '
        Me.txtJTinvSelJobs.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTinvSelJobs.Location = New System.Drawing.Point(45, 49)
        Me.txtJTinvSelJobs.Name = "txtJTinvSelJobs"
        Me.txtJTinvSelJobs.ReadOnly = True
        Me.txtJTinvSelJobs.Size = New System.Drawing.Size(100, 22)
        Me.txtJTinvSelJobs.TabIndex = 4
        Me.txtJTinvSelJobs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(212, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Amount"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(62, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "# of Jobs"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(297, 34)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "If mulitiple jobs are combined during invoicing," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " these figures may  not be tota" &
    "lly accurate."
        '
        'JTinv
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(1191, 604)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnJTinvProduce)
        Me.Controls.Add(Me.gboxJTinvSeq)
        Me.Controls.Add(Me.btnJTinvClose)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lvJTinv)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "JTinv"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "J.I.M. - Invoice Launching Pad"
        Me.gboxJTinvSeq.ResumeLayout(False)
        Me.gboxJTinvSeq.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents JobDesc As ColumnHeader
    Friend WithEvents btnJTinvClose As Button
    Friend WithEvents Spare2 As ColumnHeader
    Friend WithEvents Spare1 As ColumnHeader
    Friend WithEvents AUPHDef As ColumnHeader
    Friend WithEvents AUP As ColumnHeader
    Friend WithEvents Services As ColumnHeader
    Friend WithEvents Label2 As Label
    Friend WithEvents Material As ColumnHeader
    Friend WithEvents QuedAmt As ColumnHeader
    Friend WithEvents InvdTD As ColumnHeader
    Friend WithEvents PrcMeth As ColumnHeader
    Friend WithEvents LstInvAmt As ColumnHeader
    Friend WithEvents LstInvDate As ColumnHeader
    Friend WithEvents Job As ColumnHeader
    Friend WithEvents Customer As ColumnHeader
    Friend WithEvents Labor As ColumnHeader
    Friend WithEvents lvJTinv As ListView
    Friend WithEvents gboxJTinvSeq As GroupBox
    Friend WithEvents rbtnInvDtSeq As RadioButton
    Friend WithEvents rbtnAmtSeq As RadioButton
    Friend WithEvents rbtnJobSeq As RadioButton
    Friend WithEvents btnJTinvProduce As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtJTinvSelAmts As TextBox
    Friend WithEvents txtJTinvSelJobs As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
End Class
