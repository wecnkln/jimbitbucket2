﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTnlcE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTnlcE))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gboxJTnlcPymtSpecInfo = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.mtxtJTnlcEviDueDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJTnlcviTerms = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtJTnlcviDiscAmt = New System.Windows.Forms.TextBox()
        Me.txtJTviDiscPercent = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTviAcctNum = New System.Windows.Forms.TextBox()
        Me.txtJTviStreet = New System.Windows.Forms.TextBox()
        Me.txtJTviCityStateZip = New System.Windows.Forms.TextBox()
        Me.txtJTviVendName = New System.Windows.Forms.TextBox()
        Me.gboxJTviInterfaceOpt = New System.Windows.Forms.GroupBox()
        Me.rbtnJTnlcEviJourOnly = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnlcEviManual = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnlcEviExport = New System.Windows.Forms.RadioButton()
        Me.rbtnJTnlcEviReconcile = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gboxJTnlcENJRA = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboxJTnlcENJREAcct = New System.Windows.Forms.ComboBox()
        Me.lvMorS = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvCstToCust = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMargin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvCost = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvInvoice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSpplrName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvDescrpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnJTnlcEISITems = New System.Windows.Forms.Button()
        Me.gboxJTnlcEInvInfo = New System.Windows.Forms.GroupBox()
        Me.txtJTnlcEInvFileName = New System.Windows.Forms.TextBox()
        Me.txtJTnlcEXRef = New System.Windows.Forms.TextBox()
        Me.gboxJTYnlcEMrgnInvd = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtJTnlcEMrgnPC = New System.Windows.Forms.TextBox()
        Me.txtJTnlcEAmtTBInvd = New System.Windows.Forms.TextBox()
        Me.cboxJTnlcESupplier = New System.Windows.Forms.ComboBox()
        Me.lblJTnlcEFileName = New System.Windows.Forms.Label()
        Me.lblJTnlcEXref = New System.Windows.Forms.Label()
        Me.lblJTnlcELookUp = New System.Windows.Forms.Label()
        Me.rbtnVendSearch = New System.Windows.Forms.RadioButton()
        Me.txtJTnlcESupCost = New System.Windows.Forms.TextBox()
        Me.lblJTnlcECost = New System.Windows.Forms.Label()
        Me.lblJTnlcESupplr = New System.Windows.Forms.Label()
        Me.txtJTnlcEInvNum = New System.Windows.Forms.TextBox()
        Me.txtJTnlcEDecrpt = New System.Windows.Forms.TextBox()
        Me.lblJTnlcEInvNum = New System.Windows.Forms.Label()
        Me.lblJTnlcEDesc = New System.Windows.Forms.Label()
        Me.gboxJTnlcECustJob = New System.Windows.Forms.GroupBox()
        Me.cboxJTnlcESubID = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboxJTnlcEJobID = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.mtxtJTnlcEEntDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.radiobtnJTnlcEAdj = New System.Windows.Forms.RadioButton()
        Me.radiobtnJTnlcEISItems = New System.Windows.Forms.RadioButton()
        Me.radiobtnJTnlcENJRE = New System.Windows.Forms.RadioButton()
        Me.radiobtnJTnlcEMat = New System.Windows.Forms.RadioButton()
        Me.radiobtnJTnlcESer = New System.Windows.Forms.RadioButton()
        Me.mtxtJTnlcEDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.mtxtJTnlcWEDate = New System.Windows.Forms.MaskedTextBox()
        Me.lvJobInvDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.slJTnlcKey = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PymtMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnJtnlcEExit = New System.Windows.Forms.Button()
        Me.btnJTnlcERecord = New System.Windows.Forms.Button()
        Me.gboxInv1PDF = New System.Windows.Forms.GroupBox()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.btnJTnlcEDeleteEntry = New System.Windows.Forms.Button()
        Me.gboxJTnlcENoImage = New System.Windows.Forms.GroupBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnJTnlceMergePDF = New System.Windows.Forms.Button()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.gboxJTnlcPymtSpecInfo.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.gboxJTviInterfaceOpt.SuspendLayout()
        Me.gboxJTnlcENJRA.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gboxJTnlcEInvInfo.SuspendLayout()
        Me.gboxJTYnlcEMrgnInvd.SuspendLayout()
        Me.gboxJTnlcECustJob.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gboxInv1PDF.SuspendLayout()
        Me.gboxJTnlcENoImage.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'gboxJTnlcPymtSpecInfo
        '
        Me.gboxJTnlcPymtSpecInfo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gboxJTnlcPymtSpecInfo.Controls.Add(Me.GroupBox5)
        Me.gboxJTnlcPymtSpecInfo.Controls.Add(Me.GroupBox4)
        Me.gboxJTnlcPymtSpecInfo.Controls.Add(Me.gboxJTviInterfaceOpt)
        Me.gboxJTnlcPymtSpecInfo.Location = New System.Drawing.Point(909, 498)
        Me.gboxJTnlcPymtSpecInfo.Name = "gboxJTnlcPymtSpecInfo"
        Me.gboxJTnlcPymtSpecInfo.Size = New System.Drawing.Size(555, 191)
        Me.gboxJTnlcPymtSpecInfo.TabIndex = 115
        Me.gboxJTnlcPymtSpecInfo.TabStop = False
        Me.gboxJTnlcPymtSpecInfo.Text = "Payment Specific Information"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox5.Controls.Add(Me.mtxtJTnlcEviDueDate)
        Me.GroupBox5.Controls.Add(Me.Label10)
        Me.GroupBox5.Controls.Add(Me.txtJTnlcviTerms)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.txtJTnlcviDiscAmt)
        Me.GroupBox5.Controls.Add(Me.txtJTviDiscPercent)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 125)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(348, 59)
        Me.GroupBox5.TabIndex = 19
        Me.GroupBox5.TabStop = False
        '
        'mtxtJTnlcEviDueDate
        '
        Me.mtxtJTnlcEviDueDate.Location = New System.Drawing.Point(194, 5)
        Me.mtxtJTnlcEviDueDate.Mask = "00/00/0000"
        Me.mtxtJTnlcEviDueDate.Name = "mtxtJTnlcEviDueDate"
        Me.mtxtJTnlcEviDueDate.Size = New System.Drawing.Size(100, 22)
        Me.mtxtJTnlcEviDueDate.TabIndex = 124
        Me.mtxtJTnlcEviDueDate.ValidatingType = GetType(Date)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(11, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Discount (%/$$$)"
        '
        'txtJTnlcviTerms
        '
        Me.txtJTnlcviTerms.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcviTerms.Location = New System.Drawing.Point(148, 5)
        Me.txtJTnlcviTerms.Name = "txtJTnlcviTerms"
        Me.txtJTnlcviTerms.Size = New System.Drawing.Size(41, 22)
        Me.txtJTnlcviTerms.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(11, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(115, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Due Date (Terms/Date)"
        '
        'txtJTnlcviDiscAmt
        '
        Me.txtJTnlcviDiscAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTnlcviDiscAmt.Location = New System.Drawing.Point(195, 31)
        Me.txtJTnlcviDiscAmt.Name = "txtJTnlcviDiscAmt"
        Me.txtJTnlcviDiscAmt.Size = New System.Drawing.Size(80, 22)
        Me.txtJTnlcviDiscAmt.TabIndex = 15
        '
        'txtJTviDiscPercent
        '
        Me.txtJTviDiscPercent.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTviDiscPercent.Location = New System.Drawing.Point(148, 30)
        Me.txtJTviDiscPercent.Name = "txtJTviDiscPercent"
        Me.txtJTviDiscPercent.Size = New System.Drawing.Size(41, 22)
        Me.txtJTviDiscPercent.TabIndex = 12
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.txtJTviAcctNum)
        Me.GroupBox4.Controls.Add(Me.txtJTviStreet)
        Me.GroupBox4.Controls.Add(Me.txtJTviCityStateZip)
        Me.GroupBox4.Controls.Add(Me.txtJTviVendName)
        Me.GroupBox4.Location = New System.Drawing.Point(17, 21)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(347, 97)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Acct. #"
        '
        'txtJTviAcctNum
        '
        Me.txtJTviAcctNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTviAcctNum.Location = New System.Drawing.Point(69, 5)
        Me.txtJTviAcctNum.Name = "txtJTviAcctNum"
        Me.txtJTviAcctNum.Size = New System.Drawing.Size(213, 22)
        Me.txtJTviAcctNum.TabIndex = 11
        '
        'txtJTviStreet
        '
        Me.txtJTviStreet.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTviStreet.Location = New System.Drawing.Point(9, 49)
        Me.txtJTviStreet.Name = "txtJTviStreet"
        Me.txtJTviStreet.Size = New System.Drawing.Size(273, 22)
        Me.txtJTviStreet.TabIndex = 4
        '
        'txtJTviCityStateZip
        '
        Me.txtJTviCityStateZip.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTviCityStateZip.Location = New System.Drawing.Point(9, 71)
        Me.txtJTviCityStateZip.Name = "txtJTviCityStateZip"
        Me.txtJTviCityStateZip.Size = New System.Drawing.Size(273, 22)
        Me.txtJTviCityStateZip.TabIndex = 5
        '
        'txtJTviVendName
        '
        Me.txtJTviVendName.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTviVendName.Location = New System.Drawing.Point(9, 27)
        Me.txtJTviVendName.Name = "txtJTviVendName"
        Me.txtJTviVendName.Size = New System.Drawing.Size(273, 22)
        Me.txtJTviVendName.TabIndex = 3
        '
        'gboxJTviInterfaceOpt
        '
        Me.gboxJTviInterfaceOpt.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTnlcEviJourOnly)
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTnlcEviManual)
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTnlcEviExport)
        Me.gboxJTviInterfaceOpt.Controls.Add(Me.rbtnJTnlcEviReconcile)
        Me.gboxJTviInterfaceOpt.Location = New System.Drawing.Point(370, 18)
        Me.gboxJTviInterfaceOpt.Name = "gboxJTviInterfaceOpt"
        Me.gboxJTviInterfaceOpt.Size = New System.Drawing.Size(171, 162)
        Me.gboxJTviInterfaceOpt.TabIndex = 1
        Me.gboxJTviInterfaceOpt.TabStop = False
        Me.gboxJTviInterfaceOpt.Text = "Payment Method:"
        '
        'rbtnJTnlcEviJourOnly
        '
        Me.rbtnJTnlcEviJourOnly.AutoSize = True
        Me.rbtnJTnlcEviJourOnly.Location = New System.Drawing.Point(10, 88)
        Me.rbtnJTnlcEviJourOnly.Name = "rbtnJTnlcEviJourOnly"
        Me.rbtnJTnlcEviJourOnly.Size = New System.Drawing.Size(102, 20)
        Me.rbtnJTnlcEviJourOnly.TabIndex = 4
        Me.rbtnJTnlcEviJourOnly.TabStop = True
        Me.rbtnJTnlcEviJourOnly.Text = "Journal Only"
        Me.rbtnJTnlcEviJourOnly.UseVisualStyleBackColor = True
        '
        'rbtnJTnlcEviManual
        '
        Me.rbtnJTnlcEviManual.AutoSize = True
        Me.rbtnJTnlcEviManual.Location = New System.Drawing.Point(10, 65)
        Me.rbtnJTnlcEviManual.Name = "rbtnJTnlcEviManual"
        Me.rbtnJTnlcEviManual.Size = New System.Drawing.Size(151, 20)
        Me.rbtnJTnlcEviManual.TabIndex = 2
        Me.rbtnJTnlcEviManual.TabStop = True
        Me.rbtnJTnlcEviManual.Text = "Processed Manually"
        Me.rbtnJTnlcEviManual.UseVisualStyleBackColor = True
        '
        'rbtnJTnlcEviExport
        '
        Me.rbtnJTnlcEviExport.AutoSize = True
        Me.rbtnJTnlcEviExport.Location = New System.Drawing.Point(10, 41)
        Me.rbtnJTnlcEviExport.Name = "rbtnJTnlcEviExport"
        Me.rbtnJTnlcEviExport.Size = New System.Drawing.Size(118, 20)
        Me.rbtnJTnlcEviExport.TabIndex = 1
        Me.rbtnJTnlcEviExport.TabStop = True
        Me.rbtnJTnlcEviExport.Text = "Exported to QB"
        Me.rbtnJTnlcEviExport.UseVisualStyleBackColor = True
        '
        'rbtnJTnlcEviReconcile
        '
        Me.rbtnJTnlcEviReconcile.AutoSize = True
        Me.rbtnJTnlcEviReconcile.Location = New System.Drawing.Point(10, 20)
        Me.rbtnJTnlcEviReconcile.Name = "rbtnJTnlcEviReconcile"
        Me.rbtnJTnlcEviReconcile.Size = New System.Drawing.Size(97, 20)
        Me.rbtnJTnlcEviReconcile.TabIndex = 0
        Me.rbtnJTnlcEviReconcile.TabStop = True
        Me.rbtnJTnlcEviReconcile.Text = "Reconciled"
        Me.rbtnJTnlcEviReconcile.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(301, -60)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 16)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "Entry Date"
        '
        'gboxJTnlcENJRA
        '
        Me.gboxJTnlcENJRA.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTnlcENJRA.Controls.Add(Me.Label12)
        Me.gboxJTnlcENJRA.Controls.Add(Me.cboxJTnlcENJREAcct)
        Me.gboxJTnlcENJRA.Location = New System.Drawing.Point(14, 136)
        Me.gboxJTnlcENJRA.Name = "gboxJTnlcENJRA"
        Me.gboxJTnlcENJRA.Size = New System.Drawing.Size(338, 90)
        Me.gboxJTnlcENJRA.TabIndex = 110
        Me.gboxJTnlcENJRA.TabStop = False
        Me.gboxJTnlcENJRA.Text = "NJR Expense Transaction Account"
        Me.gboxJTnlcENJRA.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(22, 59)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(200, 16)
        Me.Label12.TabIndex = 118
        Me.Label12.Text = "* Required for NJR Transactions"
        '
        'cboxJTnlcENJREAcct
        '
        Me.cboxJTnlcENJREAcct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnlcENJREAcct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnlcENJREAcct.FormattingEnabled = True
        Me.cboxJTnlcENJREAcct.Location = New System.Drawing.Point(10, 25)
        Me.cboxJTnlcENJREAcct.Name = "cboxJTnlcENJREAcct"
        Me.cboxJTnlcENJREAcct.Size = New System.Drawing.Size(320, 24)
        Me.cboxJTnlcENJREAcct.TabIndex = 117
        '
        'lvMorS
        '
        Me.lvMorS.Text = "M or S"
        Me.lvMorS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMorS.Width = 50
        '
        'lvCstToCust
        '
        Me.lvCstToCust.Text = "Cst to Cust"
        Me.lvCstToCust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvCstToCust.Width = 75
        '
        'lvMargin
        '
        Me.lvMargin.Text = "Mrgn %"
        Me.lvMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMargin.Width = 50
        '
        'lvInvCost
        '
        Me.lvInvCost.Text = "Cost"
        Me.lvInvCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvInvCost.Width = 75
        '
        'lvInvoice
        '
        Me.lvInvoice.Text = "Invoice #"
        Me.lvInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvSpplrName
        '
        Me.lvSpplrName.Text = "Spplr/Contrct Name"
        Me.lvSpplrName.Width = 120
        '
        'lvDescrpt
        '
        Me.lvDescrpt.Text = "Material or Service Purchased"
        Me.lvDescrpt.Width = 230
        '
        'lvSubName
        '
        Me.lvSubName.Text = "Job"
        Me.lvSubName.Width = 150
        '
        'lvDate
        '
        Me.lvDate.Text = "Date"
        Me.lvDate.Width = 80
        '
        'lvJobID
        '
        Me.lvJobID.Text = "Customer"
        Me.lvJobID.Width = 100
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox2.Controls.Add(Me.btnJTnlcEISITems)
        Me.GroupBox2.Controls.Add(Me.gboxJTnlcEInvInfo)
        Me.GroupBox2.Controls.Add(Me.gboxJTnlcENJRA)
        Me.GroupBox2.Controls.Add(Me.gboxJTnlcECustJob)
        Me.GroupBox2.Controls.Add(Me.mtxtJTnlcEEntDate)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.mtxtJTnlcEDate)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(909, 8)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(440, 482)
        Me.GroupBox2.TabIndex = 113
        Me.GroupBox2.TabStop = False
        '
        'btnJTnlcEISITems
        '
        Me.btnJTnlcEISITems.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcEISITems.Location = New System.Drawing.Point(365, 134)
        Me.btnJTnlcEISITems.Name = "btnJTnlcEISITems"
        Me.btnJTnlcEISITems.Size = New System.Drawing.Size(67, 98)
        Me.btnJTnlcEISITems.TabIndex = 138
        Me.btnJTnlcEISITems.Text = "IS/IH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Entry"
        Me.btnJTnlcEISITems.UseVisualStyleBackColor = True
        '
        'gboxJTnlcEInvInfo
        '
        Me.gboxJTnlcEInvInfo.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.txtJTnlcEInvFileName)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.txtJTnlcEXRef)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.gboxJTYnlcEMrgnInvd)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.cboxJTnlcESupplier)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcEFileName)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcEXref)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcELookUp)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.rbtnVendSearch)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.txtJTnlcESupCost)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcECost)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcESupplr)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.txtJTnlcEInvNum)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.txtJTnlcEDecrpt)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcEInvNum)
        Me.gboxJTnlcEInvInfo.Controls.Add(Me.lblJTnlcEDesc)
        Me.gboxJTnlcEInvInfo.Location = New System.Drawing.Point(13, 240)
        Me.gboxJTnlcEInvInfo.Name = "gboxJTnlcEInvInfo"
        Me.gboxJTnlcEInvInfo.Size = New System.Drawing.Size(420, 235)
        Me.gboxJTnlcEInvInfo.TabIndex = 137
        Me.gboxJTnlcEInvInfo.TabStop = False
        '
        'txtJTnlcEInvFileName
        '
        Me.txtJTnlcEInvFileName.Location = New System.Drawing.Point(119, 206)
        Me.txtJTnlcEInvFileName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEInvFileName.Name = "txtJTnlcEInvFileName"
        Me.txtJTnlcEInvFileName.ReadOnly = True
        Me.txtJTnlcEInvFileName.Size = New System.Drawing.Size(294, 22)
        Me.txtJTnlcEInvFileName.TabIndex = 138
        Me.txtJTnlcEInvFileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcEXRef
        '
        Me.txtJTnlcEXRef.Location = New System.Drawing.Point(7, 206)
        Me.txtJTnlcEXRef.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEXRef.Name = "txtJTnlcEXRef"
        Me.txtJTnlcEXRef.Size = New System.Drawing.Size(102, 22)
        Me.txtJTnlcEXRef.TabIndex = 139
        Me.txtJTnlcEXRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gboxJTYnlcEMrgnInvd
        '
        Me.gboxJTYnlcEMrgnInvd.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTYnlcEMrgnInvd.Controls.Add(Me.Label16)
        Me.gboxJTYnlcEMrgnInvd.Controls.Add(Me.Label17)
        Me.gboxJTYnlcEMrgnInvd.Controls.Add(Me.txtJTnlcEMrgnPC)
        Me.gboxJTYnlcEMrgnInvd.Controls.Add(Me.txtJTnlcEAmtTBInvd)
        Me.gboxJTYnlcEMrgnInvd.Location = New System.Drawing.Point(197, 143)
        Me.gboxJTYnlcEMrgnInvd.Name = "gboxJTYnlcEMrgnInvd"
        Me.gboxJTYnlcEMrgnInvd.Size = New System.Drawing.Size(186, 47)
        Me.gboxJTYnlcEMrgnInvd.TabIndex = 119
        Me.gboxJTYnlcEMrgnInvd.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(12, 5)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(63, 16)
        Me.Label16.TabIndex = 126
        Me.Label16.Text = "Margin %"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(97, 6)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(73, 16)
        Me.Label17.TabIndex = 127
        Me.Label17.Text = "JobInv Amt"
        '
        'txtJTnlcEMrgnPC
        '
        Me.txtJTnlcEMrgnPC.Location = New System.Drawing.Point(15, 20)
        Me.txtJTnlcEMrgnPC.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEMrgnPC.Name = "txtJTnlcEMrgnPC"
        Me.txtJTnlcEMrgnPC.Size = New System.Drawing.Size(41, 22)
        Me.txtJTnlcEMrgnPC.TabIndex = 128
        Me.txtJTnlcEMrgnPC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTnlcEAmtTBInvd
        '
        Me.txtJTnlcEAmtTBInvd.Location = New System.Drawing.Point(87, 21)
        Me.txtJTnlcEAmtTBInvd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEAmtTBInvd.Name = "txtJTnlcEAmtTBInvd"
        Me.txtJTnlcEAmtTBInvd.ReadOnly = True
        Me.txtJTnlcEAmtTBInvd.Size = New System.Drawing.Size(87, 22)
        Me.txtJTnlcEAmtTBInvd.TabIndex = 119
        Me.txtJTnlcEAmtTBInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboxJTnlcESupplier
        '
        Me.cboxJTnlcESupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnlcESupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnlcESupplier.FormattingEnabled = True
        Me.cboxJTnlcESupplier.Location = New System.Drawing.Point(12, 118)
        Me.cboxJTnlcESupplier.Name = "cboxJTnlcESupplier"
        Me.cboxJTnlcESupplier.Size = New System.Drawing.Size(288, 24)
        Me.cboxJTnlcESupplier.TabIndex = 117
        '
        'lblJTnlcEFileName
        '
        Me.lblJTnlcEFileName.AutoSize = True
        Me.lblJTnlcEFileName.Location = New System.Drawing.Point(115, 190)
        Me.lblJTnlcEFileName.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcEFileName.Name = "lblJTnlcEFileName"
        Me.lblJTnlcEFileName.Size = New System.Drawing.Size(159, 16)
        Me.lblJTnlcEFileName.TabIndex = 133
        Me.lblJTnlcEFileName.Text = "Invoice Image File  Name"
        '
        'lblJTnlcEXref
        '
        Me.lblJTnlcEXref.AutoSize = True
        Me.lblJTnlcEXref.Location = New System.Drawing.Point(8, 189)
        Me.lblJTnlcEXref.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcEXref.Name = "lblJTnlcEXref"
        Me.lblJTnlcEXref.Size = New System.Drawing.Size(50, 16)
        Me.lblJTnlcEXref.TabIndex = 132
        Me.lblJTnlcEXref.Text = "X-Ref #"
        '
        'lblJTnlcELookUp
        '
        Me.lblJTnlcELookUp.AutoSize = True
        Me.lblJTnlcELookUp.Location = New System.Drawing.Point(306, 99)
        Me.lblJTnlcELookUp.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcELookUp.Name = "lblJTnlcELookUp"
        Me.lblJTnlcELookUp.Size = New System.Drawing.Size(81, 16)
        Me.lblJTnlcELookUp.TabIndex = 130
        Me.lblJTnlcELookUp.Text = "Supplr LkUp"
        '
        'rbtnVendSearch
        '
        Me.rbtnVendSearch.AutoSize = True
        Me.rbtnVendSearch.Location = New System.Drawing.Point(340, 121)
        Me.rbtnVendSearch.Name = "rbtnVendSearch"
        Me.rbtnVendSearch.Size = New System.Drawing.Size(17, 16)
        Me.rbtnVendSearch.TabIndex = 115
        Me.rbtnVendSearch.TabStop = True
        Me.rbtnVendSearch.UseVisualStyleBackColor = True
        '
        'txtJTnlcESupCost
        '
        Me.txtJTnlcESupCost.Location = New System.Drawing.Point(89, 164)
        Me.txtJTnlcESupCost.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcESupCost.Name = "txtJTnlcESupCost"
        Me.txtJTnlcESupCost.Size = New System.Drawing.Size(81, 22)
        Me.txtJTnlcESupCost.TabIndex = 118
        Me.txtJTnlcESupCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblJTnlcECost
        '
        Me.lblJTnlcECost.AutoSize = True
        Me.lblJTnlcECost.Location = New System.Drawing.Point(108, 147)
        Me.lblJTnlcECost.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcECost.Name = "lblJTnlcECost"
        Me.lblJTnlcECost.Size = New System.Drawing.Size(34, 16)
        Me.lblJTnlcECost.TabIndex = 125
        Me.lblJTnlcECost.Text = "Cost"
        '
        'lblJTnlcESupplr
        '
        Me.lblJTnlcESupplr.AutoSize = True
        Me.lblJTnlcESupplr.Location = New System.Drawing.Point(8, 99)
        Me.lblJTnlcESupplr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcESupplr.Name = "lblJTnlcESupplr"
        Me.lblJTnlcESupplr.Size = New System.Drawing.Size(128, 16)
        Me.lblJTnlcESupplr.TabIndex = 124
        Me.lblJTnlcESupplr.Text = "Spplr/Contrctr Name"
        '
        'txtJTnlcEInvNum
        '
        Me.txtJTnlcEInvNum.Location = New System.Drawing.Point(6, 164)
        Me.txtJTnlcEInvNum.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEInvNum.Name = "txtJTnlcEInvNum"
        Me.txtJTnlcEInvNum.Size = New System.Drawing.Size(67, 22)
        Me.txtJTnlcEInvNum.TabIndex = 117
        '
        'txtJTnlcEDecrpt
        '
        Me.txtJTnlcEDecrpt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTnlcEDecrpt.Location = New System.Drawing.Point(12, 22)
        Me.txtJTnlcEDecrpt.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTnlcEDecrpt.MaxLength = 1024
        Me.txtJTnlcEDecrpt.Multiline = True
        Me.txtJTnlcEDecrpt.Name = "txtJTnlcEDecrpt"
        Me.txtJTnlcEDecrpt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtJTnlcEDecrpt.Size = New System.Drawing.Size(401, 73)
        Me.txtJTnlcEDecrpt.TabIndex = 113
        Me.txtJTnlcEDecrpt.TabStop = False
        '
        'lblJTnlcEInvNum
        '
        Me.lblJTnlcEInvNum.AutoSize = True
        Me.lblJTnlcEInvNum.Location = New System.Drawing.Point(6, 147)
        Me.lblJTnlcEInvNum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcEInvNum.Name = "lblJTnlcEInvNum"
        Me.lblJTnlcEInvNum.Size = New System.Drawing.Size(60, 16)
        Me.lblJTnlcEInvNum.TabIndex = 123
        Me.lblJTnlcEInvNum.Text = "Invoice #"
        '
        'lblJTnlcEDesc
        '
        Me.lblJTnlcEDesc.AutoSize = True
        Me.lblJTnlcEDesc.Location = New System.Drawing.Point(4, 1)
        Me.lblJTnlcEDesc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblJTnlcEDesc.Name = "lblJTnlcEDesc"
        Me.lblJTnlcEDesc.Size = New System.Drawing.Size(204, 16)
        Me.lblJTnlcEDesc.TabIndex = 122
        Me.lblJTnlcEDesc.Text = "Description of Material or Service"
        '
        'gboxJTnlcECustJob
        '
        Me.gboxJTnlcECustJob.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTnlcECustJob.Controls.Add(Me.cboxJTnlcESubID)
        Me.gboxJTnlcECustJob.Controls.Add(Me.Label4)
        Me.gboxJTnlcECustJob.Controls.Add(Me.cboxJTnlcEJobID)
        Me.gboxJTnlcECustJob.Controls.Add(Me.Label9)
        Me.gboxJTnlcECustJob.Location = New System.Drawing.Point(16, 131)
        Me.gboxJTnlcECustJob.Name = "gboxJTnlcECustJob"
        Me.gboxJTnlcECustJob.Size = New System.Drawing.Size(343, 102)
        Me.gboxJTnlcECustJob.TabIndex = 117
        Me.gboxJTnlcECustJob.TabStop = False
        Me.gboxJTnlcECustJob.Text = "Customer Job Information"
        '
        'cboxJTnlcESubID
        '
        Me.cboxJTnlcESubID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnlcESubID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnlcESubID.FormattingEnabled = True
        Me.cboxJTnlcESubID.Location = New System.Drawing.Point(54, 64)
        Me.cboxJTnlcESubID.Name = "cboxJTnlcESubID"
        Me.cboxJTnlcESubID.Size = New System.Drawing.Size(274, 24)
        Me.cboxJTnlcESubID.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 38)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 16)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "Customer"
        '
        'cboxJTnlcEJobID
        '
        Me.cboxJTnlcEJobID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboxJTnlcEJobID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboxJTnlcEJobID.FormattingEnabled = True
        Me.cboxJTnlcEJobID.Location = New System.Drawing.Point(85, 31)
        Me.cboxJTnlcEJobID.Name = "cboxJTnlcEJobID"
        Me.cboxJTnlcEJobID.Size = New System.Drawing.Size(243, 24)
        Me.cboxJTnlcEJobID.TabIndex = 137
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 68)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 16)
        Me.Label9.TabIndex = 114
        Me.Label9.Text = "Job"
        '
        'mtxtJTnlcEEntDate
        '
        Me.mtxtJTnlcEEntDate.CausesValidation = False
        Me.mtxtJTnlcEEntDate.Location = New System.Drawing.Point(109, 15)
        Me.mtxtJTnlcEEntDate.Mask = "00/00/0000"
        Me.mtxtJTnlcEEntDate.Name = "mtxtJTnlcEEntDate"
        Me.mtxtJTnlcEEntDate.Size = New System.Drawing.Size(98, 22)
        Me.mtxtJTnlcEEntDate.TabIndex = 135
        Me.mtxtJTnlcEEntDate.ValidatingType = GetType(Date)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(16, 18)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(69, 16)
        Me.Label18.TabIndex = 136
        Me.Label18.Text = "Entry Date"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox3.Controls.Add(Me.radiobtnJTnlcEAdj)
        Me.GroupBox3.Controls.Add(Me.radiobtnJTnlcEISItems)
        Me.GroupBox3.Controls.Add(Me.radiobtnJTnlcENJRE)
        Me.GroupBox3.Controls.Add(Me.radiobtnJTnlcEMat)
        Me.GroupBox3.Controls.Add(Me.radiobtnJTnlcESer)
        Me.GroupBox3.Location = New System.Drawing.Point(15, 43)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(404, 52)
        Me.GroupBox3.TabIndex = 129
        Me.GroupBox3.TabStop = False
        '
        'radiobtnJTnlcEAdj
        '
        Me.radiobtnJTnlcEAdj.AutoSize = True
        Me.radiobtnJTnlcEAdj.Location = New System.Drawing.Point(127, 6)
        Me.radiobtnJTnlcEAdj.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTnlcEAdj.Name = "radiobtnJTnlcEAdj"
        Me.radiobtnJTnlcEAdj.Size = New System.Drawing.Size(94, 20)
        Me.radiobtnJTnlcEAdj.TabIndex = 109
        Me.radiobtnJTnlcEAdj.TabStop = True
        Me.radiobtnJTnlcEAdj.Text = "Adjustment"
        Me.radiobtnJTnlcEAdj.UseVisualStyleBackColor = True
        '
        'radiobtnJTnlcEISItems
        '
        Me.radiobtnJTnlcEISItems.AutoSize = True
        Me.radiobtnJTnlcEISItems.Location = New System.Drawing.Point(128, 28)
        Me.radiobtnJTnlcEISItems.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTnlcEISItems.Name = "radiobtnJTnlcEISItems"
        Me.radiobtnJTnlcEISItems.Size = New System.Drawing.Size(81, 20)
        Me.radiobtnJTnlcEISItems.TabIndex = 108
        Me.radiobtnJTnlcEISItems.TabStop = True
        Me.radiobtnJTnlcEISItems.Text = "I.S. Items"
        Me.radiobtnJTnlcEISItems.UseVisualStyleBackColor = True
        '
        'radiobtnJTnlcENJRE
        '
        Me.radiobtnJTnlcENJRE.AutoSize = True
        Me.radiobtnJTnlcENJRE.Location = New System.Drawing.Point(302, 7)
        Me.radiobtnJTnlcENJRE.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTnlcENJRE.Name = "radiobtnJTnlcENJRE"
        Me.radiobtnJTnlcENJRE.Size = New System.Drawing.Size(81, 36)
        Me.radiobtnJTnlcENJRE.TabIndex = 107
        Me.radiobtnJTnlcENJRE.Text = "NJR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Expense"
        Me.radiobtnJTnlcENJRE.UseVisualStyleBackColor = True
        '
        'radiobtnJTnlcEMat
        '
        Me.radiobtnJTnlcEMat.AutoSize = True
        Me.radiobtnJTnlcEMat.Checked = True
        Me.radiobtnJTnlcEMat.Location = New System.Drawing.Point(12, 7)
        Me.radiobtnJTnlcEMat.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTnlcEMat.Name = "radiobtnJTnlcEMat"
        Me.radiobtnJTnlcEMat.Size = New System.Drawing.Size(76, 20)
        Me.radiobtnJTnlcEMat.TabIndex = 3
        Me.radiobtnJTnlcEMat.TabStop = True
        Me.radiobtnJTnlcEMat.Text = "Material"
        Me.radiobtnJTnlcEMat.UseVisualStyleBackColor = True
        '
        'radiobtnJTnlcESer
        '
        Me.radiobtnJTnlcESer.AutoSize = True
        Me.radiobtnJTnlcESer.Location = New System.Drawing.Point(12, 29)
        Me.radiobtnJTnlcESer.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTnlcESer.Name = "radiobtnJTnlcESer"
        Me.radiobtnJTnlcESer.Size = New System.Drawing.Size(74, 20)
        Me.radiobtnJTnlcESer.TabIndex = 102
        Me.radiobtnJTnlcESer.Text = "Service"
        Me.radiobtnJTnlcESer.UseVisualStyleBackColor = True
        '
        'mtxtJTnlcEDate
        '
        Me.mtxtJTnlcEDate.CausesValidation = False
        Me.mtxtJTnlcEDate.Location = New System.Drawing.Point(114, 101)
        Me.mtxtJTnlcEDate.Mask = "00/00/0000"
        Me.mtxtJTnlcEDate.Name = "mtxtJTnlcEDate"
        Me.mtxtJTnlcEDate.Size = New System.Drawing.Size(110, 22)
        Me.mtxtJTnlcEDate.TabIndex = 111
        Me.mtxtJTnlcEDate.ValidatingType = GetType(Date)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 104)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 16)
        Me.Label5.TabIndex = 121
        Me.Label5.Text = "Invoice Date"
        '
        'mtxtJTnlcWEDate
        '
        Me.mtxtJTnlcWEDate.BeepOnError = True
        Me.mtxtJTnlcWEDate.Location = New System.Drawing.Point(392, -66)
        Me.mtxtJTnlcWEDate.Margin = New System.Windows.Forms.Padding(4)
        Me.mtxtJTnlcWEDate.Mask = "00/00/0000"
        Me.mtxtJTnlcWEDate.Name = "mtxtJTnlcWEDate"
        Me.mtxtJTnlcWEDate.Size = New System.Drawing.Size(100, 22)
        Me.mtxtJTnlcWEDate.TabIndex = 112
        Me.mtxtJTnlcWEDate.ValidatingType = GetType(Date)
        '
        'lvJobInvDt
        '
        Me.lvJobInvDt.Text = "JobInv Dt"
        Me.lvJobInvDt.Width = 80
        '
        'slJTnlcKey
        '
        Me.slJTnlcKey.Width = 0
        '
        'PymtMeth
        '
        Me.PymtMeth.Text = "P.M."
        Me.PymtMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.PymtMeth.Width = 35
        '
        'btnJtnlcEExit
        '
        Me.btnJtnlcEExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJtnlcEExit.Location = New System.Drawing.Point(1356, 149)
        Me.btnJtnlcEExit.Name = "btnJtnlcEExit"
        Me.btnJtnlcEExit.Size = New System.Drawing.Size(108, 135)
        Me.btnJtnlcEExit.TabIndex = 118
        Me.btnJtnlcEExit.Text = "E X I T"
        Me.btnJtnlcEExit.UseVisualStyleBackColor = True
        '
        'btnJTnlcERecord
        '
        Me.btnJTnlcERecord.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcERecord.Location = New System.Drawing.Point(1356, 8)
        Me.btnJTnlcERecord.Name = "btnJTnlcERecord"
        Me.btnJTnlcERecord.Size = New System.Drawing.Size(108, 135)
        Me.btnJTnlcERecord.TabIndex = 119
        Me.btnJTnlcERecord.Text = "Record," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Modify"
        Me.btnJTnlcERecord.UseVisualStyleBackColor = True
        '
        'gboxInv1PDF
        '
        Me.gboxInv1PDF.Controls.Add(Me.WebBrowser1)
        Me.gboxInv1PDF.Location = New System.Drawing.Point(2, 6)
        Me.gboxInv1PDF.Name = "gboxInv1PDF"
        Me.gboxInv1PDF.Size = New System.Drawing.Size(880, 680)
        Me.gboxInv1PDF.TabIndex = 120
        Me.gboxInv1PDF.TabStop = False
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.Location = New System.Drawing.Point(3, 18)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(874, 659)
        Me.WebBrowser1.TabIndex = 0
        '
        'btnJTnlcEDeleteEntry
        '
        Me.btnJTnlcEDeleteEntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlcEDeleteEntry.Location = New System.Drawing.Point(1356, 290)
        Me.btnJTnlcEDeleteEntry.Name = "btnJTnlcEDeleteEntry"
        Me.btnJTnlcEDeleteEntry.Size = New System.Drawing.Size(108, 135)
        Me.btnJTnlcEDeleteEntry.TabIndex = 121
        Me.btnJTnlcEDeleteEntry.Text = "D E L E T E" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "E N T R Y"
        Me.btnJTnlcEDeleteEntry.UseVisualStyleBackColor = True
        '
        'gboxJTnlcENoImage
        '
        Me.gboxJTnlcENoImage.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.gboxJTnlcENoImage.Controls.Add(Me.TextBox1)
        Me.gboxJTnlcENoImage.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxJTnlcENoImage.Location = New System.Drawing.Point(223, 224)
        Me.gboxJTnlcENoImage.Name = "gboxJTnlcENoImage"
        Me.gboxJTnlcENoImage.Size = New System.Drawing.Size(394, 199)
        Me.gboxJTnlcENoImage.TabIndex = 122
        Me.gboxJTnlcENoImage.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(41, 41)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(293, 127)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "IMAGE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "AVAILABLE"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnJTnlceMergePDF
        '
        Me.btnJTnlceMergePDF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTnlceMergePDF.Location = New System.Drawing.Point(1356, 432)
        Me.btnJTnlceMergePDF.Name = "btnJTnlceMergePDF"
        Me.btnJTnlceMergePDF.Size = New System.Drawing.Size(108, 55)
        Me.btnJTnlceMergePDF.TabIndex = 123
        Me.btnJTnlceMergePDF.Text = "Multi-Page " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoice Merge"
        Me.btnJTnlceMergePDF.UseVisualStyleBackColor = True
        '
        'JTnlcE
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1472, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJTnlceMergePDF)
        Me.Controls.Add(Me.gboxJTnlcENoImage)
        Me.Controls.Add(Me.btnJTnlcEDeleteEntry)
        Me.Controls.Add(Me.gboxInv1PDF)
        Me.Controls.Add(Me.btnJTnlcERecord)
        Me.Controls.Add(Me.btnJtnlcEExit)
        Me.Controls.Add(Me.gboxJTnlcPymtSpecInfo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.mtxtJTnlcWEDate)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "JTnlcE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JTnlcE - Job Information Manager - Non Labor Cost - Invoice Processing"
        Me.gboxJTnlcPymtSpecInfo.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.gboxJTviInterfaceOpt.ResumeLayout(False)
        Me.gboxJTviInterfaceOpt.PerformLayout()
        Me.gboxJTnlcENJRA.ResumeLayout(False)
        Me.gboxJTnlcENJRA.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gboxJTnlcEInvInfo.ResumeLayout(False)
        Me.gboxJTnlcEInvInfo.PerformLayout()
        Me.gboxJTYnlcEMrgnInvd.ResumeLayout(False)
        Me.gboxJTYnlcEMrgnInvd.PerformLayout()
        Me.gboxJTnlcECustJob.ResumeLayout(False)
        Me.gboxJTnlcECustJob.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gboxInv1PDF.ResumeLayout(False)
        Me.gboxJTnlcENoImage.ResumeLayout(False)
        Me.gboxJTnlcENoImage.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents gboxJTnlcPymtSpecInfo As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtJTnlcviTerms As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtJTnlcviDiscAmt As TextBox
    Friend WithEvents txtJTviDiscPercent As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents txtJTviAcctNum As TextBox
    Friend WithEvents txtJTviStreet As TextBox
    Friend WithEvents txtJTviCityStateZip As TextBox
    Friend WithEvents txtJTviVendName As TextBox
    Friend WithEvents gboxJTviInterfaceOpt As GroupBox
    Friend WithEvents rbtnJTnlcEviJourOnly As RadioButton
    Friend WithEvents rbtnJTnlcEviManual As RadioButton
    Friend WithEvents rbtnJTnlcEviExport As RadioButton
    Friend WithEvents rbtnJTnlcEviReconcile As RadioButton
    Friend WithEvents Label3 As Label
    Friend WithEvents gboxJTnlcENJRA As GroupBox
    Friend WithEvents lvMorS As ColumnHeader
    Friend WithEvents lvCstToCust As ColumnHeader
    Friend WithEvents lvMargin As ColumnHeader
    Friend WithEvents lvInvCost As ColumnHeader
    Friend WithEvents lvInvoice As ColumnHeader
    Friend WithEvents lvSpplrName As ColumnHeader
    Friend WithEvents lvDescrpt As ColumnHeader
    Friend WithEvents lvSubName As ColumnHeader
    Friend WithEvents lvDate As ColumnHeader
    Friend WithEvents lvJobID As ColumnHeader
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents mtxtJTnlcWEDate As MaskedTextBox
    Friend WithEvents lvJobInvDt As ColumnHeader
    Friend WithEvents slJTnlcKey As ColumnHeader
    Friend WithEvents PymtMeth As ColumnHeader
    Friend WithEvents mtxtJTnlcEEntDate As MaskedTextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents radiobtnJTnlcENJRE As RadioButton
    Friend WithEvents radiobtnJTnlcEMat As RadioButton
    Friend WithEvents radiobtnJTnlcESer As RadioButton
    Friend WithEvents mtxtJTnlcEDate As MaskedTextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtJTnlcEInvNum As TextBox
    Friend WithEvents txtJTnlcEDecrpt As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblJTnlcEDesc As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents cboxJTnlcENJREAcct As ComboBox
    Friend WithEvents gboxJTnlcECustJob As GroupBox
    Friend WithEvents cboxJTnlcESubID As ComboBox
    Friend WithEvents cboxJTnlcEJobID As ComboBox
    Friend WithEvents cboxJTnlcESupplier As ComboBox
    Friend WithEvents btnJtnlcEExit As Button
    Friend WithEvents btnJTnlcERecord As Button
    Friend WithEvents gboxInv1PDF As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents radiobtnJTnlcEAdj As RadioButton
    Friend WithEvents radiobtnJTnlcEISItems As RadioButton
    Friend WithEvents gboxJTYnlcEMrgnInvd As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents txtJTnlcEMrgnPC As TextBox
    Friend WithEvents txtJTnlcEAmtTBInvd As TextBox
    Friend WithEvents lblJTnlcEFileName As Label
    Friend WithEvents lblJTnlcEXref As Label
    Friend WithEvents lblJTnlcELookUp As Label
    Friend WithEvents rbtnVendSearch As RadioButton
    Friend WithEvents txtJTnlcESupCost As TextBox
    Friend WithEvents lblJTnlcECost As Label
    Friend WithEvents lblJTnlcESupplr As Label
    Friend WithEvents lblJTnlcEInvNum As Label
    Friend WithEvents gboxJTnlcEInvInfo As GroupBox
    Friend WithEvents txtJTnlcEInvFileName As TextBox
    Friend WithEvents txtJTnlcEXRef As TextBox
    Friend WithEvents btnJTnlcEISITems As Button
    Friend WithEvents btnJTnlcEDeleteEntry As Button
    Friend WithEvents gboxJTnlcENoImage As GroupBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents btnJTnlceMergePDF As Button
    Friend WithEvents mtxtJTnlcEviDueDate As MaskedTextBox
    Friend WithEvents WebBrowser1 As WebBrowser
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
