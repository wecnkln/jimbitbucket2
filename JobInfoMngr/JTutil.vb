﻿'   Imports iTextSharp.text.pdf
'   Imports iTextSharp.text
Imports System.IO


Public Class JTutil

    Public Sub JTutilInit()
        rbtnPrtDoc.Checked = False
        rbtnJobDeact.Checked = False
        rbtnInvVoid.Checked = False
        tabJTutil.SelectedTab = tabJTutilSelections

        Me.Show()
    End Sub

    Public Sub JTutilInitPFA()
        tabJTutil.SelectedTab = tabJTutilPFA
        rbtnPrtDoc.Checked = False
        RadioBtnJTutilInv.Checked = False
        RadioBtnJTutilRec.Checked = False
        RadioBtnJTutilQB.Checked = False
        Me.Show()
    End Sub
    Public Sub JTutilInitIV()
        tabJTutil.SelectedTab = tabJTutilIV
        rbtnInvVoid.Checked = False
        '
        txtJTutilJobID.Text = ""
        txtJTutilSubJob.Text = ""
        txtJTutilInvDate.Text = ""
        txtJTutilInvNum.Text = ""
        txtJTutilInvNum.Select()
        btnJTutilLookUp.Visible = True
        btnJTutilVoid.Visible = False
        Me.Show()
    End Sub
    Public Sub JTutilInitIL()
        tabJTutil.SelectedTab = tabJTutilIL
        btnJTutilILDisplay.Visible = False
        btnJTutilILInvHist.Visible = False
        txtJTutilILInvNum.Text = ""
        txtJTutilILJob.Text = ""
        txtJTutilILSubjob.Text = ""
        txtJTutilILInvDate.Text = ""
        txtJTutilILInvAmt.Text = ""
        Me.Show()
    End Sub
    Public Sub JTutiInitJDeact()
        JTutilPopDeactCandidates()
        tabJTutil.SelectedTab = tabJTutilJDeact
        rbtnJobDeact.Checked = False
        Me.Show()
    End Sub
    Private Sub btnJtutilDone_Click(sender As Object, e As EventArgs) Handles btnJtutilDone.Click
        lvJTutilPDFNames.Clear()
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub

    Private Sub RadioBtnJTutilInv_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBtnJTutilInv.CheckedChanged
        If RadioBtnJTutilInv.Checked = True Then
            JTutilFillInvFileNames()
        End If
    End Sub
    Private Sub RadioBtnJTutilRec_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBtnJTutilRec.CheckedChanged
        If RadioBtnJTutilRec.Checked = True Then
            lvJTutilPDFNames.Clear()
            Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePath & "ReconcilePDF\", "*.pdf")
            Dim tmpPrefixLen As Integer = Len(JTVarMaint.JTvmFilePath & "ReconcilePDF\")
            ' Process the list of .txt files found in the directory. '
            Dim fileName As String

            For Each fileName In fileEntries
                Dim tmpSmallFilename As String = fileName.Substring(tmpPrefixLen, Len(fileName) - tmpPrefixLen)
                lvJTutilPDFNames.Items.Add(tmpSmallFilename)
            Next
        End If
    End Sub
    Private Sub RadioBtnJTutilQB_CheckedChanged(sender As Object, e As EventArgs) Handles RadioBtnJTutilQB.CheckedChanged
        If RadioBtnJTutilQB.Checked = True Then
            lvJTutilPDFNames.Clear()
            Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePath & "QBPDF\", "*.pdf")
            Dim tmpPrefixLen As Integer = Len(JTVarMaint.JTvmFilePath & "QBPDF\")
            ' Process the list of .txt files found in the directory. '
            Dim fileName As String

            For Each fileName In fileEntries
                Dim tmpSmallFilename As String = fileName.Substring(tmpPrefixLen, Len(fileName) - tmpPrefixLen)
                lvJTutilPDFNames.Items.Add(tmpSmallFilename)
            Next
        End If
    End Sub
    Private Sub lvJTutilPDFNames_DoubleClick(sender As Object, e As EventArgs) Handles lvJTutilPDFNames.DoubleClick
        Me.TopMost = False
        MsgBox("This routine has been modified to find files from 2021 only. It will be replaced by a new DocLibrary function. " &
               "Sub name JTutil.lvJTutilPDFNames_DoubleClick --- JTutil~96 --- 12/6/2021")
        If RadioBtnJTutilRec.Checked = True Then
            Process.Start(JTVarMaint.JTvmFilePathDoc & "ReconcilePDF\2021\" & lvJTutilPDFNames.SelectedItems(0).Text)
        End If
        If RadioBtnJTutilInv.Checked = True Then
            Process.Start(JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\2021\" & lvJTutilPDFNames.SelectedItems(0).Text)
            If JTas.JTasChkEmail(lvJTutilPDFNames.SelectedItems(0).Text) = False Then
                Select Case MsgBox("This invoice has not been emailed. Would you" & vbCrLf & "like to email it now? (UTIL.006)" & vbCrLf & vbCrLf &
                       "'YES'- Invoice will be emailed." & vbCrLf &
                       "'NO' - Invoice email is cancelled." & vbCrLf &
                       "'CANCEL' - No action is taken." & vbCrLf, MsgBoxStyle.YesNoCancel, "Invoice Emailing Utility")
                    Case MsgBoxResult.Yes
                        If CheckForInternetConnection() = False Then
                            MsgBox("Computer does not currently have Internet access. Resolve issue and try again. (UTIL.007")
                        Else
                            Dim tmpJob As String = ""
                            Dim tmpSubJob As String = ""
                            Dim tmpInvNumber As String = ""
                            Dim tmpInvDate As String = ""
                            Dim tmpToAdd As String = ""
                            Dim tmpCC As String = JTVarMaint.JTCCEmailAddress
                            Dim tmpBCC As String = ""
                            Dim tmpFromAdd As String = JTVarMaint.JTFromEmailAddress
                            Dim tmpAttach As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\2021" & lvJTutilPDFNames.SelectedItems(0).Text
                            JTas.JTasInvEmailInfo(lvJTutilPDFNames.SelectedItems(0).Text, tmpJob, tmpSubJob,
                                                  tmpInvNumber, tmpInvDate)
                            tmpToAdd = JTjim.JTjimInvAddTo(tmpJob, tmpSubJob)
                            Dim tmpSubject As String = JTVarMaint.CompanyNme & " - Invoice # " & tmpInvNumber
                            Dim tmpMsg As String = "Attached is Invoice # " & tmpInvNumber & "  " & tmpInvDate
                            JTEmail.JTSndEmail(tmpToAdd, tmpCC, tmpBCC, tmpFromAdd, tmpSubject, tmpMsg, tmpAttach)
                            Dim tmpEmailDate As String = String.Format("{0:mm/dd/yyyy}", Date.Today)
                            JTas.JTasUpdtInvEmailDt(lvJTutilPDFNames.SelectedItems(0).Text, tmpEmailDate)
                            JTas.JTasHistFlush()
                            JTutilFillInvFileNames()
                        End If
                    Case MsgBoxResult.Cancel
                        ' Everything is left just the way it was ... no changes.
                    Case MsgBoxResult.No
                        JTas.JTasUpdtInvEmailDt(lvJTutilPDFNames.SelectedItems(0).Text, "Cancelled")
                        JTas.JTasHistFlush()
                        JTutilFillInvFileNames()
                End Select
            End If
        End If
        If RadioBtnJTutilQB.Checked = True Then
            Process.Start(JTVarMaint.JTvmFilePathDoc & "QBPDF\2021" & lvJTutilPDFNames.SelectedItems(0).Text)
        End If
    End Sub
    ' --------------------------------------------------------------------------------
    ' Code to support invoice void routine.
    ' --------------------------------------------------------------------------------
    '
    Private Sub txtJTutilJobID_LostFocus(sender As Object, e As EventArgs) Handles txtJTutilJobID.LostFocus

        txtJTutilJobID.Text = txtJTutilJobID.Text.ToUpper
        Dim tmpPOS As String = ""
        Dim tmpJobId As String = txtJTutilJobID.Text
        Dim tmpSubName As String = ""
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpCstMeth As String = ""
        If JTjim.JTjimFindFunc(tmpPOS, tmpJobId, tmpSubName, tmpJobType, tmpDeactDate) <> "DATA" Then
            ToolTip1.Show("Invalidate Job Name entered ... Correct And resubmit.(UTIL.001)", txtJTutilJobID, 80, 10, 7000)
            txtJTutilJobID.Focus()
            Exit Sub

        Else
            If tmpJobType = "M" Then
                txtJTutilSubJob.ReadOnly = False
                txtJTutilSubJob.Select()
                '
                Dim JTtkSubList As New AutoCompleteStringCollection
                Dim tmpJobSub = 0
                Dim tmpJobName As String = ""
                '
                Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
                    If tmpJobName = txtJTutilJobID.Text And tmpSubName <> "" Then
                        JTtkSubList.Add(tmpSubName)
                    End If
                    tmpJobSub += 1
                Loop
                'Then Set ComboBox AutoComplete properties
                txtJTutilSubJob.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                txtJTutilSubJob.AutoCompleteSource = AutoCompleteSource.CustomSource
                txtJTutilSubJob.AutoCompleteCustomSource = JTtkSubList

            Else
                txtJTutilSubJob.ReadOnly = True
                txtJTutilInvDate.Select()
            End If
        End If
    End Sub
    Private Sub txtJTutilSubJob_TextChanged_1(sender As Object, e As EventArgs) Handles txtJTutilSubJob.LostFocus

        txtJTutilSubJob.Text = txtJTutilSubJob.Text.ToUpper
        Dim tmpPOS As String = ""
        Dim tmpJobId As String = txtJTutilJobID.Text
        Dim tmpSubName As String = txtJTutilSubJob.Text
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        If JTjim.JTjimFindFunc(tmpPOS, tmpJobId, tmpSubName, tmpJobType, tmpDeactDate) <> "DATA" Then
            ToolTip1.Show("Invalidate SubJob Name entered ... Correct And resubmit.(UTIL.002)", txtJTutilJobID, 80, 10, 7000)
            txtJTutilSubJob.Focus()
            Exit Sub
        End If


    End Sub

    Private Sub mtxtJTutilInvDate_LostFocus(sender As Object, e As EventArgs) Handles txtJTutilInvDate.LostFocus
        ' -------------------------------------------------------------------------------
        '  mtxt date validation
        ' -------------------------------------------------------------------------------
        '
        ' check for entry with no year ... append year prior to validation
        '
        If txtJTutilInvDate.Text <> "  /  /" Then
            If txtJTutilInvDate.Text.Length = 6 Then
                If txtJTutilInvDate.Text.Substring(0, 2) > Today.Month Then
                    txtJTutilInvDate.Text = txtJTutilInvDate.Text & Today.Year - 1

                Else
                    txtJTutilInvDate.Text = txtJTutilInvDate.Text & Today.Year
                End If
            End If
        End If

        If IsDate(txtJTutilInvDate.Text) = False Then
            ToolTip1.Show("Invoice Date entered Not valid. Correct And resubmit.(UTIL.003)", txtJTutilInvDate, 20, -55, 6000)
            txtJTutilInvDate.Select()
            Exit Sub
        End If
        txtJTutilInvNum.Select()
    End Sub
    Private Sub btnJTutilLookUp_Click(sender As Object, e As EventArgs) Handles btnJTutilLookUp.Click
        If txtJTutilInvNum.Text = "" Or IsNumeric(txtJTutilInvNum.Text) = False Then
            ToolTip1.Show("Invoice Number Not entered or is not numeric. Correct sand Resubmit.(UTIL.004)", txtJTutilInvNum, 20, -55, 6000)
            txtJTutilInvNum.Select()
        Else
            Dim tmpInvPDFFileName As String = ""
            Dim tmpFoundInv As Boolean = False

            Dim keys As ICollection = JTas.slJTasIH.Keys
            Dim k As String
            For Each k In keys
                '

                If txtJTutilInvNum.Text = JTas.slJTasIH(k).ihInvNum Then
                    ' Invoice found

                    txtJTutilJobID.Text = JTas.slJTasIH(k).ihJobID
                    txtJTutilSubJob.Text = JTas.slJTasIH(k).ihSubID
                    txtJTutilInvDate.Text = JTas.slJTasIH(k).ihInvDate
                    btnJTutilLookUp.Visible = False
                    btnJTutilVoid.Visible = True
                    btnJTutilVoid.Select()
                    Exit Sub
                End If

            Next
            ToolTip1.Show("Invoice Number not valid. Correct sand Resubmit.(UTIL.008)", txtJTutilInvNum, 20, -55, 6000)
            txtJTutilInvNum.Select()
        End If
    End Sub
    ''' <summary>
    ''' This sub voids an invoice that has been recorded. It verifies that all work detail that were on the 
    ''' invoice are still in the active files (if the void is attempted after archive logic has taken some of the transactions,
    ''' invoice voiding will no longer be an option ... adjustments will have to be made to fix any issues.
    ''' 
    ''' After the transactions are verified, it erases the invoice date and number from the records making them 
    ''' "unbilled" again and marks the invoice PDF as "VOIDED"
    ''' </summary>
    Private Sub btnJTutilVoid_Click(sender As Object, e As EventArgs) Handles btnJTutilVoid.Click
        '
        ' Invoice Number Edit and lookup
        '
        If txtJTutilInvNum.Text = "" Or IsNumeric(txtJTutilInvNum.Text) = False Then
            ToolTip1.Show("Invoice Number Not entered or is not numeric. Correct and Resubmit.(UTIL.004)", txtJTutilInvNum, 20, -55, 6000)
            txtJTutilInvDate.Select()
        Else
            Dim tmpInvPDFFileName As String = ""
            Dim tmpFoundInv As Boolean = False
            Dim tmpInvTlHash As Double = 0
            Dim keys As ICollection = JTas.slJTasIH.Keys
            Dim k As String
            For Each k In keys
                '

                If txtJTutilInvNum.Text = JTas.slJTasIH(k).ihInvNum Then
                    ' Invoice found
                    tmpFoundInv = True
                    ' Adding up amount on the IH record for each invoice with this date and number.
                    tmpInvTlHash = tmpInvTlHash + Val(JTas.slJTasIH(k).ihLab) +
                            Val(JTas.slJTasIH(k).ihMat) +
                            Val(JTas.slJTasIH(k).ihSub) +
                            Val(JTas.slJTasIH(k).ihAdj) +
                            Val(JTas.slJTasIH(k).ihIH) +
                            Val(JTas.slJTasIH(k).ihAUPInv)
                    tmpInvPDFFileName = JTas.slJTasIH(k).ihFile
                End If
            Next
            If tmpFoundInv = True Then
                Dim tmpInvTotal As Decimal = 0
                Dim keys2 As ICollection = JTtk.JTtkslReservoir.Keys
                Dim k2 As String
                For Each k2 In keys2
                    If txtJTutilInvNum.Text = JTtk.JTtkslReservoir(k2).JTtkInvnumber Then
                        tmpInvTotal = tmpInvTotal + Val(String.Format("{0:0.00}", Val(JTtk.JTtkslReservoir(k2).JTtkDolAmt)))
                    End If
                Next
                '
                Dim keys3 As ICollection = JTnlc.slJTnlc.Keys
                Dim k3 As String = ""
                For Each k3 In keys3
                    If txtJTutilInvNum.Text = JTnlc.slJTnlc(k3).CustInvNumber Then
                        tmpInvTotal = tmpInvTotal + Val(String.Format("{0:0.00}", Val(JTnlc.slJTnlc(k3).TBInvd)))
                    End If
                Next
                '

                Dim keys4 As ICollection = JTaup.slJTaupInv.Keys
                Dim k4 As String = ""
                For Each k4 In keys4
                    If txtJTutilInvNum.Text = JTaup.slJTaupInv(k4).JTaupIInvNum Then
                        tmpInvTotal = tmpInvTotal + Val(String.Format("{0:0.00}", Val(JTaup.slJTaupInv(k4).JTaupIAmt)))
                    End If
                Next
                '            
                If tmpInvTotal - tmpInvTlHash = 0 Then
                    Dim tmpAnswer = MsgBox("Invoice found - " & txtJTutilInvNum.Text & " - " & txtJTutilInvDate.Text &
                                           " Invoice Hash Total - " & String.Format("{0:0.00}", tmpInvTotal) & vbCrLf &
                        "Do you want to void this invoice? (Yes Or No)", vbYesNo, "Job Informstion Manager - Invoice void")
                    If tmpAnswer = DialogResult.Yes Then
                        btnJtutilDone.Enabled = False
                        ' reactivate transactions and delete IH record for this invoice.
                        Dim tmptkItem As New JTtk.JTtkArrayStructure
                        Dim tmpslCount As Integer = 0
                        keys2 = JTtk.JTtkslReservoir.Keys
                        Do While tmpslCount < keys2.Count
                            Dim tmpKey As String = keys2(tmpslCount)

                            If txtJTutilInvDate.Text = JTtk.JTtkslReservoir(tmpKey).JTtkInvDate Then
                                If txtJTutilInvNum.Text = JTtk.JTtkslReservoir(tmpKey).JTtkInvNumber Then
                                    tmptkItem = JTtk.JTtkslReservoir(tmpKey)
                                    tmptkItem.JTtkInvDate = ""
                                    tmptkItem.JTtkInvNumber = ""
                                    JTtk.JTtkslReservoir.Remove(tmpKey)
                                    JTtk.JTtkslReservoir.Add(tmpKey, tmptkItem)
                                End If
                            End If
                            tmpslCount += 1
                        Loop
                        tmptkItem = Nothing

                        '
                        Dim tmpHasVendorInvoicePDFs As Boolean = False
                        Dim tmpnlcItem As JTnlc.JTnlcStructure
                        tmpslCount = 0
                        keys3 = JTnlc.slJTnlc.Keys
                        Do While tmpslCount < keys3.Count
                            Dim tmpkey3 As String = keys3(tmpslCount)

                            If txtJTutilInvDate.Text = JTnlc.slJTnlc(tmpkey3).CustInvDate Then
                                If txtJTutilInvNum.Text = JTnlc.slJTnlc(tmpkey3).CustInvNumber Then
                                    tmpnlcItem = JTnlc.slJTnlc(tmpkey3)
                                    tmpnlcItem.CustInvDate = ""
                                    tmpnlcItem.CustInvNumber = ""
                                    '
                                    ' ----------------------------------------------------------------
                                    ' Check to see if Vendor invoice PDFs exist. If it has one, the
                                    ' logic below will call routines to rename the file with the
                                    ' original name + Customer + Job. Customer name and invoice number
                                    ' blanked out.
                                    ' ----------------------------------------------------------------
                                    If tmpnlcItem.SupInvImageFile <> "" Then
                                        Dim tmp2digmonth As String = String.Format("{0:MM}", CDate(tmpnlcItem.InvDate))
                                        Dim tmp2digday As String = String.Format("{0:dd}", CDate(tmpnlcItem.InvDate))
                                        Dim tmp4digyear As String = String.Format("{0:yyyy}", CDate(tmpnlcItem.InvDate))
                                        '
                                        Dim tmpYMDInvDate As String = tmp4digyear & tmp2digmonth & tmp2digday
                                        Dim tmpOrigInvFileName As String = tmpnlcItem.Supplier & "_" &
                                                                           tmpnlcItem.SupInvNum & "_" &
                                                                           tmpYMDInvDate

                                        JTnlc.JTnlcAddInvInfotoPDFName(tmpnlcItem.SupInvImageFile,
                                                                       tmpOrigInvFileName,
                                                                       tmpnlcItem.JobID,
                                                                       tmpnlcItem.SubID,
                                                                       tmpnlcItem.CustInvNumber,
                                                                       tmpnlcItem.CustInvDate,
                                                                       tmpnlcItem.SupCost)
                                    End If
                                    '
                                    JTnlc.slJTnlc.Remove(tmpkey3)
                                    JTnlc.slJTnlc.Add(tmpkey3, tmpnlcItem)
                                End If
                            End If
                            tmpslCount += 1
                        Loop
                        '
                        '
                        tmpnlcItem = Nothing
                        '
                        ' clear inv# and Date from AUP inv items being voided.
                        '
                        Dim tmpAUPItem As JTaup.JTaupInvStru
                        tmpslCount = 0
                        keys3 = JTaup.slJTaupInv.Keys
                        Do While tmpslCount < keys3.Count
                            Dim tmpkey3 As String = keys3(tmpslCount)
                            '
                            '    If txtJTutilInvDate.Text = JTaup.slJTaupInv(tmpkey3).JTaupIInvDate Then
                            If txtJTutilInvNum.Text = JTaup.slJTaupInv(tmpkey3).JTaupIInvNum Then
                                tmpAUPItem = JTaup.slJTaupInv(tmpkey3)
                                tmpAUPItem.JTaupIInvDate = ""
                                tmpAUPItem.JTaupIInvNum = ""
                                JTaup.slJTaupInv.Remove(tmpkey3)
                                JTaup.slJTaupInv.Add(tmpkey3, tmpAUPItem)
                            End If
                            '   End If
                            tmpslCount += 1
                        Loop
                        tmpAUPItem = Nothing
                        keys3 = Nothing
                        k3 = Nothing
                        keys2 = Nothing
                        k2 = Nothing

                        '
                        ' Overwrite invoice file with Voided text.
                        '
                        Dim tmpInvYear As String = txtJTutilInvDate.Text, substring(txtJTutilInvDate.Text.Length - 4, 4)
                        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & tmpInvYear & "\"
                        Dim tmpOrigFile As String = tmpFilePath & tmpInvPDFFileName
                        If My.Computer.FileSystem.FileExists(tmpOrigFile) = False Then


                            Dim fileEntries As String() = Directory.GetFiles(tmpFilePath, "*.PDF")
                            '    Process the list of .PDF files found in the directory. '
                            Dim fileName As String
                            Dim tmpFoundFile As Boolean = False
                            For Each fileName In fileEntries
                                If fileName.Substring(fileName.Length - 9, 4) = txtJTutilInvNum.Text Then
                                    MsgBox("Could not locate original file named as the Invoice File." & vbCrLf &
                                           "This is probably due to Customer or job name changes. The invoice PDF" & vbCrLf &
                                           "file that will be voided is ------>" & vbCrLf &
                                           fileName & vbCrLf & "Invoice # -->" & fileName.Substring(fileName.Length - 9, 4) & "< --- (UTIL.009)")
                                    tmpOrigFile = fileName
                                    tmpFoundFile = True
                                End If
                            Next
                            If tmpFoundFile = False Then
                                MsgBox("Invoice PDF file for this invoice number not found --- JTutil~419")
                                tmpOrigFile = Nothing
                            End If
                        End If
                        Dim tmpNewFile As String = Nothing
                        If tmpOrigFile <> "" Then
                            ' ---------------------------------------------------------------------------------------
                            ' Following line was corrected --- it was using wrong field for getting file name length.
                            '    - - - Old Code - - - 
                            '    Dim tmpNewFile As String = tmpOrigFile.Substring(0, tmpInvPDFFileName.Length - 4) &
                            ' ---------------------------------------------------------------------------------------
                            If tmpNewFile.Substring(tmpNewFile.Length - 11, 11) <> "_Voided.PDF" Then
                                tmpNewFile = tmpOrigFile.Substring(0, tmpOrigFile.Length - 4) &
                                "_Voided.PDF"
                            End If
                            ' 
                            AddWatermarkText(tmpOrigFile, tmpNewFile, "VOIDED INVOICE")
                                '       
                                My.Computer.FileSystem.DeleteFile(tmpOrigFile)
                            End If
                            '      
                            JTtk.TKArrayFlush()
                        JTnlc.JTnlcArrayFlush("")
                        JTaup.JTaupFlush()

                        ' =============================================================================
                        ' Enhancements made to modify and keep IH records that have been voided.
                        ' Replace total with "VOID". Replace file name with Void File name.
                        ' =============================================================================
                        '
                        tmpslCount = 0
                        keys = JTas.slJTasIH.Keys
                        Do While tmpslCount < keys.Count
                            Dim tmpkey As String = keys(tmpslCount)
                            '
                            If txtJTutilInvNum.Text = JTas.slJTasIH(tmpkey).ihInvNum Then
                                Dim tmpIHItem As JTas.JTasIHStru = JTas.slJTasIH(tmpkey)
                                ' remove original record
                                JTas.slJTasIH.Remove(tmpkey)
                                ' update fields in saved record
                                tmpIHItem.ihLab = Nothing
                                tmpIHItem.ihMat = Nothing
                                tmpIHItem.ihSub = Nothing
                                tmpIHItem.ihAdj = Nothing
                                tmpIHItem.ihIH = Nothing
                                tmpIHItem.ihEstMrgn = Nothing
                                tmpIHItem.ihAUPInv = Nothing
                                '
                                tmpIHItem.ihInvTl = "VOIDED"    ' plug Voided in total amount field
                                ' ----------------------------------------------------------------
                                ' Trimming directory information off filename entry for storage.
                                ' ----------------------------------------------------------------
                                Dim tmpFileLocation As Integer = 20 + tmpNewFile.IndexOf("CustInvoicePDF\")
                                tmpIHItem.ihFile = tmpNewFile.Substring(tmpFileLocation, tmpNewFile.Length - tmpFileLocation)   ' update new file name
                                ' clear remaining fields - - - - - 
                                tmpIHItem.ihWorkLocState = Nothing
                                tmpIHItem.ihTaxExempt = Nothing
                                tmpIHItem.ihTaxableSales = Nothing
                                tmpIHItem.ihTaxAmount = Nothing
                                Dim ihFileDetails = Nothing
                                tmpIHItem.ihCostMeth = Nothing
                                tmpIHItem.ihAUPTKsw = Nothing
                                tmpIHItem.ihAUPMATsw = Nothing
                                tmpIHItem.ihAUPSERsw = Nothing
                                tmpIHItem.ihDoNotExpt = Nothing
                                tmpIHItem.ihAcctExptDate = Nothing
                                tmpIHItem.ihEmailedDate = Nothing
                                '
                                Dim tmpihKey As String = tmpIHItem.ihJobID & tmpIHItem.ihSubID & "VOIDED" & Str(tmpslCount)
                                JTas.slJTasIH.Add(tmpihKey, tmpIHItem)
                            End If
                            '
                            tmpslCount += 1
                        Loop
                        '
                        JTas.JTasHistFlush()
                        ' Force mainmenu tables to be rebuilt
                        JTMainMenu.JTMMFirstTimeThru = "FIRST"
                        keys = Nothing
                        k = Nothing
                    Else
                        MsgBox("Invoice void operation aborted")
                    End If
                Else
                    MsgBox("Invoice found - " & txtJTutilInvNum.Text & " - " & txtJTutilInvDate.Text &
                           vbCrLf & "Some of the detailed activity on this invoice has been purged." & vbCrLf &
                           "Invoice can no longer be Voided." & vbCrLf & "Inv Hash - " & tmpInvTlHash.ToString("#,###.00") &
                           " Accum Hash - " & tmpInvTotal.ToString("#,###.00"),, "Job Informstion Manager - Invoice void")
                    JTutilInitIV()
                    Exit Sub
                End If
            Else
                ToolTip1.Show("Invoice Number entered Not found. Correct sand Resubmit.(UTIL.005)", txtJTutilInvNum, 20, -55, 6000)
                txtJTutilInvNum.Select()
                btnJTutilVoid.Visible = False
                btnJTutilLookUp.Visible = True
                Exit Sub
            End If
            MsgBox("Invoice # - " & txtJTutilInvNum.Text & " - " & txtJTutilInvDate.Text & " has been VOIDED" & vbCrLf &
                   "JobID - " & txtJTutilJobID.Text & " " & txtJTutilSubJob.Text)
            ' call to reactivate job ... just in case it was Deactivated during invoicing.
            JTjim.JTjimPurgeJob(txtJTutilJobID.Text, txtJTutilSubJob.Text, "REACTIVATE")
            ' clear fields
            txtJTutilJobID.Text = ""
            txtJTutilSubJob.Text = ""
            txtJTutilInvNum.Text = ""
            txtJTutilInvNum.Select()
            txtJTutilInvDate.Text = ""
            btnJTutilVoid.Visible = False
            btnJTutilLookUp.Visible = True
            btnJtutilDone.Enabled = True

        End If
    End Sub

    Private Sub rbtnPrtDoc_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnPrtDoc.CheckedChanged
        If rbtnPrtDoc.Checked = True Then
            tabJTutil.SelectedTab = tabJTutilPFA
            rbtnPrtDoc.Checked = False
            RadioBtnJTutilInv.Checked = False
            RadioBtnJTutilRec.Checked = False
            RadioBtnJTutilQB.Checked = False
        End If
    End Sub

    Private Sub rbtnInvVoid_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnInvVoid.CheckedChanged
        If rbtnInvVoid.Checked = True Then
            tabJTutil.SelectedTab = tabJTutilIV
            rbtnInvVoid.Checked = False
            '
            txtJTutilJobID.Text = ""
            txtJTutilSubJob.Text = ""
            txtJTutilInvDate.Text = ""
            txtJTutilInvNum.Text = ""
        End If
    End Sub

    Private Sub rbtnJobName_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJobDeact.CheckedChanged
        If rbtnJobDeact.Checked = True Then
            JtutilPopDeactCandidates()
            tabJTutil.SelectedTab = tabJTutilJDeact
            rbtnJobDeact.Checked = False
        End If
    End Sub


    Private Sub rbtnEmail_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnEmail.CheckedChanged
        Me.Hide()
        JTEmail.JTEmailInit()
    End Sub
    Private Function JTutilFillInvFileNames() As Boolean
        ' -------------------------------------------------------------------------------
        ' Function clears and fills listview of inv pdf file names. It also set color for
        ' not yet emailed invoices.
        ' 12/6/2021 --- modified fornew dir structure. Will direct search for current year only.
        ' -------------------------------------------------------------------------------
        lvJTutilPDFNames.Clear()
        Dim tmpYear As String = Date.Today.Year
        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & tmpYear & "\"
        Dim fileEntries As String() = Directory.GetFiles(tmpFilePath, "*.pdf")
        Dim tmpPrefixLen As Integer = Len(tmpFilePath)
        ' Process the list of .pdf files found in the directory. '
        Dim fileName As String

        For Each fileName In fileEntries
            Dim tmpSmallFilename As String = fileName.Substring(tmpPrefixLen, Len(fileName) - tmpPrefixLen)

            lvJTutilPDFNames.Items.Add(tmpSmallFilename)

        Next
        Dim tmpSub As Integer = 0
        Do While tmpSub < lvJTutilPDFNames.Items.Count
            If JTas.JTasChkEmail(lvJTutilPDFNames.Items(tmpSub).Text) = False Then
                lvJTutilPDFNames.Items(tmpSub).ForeColor = System.Drawing.Color.Red
            End If
            tmpSub += 1
        Loop
        Return True
    End Function

    Private Sub btnJTutilILLookUp_Click(sender As Object, e As EventArgs) Handles btnJTutilILLookUp.Click
        If txtJTutilILInvNum.Text = "" Or IsNumeric(txtJTutilILInvNum.Text) = False Then
            ToolTip1.Show("Invoice Number Not entered or is not numeric. Correct sand Resubmit.(UTIL.004)", txtJTutilILInvNum, 20, -55, 6000)
            txtJTutilILInvNum.Select()
        Else
            Dim tmpInvPDFFileName As String = ""
            Dim tmpFoundInv As Boolean = False

            Dim keys As ICollection = JTas.slJTasIH.Keys
            Dim k As String
            For Each k In keys
                '

                If txtJTutilILInvNum.Text = JTas.slJTasIH(k).ihInvNum Then
                    ' Invoice found

                    txtJTutilILJob.Text = JTas.slJTasIH(k).ihJobID
                    txtJTutilILSubjob.Text = JTas.slJTasIH(k).ihSubID
                    txtJTutilILInvDate.Text = JTas.slJTasIH(k).ihInvDate
                    txtJTutilILInvAmt.Text = JTas.slJTasIH(k).ihInvTl
                    txtJTutilILInvPDF.Text = JTas.slJTasIH(k).ihFile
                    btnJTutilILDisplay.Visible = True
                    btnJTutilILInvHist.Visible = True
                    txtJTutilILInvNum.Select()
                    Exit Sub
                End If

            Next
            ToolTip1.Show("Invoice Number not valid. Correct and Resubmit.(UTIL.008)", txtJTutilILInvNum, 20, -55, 6000)
            txtJTutilILInvNum.Select()
        End If
    End Sub

    Private Sub btnJTutilILDisplay_Click(sender As Object, e As EventArgs) Handles btnJTutilILDisplay.Click
        ' -------------------------------------------------------------------
        ' modified to handle new dir structure --- 12/6/2021
        ' -------------------------------------------------------------------
        Dim tmpCharLoc As Short
        Dim tmpYear As String = Date.Today.Year
        If txtJTutilILInvPDF.Text.Contains("_") Then
            tmpCharLoc = txtJTutilILInvPDF.Text.IndexOf("_")
        End If
        If tmpCharLoc <> -1 Then
            tmpYear = txtJTutilILInvPDF.Text.Substring(tmpCharLoc, 4)
        End If

        Dim filename As String = JTVarMaint.JTvmFilePathDoc & "CustInvoicePDF\" & tmpYear & "\" & txtJTutilILInvPDF.Text

        ' --------------------------------------------------------------------------
        ' Code inserted to check to see if invoice PDF file exists. On multiple job
        ' invoices. It may have been named a different name than is being requested.
        ' If the file doesn't exist, a routine is called to find the file name that
        ' was used.
        ' ------------------------------
        ' Search code may have a problem with prior year invoices.
        ' --------------------------------------------------------------------------
        If My.Computer.FileSystem.FileExists(filename) = False Then
            '               MsgBox("Going to look for name" & vbCrLf & "File Name Sent - " & filename & vbCrLf & "- JTAS~1292")
            JTas.JTasFindFileName(txtJTutilILInvNum.Text, filename)
            '
        End If




        JTvPDF.JTvPDFDispPDF(filename)
        JTutilInitIL()
    End Sub

    Private Sub btnJTutilILInvHist_Click(sender As Object, e As EventArgs) Handles btnJTutilILInvHist.Click
        Dim tmpJob As String = txtJTutilILJob.Text
        Dim tmpSubjob As String = txtJTutilILSubjob.Text
        Me.Hide()
        Dim tmpJobDesc As String = ""
        Dim tmpJobPricing As String = ""
        JTjim.JTjimGetILInfo(tmpJob, tmpSubjob, tmpJobDesc, tmpJobPricing)
        Dim tmpLayer As String = "Hist"
        JTas.JTasInit(tmpJob, tmpSubjob, tmpJobDesc, tmpJobPricing, tmpLayer)
    End Sub
    ''' <summary>
    ''' Function to fill listview with jobs that are candidates for deactivation. Processes a little slowly. It passes the activity
    ''' files for each job ... not a great approach, but it's only run once in a while.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTutilPopDeactCandidates() As String
        lvJTutilDeactCandidates.Items.Clear()
        Dim tmpSub As Integer = 0
        Dim tmpCust As String = Nothing
        Dim tmpJob As String = Nothing
        Dim tmpDeactDate As String = Nothing
        Dim tmpJobType As String = Nothing   ' No longer used
        Dim tmpCstMeth As String = Nothing
        '
        Dim tmpMakeDeactCandidate As Boolean = False
        '
        Do While JTjim.JTjimReadFunc(tmpSub, tmpCust, tmpJob, tmpDeactDate, tmpJobType, tmpCstMeth) <> "EMPTY"
            '
            tmpSub += 1 'add 1 to sub for next record.
            '
            tmpMakeDeactCandidate = False
            If tmpDeactDate = "" Then
                '
                Dim tmpLabAmt As Double = 0
                Dim tmpMatAmt As Double = 0
                Dim tmpSubAmt As Double = 0
                Dim tmpLstTKDate As String = Nothing
                Dim tmpLstNLCDate As String = Nothing
                '
                If JTtk.JTtkJobUnbilled(tmpCust, tmpJob, tmpLstTKDate) +
                            JTnlc.JTnlcJobUnbilled(tmpCust, tmpJob, tmpLstNLCDate) = 0 Then
                    ' check for Flag on AUP or AUPH jobs.
                    If tmpCstMeth = "AUP" Or tmpCstMeth = "AUPH" Then
                        'check for final invoice flag
                        If JTaup.JTaupFinalFlagSet(tmpCust, tmpJob) = True Then
                            tmpMakeDeactCandidate = True
                        End If
                    Else
                        tmpMakeDeactCandidate = True
                    End If
                End If
                If tmpMakeDeactCandidate = True Then
                    ' add listview line
                    Dim itmBuild(5) As String
                    Dim lvLine As ListViewItem
                    itmBuild(0) = tmpCust
                    itmBuild(1) = tmpJob
                    itmBuild(2) = tmpCstMeth
                    'get creation and last invoice date
                    Dim tmpJimInfo As JTjim.JTjimJobInfo = Nothing
                    JTjim.JTjimJTjrReadRec(tmpJimInfo, tmpCust, tmpJob)
                    itmBuild(3) = tmpJimInfo.JTjimJICreateDate
                    '
                    itmBuild(4) = JTas.JTasJobLstInvoice(tmpCust, tmpJob)
                    '
                    lvLine = New ListViewItem(itmBuild)
                    lvJTutilDeactCandidates.Items.Add(lvLine)
                End If

            End If
        Loop

        Return ""
    End Function
    ''' <summary>
    ''' Sub does actual job deactivation of selected jobs. Updates listview is represented at end of process.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTutilDCStartProcess_Click(sender As Object, e As EventArgs) Handles btnJTutilDCStartProcess.Click
        Dim tmpSub As Integer = 0
        Do While tmpSub < lvJTutilDeactCandidates.CheckedItems.Count
            Dim tmpCust As String = lvJTutilDeactCandidates.CheckedItems(tmpSub).Text
            Dim tmpJob As String = lvJTutilDeactCandidates.CheckedItems(tmpSub).SubItems(1).Text
            JTjim.JTjimPurgeJob(tmpCust, tmpJob, "DEACT")
            tmpSub += 1
        Loop
        JTutilPopDeactCandidates()
    End Sub


End Class
