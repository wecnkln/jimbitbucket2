﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTtk
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTtk))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtJTtkEmpID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTtkEmpName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtJTtkDate = New System.Windows.Forms.TextBox()
        Me.txtJTtkJobID = New System.Windows.Forms.TextBox()
        Me.txtJTtkActivity = New System.Windows.Forms.TextBox()
        Me.txtJTtkHours = New System.Windows.Forms.TextBox()
        Me.radiobtnJTtkShopRate = New System.Windows.Forms.RadioButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtJTtkSubJob = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJTtkLabCode = New System.Windows.Forms.TextBox()
        Me.lvJTtkData = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtJTtkHrsLogged = New System.Windows.Forms.TextBox()
        Me.txtJTtkAmount = New System.Windows.Forms.TextBox()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.chkboxJTtkOTRate = New System.Windows.Forms.CheckBox()
        Me.txtErrMsg = New System.Windows.Forms.TextBox()
        Me.btnJTtkRecord = New System.Windows.Forms.Button()
        Me.radiobtnJTtkStanRate = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mtxtJTtkWEDate = New System.Windows.Forms.MaskedTextBox()
        Me.btnJTtkModWkF = New System.Windows.Forms.Button()
        Me.btnJTtkModWkB = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 16)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 17)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Employee ID"
        '
        'txtJTtkEmpID
        '
        Me.txtJTtkEmpID.Location = New System.Drawing.Point(119, 12)
        Me.txtJTtkEmpID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkEmpID.Name = "txtJTtkEmpID"
        Me.txtJTtkEmpID.ReadOnly = True
        Me.txtJTtkEmpID.Size = New System.Drawing.Size(84, 22)
        Me.txtJTtkEmpID.TabIndex = 26
        Me.txtJTtkEmpID.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(225, 16)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 17)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Name"
        '
        'txtJTtkEmpName
        '
        Me.txtJTtkEmpName.Location = New System.Drawing.Point(280, 12)
        Me.txtJTtkEmpName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkEmpName.Name = "txtJTtkEmpName"
        Me.txtJTtkEmpName.ReadOnly = True
        Me.txtJTtkEmpName.Size = New System.Drawing.Size(215, 22)
        Me.txtJTtkEmpName.TabIndex = 28
        Me.txtJTtkEmpName.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(631, 16)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 17)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "W/E Date"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(117, 75)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Customer"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(28, 75)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 17)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Date"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(440, 75)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 17)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Activity"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(615, 75)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 17)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Hours"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Label8.Location = New System.Drawing.Point(712, 75)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 17)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Labor Rate"
        '
        'txtJTtkDate
        '
        Me.txtJTtkDate.Location = New System.Drawing.Point(7, 97)
        Me.txtJTtkDate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkDate.Name = "txtJTtkDate"
        Me.txtJTtkDate.Size = New System.Drawing.Size(84, 22)
        Me.txtJTtkDate.TabIndex = 0
        '
        'txtJTtkJobID
        '
        Me.txtJTtkJobID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList
        Me.txtJTtkJobID.Location = New System.Drawing.Point(105, 97)
        Me.txtJTtkJobID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkJobID.Name = "txtJTtkJobID"
        Me.txtJTtkJobID.Size = New System.Drawing.Size(103, 22)
        Me.txtJTtkJobID.TabIndex = 2
        '
        'txtJTtkActivity
        '
        Me.txtJTtkActivity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList
        Me.txtJTtkActivity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJTtkActivity.Location = New System.Drawing.Point(331, 97)
        Me.txtJTtkActivity.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkActivity.Name = "txtJTtkActivity"
        Me.txtJTtkActivity.Size = New System.Drawing.Size(247, 22)
        Me.txtJTtkActivity.TabIndex = 4
        '
        'txtJTtkHours
        '
        Me.txtJTtkHours.Location = New System.Drawing.Point(587, 97)
        Me.txtJTtkHours.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkHours.Name = "txtJTtkHours"
        Me.txtJTtkHours.Size = New System.Drawing.Size(79, 22)
        Me.txtJTtkHours.TabIndex = 5
        Me.txtJTtkHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radiobtnJTtkShopRate
        '
        Me.radiobtnJTtkShopRate.AutoSize = True
        Me.radiobtnJTtkShopRate.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.radiobtnJTtkShopRate.Location = New System.Drawing.Point(81, 16)
        Me.radiobtnJTtkShopRate.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTtkShopRate.Name = "radiobtnJTtkShopRate"
        Me.radiobtnJTtkShopRate.Size = New System.Drawing.Size(62, 21)
        Me.radiobtnJTtkShopRate.TabIndex = 16
        Me.radiobtnJTtkShopRate.TabStop = True
        Me.radiobtnJTtkShopRate.Text = "Shop"
        Me.radiobtnJTtkShopRate.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(257, 75)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(31, 17)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Job"
        '
        'txtJTtkSubJob
        '
        Me.txtJTtkSubJob.Location = New System.Drawing.Point(216, 97)
        Me.txtJTtkSubJob.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkSubJob.Name = "txtJTtkSubJob"
        Me.txtJTtkSubJob.Size = New System.Drawing.Size(106, 22)
        Me.txtJTtkSubJob.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(507, 16)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 17)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "LabCat"
        '
        'txtJTtkLabCode
        '
        Me.txtJTtkLabCode.Location = New System.Drawing.Point(572, 12)
        Me.txtJTtkLabCode.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkLabCode.Name = "txtJTtkLabCode"
        Me.txtJTtkLabCode.ReadOnly = True
        Me.txtJTtkLabCode.Size = New System.Drawing.Size(49, 22)
        Me.txtJTtkLabCode.TabIndex = 30
        Me.txtJTtkLabCode.TabStop = False
        '
        'lvJTtkData
        '
        Me.lvJTtkData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader7, Me.ColumnHeader10})
        Me.lvJTtkData.FullRowSelect = True
        Me.lvJTtkData.Location = New System.Drawing.Point(7, 148)
        Me.lvJTtkData.Margin = New System.Windows.Forms.Padding(4)
        Me.lvJTtkData.Name = "lvJTtkData"
        Me.lvJTtkData.Size = New System.Drawing.Size(855, 356)
        Me.lvJTtkData.TabIndex = 21
        Me.lvJTtkData.UseCompatibleStateImageBehavior = False
        Me.lvJTtkData.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Date"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Customer"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Job"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Activity"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader4.Width = 100
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Hours"
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader5.Width = 50
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Rate"
        Me.ColumnHeader6.Width = 50
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "RateCat"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "OT"
        Me.ColumnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader9.Width = 30
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Ext'd $$$"
        Me.ColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Inv. Date"
        Me.ColumnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader10.Width = 80
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(895, 148)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(98, 17)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Hours Logged"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(884, 196)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 17)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Amount to be billed"
        '
        'txtJTtkHrsLogged
        '
        Me.txtJTtkHrsLogged.Location = New System.Drawing.Point(899, 167)
        Me.txtJTtkHrsLogged.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkHrsLogged.Name = "txtJTtkHrsLogged"
        Me.txtJTtkHrsLogged.ReadOnly = True
        Me.txtJTtkHrsLogged.Size = New System.Drawing.Size(93, 22)
        Me.txtJTtkHrsLogged.TabIndex = 23
        Me.txtJTtkHrsLogged.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtJTtkAmount
        '
        Me.txtJTtkAmount.Location = New System.Drawing.Point(895, 215)
        Me.txtJTtkAmount.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJTtkAmount.Name = "txtJTtkAmount"
        Me.txtJTtkAmount.ReadOnly = True
        Me.txtJTtkAmount.Size = New System.Drawing.Size(104, 22)
        Me.txtJTtkAmount.TabIndex = 25
        Me.txtJTtkAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(891, 404)
        Me.btnUpdate.Margin = New System.Windows.Forms.Padding(4)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(113, 39)
        Me.btnUpdate.TabIndex = 26
        Me.btnUpdate.Text = "Update/Exit"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(891, 463)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(113, 42)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel/Exit"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(815, 75)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(73, 17)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "O/T Rates"
        '
        'chkboxJTtkOTRate
        '
        Me.chkboxJTtkOTRate.AutoSize = True
        Me.chkboxJTtkOTRate.Location = New System.Drawing.Point(844, 102)
        Me.chkboxJTtkOTRate.Margin = New System.Windows.Forms.Padding(4)
        Me.chkboxJTtkOTRate.Name = "chkboxJTtkOTRate"
        Me.chkboxJTtkOTRate.Size = New System.Drawing.Size(18, 17)
        Me.chkboxJTtkOTRate.TabIndex = 19
        Me.chkboxJTtkOTRate.UseVisualStyleBackColor = True
        '
        'txtErrMsg
        '
        Me.txtErrMsg.Location = New System.Drawing.Point(871, 261)
        Me.txtErrMsg.Margin = New System.Windows.Forms.Padding(4)
        Me.txtErrMsg.Multiline = True
        Me.txtErrMsg.Name = "txtErrMsg"
        Me.txtErrMsg.ReadOnly = True
        Me.txtErrMsg.Size = New System.Drawing.Size(147, 122)
        Me.txtErrMsg.TabIndex = 31
        '
        'btnJTtkRecord
        '
        Me.btnJTtkRecord.Location = New System.Drawing.Point(896, 94)
        Me.btnJTtkRecord.Margin = New System.Windows.Forms.Padding(4)
        Me.btnJTtkRecord.Name = "btnJTtkRecord"
        Me.btnJTtkRecord.Size = New System.Drawing.Size(51, 28)
        Me.btnJTtkRecord.TabIndex = 6
        Me.btnJTtkRecord.Text = "Rcrd"
        Me.btnJTtkRecord.UseVisualStyleBackColor = True
        '
        'radiobtnJTtkStanRate
        '
        Me.radiobtnJTtkStanRate.AutoSize = True
        Me.radiobtnJTtkStanRate.Location = New System.Drawing.Point(8, 16)
        Me.radiobtnJTtkStanRate.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnJTtkStanRate.Name = "radiobtnJTtkStanRate"
        Me.radiobtnJTtkStanRate.Size = New System.Drawing.Size(58, 21)
        Me.radiobtnJTtkStanRate.TabIndex = 0
        Me.radiobtnJTtkStanRate.TabStop = True
        Me.radiobtnJTtkStanRate.Text = "Stan"
        Me.radiobtnJTtkStanRate.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.radiobtnJTtkStanRate)
        Me.GroupBox1.Controls.Add(Me.radiobtnJTtkShopRate)
        Me.GroupBox1.Location = New System.Drawing.Point(675, 82)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(156, 48)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.SystemColors.ActiveBorder
        Me.TextBox1.Location = New System.Drawing.Point(7, 513)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(1011, 82)
        Me.TextBox1.TabIndex = 32
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'mtxtJTtkWEDate
        '
        Me.mtxtJTtkWEDate.CausesValidation = False
        Me.mtxtJTtkWEDate.Location = New System.Drawing.Point(726, 12)
        Me.mtxtJTtkWEDate.Mask = "00/00/0000"
        Me.mtxtJTtkWEDate.Name = "mtxtJTtkWEDate"
        Me.mtxtJTtkWEDate.Size = New System.Drawing.Size(92, 22)
        Me.mtxtJTtkWEDate.TabIndex = 34
        '
        'btnJTtkModWkF
        '
        Me.btnJTtkModWkF.Location = New System.Drawing.Point(853, 12)
        Me.btnJTtkModWkF.Name = "btnJTtkModWkF"
        Me.btnJTtkModWkF.Size = New System.Drawing.Size(19, 23)
        Me.btnJTtkModWkF.TabIndex = 37
        Me.btnJTtkModWkF.Text = "F"
        Me.btnJTtkModWkF.UseVisualStyleBackColor = True
        '
        'btnJTtkModWkB
        '
        Me.btnJTtkModWkB.Location = New System.Drawing.Point(835, 12)
        Me.btnJTtkModWkB.Name = "btnJTtkModWkB"
        Me.btnJTtkModWkB.Size = New System.Drawing.Size(18, 23)
        Me.btnJTtkModWkB.TabIndex = 36
        Me.btnJTtkModWkB.Text = "B"
        Me.btnJTtkModWkB.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(879, 15)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(128, 17)
        Me.Label14.TabIndex = 38
        Me.Label14.Text = "<--- Chg. W/E Date"
        '
        'JTtk
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1038, 612)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.btnJTtkModWkF)
        Me.Controls.Add(Me.btnJTtkModWkB)
        Me.Controls.Add(Me.mtxtJTtkWEDate)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.btnJTtkRecord)
        Me.Controls.Add(Me.txtErrMsg)
        Me.Controls.Add(Me.chkboxJTtkOTRate)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.txtJTtkAmount)
        Me.Controls.Add(Me.txtJTtkHrsLogged)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtJTtkLabCode)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtJTtkSubJob)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtJTtkHours)
        Me.Controls.Add(Me.txtJTtkActivity)
        Me.Controls.Add(Me.txtJTtkJobID)
        Me.Controls.Add(Me.txtJTtkDate)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtJTtkEmpName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtJTtkEmpID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lvJTtkData)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JTtk"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Timekeeping"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtJTtkEmpID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTtkEmpName As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtJTtkDate As TextBox
    Friend WithEvents txtJTtkJobID As TextBox
    Friend WithEvents txtJTtkActivity As TextBox
    Friend WithEvents txtJTtkHours As TextBox
    Friend WithEvents radiobtnJTtkShopRate As RadioButton
    Friend WithEvents Label9 As Label
    Friend WithEvents txtJTtkSubJob As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtJTtkLabCode As TextBox
    Friend WithEvents lvJTtkData As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtJTtkHrsLogged As TextBox
    Friend WithEvents txtJTtkAmount As TextBox
    Friend WithEvents btnUpdate As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents chkboxJTtkOTRate As CheckBox
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents txtErrMsg As TextBox
    Friend WithEvents btnJTtkRecord As Button
    Friend WithEvents radiobtnJTtkStanRate As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents mtxtJTtkWEDate As MaskedTextBox
    Friend WithEvents ColumnHeader10 As ColumnHeader
    Friend WithEvents btnJTtkModWkF As Button
    Friend WithEvents btnJTtkModWkB As Button
    Friend WithEvents Label14 As Label
End Class
