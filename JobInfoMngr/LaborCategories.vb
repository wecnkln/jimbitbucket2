﻿Imports System.IO
'
Public Class LaborCategories
    Public Structure JTlaborcats
        Dim JTlcKey As String
        Dim JTlcName As String
        Dim JTlcStanRate As String
        Dim JTlcShopRate As String
    End Structure
    '
    '   Public JTlcArray(25) As JTlaborcats
    Public JTlcArray As New SortedList
    Public JTlcChange As String = "N"
    Dim saveSubScript As Integer
    Dim saveLCKey As String

    '
    Public aryCounter As Integer = 0
    '
    Public Sub LCinit()
        ' ----------------------------------------------------------
        ' This SUB prepares LaborCategories panel for presentation.
        ' ----------------------------------------------------------
        '
        ' Logic to make sure ListView is empty.
        '
        StanLabCatListView.Items.Clear()
        '
        ' Logic to fill listview from value in Array. Array values
        ' were initally loaded from file.
        '
        Dim key As ICollection = JTlcArray.Keys
        Dim k As String = Nothing
        For Each k In key
            '
            Dim itmBuild(4) As String
            Dim lvLine As ListViewItem
            '
            itmBuild(0) = JTlcArray(k).JTlcKey
            itmBuild(1) = JTlcArray(k).JTlcName
            itmBuild(2) = JTlcArray(k).JTlcStanRate
            itmBuild(3) = JTlcArray(k).JTlcShopRate
            lvLine = New ListViewItem(itmBuild)
            StanLabCatListView.Items.Add(lvLine)
            '
        Next
        '
        ' clear Panel entry fields.
        '
        txtLabCatKey.Text = ""
        txtLabCatDesc.Text = ""
        txtStanRate.Text = ""
        txtShopRate.Text = ""
        '
        Me.Show()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Hide()
        MsgBox("Labor Category maintenance cancelled." & vbCrLf &
               "All changes made in this session will be rolled back.")
        '
        ' ------------------------------------------------------------
        ' Add code to reestablish LCArray with original file values. '
        ' ------------------------------------------------------------
        '
        Dim JTMM As New JTMainMenu
        JTMM.JTMMinit()
    End Sub
    Public Sub JTlcArrayFill()
        ' -----------------------------------------------
        ' This SUB initial the array and read the file to
        ' fill the array with existing values.
        ' -----------------------------------------------
        '
        ' clear array
        '
        JTlcArray.Clear()
        '
        '
        ' load array with values from JTlcLabCat.dat
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTLabCat.dat"
        '
        Try
            JTlcArray.Clear()
            '  Dim tmpItemData As JTlaborcats
            Dim tmpArrayItem As JTlaborcats = Nothing
            Dim line As String
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                line = readFile.ReadLine()
                If line Is Nothing Then
                    Exit While
                Else

                    Dim tmpReadFileKey As String
                    Dim tmpReadFileData As String

                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    Dim tmpLocKey As Integer = 0
                    Dim tmpLoc As Integer = 1

                    Do While line.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    tmpReadFileKey = line.Substring(1, tmpLoc - 1)
                    If tmpReadFileKey = "LCRecord" Or tmpReadFileKey = "/LCRecord" Then
                        tmpReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While line.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        tmpReadFileData = line.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If

                    Select Case tmpReadFileKey
                    '                     '
                        Case "LCKey"
                            tmpArrayItem.JTlcKey = tmpReadFileData
                        Case "LCTitle"
                            tmpArrayItem.JTlcName = tmpReadFileData
                        Case "LCRate1"
                            tmpArrayItem.JTlcStanRate = tmpReadFileData
                        Case "LCRate2"
                            tmpArrayItem.JTlcShopRate = tmpReadFileData
                        Case "/LCRecord"
                            JTlcArray.Add(tmpArrayItem.JTlcKey, tmpArrayItem)
                            tmpArrayItem = Nothing
                    End Select
                End If
            End While
            readFile.Close()
            readFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub StanLabCatListView_DoubleClick(sender As Object, e As EventArgs) Handles StanLabCatListView.DoubleClick
        ' listview line index ... first data item in listview is item zero
        Dim tmpLCKey As String = StanLabCatListView.SelectedItems(0).Text
        '
        txtLabCatKey.Text = JTlcArray(tmpLCKey).JTlcKey
        txtLabCatDesc.Text = JTlcArray(tmpLCKey).JTlcName
        txtStanRate.Text = JTlcArray(tmpLCKey).JTlcStanRate
        txtShopRate.Text = JTlcArray(tmpLCKey).JTlcShopRate
        txtErrorMsg.Text = ""
        '
        ' save constants for later processing
        '
        saveLCKey = JTlcArray(tmpLCKey).JTlcKey
        saveSubScript = aryCounter
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ' -----------------------------------------------------------
        ' Logic to add a new Job Category
        ' -----------------------------------------------------------
        Dim tmpErrorFlag As String = "N"
        txtErrorMsg.Text = ""
        If txtShopRate.Text = "" Then
            txtErrorMsg.Text = "Shop rate not entered."
            tmpErrorFlag = "Y"
        End If
        If IsNumeric(txtShopRate.Text) = False Then
            txtErrorMsg.Text = "Shop rate is not numeric."
            tmpErrorFlag = "Y"
        End If

        If txtStanRate.Text = "" Then
            txtErrorMsg.Text = "Standard rate is not entered."
            tmpErrorFlag = "Y"
        End If
        If IsNumeric(txtStanRate.Text) = False Then
            txtErrorMsg.Text = "Standard rate is not numeric."
            tmpErrorFlag = "Y"
        End If
        If txtLabCatDesc.Text = "" Then
            txtErrorMsg.Text = "No Job Category Description entered."
            tmpErrorFlag = "Y"
        End If
        If txtLabCatKey.Text = "" Then
            txtErrorMsg.Text = "No Job Category Key entered."
            tmpErrorFlag = "Y"
        End If
        Dim funcPos As Integer
        Dim funcKey As String = txtLabCatKey.Text
        Dim funcName As String = ""
        Dim funcStanRate As String = ""
        Dim funcShopRate As String = ""


        If JTlcFindFunc(funcPos, funcKey, funcName, funcStanRate, funcShopRate) = "DATA" Then
            txtErrorMsg.Text = "Labor Category already exists. Record not added."
            tmpErrorFlag = "Y"
        End If

        If tmpErrorFlag = "Y" Then
            Me.Hide()
            Me.Show()
            Exit Sub
        End If
        '
        ' Edit complete. Following logic will insert the new item into array.
        '
        Dim tmpArrayItem As JTlaborcats
        tmpArrayItem.JTlcKey = txtLabCatKey.Text
        tmpArrayItem.JTlcName = txtLabCatDesc.Text
        tmpArrayItem.JTlcStanRate = txtStanRate.Text
        tmpArrayItem.JTlcShopRate = txtShopRate.Text

        JTlcArray.Add(tmpArrayItem.JTlcKey, tmpArrayItem)
        tmpArrayItem = Nothing
        '
        ' Redisplay panel with updated Listview
        '
        JTlcFlush()
        LCinit()

        '
    End Sub
    Private Sub txtLabCatKey_LostFocus(sender As Object, e As EventArgs) Handles txtLabCatKey.LostFocus
        txtLabCatKey.Text = txtLabCatKey.Text.ToUpper()
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        ' ---------------------------------------
        ' Code to delete labor category
        ' ---------------------------------------

        txtErrorMsg.Text = ""
        If StrComp(saveLCKey, txtLabCatKey.Text) <> 0 Then
            txtErrorMsg.Text = "Labor Code change not allowed on Delete. No record deleted."
            Exit Sub
        End If
        '
        ' check to see if any employees are assigned to the Cat being deleted.
        '
        Dim tmpJTlcSub As Integer = 0
        Dim tmpJTlcID As String = ""
        Dim tmpJTlcEmpName As String = ""
        Dim tmpJTlcLabCat As String = ""
        Dim tmpJTlcPayMeth As String = ""
        Dim tmpJTlcTermDate As String = ""
        Do While txtLabCatKey.Text <> tmpJTlcLabCat
            If JThr.JThrReadFunc(tmpJTlcSub, tmpJTlcID, tmpJTlcEmpName, tmpJTlcLabCat, tmpJTlcPayMeth, tmpJTlcTermDate) <> "Data" Then
                Exit Do
            End If
            If txtLabCatKey.Text = tmpJTlcLabCat Then
                txtErrorMsg.Text = "Employees assigned to this Labor Category. Can't delete."
                Exit Sub
            End If
            tmpJTlcSub += 1
        Loop
        If JTlcArray.ContainsKey(txtLabCatKey.Text) = True Then
            JTlcArray.Remove(txtLabCatKey.Text)
        End If
        '    
        JTlcFlush()
        JTlcChange = "Y"
        LCinit()
    End Sub
    Private Sub btnChange_Click(sender As Object, e As EventArgs) Handles btnChange.Click
        ' -------------------------------------------
        ' Logic to make changes to a Labor Categpry.
        ' -------------------------------------------
        Dim tmpErrorFlag As String = "N"
        txtErrorMsg.Text = ""
        If txtShopRate.Text = "" Then
            txtErrorMsg.Text = "Shop rate not entered."
            tmpErrorFlag = "Y"
        End If
        If IsNumeric(txtShopRate.Text) = False Then
            txtErrorMsg.Text = "Shop rate is not numeric."
            tmpErrorFlag = "Y"
        End If

        If txtStanRate.Text = "" Then
            txtErrorMsg.Text = "Standard rate is not entered."
            tmpErrorFlag = "Y"
        End If
        If IsNumeric(txtStanRate.Text) = False Then
            txtErrorMsg.Text = "Standard rate is not numeric."
            tmpErrorFlag = "Y"
        End If
        If txtLabCatDesc.Text = "" Then
            txtErrorMsg.Text = "No Job Category Description entered."
            tmpErrorFlag = "Y"
        End If
        If StrComp(txtLabCatKey.Text, saveLCKey) <> 0 Then
            txtErrorMsg.Text = "Category Key cannot be changed. Delete and Re-add if neessary."
            tmpErrorFlag = "Y"
        End If
        If tmpErrorFlag = "Y" Then
            Me.Hide()
            Me.Show()
            Exit Sub
        End If
        Dim tmpArrayItem As JTlaborcats
        tmpArrayItem.JTlcKey = txtLabCatKey.Text
        tmpArrayItem.JTlcName = txtLabCatDesc.Text
        tmpArrayItem.JTlcStanRate = txtStanRate.Text
        tmpArrayItem.JTlcShopRate = txtShopRate.Text
        '
        If JTlcArray.ContainsKey(tmpArrayItem.JTlcKey) = True Then
            JTlcArray.Remove(tmpArrayItem.JTlcKey)
        End If
        JTlcArray.Add(tmpArrayItem.JTlcKey, tmpArrayItem)
        '
        JTlcFlush()
        LCinit()
    End Sub
    Private Sub btnDone_Click(sender As Object, e As EventArgs) Handles btnDone.Click
        ' ------------------------------------------------------------
        ' Code to complete Labor Category maintenance. If changes have
        ' been made, a new file is written to save the changes.
        ' ------------------------------------------------------------
        If JTlcChange <> "Y" Then
            Me.Hide()

            JTMainMenu.JTMMinit()
            Exit Sub
        End If
        aryCounter = 0
        JTlcFlush()
        Me.Hide()
        '
        JTMainMenu.JTMMinit()
    End Sub
    Private Function JTlcFlush() As String
        Dim key As ICollection = JTlcArray.Keys
        Dim k As String = Nothing
        '
        Dim fileRecord As String = Nothing
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTLabCat.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            For Each k In key
                fileRecord = "<LCRecord>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<LCKey>" & JTlcArray(k).JTlcKey & "</LCKey>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<LCTitle>" & JTlcArray(k).JTlcName & "</LCTitle>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<LCRate1>" & JTlcArray(k).JTlcStanRate & "</LCRate1>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<LCRate2>" & JTlcArray(k).JTlcShopRate & "</LCRate2>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "</LCRecord>"
                writeFile.WriteLine(fileRecord)
                '
            Next

            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        Return ""
    End Function

    ' ==========================================================
    ' Functions
    ' ==========================================================
    Function JTlcFindFunc(ByVal aryPos As Integer, ByRef lcrKey As String, ByRef lcrName As String, ByRef lcrStanRate As String, ByRef lcrShopRate As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values and check for a keys existance. Row is
        ' selected by 'lcrKey' variable set by calling SUB. The entire Array will be searched.
        ' If the value is found, the position and the remaining field values are returned.
        ' If the key does not exist, the function returns a 'INVALID' value.
        ' -----------------------------------------------------------------------------
        '
        If lcrKey = "" Then
            Return "INVALID"
        End If
        If JTlcArray.ContainsKey(lcrKey) = False Then
            Return "INVALID"
        End If

        lcrName = JTlcArray(lcrKey).JTlcName
        lcrStanRate = JTlcArray(lcrKey).JTlcStanRate
        lcrShopRate = JTlcArray(lcrKey).JTlcShopRate
        Return "DATA"

    End Function
    Function JTlcReadFunc(ByVal aryPos As Integer, ByRef lcrKey As String, ByRef lcrName As String, ByRef lcrStanRate As String, ByRef lcrShopRate As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values from single row of array JTlcArray. Row to be
        ' selected by 'aryPOS' variable set by calling SUB. Limits must be set to
        ' make sure arypos value is within the array scope.
        ' -----------------------------------------------------------------------------
        '
        Dim key As ICollection = JTlcArray.Keys
        If aryPos >= key.Count Then
            Return "EMPTY"
            Exit Function
        End If
        '
        lcrKey = JTlcArray(key(aryPos)).JTlcKey
        lcrName = JTlcArray(key(aryPos)).JTlcName
        lcrStanRate = JTlcArray(key(aryPos)).JTlcStanRate
        lcrShopRate = JTlcArray(key(aryPos)).JTlcShopRate
        Return "DATA"
    End Function
    '



End Class