﻿Imports System.Diagnostics
Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
'
'
''' <summary>
''' This class handles reconciliation for all NLC item paid by statement. It produces 
''' a report and updates the result in the NLC file. Work is still needed to complete
''' the interface to the acccounting system. 
''' </summary>
Public Class JTrec
    Public JTrecSuspendedFlag As Boolean = False
    Public JTrecSuspndDate As String = Nothing
    Public JTrecSuspndInvNum As String = Nothing
    Public JtrecSuspndAmount As String = Nothing
    '
    Dim JtrecItemCorrectionJTnlcKey As String = Nothing    'place to save key for line item correction call
    '
    Dim slJTrec As New SortedList
    Dim slJTrecKey As New SortedList
    '
    Dim JTrecEmptyInvoiceDate As String
    '
    Dim JTrecDiscountAmt As Decimal       ' used to hold calculated (or adjusted) discount amount.
    '

    '
    Public Sub JTrecInit()
        Dim tmpSeqNum As Integer = 0
        ' test code to make sure field is null-empty
        txtJTrecDiff.Text = ""
        ' clear all items in lvJTrecNJRInvList and lvJTrecAcctTls.
        lvJTrecNJRInvList.Items.Clear()
        lvJTrecAcctTls.Items.Clear()
        ' --------------------------------------------------------------
        ' Call to load vi Statement account names for AUTOCOMPLETE mode.
        ' --------------------------------------------------------------
        JTrecFillShortNameAutoTable()
        JTrecNJAccountAutoTable()
        lvJTrecAcctTls.Visible = True
        ' -----------------------------------------------------------
        ' call to set up autocomplete table for NJR entries in rec
        ' -----------------------------------------------------------
        JTrecBldVndAutoComplete()
        ' -----------------------------------------------
        ' Load constant with empty call values
        ' -----------------------------------------------
        JTrecEmptyInvoiceDate = mtxtJTrecStmtDate.Text
        '
        Dim tmpSLkey As String = "FIRST"
        Dim tmpslnlcitem As JTnlc.JTnlcStructure = Nothing
        '
        txtJTrecRecName.Text = JTVarMaint.JTGVrecName
        txtJTrecVndShrtName.Text = JTVarMaint.JTGVrecVndShrtName
        txtJTrecStmtInvNumb.Text = JTVarMaint.JTGVrecVndInvNumber
        mtxtJTrecStmtDate.Text = JTVarMaint.JTGVrecVndInvDate
        mtxtJTrecDueDate.Text = JTVarMaint.JTGVrecVndInvDueDate

        txtJTrecStatBal.Text = String.Format("{0:0.00}", Val(JTVarMaint.JTGVrecStmtAmt))
        txtOptMsg.Text = JTVarMaint.JTGVrecOptMsg
        '
        slJTrec.Clear()
        slJTrecKey.Clear()
        '
        Do While JTnlc.JTNLCrecExt(tmpSLkey, tmpslnlcitem) = "DATA"
            Dim tmpDate As Date = CDate(tmpslnlcitem.InvDate)
            Dim tmpJulian As String
            tmpJulian = Format$(tmpDate, "yyyy") & tmpDate.DayOfYear.ToString.PadLeft(3, "0")
            If tmpslnlcitem.ReconcileDate = "" Or
               tmpslnlcitem.ReconcileDate = "PENDING" Then
                If tmpslnlcitem.MorS <> "ADJ" Then
                    If tmpslnlcitem.viRcrdEntrySrce = "" Then    'normal NLC item ... if valued, relates to NJR entries
                        '              If tmpslnlcitem.MorS <> "NJR" Then
                        Dim tmpslRECKey As String = Nothing
                            If rbtnJTrecVJD.Checked = True Then
                                tmpslRECKey = tmpslnlcitem.Supplier & tmpslnlcitem.JobID & tmpJulian &
                        tmpslnlcitem.SupInvNum & Str(tmpSeqNum)
                            End If
                            If rbtnJTrecVJA.Checked = True Then
                                tmpslRECKey = tmpslnlcitem.Supplier & tmpslnlcitem.JobID &
                            tmpslnlcitem.TBInvd & Str(tmpSeqNum)
                            End If
                            If rbtnJTrecVDJ.Checked = True Then
                                tmpslRECKey = tmpslnlcitem.Supplier & tmpJulian &
                            tmpslnlcitem.SupInvNum & tmpslnlcitem.JobID & Str(tmpSeqNum)
                            End If
                            If rbtnJTrecDVJ.Checked = True Then
                                tmpslRECKey = tmpJulian & tmpslnlcitem.Supplier & tmpslnlcitem.JobID &
                        tmpslnlcitem.SupInvNum & Str(tmpSeqNum)
                            End If


                            Do While slJTrec.ContainsKey(tmpslRECKey) = True
                                tmpSeqNum += 1

                                If rbtnJTrecVJD.Checked = True Then
                                    tmpslRECKey = tmpslnlcitem.Supplier & tmpslnlcitem.JobID & tmpJulian &
                                tmpslnlcitem.SupInvNum & Str(tmpSeqNum)
                                End If
                                If rbtnJTrecVJA.Checked = True Then
                                    tmpslRECKey = tmpslnlcitem.Supplier & tmpslnlcitem.JobID &
                                tmpslnlcitem.TBInvd & Str(tmpSeqNum)
                                End If
                                If rbtnJTrecVDJ.Checked = True Then
                                    tmpslRECKey = tmpslnlcitem.JobID & tmpslnlcitem.Supplier & tmpJulian &
                                tmpslnlcitem.SupInvNum & Str(tmpSeqNum)
                                End If
                                If rbtnJTrecDVJ.Checked = True Then
                                    tmpslRECKey = tmpJulian & tmpslnlcitem.Supplier & tmpslnlcitem.JobID &
                                tmpslnlcitem.SupInvNum & Str(tmpSeqNum)
                                End If
                            Loop
                            '
                            slJTrec.Add(tmpslRECKey, tmpslnlcitem)
                            slJTrecKey.Add(tmpslRECKey, tmpSLkey)
                            '
                            txtJTrecSelTotal.Text = ""
                            txtJTrecDiff.Text = ""
                        Else
                            ' items from nlc file that are NJR.
                            Dim itmbuild2(8) As String
                            Dim lvline2 As ListViewItem
                            itmbuild2(0) = tmpslnlcitem.InvDate
                            itmbuild2(1) = tmpslnlcitem.NJREAcct
                            itmbuild2(2) = tmpslnlcitem.Supplier
                            itmbuild2(3) = tmpslnlcitem.SupInvNum
                            itmbuild2(4) = tmpslnlcitem.Descrpt
                            itmbuild2(5) = tmpslnlcitem.SupCost
                            itmbuild2(6) = tmpslnlcitem.viRcrdEntrySrce
                            itmbuild2(7) = tmpSLkey
                            '



                            lvline2 = New ListViewItem(itmbuild2)
                            lvJTrecNJRInvList.Items.Add(lvline2)
                            '
                            If tmpslnlcitem.ReconcileDate = "PENDING" Then
                                lvJTrecNJRInvList.Items(lvJTrecNJRInvList.Items.Count - 1).Checked = True
                            End If
                            '
                        End If
                    End If
                End If
        Loop
        '
        lvJTrecData.Items.Clear()
        '
        Dim itmBuild(9) As String
        Dim lvLine As ListViewItem
        Dim Keys As ICollection = slJTrec.Keys
        Dim k As String
        '
        For Each k In Keys
            itmBuild(0) = slJTrec(k).InvDate
            itmBuild(1) = slJTrec(k).Supplier
            itmBuild(2) = slJTrec(k).Descrpt
            itmBuild(3) = slJTrec(k).SupInvNum
            itmBuild(4) = String.Format("{0:0.00}", Val(slJTrec(k).SupCost))
            itmBuild(5) = slJTrec(k).MorS
            itmBuild(6) = slJTrec(k).JobID
            itmBuild(7) = slJTrec(k).SubID
            itmBuild(8) = slJTrec(k).CustInvDate

            lvLine = New ListViewItem(itmBuild)
            lvJTrecData.Items.Add(lvLine)



        Next

        ' -------------------------------------------------------------------------
        ' code to check items with a "PENDING" status.
        ' -------------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Keys = slJTrec.Keys
        Do While tmpSub < Keys.Count
            Dim tmpKey As String = Keys(tmpSub)
            If slJTrec(tmpKey).ReconcileDate = "PENDING" Then
                lvJTrecData.Items(tmpSub).Checked = True
            End If
            tmpSub += 1
        Loop
        '
        JTrecBldAutoCompleteNJRAccounts()          ' build auto complete table for NJR accounts.

        '
        mtxtJTrecDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        txtOptMsg.Text = ""
        btnExit.Text = "Exit"
        mtxtJTrecNJDate.Text = ""
        txtJTrecNJAccount.Text = ""
        txtJTrecNJVendor.Text = ""
        txtJTrecNJInvNumber.Text = ""
        txtJTrecNJDesc.Text = ""
        txtJTrecNJAmount.Text = ""

        txtJTrecVndShrtName.Select()
        'turn picture off on JTStart so that it doesn't overlay the JTrec panel.
        JTstart.JTstPictureBox1("HIDE")
        Me.Show()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Hide()
        lvJTrecData.Items.Clear()
        slJTrec.Clear()
        Dim tmpSub As Integer = 0
        Dim keys As ICollection = JTnlc.slJTnlc.Keys
        Dim tmpItem As JTnlc.JTnlcStructure
        Do While tmpSub < JTnlc.slJTnlc.Count
            tmpItem = JTnlc.slJTnlc(keys(tmpSub))
            If tmpItem.ReconcileDate = "PENDING" Then
                Dim tmpKey As String = keys(tmpSub)
                JTnlc.JTnlcDelSLRecord(tmpKey)
                tmpItem.ReconcileDate = ""
                JTnlc.JTnlcAddSLItem(tmpKey, tmpItem, "btnExit_Click - JTrec~210")
            End If
            tmpSub += 1
        Loop
        ' delete NJR items input through the JTrec panel from slJTnls.
        tmpSub = 0
        Do While tmpSub < lvJTrecNJRInvList.Items.Count - 1
            If lvJTrecNJRInvList.Items(tmpSub).SubItems(6).Text = "REC" Then
                JTnlc.JTnlcDelSLRecord(lvJTrecNJRInvList.Items(tmpSub).SubItems(7).Text)
            End If
            tmpSub += 1
        Loop
        '
        ' Force NLC File flush 
        '
        JTnlc.JTnlcArrayFlush("")
        '
        ' Clear JTVM fields being saved for suspended session.
        '
        JTVarMaint.JTGVrecName = Nothing
        JTVarMaint.JTGVrecStmtAmt = Nothing
        JTVarMaint.JTGVrecOptMsg = Nothing
        JTVarMaint.JTGVrecVndShrtName = Nothing
        JTVarMaint.JTGVrecVndInvNumber = Nothing
        JTVarMaint.JTGVrecVndInvDate = Nothing
        JTVarMaint.JTGVrecVndInvDueDate = Nothing
        '
        ' Clear vendor address fields on panel
        '
        txtJTrecAcctNum.Text = Nothing
        txtJTrecVendorName.Text = Nothing
        txtJTrecVendAddrss.Text = Nothing
        txtJTrecVendCityState.Text = Nothing

        txtJTrecStatBal.Text = String.Format("{0:0.00}", Val(JTVarMaint.JTGVrecStmtAmt))
        txtOptMsg.Text = JTVarMaint.JTGVrecOptMsg
        '
        JTMainMenu.JTMMinit()
    End Sub



    Private Sub lvJTrecData_MouseHover(sender As Object, e As EventArgs) Handles lvJTrecData.MouseMove, lvJTrecNJRInvList.MouseMove

        Dim tmpCheckedTotal As Decimal = 0
        Dim tmpSub As Integer = 0
        If lvJTrecData.CheckedItems.Count > 0 Or lvJTrecNJRInvList.CheckedItems.Count > 0 Then
            btnExit.Text = "Cancel Current Reconciation and Exit"
        End If
        'sum checked items for job related nlc items
        Do While tmpSub < lvJTrecData.CheckedItems.Count
            tmpCheckedTotal += Val(lvJTrecData.CheckedItems(tmpSub).SubItems(4).Text)
            tmpSub += 1
        Loop
        txtJTrecSelTotal.Text = String.Format("{0:0.00}", tmpCheckedTotal)
        ' sum checked items for NJR items --- Also rebuild account totals (lvJTrecAcctTls).
        lvJTrecAcctTls.Items.Clear()
        tmpSub = 0
        tmpCheckedTotal = 0
        Do While tmpSub < lvJTrecNJRInvList.CheckedItems.Count
            tmpCheckedTotal += Val(lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(5).Text)
            Dim tmpAcct As String = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(1).Text
            Dim tmpAmount As String = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(5).Text

            JTrecUpdtlvJTrecAcctTls(tmpAcct, tmpAmount)
            tmpSub += 1
        Loop
        txtJTrecNJRSelTotal.Text = String.Format("{0:0.00}", tmpCheckedTotal)
        JTrecUpdateRecTranTotals()
    End Sub

    Public Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        ' -----------------------------------------------------------------------------------------------------
        ' Validate the statement heading information. Make sure it exists and is reasonable.
        ' -----------------------------------------------------------------------------------------------------

        Dim tmpErrorFlag As Boolean = False
        Dim tmpMsgBoxText As String = "Statement Heading Information Audit" & vbCrLf
        If txtJTrecVndShrtName.Text = "" Then
            tmpErrorFlag = True
            tmpMsgBoxText = tmpMsgBoxText & "Error - No Vendor Name has been entered." & vbCrLf
        End If
        If TestForValidDate(mtxtJTrecStmtDate.Text) = False Then
            tmpErrorFlag = True
            tmpMsgBoxText = tmpMsgBoxText & "Error - No Statement Date has been entered." & vbCrLf
        End If
        '
        '
        '
        If txtJTrecStmtInvNumb.Text = "" Then
            tmpErrorFlag = True
            tmpMsgBoxText = tmpMsgBoxText & "Error - No Invoice Number has been entered." & vbCrLf
        Else
            Dim tmpDupKeyChk As String = "0" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text
            If JTnlc.JTDupStmtNumChk(tmpDupKeyChk) = True Then
                tmpErrorFlag = True
                tmpMsgBoxText = tmpMsgBoxText & "Error - Duplicate Statement Invoice Number has been entered." & vbCrLf
            End If
        End If
        '
        If TestForValidDate(mtxtJTrecDueDate.Text) = False Then
            tmpErrorFlag = True
            tmpMsgBoxText = tmpMsgBoxText & "Error - No Payment Due Date exists." & vbCrLf
        End If
        If tmpErrorFlag = True Then
            tmpMsgBoxText = tmpMsgBoxText & vbCrLf & "MAKE CORRECTION(S) AND RESUBMIT."
            txtJTrecVndShrtName.Select()
            MsgBox(tmpMsgBoxText)
            Exit Sub
        End If
        '
        ' -----------------------------------------------------------------------
        ' Check to see if there is a discount percent for the Vendor. If yes,
        ' do the calculation (Statement Total * Discount %. Then present an 
        ' InputBox with the calculated amount to allow for changes.
        ' -----------------------------------------------------------------------

        If Val(txtJTrecVendDisc.Text) <> 0 Then
            Dim tmpDiscPercent As Decimal = Val(txtJTrecVendDisc.Text) / 100
            Dim tmpInvoiceTotal As Decimal = Val(txtJTrecRecStmtTl.Text)
            JTrecDiscountAmt = Val(Math.Round(tmpDiscPercent * tmpInvoiceTotal, 2))
            Dim Message, Title, tmpDiscAmt, MyValue
            MyValue = " "
            Do While IsNumeric(MyValue) = False
                Message = txtJTrecVndShrtName.Text & " allows a " & txtJTrecVendDisc.Text &
                "% discount for prompt payment. The calculated amount is below." &
                " Accept this amount or change it to what you want(must be numeric). (REC.007)"
                Title = "Discount Amount Verification"    ' Set title.
                tmpDiscAmt = String.Format("{0:0.00}", JTrecDiscountAmt)    ' Set default.
                ' Display message, title, and default value.
                MyValue = InputBox(Message, Title, tmpDiscAmt)
            Loop
            JTrecDiscountAmt = Val(MyValue)
        Else
            JTrecDiscountAmt = 0
        End If
        '
        ' --------------------------------------------------------------
        ' Call to print reconciliation report
        ' --------------------------------------------------------------
        JTrecPrtRprt()
        ' 
        ' --------------------------------------------------------------
        ' Update selected items in JTvi sl as reconsuggest
        ' --------------------------------------------------------------
        '
        JTrecUpdtJTviReconSuggest()
        '
        ' ---------------------------------------------------------------------------
        ' Call to update Reconciliation Dates on the items that have been reconciled.
        ' ---------------------------------------------------------------------------
        JTrecUpdtRecDate("")
        '
        ' -----------------------------------
        ' Clean up and exit
        ' -----------------------------------

        Me.Hide()
        '        '
        '
        ' Clear JTvm fields being saved for suspended session.
        '
        JTVarMaint.JTGVrecName = Nothing
        JTVarMaint.JTGVrecStmtAmt = Nothing
        JTVarMaint.JTGVrecOptMsg = Nothing
        JTVarMaint.JTGVrecVndShrtName = Nothing
        JTVarMaint.JTGVrecVndInvNumber = Nothing
        JTVarMaint.JTGVrecVndInvDate = Nothing
        JTVarMaint.JTGVrecVndInvDueDate = Nothing
        '
        ' clear out vendor address fields on JTrec panel
        txtJTrecAcctNum.Text = Nothing
        txtJTrecVendorName.Text = Nothing
        txtJTrecVendAddrss.Text = Nothing
        txtJTrecVendCityState.Text = Nothing
        '
        slJTrec.Clear()
        slJTrecKey.Clear()
        lvJTrecData.Items.Clear()
        '
        JTMainMenu.JTMMinit()
        '
    End Sub
    ''' <summary>
    ''' This function is the main function for printing the A/P Reconciliation Report. Other functions
    ''' are called from this function to build print lines, etc.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTrecPrtRprt() As String
        Dim tmpTodayYear As String = Date.Today.Year
        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "ReconcilePDF\" & tmpTodayYear
        If Not System.IO.Directory.Exists(tmpFilePath) Then
            System.IO.Directory.CreateDirectory(tmpFilePath)
        End If
        Dim filename As String = tmpFilePath & "\" & "RR_" & txtJTrecRecName.Text & ".PDF"
        ' ----------------------------------------------------------------
        ' Generate iTextSharp PDF report and update Reconciliation Date for NLC item.
        ' ----------------------------------------------------------------
        Try
            Using fs As System.IO.FileStream = New FileStream(filename, FileMode.Create)
                '
                Dim myDoc As Document = New Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 72)
                Dim Writer As PdfWriter = PdfWriter.GetInstance(myDoc, fs)
                Dim ev As New JTrecitsEvents
                Writer.PageEvent = ev
                '
                '
                '
                myDoc.AddTitle("JIM Statement Reconciliation Report")
                myDoc.Open()
                '
                '
                JTrecBldRprtHeader(myDoc)      ' Build report headings
                '
                JTrecBldInvoiceRelatedSection(myDoc)      'Build cleared Job Related Invoice table
                '
                JTrecBldInvSummarySection(myDoc)          ' Build table of costs by expense account.
                '
                Dim JIMwhtFooter As New Phrase(FP_H6Blue("Information managed and reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                myDoc.Add(JIMwhtFooter)
                '
                myDoc.Close()
                '
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        JTvPDF.JTvPDFDispPDF(filename)
        '
        TopMost = False
        '
        Return ""
    End Function
    ''' <summary>
    ''' Function builds the report headers for the A/P Reconciliation Report.
    ''' </summary>
    ''' <param name="myDoc"></param>
    ''' <returns></returns>
    Private Function JTrecBldRprtHeader(ByRef myDoc As Document) As String
        ' ----------------------------------------------------------------
        ' Generate PDF Invoice Header section..
        ' ----------------------------------------------------------------

        ' ----------------------------------------------------------
        ' table structure to hold logo and date
        ' ----------------------------------------------------------
        Dim table As New PdfPTable(2)
        ' Actual width of table in points
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {8.5F, 6.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 0.0F
        table.SpacingAfter = 10.0F
        '
        Dim logolocation As String = JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.CompanyLogo
        '
        ' add code to see if logo files exists
        '
        If Not File.Exists(logolocation) Then
            iTSFillCell("-------- No logo file found -------", table, 0, 0, 0, "RED")
        Else
            '
            '
            Dim tmpImage As Image = Image.GetInstance(logolocation)
            tmpImage.ScaleAbsolute(140.0F, 50.0F)
            Dim CellContents = New PdfPCell(tmpImage)
            CellContents.Border = 0
            table.AddCell(CellContents)
        End If
        ' -----------------------------------------------------------------
        ' Add invoice Date.
        ' -----------------------------------------------------------------
        Dim tmpHeadingInfo As String = "A/P STATEMENT RECONCILIATION REPORT" & vbCrLf & vbCrLf &
            "Vendor:            " & txtJTrecVendorName.Text & vbCrLf &
            "Invoice #:         " & txtJTrecStmtInvNumb.Text & vbCrLf &
            "Invoice Date:      " & mtxtJTrecStmtDate.Text & vbCrLf &
            "Payment Due Date:  " & mtxtJTrecDueDate.Text & vbCrLf &
            "Statement Balance: " & txtJTrecStatBal.Text & vbCrLf &
            "Discount Amount:   " & String.Format("{0:0.00}", JTrecDiscountAmt) & vbCrLf & vbCrLf &
            "Report Name:       " & txtJTrecRecName.Text
        '
        iTSFillCell(tmpHeadingInfo, table, 0, 1, 0, "")
        '
        '
        myDoc.Add(table)
        '
        Return ""
    End Function
    Public MatTotal As Double = 0
    Public SerTotal As Double = 0
    Public PRTotal As Double = 0
    ''' <summary>
    ''' This Function prescreen the reconciled job-related invoices and calls another function to build
    ''' actual print lines for each category of invoices that are present.
    ''' </summary>
    ''' <param name="mydoc"></param>
    ''' <returns></returns>
    Private Function JTrecBldInvoiceRelatedSection(ByRef mydoc As Document) As String

        Dim tmpSub As Integer = 0
        Dim tmpSectionTotal As Decimal = 0
        Dim tmpHasRecords As Boolean = False
        If lvJTrecData.CheckedItems.Count > 0 Then
            Do While tmpSub < lvJTrecData.CheckedItems.Count
                If lvJTrecData.CheckedItems(tmpSub).SubItems(5).Text = "M" Then
                    Dim tmpLineAmt As Decimal = Val(lvJTrecData.CheckedItems(tmpSub).SubItems(4).Text)
                    tmpSectionTotal += tmpLineAmt
                    tmpHasRecords = True
                End If
                tmpSub += 1
            Loop
        End If
        MatTotal = tmpSectionTotal
        If tmpHasRecords = True Then
            JTrecBldClearedJRTable(mydoc, "M", tmpSectionTotal)
        End If
        '
        tmpSub = 0
        tmpSectionTotal = 0
        tmpHasRecords = False
        If lvJTrecData.CheckedItems.Count > 0 Then
            Do While tmpSub < lvJTrecData.CheckedItems.Count
                If lvJTrecData.CheckedItems(tmpSub).SubItems(5).Text = "S" Then
                    Dim tmpLineAmt As Decimal = Val(lvJTrecData.CheckedItems(tmpSub).SubItems(4).Text)
                    tmpSectionTotal += tmpLineAmt
                    tmpHasRecords = True
                End If
                tmpSub += 1
            Loop
        End If
        SerTotal = tmpSectionTotal
        If tmpHasRecords = True Then
            JTrecBldClearedJRTable(mydoc, "S", tmpSectionTotal)
        End If
        '
        tmpSub = 0
        tmpSectionTotal = 0
        tmpHasRecords = False
        If lvJTrecData.CheckedItems.Count > 0 Then
            Do While tmpSub < lvJTrecData.CheckedItems.Count
                If lvJTrecData.CheckedItems(tmpSub).SubItems(5).Text = "PR" Then
                    Dim tmpLineAmt As Decimal = Val(lvJTrecData.CheckedItems(tmpSub).SubItems(4).Text)
                    tmpSectionTotal += tmpLineAmt
                    tmpHasRecords = True
                End If
                tmpSub += 1
            Loop
        End If
        PRTotal = tmpSectionTotal
        If tmpHasRecords = True Then
            JTrecBldClearedJRTable(mydoc, "PR", tmpSectionTotal)
        End If
        '
        ' Logic to process NJR items
        tmpSub = 0
        tmpSectionTotal = 0
        tmpHasRecords = False
        If lvJTrecNJRInvList.CheckedItems.Count > 0 Then
            Do While tmpSub < lvJTrecNJRInvList.CheckedItems.Count
                Dim tmpLineAmt As Decimal = Val(lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(5).Text)
                tmpSectionTotal += tmpLineAmt
                tmpHasRecords = True
                tmpSub += 1
            Loop
        End If
        If tmpHasRecords = True Then
            JTrecBldClearedNJRTable(mydoc, tmpSectionTotal)
        End If
        '
        Return ""
    End Function
    ''' <summary>
    ''' This function builds the actual printline for the reconciled job-related invoices. It is called one or 
    ''' multiple times depending on how many categories of job-related invoices have been reconciled. 
    ''' </summary>
    ''' <param name="mydoc">The overall document being built for the report</param>
    ''' <param name="SectionCode">This will be 'M', 'S' or 'PR' depending on what type of invoices are being presented.</param>
    ''' <param name="tmpSectionTotal">This is a total amount being passed through for printing the totals on the
    ''' report.</param>
    ''' <returns></returns>
    Private Function JTrecBldClearedJRTable(ByRef mydoc As Document, SectionCode As String, tmpSectionTotal As Double)
        Dim table As New PdfPTable(7)
        ' Actual width of table in points
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {1.45F, 2.2F, 3.9F, 1.2F, 1.2F, 2.0F, 1.4F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim tmpTableHeader As String = ""
        Dim tmpTotalLineText As String = ""
        Select Case SectionCode
            Case "M"
                tmpTableHeader = "JOB RELATED MATERIAL INVOICES"
                tmpTotalLineText = "Reconciled Material Invoice Total ---------> "
            Case "S"
                tmpTableHeader = "JOB RELATED SERVICES INVOICES"
                tmpTotalLineText = "Reconciled Services Invoice Total ---------> "
            Case "PR"
                tmpTableHeader = "1099 PAYROLL COST INVOICES"
                tmpTotalLineText = "Reconciled 1099 Payroll Invoice Total -----> "
        End Select
        '
        iTSFillCell(tmpTableHeader, table, 1, 8, 2, "YELLOW")
        '
        iTSFillCell("Date", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Source", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Description", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Inv #", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Cost", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Cust./Job", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Inv. Dt", table, 1, 1, 2, "YELLOW")
        '
        Dim tmpSub As Integer = 0
        '
        Do While tmpSub < lvJTrecData.CheckedItems.Count
            If lvJTrecData.CheckedItems(tmpSub).SubItems(5).Text = SectionCode Then
                Dim tmpData As String = lvJTrecData.CheckedItems(tmpSub).Text
                iTSFillCell(tmpData, table, 1, 1, 0, "")
                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(1).Text
                iTSFillCell(tmpData, table, 1, 1, 0, "")
                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(2).Text
                iTSFillCell(tmpData, table, 1, 1, 0, "")
                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(3).Text
                iTSFillCell(tmpData, table, 1, 1, 0, "")
                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(4).Text
                iTSFillCell(tmpData, table, 2, 1, 0, "")
                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(6).Text
                If lvJTrecData.CheckedItems(tmpSub).SubItems(7).Text <> "" Then
                    tmpData = tmpData & "~" & lvJTrecData.CheckedItems(tmpSub).SubItems(7).Text
                End If
                iTSFillCell(tmpData, table, 1, 1, 0, "")

                tmpData = lvJTrecData.CheckedItems(tmpSub).SubItems(8).Text
                iTSFillCell(tmpData, table, 1, 1, 0, "")
            End If
            tmpSub += 1
        Loop
        iTSFillCell(tmpTotalLineText, table, 2, 4, 2, "YELLOW")
        iTSFillCell(String.Format("{0:0.00}", tmpSectionTotal), table, 2, 1, 2, "YELLOW")
        iTSFillCell("", table, 1, 2, 2, "YELLOW")
        '
        mydoc.Add(table)

        Return ""
    End Function
    ''' <summary>
    ''' This function builds the actual printline for the NJR items being reconciled.
    ''' </summary>
    ''' <param name="mydoc"></param>
    ''' <param name="tmpSectionTotal"></param>
    ''' <returns></returns>
    Private Function JTrecBldClearedNJRTable(ByRef mydoc As Document, tmpSectionTotal As Double)
        Dim table As New PdfPTable(6)
        ' Actual width of table in points
        table.TotalWidth = 400.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {1.3F, 2.0F, 1.4F, 2.4F, 1.15F, 1.3F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim tmpTableHeader As String = "NON-JOB-RELATED INVOICES"
        Dim tmpTotalLineText As String = "Reconciled Non-Job-Related Invoice Total ---------> "

        '
        iTSFillCell(tmpTableHeader, table, 1, 6, 2, "YELLOW")
        '
        iTSFillCell("Date", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Account", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Vendor", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Description", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Invoice #", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Cost", table, 1, 1, 2, "YELLOW")

        '
        Dim tmpSub As Integer = 0
        '
        Do While tmpSub < lvJTrecNJRInvList.CheckedItems.Count

            Dim tmpData As String = lvJTrecNJRInvList.CheckedItems(tmpSub).Text
            iTSFillCell(tmpData, table, 1, 1, 0, "")
            tmpData = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(1).Text
            iTSFillCell(tmpData, table, 1, 1, 0, "")
            tmpData = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(2).Text
            iTSFillCell(tmpData, table, 1, 1, 0, "")
            tmpData = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(4).Text
            iTSFillCell(tmpData, table, 1, 1, 0, "")
            tmpData = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(3).Text
            iTSFillCell(tmpData, table, 2, 1, 0, "")
            tmpData = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(5).Text
            iTSFillCell(tmpData, table, 2, 1, 0, "")


            tmpSub += 1
        Loop
        iTSFillCell(tmpTotalLineText, table, 2, 5, 2, "YELLOW")
        iTSFillCell(String.Format("{0:0.00}", tmpSectionTotal), table, 2, 1, 2, "YELLOW")

        '
        mydoc.Add(table)

        Return ""
    End Function
    ''' <summary>
    ''' This function build the actual printlines for the statement recap by account.
    ''' </summary>
    ''' <param name="myDoc"></param>
    ''' <returns></returns>
    Private Function JTrecBldInvSummarySection(ByRef myDoc As Document) As String
        Dim table As New PdfPTable(3)
        ' Actual width of table in points
        table.TotalWidth = 350.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 2
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.25F, 3.75F, 1.75F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        Dim tmpTableHeader As String = "STATEMENT RECAP BY ACCOUNT"
        Dim tmpTotalLineText As String = "Reconciled Non-Job-Related Invoice Total ---------> "
        Dim tmpSectionTotal As Short = 0
        '
        iTSFillCell(tmpTableHeader, table, 1, 3, 2, "YELLOW")
        '
        iTSFillCell("Account", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Account Description", table, 1, 1, 2, "YELLOW")
        iTSFillCell("Amount", table, 1, 1, 2, "YELLOW")
        '
        Dim tmpJRTotal As Double = 0
        Dim tmpNJRTotal As Double = 0
        '
        If MatTotal <> 0 Then
            tmpJRTotal += MatTotal
            JTrecBldRecapLines(table, "JOBRELATED:MAT", "Materials directly billed to jobs", String.Format("{0:0.00}", MatTotal))
        End If
        If SerTotal <> 0 Then
            tmpJRTotal += SerTotal
            JTrecBldRecapLines(table, "JOBRELATED:SER", "Services directly billed to jobs", String.Format("{0:0.00}", SerTotal))
        End If
        If PRTotal <> 0 Then
            tmpJRTotal += PRTotal
            JTrecBldRecapLines(table, "1099Payroll", "Labor bills from 1099 workers", String.Format("{0:0.00}", PRTotal))
        End If
        If tmpJRTotal <> 0 Then
            iTSFillCell("Total --- Job Related Costs ----> ", table, 2, 2, 2, "YELLOW")
            iTSFillCell(String.Format("{0:0.00}", tmpJRTotal), table, 2, 1, 2, "YELLOW")
            iTSFillCell("  ", table, 0, 3, 0, "")
        End If

        '
        Dim tmpSub As Integer = 0
        If lvJTrecAcctTls.Items.Count > 0 Then
            iTSFillCell("Account", table, 1, 1, 2, "YELLOW")
            iTSFillCell("Account Description", table, 1, 1, 2, "YELLOW")
            iTSFillCell("Amount", table, 1, 1, 2, "YELLOW")
            Do While tmpSub < lvJTrecAcctTls.Items.Count
                tmpNJRTotal += Val(lvJTrecAcctTls.Items(tmpSub).SubItems(2).Text)
                JTrecBldRecapLines(table, lvJTrecAcctTls.Items(tmpSub).Text,
                                   lvJTrecAcctTls.Items(tmpSub).SubItems(1).Text,
                                   lvJTrecAcctTls.Items(tmpSub).SubItems(2).Text)
                tmpSub += 1
            Loop
            iTSFillCell("Total --- Non-Job-Related Costs ----> ", table, 2, 2, 2, "YELLOW")
            iTSFillCell(String.Format("{0:0.00}", tmpNJRTotal), table, 2, 1, 2, "YELLOW")
            iTSFillCell("  ", table, 0, 3, 0, "")
        End If
        '
        iTSFillCell("Grand Total --------------> ", table, 2, 2, 2, "YELLOW")
        Dim tmpGrandTotal As Double = tmpJRTotal + tmpNJRTotal
        iTSFillCell(String.Format("{0:0.00}", tmpGrandTotal), table, 2, 1, 2, "YELLOW")
        '
        myDoc.Add(table)
        Return ""
    End Function
    ''' <summary>
    '''  This little Function Is used To actually produce one printline With the data that Is passed through arguments. It Is 
    '''  used while printing the account summary for this invoice.
    ''' </summary>
    Private Function JTrecBldRecapLines(ByRef table As PdfPTable, tmpAcct As String, tmpAcctDesc As String, tmpAmount As String)
        iTSFillCell(tmpAcct, table, 1, 1, 0, "")
        iTSFillCell(tmpAcctDesc, table, 1, 1, 0, "")
        iTSFillCell(tmpAmount, table, 2, 1, 0, "")
        Return ""
    End Function
    '

    Public Function JTrecUpdtRecDate(ByRef TypeUpdt As String) As String
        ' ----------------------------------------------------------------------------------------
        ' Unless this session is to suspend the REC session,
        ' delete any NJR records with a source of REC from slJTnlc. Even if they were not checked
        ' they will be deleted. If they belong on another statement they can be reentered.
        ' ----------------------------------------------------------------------------------------
        Dim tmpSub As Integer = 0
        '
        Dim tmpNLCitem As JTnlc.JTnlcStructure
        ' Update Rec data on job related nlc items
        Dim keys As ICollection = slJTrecKey.Keys
        Do While tmpSub < lvJTrecData.Items.Count
            If lvJTrecData.Items(tmpSub).Checked = True Then

                Dim tmpKey As String = slJTrecKey(keys(tmpSub))
                tmpNLCitem = JTnlc.slJTnlc(tmpKey)
                JTnlc.JTnlcDelSLRecord(tmpKey)
                If TypeUpdt = "SUSPEND" Then
                    tmpNLCitem.ReconcileDate = "PENDING"
                Else
                    tmpNLCitem.ReconcileDate = Date.Today
                    tmpNLCitem.ReconcileDoc = txtJTrecRecName.Text
                End If

                JTnlc.JTnlcAddSLItem(tmpKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~793")
            End If
            tmpSub += 1
        Loop
        ' -------------------------------------------------------------------------------------
        ' Update Rec data on NJR nlc items. If reconciliation is being finalized, the NJR items
        ' entered in JTrec are dropped.
        ' -------------------------------------------------------------------------------------
        tmpSub = 0
        Do While tmpSub < lvJTrecNJRInvList.Items.Count
            Dim tmpKey As String = lvJTrecNJRInvList.Items(tmpSub).SubItems(7).Text
            tmpNLCitem = JTnlc.slJTnlc(tmpKey)
            If lvJTrecNJRInvList.Items(tmpSub).Checked = True Then
                JTnlc.JTnlcDelSLRecord(tmpKey)
                If TypeUpdt = "SUSPEND" Then
                    tmpNLCitem.ReconcileDate = "PENDING"
                Else
                    tmpNLCitem.ReconcileDate = Date.Today
                    tmpNLCitem.ReconcileDoc = txtJTrecRecName.Text
                End If
                ' ----------------------------------------------------------------------------
                ' On completion of JTrec session, only save NJR items whose source is
                ' NLC. REC items should be dropped. On SUSPEND save all items.
                ' ----------------------------------------------------------------------------
                If TypeUpdt <> "SUSPEND" Then
                    If lvJTrecNJRInvList.Items(tmpSub).SubItems(6).Text = "NLC" Then
                        JTnlc.JTnlcAddSLItem(tmpKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~819")
                    End If
                Else
                    JTnlc.JTnlcAddSLItem(tmpKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~822")
                End If
            Else
                ' process not checked items
                If TypeUpdt = "SUSPEND" Then
                    If tmpNLCitem.ReconcileDate <> "" Or tmpNLCitem.ReconcileDoc <> "" Then
                        JTnlc.JTnlcDelSLRecord(tmpKey)
                        tmpNLCitem.ReconcileDate = ""
                        tmpNLCitem.ReconcileDoc = ""
                        JTnlc.JTnlcAddSLItem(tmpKey, tmpNLCitem, "JtrecUpdtRecDate - Jtrec~831")
                    End If
                Else
                    If lvJTrecNJRInvList.Items(tmpSub).SubItems(6).Text = "REC" Then
                        JTnlc.JTnlcDelSLRecord(tmpKey)
                    End If
                End If
            End If
            tmpSub += 1
        Loop
        ' ------------------------------------------------------------
        ' Clear Job related nlc items that might have a leftover "PENDING" in them.
        ' ------------------------------------------------------------
        keys = slJTrecKey.Keys
        tmpSub = 0
        Do While tmpSub < lvJTrecData.Items.Count
            If lvJTrecData.Items(tmpSub).Checked <> True Then
                Dim tmpKey As String = slJTrecKey(keys(tmpSub))
                tmpNLCitem = JTnlc.slJTnlc(tmpKey)
                If tmpNLCitem.ReconcileDate <> "" Or tmpNLCitem.ReconcileDoc <> "" Then
                    JTnlc.JTnlcDelSLRecord(tmpKey)
                    tmpNLCitem.ReconcileDate = ""
                    tmpNLCitem.ReconcileDoc = ""
                    JTnlc.JTnlcAddSLItem(tmpKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~854")
                End If
            End If
            tmpSub += 1
        Loop
        '
        ' ------------------------------------------------------------
        ' Clear (NJR) non-Job related nlc items that might have a leftover "PENDING" in them.
        ' ------------------------------------------------------------
        '     If TypeUpdt <> "SUSPEND" Then
        '         tmpSub = 0
        '         Do While tmpSub < lvJTrecNJRInvList.Items.Count
        '             If lvJTrecNJRInvList.Items(tmpSub).Checked = False Then
        '                Dim tmpKey As String = lvJTrecNJRInvList.Items(tmpSub).SubItems(7).Text
        '                tmpNLCitem = JTnlc.slJTnlc(tmpKey)
        '                If tmpNLCitem.ReconcileDate <> "" Or tmpNLCitem.ReconcileDoc <> "" Then
        '                   JTnlc.JTnlcDelSLRecord(tmpKey)
        '                   tmpNLCitem.ReconcileDate = ""
        '                   tmpNLCitem.ReconcileDoc = ""
        '                   JTnlc.JTnlcAddSLRecord(tmpKey, tmpNLCitem)
        '                End If
        '             End If
        '             tmpSub += 1
        '         Loop
        '    End If
        '
        ' ------------------------------------------------------------------------------------------
        ' Add records to slJTnlc to support statement extract for Accounting.
        ' A header will be added and as many account records as there was activity on the statement.
        ' ------------------------------------------------------------------------------------------
        '
        If TypeUpdt <> "SUSPEND" Then                     ' only execute when reconcile is being finalized
            tmpNLCitem = Nothing
            '
            tmpNLCitem.EntryDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
            tmpNLCitem.InvDate = mtxtJTrecStmtDate.Text
            tmpNLCitem.JobID = "ZZZSTMT"
            tmpNLCitem.SubID = "0" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text
            Dim tmpRecKey As String = JTnlc.JTnlcBldSLKey(tmpNLCitem.EntryDate, tmpNLCitem.JobID, tmpNLCitem.SubID, "", tmpNLCitem.Supplier)
            tmpNLCitem.Supplier = txtJTrecVndShrtName.Text
            tmpNLCitem.SupInvNum = txtJTrecStmtInvNumb.Text
            tmpNLCitem.SupCost = txtJTrecStatBal.Text
            If rbtnJTrecExport.Checked = True Then
                tmpNLCitem.PymtSwitch = "E"
            Else
                tmpNLCitem.PymtSwitch = "M"
            End If
            tmpNLCitem.NJREAcct = ""
            '
            tmpNLCitem.viAcctNum = txtJTrecAcctNum.Text
            tmpNLCitem.viVendName = txtJTrecVendorName.Text
            tmpNLCitem.viStreet = txtJTrecVendAddrss.Text
            tmpNLCitem.viCityState = txtJTrecVendCityState.Text
            '
            tmpNLCitem.viDueDate = mtxtJTrecDueDate.Text
            tmpNLCitem.viDiscpercent = txtJTrecVendDisc.Text
            ' Saving discount amount, if there is one.
            If JTrecdiscountAmt <> 0 Then
                tmpNLCitem.viDiscAmt = JTrecDiscountAmt.ToString
            Else
                tmpNLCitem.viDiscAmt = Nothing
            End If
            JTrecDiscountAmt = 0

            ' statement header record --- inserted into slJTnlc
            JTnlc.JTnlcAddSLItem(tmpRecKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~974")
            '
            ' clear fields not needed in account records.
            tmpNLCitem.SupCost = Nothing
            tmpNLCitem.PymtSwitch = Nothing
            tmpNLCitem.viDueDate = Nothing
            tmpNLCitem.viDiscpercent = Nothing
            tmpNLCitem.viDiscAmt = Nothing
            '
            If MatTotal <> 0 Then
                tmpNLCitem.JobID = "ZZZSTMT"
                tmpNLCitem.SubID = "2" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text & "JOBRELATEDMAT"
                tmpRecKey = JTnlc.JTnlcBldSLKey(tmpNLCitem.EntryDate, tmpNLCitem.JobID, tmpNLCitem.SubID, "", tmpNLCitem.Supplier)
                tmpNLCitem.SupCost = String.Format("{0:0.00}", MatTotal)
                tmpNLCitem.NJREAcct = "JOBRELATED:MAT"
                JTnlc.JTnlcAddSLItem(tmpRecKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~921")
            End If
            If SerTotal <> 0 Then
                    tmpNLCitem.JobID = "ZZZSTMT"
                    tmpNLCitem.SubID = "2" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text & "JOBRELATEDSER"
                    tmpRecKey = JTnlc.JTnlcBldSLKey(tmpNLCitem.EntryDate, tmpNLCitem.JobID, tmpNLCitem.SubID, "", tmpNLCitem.Supplier)
                    tmpNLCitem.SupCost = String.Format("{0:0.00}", SerTotal)
                    tmpNLCitem.NJREAcct = "JOBRELATED:SER"
                    JTnlc.JTnlcAddSLItem(tmpRecKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~929")
                End If
                If PRTotal <> 0 Then
                    tmpNLCitem.JobID = "ZZZSTMT"
                    tmpNLCitem.SubID = "2" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text & "1099PAYROLL"
                    tmpRecKey = JTnlc.JTnlcBldSLKey(tmpNLCitem.EntryDate, tmpNLCitem.JobID, tmpNLCitem.SubID, "", tmpNLCitem.Supplier)
                    tmpNLCitem.SupCost = String.Format("{0:0.00}", PRTotal)
                    tmpNLCitem.NJREAcct = "1099PAYROLL"
                    JTnlc.JTnlcAddSLItem(tmpRecKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~937")
                End If
                ' create record for each NJR category present on this statement.
                tmpSub = 0
                Do While tmpSub < lvJTrecAcctTls.Items.Count

                    tmpNLCitem.NJREAcct = lvJTrecAcctTls.Items(tmpSub).Text
                    tmpNLCitem.JobID = "ZZZSTMT"
                    tmpNLCitem.SubID = "2" & txtJTrecVndShrtName.Text & txtJTrecStmtInvNumb.Text & tmpNLCitem.NJREAcct
                    tmpRecKey = JTnlc.JTnlcBldSLKey(tmpNLCitem.EntryDate, tmpNLCitem.JobID, tmpNLCitem.SubID, "", tmpNLCitem.Supplier)
                    tmpNLCitem.SupCost = lvJTrecAcctTls.Items(tmpSub).SubItems(2).Text
                    JTnlc.JTnlcAddSLItem(tmpRecKey, tmpNLCitem, "JTrecUpdtRecDate - JTrec~948")
                    tmpSub += 1
                Loop
            End If
            '
            JTnlc.JTnlcArrayFlush(TypeUpdt)
        lvJTrecData.Items.Clear()
        slJTrec.Clear()
        '
        Return ""
    End Function


    Private Sub txtOptMsg_LostFocus(sender As Object, e As EventArgs) Handles txtOptMsg.LostFocus
        JTVarMaint.JTGVrecOptMsg = txtOptMsg.Text
    End Sub

    Private Sub btnJTrecSuspend_Click(sender As Object, e As EventArgs) Handles btnJTrecSuspend.Click
        JTrecUpdtRecDate("SUSPEND")
        Me.Hide()
        JTMainMenu.JTMMinit()
    End Sub

    Private Sub rbtnJTrecVJD_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTrecVJD.CheckedChanged,
            rbtnJTrecVJA.CheckedChanged, rbtnJTrecDVJ.CheckedChanged, rbtnJTrecVDJ.CheckedChanged
        JTrecUpdtRecDate("SUSPEND")
        Me.Hide()
        JTrecInit()
    End Sub
    ''' <summary>
    ''' Trigger by Corrections button ... determines if there selected items and saves key for JTnlc. It
    ''' then calls JTnlc so that a line or lines can be corrected.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTrecCorrections_Click(sender As Object, e As EventArgs) Handles btnJTrecCorrections.Click
        ' ----------------------------------------------------------------------------
        ' Suspend Rec session and send to NLC. Set flag so that NLC knows to send back
        ' to Rec when done modifications.
        ' ----------------------------------------------------------------------------
        JTrecSuspendedFlag = True
        ' ----------------------------------------------------------------
        ' Determine if a table line has been highlighted. If true, save
        ' the date, inv # and amount so that NLC code can locate it.
        ' ----------------------------------------------------------------
        If lvJTrecData.SelectedItems.Count > 0 Then
            JTrecSuspndDate = lvJTrecData.SelectedItems(0).Text
            JTrecSuspndInvNum = lvJTrecData.SelectedItems(0).SubItems(3).Text
            JtrecSuspndAmount = lvJTrecData.SelectedItems(0).SubItems(4).Text
        Else
            If lvJTrecNJRInvList.SelectedItems.Count > 0 Then
                JTrecSuspndDate = lvJTrecNJRInvList.SelectedItems(0).Text
                JTrecSuspndInvNum = lvJTrecNJRInvList.SelectedItems(0).SubItems(3).Text
                JtrecSuspndAmount = lvJTrecNJRInvList.SelectedItems(0).SubItems(5).Text
            End If
        End If
        JTrecUpdtRecDate("SUSPEND")
        Me.Hide()
        JTnlcE.JtnlcEInit("REVMODREC", JtrecItemCorrectionJTnlcKey)
    End Sub

    Private Sub txtJTrecVndShrtName_Leave(sender As Object, e As EventArgs) _
        Handles txtJTrecVndShrtName.Leave
        If txtJTrecVndShrtName.Text = "" Then
            Exit Sub
        End If
        txtJTrecVndShrtName.Text = txtJTrecVndShrtName.Text.ToUpper
        Dim tmpVendShtName As String = txtJTrecVndShrtName.Text & "_STMT"
        Dim tmpJTviRecord As JTvi.JTviArrayStructure = Nothing
        If JTvi.JTviRetrieveVendInfo(tmpVendShtName, tmpJTviRecord) = "NOTFOUND" Then
            ToolTip1.Show("Vendor Short Name does Not exist. Correct And resubmit Or" & vbCrLf &
                          "check To go To Vendor Information To create the account.(REC.001)",
                          txtJTrecVndShrtName, 20, -55, 6000)
            txtJTrecVndShrtName.Select()
            Exit Sub
        End If
        txtJTrecAcctNum.Text = tmpJTviRecord.JTviAcctNum
        txtJTrecVendorName.Text = tmpJTviRecord.JTviVendName
        txtJTrecVendAddrss.Text = tmpJTviRecord.JTviStreet
        txtJTrecVendCityState.Text = tmpJTviRecord.JTviCityStateZip
        '
        txtJTrecVendTerms.Text = tmpJTviRecord.JTviPymtTerms
        txtJTrecVendDisc.Text = tmpJTviRecord.JTviDiscPercent
        If tmpJTviRecord.JTviActivityInterface = "E" Then
            rbtnJTrecExport.Checked = True
        Else
            rbtnJTrecManual.Checked = True
        End If
        JTVarMaint.JTGVrecVndShrtName = txtJTrecVndShrtName.Text
        '
        mtxtJTrecStmtDate.Select()
    End Sub
    Private Function JTrecUpdtSuggestionsinLV() As String
        ' ----------------------------------------------------------------------------------------------------------------
        ' Loop through two main list views and add checks for entries that have RecSuggest on for this Vendor's Statement.
        ' ----------------------------------------------------------------------------------------------------------------
        Dim tmpStmtDate As Date = CDate(String.Format("{0:MM/dd/yyyy}", mtxtJTrecStmtDate.Text))
        Dim tmpSub As Integer = 0
        Dim tmpMode As String = "LOOKUP"
        Do While tmpSub < lvJTrecData.Items.Count
            Dim tmpSpplrName As String = lvJTrecData.Items(tmpSub).SubItems(1).Text
            If JTvi.JTviReconSuggest(tmpMode, tmpSpplrName, txtJTrecVndShrtName.Text) = True Or
                tmpSpplrName = txtJTrecVndShrtName.Text Then
                ' checking to make sure invoice is not after statement date
                Dim tmpLVDate As Date = CDate(String.Format("{0:MM/dd/yyyy}", lvJTrecData.Items(tmpSub).Text))
                If tmpLVDate <= tmpStmtDate Then
                    lvJTrecData.Items(tmpSub).Checked = True
                End If
            End If
            tmpSub += 1
        Loop
        tmpSub = 0
        Do While tmpSub < lvJTrecNJRInvList.Items.Count
            Dim tmpSpplrName As String = lvJTrecNJRInvList.Items(tmpSub).SubItems(2).Text
            If JTvi.JTviReconSuggest(tmpMode, tmpSpplrName, txtJTrecVndShrtName.Text) = True Or
                tmpSpplrName = txtJTrecVndShrtName.Text Then
                Dim tmpLVDate As Date = CDate(String.Format("{0:MM/dd/yyyy}", lvJTrecNJRInvList.Items(tmpSub).Text))
                ' checking to make sure invoice is not after statement date
                If tmpLVDate <= tmpStmtDate Then
                    lvJTrecNJRInvList.Items(tmpSub).Checked = True
                End If
            End If
            tmpSub += 1
        Loop
        Return ""
    End Function

    ''' <summary>Check input of statement date for a valid date. If correct, use that date and 
    '''          the Statement Terms to calculate due date.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTrecStmtDate_Leave(sender As Object, e As EventArgs) Handles mtxtJTrecStmtDate.Leave
        ' if entry has been skipped or invalid date entered just skip the rest of the sub for now.
        If TestForValidDate(mtxtJTrecStmtDate.Text) = False Then
            Exit Sub
        End If
        JTVarMaint.JTGVrecVndInvDate = mtxtJTrecStmtDate.Text
        ' ----------------------------------------------------------------------------
        ' Using Statement date entered, calculate statement due date.
        ' ----------------------------------------------------------------------------
        mtxtJTrecDueDate.Text = JTrecCalcDueDate(mtxtJTrecStmtDate.Text, txtJTrecVendTerms.Text)
        JTVarMaint.JTGVrecVndInvDueDate = mtxtJTrecDueDate.Text
        txtJTrecStmtInvNumb.Select()
        ' ------------------------------------------------------------------------------------
        ' Fill checks on suggested lv lines. This routine will be called if none of the items 
        ' are currently checked. This should take care of the problem where a suspend/resume
        ' session takes place and this routine overrides corrections already made.
        '
        ' Suggestions for reconciliation can be disables or enabled in global variables
        ' in the NLC section.
        ' --------------------------------------
        ' If any items in the arrays are checked the suggestion logic will be skipped.
        ' ------------------------------------------------------------------------------------
        If JTVarMaint.ReconMakeSuggestions = True Then
            If lvJTrecData.CheckedItems.Count = 0 Then
                If lvJTrecNJRInvList.CheckedItems.Count = 0 Then
                    JTrecUpdtSuggestionsinLV()
                End If
            End If
        End If
        '
    End Sub
    ''' <summary>
    ''' Function to validate stmt date. It also load the stmt number with a suggestion
    ''' (since many vendor statements do not include a number) for a 'created' statement number.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTrecStmtDate_LostFocus(sender As Object, e As EventArgs) Handles mtxtJTrecStmtDate.LostFocus
        If TestForValidDate(mtxtJTrecStmtDate.Text) = False Then
            ToolTip1.Show("Invoice date must have an valid entry.(REC.009)",
                          mtxtJTrecStmtDate, 20, -55, 6000)
            mtxtJTrecStmtDate.Select()
            Exit Sub
        End If
        txtJTrecStmtInvNumb.Text = "STMT" & mtxtJTrecStmtDate.Text.Substring(6, 4) & mtxtJTrecStmtDate.Text.Substring(0, 2)
    End Sub

    ''' <summary>Sub handles logic after statement invoice number has been entered. Using the terms on the vendor record (JTvi), a due date for the statement is calculated.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecStmtInvNumb_LostFocus(sender As Object, e As EventArgs) Handles txtJTrecStmtInvNumb.LostFocus
        If txtJTrecStmtInvNumb.Text = "" Then
            ToolTip1.Show("Invoice # must have an entry.(REC.004)",
                          txtJTrecStmtInvNumb, 20, -55, 6000)
            txtJTrecStmtInvNumb.Select()
            Exit Sub
        End If

        JTVarMaint.JTGVrecVndInvNumber = txtJTrecStmtInvNumb.Text
        Dim tmpDate As Date = mtxtJTrecStmtDate.Text
        txtJTrecRecName.Text = txtJTrecVndShrtName.Text & "_" & txtJTrecStmtInvNumb.Text & "_" & tmpDate.Year &
            String.Format("{0:00}", Val(tmpDate.Month)) & String.Format("{0:00}", Val(tmpDate.Day))
        If Not FileNameIsOk(txtJTrecRecName.Text) Then
            ToolTip1.Show("Entry in Invoice # field must be unique. It becomes part of the Reconcilation Name." &
                          vbCrLf & "The current combination(Vend Name & Invoice #) is not unique. Change And resubmit.(REC.005)", txtJTrecStmtInvNumb, 20, -55, 6000)
            txtJTrecStmtInvNumb.Select()
        End If
        Dim filename As String = JTVarMaint.JTvmFilePath & "ReconcilePDF\" & txtJTrecRecName.Text & ".PDF"
        If My.Computer.FileSystem.FileExists(filename) Then
            ToolTip1.Show("Multiple Rec done on this Vendor/Inv # today. Make a change to the invoice #" & vbCrLf &
                          "and resubmit.(REC.006)", txtJTrecStmtInvNumb, 20, -55, 6000)
            txtJTrecStmtInvNumb.Select()
        Else
            JTVarMaint.JTGVrecName = txtJTrecRecName.Text
        End If
        ' Editing complete on invoice # ... On to balance.
        txtJTrecStatBal.Select()

    End Sub

    ''' <summary>Sub validates invoice total entry for numerics, etc. It copies total to Rec Recap window for viewing activity.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecStatBal_Leave(sender As Object, e As EventArgs) Handles txtJTrecStatBal.Leave
        If txtJTrecStatBal.Text <> "" Then
            If IsNumeric(txtJTrecStatBal.Text) <> True Then
                ToolTip1.Show("Statement Balance entry not numeric. Correct and resubmit.", txtJTrecStatBal, 20, -55, 6000)
                txtJTrecStatBal.Text = ""
                txtJTrecStatBal.Select()
            Else
                txtJTrecStatBal.Text = String.Format("{0:0.00}", Val(txtJTrecStatBal.Text))
                txtJTrecRecStmtTl.Text = txtJTrecStatBal.Text
                JTVarMaint.JTGVrecStmtAmt = txtJTrecStatBal.Text
            End If
        End If
        mtxtJTrecDueDate.Select()
    End Sub

    ''' <summary>Handles check to add or update vendor info for this statement.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTrecAddUpdtVend_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTrecAddUpdtVend.CheckedChanged
        If cboxJTrecAddUpdtVend.Checked = True Then
            cboxJTrecAddUpdtVend.Checked = False
            JTvi.JTviReturnPanel = "REC"
            txtJTrecVndShrtName.Select()
            '         Me.Hide()
            If txtJTrecVndShrtName.Text <> "" Then
                JTvi.JTviPopPanel(txtJTrecVndShrtName.Text, "STMT")
                JTvi.Show()
            Else
                ToolTip1.Show("A vendor name for the statement must be entered" & vbCrLf &
                              "prior to adding or modifying its information(REC.008)", txtJTrecVndShrtName, 100, 0, 6000)
            End If
        End If
    End Sub
    ''' <summary>Function to fill autocomplete tables for vend short name on JTrec panel. It is called on initial init of the panel and also if JTvi functions have been executed.</summary>
    Public Function JTrecFillShortNameAutoTable() As String
        '
        Dim JTrecSTMTList As New AutoCompleteStringCollection
        Dim tmpviSub As Integer = 0
        Dim tmpShortName As String = ""
        Dim tmpVendType As String = ""
        Do While JTvi.JTviShortNames(tmpviSub, tmpShortName, tmpVendType) = "DATA"
            If tmpVendType = "STMT" Then
                JTrecSTMTList.Add(tmpShortName)
            End If
            tmpviSub += 1
        Loop
        ' ------------------------------------
        ' Set ComboBox AutoComplete properties
        ' ------------------------------------
        txtJTrecVndShrtName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTrecVndShrtName.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTrecVndShrtName.AutoCompleteCustomSource = JTrecSTMTList
        Return ""
    End Function

    ''' <summary>Check for valid date ... NJR line item entry.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub mtxtJTrecNJDate_Leave(sender As Object, e As EventArgs) Handles mtxtJTrecNJDate.Leave
        JTrecBldAutoCompleteNJRAccounts()
        If TestForValidDate(mtxtJTrecNJDate.Text) = False Then
            ToolTip1.Show("Invoice date Not valid. Correct And resubmit.(REC.002)",
                          mtxtJTrecNJDate, 20, -55, 6000)
            Exit Sub
        End If
        '
        txtJTrecNJAccount.Select()
    End Sub
    ''' <summary>Create the autofill table for the NJR accounts.</summary>
    Public Function JTrecNJAccountAutoTable() As String
        '
        Dim JTrecNJAcctList As New AutoCompleteStringCollection
        Dim tmpSub As Integer = 0
        Dim tmpAcctName As String = Nothing
        Dim tmpAcctDesc As String = Nothing
        Do While JTVarMaint.JTvmReadNJREA(tmpSub, tmpAcctName, tmpAcctDesc) = "DATA"
            JTrecNJAcctList.Add(tmpAcctName)
            '          MsgBox("Acct Name being added to JTrecNJAcctList - >" & tmpAcctName & "<  JTrec~712")
            tmpSub += 1
        Loop
        ' ------------------------------------
        ' Set ComboBox AutoComplete properties
        ' ------------------------------------
        txtJTrecNJAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTrecNJAccount.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTrecNJAccount.AutoCompleteCustomSource = JTrecNJAcctList
        Return ""
    End Function

    ''' <summary>
    ''' Validate the account after entry in NJR. If it exists, it is update in the expense summay LV. If it doesn't exist, logic is queued to either make a new entry or to create a new account.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecNJAccount_Leave(sender As Object, e As EventArgs) Handles txtJTrecNJAccount.Leave
        If txtJTrecNJAccount.Text = "" Then
            Exit Sub
        End If
        txtJTrecNJAccount.Text = txtJTrecNJAccount.Text.ToUpper.Trim
        Dim tmpAcctDesc As String = ""
        If JTVarMaint.JTvmValidateUpdateNJREA("Validate", txtJTrecNJAccount.Text, tmpAcctDesc) <> "EXISTS" Then
            If JTnjram.JTnjramInit(txtJTrecNJAccount.Text, tmpAcctDesc) = "ADDED" Then
                JTreclvJVrecAcctTlsAddAccts(txtJTrecNJAccount.Text, tmpAcctDesc)
                txtJTrecNJVendor.Select()
                Exit Sub
            Else
                ToolTip1.Show("Invalid Account. Correct and resubmit.", txtJTrecNJAccount, 20, -55, 6000)
                txtJTrecNJAccount.Select()
                Exit Sub
            End If
        Else
            JTreclvJVrecAcctTlsAddAccts(txtJTrecNJAccount.Text, tmpAcctDesc)
            txtJTrecNJVendor.Select()
        End If


    End Sub
    ''' <summary>Function to update Account and its description as a line in the lvJTrecAcctTls display. Amount will later be added to each line in this array.</summary>
    ''' <param name="tmpAcctName"></param>
    Private Function JTreclvJVrecAcctTlsAddAccts(tmpAcctName As String, tmpAcctDesc As String) As String
        '   Dim tmpAcctDesc As String = ""
        Dim tmpSub As Integer = 0
        Dim tmpAcctExists As Boolean = False
        If lvJTrecAcctTls.Items.Count > 0 Then
            Do While tmpSub < lvJTrecAcctTls.Items.Count
                If lvJTrecAcctTls.Items(tmpSub).Text = txtJTrecNJAccount.Text Then
                    tmpAcctExists = True
                End If
                tmpSub += 1
            Loop
        End If
        If tmpAcctExists = False Then
            Dim itmBuild(3) As String
            Dim lvLine As ListViewItem
            itmBuild(0) = txtJTrecNJAccount.Text
            itmBuild(1) = tmpAcctDesc
            itmBuild(2) = "0.00"
            lvLine = New ListViewItem(itmBuild)
            lvJTrecAcctTls.Items.Add(lvLine)
        End If
        lvJTrecAcctTls.Visible = True
        Return ""
    End Function
    ''' <summary>
    ''' Function to build autocomplete table to vendors.
    ''' </summary>
    Private Function JTrecBldVndAutoComplete()

        Dim JTrecNJVendorList As New AutoCompleteStringCollection
        ' ---------------------------------------------------------------
        ' Build list of suggested Vendors from VendInfo (slJTvi) table.
        ' ---------------------------------------------------------------
        Dim tmpSub As Integer = 0
        Dim tmpShortName As String = ""
        Dim tmpVendType As String = ""
        Do While JTvi.JTviShortNames(tmpSub, tmpShortName, tmpVendType) = "DATA"
            If tmpVendType <> "STMT" Then
                JTrecNJVendorList.Add(tmpShortName)
            End If
            tmpSub += 1
        Loop
        ' ---------------------------------------------------------
        ' Then Set ComboBox AutoComplete properties
        ' ---------------------------------------------------------

        txtJTrecNJVendor.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTrecNJVendor.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTrecNJVendor.AutoCompleteCustomSource = JTrecNJVendorList

        Return ""
    End Function

    ''' <summary>Validate that something has been entered for a description in the NJR entry.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecNJVendor_Leave(sender As Object, e As EventArgs) Handles txtJTrecNJVendor.Leave
        If txtJTrecNJVendor.Text = "" Then
            '         txtJTrecNJDesc.Select()
            Exit Sub
        End If
        txtJTrecNJVendor.Text = txtJTrecNJVendor.Text.ToUpper.Trim
    End Sub

    ''' <summary>Validates that something has been entered for the description on the NJR entry.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecNJDesc_Leave(sender As Object, e As EventArgs) Handles txtJTrecNJDesc.Leave
        If txtJTrecNJDesc.Text = "" Then
            txtJTrecNJAmount.Select()
            Exit Sub
        End If
        txtJTrecNJDesc.Text = txtJTrecNJDesc.Text.ToUpper.Trim
    End Sub

    ''' <summary>Reformats amount entry to be viewed with 2 decimal points. ... 0.00.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTrecNJAmount_Leave(sender As Object, e As EventArgs) Handles txtJTrecNJAmount.Leave
        txtJTrecNJAmount.Text = String.Format("{0:0.00}", Val(txtJTrecNJAmount.Text))
        btnJTrecNJRecord.Select()
        Exit Sub
    End Sub
    ''' <summary>
    ''' Sub to reedit fields to make sure there are entries and then move data to lv and update account totals.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTrecNJRecord_Click(sender As Object, e As EventArgs) Handles btnJTrecNJRecord.Click
        If TestForValidDate(mtxtJTrecNJDate.Text) = False Then
            ToolTip1.Show("Transaction requires valid date. Correct and resubmit.", mtxtJTrecNJDate, 20, -55, 6000)
            mtxtJTrecNJDate.Select()
            Exit Sub
        End If
        If txtJTrecNJAccount.Text = "" Then
            ToolTip1.Show("Transaction requires a vendor. Correct and resubmit.", txtJTrecNJAccount, 20, -55, 6000)
            txtJTrecNJAccount.Select()
            Exit Sub
        End If
        If txtJTrecNJVendor.Text = "" Then
            ToolTip1.Show("Transaction requires a vendor. Correct and resubmit.", txtJTrecNJVendor, 20, -55, 6000)
            txtJTrecNJVendor.Select()
            Exit Sub
        End If
        If txtJTrecNJInvNumber.Text = "" Then
            ToolTip1.Show("Transaction requires an invoice number. Correct and resubmit.", txtJTrecNJInvNumber, 20, -55, 6000)
            txtJTrecNJInvNumber.Select()
            Exit Sub
        End If
        If txtJTrecNJDesc.Text = "" Then
            ToolTip1.Show("Transaction requires a description. Correct and resubmit.", txtJTrecNJDesc, 20, -55, 6000)
            txtJTrecNJDesc.Select()
            Exit Sub
        End If
        If Val(txtJTrecNJAmount.Text) = 0 Then
            ToolTip1.Show("Transaction requires an amount. Correct and resubmit.", txtJTrecNJAmount, 20, -55, 6000)
            txtJTrecNJAmount.Select()
            Exit Sub
        End If
        '
        ' -----------------------------------------------------------------------------------------
        ' Create slJTnls record for storing NJR/REC item.
        ' -----------------------------------------------------------------------------------------
        Dim tmpinvdate As Date = mtxtJTrecNJDate.Text
        Dim tmpRecKey As String = JTnlc.JTnlcBldSLKey(tmpinvdate, "", "", "", txtJTrecNJVendor.Text)
        Dim tmpItem As JTnlc.JTnlcStructure = Nothing
        '
        tmpItem.EntryDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
        tmpItem.InvDate = mtxtJTrecNJDate.Text
        tmpItem.MorS = "NJR"
        tmpItem.Descrpt = txtJTrecNJDesc.Text
        tmpItem.Supplier = txtJTrecNJVendor.Text
        tmpItem.SupInvNum = txtJTrecNJInvNumber.Text
        tmpItem.SupCost = txtJTrecNJAmount.Text
        tmpItem.PymtSwitch = "R"
        tmpItem.NJREAcct = txtJTrecNJAccount.Text
        tmpItem.viRcrdEntrySrce = "REC"
        '
        JTnlc.JTnlcAddSLItem(tmpRecKey, tmpItem, " btnJTrecNJRecord_Click - JTrec~1403")
        '

        ' -----------------------------------------------------------------------------------------
        ' Create lv item for JTrecPanel. Last two items are invisible --- only used internally 
        ' by program logic.
        ' -----------------------------------------------------------------------------------------
        Dim itmBuild(8) As String
        Dim lvLine As ListViewItem
        itmBuild(0) = mtxtJTrecNJDate.Text
        itmBuild(1) = txtJTrecNJAccount.Text
        itmBuild(2) = txtJTrecNJVendor.Text
        itmBuild(3) = txtJTrecNJInvNumber.Text
        itmBuild(4) = txtJTrecNJDesc.Text
        itmBuild(5) = txtJTrecNJAmount.Text
        itmBuild(6) = "REC"
        itmBuild(7) = tmpRecKey
        '
        lvLine = New ListViewItem(itmBuild)
        lvJTrecNJRInvList.Items.Add(lvLine)
        lvJTrecNJRInvList.Items(lvJTrecNJRInvList.Items.Count - 1).Checked = True
        '
        mtxtJTrecNJDate.Text = Nothing
        txtJTrecNJAccount.Text = Nothing
        txtJTrecNJVendor.Text = Nothing
        txtJTrecNJInvNumber.Text = Nothing
        txtJTrecNJDesc.Text = Nothing
        txtJTrecNJAmount.Text = Nothing
        tmpRecKey = Nothing
    End Sub
    Private Function JTrecUpdateRecTranTotals()
        Dim tmpNJTotalAmount As Integer = 0
        Dim tmpSub As Integer = 0
        Dim tmpAcctExists As Boolean = False
        txtJTrecRecStmtTl.Text = txtJTrecStatBal.Text
        Dim tmpDiffAmt As Decimal = Val(txtJTrecRecStmtTl.Text)
        tmpDiffAmt -= Val(txtJTrecSelTotal.Text)
        tmpDiffAmt -= Val(txtJTrecNJRSelTotal.Text)
        tmpDiffAmt -= tmpNJTotalAmount
        txtJTrecDiff.Text = String.Format("{0:0.00}", tmpDiffAmt)
        If Val(txtJTrecDiff.Text) = 0 And Val(txtJTrecRecStmtTl.Text) <> 0 Then
            btnUpdate.Enabled = True
        Else
            btnUpdate.Enabled = False
        End If
        Return ""
    End Function
    Public Function JTrecCalcDueDate(tmpInvDate As String, tmpPymtTerms As String) As String
        Dim tmpDueDate As String = ""
        Select Case tmpPymtTerms
            Case "DUR"
                tmpDueDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
            Case "10Days"
                Dim tmpDate As Date = tmpInvDate
                tmpDate = tmpDate.AddDays(10)
                tmpDueDate = String.Format("{0:MM/dd/yyyy}", tmpDate)
            Case "30Days"
                Dim tmpDate As Date = mtxtJTrecStmtDate.Text
                tmpDate = tmpDate.AddDays(30)
                tmpDueDate = String.Format("{0:MM/dd/yyyy}", tmpDate)
            Case "EOM"
                Dim tmpDate As Date = mtxtJTrecDate.Text
                Dim tmpLastDayofMth As String = 30
                Select Case tmpDate.Month
                    Case 2
                        tmpLastDayofMth = 28
                        If tmpDate.Year / 4 = Int(tmpDate.Year / 4) Then
                            tmpLastDayofMth = 29
                        End If
                    Case 1, 3, 5, 7, 8, 10, 12
                        tmpLastDayofMth = 31
                End Select
                '
                tmpDate = tmpDate.AddDays(tmpLastDayofMth - tmpDate.Day)
                tmpDueDate = String.Format("{0:MM/dd/yyyy}", tmpDate)
            Case "Manual"
                tmpDueDate = ""
        End Select
        Return tmpDueDate
    End Function
    Private Function JTrecUpdtlvJTrecAcctTls(tmpAcct As String, tmpAmount As String) As String
        Dim tmpSub As Integer = 0
        If lvJTrecAcctTls.Items.Count > 0 Then
            Do While tmpSub < lvJTrecAcctTls.Items.Count
                If tmpAcct = lvJTrecAcctTls.Items(tmpSub).Text Then
                    Dim tmplvAmount As String = lvJTrecAcctTls.Items(tmpSub).SubItems(2).Text
                    lvJTrecAcctTls.Items(tmpSub).SubItems(2).Text =
                    String.Format("{0:0.00}", Val(tmplvAmount) + Val(tmpAmount))
                    Return "UPDATED"
                End If
                tmpSub += 1
            Loop
        End If
        '
        Dim tmpDesc As String = Nothing
        JTVarMaint.JTvmValidateUpdateNJREA("Validate", tmpAcct, tmpDesc)
        Dim itmBuild(3) As String
        Dim lvline As ListViewItem
        itmBuild(0) = tmpAcct
        itmBuild(1) = tmpDesc
        itmBuild(2) = tmpAmount
        lvline = New ListViewItem(itmBuild)
        lvJTrecAcctTls.Items.Add(lvline)
        Return "ADDED"
    End Function
    ''' <summary>
    ''' Sub handles selection of a NJR item on the JTrec panel. If the source of the items that
    ''' was selected is NLC (created as part of an JTnlc session), operator is direct to make 
    ''' corrections using NLC. If item was created in JTrec, item is deleted from list and fields 
    ''' in the entry section are populated with the items values.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LvJTrecNJRInvList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTrecNJRInvList.SelectedIndexChanged
        If lvJTrecNJRInvList.SelectedItems.Count > 0 Then
            If lvJTrecNJRInvList.SelectedItems(0).SubItems(6).Text = "NLC" Then
                ' saving key for potential line item correction call.
                JtrecItemCorrectionJTnlcKey = lvJTrecNJRInvList.SelectedItems(0).SubItems(7).Text

                ToolTip1.IsBalloon = False
                ToolTip1.Show("The Item Selected ---" & vbCrLf & vbCrLf &
                    "Vendor - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(2).Text & vbCrLf &
                    "Invoice # - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(3).Text & vbCrLf &
                    "Description - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(4).Text & vbCrLf &
                    "Amount - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(5).Text & vbCrLf & vbCrLf &
                    "--- was entered in NLC. Any modifications must be made in NLC." & vbCrLf &
                    "Select 'Make Line Item Corrections' to go there. (REC.007)",
                              lvJTrecNJRInvList, 615, 100, 6000)
                ToolTip1.IsBalloon = True
                '
                ' -------------------------------------------------------------------------------
                ' Changed code to use a Tooltip vs. MsgBox. Commented code can be deleted at some
                ' future date. Today's date - 7/19/2020
                ' -------------------------------------------------------------------------------
                '
                '   MsgBox("The Item Selected ---" & vbCrLf & vbCrLf &
                '          "Vendor - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(2).Text & vbCrLf &
                '          "Invoice # - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(3).Text & vbCrLf &
                '          "Description - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(4).Text & vbCrLf &
                '          "Amount - " & lvJTrecNJRInvList.SelectedItems(0).SubItems(5).Text & vbCrLf & vbCrLf &
                '          "--- was entered in NLC. Any modifications must be made in NLC." & vbCrLf &
                '          "Select 'Make Line Item Corrections' to go there. (REC.007)")
                '   btnJTrecCorrections.Focus()
            Else
                If vbYes = MsgBox("This item was entered as part of the reconciliation" & vbCrLf &
                                  "process. Do you want to modify or delete this item?" & vbCrLf &
                                  "(REC.010)", 4, "Modify NJR Item") Then
                    mtxtJTrecNJDate.Text = lvJTrecNJRInvList.SelectedItems(0).Text
                    txtJTrecNJAccount.Text = lvJTrecNJRInvList.SelectedItems(0).SubItems(1).Text
                    txtJTrecNJVendor.Text = lvJTrecNJRInvList.SelectedItems(0).SubItems(2).Text
                    txtJTrecNJInvNumber.Text = lvJTrecNJRInvList.SelectedItems(0).SubItems(3).Text
                    txtJTrecNJDesc.Text = lvJTrecNJRInvList.SelectedItems(0).SubItems(4).Text
                    txtJTrecNJAmount.Text = lvJTrecNJRInvList.SelectedItems(0).SubItems(5).Text
                    JTnlc.JTnlcDelSLRecord(lvJTrecNJRInvList.SelectedItems(0).SubItems(7).Text)       'Delete slJTnlc item
                    lvJTrecNJRInvList.SelectedItems(0).Remove()    'remove LV item
                    JTrecBldAutoCompleteNJRAccounts()
                    mtxtJTrecNJDate.Select()
                End If
            End If
        End If

    End Sub
    '
    Private Sub LvJTrecData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTrecData.SelectedIndexChanged
        If lvJTrecData.SelectedItems.Count > 0 Then
            Dim tmpCategory As String = ""
            Select Case lvJTrecData.SelectedItems(0).SubItems(5).Text
                Case "M"
                    tmpCategory = "Material"
                Case "S"
                    tmpCategory = "Service"
            End Select
            Dim tmpSelectedRowNumber As Integer = lvJTrecData.SelectedItems(0).Index
            Dim tmpKeys As ICollection = slJTrecKey.Keys
            JtrecItemCorrectionJTnlcKey = slJTrecKey(tmpKeys(tmpSelectedRowNumber))
            ToolTip1.IsBalloon = False
            ToolTip1.Show("Item Details ---" & vbCrLf & "Transaction Category - " & tmpCategory & vbCrLf &
                              "Customer - " & lvJTrecData.SelectedItems(0).SubItems(6).Text & vbCrLf &
                              "Job - " & lvJTrecData.SelectedItems(0).SubItems(7).Text & vbCrLf &
                              "Invoiced on - " & lvJTrecData.SelectedItems(0).SubItems(8).Text,
                              lvJTrecData, 615, 100, 6000)
            ToolTip1.IsBalloon = True
        End If
    End Sub
    ''' <summary>
    ''' Function builds auto complete table for JTrec function for NJR accounts.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTrecBldAutoCompleteNJRAccounts() As String
        Dim JTrecNJRAccountList As New AutoCompleteStringCollection
        Dim tmpSub As Integer = 0
        Dim tmpAccount As String = Nothing
        Dim tmpDesc As String = Nothing
        Do While JTVarMaint.JTvmReadNJREA(tmpSub, tmpAccount, tmpDesc) = "DATA"
            JTrecNJRAccountList.Add(tmpAccount)
            tmpSub += 1
        Loop
        '
        txtJTrecNJAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        txtJTrecNJAccount.AutoCompleteSource = AutoCompleteSource.CustomSource
        txtJTrecNJAccount.AutoCompleteCustomSource = JTrecNJRAccountList
        Return ""
    End Function
    ''' <summary>
    ''' Function called as part of the btnUpdate logic. It's job is to send names of reconciled items
    ''' back to JTvi so the suggestion field can be updated.
    ''' </summary>
    ''' <returns></returns>
    Private Function JTrecUpdtJTviReconSuggest() As Boolean
        Dim tmpSub As Integer = 0
        Dim tmpMode As String = "UPDATE"
        Do While tmpSub < lvJTrecData.CheckedItems.Count
            Dim tmpSpplrName As String = lvJTrecData.CheckedItems(tmpSub).SubItems(1).Text
            JTvi.JTviReconSuggest(tmpMode, tmpSpplrName, txtJTrecVndShrtName.Text)
            tmpSub += 1
        Loop
        tmpSub = 0
        Do While tmpSub < lvJTrecNJRInvList.CheckedItems.Count
            Dim tmpSpplrName As String = lvJTrecNJRInvList.CheckedItems(tmpSub).SubItems(2).Text
            JTvi.JTviReconSuggest(tmpMode, tmpSpplrName, txtJTrecVndShrtName.Text)
            lvJTrecNJRInvList.Items(tmpSub).Checked = True
            tmpSub += 1
        Loop
        Return True
    End Function

End Class