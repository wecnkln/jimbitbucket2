﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JThr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtJThrID = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkbJThrWAcustcomm = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAcustimage = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAvend = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAlabor = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAtkMgmt = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAcust = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWAtk = New System.Windows.Forms.CheckBox()
        Me.chkbJThrWABlog = New System.Windows.Forms.CheckBox()
        Me.btnTerminate = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtJThrRate = New System.Windows.Forms.TextBox()
        Me.txtJThrEmail = New System.Windows.Forms.TextBox()
        Me.txtJThrPRID = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.radiobtn1099 = New System.Windows.Forms.RadioButton()
        Me.radiobtnW2 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnChange = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtJThrJobCat = New System.Windows.Forms.TextBox()
        Me.txtJThrEmpName = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnDone = New System.Windows.Forms.Button()
        Me.HRPayMeth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HRLaborCat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HRName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HRID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtErrorMsg = New System.Windows.Forms.TextBox()
        Me.JThrListView = New System.Windows.Forms.ListView()
        Me.HREmpID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HRPayRate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.HREmail = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TermDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.WebAuth = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PendWebUpdt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(532, 5)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(369, 64)
        Me.TextBox1.TabIndex = 22
        Me.TextBox1.Text = "Double Click entry for 'Change' or  'Delete'. Enter data and hit 'Add' for new em" &
    "ployee. Enter 'Done' to save and exit. 'Cancel"" will erase all changes made in t" &
    "his session."
        Me.TextBox1.UseWaitCursor = True
        '
        'txtJThrID
        '
        Me.txtJThrID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJThrID.Location = New System.Drawing.Point(209, 11)
        Me.txtJThrID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrID.Name = "txtJThrID"
        Me.txtJThrID.Size = New System.Drawing.Size(75, 22)
        Me.txtJThrID.TabIndex = 0
        Me.txtJThrID.UseWaitCursor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 109)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Pay Method"
        Me.Label5.UseWaitCursor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.btnTerminate)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtJThrRate)
        Me.GroupBox1.Controls.Add(Me.txtJThrEmail)
        Me.GroupBox1.Controls.Add(Me.txtJThrPRID)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtJThrID)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnDelete)
        Me.GroupBox1.Controls.Add(Me.btnChange)
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.txtJThrJobCat)
        Me.GroupBox1.Controls.Add(Me.txtJThrEmpName)
        Me.GroupBox1.Location = New System.Drawing.Point(909, 5)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(367, 437)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.UseWaitCursor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAcustcomm)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAcustimage)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAvend)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAlabor)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAtkMgmt)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAcust)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWAtk)
        Me.GroupBox3.Controls.Add(Me.chkbJThrWABlog)
        Me.GroupBox3.Location = New System.Drawing.Point(24, 238)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(322, 131)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Web Portal Authorizations"
        Me.GroupBox3.UseWaitCursor = True
        '
        'chkbJThrWAcustcomm
        '
        Me.chkbJThrWAcustcomm.AutoSize = True
        Me.chkbJThrWAcustcomm.Location = New System.Drawing.Point(140, 48)
        Me.chkbJThrWAcustcomm.Name = "chkbJThrWAcustcomm"
        Me.chkbJThrWAcustcomm.Size = New System.Drawing.Size(158, 21)
        Me.chkbJThrWAcustcomm.TabIndex = 8
        Me.chkbJThrWAcustcomm.Text = "Cust Communication"
        Me.chkbJThrWAcustcomm.UseVisualStyleBackColor = True
        Me.chkbJThrWAcustcomm.UseWaitCursor = True
        '
        'chkbJThrWAcustimage
        '
        Me.chkbJThrWAcustimage.AutoSize = True
        Me.chkbJThrWAcustimage.Location = New System.Drawing.Point(140, 71)
        Me.chkbJThrWAcustimage.Name = "chkbJThrWAcustimage"
        Me.chkbJThrWAcustimage.Size = New System.Drawing.Size(107, 21)
        Me.chkbJThrWAcustimage.TabIndex = 7
        Me.chkbJThrWAcustimage.Text = "Cust Images"
        Me.chkbJThrWAcustimage.UseVisualStyleBackColor = True
        Me.chkbJThrWAcustimage.UseWaitCursor = True
        '
        'chkbJThrWAvend
        '
        Me.chkbJThrWAvend.AutoSize = True
        Me.chkbJThrWAvend.Location = New System.Drawing.Point(10, 95)
        Me.chkbJThrWAvend.Name = "chkbJThrWAvend"
        Me.chkbJThrWAvend.Size = New System.Drawing.Size(125, 21)
        Me.chkbJThrWAvend.TabIndex = 6
        Me.chkbJThrWAvend.Text = "Vendor Inv Info"
        Me.chkbJThrWAvend.UseVisualStyleBackColor = True
        Me.chkbJThrWAvend.UseWaitCursor = True
        '
        'chkbJThrWAlabor
        '
        Me.chkbJThrWAlabor.AutoSize = True
        Me.chkbJThrWAlabor.Location = New System.Drawing.Point(140, 95)
        Me.chkbJThrWAlabor.Name = "chkbJThrWAlabor"
        Me.chkbJThrWAlabor.Size = New System.Drawing.Size(134, 21)
        Me.chkbJThrWAlabor.TabIndex = 5
        Me.chkbJThrWAlabor.Text = "Labor Detail Info"
        Me.chkbJThrWAlabor.UseVisualStyleBackColor = True
        Me.chkbJThrWAlabor.UseWaitCursor = True
        '
        'chkbJThrWAtkMgmt
        '
        Me.chkbJThrWAtkMgmt.AutoSize = True
        Me.chkbJThrWAtkMgmt.Location = New System.Drawing.Point(10, 71)
        Me.chkbJThrWAtkMgmt.Name = "chkbJThrWAtkMgmt"
        Me.chkbJThrWAtkMgmt.Size = New System.Drawing.Size(86, 21)
        Me.chkbJThrWAtkMgmt.TabIndex = 4
        Me.chkbJThrWAtkMgmt.Text = "TK Mgmt"
        Me.chkbJThrWAtkMgmt.UseVisualStyleBackColor = True
        Me.chkbJThrWAtkMgmt.UseWaitCursor = True
        '
        'chkbJThrWAcust
        '
        Me.chkbJThrWAcust.AutoSize = True
        Me.chkbJThrWAcust.Location = New System.Drawing.Point(140, 25)
        Me.chkbJThrWAcust.Name = "chkbJThrWAcust"
        Me.chkbJThrWAcust.Size = New System.Drawing.Size(133, 21)
        Me.chkbJThrWAcust.TabIndex = 3
        Me.chkbJThrWAcust.Text = "Cust Invoice Info"
        Me.chkbJThrWAcust.UseVisualStyleBackColor = True
        Me.chkbJThrWAcust.UseWaitCursor = True
        '
        'chkbJThrWAtk
        '
        Me.chkbJThrWAtk.AutoSize = True
        Me.chkbJThrWAtk.Location = New System.Drawing.Point(10, 48)
        Me.chkbJThrWAtk.Name = "chkbJThrWAtk"
        Me.chkbJThrWAtk.Size = New System.Drawing.Size(111, 21)
        Me.chkbJThrWAtk.TabIndex = 1
        Me.chkbJThrWAtk.Text = "Timekeeping"
        Me.chkbJThrWAtk.UseVisualStyleBackColor = True
        Me.chkbJThrWAtk.UseWaitCursor = True
        '
        'chkbJThrWABlog
        '
        Me.chkbJThrWABlog.AutoSize = True
        Me.chkbJThrWABlog.Location = New System.Drawing.Point(10, 26)
        Me.chkbJThrWABlog.Name = "chkbJThrWABlog"
        Me.chkbJThrWABlog.Size = New System.Drawing.Size(109, 21)
        Me.chkbJThrWABlog.TabIndex = 0
        Me.chkbJThrWABlog.Text = "Internal Blog"
        Me.chkbJThrWABlog.UseVisualStyleBackColor = True
        Me.chkbJThrWABlog.UseWaitCursor = True
        '
        'btnTerminate
        '
        Me.btnTerminate.Location = New System.Drawing.Point(260, 399)
        Me.btnTerminate.Margin = New System.Windows.Forms.Padding(4)
        Me.btnTerminate.Name = "btnTerminate"
        Me.btnTerminate.Size = New System.Drawing.Size(99, 27)
        Me.btnTerminate.TabIndex = 25
        Me.btnTerminate.Text = "Terminate"
        Me.btnTerminate.UseVisualStyleBackColor = True
        Me.btnTerminate.UseWaitCursor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(21, 148)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 17)
        Me.Label8.TabIndex = 24
        Me.Label8.Text = "Emp. P/R ID #"
        Me.Label8.UseWaitCursor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(21, 178)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 17)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Pay Rate"
        Me.Label7.UseWaitCursor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(21, 208)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 17)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Emp. Email"
        Me.Label6.UseWaitCursor = True
        '
        'txtJThrRate
        '
        Me.txtJThrRate.Location = New System.Drawing.Point(209, 173)
        Me.txtJThrRate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrRate.Name = "txtJThrRate"
        Me.txtJThrRate.Size = New System.Drawing.Size(75, 22)
        Me.txtJThrRate.TabIndex = 5
        Me.txtJThrRate.UseWaitCursor = True
        '
        'txtJThrEmail
        '
        Me.txtJThrEmail.Location = New System.Drawing.Point(155, 203)
        Me.txtJThrEmail.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrEmail.Name = "txtJThrEmail"
        Me.txtJThrEmail.Size = New System.Drawing.Size(191, 22)
        Me.txtJThrEmail.TabIndex = 6
        Me.txtJThrEmail.UseWaitCursor = True
        '
        'txtJThrPRID
        '
        Me.txtJThrPRID.Location = New System.Drawing.Point(187, 143)
        Me.txtJThrPRID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrPRID.Name = "txtJThrPRID"
        Me.txtJThrPRID.Size = New System.Drawing.Size(111, 22)
        Me.txtJThrPRID.TabIndex = 4
        Me.txtJThrPRID.UseWaitCursor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox2.Controls.Add(Me.radiobtn1099)
        Me.GroupBox2.Controls.Add(Me.radiobtnW2)
        Me.GroupBox2.Location = New System.Drawing.Point(169, 101)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(171, 34)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.UseWaitCursor = True
        '
        'radiobtn1099
        '
        Me.radiobtn1099.AutoSize = True
        Me.radiobtn1099.Location = New System.Drawing.Point(95, 6)
        Me.radiobtn1099.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtn1099.Name = "radiobtn1099"
        Me.radiobtn1099.Size = New System.Drawing.Size(61, 21)
        Me.radiobtn1099.TabIndex = 17
        Me.radiobtn1099.TabStop = True
        Me.radiobtn1099.Text = "1099"
        Me.radiobtn1099.UseVisualStyleBackColor = True
        Me.radiobtn1099.UseWaitCursor = True
        '
        'radiobtnW2
        '
        Me.radiobtnW2.AutoSize = True
        Me.radiobtnW2.Location = New System.Drawing.Point(13, 6)
        Me.radiobtnW2.Margin = New System.Windows.Forms.Padding(4)
        Me.radiobtnW2.Name = "radiobtnW2"
        Me.radiobtnW2.Size = New System.Drawing.Size(50, 21)
        Me.radiobtnW2.TabIndex = 16
        Me.radiobtnW2.TabStop = True
        Me.radiobtnW2.Text = "W2"
        Me.radiobtnW2.UseVisualStyleBackColor = True
        Me.radiobtnW2.UseWaitCursor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 76)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Labor Category"
        Me.Label4.UseWaitCursor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 46)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 17)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Name"
        Me.Label3.UseWaitCursor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 14)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Worker ID"
        Me.Label2.UseWaitCursor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(187, 400)
        Me.btnDelete.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(65, 27)
        Me.btnDelete.TabIndex = 9
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.UseWaitCursor = True
        '
        'btnChange
        '
        Me.btnChange.Location = New System.Drawing.Point(103, 399)
        Me.btnChange.Margin = New System.Windows.Forms.Padding(4)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(70, 28)
        Me.btnChange.TabIndex = 8
        Me.btnChange.Text = "Change"
        Me.btnChange.UseVisualStyleBackColor = True
        Me.btnChange.UseWaitCursor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(10, 398)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 28)
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        Me.btnAdd.UseWaitCursor = True
        '
        'txtJThrJobCat
        '
        Me.txtJThrJobCat.Location = New System.Drawing.Point(209, 71)
        Me.txtJThrJobCat.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrJobCat.Name = "txtJThrJobCat"
        Me.txtJThrJobCat.Size = New System.Drawing.Size(75, 22)
        Me.txtJThrJobCat.TabIndex = 2
        Me.txtJThrJobCat.UseWaitCursor = True
        '
        'txtJThrEmpName
        '
        Me.txtJThrEmpName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtJThrEmpName.Location = New System.Drawing.Point(155, 41)
        Me.txtJThrEmpName.Margin = New System.Windows.Forms.Padding(4)
        Me.txtJThrEmpName.Name = "txtJThrEmpName"
        Me.txtJThrEmpName.Size = New System.Drawing.Size(191, 22)
        Me.txtJThrEmpName.TabIndex = 1
        Me.txtJThrEmpName.UseWaitCursor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(136, 400)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(107, 43)
        Me.btnCancel.TabIndex = 19
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.UseWaitCursor = True
        '
        'btnDone
        '
        Me.btnDone.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDone.Location = New System.Drawing.Point(9, 400)
        Me.btnDone.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(115, 42)
        Me.btnDone.TabIndex = 18
        Me.btnDone.Text = "Done"
        Me.btnDone.UseVisualStyleBackColor = True
        Me.btnDone.UseWaitCursor = True
        '
        'HRPayMeth
        '
        Me.HRPayMeth.Text = "Pay Method"
        Me.HRPayMeth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.HRPayMeth.Width = 70
        '
        'HRLaborCat
        '
        Me.HRLaborCat.Text = "Labor Cat"
        Me.HRLaborCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.HRLaborCat.Width = 70
        '
        'HRName
        '
        Me.HRName.Text = "Name"
        Me.HRName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.HRName.Width = 150
        '
        'HRID
        '
        Me.HRID.Text = "       ID"
        Me.HRID.Width = 80
        '
        'txtErrorMsg
        '
        Me.txtErrorMsg.Location = New System.Drawing.Point(263, 394)
        Me.txtErrorMsg.Margin = New System.Windows.Forms.Padding(4)
        Me.txtErrorMsg.Multiline = True
        Me.txtErrorMsg.Name = "txtErrorMsg"
        Me.txtErrorMsg.ReadOnly = True
        Me.txtErrorMsg.Size = New System.Drawing.Size(476, 48)
        Me.txtErrorMsg.TabIndex = 21
        Me.txtErrorMsg.UseWaitCursor = True
        '
        'JThrListView
        '
        Me.JThrListView.BackColor = System.Drawing.SystemColors.Window
        Me.JThrListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.HRID, Me.HRName, Me.HRLaborCat, Me.HRPayMeth, Me.HREmpID, Me.HRPayRate, Me.HREmail, Me.TermDate, Me.WebAuth, Me.PendWebUpdt})
        Me.JThrListView.FullRowSelect = True
        Me.JThrListView.GridLines = True
        Me.JThrListView.HideSelection = False
        Me.JThrListView.Location = New System.Drawing.Point(13, 77)
        Me.JThrListView.Margin = New System.Windows.Forms.Padding(4)
        Me.JThrListView.MultiSelect = False
        Me.JThrListView.Name = "JThrListView"
        Me.JThrListView.Size = New System.Drawing.Size(888, 307)
        Me.JThrListView.TabIndex = 17
        Me.JThrListView.UseCompatibleStateImageBehavior = False
        Me.JThrListView.UseWaitCursor = True
        Me.JThrListView.View = System.Windows.Forms.View.Details
        '
        'HREmpID
        '
        Me.HREmpID.Text = "P/R ID #"
        Me.HREmpID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.HREmpID.Width = 90
        '
        'HRPayRate
        '
        Me.HRPayRate.Text = "Rate"
        Me.HRPayRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.HRPayRate.Width = 80
        '
        'HREmail
        '
        Me.HREmail.Text = "Email Address"
        Me.HREmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.HREmail.Width = 140
        '
        'TermDate
        '
        Me.TermDate.Text = "Terminated"
        Me.TermDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TermDate.Width = 85
        '
        'WebAuth
        '
        Me.WebAuth.Text = "Web Auth (BTVC)"
        Me.WebAuth.Width = 80
        '
        'PendWebUpdt
        '
        Me.PendWebUpdt.Width = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(368, 29)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Human Resource Maintenance"
        Me.Label1.UseWaitCursor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'JThr
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1287, 457)
        Me.ControlBox = False
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.txtErrorMsg)
        Me.Controls.Add(Me.JThrListView)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "JThr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Human Resource Management"
        Me.TopMost = True
        Me.UseWaitCursor = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents txtJThrID As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnDelete As Button
    Friend WithEvents btnChange As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents txtJThrJobCat As TextBox
    Friend WithEvents txtJThrEmpName As TextBox
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnDone As Button
    Friend WithEvents HRPayMeth As ColumnHeader
    Friend WithEvents HRLaborCat As ColumnHeader
    Friend WithEvents HRName As ColumnHeader
    Friend WithEvents HRID As ColumnHeader
    Friend WithEvents txtErrorMsg As TextBox
    Friend WithEvents JThrListView As ListView
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents radiobtn1099 As RadioButton
    Friend WithEvents radiobtnW2 As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtJThrRate As TextBox
    Friend WithEvents txtJThrEmail As TextBox
    Friend WithEvents txtJThrPRID As TextBox
    Friend WithEvents HREmpID As ColumnHeader
    Friend WithEvents HRPayRate As ColumnHeader
    Friend WithEvents HREmail As ColumnHeader
    Friend WithEvents TermDate As ColumnHeader
    Friend WithEvents btnTerminate As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents chkbJThrWAcust As CheckBox
    Friend WithEvents chkbJThrWAtk As CheckBox
    Friend WithEvents chkbJThrWABlog As CheckBox
    Friend WithEvents WebAuth As ColumnHeader
    Friend WithEvents chkbJThrWAlabor As CheckBox
    Friend WithEvents chkbJThrWAtkMgmt As CheckBox
    Friend WithEvents chkbJThrWAcustcomm As CheckBox
    Friend WithEvents chkbJThrWAcustimage As CheckBox
    Friend WithEvents chkbJThrWAvend As CheckBox
    Friend WithEvents PendWebUpdt As ColumnHeader
End Class
