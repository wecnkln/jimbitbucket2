﻿''' <summary>
''' This class builds a panel that documents if there are gaps in numbers on the
''' invoice pdf files. If there are gaps, this shows an issue. This class will fix 
''' the issue but it will point it out.
''' 
''' The panel will display if there are gaps.
''' </summary>
Public Class JTInvSeq
    ''' <summary>
    ''' Function that will gather invoice number from the PDf file names and check to see if there are gaps
    ''' in the file sequence, This check is to trap information for a problem that may exists related
    ''' to missing invoice PDFs.
    ''' 
    ''' ===============================================================================================
    ''' The call to this routine has been commented out. Noone seemed to be looking at the panel. The
    ''' call to make it active again is in JTas ~2021.
    ''' ===============================================================================================
    ''' 
    ''' </summary>
    ''' <returns></returns>
    Public Function JTInvSeqCheckInvoicePDFSeq() As Boolean
        Dim tmpYear As String = Date.Today.Year
        Dim tmpInvNumber As String = Nothing
        Dim InvNumList As New List(Of String)
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(JTVarMaint.JTvmFilePathDoc &
                                       "CustInvoicePDF\" & tmpYear & "\")
            '
            If foundFile.Substring(foundFile.Length - 12, 12).ToUpper = "D_VOIDED.PDF" Then
                tmpInvNumber = foundFile.Substring(foundFile.Length - 16, 4)
            Else
                If foundFile.Substring(foundFile.Length - 5, 5).ToUpper = "D.PDF" Then
                    tmpInvNumber = foundFile.Substring(foundFile.Length - 9, 4)
                End If
            End If
            If IsNumeric(tmpInvNumber) = True Then
                Dim tmpTrackIt As Boolean = False
                If tmpInvNumber <> Nothing Then
                    If tmpInvNumber < "8000" Then
                        If tmpInvNumber > "1500" Then          'starting invoice number to track sequence
                            tmpTrackIt = True
                        End If
                    Else
                        If tmpInvNumber > "8092" Then          'starting 8000 series invoice number to track
                            tmpTrackIt = True
                        End If
                    End If
                End If
                If tmpTrackIt = True Then
                    InvNumList.Add(tmpInvNumber)
                End If
            End If
        Next
        '
        Dim tmpGaps As Boolean = False
        ' ---------------------------------------------------------
        ' Check to see if invoices were extracted before looking
        ' For gaps -- starting numbers could be set too high. Abort
        ' routine if nothing extracted.
        ' ---------------------------------------------------------
        If InvNumList.Count > 0 Then
            InvNumList.Sort()
            txtJTInvSeqList.Text = Nothing

            Dim tmpSub As Integer = 0
            Do While tmpSub < InvNumList.Count - 1
                txtJTInvSeqList.Text = txtJTInvSeqList.Text & InvNumList(tmpSub) & vbCrLf
                If Val(InvNumList(tmpSub)) + 1 <> Val(InvNumList(tmpSub + 1)) Then
                    If Val(InvNumList(tmpSub + 1)) > 8000 And (InvNumList(tmpSub)) < 8000 Then
                        txtJTInvSeqList.Text = txtJTInvSeqList.Text & "   <==== 8000 sequence start ====>" & vbCrLf
                    Else
                        txtJTInvSeqList.Text = txtJTInvSeqList.Text & "   <==== Gap in number sequence" & vbCrLf
                        tmpGaps = True
                    End If
                End If
                tmpSub += 1
            Loop
            txtJTInvSeqList.Text = txtJTInvSeqList.Text & InvNumList(tmpSub)
            txtJTInvSeqList.Visible = True
            If tmpGaps = True Then
                Me.MdiParent = JTstart
                Me.Show()
            End If
            '
        End If
        Return tmpGaps
    End Function

    Private Sub btnJTInvSeqExit_Click(sender As Object, e As EventArgs) Handles btnJTInvSeqExit.Click
        Me.Hide()
        '      JTstart.JTstartExitSysCont2()
        Me.Close()
    End Sub
    '
End Class