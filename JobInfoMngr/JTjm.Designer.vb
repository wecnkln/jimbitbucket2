﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTjm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTjm))
        Me.JTjmNewCustLabel = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtJTjmNewJob = New System.Windows.Forms.TextBox()
        Me.txtJTjmNewCust = New System.Windows.Forms.TextBox()
        Me.txtJTjmExCust = New System.Windows.Forms.TextBox()
        Me.JTjmExJobLabel = New System.Windows.Forms.Label()
        Me.JTjmNewJobLabel = New System.Windows.Forms.Label()
        Me.JTjmExLabel = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtJTjmExJob = New System.Windows.Forms.TextBox()
        Me.JTjmNewLabel = New System.Windows.Forms.Label()
        Me.btnJTjmAction = New System.Windows.Forms.Button()
        Me.btnJTjmDone = New System.Windows.Forms.Button()
        Me.txtJTjmComment = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.rbtnJTjmRename = New System.Windows.Forms.RadioButton()
        Me.rbtnJTjmCustNameChg = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnJTjmCancel = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'JTjmNewCustLabel
        '
        Me.JTjmNewCustLabel.AutoSize = True
        Me.JTjmNewCustLabel.Location = New System.Drawing.Point(85, 66)
        Me.JTjmNewCustLabel.Name = "JTjmNewCustLabel"
        Me.JTjmNewCustLabel.Size = New System.Drawing.Size(68, 17)
        Me.JTjmNewCustLabel.TabIndex = 9
        Me.JTjmNewCustLabel.Text = "Customer"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(85, 31)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 17)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Customer"
        '
        'txtJTjmNewJob
        '
        Me.txtJTjmNewJob.Location = New System.Drawing.Point(327, 63)
        Me.txtJTjmNewJob.Name = "txtJTjmNewJob"
        Me.txtJTjmNewJob.Size = New System.Drawing.Size(139, 22)
        Me.txtJTjmNewJob.TabIndex = 3
        Me.txtJTjmNewJob.TabStop = False
        '
        'txtJTjmNewCust
        '
        Me.txtJTjmNewCust.Location = New System.Drawing.Point(163, 63)
        Me.txtJTjmNewCust.Name = "txtJTjmNewCust"
        Me.txtJTjmNewCust.Size = New System.Drawing.Size(120, 22)
        Me.txtJTjmNewCust.TabIndex = 2
        Me.txtJTjmNewCust.TabStop = False
        '
        'txtJTjmExCust
        '
        Me.txtJTjmExCust.Location = New System.Drawing.Point(164, 26)
        Me.txtJTjmExCust.Name = "txtJTjmExCust"
        Me.txtJTjmExCust.Size = New System.Drawing.Size(120, 22)
        Me.txtJTjmExCust.TabIndex = 4
        Me.txtJTjmExCust.TabStop = False
        '
        'JTjmExJobLabel
        '
        Me.JTjmExJobLabel.AutoSize = True
        Me.JTjmExJobLabel.Location = New System.Drawing.Point(290, 31)
        Me.JTjmExJobLabel.Name = "JTjmExJobLabel"
        Me.JTjmExJobLabel.Size = New System.Drawing.Size(31, 17)
        Me.JTjmExJobLabel.TabIndex = 3
        Me.JTjmExJobLabel.Text = "Job"
        '
        'JTjmNewJobLabel
        '
        Me.JTjmNewJobLabel.AutoSize = True
        Me.JTjmNewJobLabel.Location = New System.Drawing.Point(290, 66)
        Me.JTjmNewJobLabel.Name = "JTjmNewJobLabel"
        Me.JTjmNewJobLabel.Size = New System.Drawing.Size(31, 17)
        Me.JTjmNewJobLabel.TabIndex = 2
        Me.JTjmNewJobLabel.Text = "Job"
        '
        'JTjmExLabel
        '
        Me.JTjmExLabel.AutoSize = True
        Me.JTjmExLabel.Location = New System.Drawing.Point(11, 31)
        Me.JTjmExLabel.Name = "JTjmExLabel"
        Me.JTjmExLabel.Size = New System.Drawing.Size(56, 17)
        Me.JTjmExLabel.TabIndex = 5
        Me.JTjmExLabel.Text = "Existing"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Controls.Add(Me.txtJTjmExJob)
        Me.GroupBox2.Controls.Add(Me.JTjmNewCustLabel)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtJTjmNewJob)
        Me.GroupBox2.Controls.Add(Me.txtJTjmNewCust)
        Me.GroupBox2.Controls.Add(Me.txtJTjmExCust)
        Me.GroupBox2.Controls.Add(Me.JTjmExJobLabel)
        Me.GroupBox2.Controls.Add(Me.JTjmNewJobLabel)
        Me.GroupBox2.Controls.Add(Me.JTjmNewLabel)
        Me.GroupBox2.Controls.Add(Me.JTjmExLabel)
        Me.GroupBox2.Location = New System.Drawing.Point(29, 180)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(472, 117)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Job Name Info"
        '
        'txtJTjmExJob
        '
        Me.txtJTjmExJob.Location = New System.Drawing.Point(327, 26)
        Me.txtJTjmExJob.Name = "txtJTjmExJob"
        Me.txtJTjmExJob.Size = New System.Drawing.Size(139, 22)
        Me.txtJTjmExJob.TabIndex = 10
        Me.txtJTjmExJob.TabStop = False
        '
        'JTjmNewLabel
        '
        Me.JTjmNewLabel.AutoSize = True
        Me.JTjmNewLabel.Location = New System.Drawing.Point(11, 66)
        Me.JTjmNewLabel.Name = "JTjmNewLabel"
        Me.JTjmNewLabel.Size = New System.Drawing.Size(39, 17)
        Me.JTjmNewLabel.TabIndex = 1
        Me.JTjmNewLabel.Text = "New "
        '
        'btnJTjmAction
        '
        Me.btnJTjmAction.Location = New System.Drawing.Point(109, 315)
        Me.btnJTjmAction.Name = "btnJTjmAction"
        Me.btnJTjmAction.Size = New System.Drawing.Size(143, 37)
        Me.btnJTjmAction.TabIndex = 0
        Me.btnJTjmAction.Text = "Action"
        Me.btnJTjmAction.UseVisualStyleBackColor = True
        '
        'btnJTjmDone
        '
        Me.btnJTjmDone.Location = New System.Drawing.Point(192, 496)
        Me.btnJTjmDone.Name = "btnJTjmDone"
        Me.btnJTjmDone.Size = New System.Drawing.Size(143, 37)
        Me.btnJTjmDone.TabIndex = 1
        Me.btnJTjmDone.Text = "Done/Exit"
        Me.btnJTjmDone.UseVisualStyleBackColor = True
        '
        'txtJTjmComment
        '
        Me.txtJTjmComment.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.txtJTjmComment.Location = New System.Drawing.Point(56, 371)
        Me.txtJTjmComment.Multiline = True
        Me.txtJTjmComment.Name = "txtJTjmComment"
        Me.txtJTjmComment.ReadOnly = True
        Me.txtJTjmComment.Size = New System.Drawing.Size(404, 101)
        Me.txtJTjmComment.TabIndex = 12
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBox2.Location = New System.Drawing.Point(556, 315)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(404, 218)
        Me.TextBox2.TabIndex = 11
        Me.TextBox2.Text = resources.GetString("TextBox2.Text")
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBox1.Location = New System.Drawing.Point(556, 79)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(404, 218)
        Me.TextBox1.TabIndex = 10
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'rbtnJTjmRename
        '
        Me.rbtnJTjmRename.AutoSize = True
        Me.rbtnJTjmRename.Location = New System.Drawing.Point(28, 61)
        Me.rbtnJTjmRename.Name = "rbtnJTjmRename"
        Me.rbtnJTjmRename.Size = New System.Drawing.Size(173, 21)
        Me.rbtnJTjmRename.TabIndex = 1
        Me.rbtnJTjmRename.TabStop = True
        Me.rbtnJTjmRename.Text = "Rename Customer/Job"
        Me.rbtnJTjmRename.UseVisualStyleBackColor = True
        '
        'rbtnJTjmCustNameChg
        '
        Me.rbtnJTjmCustNameChg.AutoSize = True
        Me.rbtnJTjmCustNameChg.Location = New System.Drawing.Point(28, 34)
        Me.rbtnJTjmCustNameChg.Name = "rbtnJTjmCustNameChg"
        Me.rbtnJTjmCustNameChg.Size = New System.Drawing.Size(183, 21)
        Me.rbtnJTjmCustNameChg.TabIndex = 0
        Me.rbtnJTjmCustNameChg.TabStop = True
        Me.rbtnJTjmCustNameChg.Text = "Change Customer Name"
        Me.rbtnJTjmCustNameChg.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox1.Controls.Add(Me.rbtnJTjmRename)
        Me.GroupBox1.Controls.Add(Me.rbtnJTjmCustNameChg)
        Me.GroupBox1.Location = New System.Drawing.Point(128, 53)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(269, 102)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Action to be Executed"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(334, 25)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Customer/Job NameMaintenance"
        '
        'btnJTjmCancel
        '
        Me.btnJTjmCancel.Location = New System.Drawing.Point(275, 315)
        Me.btnJTjmCancel.Name = "btnJTjmCancel"
        Me.btnJTjmCancel.Size = New System.Drawing.Size(143, 37)
        Me.btnJTjmCancel.TabIndex = 4
        Me.btnJTjmCancel.Text = "Cancel Action"
        Me.btnJTjmCancel.UseVisualStyleBackColor = True
        '
        'JTjm
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1006, 573)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJTjmCancel)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnJTjmAction)
        Me.Controls.Add(Me.btnJTjmDone)
        Me.Controls.Add(Me.txtJTjmComment)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "JTjm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Informastion Manager -Customer/Job Name Maintenance"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents JTjmNewCustLabel As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtJTjmNewJob As TextBox
    Friend WithEvents txtJTjmNewCust As TextBox
    Friend WithEvents txtJTjmExCust As TextBox
    Friend WithEvents JTjmExJobLabel As Label
    Friend WithEvents JTjmNewJobLabel As Label
    Friend WithEvents JTjmExLabel As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents JTjmNewLabel As Label
    Friend WithEvents btnJTjmAction As Button
    Friend WithEvents btnJTjmDone As Button
    Friend WithEvents txtJTjmComment As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents rbtnJTjmRename As RadioButton
    Friend WithEvents rbtnJTjmCustNameChg As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents btnJTjmCancel As Button
    Friend WithEvents txtJTjmExJob As TextBox
End Class
