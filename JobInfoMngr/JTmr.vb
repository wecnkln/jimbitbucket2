﻿Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Module JTmr
    Public JTmrReportHeader As String = Nothing
    Public Sub JTmrInit(tmpReportType As String)
        Select Case tmpReportType
            Case "NJRA"
                JTmrReportHeader = "NON-JOB-RELATED EXPENSE ACCOUNT LISTING"
            Case "CUSTJOB"
                JTmrReportHeader = "CUSTOMER/JOB LISTING"
            Case "VENDOR"
                JTmrReportHeader = "VENDOR LISTING"
        End Select
        JTmrITextInit(tmpReportType)

    End Sub

    Private Function JTmrITextInit(tmpReportType As String) As String

        Dim filename As String = JTVarMaint.JTvmFilePath & "JtTmpRprtFile" & ".PDF"
        Dim filenameWM As String = JTVarMaint.JTvmFilePath & "JtTmpRprtFileWM" & ".PDF"
        '
        Try
            Using fs As System.IO.FileStream = New FileStream(filename, FileMode.Create)
                '
                Dim myDoc As Document
                If tmpReportType = "VENDOR" Or tmpReportType = "CUSTJOB" Then
                    myDoc = New Document(iTextSharp.text.PageSize.LETTER.Rotate, 72, 72, 72, 72)
                Else
                    myDoc = New Document(iTextSharp.text.PageSize.LETTER, 72, 72, 72, 72)
                End If
                Dim Writer As PdfWriter = PdfWriter.GetInstance(myDoc, fs)
                Dim ev As New JTmritsEvents

                Writer.PageEvent = ev
                '
                '
                '
                myDoc.AddTitle("JIM Miscellaneous Report")
                myDoc.Open()
                '
                '
                JTmrBldRprtHeader(myDoc)      ' Build report headings
                '
                Select Case tmpReportType
                    Case "NJRA"
                        JTmrBldAcctList(myDoc)
                    Case "CUSTJOB"
                        JTmrBldCustJob(myDoc)
                    Case "VENDOR"
                        JTmrBldVendor(myDoc)
                End Select
                '
                '
                '
                '
                Dim JIMwhtFooter As New Phrase(FP_H6Blue(vbCrLf & "Information managed and reports produced by Job Information Manager. For information email info@watchhilltech.com"))
                myDoc.Add(JIMwhtFooter)
                '

                myDoc.Close()
                '
                fs.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        JTvPDF.JTvPDFDispPDF(filename)
        '
        Return ""
    End Function
    ''' <summary>
    ''' Function builds the report headers for the Several Reports
    ''' </summary>
    ''' <param name="myDoc"></param>
    ''' <returns></returns>
    Private Function JTmrBldRprtHeader(ByRef myDoc As Document) As String

        ' ----------------------------------------------------------------
        ' Generate PDF Invoice Header section..
        ' ----------------------------------------------------------------

        ' ----------------------------------------------------------
        ' table structure to hold logo and date
        ' ----------------------------------------------------------
        Dim table As New PdfPTable(2)
        ' Actual width of table in points
        table.TotalWidth = 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {8.5F, 6.5F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 0.0F
        table.SpacingAfter = 10.0F
        '
        Dim logolocation As String = JTVarMaint.JTvmFilePath & "logo\" & JTVarMaint.CompanyLogo
        '
        ' add code to see if logo files exists
        '
        If Not File.Exists(logolocation) Then
            iTSFillCell("-------- No logo file found -------", table, 0, 0, 0, "RED")
        Else
            '
            '
            Dim tmpImage As Image = Image.GetInstance(logolocation)
            tmpImage.ScaleAbsolute(140.0F, 50.0F)
            Dim CellContents = New PdfPCell(tmpImage)
            CellContents.Border = 0
            table.AddCell(CellContents)
        End If
        ' -----------------------------------------------------------------
        ' Add invoice Date.
        ' -----------------------------------------------------------------
        Dim tmpHeadingInfo As String = "JOB INFORMATION MANAGER" & vbCrLf & JTmrReportHeader & vbCrLf & vbCrLf &
            "Report Run: " & String.Format("{0:MM/dd/yyyy HH:mm}", DateTime.Now)

        '
        iTSFillCell(tmpHeadingInfo, table, 0, 1, 0, "")
        '
        '
        myDoc.Add(table)
        '
        Return ""
    End Function
    '
    Private Function JTmrBldAcctList(ByRef myDoc As Document) As String
        Dim table As New PdfPTable(2)
        ' Actual width of table in points
        table.TotalWidth = 275.0F         ' full page width - 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 0
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.0F, 3.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        '

        iTSFillCell("Account ID", table, 1, 1, 1, "")
        iTSFillCell("Account Description", table, 1, 1, 1, "")
        Dim tmpSub As Integer = 0
        Dim tmpAccount As String = Nothing
        Dim tmpDesc As String = Nothing
        Do While JTVarMaint.JTvmReadNJREA(tmpSub, tmpAccount, tmpDesc) = "DATA"
            iTSFillCell(tmpAccount, table, 0, 1, 0, "")
            iTSFillCell(tmpDesc, table, 0, 1, 0, "")
        Loop
        '
        myDoc.Add(table)
        '
        Return ""
    End Function
    Private Function JTmrBldCustJob(myDoc) As String
        Dim table As New PdfPTable(7)
        ' Actual width of table in points
        table.TotalWidth = 668.0F         ' full page width - 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 1
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {2.9F, 2.9F, 4.0F, 2.0F, 2.0F, 2.0F, 2.0F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        '

        iTSFillCell("Customer", table, 1, 1, 1, "")
        iTSFillCell("Job", table, 1, 1, 1, "")
        iTSFillCell("Job Description", table, 1, 1, 1, "")
        iTSFillCell("Pricing Method", table, 1, 1, 1, "")
        iTSFillCell("Created", table, 1, 1, 1, "")
        iTSFillCell("Last Activity", table, 1, 1, 1, "")
        iTSFillCell("Deactivated", table, 1, 1, 1, "")
        Dim tmpSub As Integer = 0
        Dim tmpJTjimRec As JTjim.JTjimJobInfo = Nothing
        Dim tmpLastCustomer As String = Nothing

        Do While JTjim.JTjimReadJtjim(tmpSub, tmpJTjimRec) = "DATA"
            If tmpJTjimRec.JTjimJIJobName = tmpLastCustomer Then
                iTSFillCell("", table, 0, 1, 0, "")
            Else
                iTSFillCell(tmpJTjimRec.JTjimJIJobName, table, 0, 1, 0, "")
                tmpLastCustomer = tmpJTjimRec.JTjimJIJobName
            End If
            iTSFillCell(tmpJTjimRec.JTjimJISubName, table, 0, 1, 0, "")
            iTSFillCell(tmpJTjimRec.JTjimJIDescName, table, 0, 1, 0, "")
            iTSFillCell(tmpJTjimRec.JTjimJIJobPricMeth, table, 1, 1, 0, "")
            iTSFillCell(tmpJTjimRec.JTjimJICreateDate, table, 1, 1, 0, "")
            iTSFillCell(tmpJTjimRec.JTjimJILastActDate, table, 1, 1, 0, "")
            iTSFillCell(tmpJTjimRec.JTjimJIDeactDate, table, 1, 1, 0, "")
            '
        Loop
        '
        myDoc.Add(table)
        tmpJTjimRec = Nothing
        '
        Return ""
    End Function
    Private Function JTmrBldVendor(myDoc) As String
        Dim table As New PdfPTable(9)
        ' Actual width of table in points
        table.TotalWidth = 670.0F         ' full page width - 468.0F 'fix the absolute width of the table
        table.LockedWidth = True
        table.HeaderRows = 1
        '
        ' Column widths are proportional to other columns and the table width.
        Dim widths() As Single = {3.5F, 4.9F, 1.2F, 1.8F, 1.8F, 1.2F, 2.9F, 1.2F, 0.8F}
        table.SetWidths(widths)
        '
        table.HorizontalAlignment = 0 ' Determines the location of the table on the page.
        ' leave a gap before and after the table
        table.SpacingBefore = 10.0F
        table.SpacingAfter = 10.0F
        '
        '

        iTSFillCell("Vendor ID", table, 1, 1, 1, "")
        iTSFillCell("Name", table, 1, 1, 1, "")
        iTSFillCell("Type", table, 1, 1, 1, "")
        iTSFillCell("Established", table, 1, 1, 1, "")
        iTSFillCell("Lst Activity", table, 1, 1, 1, "")
        iTSFillCell("PayMeth", table, 1, 1, 1, "")
        iTSFillCell("Acct #", table, 1, 1, 1, "")
        iTSFillCell("Terms", table, 1, 1, 1, "")
        iTSFillCell("Disc %", table, 1, 1, 1, "")
        '
        Dim tmpSub As Integer = 0
        Dim tmpJTviRec As JTvi.JTviArrayStructure = Nothing
        '
        Do While JTvi.JTviReadVendInfo(tmpSub, tmpJTviRec) = "DATA"
            '
            iTSFillCell(tmpJTviRec.JTviShortName, table, 0, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviVendName, table, 0, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviVendType, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviEstDate, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviLstActDate, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviActivityInterface, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviAcctNum, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviPymtTerms, table, 1, 1, 0, "")
            iTSFillCell(tmpJTviRec.JTviDiscPercent, table, 1, 1, 0, "")
            '
        Loop
        '
        myDoc.Add(table)
        tmpJTviRec = Nothing
        '
        Return ""
    End Function
End Module
