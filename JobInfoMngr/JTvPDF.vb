﻿Public Class JTvPDF
    Public JTvPDFDelayFlag As Boolean = False
    Sub JTvPDFDispPDF(tmpFileName As String)
        Dim tmpFilePath As String = CreateWorkingFile(tmpFileName)
        ' making sure that switches that could cause a rerun of invoice record are set correctly to avoid that error.
        If txtJTvPDFRprtType.Text = "" Then
            rbtnEmailYes.Checked = False
            rbtnEmailNever.Checked = False
            rbtnEmailNotNow.Checked = False
            rbtnRecInv.Checked = False
            rbtnDiscard.Checked = True
            gboxInvSettings.Visible = False
            gboxInvReview.Visible = False
        End If
        '
        '
        AxAcroPDF1.Dock = System.Windows.Forms.DockStyle.None
        AxAcroPDF1.Enabled = True
        AxAcroPDF1.Name = tmpFilePath
        AxAcroPDF1.LoadFile(tmpFilePath)
        AxAcroPDF1.Visible = True
        JTstart.JTstartMenuOff()
        ' deleted next line --- may have issue with jr & ir
        '     txtJTvPDFRprtType.Text = ""
        Me.Show()
        '
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Hide()
        JTstart.JTstartMenuOn()
        '
        If txtJTvPDFRprtType.Text = "NewInv" Then
            Dim tmpRecordSW As String
            Dim tmpEmailSW As String = Nothing
            If rbtnRecInv.Checked = True Then
                tmpRecordSW = "Y"
            Else
                tmpRecordSW = "N"
            End If
            If rbtnEmailYes.Checked = True Then
                tmpEmailSW = "Y"
            End If
            If rbtnEmailNotNow.Checked = True Then
                tmpEmailSW = "N"
            End If
            If rbtnEmailNever.Checked = True Then
                tmpEmailSW = "C"
            End If
            txtJTvPDFRprtType.Text = ""
            gboxInvRecord.Visible = False
            JTas.BtnJTasInvoice_Continue2(txtfilename.Text, txttmpSubJobID.Text,
                                        tmpRecordSW, tmpEmailSW)
        End If
        If txtJTvPDFRprtType.Text = "RevInv" Then

            '           Me.Hide()
            If rbtnReemailNo.Checked = False Then
                If rbtnReemailNever.Checked = True Then
                    JTas.JTasUpdtEmailDate(txttmpJobID.Text, txttmpInvNum.Text,
                                      "Canceled")
                ElseIf rbtnRemailYes.Checked = True Then
                    '
                    If JTVarMaint.JTvmDemoBypass <> "DexterCoventry" Then
                        MsgBox("Email functions disabled in Demo Mode")     'Bypass email if in demo mode
                    Else
                        If CheckForInternetConnection() = True Then
                            Dim tmpEmailDate As String = String.Format("{0:MM/dd/yyyy}", Date.Today)

                            Dim tmpCC As String = JTVarMaint.JTCCEmailAddress
                            Dim tmpBCC As String = ""
                            Dim tmpFromAdd As String = JTVarMaint.JTFromEmailAddress
                            Dim tmpSubject As String = JTVarMaint.CompanyNme & " - Invoice # " &
                            txttmpInvNum.Text
                            Dim tmpMsg As String = "Attached is Invoice # " & txttmpInvNum.Text &
                            "  " & String.Format("{0:MM/ dd / yyyy}", txtTmpInvDate.Text)
                            Dim tmpAttach As String = txtfilename.Text
                            JTEmail.JTSndEmail(JTas.JTasBldEmailToAdd(), tmpCC, tmpBCC, tmpFromAdd,
                                           tmpSubject, tmpMsg, tmpAttach)
                            '
                            JTas.JTasUpdtEmailDate(txttmpJobID.Text, txttmpInvNum.Text,
                                                              tmpEmailDate)
                        Else
                            MsgBox("The Internet connection is not available." & vbCrLf &
                               "Emails cannot currently be sent.")
                        End If
                    End If
                End If
            End If
            txtJTvPDFRprtType.Text = ""
        End If
        gboxInvReview.Visible = False
        JTvPDFDelayFlag = False
        If JTstart.JTstEOJReporting = True Then
            JTstart.JTstartExitSysCont()
        End If
        '
        txtJTvPDFRprtType.Text = ""

    End Sub
    '
    Public Sub JTvPDFDispPDFInv(JTvPDFRprtType As String, filename As String,
                                tmpSubJobID As String, tmpEmailDate As String, tmpJobID As String,
                                tmpInvNum As String, tmpInvDate As String)
        gboxInvReview.Visible = False
        gboxInvSettings.Visible = False
        Dim tmpFstChar As Integer = filename.IndexOf("_20")
        ' ----------------------------------------------------------------
        ' Code taken out ... used in old directory structure --- 2/22/2024
        ' ----------------------------------------------------------------
        '     If filename.Substring(tmpFstChar + 1, 4) < JTVarMaint.JTvmSystemYear Then
        '    Dim tmpInvYear As String = filename.Substring(tmpFstChar + 1, 4)
        '   End If
        '
        txtJTvPDFRprtType.Text = JTvPDFRprtType
        txtfilename.Text = filename
        txttmpSubJobID.Text = tmpSubJobID
        txttmpJobID.Text = tmpJobID
        txttmpInvNum.Text = tmpInvNum
        txtTmpInvDate.Text = tmpInvDate
        txtEmailDate.Text = tmpEmailDate
        ' Display setting when new invoice
        If JTvPDFRprtType = "NewInv" Then
            rbtnEmailYes.Checked = True
            rbtnEmailNever.Checked = False
            rbtnEmailNotNow.Checked = False
            rbtnRecInv.Checked = True
            rbtnDiscard.Checked = False
            gboxInvSettings.Visible = True
            '        gboxInvReview.Visible = False
            gboxInvRecord.Visible = True
        End If
        ' Display settings when an invoice is being reviewed.
        If JTvPDFRprtType = "RevInv" Then
            '
            If tmpEmailDate = "" Then
                txtEmailComment.Text = "This invoice has not been emailed."
                rbtnReemailNever.Visible = True
            Else
                txtEmailComment.Text = "This invoice was last emailed on " & tmpEmailDate & "."
                rbtnReemailNever.Visible = False
            End If
            '
            If CheckForInternetConnection() = True Then
                InvRevEmailLabel.Text = "Do you to want Email" &
                                         vbCrLf & "(re-Email) this invoice?"
                rbtnReemailNo.Checked = True
                rbtnReemailNo.Visible = True
                rbtnRemailYes.Checked = False
                rbtnRemailYes.Visible = True
            Else
                InvRevEmailLabel.Text = "No Internet Connection" &
                                         vbCrLf & "Can't email ..."
                rbtnReemailNo.Checked = False
                rbtnReemailNo.Visible = False
                rbtnRemailYes.Checked = False
                rbtnRemailYes.Visible = False
                rbtnReemailNever.Checked = False
                rbtnReemailNever.Visible = False
            End If
            gboxInvSettings.Visible = False
            gboxInvReview.Visible = True
        End If

        JTvPDFDispPDF(filename)
        '
    End Sub

    Private Sub rbtnRecInv_CheckedChanged(sender As Object, e As EventArgs) _
        Handles rbtnRecInv.CheckedChanged, rbtnDiscard.CheckedChanged
        If rbtnRecInv.Checked = True Then
            gboxInvEmail.Visible = True
        Else
            gboxInvEmail.Visible = False
        End If
    End Sub


End Class
