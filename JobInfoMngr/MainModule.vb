﻿' Option Strict On

Imports System.IO
Imports System.Globalization
Imports System.Net
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports iTextSharp.text.pdf.parser
Imports System.Runtime.InteropServices



''' <summary>
'''   <para>Utility type functions are include in this module.</para>
''' </summary>
Module MainModule
    '
    ''' <summary>Function returns # of days date is from today's date.</summary>
    ''' <param name="tmpLstArchiveDate">
    ''' This is a date, stored as a string that represents the last time
    ''' transaction data has been archived. The date comes from JTGlobalVar.dat and there is one for
    ''' TK and one for NLC
    ''' </param>
    ''' <returns>The number of days between the last archive and todays date is returned.</returns>
    Public Function JTmmArchiveDayCalc(tmpLstArchiveDate As String) As String
        ' --------------------------------------------------------------
        ' Function returns # of days date is from today's date.
        ' --------------------------------------------------------------
        Dim tmpDaySinceLstArchive As Decimal
        Dim tmpJulianToday As String
        tmpJulianToday = Format$(Date.Today, "yyyy") & String.Format("{0:000}", Date.Today.DayOfYear)
        If tmpLstArchiveDate.Substring(0, 4) < tmpJulianToday.Substring(0, 4) Then
            tmpDaySinceLstArchive = Val(tmpJulianToday.Substring(4, 3)) + (365 - Val(tmpLstArchiveDate.Substring(4, 3)))
        Else
            tmpDaySinceLstArchive = Val(tmpJulianToday) - Val(tmpLstArchiveDate)
        End If
        If tmpDaySinceLstArchive > Val(JTVarMaint.PurgeTiming) Then
            Return "DoArchive"
        Else
            Return "DoNotArchive"
        End If
    End Function
    ''' <summary>Function convert Julian date to MM/DD/YYYY. This is a common date utility.</summary>
    ''' <param name="vDate">This argument represents a Julian date. It can have a two or four
    ''' digit year. The function will convert it based on length of the entry.</param>
    ''' <returns>the gregorian date is returned as a date (mm/dd/YYYY)</returns>
    Public Function Julian2Gregorian(ByVal vDate As Integer) As Date
        ' ---------------------------------------------------
        ' Function convert Julian date to MM/DD/YYYY
        ' ---------------------------------------------------
        Dim y As String = ""
        If vDate.ToString.Length = 5 Then
            y = vDate.ToString.Substring(0, 2)
        ElseIf vDate.ToString.Length = 7 Then
            y = vDate.ToString.Substring(0, 4)
        End If
        Return CDate("01/01/" & y).AddDays(CInt(Strings.Right(vDate.ToString, 3)) - 1)
    End Function
    ''' <summary>Function convert mm/dd/yyyy to Julian Date. This is a common date utility.</summary>
    ''' <param name="GDate">any date in gregorian format ... sent as a datetime</param>
    ''' <returns>The cooresponding Julian date is returned</returns>
    Public Function Gregarian2Julian(GDate As DateTime) As String
        ' --------------------------------------------------------
        ' Function convert mm/dd/yyyy to Julian Date
        ' --------------------------------------------------------
        '    Dim dt As DateTime
        Dim jc As String
        Dim dt As Date = GDate
        '    dt = DateTime.ParseExact(GDate, "MM/dd/yyyy", CultureInfo.InvariantCulture)
        jc = dt.Year.ToString + String.Format("{0:000}", dt.DayOfYear)

        Return jc
    End Function

    Public Function MMSDDSYYYYtoYYYYMMDD(ByVal MMSDDSYYYY As String) As String
        Dim YYYYMMDD As String
        If MMSDDSYYYY.Length <> 10 Then
            MsgBox("Input Date To Function invalid - >" & MMSDDSYYYY & "<" & vbCrLf &
                            "Function - MMSDDSYYYYtoYYYYMMDD - MainModule ~79")
            Return " "
        End If
        YYYYMMDD = MMSDDSYYYY.Substring(MMSDDSYYYY.Length - 4, 4)
        YYYYMMDD &= MMSDDSYYYY.Substring(0, 2)
        YYYYMMDD &= MMSDDSYYYY.Substring(MMSDDSYYYY.Length - 7, 2)
        Return YYYYMMDD
    End Function
    '
    Public Function YYYYMMSStoMMSDDSYYYY(ByVal YYYYMMDD As String) As String
        Dim MMSDDSYYYY As String
        If YYYYMMDD.Length <> 8 Then
            MsgBox("Input Date To Function invalid - >" & YYYYMMDD & "<" & vbCrLf &
                            "Function - YYYYMMDDtoMMSDDSYYYY - MainModule ~92")
            Return ""
        End If
        MMSDDSYYYY = YYYYMMDD.Substring(4, 2) & "/"
        MMSDDSYYYY &= YYYYMMDD.Substring(6, 2) & "/"
        MMSDDSYYYY &= YYYYMMDD.Substring(0, 4)
        Return MMSDDSYYYY
    End Function
    '
    Public Function IsAlphaNum(ByVal strInputText As String) As Boolean
        ' -------------------------------------------------------------------------------
        ' Function to check the argument for alpha/numeric ... no spec char ... no spaces
        ' -------------------------------------------------------------------------------
        Return System.Text.RegularExpressions.Regex.IsMatch(strInputText, "^[a-zA-Z0-9]+$")
    End Function
    ''' <summary>This function is a utility to check to see if the program currently has an internet
    ''' connection.</summary>
    ''' <returns>true (internet connection exists) or false (not currently connected to internet)
    ''' is returned</returns>
    Public Function CheckForInternetConnection() As Boolean
        Try
            Using client = New WebClient()
                Using stream = client.OpenRead("http://www.google.com")
                    Return True
                End Using
            End Using
        Catch
            Return False
        End Try
    End Function
    ''' <summary>This is a utility function to check to see if a file is in use.</summary>
    ''' <param name="sFile">This is the name of the file being checked.</param>
    ''' <returns>True = File is in use.
    ''' False = File not in use.</returns>
    Public Function FileInUse(ByVal sFile As String) As Boolean
        Dim thisFileInUse As Boolean = False
        If System.IO.File.Exists(sFile) Then
            Try
                Using f As New System.IO.FileStream(sFile, FileMode.Open, FileAccess.ReadWrite, FileShare.None)
                    ' thisFileInUse = False
                End Using
            Catch
                thisFileInUse = True
            End Try
        End If
        Return thisFileInUse
    End Function
    ''' <summary>code search a string for for vbCrLf</summary>
    ''' <param name="tmpDataField">This is the string to be examined.</param>
    Public Function ChkForVBCRLF(tmpDataField As String) As Integer

        ' --------------------------------------------------------------------------------------
        ' Function to look for VBCRLFs in a data field. The count will be used to better predict
        ' y position on the printpage for end of page management. 
        ' --------------------------------------------------------------------------------------
        Dim VBCRLFcnt As Integer = 0
        Dim PosinData As Integer = 0
        '
        For PosinData = 1 To Len(tmpDataField) - 2
            If tmpDataField.Substring(PosinData, 2) = vbCrLf Then
                VBCRLFcnt += 1
            End If
        Next
        MsgBox(tmpDataField & vbCrLf & Len(tmpDataField) & vbCrLf &
                VBCRLFcnt)
        Return VBCRLFcnt
    End Function
    ''' <summary>This code convert a state name to a two character abreviation and back again.</summary>
    ''' <param name="abbreviationOrStateName">
    '''   <para>The argument is either a state name or a two letter state abreviation. The corresponding element is returned.</para>
    ''' </param>
    ''' <returns>The result of the lookup or "NOTFOUND" if input was invalid.</returns>
    Function convertState(ByVal abbreviationOrStateName As String) As String
        Select Case abbreviationOrStateName.ToUpper
            Case “AK” : Return “Alaska”
            Case “AL” : Return “Alabama”
            Case “AR” : Return “Arkansas”
            Case “AZ” : Return “Arizona”
            Case “CA” : Return “California”
            Case “CO” : Return “Colorado”
            Case “CT” : Return “Connecticut”
            Case “DE” : Return “Delaware”
            Case “FL” : Return “Florida”
            Case “GA” : Return “Georgia”
            Case “HI” : Return “Hawaii”
            Case “IA” : Return “Iowa”
            Case “ID” : Return “Idaho”
            Case “IL” : Return “Illinois”
            Case “IN” : Return “Indiana”
            Case “KS” : Return “Kansas”
            Case “KY” : Return “Kentucky”
            Case “LA” : Return “Louisiana”
            Case “MA” : Return “Massachusetts”
            Case “ME” : Return “Maine”
            Case “MD” : Return “Maryland”
            Case “MI” : Return “Michigan”
            Case “MN” : Return “Minnesota”
            Case “MO” : Return “Missouri”
            Case “MS” : Return “Mississippi”
            Case “MT” : Return “Montana”
            Case “NC” : Return “North Carolina”
            Case “ND” : Return “North Dakota”
            Case “NE” : Return “Nebraska”
            Case “NH” : Return “New Hampshire”
            Case “NJ” : Return “New Jersey”
            Case “NM” : Return “New Mexico”
            Case “NV” : Return “Nevada”
            Case “NY” : Return “New York”
            Case “OH” : Return “Ohio”
            Case “OK” : Return “Oklahoma”
            Case “OR” : Return “Oregon”
            Case “PA” : Return “Pennsylvania”
            Case “RI” : Return “Rhode Island”
            Case “SC” : Return “South Carolina”
            Case “SD” : Return “South Dakota”
            Case “TN” : Return “Tennessee”
            Case “TX” : Return “Texas”
            Case “UT” : Return “Utah”
            Case “VT” : Return “Vermont”
            Case “VA” : Return “Virginia”
            Case “WA” : Return “Washington”
            Case “WI” : Return “Wisconsin”
            Case “WV” : Return “West Virginia”
            Case “WY” : Return “Wyoming”

            Case “ALASKA” : Return “AK”
            Case “ALABAMA” : Return “AL”
            Case “ARKANSAS” : Return “AR”
            Case “ARIZONA” : Return “AZ”
            Case “CALIFORNIA” : Return “CA”
            Case “COLORADO” : Return “CO”
            Case “CONNECTICUT” : Return “CT”
            Case “DELAWARE” : Return “DE”
            Case “FLORIDA” : Return “FL”
            Case “GEORGIA” : Return “GA”
            Case “HAWAII” : Return “HI”
            Case “IOWA” : Return “IA”
            Case “IDAHO” : Return “ID”
            Case “ILLINOIS” : Return “IL”
            Case “INDIANA” : Return “IN”
            Case “KANSAS” : Return “KS”
            Case “KENTUCKY” : Return “KY”
            Case “LOUISIANA” : Return “LA”
            Case “MASSACHUSETTS” : Return “MA”
            Case “MAINE” : Return “ME”
            Case “MARYLAND” : Return “MD”
            Case “MICHIGAN” : Return “MI”
            Case “MINNESOTA” : Return “MN”
            Case “MISSOURI” : Return “MO”
            Case “MISSISSIPPI” : Return “MS”
            Case “MONTANA” : Return “MT”
            Case “NORTH CAROLINA” : Return “NC”
            Case “NORTH DAKOTA” : Return “ND”
            Case “NEBRASKA” : Return “NE”
            Case “NEW HAMPSHIRE” : Return “NH”
            Case “NEW JERSEY” : Return “NJ”
            Case “NEW MEXICO” : Return “NM”
            Case “NEVADA” : Return “NV”
            Case “NEW YORK” : Return “NY”
            Case “OHIO” : Return “OH”
            Case “OKLAHOMA” : Return “OK”
            Case “OREGON” : Return “OR”
            Case “PENNSYLVANIA” : Return “PA”
            Case “RHODE ISLAND” : Return “RI”
            Case “SOUTH CAROLINA” : Return “SC”
            Case “SOUTH DAKOTA” : Return “SD”
            Case “TENNESSEE” : Return “TN”
            Case “TEXAS” : Return “TX”
            Case “UTAH” : Return “UT”
            Case “VERMONT” : Return “VT”
            Case “VIRGINIA” : Return “VA”
            Case “WASHINGTON” : Return “WA”
            Case “WISCONSIN” : Return “WI”
            Case “WEST VIRGINIA” : Return “WV”
            Case “WYOMING” : Return “WY”
            Case Else : Return "NOTFOUND"
        End Select
    End Function
    ''' <summary>
    ''' Checks for alpha characters.
    ''' </summary>
    ''' <param name="StringToCheck">The string to check.</param>
    ''' <returns>If non-alpha character found ... False returned
    '''          Else True is returned.
    ''' </returns>
    Function CheckForAlphaCharacters(ByVal StringToCheck As String) As Boolean
        For i = 0 To StringToCheck.Length - 1
            If Not Char.IsLetter(StringToCheck.Chars(i)) Then
                Return False
            End If
        Next
        '
        Return True
        '
    End Function
    ' --------------------------------------------------------------------
    ' iTextSharp Font functions
    ' Each function is for once iteration of a font and can be utilized 
    ' with the following sample code.
    '
    ' Dim stuName As PdfPCell = New PdfPCell(FormatPhrase("Sample Document"))
    ' --------------------------------------------------------------------


    Public Function FP_H8(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 8))
    End Function
    Public Function FP_H6Blue(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 6, BaseColor.BLUE))
    End Function
    Public Function FP_H8Blue(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 8, BaseColor.BLUE))
    End Function
    Public Function FP_H10(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 10))
    End Function
    Public Function FP_H10UL(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.UNDERLINE))
    End Function
    Public Function FP_H12(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 12))
    End Function
    Public Function FP_H12UL(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.UNDERLINE))
    End Function
    Public Function FP_HB14UL(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 14))
    End Function
    Dim WMFont = New Font(FontFactory.GetFont(FontFactory.HELVETICA, 52, Font.BOLD, New GrayColor(0.75F)))
    Public Function FP_H52BG(value As String) As Phrase
        Return New Phrase(value, FontFactory.GetFont(FontFactory.HELVETICA, 52, Font.BOLD, New GrayColor(0.75F)))
    End Function
    '
    ''' <summary>
    ''' Add text as the watermark to each page of the source pdf to create 
    ''' a new pdf with text watermark
    ''' </summary>
    ''' <param name="sourceFile">           Source file. </param>
    ''' <param name="outputFile">           The output file. </param>
    ''' <param name="watermarkText">        The watermark text. </param>
    ''' <param name="watermarkFont">        (Optional) The watermark font. </param>
    ''' <param name="watermarkFontSize">    (Optional) Size of the watermark font. </param>
    ''' <param name="watermarkFontColor">   (Optional) The watermark font color. </param>
    ''' <param name="watermarkFontOpacity"> (Optional) The watermark font opacity. </param>
    ''' <param name="watermarkRotation">    (Optional) The watermark rotation. </param>

    Public Sub AddWatermarkText(ByVal sourceFile As String, ByVal outputFile As String, ByVal watermarkText As String,
                                       Optional ByVal watermarkFont As iTextSharp.text.pdf.BaseFont = Nothing,
                                       Optional ByVal watermarkFontSize As Single = 40,
                                       Optional ByVal watermarkFontColor As iTextSharp.text.BaseColor = Nothing,
                                       Optional ByVal watermarkFontOpacity As Single = 0.3F,
                                       Optional ByVal watermarkRotation As Single = 270.0F)

        Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
        Dim stamper As iTextSharp.text.pdf.PdfStamper = Nothing
        Dim gstate As iTextSharp.text.pdf.PdfGState = Nothing
        Dim underContent As iTextSharp.text.pdf.PdfContentByte = Nothing
        Dim rect As iTextSharp.text.Rectangle = Nothing
        Dim currentY As Single = 0.0F
        Dim offset As Single = 0.0F
        Dim pageCount As Integer = 0
        Try
            reader = New iTextSharp.text.pdf.PdfReader(sourceFile)
            rect = reader.GetPageSizeWithRotation(1)
            stamper = New iTextSharp.text.pdf.PdfStamper(reader, New System.IO.FileStream(outputFile, FileMode.Create))
            If watermarkFont Is Nothing Then
                watermarkFont = iTextSharp.text.pdf.BaseFont.CreateFont(iTextSharp.text.pdf.BaseFont.HELVETICA,
                                                              iTextSharp.text.pdf.BaseFont.CP1252,
                                                              iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
            End If
            If watermarkFontColor Is Nothing Then
                watermarkFontColor = iTextSharp.text.BaseColor.RED
            End If
            gstate = New iTextSharp.text.pdf.PdfGState()
            gstate.FillOpacity = watermarkFontOpacity
            gstate.StrokeOpacity = watermarkFontOpacity
            pageCount = reader.NumberOfPages()
            For i As Integer = 1 To pageCount
                underContent = stamper.GetOverContent(i)
                With underContent
                    .SaveState()
                    .SetGState(gstate)
                    .SetColorFill(watermarkFontColor)
                    .BeginText()
                    .SetFontAndSize(watermarkFont, watermarkFontSize)
                    .SetTextMatrix(30, 30)
                    If watermarkText.Length > 1 Then
                        currentY = (rect.Height / 2) + ((watermarkFontSize * watermarkText.Length) / 2)
                    Else
                        currentY = (rect.Height / 2)
                    End If
                    For j As Integer = 0 To watermarkText.Length - 1
                        If j > 0 Then
                            offset = (j * watermarkFontSize) + (watermarkFontSize / 4) * j
                        Else
                            offset = 0.0F
                        End If
                        .ShowTextAligned(iTextSharp.text.Element.ALIGN_CENTER, watermarkText(j), rect.Width / 2, currentY - offset, watermarkRotation)
                    Next
                    .EndText()
                    .RestoreState()
                End With
            Next
            stamper.Close()
            reader.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' +++++++++++++++ This function in not currently operational ++++++++++++++++++++
    ''' Its eventual intent is to print a pdf directly without any further operator
    ''' intervention.
    ''' +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ''' </summary>
    ''' <param name="PDFFileName"></param>
    ''' <returns></returns>
    Public Function PrintPDF(PDFFileName As String) As String
        ' ----------------------------------------------------
        ' Function code not completed.
        ' ----------------------------------------------------
        MsgBox("PrintPDF function not operational - MainModule ~ 361")
        '
        '  Code that will hopefully print a pdf file --------------------
        '   Dim MyProcess As New Process
        '   MyProcess.StartInfo.CreateNoWindow = False
        '   MyProcess.StartInfo.Verb = "print"
        '   MyProcess.StartInfo.FileName = "C:\Test.pdf"
        '   MyProcess.Start()
        '   MyProcess.WaitForExit(10000)
        '   MyProcess.CloseMainWindow()
        '   MyProcess.Close()
        ' -----------------------------------------------------------------
        Return ""
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>General function to validate masked text date field.</summary>
    '''
    ''' <remarks>Wecnk, 3/29/2019.</remarks>
    '''
    ''' <param name="tmpDate">This argument expects a field from a masked text field in "MM/DD/YYYY" format.</param>
    '''
    ''' <returns>True if the test passes, false if the test fails.</returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Function TestForValidDate(tmpDate As String) As Boolean
        ' -------------------------------------------------------------------------------
        '  mtxt date validation
        ' -------------------------------------------------------------------------------
        '
        ' check for entry with no year ... append year prior to validation
        '
        If tmpDate <> "  /  /" Then
            tmpDate = tmpDate.Replace(" ", "0")
            '
            If tmpDate.Length = 6 Then
                If tmpDate.Substring(0, 2) > Today.Month Then
                    tmpDate = tmpDate & Today.Year - 1

                Else
                    tmpDate = tmpDate & Today.Year
                End If
            End If
        Else
            Return False    ' no input to check in the field
        End If

        If IsDate(tmpDate) = False Or
            tmpDate.Length < 10 Or
            tmpDate.Substring(6, 2) <> "20" Or
            IsNumeric(tmpDate.Substring(6, 4)) = False Then
            Return False
        End If
        '
        Return True
    End Function
    '
    ''' <summary>
    ''' Function will cause system to pause for specified number of seconds.
    ''' </summary>
    ''' <param name="seconds"># of seconds to pause</param>
    ''' <returns></returns>
    Public Function Wait(ByVal seconds As Integer) As String
        For i As Integer = 0 To seconds * 100
            System.Threading.Thread.Sleep(10)
            Application.DoEvents()
        Next
        Return ""
    End Function
    ''' <summary>
    ''' Function to check file name to make sure that it contain only acceptable characters.
    ''' True returned with good name ... False if there is a problem.
    ''' </summary>
    ''' <param name="fileName">File name to be checked.</param>
    ''' <returns></returns>
    Public Function FileNameIsOk(ByVal fileName As String) As Boolean

        For Position As Integer = 0 To fileName.Length - 1

            Dim Character As String = fileName.Substring(Position, 1).ToUpper
            Dim AsciiCharacter As Integer = Asc(Character)

            Select Case True

                Case Character = "_" 'allow _
                Case Character = " " 'allow space
                Case AsciiCharacter >= Asc("A") And AsciiCharacter <= Asc("Z") 'Allow alphas
                Case AsciiCharacter >= Asc("0") AndAlso AsciiCharacter <= Asc("9") 'allow digits

                Case Else 'otherwise, invalid character
                    Return False

            End Select

        Next

        Return True

    End Function
    Public Function iTSFillCell(ArrayData As String, ByRef table As PdfPTable,
                               Position As Integer, Span As Integer,
                               Border As Integer, Color As String) As String
        If ArrayData = "" Then
            ArrayData = " "
        End If
        Dim CellContents = New PdfPCell(New Phrase(FP_H8(ArrayData)))
        CellContents.HorizontalAlignment = Position
        CellContents.VerticalAlignment = Element.ALIGN_TOP
        CellContents.Colspan = Span
        '
        If Border = 0 Then
            CellContents.Border = Border
        End If
        If Color <> "" Then
            Select Case Color.ToUpper
                Case "YELLOW"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(249, 231, 159)  ' Yellow
                Case "BLUE"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(0, 0, 255)
                Case "PALEBLUE"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(222, 234, 244)
                Case "GREEN"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(143, 188, 139)  ' Green
                Case "RED"
                    CellContents.BackgroundColor = New iTextSharp.text.BaseColor(255, 0, 0)
            End Select
        End If
        '

        table.AddCell(CellContents)
        Return ""
    End Function
    ''' <summary>
    ''' This function is to add page number in a # of # format.
    ''' Probably will be deleted ... Not being used.
    ''' </summary>
    ''' <param name="tmpFileIn"></param>
    ''' <param name="tmpFileOut"></param>
    ''' <returns></returns>
    Public Function AddPageNumber(tmpFileIn As String, tmpFileOut As String) As String
        Dim bytes As Byte() = File.ReadAllBytes(tmpFileIn)
        Dim blackFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK)
        Using stream As New MemoryStream()
            Dim reader As New PdfReader(bytes)
            Using stamper As New PdfStamper(reader, stream)
                Dim pages As Integer = reader.NumberOfPages
                For i As Integer = 1 To pages
                    Dim tmpPageNum As String = i.ToString & " of " & pages.ToString
                    ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_CENTER, New Phrase(tmpPageNum, blackFont), 284.0F, 15.0F, 0)
                    '              Document.Right() - document.Left()) / 2 + document.LeftMargin(), Document.Bottom() - 10, 0
                    '        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_CENTER,
                    '                                  New Phrase(tmpPageNum, blackFont),
                    '                                 (stamper.Right() - tmpFileOut.Left()) / 2 + tmpFileOut.LeftMargin(), tmpFileOut.Bottom() - 10, 0)
                Next
            End Using
            bytes = stream.ToArray()
        End Using
        File.WriteAllBytes(tmpFileOut, bytes)
        Return ""
    End Function
    ''' <summary>Little utility function to make variable length customer and job names into a fixed length key ... length is padded with "___" characters.</summary>
    ''' <param name="value"></param>
    ''' <param name="totalLength"></param>
    ''' <param name="padding"></param>
    Public Function FixedLengthString(ByVal value As String, ByVal totalLength As Integer, ByVal padding As Char) As String
        Dim tmpnullrecord = "__________"
        If value = "" Then
            value = tmpnullrecord
            Return value.PadRight(totalLength, padding)
        End If
        Dim lengthv As Integer = value.Length
        If (lengthv > totalLength) Then Return value.Substring(0, totalLength)
        Return value.PadRight(totalLength, padding)
    End Function
    '
    Public Function PDFSplitAndSave(inputPath As String, outputPath As String) As Integer
        Dim file As New FileInfo(inputPath)
        Dim name As String = file.Name.Substring(0, file.Name.LastIndexOf("."))
        Using pdfReader As New iTextSharp.text.pdf.PdfReader(inputPath)
            For pageNumber As Integer = 1 To pdfReader.NumberOfPages
                ' Get CGHS No.
                Dim strategy As ITextExtractionStrategy = New SimpleTextExtractionStrategy()
                Dim currentText As String = PdfTextExtractor.GetTextFromPage(pdfReader, pageNumber, strategy)
                ' --------------------------------------------------------------------------------
                ' The new file name is being created for each split file. It is the original file
                ' name with '__' (double underscore) and the page number appended just before the
                ' '.PDF'. This String will allow the merging code to identify candidates to merge.
                ' --------------------------------------------------------------------------------
                Dim filename As String = name & "__" & pageNumber.ToString & ".PDF"
                Dim document As New iTextSharp.text.Document()
                Dim pdfCopy As New iTextSharp.text.pdf.PdfCopy(document,
                     New FileStream(Convert.ToString(outputPath & Convert.ToString("\")) & filename, FileMode.Create))
                document.Open()
                pdfCopy.AddPage(pdfCopy.GetImportedPage(pdfReader, pageNumber))
                document.Close()
            Next
            pdfReader.Close()



            pdfReader.Dispose()

            ' -----------------------------------------------------------------------
            ' Code hacked from web to make sure files used in preceeding process
            ' are all closed.
            ' -----------------------------------------------------------------------
            Dim id As Integer 'Global variable
            id = System.Diagnostics.Process.Start(inputPath).Id
            System.Diagnostics.Process.GetProcessById(id).Kill()
            ' -----------------------------------------------------------------------
            Return pdfReader.NumberOfPages
        End Using
    End Function


    Public WorkingFileDirPath As String = Environ("OneDrive") & "\JobInfoMngrDocs\JTJIMPdfWF\"
    Public JIMWFSeqNum As Integer = 1000

    ''' <summary>
    '''  Setting up or maintaining directory that will hold working .pdf files.
    ''' </summary>
    ''' <returns></returns>
    Public Function PrepWFFolder() As Boolean
        '
        If Directory.Exists(WorkingFileDirPath) Then
            ' Delete files created in last session

            ' loop through each file in the target directory
            For Each file_path As String In Directory.GetFiles(WorkingFileDirPath)

                ' delete the file if possible...otherwise skip it
                Try
                    File.Delete(file_path)
                Catch ex As Exception
                    MsgBox("Something went wrong while deleting working files. ~95")
                End Try

            Next
        Else
            Try
                My.Computer.FileSystem.CreateDirectory(Environ("OneDrive") & "/JobInfoMngr/JTJIMPdfWorkingFiles/")
            Catch ex As Exception
                MsgBox("Program failed while creating JTJIMPdfWorkingFiles folder ~591-MainModule")
            End Try

        End If
        '
        Return True
    End Function


    ''' <summary>
    ''' This function take a file name sent to it and creates a copy of the file in
    ''' a folder named ------------- located in JobInfoMngr. These are single use files and 
    ''' will be deleted after use.
    ''' </summary>
    ''' <param name="OrigFileName">This is the name and the path of the file to be displayed
    ''' in the browser.
    ''' </param>
    ''' <returns name="GenFileName">This is the address of a file generated as a temporary
    ''' working file. It's name will be 'JIMWFXXXX.pdf'. 'XXXX' is sequential number generated
    ''' to make each file unique.</returns>
    Public Function CreateWorkingFile(ByRef OrigFileName As String) As String
        If Directory.Exists(WorkingFileDirPath) = False Then
            Directory.CreateDirectory(WorkingFileDirPath)
        End If


        Dim GenFileName As String = (WorkingFileDirPath & "JIMWF" & JIMWFSeqNum.ToString & ".pdf").ToUpper
        '
        Do While File.Exists(GenFileName)
            JIMWFSeqNum += 1
            GenFileName = (WorkingFileDirPath & "JIMWF" & JIMWFSeqNum.ToString & ".pdf").ToUpper
        Loop
        '
        Try
            System.IO.File.Copy(OrigFileName, GenFileName)
        Catch
            MsgBox("File copy failed -" & vbCrLf &
                   "Source file name - >" & OrigFileName & "<" & vbCrLf &
                   "Destination file name - >" & GenFileName & "<" & vbCrLf &
                   "~667/MainModule")
        End Try

        Return GenFileName
        '
    End Function

    ''' <summary>
    ''' Function to return number of pages included in any PDF file.
    ''' </summary>
    ''' <param name="tmpPDFFileName">Complete path and file name for PDF file being analyzed</param>
    ''' <returns>Number of pages in PDF</returns>
    Public Function GetPDFPageCount(ByRef tmpPDFFileName As String) As Integer
        Dim pr As New PdfReader(tmpPDFFileName)
        Dim tmpPageCount As Integer = pr.NumberOfPages()
        pr.Close()
        Return tmpPageCount
    End Function
    ''' <summary>
    ''' The following code merges multiple PDF files into one.
    ''' </summary>
    ''' <param name="outPutPDF"></param>
    Public Function MergePDFFiles(ByVal outPutPDF As String,
                              ByVal FirstPDFFile As String,
                              ByVal SecondPDFFile As String) As Boolean

        Dim tmpReturnValue As Boolean = True

        Dim FileArray As New List(Of String)
        FileArray.Add(FirstPDFFile)
        FileArray.Add(SecondPDFFile)

        Dim StartPath As String = FileArray(0) ' this is a List Array declared Globally
        Dim document = New Document()
        Dim outfile = outPutPDF
        '      Dim outFile = Path.Combine(outPutPDF) ' The outPutPDF varable is passed from another sub this is the output path
        Dim writer = New PdfCopy(document, New FileStream(outfile, FileMode.Create))

        Try

            document.Open()
            For Each fileName As String In FileArray

                Dim reader = New PdfReader(fileName)

                '     Dim reader = New PdfReader(Path.Combine(StartPath, fileName))

                For i As Integer = 1 To reader.NumberOfPages

                    Dim page = writer.GetImportedPage(reader, i)
                    writer.AddPage(page)

                Next i

                reader.Close()

            Next

            writer.Close()
            document.Close()

        Catch ex As Exception
            '    catch a Exception if needed
            tmpReturnValue = False

            MsgBox("Attempt To merge two PDF files failed. (PDF.002)")
        Finally

            writer.Close()
            document.Close()

        End Try

        Return tmpReturnValue

    End Function

    ' ==========================================================================

    Public Function mainmoduledummyfunction() As String
        Return ""
    End Function
    ''' <summary>
    ''' Function to add record to Event Log.
    ''' </summary>
    ''' <param name="tmpEvent"></param>
    ''' <param name="tmpDetails"></param>
    ''' <returns></returns>
    Public Function LogFileWrite(tmpEvent As String, tmpDetails As String) As Boolean
        If JTVarMaint.JTvmEventLogEnabled = False Then
            Return False
        End If
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTEventLog.XML"
        '
        Dim NewFileFlag As Boolean = False

        If My.Computer.FileSystem.FileExists(filePath) = False Then
            NewFileFlag = True
        End If
        Dim tmptime As DateTime = DateTime.Now
        Dim tmpformat As String = "MM/dd/yyyy HH:mm:ss"
        '
        Dim Rec1String As String = "<DateTime>" & tmptime.ToString(tmpformat) & "</DateTime>"
        Dim Rec2String As String = "<Event>" & tmpEvent & "</Event>"
        Dim Rec3String As String = "<Details>" & tmpDetails & "</Details>"
        '
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter(filePath, True)
        If NewFileFlag = True Then
            file.WriteLine("<?xml version=""1.0""?>")
            file.WriteLine("<EventLog>")
        End If
        file.WriteLine("<LogEvent id=""" & tmptime.ToString(tmpformat) & """>")
        file.WriteLine(Rec2String)
        file.WriteLine(Rec3String)
        file.WriteLine("</LogEvent>")
        file.Close()
        Return True
    End Function
    ''' <summary>
    ''' Function to replace selected special character with an 'Escape' sequence
    ''' so that the field will be handled correctly by system.xml.
    ''' </summary>
    ''' <param name="tmpString">string to be modified</param>
    ''' <returns></returns>
    Public Function HandleSpecCharXml(ByRef tmpString As String) As Boolean
        tmpString = tmpString.Replace("&", "&amp;")
        tmpString = tmpString.Replace("<", "&lt;")
        tmpString = tmpString.Replace(">", "&gt;")
        tmpString = tmpString.Replace("'", "&apos;")
        tmpString = tmpString.Replace("""", "&quot;")
        Return True
    End Function
    ''' <summary>
    ''' Function to restore special character in string. These character had been replaced by an 
    ''' escape sequence prior to written to an xml file.
    ''' </summary>
    ''' <param name="tmpString">string to be modified</param>
    ''' <returns></returns>
    Public Function HandleEscSeqXml(ByRef tmpString As String) As Boolean
        If tmpString.Contains("&") = False Then
            Return True
        Else
            tmpString = tmpString.Replace("&amp;", "&")
            tmpString = tmpString.Replace("&lt;", "<")
            tmpString = tmpString.Replace("&gt;", ">")
            tmpString = tmpString.Replace("&apos;", "'")
            tmpString = tmpString.Replace("&quot;", """")
        End If
        Return True
    End Function
    ''' <summary>
    ''' Function to disect pdf file name to see if it is a merge candidate.
    ''' </summary>
    ''' <param name="tmpfilepathname">complete path and filename of PDF file.</param>
    ''' <returns>Serial Number stripped from end of file name.
    ''' -1 means it is not a merge candidate.</returns>
    Public Function CheckforMergeCandidate(ByRef tmpfilepathname As String,
                                           ByRef tmpFilepathMergeCandidate As String) As Integer
        Dim FileSerNum As Integer = -1
        Dim FirstCharacter As Integer = tmpfilepathname.IndexOf("__")
        If FirstCharacter = -1 Then
            Return FileSerNum
        End If
        FirstCharacter += 2
        Dim DotPDFLocation As Integer = tmpfilepathname.IndexOf(".PDF")
        If DotPDFLocation - FirstCharacter < 1 Then
            Return -1
        End If

        FileSerNum = Val(tmpfilepathname.Substring(FirstCharacter, DotPDFLocation - FirstCharacter))
        tmpFilepathMergeCandidate = Left(tmpfilepathname, FirstCharacter) &
                                        (FileSerNum + 1).ToString & ".PDF"
        If File.Exists(tmpFilepathMergeCandidate) = False Then
            Return -1
        End If
        '
        Return FileSerNum
    End Function
    ''' <summary>
    ''' Utility function to put text from PDF file into a text string. This 
    ''' function is used as part of the process to strip text from an invoice
    ''' take is needed for NLCedi entry.
    ''' </summary>
    ''' <param name="tmpFileName">Complete file name and path of file to evaluated</param>
    ''' <param name="tmpPDFText">String area that will save text from PRF</param>
    ''' <returns>Number of pages in PDF document</returns>
    Public Function FindTextinPDF(tmpFileName As String, ByRef tmpPDFText As String) As Integer

        Dim tmpCount As Integer = 0
        tmpPDFText = " "
        If File.Exists(tmpFileName) Then
            Dim PdfReader = New PdfReader(tmpFileName)
            Dim Page As Integer
            For Page = 1 To PdfReader.NumberOfPages
                Dim strategy = New SimpleTextExtractionStrategy()
                Dim tmpCurrPageText As String = PdfTextExtractor.GetTextFromPage(PdfReader, Page, strategy)
                tmpPDFText &= tmpCurrPageText
            Next
            '          MsgBox(tmpPDFText)
            '
            PdfReader.Close()
            PdfReader.Dispose()
        End If
        Return tmpCount
    End Function
    ''' <summary>
    ''' Function to look for and count the occurances of a specific character in a string.
    ''' </summary>
    ''' <param name="value">The string to be evaluagted</param>
    ''' <param name="ch">The character that is the target of the search.</param>
    ''' <returns>Number of occurances</returns>
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function
End Module


