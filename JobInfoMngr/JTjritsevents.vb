﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.IO
Public Class JTjritsevents
    ' --------------------------------------------------------------------------------
    ' This class handles page events for JTir - Invoice History Report.
    ' --------------------------------------------------------------------------------
    '
    Inherits PdfPageEventHelper
    '
    Public Overrides Sub onEndPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
        '
        Dim cb As PdfContentByte = writer.DirectContent()
        Dim Header = New Phrase(FP_HB14UL(JTVarMaint.CompanyNme & " - Job Report"))
        Dim footer = New Phrase("Printed " & String.Format("{0:MM/dd/yyyy}", Date.Today) &
                                "   -   Page " & writer.PageNumber)
        ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER,
                Header,
                (document.Right() - document.Left()) / 2 + document.LeftMargin(),
                document.Top() + 10, 0)
        ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER,
                footer,
                (document.Right() - document.Left()) / 2 + document.LeftMargin(),
                document.Bottom() - 10, 0)
        '
        ' 
    End Sub
End Class
