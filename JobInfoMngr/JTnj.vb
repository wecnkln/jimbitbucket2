﻿Public Class JTnj
    Dim JTnjJobNameSave As String = ""
    Dim JTnjSubNameSave As String = ""
    Dim JTnjJobTypeSave As String = ""

    Public Sub JTnjInit()
        txtJTnjSubName.ReadOnly = False
        JTnlcEFillcboxJobID()
        cboxJTnjJobName.Text = ""
        txtJTnjSubName.Text = ""
        lvJTnjJobs.Items.Clear()
        gboxJTnjAUPsw.Visible = False
        cboxJTnjJobName.Select()
        Me.Show()
    End Sub

    Private Sub cboxJTnjJobName_leave(sender As Object, e As EventArgs) Handles cboxJTnjJobName.Leave
        cboxJTnjJobName.Text = cboxJTnjJobName.Text.ToUpper()
        If cboxJTnjJobName.Text.Contains(" ") = True Then
            ToolTip1.ToolTipTitle = "Customer Name Error"
            ToolTip1.Show("No spaces are allowed in Customer Name." & vbCrLf & "Correct and Resubmit.", cboxJTnjJobName, 40, -90, 7000)
            cboxJTnjJobName.Select()
            Exit Sub
        End If
        Dim tmpJTnjJobName As String = cboxJTnjJobName.Text
        '     Dim tmpJTnjPOS As String
        '     Dim tmpJTnjSubName As String
        '     Dim tmpJTnjJobType As String
        '     Dim tmpDeactDate As String
        '
        If JTjim.JTjimCustFindFunc(tmpJTnjJobName) <> "DATA" Then


            Dim tmpMsgBoxText As String = "Customer name entered - - - >" & tmpJTnjJobName & "<" & vbCrLf &
                                          "is a new customer. If this Is correct, verify the" & vbCrLf &
                                          "entry you made And hit 'YES'. If this is not correct," & vbCrLf &
                                          "hit 'NO' and re-enter the corrected name. (NJ.009"

            Dim tmpAns As Integer = MsgBox(tmpMsgBoxText, vbYesNo, "J.I.M. - New Job Entry")
            If tmpAns = vbNo Then
                cboxJTnjJobName.Text = ""
                cboxJTnjJobName.Select()
                Exit Sub
            End If
            '
            txtErrMessage.Text = "New Customer (NJ.002)"
            JTnjJobNameSave = cboxJTnjJobName.Text
            JTnjSubNameSave = ""
            txtJTnjSubName.Select()
            Exit Sub
        End If
        lvJTnjJobs.Items.Clear()
        Dim tmpSub As Integer = 0
        Dim tmpJTjimRec As JTjim.JTjimJobInfo = Nothing
        Do While JTjim.JTjimReadJtjim(tmpSub, tmpJTjimRec) = "DATA"
            If tmpJTjimRec.JTjimJIJobName = cboxJTnjJobName.Text Then
                Dim itmbuild(7) As String
                Dim lvline As ListViewItem
                itmbuild(0) = tmpJTjimRec.JTjimJISubName
                itmbuild(1) = tmpJTjimRec.JTjimJIDescName
                itmbuild(2) = tmpJTjimRec.JTjimJICreateDate
                itmbuild(3) = tmpJTjimRec.JTjimJILastActDate
                itmbuild(4) = tmpJTjimRec.JTjimJIDeactDate
                itmbuild(5) = tmpJTjimRec.JTjimJIJobLocStreet
                itmbuild(6) = tmpJTjimRec.JTjimJIJobLocCity
                lvline = New ListViewItem(itmbuild)
                lvJTnjJobs.Items.Add(lvline)
            End If
        Loop
        ' -----------------------------------------------------------------------------------------
        ' If the customer has other jobs, the first job in the lv is checked as a default to use
        ' as the work location. This can be changed by the operator.
        ' -----------------------------------------------------------------------------------------
        If lvJTnjJobs.Items.Count > 0 Then
            lvJTnjJobs.Items(0).Checked = True
        End If
        txtJTnjSubName.Select()

    End Sub

    Private Sub btnJTnjContinue_Click(sender As Object, e As EventArgs) Handles btnJTnjContinue.Click
        txtErrMessage.Text = ""
        If cboxJTnjJobName.Text = "" Then
            ToolTip1.ToolTipTitle = "Customer Name Error"
            ToolTip1.Show("No Customer Name entered --- Correct and Resubmit.(NJ.004)", cboxJTnjJobName, 40, -90, 7000)
            cboxJTnjJobName.Select()
            Exit Sub
        End If
        '
        JTnjJobNameSave = cboxJTnjJobName.Text
        '
        If txtJTnjSubName.Text = "" Then
            ToolTip1.ToolTipTitle = "Job Error"
            ToolTip1.Show("Job name required for Customere Name that" & vbCrLf & "has been entered. Correct and Resubmit.(NJ.005)",
                          txtJTnjSubName, 40, -90, 7000)
            txtJTnjSubName.ReadOnly = False
            txtJTnjSubName.Select()
            Exit Sub
        Else
            If txtJTnjSubName.Text.Contains(" ") = True Then
                ToolTip1.ToolTipTitle = "SubJob Error"
                ToolTip1.Show("Spaces not allowed in SubJob Name. " & vbCrLf & "Correct and Resubmit.(NJ.006)",
                          txtJTnjSubName, 40, -90, 7000)
                cboxJTnjJobName.Select()
                Exit Sub
            End If
        End If
        '
        If chkbJTnjAUPLab.Checked = False Then
            If chkbJTnjAUPMat.Checked = False Then
                If chkbJTnjAUPSer.Checked = False Then
                    ToolTip1.Show("At least one category must be checked in an AUP job." & vbCrLf & "Correct and Resubmit.(NJ.008)",
                                              gboxJTnjAUPsw, 40, -90, 7000)
                    cboxJTnjJobName.Select()
                    Exit Sub
                End If
            End If
        End If
        '
        JTnjSubNameSave = txtJTnjSubName.Text.ToUpper
        '
        Dim JTjimNJJobLocStreet As String = Nothing
        Dim JTjimNJJobLocCity As String = Nothing
        '
        If lvJTnjJobs.CheckedItems.Count > 0 Then
            Dim tmpRow As Integer = CInt(lvJTnjJobs.CheckedItems(0).Index)
            JTjimNJJobLocStreet = lvJTnjJobs.Items(tmpRow).SubItems(5).Text
            JTjimNJJobLocCity = lvJTnjJobs.Items(tmpRow).SubItems(6).Text
        End If
        '
        Dim JTjimNJJobPricing As String = Nothing
        If rbtnJTnjPricingCostPlus.Checked = True Then
            JTjimNJJobPricing = "COST+"
        End If
        If rbtnJTnjPricingCostPlusDeposits.Checked = True Then
            JTjimNJJobPricing = "COST+DEPOSITS"
        End If
        If rbtnJTnjPricingAUP.Checked = True Then
            JTjimNJJobPricing = "AUP"
        End If
        Dim tmpJTnjPOS As String = ""
        Dim tmpJTnjJobType As String = ""
        Dim tmpDeactDate As String = ""

        If JTjim.JTjimFindFunc(tmpJTnjPOS, JTnjJobNameSave, JTnjSubNameSave, tmpJTnjJobType, tmpDeactDate) = "DATA" Then
            ToolTip1.ToolTipTitle = "JobSub Combination Error"
            ToolTip1.Show("New Customer/Job combination already exist. Correct and Resubmit.(NJ.001)",
                         cboxJTnjJobName, 40, -45, 7000)
            cboxJTnjJobName.Select()
            JTnjSubNameSave = ""
            JTnjJobNameSave = ""
            JTnjJobTypeSave = ""
            Exit Sub
        End If
        '
        Me.Hide()
        Dim JTnjAUPLab As Boolean = chkbJTnjAUPLab.Checked
        Dim jtnjAUPMat As Boolean = chkbJTnjAUPMat.Checked
        Dim jtnjAUPSer As Boolean = chkbJTnjAUPSer.Checked

        JTjim.JTjimNJinit(JTnjJobNameSave,
                          JTnjSubNameSave,
                          JTjimNJJobLocStreet,
                          JTjimNJJobLocCity,
                          JTjimNJJobPricing,
                          JTnjAUPLab,
                          jtnjAUPMat,
                          jtnjAUPSer)
        '
        Me.Close()
        '
    End Sub

    Private Sub btnJTnjCancel_Click(sender As Object, e As EventArgs) Handles btnJTnjCancel.Click
        Me.Hide()
        MsgBox("New Job Process Cancelled.")
        JTMainMenu.JTMMinit()
        Me.Close()
    End Sub
    ''' <summary>
    ''' Convert job name entry to upper case on new job panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTnjSubName_lostfocus(sender As Object, e As EventArgs) Handles txtJTnjSubName.LostFocus
        txtJTnjSubName.Text = txtJTnjSubName.Text.ToUpper
    End Sub
    ''' <summary>
    ''' This tag holds the row number of the last item checked in the listview
    ''' on the new job panel. It is only used to turn off an old check when
    ''' a new item is checked.
    ''' </summary>
    Dim tmpLastItemChecked As Integer = 0
    ''' <summary>
    ''' Sub to handle check changes on new job panel. When item is checked, code
    ''' turns off color on previous items checked and changes the color on the
    ''' text of the selected item's job location street and city.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvJTnjJobs_ItemChecked(sender As Object, e As EventArgs) Handles lvJTnjJobs.ItemChecked
        If lvJTnjJobs.CheckedItems.Count > 1 Then
            ' -------------------------------------------------------
            ' Funny thing about following code - - - 
            ' Color change did not work when uncheck line was first. 
            ' Reversed order And everything worked.
            ' -------------------------------------------------------
            lvJTnjJobs.Items(tmpLastItemChecked).SubItems(5).ForeColor = Color.Black
            lvJTnjJobs.Items(tmpLastItemChecked).SubItems(6).ForeColor = Color.Black
            lvJTnjJobs.Items(tmpLastItemChecked).Checked = False
        End If
        '
        If lvJTnjJobs.CheckedItems.Count > 0 Then
            Dim tmpRow As Integer = CInt(lvJTnjJobs.CheckedItems(0).Index)
            tmpLastItemChecked = tmpRow
            lvJTnjJobs.Items(tmpRow).UseItemStyleForSubItems = False
            lvJTnjJobs.Items(tmpRow).SubItems(5).ForeColor = Color.Red
            lvJTnjJobs.Items(tmpRow).SubItems(6).ForeColor = Color.Red
        End If
    End Sub
    ''' <summary>
    ''' Sub to control gbox status for aup options. Switches controled by values of rbtns.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub rbtnJTnjPricingCostPlus_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTnjPricingCostPlus.CheckedChanged,
                                                                                                 rbtnJTnjPricingAUP.CheckedChanged,
                                                                                                 rbtnJTnjPricingCostPlusDeposits.CheckedChanged
        If rbtnJTnjPricingAUP.Checked = True Then
            gboxJTnjAUPsw.Visible = True
        Else
            gboxJTnjAUPsw.Visible = False
        End If
    End Sub
    ''' <summary>
    ''' Function to fill values for combo box for Customer (JobID).
    ''' </summary>
    ''' <returns>Number of customers added.</returns>
    Private Function JTnlcEFillcboxJobID() As Integer
        Dim tmpCount As Integer = 0
        cboxJTnjJobName.Items.Clear()
        Dim tmpJobSub As Integer = 0
        Dim tmpJobName As String = Nothing
        Dim tmpSubName As String = Nothing
        Dim tmpDeactDate As String = Nothing
        Dim tmpJobType As String = Nothing
        Dim tmpCstMeth As String = Nothing
        '
        Dim tmpLstName As String = Nothing
        '
        ' Filling combo box values for customer.
        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpDeactDate = "" Then
                If tmpJobName <> tmpLstName Then
                    cboxJTnjJobName.Items.Add(tmpJobName)
                    tmpLstName = tmpJobName
                    tmpCount += 1
                End If
            End If
            tmpJobSub += 1
        Loop
        '
        Return tmpCount
    End Function
End Class