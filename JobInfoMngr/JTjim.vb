﻿Imports System.IO
''' <summary>This class manages all job related, non-transactional data. It is home to the job information array. Its main panel (JTjim) is the main panel when working with a job.</summary>
Public Class JTjim
    ''' <summary>
    ''' Structure is for new JTCustomers.dat file information. It will take the place of information
    ''' previous;y stored in JTJobs. This sl and file is created as part of the work to eliminate 
    ''' redundant data ... in the process of eliminating the Job/Subjob concept.
    ''' </summary>
    Public Structure JTjimCustInfo
        Dim JTjimCIJobName As String
        Dim JTjimCITaxExemptJob As Boolean   ' new field 1/30/2018
        Dim JTjimCIInvPDF As String
        Dim JTjimCIInvUSPS As String
        Dim JTjimCIInvFile As String
        Dim JTjimCIBillStreet As String
        Dim JTjimCIBillCity As String
        Dim JTjimCIContact As String
        Dim JTjimCITelephone As String
        Dim JTjimCIEmail As String
        Dim JTjimCISecContact As String
        Dim JTjimCISecContPhone As String
        Dim JTjimCISecEmail As String      ' new 6/7/17
        Dim JTjimCIInvEmailPrim As String  ' new 6/7/17
        Dim JTjimCIInvEmailAlt As String   ' new 6/7/17
    End Structure

    ''' <summary>
    ''' This structure defines all non-transactional information for a job. It was originally defined 
    ''' for the master / subjob scenario. This is how it is currently. Shortly, it will be reprogrammed 
    ''' into a customer / job scenario. Where a customer can have many jobs. The jobs will be independanty 
    ''' of each other ... unlike the original master / subjob architecture. The only restriction is 
    ''' that each job must use identical customer (contact names, billing address, Etc.) Each job can 
    ''' be priced individually ... Cost+ v. AUP ... even hourly rates can be unique to each job. 
    ''' </summary>
    Public Structure JTjimJobInfo
        Dim JTjimJIJobName As String
        Dim JTjimJISubName As String
        Dim JTjimJIDescName As String
        Dim JTjimJIJobDetails As String
        Dim JTjimJITaxExemptJob As Boolean   ' new field 1/30/2018
        Dim JTjimJICreateDate As String
        Dim JTjimJINonStdFrmt As String   ' new 1/18/18  related to job having non-std invoice format
        Dim JTjimJIInvPDF As String
        Dim JTjimJIInvUSPS As String
        Dim JTjimJIInvFile As String
        Dim JTjimJIMatMargin As String
        Dim JTjimJISubMargin As String
        Dim JTjimJISpecJobPricing As String
        Dim JTjimJISJPRevDate As String
        Dim JTjimJIJobLocStreet As String
        Dim JTjimJIJobLocCity As String
        Dim JTjimJIJobinState As String
        Dim JTjimJIBillStreet As String
        Dim JTjimJIBillCity As String
        Dim JTjimJIContact As String
        Dim JTjimJITelephone As String
        Dim JTjimJIEmail As String
        Dim JTjimJISecContact As String
        Dim JTjimJISecContPhone As String
        Dim JTjimJISecEmail As String      ' new 6/7/17
        Dim JTjimJIInvEmailPrim As String  ' new 6/7/17
        Dim JTjimJIInvEmailAlt As String   ' new 6/7/17
        Dim JTjimJIJobPricMeth As String    ' new
        Dim JTjimJILastActDate As String    ' new
        Dim JTjimJIDeactDate As String      ' new 11/1/2017
        Dim JTjimJISndToAcct As String      ' new
        Dim JTjimJIAUPQuoted As String      ' new
        Dim JTjimJIAUPQueuedAmt As String   ' new
        Dim JTjimJIAUPInvQueued As String   ' new
        Dim JTjimJIAUPLabor As String     ' with AUP coding 8/1/17
        Dim JTjimJIAUPMat As String       ' with AUP coding 8/1/17
        Dim JTjimJIAUPServices As String  ' with AUP coding 8/1/17
    End Structure
    '
    Public tmpJimInfo As New JTjimJobInfo
    Public JTjimJIArray As New SortedList
    '
    Public tmpCustInfo As New JTjimCustInfo
    Public JTjimCIArray As New SortedList
    '

    ' --------------------------------------------
    ' Fields associated to Job Specific Rates
    ' --------------------------------------------
    ''' <summary>
    ''' This array stores customer/job specific pricing. It is assumed that most jobs will use global pricing, but if customer specific pricing is created, it will exist and be stored in this array. When this array is unloaded, it is stored in XML format with the job data.
    ''' </summary>
    Public Structure JTjimlaborcats
        Dim JTjimlcJobName As String
        Dim JTjimlcSubName As String
        Dim JTjimlcKey As String
        Dim JTjimlcName As String
        Dim JTjimlcStanRate As String
        Dim JTjimlcShopRate As String
    End Structure
    '
    Public JTjimJSRates As New JTjimlaborcats

    Public JTjimJSRsl As New SortedList
    '
    ' key is jobname+subname+labcat
    Public JTjimJSRkey As String
    ' -------------------------------------------
    ' -----------------------------------
    ' variables used in job file restore.
    ' -----------------------------------
    Public JTjimReadFileKey As String = ""
    Public JTjimReadFileData As String = ""
    '
    ' ===========================================================
    '
    ''' <summary>Handles cancel putton selection from JTjim panel.</summary>
    ''' <param name="sender">not used</param>
    ''' <param name="e">not used</param>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Hide()
        MsgBox("Changes made while in Job Information Screen have been discarded.")
        JTMainMenu.JTMMinit()
    End Sub
    ''' <summary>Validate entries made in work loc city state field. Blank is allowed ... if there is an entry must have state abrev.</summary>
    ''' <param name="sender">not used</param>
    ''' <param name="e">not used</param>
    Private Sub txtJTjimJobLocCity_LostFocus(sender As Object, e As EventArgs) Handles txtJTjimJobLocCity.LostFocus
        ' ----------------------------------------------------------
        ' Validate entries made in work loc city state field. Blank
        ' is allowed ... if there is an entry must have state abrev.
        ' ----------------------------------------------------------
        Dim tmplength As Double = txtJTjimJobLocCity.Text.Length - 2
        If txtJTjimJobLocCity.Text.Length = 0 Then
            Exit Sub
        End If
        '
        Do While CheckForAlphaCharacters(txtJTjimJobLocCity.Text.Substring(tmplength, 2)) = False
            tmplength -= 1
            If tmplength < 0 Then
                ToolTip1.Show("City, State(valid 2 letter Abrev.) and Zip Code Required for " & vbCrLf &
                              "job location. Correct and resubmit entry.(JI.007)", txtJTjimJobLocCity, 40, -55, 6000)
                txtJTjimJobLocCity.Select()
                Exit Sub
            End If
        Loop
        If convertState(txtJTjimJobLocCity.Text.Substring(tmplength, 2)) = "NOTFOUND" Then
            ToolTip1.Show("City, State(valid 2 letter Abrev.) and Zip Code Required for " & vbCrLf &
                              "job location. Correct and resubmit entry.(JI.007)", txtJTjimJobLocCity, 40, -55, 6000)
            txtJTjimJobLocCity.Select()
            Exit Sub
        End If
        txtJTjimJobLocState.Text = UCase(txtJTjimJobLocCity.Text.Substring(tmplength, 2))
        lblJTjimJobLocStateNme.Text = convertState(txtJTjimJobLocCity.Text.Substring(tmplength, 2))
        ' check for tax record
        If JTVarMaint.JTvmSTChkForRec(txtJTjimJobLocState.Text) = "NORECORD" Then
            MsgBox("Work location for this job is in a state that has no state sales tax record." & vbCrLf &
                   "A state tax record will be added. It will define the state as tax free." & vbCrLf &
                   "If your work is taxable in this state, go to Utilities/Global Settings" & vbCrLf &
                   "and update the state record with the appropriate settings and rates.")
            JTVarMaint.JTvmSTAddSkelRecord(txtJTjimJobLocState.Text)
        End If
    End Sub
    ''' <summary>Handles checkbox to make job location and bill to location the same.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkboxJTjimBillAddSwitch_CheckedChanged(sender As Object, e As EventArgs) Handles chkboxJTjimBillAddSwitch.CheckedChanged
        ' ------------------------------------------------------------
        ' Copied loc address to billing address if checked.
        ' ------------------------------------------------------------
        '
        If chkboxJTjimBillAddSwitch.Checked = True Then
            txtJTjimBillStreet.Text = txtJTjimJobLocStreet.Text
            txtJTjimBillCity.Text = txtJTjimJobLocCity.Text
        End If
    End Sub
    ''' <summary> Sub recieves a new Customer/Job combination and sets up JTjim panel to 
    ''' complete the new job setup.</summary>
    ''' <param name="JTjimNJJob">Customer</param>
    ''' <param name="JTjimNJSub">Job</param>
    ''' Sub recieves a new Customer/Job combination
    Public Sub JTjimNJinit(ByVal JTjimNJJob As String,
                           ByVal JTjimNJSub As String,
                           ByVal JTjimNJJobLocStreet As String,
                           ByVal JTjimNJJobLocCity As String,
                           ByVal JTjimNJJobPricing As String,
                           ByVal JTjimNJAUPLab As Boolean,
                           ByVal JTjimNJAUPMat As Boolean,
                           ByVal JTjimNJAUPSer As Boolean)
        ' ------------------------------------------------------------------------------
        ' Set up JTjim  panel to enter information for a new job or SubJob.
        ' ------------------------------------------------------------------------------
        '
        txtJTjimJob.Text = JTjimNJJob
        txtJTjimSubJob.Text = JTjimNJSub
        txtJtjimCreateDate.Text = Today
        txtJTjimMatMu.Text = JTVarMaint.JTvmMatMargin
        txtJTjimSubMu.Text = JTVarMaint.JTvmSubMargin
        txtJTjimJobDesc.Text = ""
        txtJTjimJobDet.Text = ""
        txtJTjimJobLocStreet.Text = JTjimNJJobLocStreet
        txtJTjimJobLocCity.Text = JTjimNJJobLocCity
        txtJTjimJobLocState.Text = ""
        '
        Select Case JTjimNJJobPricing
            Case "COST+"
                radiobtnJTjimCstPlus.Checked = True
            Case "COST+DEPOSITS"
                radiobtnJTjimCstPlusDeposits.Checked = True
                '          radiobtnJTjimAgreedUpon.Checked = True
                cboxJTjimAUPLabor.Checked = False
                cboxJTjimAUPMat.Checked = False
                cboxJTjimAUPServices.Checked = False
            Case "AUP"
                radiobtnJTjimAgreedUpon.Checked = True
                cboxJTjimAUPLabor.Checked = JTjimNJAUPLab
                cboxJTjimAUPMat.Checked = JTjimNJAUPMat
                cboxJTjimAUPServices.Checked = JTjimNJAUPSer
        End Select
        '
        chkboxJTjimInvPDF.Checked = True
        chkboxJTjimSndToAcct.Checked = True
        chkboxJTjimJobSpecRates.Checked = False
        chkboxJTjimBillAddSwitch.Checked = False
        cboxJTjimTaxExempt.Checked = False
        chkboxJTjimInvPDF.Checked = False
        chkboxJTjimInvUSPS.Checked = False
        chkboxJTjimInvFile.Checked = False
        ChkBoxInvPrim.Checked = False
        ChkBoxInvAlt.Checked = False
        '
        Dim tmpCustInfo As JTjimCustInfo = Nothing
        '
        If JTjimReadCustRec(tmpCustInfo, JTjimNJJob) = "DATA" Then
            txtJTjimBillStreet.Text = tmpCustInfo.JTjimCIBillStreet
            txtJTjimBillCity.Text = tmpCustInfo.JTjimCIBillCity
            '
            txtJTjimContact.Text = tmpCustInfo.JTjimCIContact
            txtJTjimTel.Text = tmpCustInfo.JTjimCITelephone
            txtJTjimEmail.Text = tmpCustInfo.JTjimCIEmail
            txtJTjimAltContact.Text = tmpCustInfo.JTjimCISecContact
            txtJTjimAltTel.Text = tmpCustInfo.JTjimCISecContPhone
            txtJTjimAltEmail.Text = tmpCustInfo.JTjimCISecEmail

            If tmpCustInfo.JTjimCIInvPDF = "X" Then
                chkboxJTjimInvPDF.Checked = True
            End If
            If tmpCustInfo.JTjimCIInvUSPS = "X" Then
                chkboxJTjimInvUSPS.Checked = True
            End If
            If tmpCustInfo.JTjimCIInvFile = "X" Then
                chkboxJTjimInvFile.Checked = True
            End If
            If tmpCustInfo.JTjimCIInvEmailPrim = "X" Then
                ChkBoxInvPrim.Checked = True
            End If
            If tmpCustInfo.JTjimCIInvEmailAlt = "X" Then
                ChkBoxInvAlt.Checked = True
            End If
            '
            cboxJTjimTaxExempt.Checked = tmpCustInfo.JTjimCITaxExemptJob

        Else
            txtJTjimBillStreet.Text = ""
            txtJTjimBillCity.Text = ""

            txtJTjimContact.Text = ""
            txtJTjimTel.Text = ""
            txtJTjimEmail.Text = ""
            txtJTjimAltContact.Text = ""
            txtJTjimAltTel.Text = ""
            txtJTjimAltEmail.Text = ""

        End If
        '
        ' Call sub to do initial Lab Rate lv fill.
        '
        JTjimFillLablv()
        '
        ' clear items in unbilled activity lv.
        '
        lvJTjimCurAct.Items.Clear()
        '
        ' clear any leftover invoice history
        '
        lvJTjimInvHist.Items.Clear()
        '
        HideJSLpanel()
        txtJTjimJobDesc.Select()
        tmpCustInfo = Nothing

        Me.Show()
    End Sub
    ''' <summary>Validates that a job description has been entered.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTjimJobDesc_LostFocus(sender As Object, e As EventArgs) Handles txtJTjimJobDesc.LostFocus
        txtJTjimJobDesc.Text = txtJTjimJobDesc.Text.ToUpper()
        If txtJTjimJobDesc.Text = "" Then
            ToolTip1.Show("Descriptive Job Name required", txtJTjimJobDesc, 40, -55, 6000)

            txtJTjimJobDesc.Select()
            Exit Sub
        Else
            txtErrMsg.Text = ""
        End If
        ' 
    End Sub
    ''' <summary>Loads labor pricing listview (lvJTjimLabRates) on the JTjim panel. If system 
    '''          rates are used, they are displayed. If specific job pricing has been created, 
    '''          it is loaded.</summary>
    Public Sub JTjimFillLablv()
        '
        If lvJTjimLabRates.Items.Count > 0 Then
            Dim tmpCounter As Double = lvJTjimLabRates.Items.Count - 1
            For tmpCounter = lvJTjimLabRates.Items.Count - 1 To 0 Step -1
                lvJTjimLabRates.Items.RemoveAt(tmpCounter)
            Next
        End If
        '
        Dim tmpJTPOS As Integer = 0
        Dim tmpJTLabCat As String = ""
        Dim tmpJTLabCatName As String = ""
        Dim tmpJTStanRate As String = ""
        Dim tmpJTShopRate As String = ""
        '
        Dim itmBuild(3) As String
        Dim lvLine As ListViewItem
        '
        Do While LaborCategories.JTlcReadFunc(tmpJTPOS, tmpJTLabCat, tmpJTLabCatName, tmpJTStanRate, tmpJTShopRate) = "DATA"

            '
            itmBuild(0) = tmpJTLabCat
            itmBuild(1) = String.Format("{0:0.00}", Val(tmpJTStanRate))
            itmBuild(2) = String.Format("{0:0.00}", Val(tmpJTShopRate))


            lvLine = New ListViewItem(itmBuild)
            lvJTjimLabRates.Items.Add(lvLine)
            tmpJTPOS += 1
        Loop

        If chkboxJTjimJobSpecRates.Checked = True Then
            For Each item As ListViewItem In lvJTjimLabRates.Items
                JTjimJSRkey = txtJTjimJob.Text & txtJTjimSubJob.Text & item.Text
                If JTjimJSRsl.ContainsKey(JTjimJSRkey) = True Then
                    item.SubItems.Item(1).Text = String.Format("{0:0.00}", Val(JTjimJSRsl(JTjimJSRkey).JTjimlcStanRate))
                    item.SubItems.Item(2).Text = String.Format("{0:0.00}", Val(JTjimJSRsl(JTjimJSRkey).JTjimlcShopRate))
                End If
            Next
        End If
        '
    End Sub
    ''' <summary>Calls utility (RegexUtilities) to validate that email entry is in valid email format.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTjimEmail_TextChanged(sender As Object, e As EventArgs) Handles txtJTjimEmail.LostFocus
        If txtJTjimEmail.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTjimEmail.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address Not valid. Correct And resubmit.(JI.004)", txtJTjimEmail, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub
    ''' <summary>Validates alternate email address to confirm valid email address format (same as primary email address).</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTjimAltEmail_TextChanged(sender As Object, e As EventArgs) Handles txtJTjimAltEmail.LostFocus
        If txtJTjimEmail.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJTjimAltEmail.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address Not valid. Correct And resubmit.(JI.004)", txtJTjimAltEmail, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub
    ''' <summary>
    ''' Button queued exit from JTjim panel. As part of exit logic, final field edits are completed,
    '''  (if new) a new job record is created in job sl, (if existing) all fields are updated in jobs 
    '''  sl, job sl is flushed to disk to commit to disk any changes made.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDone_Click(sender As Object, e As EventArgs) Handles btnDone.Click
        ' ---------------------------------------------------------------------------
        ' 1. Final validation of fields on Job Information panel prior to updating job.
        ' 2. If new job, create slot in job table and insert new job.
        ' 3. If existing job, update job table.
        ' 4. After all editing and array updating, Job array flush to update file.
        ' ---------------------------------------------------------------------------
        '
        ' Validation
        '
        txtErrMsg.Text = ""
        '
        If txtJTjimJobDesc.Text = "" Then
            ToolTip1.Show("A Job Description Is required.(JI.009)", txtJTjimJobDesc, 40, -60, 7000)
            txtJTjimJobDesc.Select()
            Exit Sub
        End If
        '
        If txtJTjimContact.Text = "" Then
            ToolTip1.Show("A Primary Job Contact Is required.(JI.010)", txtJTjimContact, 40, -60, 7000)
            txtJTjimContact.Select()
            Exit Sub
        End If
        If chkboxJTjimInvPDF.Checked = False And chkboxJTjimInvUSPS.Checked = False And chkboxJTjimInvFile.Checked = False Then
            ToolTip1.Show("No invoicing options were selected. Correct And Resubmit.(JI.002)", chkboxJTjimInvPDF, 40, -60, 7000)
            Exit Sub
        End If
        If chkboxJTjimInvPDF.Checked = True And txtJTjimEmail.Text = "" Then
            ToolTip1.Show("Email Invoice delivery selected And no email address input. Correct And Resubmit.(JI.012)", txtJTjimEmail, 40, -60, 7000)
            Exit Sub
        End If
        If chkboxJTjimInvUSPS.Checked = True And (txtJTjimBillCity.Text = "" Or txtJTjimBillStreet.Text = "") Then
            ToolTip1.Show("USPS Invoice delivery selected And no bill-to address input. Correct And Resubmit.(JI.013)", txtJTjimBillStreet, 40, -60, 7000)
            Exit Sub
        End If
        '
        If txtJTjimJobLocState.Text = "" Then
            ToolTip1.Show("No state shown in Job Location. Fix" & vbCrLf & "Job Location information. Correct And Resubmit.(JI.003)", txtJTjimJobLocState, 20, -75, 7000)
            Exit Sub
        End If
        '
        '
        Dim tmpJINewJobSub As String = ""
        '
        tmpJINewJobSub = FixedLengthString(txtJTjimJob.Text, 10, "_") & FixedLengthString(txtJTjimSubJob.Text, 10, "_")
        '
        If JTjimJIArray.ContainsKey(tmpJINewJobSub) Then
            JTjimtoarray(tmpJINewJobSub)
        Else
            Dim tmpJIRecArea As JTjimJobInfo = Nothing
            tmpJIRecArea.JTjimJIJobName = txtJTjimJob.Text
            tmpJIRecArea.JTjimJISubName = txtJTjimSubJob.Text
            '       JTjimJIArray.Add(tmpJINewJobSub, tmpJIRecArea)
            JTjimJIArrayAdd(tmpJINewJobSub, tmpJIRecArea, "JTjim~429 - btnDone")
            JTMainMenu.JTMMAddNewJobtoJTMMJobAct(tmpJIRecArea.JTjimJIJobName, tmpJIRecArea.JTjimJISubName)
            JTjimtoarray(tmpJINewJobSub)
        End If
        '
        Me.Hide()
        ' Write Jobs file from array data.
        JobArrayFlush()
        JTMainMenu.JTMMFirstTimeThru = "FIRST"
        If radiobtnJTjimAgreedUpon.Checked = True Or radiobtnJTjimCstPlusDeposits.Checked = True Then
            ' ------------------------------------------------------------------
            ' If AUP job, check to make sure AUP "I" entry exists.
            ' If no "I" entry exists force user to AUP panel.
            ' ------------------------------------------------------------------
            If JTaup.JTaupIentryChk(txtJTjimJob.Text, txtJTjimSubJob.Text) <> True Then
                JTjimCallAUP()
                Exit Sub
            End If
        End If
        '
        JTMainMenu.JTMMinit()
        '
    End Sub
    ''' <summary>After job to be selected has been determined this logic takes that job's data
    '''        from the jobs sl and populates the fields on JTjim.</summary>
    ''' <param name="tmpArraySub">This is the location within the jobs sl where the selected job is located.</param>
    Public Sub JTarraytojim(tmpArraySub As String, tmpCustName As String)
        ' ------------------------------------------------------------------------------
        ' This sub takes information from the JTjimJIArray and sets up JTjim panel.
        ' ------------------------------------------------------------------------------

        If JTjimCIArray.ContainsKey(tmpCustName) = False Then

            MsgBox("Custinfo Record doesn't exist (JI.008)" & vbCrLf &
                "Customer - >" & tmpCustName & "<" & vbCrLf &
                "Job - >" & JTjimJIArray(tmpArraySub).JTjimJISubName & "<")
            Dim tmpCustArrayItem As JTjimCustInfo = Nothing
            tmpCustArrayItem.JTjimCIJobName = tmpCustName
            JTjimCIArray.Add(tmpCustName, tmpCustArrayItem)
            tmpCustArrayItem = Nothing
        End If
        '
        txtJTjimJob.Text = JTjimJIArray(tmpArraySub).JTjimJIJobName
        txtJTjimSubJob.Text = JTjimJIArray(tmpArraySub).JTjimJISubName
        '
        txtJTjimJobDesc.Text = JTjimJIArray(tmpArraySub).JTjimJIDescName
        txtJTjimJobDet.Text = JTjimJIArray(tmpArraySub).JTjimJIJobDetails
        cboxJTjimTaxExempt.Checked = JTjimCIArray(tmpCustName).JTjimCITaxExemptJob
        '
        txtJtjimCreateDate.Text = JTjimJIArray(tmpArraySub).JTjimJICreateDate
        If JTjimJIArray(tmpArraySub).JTjimJINonStdFrmt = "X" Then
            chkboxJTjimNonStdFrmt.Checked = True
        Else
            chkboxJTjimNonStdFrmt.Checked = False
        End If
        If JTjimCIArray(tmpCustName).JTjimCIInvPDF = "X" Then
            chkboxJTjimInvPDF.Checked = True
        End If
        If JTjimCIArray(tmpCustName).JTjimCIInvUSPS = "X" Then
            chkboxJTjimInvUSPS.Checked = True
        End If
        If JTjimCIArray(tmpCustName).JTjimCIInvFile = "X" Then
            chkboxJTjimInvFile.Checked = True
        End If
        If JTjimJIArray(tmpArraySub).JTjimJISpecJobPricing = "X" Then
            lblJTjimPrcgRev.Visible = True
            txtJTjimSJPRevDate.Visible = True
            txtJTjimSJPRevDate.Text = JTjimJIArray(tmpArraySub).JTjimJISJPRevDate
        Else
            lblJTjimPrcgRev.Visible = False
            txtJTjimSJPRevDate.Visible = False
        End If
        txtJTjimMatMu.Text = JTjimJIArray(tmpArraySub).JTjimJIMatMargin
        txtJTjimSubMu.Text = JTjimJIArray(tmpArraySub).JTjimJISubMargin
        If JTjimJIArray(tmpArraySub).JTjimJISpecJobPricing = "X" Then
            chkboxJTjimJobSpecRates.Checked = True
        End If
        txtJTjimJobLocStreet.Text = JTjimJIArray(tmpArraySub).JTjimJIJobLocStreet
        txtJTjimJobLocCity.Text = JTjimJIArray(tmpArraySub).JTjimJIJobLocCity
        txtJTjimJobLocState.Text = JTjimJIArray(tmpArraySub).JTjimJIJobinState
        If txtJTjimJobLocState.Text <> "" Then
            lblJTjimJobLocStateNme.Text = convertState(txtJTjimJobLocState.Text)
        End If
        txtJTjimBillStreet.Text = JTjimCIArray(tmpCustName).JTjimCIBillStreet
        txtJTjimBillCity.Text = JTjimCIArray(tmpCustName).JTjimCIBillCity
        '
        txtJTjimContact.Text = JTjimCIArray(tmpCustName).JTjimCIContact
        txtJTjimTel.Text = JTjimCIArray(tmpCustName).JTjimCITelephone
        txtJTjimEmail.Text = JTjimCIArray(tmpCustName).JTjimCIEmail
        txtJTjimAltContact.Text = JTjimCIArray(tmpCustName).JTjimCISecContact
        txtJTjimAltTel.Text = JTjimCIArray(tmpCustName).JTjimCISecContPhone
        txtJTjimAltEmail.Text = JTjimCIArray(tmpCustName).JTjimCISecEmail
        If JTjimCIArray(tmpCustName).JTjimCIInvEmailPrim = "X" Then
            ChkBoxInvPrim.Checked = True
        End If
        If JTjimCIArray(tmpCustName).JTjimCIInvEmailAlt = "X" Then
            ChkBoxInvAlt.Checked = True
        End If
        ' ----- New fields
        Dim tmpAUPExcludes As Integer = 0
        If JTjimJIArray(tmpArraySub).JTjimJIJobPricMeth = "AUP" Then
            radiobtnJTjimAgreedUpon.Checked = True
            If JTjimJIArray(tmpArraySub).JTjimJIaupLabor = "X" Then
                cboxJTjimAUPLabor.Checked = True
            Else
                cboxJTjimAUPLabor.Checked = False
                tmpAUPExcludes += 1
            End If
            If JTjimJIArray(tmpArraySub).JTjimJIaupMat = "X" Then
                cboxJTjimAUPMat.Checked = True
            Else
                cboxJTjimAUPMat.Checked = False
                tmpAUPExcludes += 1
            End If
            If JTjimJIArray(tmpArraySub).JTjimJIaupServices = "X" Then
                cboxJTjimAUPServices.Checked = True
            Else
                cboxJTjimAUPServices.Checked = False
                tmpAUPExcludes += 1
            End If
            If tmpAUPExcludes = 3 Then
                radiobtnJTjimAgreedUpon.Checked = False
                radiobtnJTjimCstPlusDeposits.Checked = True
            End If
        Else
            radiobtnJTjimCstPlus.Checked = True
        End If
        If JTjimJIArray(tmpArraySub).JTjimJISndToAcct = "X" Then
            chkboxJTjimSndToAcct.Checked = True
        End If
        ' check for Deact status ...
        If JTjimJIArray(tmpArraySub).JTjimJIDeactdate <> "" Then
            labDeactDate.Text = "Inactive - " & JTjimJIArray(tmpArraySub).JTjimJIDeactdate
            labDeactDate.ForeColor = System.Drawing.Color.Red
        Else
            labDeactDate.Text = ""
        End If
        txtJTjimLastActDate.Text = JTjimJIArray(tmpArraySub).JTjimJILastActDate
        txtJTjimDeactDate.Text = JTjimJIArray(tmpArraySub).JTjimJIDeactDate
        txtJTjimAUPQuoted.Text = JTjimJIArray(tmpArraySub).JTjimJIAUPQuoted
        txtJTjimAUPQueueAmt.Text = JTjimJIArray(tmpArraySub).JTjimJIAUPQueuedAmt
        ' -----


    End Sub
    ''' <summary>
    ''' After the close buttom has been selected, this subs logic unloads the JTjim fields and updated the jobs sl. This sub is called from a few other places in cases where it is necessary to update job global information to the jobs sl prior to performing some other function such as invoicing.
    ''' </summary>
    ''' <param name="tmpArraySub">This is the location with the jobs sl for the job being processed.</param>
    Public Sub JTjimtoarray(tmpArraySub As String)
        ' ------------------------------------------------------------------------------
        ' Thus sub takes information from the JTjim panel and loads the JTjimJIArray.
        ' ------------------------------------------------------------------------------
        '

        Dim tmpJimInfo As New JTjimJobInfo
        Dim tmpCustInfo As New JTjimCustInfo

        tmpJimInfo.JTjimJIJobName = txtJTjimJob.Text
        tmpCustInfo.JTjimCIJobName = txtJTjimJob.Text
        '
        tmpJimInfo.JTjimJISubName = txtJTjimSubJob.Text
        tmpJimInfo.JTjimJIDescName = txtJTjimJobDesc.Text
        tmpJimInfo.JTjimJIJobDetails = txtJTjimJobDet.Text
        tmpCustInfo.JTjimCITaxExemptJob = cboxJTjimTaxExempt.Checked
        '
        tmpJimInfo.JTjimJICreateDate = txtJtjimCreateDate.Text
        '
        If chkboxJTjimNonStdFrmt.Checked = True Then
            tmpJimInfo.JTjimJINonStdFrmt = "X"
        Else
            tmpJimInfo.JTjimJINonStdFrmt = ""
        End If
        If chkboxJTjimInvPDF.Checked = True Then
            tmpCustInfo.JTjimCIInvPDF = "X"
        Else
            tmpCustInfo.JTjimCIInvPDF = ""
        End If
        If chkboxJTjimInvUSPS.Checked = True Then
            tmpCustInfo.JTjimCIInvUSPS = "X"
        Else
            tmpCustInfo.JTjimCIInvUSPS = ""
        End If
        If chkboxJTjimInvFile.Checked = True Then
            tmpCustInfo.JTjimCIInvFile = "X"
        Else
            tmpCustInfo.JTjimCIInvFile = ""
        End If
        ' ----- New Fields
        If radiobtnJTjimAgreedUpon.Checked = True Or radiobtnJTjimCstPlusDeposits.Checked = True Then
            tmpJimInfo.JTjimJIJobPricMeth = "AUP"
            If cboxJTjimAUPLabor.Checked = True Then
                tmpJimInfo.JTjimJIAUPLabor = "X"
            End If
            If cboxJTjimAUPMat.Checked = True Then
                tmpJimInfo.JTjimJIAUPMat = "X"
            End If
            If cboxJTjimAUPServices.Checked = True Then
                tmpJimInfo.JTjimJIAUPServices = "X"
            End If
        Else
            tmpJimInfo.JTjimJIJobPricMeth = "CSTP"
        End If
        If chkboxJTjimSndToAcct.Checked = True Then
            tmpJimInfo.JTjimJISndToAcct = "X"
        End If
        tmpJimInfo.JTjimJILastActDate = txtJTjimLastActDate.Text
        tmpJimInfo.JTjimJIDeactDate = txtJTjimDeactDate.Text
        tmpJimInfo.JTjimJIAUPQuoted = txtJTjimAUPQuoted.Text
        tmpJimInfo.JTjimJIAUPQueuedAmt = txtJTjimAUPQueueAmt.Text
        '
        tmpJimInfo.JTjimJIMatMargin = txtJTjimMatMu.Text
        tmpJimInfo.JTjimJISubMargin = txtJTjimSubMu.Text
        '
        ' logic to maintain job specific rate sortedlist
        '
        If chkboxJTjimJobSpecRates.Checked = True Then
            tmpJimInfo.JTjimJISJPRevDate = txtJTjimSJPRevDate.Text
            If tmpJimInfo.JTjimJISpecJobPricing = "" Then
                '
                ' add New jsp record
                '
                '               Dim tmplvSub As Integer = 0
                '
                For Each item As ListViewItem In lvJTjimLabRates.Items
                    JTjimJSRates.JTjimlcKey = item.Text
                    JTjimJSRates.JTjimlcStanRate = item.SubItems.Item(1).Text
                    JTjimJSRates.JTjimlcShopRate = item.SubItems.Item(2).Text
                    JTjimJSRates.JTjimlcJobName = txtJTjimJob.Text
                    JTjimJSRates.JTjimlcSubName = txtJTjimSubJob.Text
                    '
                    JTjimJSRkey = txtJTjimJob.Text & txtJTjimSubJob.Text & item.Text
                    ' ------------------------------------------------------------------------
                    ' Adding code to check for dup record on Job/Subs that are not supposed to 
                    ' have JSP previously. It won't hurt but should not be necessary.
                    ' Probably caused by Dup job problem that has been resolved.
                    ' ------------------------------------------------------------------------
                    If JTjimJSRsl.ContainsKey(JTjimJSRkey) Then
                        JTjimJSRsl.Remove(JTjimJSRkey)
                    End If
                    '
                    JTjimJSRsl.Add(JTjimJSRkey, JTjimJSRates)
                    '
                Next
                '
                tmpJimInfo.JTjimJISpecJobPricing = "X"
                '
                '          Else
                ' update jsp rates
                For Each item As ListViewItem In lvJTjimLabRates.Items
                    JTjimJSRkey = txtJTjimJob.Text & txtJTjimSubJob.Text & item.Text
                    Dim tmpStanRate As String = item.SubItems.Item(1).Text
                    Dim tmpShopRate As String = item.SubItems.Item(2).Text
                    If JTjimJSRsl.ContainsKey(JTjimJSRkey) = True Then
                        '
                        JTjimJSRsl.Remove(JTjimJSRkey)
                    End If
                    JTjimJSRates.JTjimlcKey = item.Text
                    JTjimJSRates.JTjimlcStanRate = tmpStanRate
                    JTjimJSRates.JTjimlcShopRate = tmpShopRate
                    JTjimJSRates.JTjimlcJobName = txtJTjimJob.Text
                    JTjimJSRates.JTjimlcSubName = txtJTjimSubJob.Text
                    '
                    JTjimJSRsl.Add(JTjimJSRkey, JTjimJSRates)
                Next
            End If
        Else
            If tmpJimInfo.JTjimJISpecJobPricing = "X" Then
                ' delete jsp items for this job
                For Each item As ListViewItem In lvJTjimLabRates.Items
                    JTjimJSRkey = txtJTjimJob.Text & txtJTjimSubJob.Text & item.Text
                    If JTjimJSRsl.ContainsKey(JTjimJSRkey) = True Then
                        JTjimJSRsl.Remove(JTjimJSRkey)
                    End If
                Next
            End If
            tmpJimInfo.JTjimJISpecJobPricing = ""
            tmpJimInfo.JTjimJISJPRevDate = Nothing
        End If

        '
        tmpJimInfo.JTjimJIJobLocStreet = txtJTjimJobLocStreet.Text
        tmpJimInfo.JTjimJIJobLocCity = txtJTjimJobLocCity.Text
        tmpJimInfo.JTjimJIJobinState = txtJTjimJobLocState.Text
        tmpCustInfo.JTjimCIBillStreet = txtJTjimBillStreet.Text
        tmpCustInfo.JTjimCIBillCity = txtJTjimBillCity.Text
        '
        tmpCustInfo.JTjimCIContact = txtJTjimContact.Text
        tmpCustInfo.JTjimCITelephone = txtJTjimTel.Text
        tmpCustInfo.JTjimCIEmail = txtJTjimEmail.Text
        tmpCustInfo.JTjimCISecContact = txtJTjimAltContact.Text
        tmpCustInfo.JTjimCISecContPhone = txtJTjimAltTel.Text
        tmpCustInfo.JTjimCISecEmail = txtJTjimAltEmail.Text
        If ChkBoxInvPrim.Checked = True Then
            tmpCustInfo.JTjimCIInvEmailPrim = "X"
        Else
            tmpCustInfo.JTjimCIInvEmailPrim = ""
        End If
        If ChkBoxInvAlt.Checked = True Then
            tmpCustInfo.JTjimCIInvEmailAlt = "X"
        Else
            tmpCustInfo.JTjimCIInvEmailAlt = ""
        End If
        '
        If JTjimCIArray.ContainsKey(tmpCustInfo.JTjimCIJobName) = True Then
            JTjimCIArray.Remove(tmpCustInfo.JTjimCIJobName)
        End If
        JTjimCIArray.Add(tmpCustInfo.JTjimCIJobName, tmpCustInfo)
        '
        JTjimJIArray.Remove(tmpArraySub)
        JTjimJIArrayAdd(tmpArraySub, tmpJimInfo, "JTjim~722 - JTjimToArray")
    End Sub
    ''' <summary>
    ''' This sub 'flushes' data to disk. The data associated with this flush is all
    '''        non-transactional job data, job specific pricing data and job specific
    '''        non-standard invoice formatting. This logic is called from various spots. It is used so data is safe from computer failures.
    ''' </summary>
    Public Sub JobArrayFlush()
        ' ------------------------------------------------------------------------------------------
        ' Call function to FLUSH Comtomer info  JTjimCIArray ----> JTCustomers.dat
        ' ------------------------------------------------------------------------------------------
        CustArrayFlush()
        '
        ' ---------------------------------------------------------------------------
        ' This sub reformat job data into XML sentences and write the data to a file.
        ' ---------------------------------------------------------------------------
        Dim fileRecord As String = ""
        Dim tmpJIarray As Integer = 0
        '
        '
        '
        Dim keys As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTJobs.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTJobs.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            '


            '
            For Each k In keys

                If JTjimJIArray(k).JTjimJIJobName <> "" Then
                    fileRecord = "<Job>"
                    writeFile.WriteLine(fileRecord)
                    fileRecord = "<JobName>" & JTjimJIArray(k).JTjimJIJobName & "</JobName>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJISubName <> "" Then
                    fileRecord = "<SubName>" & JTjimJIArray(k).JTjimJISubName & "</SubName>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIDescName <> "" Then
                    fileRecord = "<DescName>" & JTjimJIArray(k).JTjimJIDescName & "</DescName>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIJobDetails <> "" Then
                    fileRecord = "<JobDetails>" & JTjimJIArray(k).JTjimJIJobDetails & "</JobDetails>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If JTjimJIArray(k).JTjimJICreateDate <> "" Then
                    fileRecord = "<CreateDate>" & JTjimJIArray(k).JTjimJICreateDate & "</CreateDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If JTjimJIArray(k).JTjimJILastActDate <> "" Then
                    fileRecord = "<LastActDate>" & JTjimJIArray(k).JTjimJILastActDate & "</LastActDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If JTjimJIArray(k).JTjimJINonStdFrmt <> "" Then
                    fileRecord = "<NonStdFrmt>" & JTjimJIArray(k).JTjimJINonStdFrmt & "</NonStdFrmt>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIMatMargin <> "" Then
                    fileRecord = "<MatMargin>" & JTjimJIArray(k).JTjimJIMatMargin & "</MatMargin>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJISubMargin <> "" Then
                    fileRecord = "<SubMargin>" & JTjimJIArray(k).JTjimJISubMargin & "</SubMargin>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJISpecJobPricing <> "" Then
                    fileRecord = "<SpecJobPricing>" & JTjimJIArray(k).JTjimJISpecJobPricing & "</SpecJobPricing>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJISJPRevDate <> "" Then
                    fileRecord = "<SJPRevDate>" & JTjimJIArray(k).JTjimJISJPRevDate & "</SJPRevDate>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIJobLocStreet <> "" Then
                    fileRecord = "<LocStreet>" & JTjimJIArray(k).JTjimJIJobLocStreet & "</LocStreet>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIJobLocCity <> "" Then
                    fileRecord = "<LocCity>" & JTjimJIArray(k).JTjimJIJobLocCity & "</LocCity>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIJobinState <> "" Then
                    fileRecord = "<JobinState>" & JTjimJIArray(k).JTjimJIJobinState & "</JobinState>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                If JTjimJIArray(k).JTjimJIJobPricMeth <> "" Then
                    fileRecord = "<JobPricMeth>" & JTjimJIArray(k).JTjimJIJobPricMeth & "</JobPricMeth>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJISndToAcct <> "" Then
                    fileRecord = "<SndToAcct>" & JTjimJIArray(k).JTjimJISndToAcct & "</SndToAcct>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIAUPQuoted <> "" Then
                    fileRecord = "<AUPQuoted>" & JTjimJIArray(k).JTjimJIAUPQuoted & "</AUPQuoted>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIAUPQueuedAmt <> "" Then
                    fileRecord = "<AUPQueuedAmt>" & JTjimJIArray(k).JTjimJIAUPQueuedAmt & "</AUPQueuedAmt>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIAUPInvQueued <> "" Then
                    fileRecord = "<AUPInvQueued>" & JTjimJIArray(k).JTjimJIAUPInvQueued & "</AUPInvQueued>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimJIArray(k).JTjimJIDeactDate <> "" Then
                    fileRecord = "<DeactDate>" & JTjimJIArray(k).JTjimJIDeactDate & "</DeactDate>"
                    writeFile.WriteLine(fileRecord)
                End If

                '
                If JTjimJIArray(k).JTjimJIJobPricMeth = "AUP" Then
                    If JTjimJIArray(k).JTjimJIAUPLabor <> "" Then
                        fileRecord = "<AUPLabor>" & JTjimJIArray(k).JTjimJIAUPLabor & "</AUPLabor>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    If JTjimJIArray(k).JTjimJIAUPMat <> "" Then
                        fileRecord = "<AUPMat>" & JTjimJIArray(k).JTjimJIAUPMat & "</AUPMat>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    If JTjimJIArray(k).JTjimJIAUPServices <> "" Then
                        fileRecord = "<AUPServices>" & JTjimJIArray(k).JTjimJIAUPServices & "</AUPServices>"
                        writeFile.WriteLine(fileRecord)
                    End If
                End If
                ' -----

                fileRecord = "</Job>"
                writeFile.WriteLine(fileRecord)

            Next
            ' -----------------------------------------------------------------
            ' Logic to flush Job Specific Pricing to disk.
            ' It is exported in XML format to the same file 
            ' that Contains the job information.
            ' -----------------------------------------------------------------
            '            tmpTempSub = 0
            ' 
            ' Gets the list of keys And the list of values.
            Dim tmpKeyList As IList = JTjimJSRsl.GetKeyList()
            Dim i As Integer


            For i = 0 To JTjimJSRsl.Count - 1
                '                '
                fileRecord = "<JobSpecRates>"
                writeFile.WriteLine(fileRecord)
                '

                fileRecord = "<JSRJobID>" & JTjimJSRsl(tmpKeyList(i)).jtjimlcJobName & "</JSRJobID>"
                writeFile.WriteLine(fileRecord)
                '

                fileRecord = "<JSRSubID>" & JTjimJSRsl(tmpKeyList(i)).jtjimlcsubName & "</JSRSubID>"
                writeFile.WriteLine(fileRecord)
                '

                fileRecord = "<JSRLabCat>" & JTjimJSRsl(tmpKeyList(i)).jtjimlcKey & "</JSRLabCat>"
                writeFile.WriteLine(fileRecord)
                '

                fileRecord = "<JSRStanRate>" & JTjimJSRsl(tmpKeyList(i)).jtjimlcStanRate & "</JSRStanRate>"
                writeFile.WriteLine(fileRecord)
                '

                fileRecord = "<JSRShopRate>" & JTjimJSRsl(tmpKeyList(i)).jtjimlcShopRate & "</JSRShopRate>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "</JobSpecRates>"
                writeFile.WriteLine(fileRecord)
                '
            Next i
            '
            ' -----------------------------------------------------------------
            ' Logic to flush Non-Std Invoice Format settings to disk.
            ' It is exported in XML format to the same file 
            ' that Contains the job information and Job Specific Pricing data.
            ' ----------------------------------------------------------------
            Dim NSFSub As Integer = 0
            Dim NSFArrayItem As New JTif.JTifslStruct
            Do While JTif.JTifGetArrayLine(NSFSub, NSFArrayItem) = "DATA"
                fileRecord = "<NonStdInvFrmt>"
                writeFile.WriteLine(fileRecord)
                fileRecord = "<NSIFJob>" & NSFArrayItem.Job & "</NSIFJob>"
                writeFile.WriteLine(fileRecord)
                fileRecord = "<NSIFSubjob>" & NSFArrayItem.Subjob & "</NSIFSubjob>"
                writeFile.WriteLine(fileRecord)
                If NSFArrayItem.InvLabTaskSummary = True Then
                    fileRecord = "<NSIFInvLabTaskSummary>" & "X" & "</NSIFInvLabTaskSummary>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvLabWorkerDetail = True Then
                    fileRecord = "<NSIFInvLabWorkerDetail>" & "X" & "</NSIFInvLabWorkerDetail>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvLabTotals = True Then
                    fileRecord = "<NSIFInvLabTotals>" & "X" & "</NSIFInvLabTotals>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvSubSerDetails = True Then
                    fileRecord = "<NSIFInvSubSerDetails>" & "X" & "</NSIFInvSubSerDetails>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvSubSerTotals = True Then
                    fileRecord = "<NSIFInvSubSerTotals>" & "X" & "</NSIFInvSubSerTotals>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvMatDetails = True Then
                    fileRecord = "<NSIFInvMatDetails>" & "X" & "</NSIFInvMatDetails>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvMatTotals = True Then
                    fileRecord = "<NSIFInvMatTotals>" & "X" & "</NSIFInvMatTotals>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvMatShowCost = True Then
                    fileRecord = "<NSIFInvMatShowCost>" & "X" & "</NSIFInvMatShowCost>"
                    writeFile.WriteLine(fileRecord)
                End If
                If NSFArrayItem.InvSubShowCost = True Then
                    fileRecord = "<NSIFInvSubShowCost>" & "X" & "</NSIFInvSubShowCost>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
                fileRecord = "</NonStdInvFrmt>"
                writeFile.WriteLine(fileRecord)
                '
                NSFSub += 1
                '
            Loop
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTJobs.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTJobs.Bkup")
        End If
        ' ------------------------------------------------------------------------

    End Sub
    ''' <summary>This sub reads xml data from JTJobInfo and reloads the sl array. Any 
    '''          data previously in the array is cleared prior to processing.</summary>
    Public Sub JTjimJobArrayRestore()
        ' ---------------------------------------------------------------------------
        ' Call function to restore JTjimJIArray (sl).
        ' ---------------------------------------------------------------------------
        JTjimCustArrayRestore()
        ' -----------------------------------------------------------------------------
        ' This sub read the JOBS XML data and rebuilds jimArray.
        ' -----------------------------------------------------------------------------
        Dim tmpJIarray As Integer = 0
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        '
        '
        Dim tmpRecordType As String = ""
        Dim JThrWhichField As Integer = 1

        ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTJobs.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTJobs.Bkup") Then
            MsgBox("JTJobs --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (JI.016")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTJobs.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTJobs.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTJobs.Bkup", "JTJobs.dat")
        End If
        ' ------------------------------------------------------------------

        '
        Dim tmpJimInfo As New JTjimJobInfo
        Dim tmpBlankInfo As New JTjimJobInfo
        '
        ' ------------------------------------------------------
        ' Set up item to add NSIF array items. Also call funct
        ' to clear slJTif array.
        ' ------------------------------------------------------
        Dim NSFArrayItem As New JTif.JTifslStruct
        JTjimJIArray.Clear()
        JTjimJSRsl.Clear()
        JTif.JTifClrArray()
        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTjimReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTjimReadFileKey = "Job" Or
                        JTjimReadFileKey = "/Job" Or
                        JTjimReadFileKey = "JobSpecRates" Or
                        JTjimReadFileKey = "/JobSpecRates" Or
                        JTjimReadFileKey = "NonStdInvFrmt" Or
                        JTjimReadFileKey = "/NonStdInvFrmt" Then
                        JTjimReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTjimReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case JTjimReadFileKey
                        Case = "Job"
                            tmpRecordType = "Job"
                        Case = "JobSpecRates"
                            tmpRecordType = "JobSpecRates"
                        Case = "NonStdInvFrmt"
                            tmpRecordType = "NonStdInvFrmt"
                    End Select
                    '
                    '
                    If tmpRecordType = "Job" Then
                        Select Case JTjimReadFileKey

                            Case "JobName"
                                tmpJimInfo.JTjimJIJobName = JTjimReadFileData
                            Case "SubName"
                                tmpJimInfo.JTjimJISubName = JTjimReadFileData
                            Case "DescName"
                                tmpJimInfo.JTjimJIDescName = JTjimReadFileData
                            Case "JobDetails"
                                tmpJimInfo.JTjimJIJobDetails = JTjimReadFileData
                            Case "TaxExemptJob"
                                tmpJimInfo.JTjimJITaxExemptJob = JTjimReadFileData
                            Case "CreateDate"
                                tmpJimInfo.JTjimJICreateDate = JTjimReadFileData
                            Case "NonStdFrmt"
                                tmpJimInfo.JTjimJINonStdFrmt = JTjimReadFileData
                            Case "InvPDF"
                                tmpJimInfo.JTjimJIInvPDF = JTjimReadFileData
                            Case "InvUSPS"
                                tmpJimInfo.JTjimJIInvUSPS = JTjimReadFileData
                            Case "InvFile"
                                tmpJimInfo.JTjimJIInvFile = JTjimReadFileData
                            Case "MatMargin"
                                tmpJimInfo.JTjimJIMatMargin = JTjimReadFileData
                            Case "SubMargin"
                                tmpJimInfo.JTjimJISubMargin = JTjimReadFileData
                            Case "SpecJobPricing"
                                tmpJimInfo.JTjimJISpecJobPricing = JTjimReadFileData
                            Case "SJPRevDate"
                                tmpJimInfo.JTjimJISJPRevDate = JTjimReadFileData

                            Case "LocStreet"
                                tmpJimInfo.JTjimJIJobLocStreet = JTjimReadFileData
                            Case "LocCity"
                                tmpJimInfo.JTjimJIJobLocCity = JTjimReadFileData
                            Case "JobinState"
                                tmpJimInfo.JTjimJIJobinState = JTjimReadFileData
                            Case "BillStreet"
                                tmpJimInfo.JTjimJIBillStreet = JTjimReadFileData
                            Case "BillCity"
                                tmpJimInfo.JTjimJIBillCity = JTjimReadFileData
                            Case "Contact"
                                tmpJimInfo.JTjimJIContact = JTjimReadFileData
                            Case "Telephone"
                                tmpJimInfo.JTjimJITelephone = JTjimReadFileData
                            Case "Email"
                                tmpJimInfo.JTjimJIEmail = JTjimReadFileData
                            Case "SecContact"
                                tmpJimInfo.JTjimJISecContact = JTjimReadFileData
                            Case "SecContPhone"
                                tmpJimInfo.JTjimJISecContPhone = JTjimReadFileData
                            Case "SecEmail"
                                tmpJimInfo.JTjimJISecEmail = JTjimReadFileData
                            Case "InvEmailPrim"
                                tmpJimInfo.JTjimJIInvEmailPrim = JTjimReadFileData
                            Case "InvEmailAlt"
                                tmpJimInfo.JTjimJIInvEmailAlt = JTjimReadFileData
                            Case "JobPricMeth"
                                tmpJimInfo.JTjimJIJobPricMeth = JTjimReadFileData
                            Case "LastActDate"
                                tmpJimInfo.JTjimJILastActDate = JTjimReadFileData
                            Case "DeactDate"
                                tmpJimInfo.JTjimJIDeactDate = JTjimReadFileData
                            Case "SndToAcct"
                                tmpJimInfo.JTjimJISndToAcct = JTjimReadFileData
                            Case "AUPQuoted"
                                tmpJimInfo.JTjimJIAUPQuoted = JTjimReadFileData
                            Case "AUPQueuedAmt"
                                tmpJimInfo.JTjimJIAUPQueuedAmt = JTjimReadFileData
                            Case "AUPInvQueued"
                                tmpJimInfo.JTjimJIAUPInvQueued = JTjimReadFileData
                            Case "AUPLabor"
                                tmpJimInfo.JTjimJIAUPLabor = JTjimReadFileData
                            Case "AUPMat"
                                tmpJimInfo.JTjimJIAUPMat = JTjimReadFileData
                            Case "AUPServices"
                                tmpJimInfo.JTjimJIAUPServices = JTjimReadFileData
                            Case "/Job"
                                ' ---------------------------------------------------------
                                ' Check to make sure there is a job creation date. If blank
                                ' plug with today's date.
                                ' ---------------------------------------------------------
                                If tmpJimInfo.JTjimJICreateDate = Nothing Then
                                    tmpJimInfo.JTjimJICreateDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
                                End If
                                ' ----------------------------------
                                Dim tmpJobName As String = FixedLengthString(tmpJimInfo.JTjimJIJobName, 10, "_")
                                Dim tmpSubName As String = FixedLengthString(tmpJimInfo.JTjimJISubName, 10, "_")
                                Dim tmpKey As String = tmpJobName & tmpSubName
                                '
                                If JTjimJIArray.ContainsKey(tmpKey) Then
                                    MsgBox("Duplicate Job found - " & tmpJimInfo.JTjimJIJobName & "/" & tmpJimInfo.JTjimJISubName)
                                Else
                                    '           JTjimJIArray.Add(tmpKey, tmpJimInfo)
                                    JTjimJIArrayAdd(tmpKey, tmpJimInfo, "JTjim~1134 - JTjimJobArrayRestore")
                                End If

                                tmpJimInfo = tmpBlankInfo

                        End Select
                    End If
                    '
                    If tmpRecordType = "JobSpecRates" Then
                        Select Case JTjimReadFileKey
                            Case "JSRJobID"
                                JTjimJSRates.JTjimlcJobName = JTjimReadFileData
                            Case "JSRSubID"
                                JTjimJSRates.JTjimlcSubName = JTjimReadFileData
                            Case "JSRLabCat"
                                JTjimJSRates.JTjimlcKey = JTjimReadFileData
                            Case "JSRStanRate"
                                JTjimJSRates.JTjimlcStanRate = JTjimReadFileData
                            Case "JSRShopRate"
                                JTjimJSRates.JTjimlcShopRate = JTjimReadFileData
                            Case "/JobSpecRates"
                                JTjimJSRates.JTjimlcName = ""
                                JTjimJSRkey = JTjimJSRates.JTjimlcJobName & JTjimJSRates.JTjimlcSubName & JTjimJSRates.JTjimlcKey
                                JTjimJSRsl.Add(JTjimJSRkey, JTjimJSRates)
                        End Select
                        '
                    End If
                    '
                    If tmpRecordType = "NonStdInvFrmt" Then
                        Select Case JTjimReadFileKey
                            Case "NonStdInvFrmt"
                                NSFArrayItem.InvLabTaskSummary = False
                                NSFArrayItem.InvLabWorkerDetail = False
                                NSFArrayItem.InvLabTotals = False
                                NSFArrayItem.InvSubSerDetails = False
                                NSFArrayItem.InvSubSerTotals = False
                                NSFArrayItem.InvMatDetails = False
                                NSFArrayItem.InvMatTotals = False
                                NSFArrayItem.InvMatShowCost = False
                                NSFArrayItem.InvSubShowCost = False

                            Case "NSIFJob"
                                NSFArrayItem.Job = JTjimReadFileData
                            Case "NSIFSubjob"
                                NSFArrayItem.Subjob = JTjimReadFileData
                            Case "NSIFInvLabTaskSummary"
                                NSFArrayItem.InvLabTaskSummary = True
                            Case "NSIFInvLabWorkerDetail"
                                NSFArrayItem.InvLabWorkerDetail = True
                            Case "NSIFInvLabTotals"
                                NSFArrayItem.InvLabTotals = True
                            Case "NSIFInvSubSerDetails"
                                NSFArrayItem.InvSubSerDetails = True
                            Case "NSIFInvSubSerTotals"
                                NSFArrayItem.InvSubSerTotals = True
                            Case "NSIFInvMatDetails"
                                NSFArrayItem.InvMatDetails = True
                            Case "NSIFInvMatTotals"
                                NSFArrayItem.InvMatTotals = True
                            Case "NSIFInvMatShowCost"
                                NSFArrayItem.InvMatShowCost = True
                            Case "NSIFInvSubShowCost"
                                NSFArrayItem.InvSubShowCost = True
                            Case "/NonStdInvFrmt"
                                JTif.JTifAddArrayLine(NSFArrayItem)
                        End Select
                    End If
                End If
                '
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        ' ----------------------------------------------------------------------------------------
        ' Check to see if there are records in the JTjimCIArray. If there aren't any, this is the 
        ' first time this array has been used --- the initial load will take data from JTjimJIArray.
        ' --------------- This code can be deleted after CI conversion is done. --------------------
        ' Commented out code --- 7/19/2020 --- delete it after a period.
        ' ----------------------------------------------------------------------------------------
        '   If JTjimCIArray.Count = 0 Then
        '  MsgBox("Creating initial JTCustomers file." & vbCrLf &
        '            "This routine will only run once as part of the conversion from Job/Subjob" & vbCrLf &
        '           "to the Customer/Job Scenario.")
        'Dim tmpCIItem As JTjimCustInfo = Nothing
        '       Dim keys As ICollection = JTjimJIArray.Keys
        '      Dim k As String = ""
        '     For Each k In keys
        '    If JTjimCIArray.ContainsKey(JTjimJIArray(k).JTjimJIJobName) = False Then
        '   tmpCIItem.JTjimCIJobName = JTjimJIArray(k).JTjimJIJobName
        '              tmpCIItem.JTjimCITaxExemptJob = JTjimJIArray(k).JTjimJITaxExemptJob
        '  tmpCIItem.JTjimCIInvPDF = JTjimJIArray(k).JTjimJIInvPDF
        '        tmpCIItem.JTjimCIInvUSPS = JTjimJIArray(k).JTjimJIInvUSPS
        '       tmpCIItem.JTjimCIInvFile = JTjimJIArray(k).JTjimJIInvFile
        '      tmpCIItem.JTjimCIBillStreet = JTjimJIArray(k).JTjimJIBillStreet
        '     tmpCIItem.JTjimCIBillCity = JTjimJIArray(k).JTjimJIBillCity
        '    tmpCIItem.JTjimCIContact = JTjimJIArray(k).JTjimJIContact
        '   tmpCIItem.JTjimCITelephone = JTjimJIArray(k).JTjimJITelephone
        '  tmpCIItem.JTjimCIEmail = JTjimJIArray(k).JTjimJIEmail
        '        tmpCIItem.JTjimCISecContact = JTjimJIArray(k).JTjimJISecContact
        '       tmpCIItem.JTjimCISecContPhone = JTjimJIArray(k).JTjimJISecContPhone
        '      tmpCIItem.JTjimCISecEmail = JTjimJIArray(k).JTjimJISecEmail
        '     tmpCIItem.JTjimCIInvEmailPrim = JTjimJIArray(k).JTjimJIInvEmailPrim
        '    tmpCIItem.JTjimCIInvEmailAlt = JTjimJIArray(k).JTjimJIInvEmailAlt
        '   JTjimCIArray.Add(tmpCIItem.JTjimCIJobName, tmpCIItem)
        '  tmpCIItem = Nothing
        '     End If
        '      Next
        '    
        '    CustArrayFlush()

        '    End If
        '
    End Sub
    ''' <summary>
    ''' This sub organizes and calls the routines necessary to populate the JTjim panel 
    ''' for a job. As part of the process, TK and NLC data is processed to display a current 
    ''' status for this job on the panel. It also displays a list of prior invoices produced.
    ''' </summary>
    ''' <param name = "tmpJobName" > Job (more accurately called customer) for the panel to be displayed.</param>
    ''' <param name = "tmpSubName" > Subjob (more accurately called Job) for the panel to be displayed.</param>

    Public Sub JTjimPopPanel(ByRef tmpJobName As String, ByRef tmpSubName As String)
        ' ------------------------------------------------------------------------
        ' Sub to move and interpret values in jim array to user panel for 
        ' review Or updating.
        ' ------------------------------------------------------------------------
     Dim tmpJobName2 As String = FixedLengthString(tmpJobName, 10, "_")

        Dim tmpSubName2 As String = FixedLengthString(tmpSubName, 10, "_")
        Dim tmpArraySub As String = tmpJobName2 & tmpSubName2

        '
        Dim tmpCustName As String = tmpJobName
        '

        '
        JTarraytojim(tmpArraySub, tmpCustName)
        ' -----------------------------------------------------------------------------------
        ' Retrieve data and populate lvJTjimInvHist.
        ' -----------------------------------------------------------------------------------
        Dim tmpCumInvTotals As Long = Nothing
        lvJTjimInvHist.Items.Clear()
        Dim tmpInvDate As String = "FIRST"
        Dim tmpInvAmt As String = Nothing
        Dim tmpSub As Integer = 0
        Dim itmBuild(2) As String
        Dim lvLine As ListViewItem
        Do While JTas.JTasReturnInvHist(tmpsub, txtJTjimJob.Text, txtJTjimSubJob.Text, tmpInvDate, tmpInvAmt) = "DATA"
            tmpCumInvTotals += Val(tmpInvAmt)
            itmBuild(0) = tmpInvDate
            itmBuild(1) = String.Format("{0:0.00}", Val(tmpInvAmt))
            lvLine = New ListViewItem(itmBuild)
            lvJTjimInvHist.Items.Add(lvLine)
        Loop
        If lvJTjimInvHist.Items.Count = 0 Then
            itmBuild(0) = "No Invoice History"
            itmBuild(1) = ""
            lvLine = New ListViewItem(itmBuild)
            lvJTjimInvHist.Items.Add(lvLine)
        End If
        ' -------------------------------------------------------------------------
        ' If job has an invoice history with amount, lock ability to change pricing
        ' method by disabling radio buttons.
        ' -------------------------------------------------------------------------
        If tmpCumInvTotals <> 0 Then
            radiobtnJTjimAgreedUpon.Enabled = False
            radiobtnJTjimCstPlus.Enabled = False
            radiobtnJTjimCstPlusDeposits.Enabled = False
            cboxJTjimAUPLabor.Enabled = False
            cboxJTjimAUPMat.Enabled = False
            cboxJTjimAUPServices.Enabled = False
        Else
            radiobtnJTjimAgreedUpon.Enabled = True
            radiobtnJTjimCstPlus.Enabled = True
            radiobtnJTjimCstPlusDeposits.Enabled = True
            cboxJTjimAUPLabor.Enabled = True
            cboxJTjimAUPMat.Enabled = True
            cboxJTjimAUPServices.Enabled = True
        End If
        '
        ' ---------------------------------------------------------------------
        ' Retrieve and load data into lvJTjimCurAct listview.
        ' Not yet invoiced activity...
        ' -------------------------------------------------------------------
        '
        Dim tmpNotInvLab As Double = 0
        Dim tmpNotInvMat As Double = 0
        Dim tmpNotInvSub As Double = 0
        '
        lvJTjimCurAct.Items.Clear()
        '
        JTMainMenu.JTMMNotInvValues(tmpJobName, tmpSubName, tmpNotInvLab, tmpNotInvMat, tmpNotInvSub)
        '
        '
        itmBuild(0) = "Labor"
        If tmpNotInvLab = 99999999 Then
            itmBuild(1) = "DEACT"
        Else
            itmBuild(1) = String.Format("{0:0.00}", tmpNotInvLab)
        End If
        lvLine = New ListViewItem(itmBuild)
        lvJTjimCurAct.Items.Add(lvLine)
        '
        itmBuild(0) = "Materials"
        itmBuild(1) = String.Format("{0:0.00}", tmpNotInvMat)
        lvLine = New ListViewItem(itmBuild)
        lvJTjimCurAct.Items.Add(lvLine)
        '
        itmBuild(0) = "SubContractors"
        itmBuild(1) = String.Format("{0:0.00}", tmpNotInvSub)
        lvLine = New ListViewItem(itmBuild)
        lvJTjimCurAct.Items.Add(lvLine)
        '
        itmBuild(0) = "Total"
        If tmpNotInvLab = 99999999 Then
            itmBuild(1) = String.Format("{0:0.00}", 0)
        Else
            itmBuild(1) = String.Format("{0:0.00}", tmpNotInvLab + tmpNotInvMat + tmpNotInvSub)
        End If
        lvLine = New ListViewItem(itmBuild)
        lvJTjimCurAct.Items.Add(lvLine)
        ' ---------------------------------------------------------------------------
        JTjimFillLablv()
        HideJSLpanel()
        txtJTjimJobDesc.Select()
        Me.GroupBox6.BringToFront()
        If radiobtnJTjimAgreedUpon.Checked = True Or radiobtnJTjimCstPlusDeposits.Checked = True Then
            JTaup.JTaupFillJTjim(tmpJobName, tmpSubName)
        End If
        JTstart.JTstartMenuOff()
        Me.MdiParent = JTstart
        Me.Show()
        ' -----------------------------------------------------------------------------------------------
        ' Code added to check Spec Job Pricing to see if it still applies. Many job have special pricing
        ' turned on and not really being used. This code will check the switch and review date. If it is
        ' on and if the review date is either blank or more than a year old, the system will ask if SJP
        ' is still appropriate ... yes updates the review date ... no turns off SJP.
        ' ----------------------------------------------------------------------------------------------
        If chkboxJTjimJobSpecRates.Checked = True Then
            Dim tmpDatePlusOneYr As Date = Date.Today.AddYears(1)
            ' fill review date, if its never been populated
            If txtJTjimSJPRevDate.Text = "" Then
                txtJTjimSJPRevDate.Text = txtJtjimCreateDate.Text
            End If
            '
            Dim tmpRevDate As Date = Convert.ToDateTime(txtJTjimSJPRevDate.Text)
            If tmpRevDate.AddYears(1) < Date.Today Then
                Dim tmpMsgboxText As String =
                    "Job has Job Specific Pricing activated. The pricing is > one year old." & vbCrLf &
                    "If Job Specific Pricing is still appropriate for this job, hit 'YES'" & vbCrLf &
                    "(Individual Job Category Rates can be modified at any time). If Job" & vbCrLf &
                    "Specific Pricing is not longer required, hit 'NO'. This will turn the" & vbCrLf &
                    "JSP option off and reset all associated fields. (JI.015)"
                Dim tmpAns As Integer = MsgBox(tmpMsgboxText, vbYesNo, "J.I.M. - Job Specific Pricing Review")
                If tmpAns = vbYes Then
                    txtJTjimSJPRevDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
                Else
                    chkboxJTjimJobSpecRates.Checked = False
                    txtJTjimSJPRevDate.Text = Nothing
                End If

            End If


        End If
    End Sub
    ''' <summary>This sub is queued by a double-click of an item in the job labor rates lv on the JTjim panel. It presents routines to create JSP (job specific pricing) for a labor category.</summary>
    ''' <param name="sender">not used</param>
    ''' <param name="e">not used</param>
    Private Sub lvJTjimLabRates_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvJTjimLabRates.DoubleClick
        ' -------------------------------------------------------------------
        ' Sub to create JSP for one labor category.
        ' Sub is initiated by double clicking line in listview
        ' -------------------------------------------------------------------
        If chkboxJTjimJobSpecRates.Checked = False Then
            txtErrMsg.Text = "Job Specific Labor Rates must be checked to modify rates for this job."
            lvJTjimLabRates.HideSelection = True
            lvJTjimLabRates.SelectedItems.Clear()
            Exit Sub
        Else
            'modify pricing
            lvJTjimLabRates.HideSelection = False
            ' initialize JSL panel
            ShowJSLpanel()
            '
            txtJTjimJSLCat.Text = lvJTjimLabRates.SelectedItems.Item(0).Text
            txtJTjimJSLStan.Text = lvJTjimLabRates.SelectedItems(0).SubItems(1).Text
            txtJTjimJSLshop.Text = lvJTjimLabRates.SelectedItems(0).SubItems(2).Text
            '
        End If
    End Sub
    ''' <summary>JSP (job specific pricing) is maintained through activating a groupbox panel 
    '''          on the JTjim panel. This sub is a utility routine used to hide that panel.</summary>
    Public Sub HideJSLpanel()
        txtJTjimJSLtext3.Visible = False
        txtJTjimJSLtext2.Visible = False
        txtJTjimJSLtext1.Visible = False
        txtJTjimJSLerrmsg.Visible = False

        txtJTjimJSLerrmsg.Visible = False
        '
        txtJTjimJSLCat.Visible = False
        txtJTjimJSLshop.ReadOnly = True
        txtJTjimJSLshop.Visible = False
        txtJTjimJSLStan.ReadOnly = True
        txtJTjimJSLStan.Visible = False
        '
        chkboxJTjimJSLclone.Enabled = False
        chkboxJTjimJSLclone.Visible = False
        '
        btnJTjimJSLupdate.Enabled = False
        btnJTjimJSLupdate.Visible = False
        btnJTjimJSLcancel.Enabled = False
        btnJTjimJSLcancel.Visible = False
        '
        gboxJTjimJSLframe.Visible = False

    End Sub
    ''' <summary>
    ''' If JSP routines have been initiated (double-click on the labor pricing LV), 
    ''' this sub is used as a utility sub to display the groupbox panel on JTjim panel 
    ''' to make labor pricing modifications.
    ''' </summary>
    Public Sub ShowJSLpanel()
        gboxJTjimJSLframe.Visible = True
        gboxJTjimJSLframe.BringToFront()

        txtJTjimJSLtext3.Visible = True
        txtJTjimJSLtext2.Visible = True
        txtJTjimJSLtext1.Visible = True
        txtJTjimJSLerrmsg.Visible = True

        txtJTjimJSLerrmsg.Visible = True
        '
        txtJTjimJSLCat.Visible = True
        txtJTjimJSLshop.ReadOnly = False
        txtJTjimJSLshop.Visible = True
        txtJTjimJSLStan.ReadOnly = False
        txtJTjimJSLStan.Visible = True
        '
        txtJTjimJSLtext3.BringToFront()
        txtJTjimJSLtext2.BringToFront()
        txtJTjimJSLtext1.BringToFront()
        txtJTjimJSLerrmsg.BringToFront()

        txtJTjimJSLerrmsg.BringToFront()
        '
        txtJTjimJSLCat.BringToFront()
        txtJTjimJSLshop.BringToFront()
        txtJTjimJSLshop.BringToFront()
        txtJTjimJSLStan.BringToFront()
        txtJTjimJSLStan.BringToFront()
        '
        Dim tmpJobPOS As String = ""
        Dim tmpJobName As String = txtJTjimJob.Text
        Dim tmpSubName As String = ""
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        '
        If txtJTjimSubJob.Text <> "" Then
            If JTjimFindFunc(tmpJobPOS, tmpJobName, tmpSubName, tmpJobType, tmpDeactDate) = "DATA" Then
                If JTjimJIArray(tmpJobPOS).JTjimJISpecJobPricing = "X" Then
                    chkboxJTjimJSLclone.Enabled = True
                    chkboxJTjimJSLclone.Visible = True
                    chkboxJTjimJSLclone.BringToFront()
                End If
            End If
        End If


        '
        btnJTjimJSLupdate.Enabled = True
        btnJTjimJSLupdate.Visible = True
        btnJTjimJSLcancel.Enabled = True
        btnJTjimJSLcancel.Visible = True
        '
        btnJTjimJSLupdate.BringToFront()
        btnJTjimJSLcancel.BringToFront()

    End Sub
    ''' <summary>This sub handles checked changes related to JSP. It is queued by a check change and caused the jsp panel to be hiden or displayed.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkboxJTjimJobSpecRates_CheckedChanged(sender As Object, e As EventArgs) Handles chkboxJTjimJobSpecRates.CheckedChanged
        If chkboxJTjimJobSpecRates.Checked = False Then
            HideJSLpanel()
            ' ---------------------------------------------------------------------------
            ' Check to see if there are pricing records that had been previously created.
            ' If, yes --- delete the records for this job.
            ' ---------------------------------------------------------------------------
            Dim key As ICollection = JTjimJSRsl.Keys
            Dim k As String = Nothing
            Dim tmpFoundJSPRecord As Boolean = True

            Do While tmpFoundJSPRecord = True
                For Each k In key
                    tmpFoundJSPRecord = False
                    If JTjimJSRsl(k).JTjimlcJobName = txtJTjimJob.Text Then
                        If JTjimJSRsl(k).JTjimlcSubName = txtJTjimSubJob.Text Then
                            JTjimJSRsl.Remove(k)
                            tmpFoundJSPRecord = True
                            key = JTjimJSRsl.Keys
                            Exit For
                        End If
                    End If
                Next
                '
            Loop
        Else
            ShowJSLpanel()
        End If
        '
    End Sub
    ''' <summary>This sub is queued after a jsl price entry has been made for the 
    '''          standard pricing category. It does numeric checking for the entry and asks for 
    '''          corrections if entry is not valid.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTjimJSLStan_TextChanged(sender As Object, e As EventArgs) Handles txtJTjimJSLStan.LostFocus
        If IsNumeric(txtJTjimJSLStan.Text) <> True Then
            txtJTjimJSLerrmsg.Text = "New Standard Rate Is Not numeric. Correct And resubmit."
            txtJTjimJSLStan.Select()
        End If
    End Sub
    ''' <summary>This is the matching edit sub (is paired with txtJTjimJSLStan_TextChanged) for JSL shop 
    '''          rate entry. Must be numeric.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtJTjimJSLshop_TextChanged(sender As Object, e As EventArgs) Handles txtJTjimJSLshop.LostFocus
        If IsNumeric(txtJTjimJSLshop.Text) <> True Then
            txtJTjimJSLerrmsg.Text = "New Shop Rate Is Not numeric. Correct And resubmit."
            txtJTjimJSLshop.Select()
        End If
    End Sub
    ''' <summary>Part of the JSL logic. This sub cancels activity related to making jsl changes.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTjimJSLcancel_Click(sender As Object, e As EventArgs) Handles btnJTjimJSLcancel.Click
        HideJSLpanel()

    End Sub
    ''' <summary>
    ''' Part of JSL logic. This sub completes the update related to JSL modifications. It moves the 
    ''' modified data to the appropriate slot in the labor pricing lv on JTjim and hides the JSL panel.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTjimJSLupdate_Click(sender As Object, e As EventArgs) Handles btnJTjimJSLupdate.Click
        If txtJTjimJSLCat.Text <> "" Then
            If IsNumeric(txtJTjimJSLStan.Text) Then
                If IsNumeric(txtJTjimJSLshop.Text) Then
                    lvJTjimLabRates.SelectedItems(0).SubItems(1).Text = txtJTjimJSLStan.Text
                    lvJTjimLabRates.SelectedItems(0).SubItems(2).Text = txtJTjimJSLshop.Text
                    HideJSLpanel()
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' When the JSP panel is active, a button to queue JSP cloning from another job for this customer 
    ''' is available. This sub handles this task. If selected, JSP are copied from the master job to this job. 
    ''' This sub will likely need some modification in the switch from Mstr/Sub to Cust/Job relationship changes.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkboxJTjimJSLclone_CheckedChanged(sender As Object, e As EventArgs) Handles chkboxJTjimJSLclone.CheckedChanged
        If chkboxJTjimJSLclone.Checked = True Then
            Dim tmpLVSub As Integer = 0
            For Each item As ListViewItem In lvJTjimLabRates.Items
                JTjimJSRkey = txtJTjimJob.Text & "" & item.Text
                If JTjimJSRsl.ContainsKey(JTjimJSRkey) = True Then
                    item.SubItems.Item(1).Text = JTjimJSRsl(JTjimJSRkey).JTjimlcStanRate
                    item.SubItems.Item(2).Text = JTjimJSRsl(JTjimJSRkey).JTjimlcShopRate
                End If
            Next
            chkboxJTjimJSLclone.Checked = False
            HideJSLpanel()
        End If
    End Sub
    ''' <summary>
    ''' This sub is queued if radiobtnJTjimCstPlus is changed. If the value is checked, the fields
    ''' associated with a Cost + job are updated. Some additional code might be useful in this sub
    ''' to validate that no changes are made after certain job events have taken place ...
    ''' such as an AUP invoice has previously been produced. The next sub (takes care when AUP cost
    ''' method is selected) likely needs the same additional data.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub radiobtnJTjimCstPlus_CheckedChanged(sender As Object, e As EventArgs) Handles radiobtnJTjimCstPlus.CheckedChanged,
                                                                                              radiobtnJTjimAgreedUpon.CheckedChanged,
                                                                                              radiobtnJTjimCstPlusDeposits.CheckedChanged
        ' -------------------------------------------------------------------------------------------
        ' Check to see if this job used to be AUP. If so, warn user that AUP records will be deleted.
        ' -------------------------------------------------------------------------------------------
        If radiobtnJTjimCstPlus.Checked = True Then
            If JTaup.JTaupChkforRecords(txtJTjimJob.Text, txtJTjimSubJob.Text) = True Then
                Dim tmpAns As Integer = MsgBox("A request has been made to change this job's pricing method" & vbCrLf &
                                              "to Cost+. You will lose any AUP or CPWD informtion on file." & vbCrLf &
                                              "Do you want to proceed? (Yes) - (JI.014)",
                                              vbYesNo, "J.I.M. - Price Method Change")
                If tmpAns = vbYes Then
                    JTaup.JTaupPurgeAUPInfo(txtJTjimJob.Text, txtJTjimSubJob.Text)
                    ' clear fields on JIM AUP Groupbox
                    txtJTjimAUPInvdTD.Text = ""
                    txtJTjimAUPNetRemain.Text = ""
                    txtJTjimAUPQueueAmt.Text = ""
                    txtJTjimAUPQuoted.Text = ""
                Else
                    radiobtnJTjimCstPlus.Checked = False
                    radiobtnJTjimAgreedUpon.Checked = True
                    If cboxJTjimAUPLabor.Checked = False Then
                        If cboxJTjimAUPMat.Checked = False Then
                            If cboxJTjimAUPServices.Checked = False Then
                                radiobtnJTjimCstPlusDeposits.Checked = True
                            End If
                        End If
                    End If
                    Exit Sub
                End If
            End If
        End If
        ' 
        If radiobtnJTjimCstPlus.Checked = True Then
            gboxJTjimAUP.Visible = False
            gboxJTjimCPWD.Visible = False
        End If
        '
        If radiobtnJTjimAgreedUpon.Checked = True Then
            gboxJTjimCPWD.Visible = False
            gboxJTjimAUP.Visible = True
            cboxJTjimAUPLabor.Checked = True
            cboxJTjimAUPMat.Checked = True
            cboxJTjimAUPServices.Checked = True
        End If
        '
        If radiobtnJTjimCstPlusDeposits.Checked = True Then
            gboxJTjimAUP.Visible = False
            gboxJTjimCPWD.Visible = True
            cboxJTjimAUPLabor.Checked = False
            cboxJTjimAUPMat.Checked = False
            cboxJTjimAUPServices.Checked = False
        End If
    End Sub
    ''' <summary>The sub handles a double-click on the JTjim invoice history lv. It queues opening
    '''        the JTas panel and displays more details on all prior invoices produced for this job.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvJTjimInvHist_DoubleClick(sender As Object, e As EventArgs) Handles lvJTjimInvHist.DoubleClick
        Me.Hide()
        Dim tmpJobPricing As String = ""
        If radiobtnJTjimCstPlus.Checked = True Then
            tmpJobPricing = "CPLUS"
        Else
            If radiobtnJTjimCstPlus.Checked = True Then
                tmpJobPricing = "AUP"
            Else
                tmpJobPricing = "CPWD"
            End If
        End If
        Dim tmpLayer As String = "Hist"
        JTas.JTasInit(txtJTjimJob.Text, txtJTjimSubJob.Text, txtJTjimJobDesc.Text, tmpJobPricing, tmpLayer)
    End Sub
    ''' <summary>
    ''' This sub handles a double-click on the JTjimCurAct lv. After some editing related to checking 
    ''' status related to AUP invoicing, the JTas panel is queued. This action is normally taken to 
    ''' queue invoicing for this job.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lvJTjimCurAct_DoubleClick(sender As Object, e As EventArgs) Handles lvJTjimCurAct.DoubleClick
        Dim tmpJobPricing As String = ""
        If radiobtnJTjimAgreedUpon.Checked = True Then
            If txtJTjimAUPQueueAmt.Text = "" Then
                Dim tmpAns As Integer = MsgBox("This Job/SubJob Is an AUP Job And there Is no amount queued for " &
                                               "invoicing. Do you want to proceed? (Yes)" &
                       vbCrLf & "(To add a Queued amount, click the 'Mod/Create Job/Chg or Inv'g' buttom on the " &
                    "main job panel) - (JI.005)", vbYesNoCancel, "J.I.M. - AUP Invoice Warning")
                If tmpAns = vbNo Then
                    Exit Sub
                End If
            End If
        End If
        If JTjimCntCustJobs(txtJTjimJob.Text) > 1 Then
            Dim tmpAns As Integer = MsgBox("Customer selected for invoicing has multiple jobs." & vbCrLf &
                 "Do you want to invoice all jobs of this customer (Yes) or" & vbCrLf &
               "just the specific job selected (No)? (JI.006)", vbYesNoCancel, "J.I.M. - Invoice Select Options")
            If tmpAns = vbCancel Then
                Exit Sub
            End If
            If tmpAns = vbYes Then
                tmpJobPricing = "MSCOMB"
            End If
        End If
        Me.Hide()
        ' --------------------------------------------------------------------------------------------
        ' JTaupDepositChk called here. It will check the job or jobs being invoiced to see if they are 
        ' "CPWD" (Cost+ with Deposits --- coded as AUP excluding LAB, Mat and Ser). If it is a CPWD
        ' job, it will be anlyzed to see if any deposit reversals should be processed.
        ' After the job has been checked, invoicing will continue.
        ' --------------------------------------------------------------------------------------------
        If tmpJobPricing = "MSCOMB" Then
            JTaup.JTaupDepositChk(txtJTjimJob.Text, "MSCOMB")
        Else
            JTaup.JTaupDepositChk(txtJTjimJob.Text, txtJTjimSubJob.Text)
        End If
        If tmpJobPricing = "" Then
            If radiobtnJTjimCstPlus.Checked = True Then
                tmpJobPricing = "CPLUS"
            Else
                If radiobtnJTjimCstPlusDeposits.Checked = True Then
                    tmpJobPricing = "CPWD"
                Else
                    tmpJobPricing = "AUP"
                End If

            End If
        End If
        Dim tmpLayer As String = ""
        If lvJTjimCurAct.SelectedItems(0).Text = "Labor" Then
            tmpLayer = "Lab"
        Else
            If lvJTjimCurAct.SelectedItems(0).Text = "SubContractors" Then
                tmpLayer = "Sub"
            Else
                tmpLayer = "Mat"
            End If
        End If
        ' -------------------------------------------------------------------------
        ' Copy JIM to array prior to going to invoicing to make sure nothing is 
        ' lost related to value for the job being changed ... and not yet updated.
        ' -------------------------------------------------------------------------
        Dim tmpJIJobSub As String = FixedLengthString(txtJTjimJob.Text, 10, "_") & FixedLengthString(txtJTjimSubJob.Text, 10, "_")
        JTjimtoarray(tmpJIJobSub)
        JTas.JTasInit(txtJTjimJob.Text, txtJTjimSubJob.Text, txtJTjimJobDesc.Text, tmpJobPricing, tmpLayer)
    End Sub
    ' ==========================================================
    ' Functions
    ' ==========================================================
    ''' <summary>Function to return applicable hourly rate for a labor class to the TK function.</summary>
    ''' <param name="tmpJobId"></param>
    ''' <param name="tmpSubID"></param>
    ''' <param name="TmpLabCat"></param>
    ''' <param name="tmpStanRate"></param>
    ''' <param name="tmpShopRate"></param>
    Public Function JTjimJSRLkUp(tmpJobId As String, tmpSubID As String, TmpLabCat As String, ByRef tmpStanRate As String, ByRef tmpShopRate As String) As String
        ' ---------------------------------------------------------------------------------------
        ' Function to check for and return JSR pricing to TK.
        ' ---------------------------------------------------------------------------------------
        Dim key As ICollection = JTjimJSRsl.Keys
        Dim k As String
        For Each k In key
            If JTjimJSRsl(k).JTjimlcJobName = tmpJobId And JTjimJSRsl(k).JTjimlcSubName = tmpSubID And JTjimJSRsl(k).JTjimlcKey = TmpLabCat Then
                tmpStanRate = JTjimJSRsl(k).JTjimlcStanRate
                tmpShopRate = JTjimJSRsl(k).JTjimlcShopRate
                Return "DATA"
            End If
        Next
        Return "NODATA"
    End Function
    ''' <summary>Function used to determine if a customer~job key exists. If the key is found, the remainder of the fields are filled and returned to the calling sub.</summary>
    ''' <param name="JTjimfPOS">position within the job sl.</param>
    ''' <param name="JTjimfJobName">customer key for the search</param>
    ''' <param name="JTjimfSubname">job name for the search</param>
    ''' <param name="JTjimfJobType">previously would be filled in with M,S or N. No longer applicable with the Customer~Job modifications.</param>
    ''' <param name="JTjimfDeactDate">Date job was deactivated, ... blank if active.</param>
    Function JTjimFindFunc(ByRef JTjimfPOS As String, ByRef JTjimfJobName As String,
                           ByRef JTjimfSubname As String, ByRef JTjimfJobType As String,
                           ByRef JTjimfDeactDate As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values and check for a keys existance. Row is
        ' selected by 'JTjimfJobName' variable set by calling SUB. 
        ' If the value is found, the key and the remaining field values are returned.
        ' If the key does not exist, the function returns a 'INVALID' value.
        ' -----------------------------------------------------------------------------
        ' converted to use sl array
        ' -----------------------------------------------------------------------------
        '
        If JTjimfJobName = "" Then
            Return "INVALID"
        End If
        Dim tmpJobName As String = FixedLengthString(JTjimfJobName, 10, "_")
        Dim tmpSubName As String = FixedLengthString(JTjimfSubname, 10, "_")
        Dim tmpKey As String = tmpJobName & tmpSubName
        JTjimfPOS = tmpKey

        If JTjimJIArray.ContainsKey(tmpKey) Then
            '  JTjimfJobType = JTjimJIArray(tmpKey).JTjimJIJobType  'no longer used.
            JTjimfDeactDate = JTjimJIArray(tmpKey).JTjimJIDeactDate
            Return "DATA"
        Else
            Return "INVALID"
        End If


    End Function   '

    Function JTjimCustFindFunc(ByRef JTjimfJobName As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to  check for a customer keys existance. Row is
        ' selected by 'JTjimfJobName' variable set by calling SUB. 
        ' If the value is found, the key and the remaining field values are returned.
        ' If the key does not exist, the function returns a 'INVALID' value.
        ' -----------------------------------------------------------------------------
        ' converted to use sl array
        ' -----------------------------------------------------------------------------
        '
        If JTjimfJobName = "" Then
            Return "INVALID"
        End If
        If JTjimCIArray.ContainsKey(JTjimfJobName) Then
            Return "DATA"
        Else
            Return "INVALID"
        End If


    End Function
    ''' <summary>
    '''   <para>Function used to return values from single row of JTjimJIArray (sorted list). Returns job related to a recrod number contained in JTjimfPOS. This field must be validated to make sure it is within the range of records. If JTjimfPOS is greater than the number of records within JTjimJIArray, value of "EMPTY" is returned.</para>
    ''' </summary>
    ''' <param name="JTjimfPOS">relative record number within the SL</param>
    ''' <param name="JTjimfJobName">returned ... customer name</param>
    ''' <param name="JTjimfSubname">returned ... job name</param>
    ''' <param name="JTjimfDeactDate">returned ... Date if deactivated ... blank if active</param>
    ''' <param name="JTjimfJobType">returned ... used to be M,S or N ... no longer applicable with customer~job modifications</param>
    ''' <param name="JTjimfCstMeth">returned ... Cst+, AUP or AUPH</param>
    Function JTjimReadFunc(JTjimfPOS As Integer, ByRef JTjimfJobName As String,
                           ByRef JTjimfSubname As String, ByRef JTjimfDeactDate As String,
                           ByRef JTjimfJobType As String, ByRef JTjimfCstMeth As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values from single row of array JTjimJIArray. Row to be
        ' selected by 'JTjimfPOS' variable set by calling SUB. Limits must be set to
        ' make sure JThrfPOS value is within the array scope.
        ' -----------------------------------------------------------------------------
        '
        Dim keys As ICollection = JTjimJIArray.Keys

        Dim tmpTempSub As Integer = JTjimfPOS
        If JTjimfPOS > keys.Count - 1 Then
            Return "EMPTY"
            Exit Function
        End If
        '
        JTjimfJobName = JTjimJIArray(keys(tmpTempSub)).JTjimJIJobName
        JTjimfSubname = JTjimJIArray(keys(tmpTempSub)).JTjimJISubName
        JTjimfDeactDate = JTjimJIArray(keys(tmpTempSub)).JTjimJIDeactDate
        If JTjimJIArray(keys(tmpTempSub)).JTjimJIJobPricMeth = "CSTP" Then
            JTjimfCstMeth = "CST+"
        Else
            JTjimfCstMeth = "AUP"
            If JTjimJIArray(keys(tmpTempSub)).JTjimJIAUPLabor <> "X" Or
                JTjimJIArray(keys(tmpTempSub)).JTjimJIAUPMat <> "X" Or
                JTjimJIArray(keys(tmpTempSub)).JTjimJIAUPServices <> "X" Then
                JTjimfCstMeth = "CPWD"
            End If
        End If
        '
        Return "DATA"
    End Function

    ''' <summary>Function to find the state where the work for a specific job is taking place. 
    '''          Function called from TK/PR functions.</summary>
    ''' <param name="WISJobid"></param>
    ''' <param name="WISSubId"></param>
    Function WorkInState(WISJobid As String, WISSubId As String) As String

        Dim tmpJobName As String = FixedLengthString(WISJobid, 10, "_")
        Dim tmpSubName As String = FixedLengthString(WISSubId, 10, "_")
        Dim tmpKey As String = tmpJobName & tmpSubName

        Return JTjimJIArray(tmpKey).JTjimJIJobinState


        Return "NoStateFound"
    End Function
    ''' <summary>Function to return the material or services markon for this job. This is used 
    '''          in the NLC entry routines.</summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <param name="tmpMorS"></param>
    Function JTjimMrgnPC(tmpJobID As String, tmpSubID As String, tmpMorS As String) As String
        '
        Dim tmpJobName As String = FixedLengthString(tmpJobID, 10, "_")
        Dim tmpSubName As String = FixedLengthString(tmpSubID, 10, "_")
        Dim funcsub As String = tmpJobName & tmpSubName
        Select Case tmpMorS
            Case = "M"
                Return JTjimJIArray(funcsub).JTjimJIMatMargin
            Case "S"
                Return JTjimJIArray(funcsub).JTjimJISubMargin
            Case Else
                Return "0"        'tke care of NJRE
        End Select
        '
        Return "JobNotFound"
    End Function
    ''' <summary>Function to update the last activity on the job master record. This function 
    '''          is called from TK and NLC routines.</summary>
    ''' <param name="tmpjobID"></param>
    ''' <param name="tmpSubID"></param>
    Public Function JTjimUpdtActDate(tmpjobID As String, tmpSubID As String) As String
        Dim tmpPOS As String = ""
        Dim tmpJobID2 As String = ""
        Dim tmpSubID2 As String = ""
        Dim tmpJobType As String = ""
        Dim tmpDeactDate As String = ""
        tmpJobID2 = tmpjobID
        tmpSubID2 = tmpSubID
        If JTjimFindFunc(tmpPOS, tmpJobID2, tmpSubID2, tmpJobType, tmpDeactDate) <> "DATA" Then
            Return "ERROR"
        End If
        Dim tmpJIData As JTjimJobInfo = JTjimJIArray(tmpPOS)
        tmpJIData.JTjimJILastActDate = Today
        JTjimJIArray.Remove(tmpPOS)
        '    JTjimJIArray.Add(tmpPOS, tmpJIData)
        JTjimJIArrayAdd(tmpPOS, tmpJIData, "JTjim~1903 - JTjimUpdtActDate")
        Return "UPDATED"
    End Function


    ''' <summary>Function updates the activity date on the job master record. Seems similar to 
    '''          last function except this function actually used date of transaction versus to the 
    '''          system date.</summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSub"></param>
    ''' <param name="tmpDate"></param>
    Public Function JTJimUpdtLstActivity(tmpJob As String, tmpSub As String, tmpDate As String)
        ' ---------------------------------------------------------------------------------------
        ' Accepts activity date from tk and nlc, determines if it later than latest activity date
        ' and updates the JTjim sl if date is later.
        ' ---------------------------------------------------------------------------------------
        Dim keys As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        For Each k In keys
            If tmpJob = JTjimJIArray(k).JTjimJIJobName And tmpSub = JTjimJIArray(k).JTjimJISubName Then
                Dim tmpArea As New JTjimJobInfo
                tmpArea = JTjimJIArray.Item(k)
                If tmpArea.JTjimJILastActDate <> "" Then

                    Dim TmpActDate As Date = CDate(tmpDate)
                    Dim tmpActJulDate As String = Format(TmpActDate, "yyyy") & Format(DatePart("y", TmpActDate), "000")
                    '
                    Dim tmpLstActDate As Date = CDate(tmpArea.JTjimJILastActDate)
                    Dim tmpLstActJulDate As String = Format(tmpLstActDate, "yyyy") & Format(DatePart("y", tmpLstActDate), "000")
                    ' 
                    If tmpActJulDate <= tmpLstActJulDate Then
                        Return "DONE"
                    Else
                        tmpArea.JTjimJILastActDate = tmpDate
                    End If
                End If
                If tmpArea.JTjimJILastActDate = "" Then
                    tmpArea.JTjimJILastActDate = tmpDate
                End If
                JTjimJIArray.Remove(k)
                JTjimJIArrayAdd(k, tmpArea, "JTjim~1944 - JTJimUpdtLstActivity")
                JobArrayFlush()
                Return "DONE"
            End If
        Next
        Return "ERROR"
    End Function
    ''' <summary>
    ''' Function called from JTas code. Its function is to fill fields in the JTas structure.
    ''' Job description is returned, cost method as well as settings, in the case of an AUPH job.
    ''' If customer~job combination not found, "NOTFOUND" is returned.
    ''' </summary>
    ''' <param name="tmpNewItem">This is the entrie JTas array record (format is JTasIJstru).</param>
    Public Function JTjimInvIDUpdt(ByRef tmpNewItem As JTas.JTasIJstru)
        Dim keys As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        For Each k In keys
            If tmpNewItem.JTasIJJobID = JTjimJIArray(k).JTjimJIJobName And
                tmpNewItem.JTasIJSubJobID = JTjimJIArray(k).JTjimJISubName Then
                tmpNewItem.JTasIJDescrip = JTjimJIArray(k).JTjimJIDescName
                tmpNewItem.JTasIJCostMeth = JTjimJIArray(k).JTjimJIJobPricMeth
                tmpNewItem.JtasIJTKsw = JTjimJIArray(k).JTjimJIAUPLabor
                tmpNewItem.JTasIJMATsw = JTjimJIArray(k).JTjimJIAUPMat
                tmpNewItem.JTasIJSERsw = JTjimJIArray(k).JTjimJIAUPServices
                '
                Return "Updated"
            End If
        Next

        Return "NotFound"
    End Function
    ''' <summary>Function is called to search for and return a specific job record. Returns all the master record data for the job.</summary>
    ''' <param name="JimInfo">
    ''' This is the structure of a JTjim job record (format is JTjimjobrecord). It is sent with the costomer and job fields filled. It is returned (if the record of found) with the entire record.
    ''' </param>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubjob"></param>
    Public Function JTjimJTjrReadRec(ByRef JimInfo As JTjimJobInfo,
                                     tmpJob As String, tmpSubjob As String) As String
        If tmpJob = "" Then
            Return "INVALID"
        End If
        Dim tmpJobName As String = FixedLengthString(tmpJob, 10, "_")
        Dim tmpSubName As String = FixedLengthString(tmpSubjob, 10, "_")
        Dim tmpKey As String = tmpJobName & tmpSubName
        '
        If JTjimJIArray.ContainsKey(tmpKey) Then
            JimInfo = JTjimJIArray(tmpKey)
            Return "DATA"
        Else
            Return "INVALID"
        End If
    End Function
    '
    ''' <summary>
    ''' This sub edits the entry when AUP is selected as the cost method. An AUP job can cover 
    ''' all costs for a job ... the most normal situation or it can be a highbred where only a 
    ''' portion of the job is on an agreed upon price. For example, a job could have a fixed labor 
    ''' segment with cost+ material and services. In this situation the the cost method become AUPH. 
    ''' This sub edited for appropriates setting for this situation.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnJTjimAUPJob_Click(sender As Object, e As EventArgs) Handles btnJTjimAUPJob.Click
        '
        Me.Hide()
        JTjimCallAUP()
        '
    End Sub
    Private Sub JTjimCallAUP()
        Dim tmpAUPCategories As String = ""
        If cboxJTjimAUPLabor.Checked = True Then
            tmpAUPCategories = "Labor"
        End If
        If cboxJTjimAUPMat.Checked = True Then
            If tmpAUPCategories <> "" Then
                tmpAUPCategories &= ", "
            End If
            tmpAUPCategories &= "Materials"
        End If
        If cboxJTjimAUPServices.Checked = True Then
            If tmpAUPCategories <> "" Then
                tmpAUPCategories &= " & "
            End If
            tmpAUPCategories &= "Services"
        End If
        If tmpAUPCategories <> "" Then
            JTaup.txtJTaupCategories.Text = "AUP quote includes " & tmpAUPCategories
        Else
            JTaup.txtJTaupCategories.Text = "Cost+ Job --- with Deposits"
        End If
        JTaup.JTaupInit(txtJTjimJob.Text, txtJTjimSubJob.Text, txtJTjimJobDesc.Text)
        '
    End Sub
    ' ========================================================================================================
    ' Next three subs have been commented out ... can't quite figure out what they were supposed to do.
    ' Keeping the user from changing the values after invoicing has happened is control by readonly settings
    ' on JTjim panel. - - - 3/20/2020
    ' ========================================================================================================
    '   ''' <summary>
    '    ''' After an invoice has been produced in an AUP job, components setting up the job can no longer be changed. 
    '    ''' This sub and the next two check for and stop any operator action trying to to do this.
    '    ''' </summary>
    '   ''' <param name="sender"></param>
    '  ''' <param name="e"></param>
    '   Private Sub cboxJTjimAUPLabor_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTjimAUPLabor.CheckedChanged
    '  If Val(txtJTjimAUPInvdTD.Text) <> 0 Then
    '         ToolTip1.Show("Categories in an AUP job cannot be changed" & vbCrLf &
    '                      "after an invoice for the job has been produced.", cboxJTjimAUPLabor, 40, -55, 6000)
    'If cboxJTjimAUPLabor.Checked = True Then
    '           cboxJTjimAUPLabor.Checked = False
    'Else
    '           cboxJTjimAUPLabor.Checked = True
    '   End If
    '  End If
    ' End Sub

    '   ''' <summary>Same as above ... checks for invalid pricing category changes in AUP job.</summary>
    '  ''' <param name="sender"></param>
    '   ''' <param name="e"></param>
    '   Private Sub cboxJTjimAUPMat_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTjimAUPMat.CheckedChanged
    '  If Val(txtJTjimAUPInvdTD.Text) <> 0 Then
    '         ToolTip1.Show("Categories in an AUP job cannot be changed" & vbCrLf &
    '                      "after an invoice for the job has been produced.", cboxJTjimAUPLabor, 40, -55, 6000)
    ''    If cboxJTjimAUPMat.Checked = True Then
    '              cboxJTjimAUPMat.Checked = False
    ' Else
    '            cboxJTjimAUPMat.Checked = True
    '   End If
    '  End If
    '   End Sub

    '   ''' <summary>Same as above ... edits changes to AUP settings.</summary>
    '   ''' <param name="sender"></param>
    '   ''' <param name="e"></param>
    '   Private Sub cboxJTjimAUPServices_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTjimAUPServices.CheckedChanged
    '  If Val(txtJTjimAUPInvdTD.Text) <> 0 Then
    '         ToolTip1.Show("Categories in an AUP job cannot be changed" & vbCrLf &
    '                      "after an invoice for the job has been produced.", cboxJTjimAUPLabor, 40, -55, 6000)
    '   If cboxJTjimAUPServices.Checked = True Then
    '              cboxJTjimAUPServices.Checked = False
    '   Else
    '              cboxJTjimAUPServices.Checked = True
    '   End If
    '  End If
    '   End Sub
    ' ===========================================================================================================
    ' ===========================================================================================================
    '
    ''' <summary>
    ''' Invoices are formatted bases on global settings. In some cases, specific jobs are set to issue invoices differently from the global settings. This function turns that option off and the job will again be printed using the global settings.
    ''' </summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubjob"></param>
    Public Function JTjimTurnOffNonStdFrmt(tmpJob As String, tmpSubjob As String) As String
        Dim tmpJobName As String = FixedLengthString(tmpJob, 10, "_")
        Dim tmpSubName As String = FixedLengthString(tmpSubjob, 10, "_")
        Dim tmpKey As String = tmpJobName & tmpSubName
        If JTjimJIArray.ContainsKey(tmpKey) Then
            Dim tmpItem As JTjimJobInfo
            tmpItem = JTjimJIArray(tmpKey)
            tmpItem.JTjimJINonStdFrmt = ""
            JTjimJIArray.Remove(tmpKey)
            '    JTjimJIArray.Add(tmpKey, tmpItem)
            JTjimJIArrayAdd(tmpKey, tmpItem, "JTjim~2122 - JTjimTurnOffNonStdFrmt")
            JobArrayFlush()
            '
        End If
        Return ""
    End Function

    ''' <summary>Function rertrieve and format email addresses for a job in preparation for invoicing email.</summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubJob"></param>
    Public Function JTjimInvAddTo(tmpJob, tmpSubJob) As String
        ' ---------------------------------------------------------------------------------------
        ' Function locates, formats and returns the Inv Email Address for a job, SubJob. Used
        ' by the UTIL inv email function.
        ' ---------------------------------------------------------------------------------------
        Dim tmpToAdd As String = ""
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        For Each k In key
            If JTjimJIArray(k).JTjimJIJobName = tmpJob And
                JTjimJIArray(k).JTjimJISubName = tmpSubJob Then
                '
                If JTjimJIArray(k).JTjimJIInvEmailPrim = "X" Then
                    tmpToAdd = JTjimJIArray(k).JTjimJIEmail
                End If
                '
                If JTjimJIArray(k).JTjimJIInvEmailAlt = "X" Then
                    If JTjimJIArray(k).JTjimJISecEmail <> "" Then
                        If JTjimJIArray(k).JTjimJIInvEmailPrim = "X" Then
                            tmpToAdd = tmpToAdd & ", " & JTjimJIArray(k).JTjimJISecEmail
                        Else
                            tmpToAdd = JTjimJIArray(k).JTjimJISecEmail
                        End If
                    End If
                End If
                Return tmpToAdd
            End If
        Next
        Return "JObNotFound"


    End Function
    ''' <summary>Function checks to see if a master job has sub jobs. NOTE --- this function will likely be absolete with the modification to use Customer~Job versus Master~Subjob.</summary>
    ''' <param name="tmpJobName"></param>
    Public Function JTjimHasSubJobs(tmpJobName As String) As Boolean
        ' ------------------------------------------------------------------------
        ' Function checks to see if a job has subjobs.
        ' ------------------------------------------------------------------------
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        '
        For Each k In key
            If tmpJobName = JTjimJIArray(k).JTjimJIJobName And JTjimJIArray(k).JTjimJISubName <> "" Then
                Return True
            End If
        Next
        Return False
    End Function
    ''' <summary>Function either purges a job or sets the deactivation date. Care must be taken that 
    '''          everything has been checked to make sure this job is inactive with no attached history.</summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubJob"></param>
    ''' <param name="tmpAction"></param>
    Public Function JTjimPurgeJob(tmpJob As String, tmpSubJob As String, tmpAction As String) As Boolean
        ' ------------------------------------------------------------------------------------
        ' Function either deletes job or set Deactivate date. Used as part of Job Maintenance.
        ' ------------------------------------------------------------------------------------
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        '
        For Each k In key
            If tmpJob = JTjimJIArray(k).JTjimJIJobName Then
                If JTjimJIArray(k).JTjimJISubName = tmpSubJob Then
                    If tmpAction = "PURGE" Then
                        JTjimJIArray.Remove(k)
                    Else
                        tmpJimInfo = JTjimJIArray(k)
                        If tmpAction = "DEACT" Then
                            tmpJimInfo.JTjimJIDeactDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
                        Else
                            'reactivate a job --- used by invoice voiding logic.
                            tmpJimInfo.JTjimJIDeactDate = ""
                            JTMainMenu.JTMMFirstTimeThru = "FIRST"
                        End If
                        JTjimJIArray.Remove(k)
                        JTjimJIArrayAdd(k, tmpJimInfo, "JTjim~2207 - JTjimPurgeJob")
                    End If
                    JobArrayFlush()
                    Return True
                End If
            End If
        Next
        Return False
    End Function
    ''' <summary>Function reactivates a previously deactivated job. This function is called when operator action asked to do something with this job.</summary>
    ''' <param name="tmpJobName"></param>
    ''' <param name="tmpSubJobName"></param>
    Public Function JTjimReactivateJob(tmpJobName As String, tmpSubJobName As String) As Boolean
        ' ---------------------------------------------------------------------------------
        ' Function called from mainmenu when job is being reactivated.
        ' ---------------------------------------------------------------------------------
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        For Each k In key
            If JTjimJIArray(k).JTjimJIJobName = tmpJobName And
                    JTjimJIArray(k).JTjimJISubName = tmpSubJobName Then
                tmpJimInfo = JTjimJIArray(k)
                tmpJimInfo.JTjimJIDeactDate = ""
                JTjimJIArray.Remove(k)
                '        JTjimJIArray.Add(k, tmpJimInfo)
                JTjimJIArrayAdd(k, tmpJimInfo, "JTjim~2231 - JTjimReactivateJob")
                JobArrayFlush()
                Return True
            End If
        Next
        Return False
    End Function
    ''' <summary>Function is call for a specific customer~job. If job is found, job description and price method is returned.</summary>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpSubjob"></param>
    ''' <param name="tmpJobDesc"></param>
    ''' <param name="tmpJobPricing"></param>
    Public Function JTjimGetILInfo(tmpJob As String, tmpSubjob As String,
                                   ByRef tmpJobDesc As String, ByRef tmpJobPricing As String) As String
        Dim tmpJobName As String = FixedLengthString(tmpJob, 10, "_")
        Dim tmpSubName As String = FixedLengthString(tmpSubjob, 10, "_")
        Dim tmpKey As String = tmpJobName & tmpSubName
        '
        If JTjimJIArray.ContainsKey(tmpKey) Then
            tmpJobDesc = JTjimJIArray(tmpKey).JTjimJIDescName
            tmpJobPricing = JTjimJIArray(tmpKey).JTjimJIJobPricMeth
            Return "DATA"
        End If
        Return "NOTFOUND"
    End Function

    ''' <summary>
    ''' If check changed to FALSE on non-standard invoice format option, record specifying the format is delated. If it is changed to TRUE, this sub doesn't do anything. The remainder of the activity is done while invoicing.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkboxJTjimNonStdFrmt_CheckedChanged(sender As Object, e As EventArgs) Handles chkboxJTjimNonStdFrmt.CheckedChanged
        If chkboxJTjimNonStdFrmt.Checked = False Then
            JTif.JTifRemoveRecord(txtJTjimJob.Text, txtJTjimSubJob.Text)
        End If
    End Sub
    ''' <summary>Function purges JSP for jobs that previously had selected the option and no longer need it. It is also used by year-end job deltion routines.</summary>
    ''' <param name="tmpjob"></param>
    ''' <param name="tmpsubjob"></param>
    Public Function JTjimPurgeJobSpecificPricing(tmpjob As String, tmpsubjob As String) As String
        Dim tmpPurgeStatus As String = "NOTDONE"
        Dim tmpPurgeCnt As Integer = 0
        Do While tmpPurgeStatus <> "DONE"
            tmpPurgeStatus = JTjimPurgeJobSpecificPricingLoop(tmpjob, tmpsubjob, tmpPurgeCnt)
        Loop
        '
        JobArrayFlush()
        '
        Return "PURGED"
    End Function
    ''' <summary>Function is call by preceeding function to do the actual deletions.</summary>
    ''' <param name="tmpjob"></param>
    ''' <param name="tmpsubjob"></param>
    ''' <param name="tmpPurgeCnt"></param>
    Private Function JTjimPurgeJobSpecificPricingLoop(tmpjob As String, tmpsubjob As String,
            ByRef tmpPurgeCnt As Integer) As String
        Dim key As ICollection = JTjimJSRsl.Keys
        Dim k As String = ""
        For Each k In key
            If JTjimJSRsl(k).JTjimlcJobName = tmpjob Then
                If JTjimJSRsl(k).JTjimlcSubName = tmpsubjob Then
                    JTjimJSRsl.Remove(k)
                    tmpPurgeCnt += 1
                    Return "NOTDONE"
                End If
            End If
        Next
        '
        Return "DONE"
    End Function
    ''' <summary>
    ''' This function is part of the year end job purge logic.
    ''' It find the job record and purges it. It also checks to 
    ''' makes sure that there there aren't any job specific 
    ''' invoice format records ... if any are found, they are
    ''' also purged.
    ''' </summary>
    ''' <param name="tmppjob">Job to be purged</param>
    ''' <param name="tmppsubjob">Subjob to be purged</param>
    ''' <returns></returns>
    Public Function JTjimPurgeJobInfoRecord(tmppjob, tmppsubjob) As String
        ' logic to purge job record.
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = ""
        For Each k In key
            If JTjimJIArray(k).JTjimJIJobName = tmppjob Then
                If JTjimJIArray(k).JTjimJISubName = tmppsubjob Then
                    JTjimJIArray.Remove(k)
                    Exit For
                End If
            End If
        Next
        '
        ' Logic to check for and purge job specific invoice formatting record.
        ' Function called ... will delete record in slJTif if found.
        '
        JTif.JTifRemoveRecord(tmppjob, tmppsubjob)
        '
        ' -------------------------------------------------------------------------
        ' Check to see if there are other jobs for this customer. If "no",
        ' Remove the customer info record
        ' -------------------------------------------------------------------------
        '
        Dim tmpNumofJobs As Integer = 0
        key = JTjimJIArray.Keys
        '
        For Each k In key
            If JTjimJIArray(k).JTjimJIJobName = tmppjob Then
                tmpNumofJobs += 1
            End If
        Next
        '
        If tmpNumofJobs = 0 Then
            '
            If JTjimCIArray.ContainsKey(tmppjob) Then
                JTjimCIArray.Remove(tmppjob)
            End If
            '
        End If

        '
        JobArrayFlush()
        Return "PURGED"
    End Function
    Public Function JTjimCustArrayRestore()
        ' -----------------------------------------------------------------------------
        ' This sub read the JOBS XML data and rebuilds jimArray.
        ' -----------------------------------------------------------------------------
        '       Dim tmpJIarray As Integer = 0
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        '
        '
        Dim tmpRecordType As String = ""
        Dim JThrWhichField As Integer = 1

        ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTCustomers.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTCustomers.Bkup") Then
            MsgBox("JTCustomers --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (JI.016)")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTCustomers.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTCustomers.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTCustomers.Bkup", "JTCustomers.dat")
        End If
        ' ------------------------------------------------------------------

        '
        Dim tmpJimInfo As New JTjimCustInfo
        '
        JTjimCIArray.Clear()
        ' --------------------------------------------------------------------------------------------
        ' Check to see if file JTCustomers exists. If it does proceed with the logic to load the file.
        ' If it doesn't exist, exit the function. Data will be loaded through JTJobs load.
        ' This will only happen the first time this logic is loaded.
        ' --------------------------------------------------------------------------------------------

        If System.IO.File.Exists(filePath) = False Then
            '          MsgBox("JTCustomers.dat does not exist - JTjim~2360")
            Return "NOFILELOAD"
        End If

        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTjimReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTjimReadFileKey = "Customers" Or
                        JTjimReadFileKey = "/Customers" Then
                        JTjimReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTjimReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    '




                    Select Case JTjimReadFileKey
                        Case "JobName"
                            tmpJimInfo.JTjimCIJobName = JTjimReadFileData
                        Case "TaxExemptJob"
                            tmpJimInfo.JTjimCITaxExemptJob = JTjimReadFileData
                        Case "InvPDF"
                            tmpJimInfo.JTjimCIInvPDF = JTjimReadFileData
                        Case "InvUSPS"
                            tmpJimInfo.JTjimCIInvUSPS = JTjimReadFileData
                        Case "InvFile"
                            tmpJimInfo.JTjimCIInvFile = JTjimReadFileData
                        Case "BillStreet"
                            tmpJimInfo.JTjimCIBillStreet = JTjimReadFileData
                        Case "BillCity"
                            tmpJimInfo.JTjimCIBillCity = JTjimReadFileData
                        Case "Contact"
                            tmpJimInfo.JTjimCIContact = JTjimReadFileData
                        Case "Telephone"
                            tmpJimInfo.JTjimCITelephone = JTjimReadFileData
                        Case "Email"
                            tmpJimInfo.JTjimCIEmail = JTjimReadFileData
                        Case "SecContact"
                            tmpJimInfo.JTjimCISecContact = JTjimReadFileData
                        Case "SecContPhone"
                            tmpJimInfo.JTjimCISecContPhone = JTjimReadFileData
                        Case "SecEmail"
                            tmpJimInfo.JTjimCISecEmail = JTjimReadFileData
                        Case "InvEmailPrim"
                            tmpJimInfo.JTjimCIInvEmailPrim = JTjimReadFileData
                        Case "InvEmailAlt"
                            tmpJimInfo.JTjimCIInvEmailAlt = JTjimReadFileData
                        Case "/Customers"
                            JTjimCIArray.Add(tmpJimInfo.JTjimCIJobName, tmpJimInfo)
                            tmpJimInfo = Nothing
                    End Select

                    '
                End If
                '
            End While
            readFile.Close()
            readFile = Nothing

        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        '
        Return ""
    End Function
    Public Function CustArrayFlush()
        ' ---------------------------------------------------------------------------
        ' This sub reformat job data into XML sentences and write the data to a file.
        ' ---------------------------------------------------------------------------
        Dim fileRecord As String = ""
        '
        Dim keys As ICollection = JTjimCIArray.Keys
        Dim k As String = ""
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTCustomers.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTCustomers.Bkup")
        End If
        ' -------------------------------------------------------------------------
        '
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)

            For Each k In keys
                fileRecord = "<Customers>"
                writeFile.WriteLine(fileRecord)
                '
                fileRecord = "<JobName>" & JTjimCIArray(k).JTjimCIJobName & "</JobName>"
                writeFile.WriteLine(fileRecord)

                fileRecord = "<TaxExemptJob>" & JTjimCIArray(k).JTjimCITaxExemptJob & "</TaxExemptJob>"
                writeFile.WriteLine(fileRecord)
                If JTjimCIArray(k).JTjimCIInvPDF <> "" Then
                    fileRecord = "<InvPDF>" & JTjimCIArray(k).JTjimCIInvPDF & "</InvPDF>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIInvUSPS <> "" Then
                    fileRecord = "<InvUSPS>" & JTjimCIArray(k).JTjimCIInvUSPS & "</InvUSPS>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIInvFile <> "" Then
                    fileRecord = "<InvFile>" & JTjimCIArray(k).JTjimCIInvFile & "</InvFile>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIBillStreet <> "" Then
                    fileRecord = "<BillStreet>" & JTjimCIArray(k).JTjimCIBillStreet & "</BillStreet>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIBillCity <> "" Then
                    fileRecord = "<BillCity>" & JTjimCIArray(k).JTjimCIBillCity & "</BillCity>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIContact <> "" Then
                    fileRecord = "<Contact>" & JTjimCIArray(k).JTjimCIContact & "</Contact>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCITelephone <> "" Then
                    fileRecord = "<Telephone>" & JTjimCIArray(k).JTjimCITelephone & "</Telephone>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIEmail <> "" Then
                    fileRecord = "<Email>" & JTjimCIArray(k).JTjimCIEmail & "</Email>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCISecContact <> "" Then
                    fileRecord = "<SecContact>" & JTjimCIArray(k).JTjimCISecContact & "</SecContact>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCISecContPhone <> "" Then
                    fileRecord = "<SecContPhone>" & JTjimCIArray(k).JTjimCISecContPhone & "</SecContPhone>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCISecEmail <> "" Then
                    fileRecord = "<SecEmail>" & JTjimCIArray(k).JTjimCISecEmail & "</SecEmail>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIInvEmailPrim = "X" Then
                    fileRecord = "<InvEmailPrim>" & JTjimCIArray(k).JTjimCIInvEmailPrim & "</InvEmailPrim>"
                    writeFile.WriteLine(fileRecord)
                End If
                If JTjimCIArray(k).JTjimCIInvEmailAlt = "X" Then
                    fileRecord = "<InvEmailAlt>" & JTjimCIArray(k).JTjimCIInvEmailAlt & "</InvEmailAlt>"
                    writeFile.WriteLine(fileRecord)
                End If
                fileRecord = "</Customers>"
                writeFile.WriteLine(fileRecord)
                '
            Next
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        ' ------------------------------------------------------------------------
        ' Recovery logic --- after successful save
        ' ------------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTCustomers.Bkup") Then
            My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTCustomers.Bkup")
        End If
        ' ------------------------------------------------------------------------

        Return ""
    End Function
    ''' <summary>
    '''  Simple function to read through JTjimJIArray (SL) and return the entire record as an argument.
    '''  Written to support the existing jobs LV on JTnj panel.
    ''' </summary>
    ''' <param name="tmpSub"></param>
    ''' <param name="tmpJTjimRec"></param>
    ''' <returns></returns>
    Public Function JTjimReadJtjim(ByRef tmpSub As Integer, ByRef tmpJTjimRec As JTjimJobInfo)
        If tmpSub >= JTjimJIArray.Count Then
            Return "END"
        End If
        Dim keys As ICollection = JTjimJIArray.Keys
        tmpJTjimRec = JTjimJIArray(keys(tmpSub))
        tmpSub += 1
        Return "DATA"
    End Function
    ''' <summary>
    ''' Function that return an image of a JTjimCIArray item for a requested customer.
    ''' </summary>
    ''' <param name="CustInfo">image as customer data</param>
    ''' <param name="tmpJob">Customer being requested</param>
    ''' <returns></returns>
    Public Function JTjimReadCustRec(ByRef CustInfo As JTjimCustInfo, tmpJob As String) As String
        If JTjimCIArray.ContainsKey(tmpJob) = True Then
            CustInfo = JTjimCIArray(tmpJob)
            Return "DATA"
        End If
        Return "ERROR"
    End Function
    ''' <summary>
    ''' Function to count number of jobs for a specified customer. Used in an invoicing request to see
    ''' if multiple jobs can be combined on the invoice.
    ''' </summary>
    ''' <param name="TmpCustID">Current Customer Name.</param>
    ''' <returns>Count of jobs for this customer.</returns>
    Public Function JTjimCntCustJobs(TmpCustID As String) As Integer
        Dim tmpJobCnt As Integer = 0
        Dim keys As ICollection = JTjimJIArray.Keys
        Dim k As String = Nothing
        ' -------------------------------------------------------------------------
        ' Modified routine to excluded Deact jobs from count - - - 3/3/2020
        ' -------------------------------------------------------------------------
        For Each k In keys
            If TmpCustID = JTjimJIArray(k).JTjimJIJobName Then
                If JTjimJIArray(k).JTjimJIDeactDate = "" Then
                    tmpJobCnt += 1
                End If
            End If
        Next
        Return tmpJobCnt
    End Function
    ''' <summary>
    ''' Function to pass Customer info back to JTas for data required to print invoices.
    ''' </summary>
    ''' <param name="tmpCustomer"></param>
    ''' <param name="tmpCustInfo"></param>
    ''' <returns></returns>
    Public Function JTjimGetBillingInfo(tmpCustomer As String, ByRef tmpCustInfo As JTjimCustInfo) As String
        If JTjimCIArray.ContainsKey(tmpCustomer) = True Then
            tmpCustInfo = JTjimCIArray(tmpCustomer)
        End If
        Return ""
    End Function
    ''' <summary>
    ''' Function to return value of switch to send invoice to accounting. Used by JTas.
    ''' </summary>
    ''' <param name="tmpCustID"></param>
    ''' <param name="tmpJobID"></param>
    ''' <returns></returns>
    Public Function JTjimSendToAcctng(tmpCustID As String, tmpJobID As String) As Boolean
        Dim tmpSwitch As Boolean = False
        Dim tmpCustName As String = FixedLengthString(tmpCustID, 10, "_")
        Dim tmpJobName As String = FixedLengthString(tmpJobID, 10, "_")
        Dim tmpKey As String = tmpCustName & tmpJobName
        If JTjimJIArray(tmpKey).JTjimJISndToAcct = "X" Then
            tmpSwitch = True
        End If
        Return tmpSwitch
    End Function
    '
    ''' <summary>
    ''' function to supply a list of customer names. Used by JTjm for autofill fields on panel.
    ''' </summary>
    ''' <param name="tmpSub"></param>
    ''' <param name="tmpCustName"></param>
    ''' <returns></returns>
    Public Function JTjimReadCI(ByRef tmpSub As Integer, ByRef tmpCustName As String) As String
        Dim key As ICollection = JTjimCIArray.Keys
        If tmpSub < key.Count Then
            tmpCustName = JTjimCIArray(key(tmpSub)).JTjimCIJobName
            Return "DATA"
        End If
        Return "END"
    End Function
    ''' <summary>
    ''' Function to check to see if customer name is valid.
    ''' </summary>
    ''' <param name="tmpCustname"></param>
    ''' <returns></returns>
    Public Function JTjimFindCI(tmpCustname As String) As Boolean
        If JTjimCIArray.ContainsKey(tmpCustname) = True Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Function to check for the existence of a specific Cust/Job combination.
    ''' </summary>
    ''' <param name="tmpCustname"></param>
    ''' <param name="tmpJobName"></param>
    ''' <returns></returns>
    Public Function JTjimFindJI(tmpCustname As String, tmpJobName As String) As Boolean
        Dim tmpCust As String = FixedLengthString(tmpCustname, 10, "_")
        Dim tmpJob As String = FixedLengthString(tmpJobName, 10, "_")
        Dim tmpKey As String = tmpCust & tmpJob
        If JTjimJIArray.ContainsKey(tmpKey) = True Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Function to send back Job Id for a given Customer. Used for creating iedit tables and autocomplete
    ''' ... initially used by JTjm.
    ''' </summary>
    ''' <param name="tmpSub">control to keep track of where the routine is in the array</param>
    ''' <param name="tmpCustName">Customer whose job are being located.</param>
    ''' <param name="tmpJobName">Job ID being passed back.</param>
    ''' <returns></returns>
    '''
    Public Function JTjimReadJI(ByRef tmpSub As Integer, ByRef tmpCustName As String, ByRef tmpJobName As String) As String
        Dim key As ICollection = JTjimJIArray.Keys
        Do While tmpSub < key.Count
            If JTjimJIArray(key(tmpSub)).JTjimJIJobName = tmpCustName Then
                If JTjimJIArray(key(tmpSub)).JTjimJISubName <> "" Then
                    tmpJobName = JTjimJIArray(key(tmpSub)).JTjimJISubName
                    tmpSub += 1
                    Return "DATA"
                End If
            End If
            tmpSub += 1
        Loop
        Return "END"
    End Function
    ''' <summary>
    ''' Function to perform the actual customer name or job name change actions for 
    '''    - JTjobs
    '''        - Job Info
    '''        - Special Pricing
    '''        - Non standard Invoice Settings
    '''    - JTCustomers
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTjimCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String


        ' Public Structure JTjmSettingsStru
        '      Dim ExCust As String
        '      Dim ExJob As String
        '      Dim NewCust As String
        '      Dim NewJob As String
        '      Dim CustNameChg As Boolean
        '      Dim JobNameChg As Boolean
        ' End Structure
        ' --------------------------------------------------
        ' Start of JTjimJIArray section
        ' --------------------------------------------------
        Dim tmpKeyCollection As New Collection()
        '
        Dim key As ICollection = JTjimJIArray.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTjimJIArray(k).JTjimJIJobName Then
                    tmpKeyCollection.Add(k)
                End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTjimJIArray(k).JTjimJIJobName Then
                    If JTjmSettings.ExJob = JTjimJIArray(k).JTjimJISubName Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
        End If
        ' Modify logic for JTjimJIArray placed here
        For Each k In tmpKeyCollection
            Dim tmpItem As JTjimJobInfo = JTjimJIArray(k)
            JTjimJIArray.Remove(k)
            '
            tmpItem.JTjimJIJobName = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.JTjimJISubName = JTjmSettings.NewJob
            End If
            Dim tmpJobName As String = FixedLengthString(tmpItem.JTjimJIJobName, 10, "_")
            Dim tmpSubName As String = FixedLengthString(tmpItem.JTjimJISubName, 10, "_")
            Dim tmpKey As String = tmpJobName & tmpSubName
            '    JTjimJIArray.Add(tmpKey, tmpItem)
            JTjimJIArrayAdd(tmpKey, tmpItem, "JTjim~2755 - JTjimCustJobNameChg")
        Next
        ' --------------------------------------------------
        ' End of JTjimJIArray section
        ' --------------------------------------------------
        '
        ' --------------------------------------------------
        ' Start of JTjimJSPsl section
        ' --------------------------------------------------
        '  Public Structure JTjimlaborcats
        '     Dim JTjimlcJobName As String
        '     Dim JTjimlcSubName As String
        '     Dim JTjimlcKey As String
        '     Dim JTjimlcName As String
        '     Dim JTjimlcStanRate As String
        '     Dim JTjimlcShopRate As String
        '  End Structure

        tmpKeyCollection.Clear()
        key = JTjimJSRsl.Keys
        k = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTjimJSRsl(k).JTjimlcJobName Then
                    tmpKeyCollection.Add(k)
                End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = JTjimJSRsl(k).JTjimlcJobName Then
                    If JTjmSettings.ExJob = JTjimJSRsl(k).JTjimlcSubName Then
                        tmpKeyCollection.Add(k)
                    End If
                End If

            Next
        End If
        ' Modify logic for JTjimJSPsl placed here .....
        For Each k In tmpKeyCollection
            Dim tmpItem As JTjimlaborcats = JTjimJSRsl(k)
            JTjimJSRsl.Remove(k)
            '
            tmpItem.JTjimlcJobName = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.JTjimlcSubName = JTjmSettings.NewJob
            End If
            Dim tmpKey As String = tmpItem.JTjimlcJobName & tmpItem.JTjimlcSubName & tmpItem.JTjimlcKey

            JTjimJSRsl.Add(tmpKey, tmpItem)
        Next
        ' --------------------------------------------------
        ' End of JTjimJSPsl section
        ' --------------------------------------------------
        '
        '
        ' --------------------------------------------------
        ' Start of JTjimCIArray section
        ' --------------------------------------------------
        tmpKeyCollection.Clear()
        '
        If JTjmSettings.CustNameChg = True Then

            If JTjimCIArray.ContainsKey(JTjmSettings.ExCust) = True Then
                tmpKeyCollection.Add(k)
            End If
            '
        End If
        ' Modify logic for JTjimCIArray placed here .....
        If JTjmSettings.CustNameChg = True Then
            Dim tmpItem As JTjimCustInfo = JTjimCIArray(JTjmSettings.ExCust)
            JTjimCIArray.Remove(JTjmSettings.ExCust)
            '
            tmpItem.JTjimCIJobName = JTjmSettings.NewCust
            '
            JTjimCIArray.Add(tmpItem.JTjimCIJobName, tmpItem)
        End If

        ' --------------------------------------------------
        ' End of JTjimCIArray section
        ' --------------------------------------------------
        Return ""
    End Function

    Public Function JTjimJIArrayAdd(ByRef tmpKey As String, ByRef tmpItem As JTjimJobInfo, ByRef tmpCalledBy As String) As Boolean
        Dim tmpReturnTag As Boolean = True
        Dim tmpDetails As String =
            "Key --->" & tmpKey & "< - - -" &
            "Called by --->" & tmpCalledBy & "<"
        JTjimJIArray.Add(tmpKey, tmpItem)
        '
        Return tmpReturnTag
    End Function
    ''' <summary>
    ''' Button on gbocJTjimCPWD to go aup panel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
        JTjimCallAUP()
    End Sub

    Private Sub lvJTjimLabRates_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles lvJTjimLabRates.SelectedIndexChanged

    End Sub


    '
End Class
