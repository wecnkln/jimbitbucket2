﻿''' <summary>
''' This class generates the JTvis panel. This panel is used for vendor lookup.
''' There are several options on the panel. They include:
'''     - Editing a specific vendor's information.
'''     - Show all categories of vendors on one panel.
'''     - Add a new vendor
''' The primary use of the panel, however, is to return an accurate Vendor Short Name 
''' to the NLC panel.
''' </summary>
Public Class JTvis
    Public JTvisNLCEntrySave As String = ""
    Sub JTvisInit(NLCEntryType As String)
        JTvisNLCEntrySave = NLCEntryType
        Dim tmpDispAll As String = ""
        If NLCEntryType = "B" Then
            tmpDispAll = "Y"
        End If
        cboxJTvisShowAll.Checked = False
        JTvisLoadLV(NLCEntryType, tmpDispAll)
        '       Me.MdiParent = JTstart
        JTnlcE.BringToFront()
        Me.BringToFront()
        Me.Show()
    End Sub
    Public Function JTvisLoadLV(NLCEntryType As String, tmpDispAll As String)
        '
        ' -----------------------------------------------------------------
        ' load data into lvVISvendors listview.
        ' -----------------------------------------------------------------
        '
        Dim tmpSub As Integer = 0
        Dim itmBuildvi(3) As String
        Dim lvLinevi As ListViewItem
        Dim ShortName As String = ""
        Dim VendName As String = ""
        Dim VendType As String = ""
        Dim LstActDate As String = ""
        Dim ActivityInterface As String = ""
        Dim Issues As String = ""
        '
        lvVISvendors.Items.Clear()
        tmpSub = 0
        '
        ' -----------------------------------------------------------
        ' Code to populate the VISvendor lv on the JTvis panel
        ' -----------------------------------------------------------
        Do While JTvi.JTviReadJTviforMM(tmpSub, ShortName, VendName, VendType,
                                        LstActDate, ActivityInterface, Issues) _
                                        = "DATA" Or tmpSub = 0
            '
            If VendType = NLCEntryType Or
                    VendType = "B" Or
                    tmpDispAll = "Y" Then
                itmBuildvi(0) = ShortName
                itmBuildvi(1) = VendName
                itmBuildvi(2) = VendType
                '
                lvLinevi = New ListViewItem(itmBuildvi)
                lvLinevi.ForeColor = Color.Black
                If tmpDispAll = "Y" Then
                    If VendType <> NLCEntryType Then
                        If VendType <> "B" Then
                            lvLinevi.ForeColor = Color.Blue
                        End If
                    End If

                End If
                lvVISvendors.Items.Add(lvLinevi)
            End If
            '
            tmpSub += 1
        Loop
        If tmpDispAll = "Y" Then
            cboxJTvisShowAll.Checked = True
        Else
            cboxJTvisShowAll.Checked = False
        End If
        Return ""
    End Function

    Private Sub btnJTvisClosePanel_Click(sender As Object, e As EventArgs) Handles btnJTvisClosePanel.Click
        Me.Hide()
        JTnlcE.BringToFront()
    End Sub

    Private Sub cboxJTvisShowAll_CheckedChanged(sender As Object, e As EventArgs) Handles cboxJTvisShowAll.CheckedChanged
        If cboxJTvisShowAll.Checked = True Then
            Me.Hide()
            JTvisLoadLV(JTvisNLCEntrySave, "Y")
            Me.Show()
        Else
            Me.Hide()
            JTvisLoadLV(JTvisNLCEntrySave, "N")
            Me.Show()
        End If
    End Sub



    Private Sub lvVISvendors_DoubleClick(sender As Object, e As EventArgs) Handles lvVISvendors.DoubleClick
        Dim tmpShortName As String = lvVISvendors.SelectedItems.Item(0).Text
        If cboxJTvisEdit.Checked = False Then
            JTnlcE.cboxJTnlcESupplier.Text = tmpShortName
            JTnlcE.cboxJTnlcESupplier.Select()
            Me.Hide()
            Exit Sub
        Else
            JTvi.JTviUpdateEntry(tmpShortName, lvVISvendors.SelectedItems(0).SubItems(2).Text)
        End If
        cboxJTvisEdit.Checked = False
    End Sub

    Private Sub rbtnJTvisNewVendor_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnJTvisNewVendor.CheckedChanged
        If rbtnJTvisNewVendor.Checked = True Then
            JTvi.JTviUpdateEntry("", "")
        End If
        rbtnJTvisNewVendor.Checked = False
    End Sub
End Class