﻿Imports System.Net.Mail
Public Class JTEmail
    '
    ' ------------------------------------------------------------------------------
    ' This site has different sign on stuff including app specific authentication.
    ' http://www.codeguru.com/columns/dotnet/sending-email-using-.net.html
    ' Probably should change to this.
    '-------------------------------------------------------------------------------
    '

    ' -------------------------------------------------------------------------------
    ' I Sub to send email.                                                          I
    ' I Got code from: https://www.tutorialspoint.com/vb.net/vb.net_send_email.htm  I
    ' I Site also has good explanation.                                             I
    ' I Had to lower Gmail security settings to use code.                           I
    ' -------------------------------------------------------------------------------
    '
    Dim tmpStatus As String = ""
    Public JTEmailPath As String = ""
    '
    Private Sub btnSend_Click_1(sender As Object, e As EventArgs) Handles btnSend.Click
        Me.TopMost = False
        '       If JTEmailPath <> "FUNC" Then
        Dim tmpStatus As String = ""



        JTEmailSend(tmpStatus)
            If tmpStatus = "SENT" Then
            MsgBox("Message Sent Successfully --- To: " & txtTo.Text)
        Else
            MsgBox("Email Send Failure --- Intended Addressee - " & txtTo.Text)
        End If
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub
    Private Function JTEmailSend(ByRef tmpStatus As String) As String
        tmpStatus = "ERROR"
        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(JTVarMaint.CompanyContactEmail, JTVarMaint.GoogleValidation)
            Smtp_Server.Port = 587
            Smtp_Server.EnableSsl = True
            Smtp_Server.Host = "smtp.gmail.com"
            ' ----------------------------------------------------------------------------------------------------------------
            ' Code I found to try to display HTML text in a textbox ...
            '           txtMessage.Text = Smtp_Server.Host.HtmlDecode("your html text")
            ' ----------------------------------------------------------------------------------------------------------------

            e_mail = New MailMessage()
            e_mail.From = New MailAddress(txtFrom.Text)
            '          MsgBox("txtfrom - " & txtFrom.Text)
            If txtTo.Text <> "" Then
                e_mail.To.Add(txtTo.Text)
                '         MsgBox("txtto - " & txtTo.Text)
            End If
            If txtCC.Text <> "" Then
                e_mail.CC.Add(txtCC.Text)
                '        MsgBox("txtCC - " & txtCC.Text)
            End If
            '   If txtBCC.Text <> "" Then
            '  e_mail.Bcc.Add(txtBCC.Text)
            '       MsgBox("txtBCC - " & txtBCC.Text)
            ' End If
            If txtAttachment.Text <> "" Then
                e_mail.Attachments.Add(New Attachment(txtAttachment.Text))
            End If
            If txtSubject.Text <> "" Then
                e_mail.Subject = txtSubject.Text
            End If
            e_mail.IsBodyHtml = False
            If txtMessage.Text <> "" Then
                e_mail.Body = txtMessage.Text
            End If
            Smtp_Server.Send(e_mail)
            tmpStatus = "SENT"

        Catch error_t As Exception
            JTErrHandle.JTEHinit("JTEmailSend")
            ' ---------------------------------------------------------------------------------------------
            ' Following msgbox code has been replaced by JTErrHandle code. Can be deleted in the future.
            ' 11/29/2020
            ' ---------------------------------------------------------------------------------------------
            '   MsgBox("Email server refused the connection request from Job Information Manager" & vbCrLf &
            '          "The most likely cause is an incorrect password (Utilities/Global Settings)" & vbCrLf &
            '          "If email has been working and suddenly stopped, it is likely your email" & vbCrLf &
            '          "password has been changed. If your email password is changed, Google voids" & vbCrLf &
            '          "any other passwords for this account. A new one has to be generated. Go to your" & vbCrLf &
            '          "Google account security section. A new password (16 character alpha/numeric) can" & vbCrLf &
            '          "be created here and enter into Job Information Manager." & vbCrLf & error_t.ToString)
            ' ------------------------------------------------------------------------------------------------
        End Try
        Return ""
    End Function
    '
    '
    Public Sub JTEmailInit()
        JTEmailPath = ""
        Me.Show()
    End Sub
    '

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
        JTMainMenu.JTMMinit()
    End Sub
    Public Sub JTSndEmail(tmpToAdd As String, tmpCC As String, tmpBCC As String, tmpFromAdd As String, tmpSubject As String,
                               tmpMsg As String, tmpAttach As String)
        ' ----------------------------------------------------------------------------
        ' Function for sending email from other sections of the system.
        ' "SENT" return, if good transmission ... "ERROR" if something has gone wrong.
        ' ----------------------------------------------------------------------------
        '
        '  JTEmailPath = "FUNC"

        txtTo.Text = tmpToAdd
        txtCC.Text = tmpCC
        txtBCC.Text = tmpBCC
        txtFrom.Text = tmpFromAdd
        txtAttachment.Text = tmpAttach
        txtSubject.Text = tmpSubject
        txtMessage.Text = tmpMsg
        Me.TopMost = True
        ' hide mainmenu
        JTMainMenu.Hide()
        tmpStatus = ""
        Me.MdiParent = JTstart
        Me.Show()
    End Sub

    Private Sub txtTo_LostFocus(sender As Object, e As EventArgs) Handles txtTo.LostFocus
        If txtTo.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtTo.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(EM.001)", txtTo, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtCC_LostFocus(sender As Object, e As EventArgs) Handles txtCC.LostFocus
        If txtCC.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtCC.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(EM.001)", txtCC, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub


End Class



