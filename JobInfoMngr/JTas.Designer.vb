﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(JTas))
        Me.tabJTas = New System.Windows.Forms.TabControl()
        Me.tabLabor = New System.Windows.Forms.TabPage()
        Me.txtJtasLabTlDol = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtJTasLabMrgnDol = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtJTasLabHrs = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.JTaslvLab = New System.Windows.Forms.ListView()
        Me.lvLabDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvLabCat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvLabDescrpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvHours = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvCharge = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvWorker = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMargin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvLabJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvLabSubJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabMaterial = New System.Windows.Forms.TabPage()
        Me.txtJTasMatCstInvDol = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtJTasMatMrgnDol = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtJTasMatCostDol = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.JTaslvMat = New System.Windows.Forms.ListView()
        Me.lvMatDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatSupplier = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatDescrpt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatSupCst = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatTBInvd = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatInvNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatMrgn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvMatSubJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabSubContrctr = New System.Windows.Forms.TabPage()
        Me.txtJTasSubCstInvDol = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtJTasSubMrgnDol = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtJTasSubCstDol = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.JTaslvSub = New System.Windows.Forms.ListView()
        Me.lvSubDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubSupplier = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubSupDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubSupInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubCustChg = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubInvNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubMrgn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvSubJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabAUP = New System.Windows.Forms.TabPage()
        Me.txtJTasAUPInvDol = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.JTaslvAUP = New System.Windows.Forms.ListView()
        Me.lvAUPDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvAUPDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvAUPAmount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvAUPFinalInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvAUPJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvAUPSubJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tabJobPandL = New System.Windows.Forms.TabPage()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtJTasIHInvTl = New System.Windows.Forms.TextBox()
        Me.txtJTasIHEstMrgnTl = New System.Windows.Forms.TextBox()
        Me.txtJTasIHAUPInvTl = New System.Windows.Forms.TextBox()
        Me.txtJTasIHSubTl = New System.Windows.Forms.TextBox()
        Me.txtJTasIHMatTl = New System.Windows.Forms.TextBox()
        Me.txtJTasIHLabTl = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lvJTasInvHist = New System.Windows.Forms.ListView()
        Me.JTasIHInvDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHInvNum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHLab = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHMat = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHSub = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHInvTl = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHEstMrgn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHAUPInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHAUPNet = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lvJTasIHSubJobID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtJTasJobID = New System.Windows.Forms.TextBox()
        Me.txtJTasJobDesc = New System.Windows.Forms.TextBox()
        Me.txtJTasSubID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJTasPricing = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboxJTasDeact = New System.Windows.Forms.CheckBox()
        Me.mtxtJTasCutOffDate = New System.Windows.Forms.MaskedTextBox()
        Me.mtxtJTasInvDate = New System.Windows.Forms.MaskedTextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BtnJTasInvoice = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtJTasASaupSel = New System.Windows.Forms.TextBox()
        Me.txtJTasASaupUnb = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtJTasASTlSel = New System.Windows.Forms.TextBox()
        Me.txtJTasASTlUnb = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtJTasASSubSel = New System.Windows.Forms.TextBox()
        Me.txtJTasASMatSel = New System.Windows.Forms.TextBox()
        Me.txtJTasASLabSel = New System.Windows.Forms.TextBox()
        Me.txtJTasASLabUnb = New System.Windows.Forms.TextBox()
        Me.txtJTasASMatUnb = New System.Windows.Forms.TextBox()
        Me.txtJTasASSubUnb = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnJTasLineItemCorr = New System.Windows.Forms.Button()
        Me.tabJTas.SuspendLayout()
        Me.tabLabor.SuspendLayout()
        Me.tabMaterial.SuspendLayout()
        Me.tabSubContrctr.SuspendLayout()
        Me.tabAUP.SuspendLayout()
        Me.tabJobPandL.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabJTas
        '
        Me.tabJTas.Controls.Add(Me.tabLabor)
        Me.tabJTas.Controls.Add(Me.tabMaterial)
        Me.tabJTas.Controls.Add(Me.tabSubContrctr)
        Me.tabJTas.Controls.Add(Me.tabAUP)
        Me.tabJTas.Controls.Add(Me.tabJobPandL)
        Me.tabJTas.Location = New System.Drawing.Point(15, 79)
        Me.tabJTas.Name = "tabJTas"
        Me.tabJTas.SelectedIndex = 0
        Me.tabJTas.Size = New System.Drawing.Size(819, 381)
        Me.tabJTas.TabIndex = 0
        '
        'tabLabor
        '
        Me.tabLabor.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tabLabor.Controls.Add(Me.txtJtasLabTlDol)
        Me.tabLabor.Controls.Add(Me.Label13)
        Me.tabLabor.Controls.Add(Me.txtJTasLabMrgnDol)
        Me.tabLabor.Controls.Add(Me.Label11)
        Me.tabLabor.Controls.Add(Me.txtJTasLabHrs)
        Me.tabLabor.Controls.Add(Me.Label10)
        Me.tabLabor.Controls.Add(Me.JTaslvLab)
        Me.tabLabor.Location = New System.Drawing.Point(4, 25)
        Me.tabLabor.Name = "tabLabor"
        Me.tabLabor.Padding = New System.Windows.Forms.Padding(3)
        Me.tabLabor.Size = New System.Drawing.Size(811, 352)
        Me.tabLabor.TabIndex = 0
        Me.tabLabor.Text = "Labor"
        '
        'txtJtasLabTlDol
        '
        Me.txtJtasLabTlDol.Location = New System.Drawing.Point(352, 319)
        Me.txtJtasLabTlDol.Name = "txtJtasLabTlDol"
        Me.txtJtasLabTlDol.ReadOnly = True
        Me.txtJtasLabTlDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJtasLabTlDol.TabIndex = 14
        Me.txtJtasLabTlDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(256, 321)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 16)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Total $$$"
        '
        'txtJTasLabMrgnDol
        '
        Me.txtJTasLabMrgnDol.Location = New System.Drawing.Point(586, 318)
        Me.txtJTasLabMrgnDol.Name = "txtJTasLabMrgnDol"
        Me.txtJTasLabMrgnDol.ReadOnly = True
        Me.txtJTasLabMrgnDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasLabMrgnDol.TabIndex = 12
        Me.txtJTasLabMrgnDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(461, 321)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 16)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Est. Mrgn $$$"
        '
        'txtJTasLabHrs
        '
        Me.txtJTasLabHrs.Location = New System.Drawing.Point(133, 319)
        Me.txtJTasLabHrs.Name = "txtJTasLabHrs"
        Me.txtJTasLabHrs.ReadOnly = True
        Me.txtJTasLabHrs.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasLabHrs.TabIndex = 10
        Me.txtJTasLabHrs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(48, 321)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 16)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Hours"
        '
        'JTaslvLab
        '
        Me.JTaslvLab.CheckBoxes = True
        Me.JTaslvLab.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvLabDate, Me.lvLabCat, Me.lvLabDescrpt, Me.lvHours, Me.lvCharge, Me.lvWorker, Me.lvMargin, Me.lvLabJobID, Me.lvLabSubJobID})
        Me.JTaslvLab.FullRowSelect = True
        Me.JTaslvLab.HideSelection = False
        Me.JTaslvLab.Location = New System.Drawing.Point(30, 44)
        Me.JTaslvLab.Name = "JTaslvLab"
        Me.JTaslvLab.Size = New System.Drawing.Size(751, 266)
        Me.JTaslvLab.TabIndex = 0
        Me.JTaslvLab.UseCompatibleStateImageBehavior = False
        Me.JTaslvLab.View = System.Windows.Forms.View.Details
        '
        'lvLabDate
        '
        Me.lvLabDate.Text = "             Date"
        Me.lvLabDate.Width = 110
        '
        'lvLabCat
        '
        Me.lvLabCat.Text = "Lab Cat"
        Me.lvLabCat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvLabDescrpt
        '
        Me.lvLabDescrpt.Text = "Decription of Work"
        Me.lvLabDescrpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvLabDescrpt.Width = 150
        '
        'lvHours
        '
        Me.lvHours.Text = "Hours"
        Me.lvHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvHours.Width = 75
        '
        'lvCharge
        '
        Me.lvCharge.Text = "Charge"
        Me.lvCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvCharge.Width = 85
        '
        'lvWorker
        '
        Me.lvWorker.Text = "Worker"
        Me.lvWorker.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvMargin
        '
        Me.lvMargin.Text = "Margin"
        Me.lvMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lvLabJobID
        '
        Me.lvLabJobID.Text = "JobID"
        Me.lvLabJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvLabJobID.Width = 0
        '
        'lvLabSubJobID
        '
        Me.lvLabSubJobID.Text = "SubJobID"
        Me.lvLabSubJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvLabSubJobID.Width = 0
        '
        'tabMaterial
        '
        Me.tabMaterial.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tabMaterial.Controls.Add(Me.txtJTasMatCstInvDol)
        Me.tabMaterial.Controls.Add(Me.Label14)
        Me.tabMaterial.Controls.Add(Me.txtJTasMatMrgnDol)
        Me.tabMaterial.Controls.Add(Me.Label15)
        Me.tabMaterial.Controls.Add(Me.txtJTasMatCostDol)
        Me.tabMaterial.Controls.Add(Me.Label16)
        Me.tabMaterial.Controls.Add(Me.JTaslvMat)
        Me.tabMaterial.Location = New System.Drawing.Point(4, 25)
        Me.tabMaterial.Name = "tabMaterial"
        Me.tabMaterial.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMaterial.Size = New System.Drawing.Size(811, 352)
        Me.tabMaterial.TabIndex = 1
        Me.tabMaterial.Text = "Material"
        '
        'txtJTasMatCstInvDol
        '
        Me.txtJTasMatCstInvDol.Location = New System.Drawing.Point(349, 316)
        Me.txtJTasMatCstInvDol.Name = "txtJTasMatCstInvDol"
        Me.txtJTasMatCstInvDol.ReadOnly = True
        Me.txtJTasMatCstInvDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasMatCstInvDol.TabIndex = 20
        Me.txtJTasMatCstInvDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(253, 318)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 16)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Cust. Inv. $$$"
        '
        'txtJTasMatMrgnDol
        '
        Me.txtJTasMatMrgnDol.Location = New System.Drawing.Point(583, 315)
        Me.txtJTasMatMrgnDol.Name = "txtJTasMatMrgnDol"
        Me.txtJTasMatMrgnDol.ReadOnly = True
        Me.txtJTasMatMrgnDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasMatMrgnDol.TabIndex = 18
        Me.txtJTasMatMrgnDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(458, 318)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 16)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Margin $$$"
        '
        'txtJTasMatCostDol
        '
        Me.txtJTasMatCostDol.Location = New System.Drawing.Point(130, 316)
        Me.txtJTasMatCostDol.Name = "txtJTasMatCostDol"
        Me.txtJTasMatCostDol.ReadOnly = True
        Me.txtJTasMatCostDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasMatCostDol.TabIndex = 16
        Me.txtJTasMatCostDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(45, 318)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 16)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Cost $$$"
        '
        'JTaslvMat
        '
        Me.JTaslvMat.CheckBoxes = True
        Me.JTaslvMat.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvMatDate, Me.lvMatSupplier, Me.lvMatDescrpt, Me.lvMatSupCst, Me.lvMatTBInvd, Me.lvMatInvNum, Me.lvMatMrgn, Me.lvMatJobID, Me.lvMatSubJobID})
        Me.JTaslvMat.FullRowSelect = True
        Me.JTaslvMat.HideSelection = False
        Me.JTaslvMat.Location = New System.Drawing.Point(35, 49)
        Me.JTaslvMat.Name = "JTaslvMat"
        Me.JTaslvMat.Size = New System.Drawing.Size(737, 255)
        Me.JTaslvMat.TabIndex = 1
        Me.JTaslvMat.UseCompatibleStateImageBehavior = False
        Me.JTaslvMat.View = System.Windows.Forms.View.Details
        '
        'lvMatDate
        '
        Me.lvMatDate.Text = "           Date"
        Me.lvMatDate.Width = 110
        '
        'lvMatSupplier
        '
        Me.lvMatSupplier.Text = "Supplier"
        Me.lvMatSupplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMatSupplier.Width = 90
        '
        'lvMatDescrpt
        '
        Me.lvMatDescrpt.Text = "Material Description"
        Me.lvMatDescrpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMatDescrpt.Width = 130
        '
        'lvMatSupCst
        '
        Me.lvMatSupCst.Text = "Sup. Inv."
        Me.lvMatSupCst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMatSupCst.Width = 70
        '
        'lvMatTBInvd
        '
        Me.lvMatTBInvd.Text = "Cust. Chg."
        Me.lvMatTBInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMatTBInvd.Width = 70
        '
        'lvMatInvNum
        '
        Me.lvMatInvNum.Text = "Inv. #"
        Me.lvMatInvNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvMatMrgn
        '
        Me.lvMatMrgn.Text = "Margin"
        Me.lvMatMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvMatMrgn.Width = 70
        '
        'lvMatJobID
        '
        Me.lvMatJobID.Text = "JobID"
        Me.lvMatJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMatJobID.Width = 0
        '
        'lvMatSubJobID
        '
        Me.lvMatSubJobID.Text = "SubJobID"
        Me.lvMatSubJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvMatSubJobID.Width = 0
        '
        'tabSubContrctr
        '
        Me.tabSubContrctr.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tabSubContrctr.Controls.Add(Me.txtJTasSubCstInvDol)
        Me.tabSubContrctr.Controls.Add(Me.Label17)
        Me.tabSubContrctr.Controls.Add(Me.txtJTasSubMrgnDol)
        Me.tabSubContrctr.Controls.Add(Me.Label18)
        Me.tabSubContrctr.Controls.Add(Me.txtJTasSubCstDol)
        Me.tabSubContrctr.Controls.Add(Me.Label19)
        Me.tabSubContrctr.Controls.Add(Me.JTaslvSub)
        Me.tabSubContrctr.Location = New System.Drawing.Point(4, 25)
        Me.tabSubContrctr.Name = "tabSubContrctr"
        Me.tabSubContrctr.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSubContrctr.Size = New System.Drawing.Size(811, 352)
        Me.tabSubContrctr.TabIndex = 2
        Me.tabSubContrctr.Text = "SubContractors"
        '
        'txtJTasSubCstInvDol
        '
        Me.txtJTasSubCstInvDol.Location = New System.Drawing.Point(346, 316)
        Me.txtJTasSubCstInvDol.Name = "txtJTasSubCstInvDol"
        Me.txtJTasSubCstInvDol.ReadOnly = True
        Me.txtJTasSubCstInvDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasSubCstInvDol.TabIndex = 26
        Me.txtJTasSubCstInvDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(250, 318)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 16)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "Cust. Inv.$$$"
        '
        'txtJTasSubMrgnDol
        '
        Me.txtJTasSubMrgnDol.Location = New System.Drawing.Point(580, 315)
        Me.txtJTasSubMrgnDol.Name = "txtJTasSubMrgnDol"
        Me.txtJTasSubMrgnDol.ReadOnly = True
        Me.txtJTasSubMrgnDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasSubMrgnDol.TabIndex = 24
        Me.txtJTasSubMrgnDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(455, 318)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 16)
        Me.Label18.TabIndex = 23
        Me.Label18.Text = "Margin $$$"
        '
        'txtJTasSubCstDol
        '
        Me.txtJTasSubCstDol.Location = New System.Drawing.Point(127, 316)
        Me.txtJTasSubCstDol.Name = "txtJTasSubCstDol"
        Me.txtJTasSubCstDol.ReadOnly = True
        Me.txtJTasSubCstDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasSubCstDol.TabIndex = 22
        Me.txtJTasSubCstDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(42, 318)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(58, 16)
        Me.Label19.TabIndex = 21
        Me.Label19.Text = "Cost $$$"
        '
        'JTaslvSub
        '
        Me.JTaslvSub.CheckBoxes = True
        Me.JTaslvSub.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvSubDate, Me.lvSubSupplier, Me.lvSubSupDesc, Me.lvSubSupInv, Me.lvSubCustChg, Me.lvSubInvNum, Me.lvSubMrgn, Me.lvJobID, Me.lvSubJobID})
        Me.JTaslvSub.FullRowSelect = True
        Me.JTaslvSub.HideSelection = False
        Me.JTaslvSub.Location = New System.Drawing.Point(29, 51)
        Me.JTaslvSub.Name = "JTaslvSub"
        Me.JTaslvSub.Size = New System.Drawing.Size(756, 255)
        Me.JTaslvSub.TabIndex = 0
        Me.JTaslvSub.UseCompatibleStateImageBehavior = False
        Me.JTaslvSub.View = System.Windows.Forms.View.Details
        '
        'lvSubDate
        '
        Me.lvSubDate.Text = "           Date"
        Me.lvSubDate.Width = 110
        '
        'lvSubSupplier
        '
        Me.lvSubSupplier.Text = "SubContract"
        Me.lvSubSupplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvSubSupplier.Width = 90
        '
        'lvSubSupDesc
        '
        Me.lvSubSupDesc.Text = "Service Purchased"
        Me.lvSubSupDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvSubSupDesc.Width = 130
        '
        'lvSubSupInv
        '
        Me.lvSubSupInv.Text = "Sub. Inv."
        Me.lvSubSupInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvSubSupInv.Width = 70
        '
        'lvSubCustChg
        '
        Me.lvSubCustChg.Text = "Cust. Chg."
        Me.lvSubCustChg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvSubCustChg.Width = 70
        '
        'lvSubInvNum
        '
        Me.lvSubInvNum.Text = "Inv. #"
        Me.lvSubInvNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvSubMrgn
        '
        Me.lvSubMrgn.Text = "Margin"
        Me.lvSubMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvSubMrgn.Width = 70
        '
        'lvJobID
        '
        Me.lvJobID.Text = "JobID"
        Me.lvJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvJobID.Width = 0
        '
        'lvSubJobID
        '
        Me.lvSubJobID.Text = "SubJobID"
        Me.lvSubJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvSubJobID.Width = 0
        '
        'tabAUP
        '
        Me.tabAUP.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tabAUP.Controls.Add(Me.txtJTasAUPInvDol)
        Me.tabAUP.Controls.Add(Me.Label25)
        Me.tabAUP.Controls.Add(Me.JTaslvAUP)
        Me.tabAUP.Location = New System.Drawing.Point(4, 25)
        Me.tabAUP.Name = "tabAUP"
        Me.tabAUP.Padding = New System.Windows.Forms.Padding(3)
        Me.tabAUP.Size = New System.Drawing.Size(811, 352)
        Me.tabAUP.TabIndex = 3
        Me.tabAUP.Text = "AUPInv"
        '
        'txtJTasAUPInvDol
        '
        Me.txtJTasAUPInvDol.Location = New System.Drawing.Point(492, 316)
        Me.txtJTasAUPInvDol.Name = "txtJTasAUPInvDol"
        Me.txtJTasAUPInvDol.ReadOnly = True
        Me.txtJTasAUPInvDol.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasAUPInvDol.TabIndex = 22
        Me.txtJTasAUPInvDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(396, 318)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(82, 16)
        Me.Label25.TabIndex = 21
        Me.Label25.Text = "AUP Inv. $$$"
        '
        'JTaslvAUP
        '
        Me.JTaslvAUP.CheckBoxes = True
        Me.JTaslvAUP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvAUPDate, Me.lvAUPDesc, Me.lvAUPAmount, Me.lvAUPFinalInv, Me.lvAUPJobID, Me.lvAUPSubJobID})
        Me.JTaslvAUP.HideSelection = False
        Me.JTaslvAUP.Location = New System.Drawing.Point(35, 49)
        Me.JTaslvAUP.Name = "JTaslvAUP"
        Me.JTaslvAUP.Size = New System.Drawing.Size(740, 255)
        Me.JTaslvAUP.TabIndex = 1
        Me.JTaslvAUP.UseCompatibleStateImageBehavior = False
        Me.JTaslvAUP.View = System.Windows.Forms.View.Details
        '
        'lvAUPDate
        '
        Me.lvAUPDate.Text = "           Date"
        Me.lvAUPDate.Width = 110
        '
        'lvAUPDesc
        '
        Me.lvAUPDesc.Text = "AUP Entry Description"
        Me.lvAUPDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvAUPDesc.Width = 350
        '
        'lvAUPAmount
        '
        Me.lvAUPAmount.Text = "Amount"
        Me.lvAUPAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvAUPAmount.Width = 70
        '
        'lvAUPFinalInv
        '
        Me.lvAUPFinalInv.Text = "Final Inv."
        Me.lvAUPFinalInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvAUPFinalInv.Width = 80
        '
        'lvAUPJobID
        '
        Me.lvAUPJobID.Text = "JobID"
        Me.lvAUPJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvAUPJobID.Width = 0
        '
        'lvAUPSubJobID
        '
        Me.lvAUPSubJobID.Text = "SubJobID"
        Me.lvAUPSubJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvAUPSubJobID.Width = 0
        '
        'tabJobPandL
        '
        Me.tabJobPandL.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.tabJobPandL.Controls.Add(Me.TextBox1)
        Me.tabJobPandL.Controls.Add(Me.GroupBox2)
        Me.tabJobPandL.Controls.Add(Me.Label20)
        Me.tabJobPandL.Controls.Add(Me.lvJTasInvHist)
        Me.tabJobPandL.Location = New System.Drawing.Point(4, 25)
        Me.tabJobPandL.Name = "tabJobPandL"
        Me.tabJobPandL.Padding = New System.Windows.Forms.Padding(3)
        Me.tabJobPandL.Size = New System.Drawing.Size(811, 352)
        Me.tabJobPandL.TabIndex = 4
        Me.tabJobPandL.Text = "Invoice History"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(23, 229)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(219, 112)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Text = "DoubleClick an invoice line in the above table to display a PDF of the original i" &
    "nvoice. A duplicate copy of the invoice can be printed through this display."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtJTasIHInvTl)
        Me.GroupBox2.Controls.Add(Me.txtJTasIHEstMrgnTl)
        Me.GroupBox2.Controls.Add(Me.txtJTasIHAUPInvTl)
        Me.GroupBox2.Controls.Add(Me.txtJTasIHSubTl)
        Me.GroupBox2.Controls.Add(Me.txtJTasIHMatTl)
        Me.GroupBox2.Controls.Add(Me.txtJTasIHLabTl)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(257, 229)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(433, 111)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "TD Financial Recap"
        '
        'txtJTasIHInvTl
        '
        Me.txtJTasIHInvTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHInvTl.Location = New System.Drawing.Point(329, 25)
        Me.txtJTasIHInvTl.Name = "txtJTasIHInvTl"
        Me.txtJTasIHInvTl.ReadOnly = True
        Me.txtJTasIHInvTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHInvTl.TabIndex = 32
        Me.txtJTasIHInvTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasIHEstMrgnTl
        '
        Me.txtJTasIHEstMrgnTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHEstMrgnTl.Location = New System.Drawing.Point(329, 56)
        Me.txtJTasIHEstMrgnTl.Name = "txtJTasIHEstMrgnTl"
        Me.txtJTasIHEstMrgnTl.ReadOnly = True
        Me.txtJTasIHEstMrgnTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHEstMrgnTl.TabIndex = 31
        Me.txtJTasIHEstMrgnTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasIHAUPInvTl
        '
        Me.txtJTasIHAUPInvTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHAUPInvTl.Location = New System.Drawing.Point(329, 86)
        Me.txtJTasIHAUPInvTl.Name = "txtJTasIHAUPInvTl"
        Me.txtJTasIHAUPInvTl.ReadOnly = True
        Me.txtJTasIHAUPInvTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHAUPInvTl.TabIndex = 30
        Me.txtJTasIHAUPInvTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasIHSubTl
        '
        Me.txtJTasIHSubTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHSubTl.Location = New System.Drawing.Point(115, 86)
        Me.txtJTasIHSubTl.Name = "txtJTasIHSubTl"
        Me.txtJTasIHSubTl.ReadOnly = True
        Me.txtJTasIHSubTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHSubTl.TabIndex = 29
        Me.txtJTasIHSubTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasIHMatTl
        '
        Me.txtJTasIHMatTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHMatTl.Location = New System.Drawing.Point(114, 56)
        Me.txtJTasIHMatTl.Name = "txtJTasIHMatTl"
        Me.txtJTasIHMatTl.ReadOnly = True
        Me.txtJTasIHMatTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHMatTl.TabIndex = 28
        Me.txtJTasIHMatTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasIHLabTl
        '
        Me.txtJTasIHLabTl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasIHLabTl.Location = New System.Drawing.Point(114, 25)
        Me.txtJTasIHLabTl.Name = "txtJTasIHLabTl"
        Me.txtJTasIHLabTl.ReadOnly = True
        Me.txtJTasIHLabTl.Size = New System.Drawing.Size(83, 24)
        Me.txtJTasIHLabTl.TabIndex = 27
        Me.txtJTasIHLabTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(214, 89)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(92, 18)
        Me.Label26.TabIndex = 25
        Me.Label26.Text = "AUPInvoices"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(214, 59)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(83, 18)
        Me.Label27.TabIndex = 26
        Me.Label27.Text = "Est. Margin"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(6, 28)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(46, 18)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = "Labor"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(6, 59)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 18)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Materials"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(6, 89)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(88, 18)
        Me.Label22.TabIndex = 3
        Me.Label22.Text = "Subcontract"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(214, 28)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(91, 18)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Invoice Total"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(25, 15)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(135, 20)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Invoice History"
        '
        'lvJTasInvHist
        '
        Me.lvJTasInvHist.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.JTasIHInvDate, Me.lvJTasIHInvNum, Me.lvJTasIHLab, Me.lvJTasIHMat, Me.lvJTasIHSub, Me.lvJTasIHInvTl, Me.lvJTasIHEstMrgn, Me.lvJTasIHAUPInv, Me.lvJTasIHAUPNet, Me.lvJTasIHJobID, Me.lvJTasIHSubJobID})
        Me.lvJTasInvHist.FullRowSelect = True
        Me.lvJTasInvHist.HideSelection = False
        Me.lvJTasInvHist.Location = New System.Drawing.Point(18, 46)
        Me.lvJTasInvHist.MultiSelect = False
        Me.lvJTasInvHist.Name = "lvJTasInvHist"
        Me.lvJTasInvHist.RightToLeftLayout = True
        Me.lvJTasInvHist.Size = New System.Drawing.Size(773, 175)
        Me.lvJTasInvHist.TabIndex = 0
        Me.lvJTasInvHist.UseCompatibleStateImageBehavior = False
        Me.lvJTasInvHist.View = System.Windows.Forms.View.Details
        '
        'JTasIHInvDate
        '
        Me.JTasIHInvDate.Text = "Invoice Date"
        Me.JTasIHInvDate.Width = 100
        '
        'lvJTasIHInvNum
        '
        Me.lvJTasIHInvNum.Text = "Inv #"
        Me.lvJTasIHInvNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lvJTasIHLab
        '
        Me.lvJTasIHLab.Text = "Labor"
        Me.lvJTasIHLab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHLab.Width = 70
        '
        'lvJTasIHMat
        '
        Me.lvJTasIHMat.Text = "Material"
        Me.lvJTasIHMat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHMat.Width = 70
        '
        'lvJTasIHSub
        '
        Me.lvJTasIHSub.Text = "Subcontract"
        Me.lvJTasIHSub.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHSub.Width = 70
        '
        'lvJTasIHInvTl
        '
        Me.lvJTasIHInvTl.Text = "Inv. Total"
        Me.lvJTasIHInvTl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHInvTl.Width = 70
        '
        'lvJTasIHEstMrgn
        '
        Me.lvJTasIHEstMrgn.Text = "Cst+ Mrgn"
        Me.lvJTasIHEstMrgn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHEstMrgn.Width = 80
        '
        'lvJTasIHAUPInv
        '
        Me.lvJTasIHAUPInv.Text = "AUP Inv"
        Me.lvJTasIHAUPInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHAUPInv.Width = 70
        '
        'lvJTasIHAUPNet
        '
        Me.lvJTasIHAUPNet.Text = "AUP Net"
        Me.lvJTasIHAUPNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.lvJTasIHAUPNet.Width = 70
        '
        'lvJTasIHJobID
        '
        Me.lvJTasIHJobID.Text = "JobID"
        Me.lvJTasIHJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvJTasIHJobID.Width = 0
        '
        'lvJTasIHSubJobID
        '
        Me.lvJTasIHSubJobID.Text = "SubJobID"
        Me.lvJTasIHSubJobID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.lvJTasIHSubJobID.Width = 0
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(916, 497)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(147, 46)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Exit to Main Menu"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(311, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Detail Activity Review/Invoicing"
        '
        'txtJTasJobID
        '
        Me.txtJTasJobID.Location = New System.Drawing.Point(344, 17)
        Me.txtJTasJobID.Name = "txtJTasJobID"
        Me.txtJTasJobID.ReadOnly = True
        Me.txtJTasJobID.Size = New System.Drawing.Size(103, 22)
        Me.txtJTasJobID.TabIndex = 3
        '
        'txtJTasJobDesc
        '
        Me.txtJTasJobDesc.Location = New System.Drawing.Point(560, 17)
        Me.txtJTasJobDesc.Name = "txtJTasJobDesc"
        Me.txtJTasJobDesc.ReadOnly = True
        Me.txtJTasJobDesc.Size = New System.Drawing.Size(174, 22)
        Me.txtJTasJobDesc.TabIndex = 4
        '
        'txtJTasSubID
        '
        Me.txtJTasSubID.Location = New System.Drawing.Point(455, 17)
        Me.txtJTasSubID.Name = "txtJTasSubID"
        Me.txtJTasSubID.ReadOnly = True
        Me.txtJTasSubID.Size = New System.Drawing.Size(97, 22)
        Me.txtJTasSubID.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(751, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Pricing Method"
        '
        'txtJTasPricing
        '
        Me.txtJTasPricing.Location = New System.Drawing.Point(859, 17)
        Me.txtJTasPricing.Name = "txtJTasPricing"
        Me.txtJTasPricing.ReadOnly = True
        Me.txtJTasPricing.Size = New System.Drawing.Size(114, 22)
        Me.txtJTasPricing.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox1.Controls.Add(Me.cboxJTasDeact)
        Me.GroupBox1.Controls.Add(Me.mtxtJTasCutOffDate)
        Me.GroupBox1.Controls.Add(Me.mtxtJTasInvDate)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(840, 299)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(289, 134)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Invoicing"
        '
        'cboxJTasDeact
        '
        Me.cboxJTasDeact.AutoSize = True
        Me.cboxJTasDeact.Location = New System.Drawing.Point(25, 100)
        Me.cboxJTasDeact.Name = "cboxJTasDeact"
        Me.cboxJTasDeact.Size = New System.Drawing.Size(253, 22)
        Me.cboxJTasDeact.TabIndex = 25
        Me.cboxJTasDeact.Text = "Deactivate Job After Invoicing"
        Me.cboxJTasDeact.UseVisualStyleBackColor = True
        '
        'mtxtJTasCutOffDate
        '
        Me.mtxtJTasCutOffDate.CausesValidation = False
        Me.mtxtJTasCutOffDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTasCutOffDate.Location = New System.Drawing.Point(168, 61)
        Me.mtxtJTasCutOffDate.Mask = "00/00/0000"
        Me.mtxtJTasCutOffDate.Name = "mtxtJTasCutOffDate"
        Me.mtxtJTasCutOffDate.Size = New System.Drawing.Size(102, 24)
        Me.mtxtJTasCutOffDate.TabIndex = 14
        Me.mtxtJTasCutOffDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.mtxtJTasCutOffDate.ValidatingType = GetType(Date)
        '
        'mtxtJTasInvDate
        '
        Me.mtxtJTasInvDate.CausesValidation = False
        Me.mtxtJTasInvDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtJTasInvDate.Location = New System.Drawing.Point(168, 26)
        Me.mtxtJTasInvDate.Mask = "00/00/0000"
        Me.mtxtJTasInvDate.Name = "mtxtJTasInvDate"
        Me.mtxtJTasInvDate.Size = New System.Drawing.Size(102, 24)
        Me.mtxtJTasInvDate.TabIndex = 13
        Me.mtxtJTasInvDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(22, 66)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(132, 18)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "Activity Cutoff Date"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 29)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 18)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Invoice Date"
        '
        'BtnJTasInvoice
        '
        Me.BtnJTasInvoice.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnJTasInvoice.Location = New System.Drawing.Point(869, 447)
        Me.BtnJTasInvoice.Name = "BtnJTasInvoice"
        Me.BtnJTasInvoice.Size = New System.Drawing.Size(239, 40)
        Me.BtnJTasInvoice.TabIndex = 15
        Me.BtnJTasInvoice.Text = "Produce and Email Invoice"
        Me.BtnJTasInvoice.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GroupBox3.Controls.Add(Me.txtJTasASaupSel)
        Me.GroupBox3.Controls.Add(Me.txtJTasASaupUnb)
        Me.GroupBox3.Controls.Add(Me.Label28)
        Me.GroupBox3.Controls.Add(Me.txtJTasASTlSel)
        Me.GroupBox3.Controls.Add(Me.txtJTasASTlUnb)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txtJTasASSubSel)
        Me.GroupBox3.Controls.Add(Me.txtJTasASMatSel)
        Me.GroupBox3.Controls.Add(Me.txtJTasASLabSel)
        Me.GroupBox3.Controls.Add(Me.txtJTasASLabUnb)
        Me.GroupBox3.Controls.Add(Me.txtJTasASMatUnb)
        Me.GroupBox3.Controls.Add(Me.txtJTasASSubUnb)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(840, 79)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(287, 214)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Activity Summary"
        '
        'txtJTasASaupSel
        '
        Me.txtJTasASaupSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASaupSel.Location = New System.Drawing.Point(194, 153)
        Me.txtJTasASaupSel.Name = "txtJTasASaupSel"
        Me.txtJTasASaupSel.ReadOnly = True
        Me.txtJTasASaupSel.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASaupSel.TabIndex = 17
        Me.txtJTasASaupSel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASaupUnb
        '
        Me.txtJTasASaupUnb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASaupUnb.Location = New System.Drawing.Point(105, 153)
        Me.txtJTasASaupUnb.Name = "txtJTasASaupUnb"
        Me.txtJTasASaupUnb.ReadOnly = True
        Me.txtJTasASaupUnb.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASaupUnb.TabIndex = 16
        Me.txtJTasASaupUnb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(10, 156)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(38, 16)
        Me.Label28.TabIndex = 15
        Me.Label28.Text = "AUP"
        '
        'txtJTasASTlSel
        '
        Me.txtJTasASTlSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASTlSel.Location = New System.Drawing.Point(194, 182)
        Me.txtJTasASTlSel.Name = "txtJTasASTlSel"
        Me.txtJTasASTlSel.ReadOnly = True
        Me.txtJTasASTlSel.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASTlSel.TabIndex = 14
        Me.txtJTasASTlSel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASTlUnb
        '
        Me.txtJTasASTlUnb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASTlUnb.Location = New System.Drawing.Point(105, 182)
        Me.txtJTasASTlUnb.Name = "txtJTasASTlUnb"
        Me.txtJTasASTlUnb.ReadOnly = True
        Me.txtJTasASTlUnb.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASTlUnb.TabIndex = 13
        Me.txtJTasASTlUnb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 185)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 16)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Total"
        '
        'txtJTasASSubSel
        '
        Me.txtJTasASSubSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASSubSel.Location = New System.Drawing.Point(193, 126)
        Me.txtJTasASSubSel.Name = "txtJTasASSubSel"
        Me.txtJTasASSubSel.ReadOnly = True
        Me.txtJTasASSubSel.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASSubSel.TabIndex = 11
        Me.txtJTasASSubSel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASMatSel
        '
        Me.txtJTasASMatSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASMatSel.Location = New System.Drawing.Point(193, 99)
        Me.txtJTasASMatSel.Name = "txtJTasASMatSel"
        Me.txtJTasASMatSel.ReadOnly = True
        Me.txtJTasASMatSel.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASMatSel.TabIndex = 10
        Me.txtJTasASMatSel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASLabSel
        '
        Me.txtJTasASLabSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASLabSel.Location = New System.Drawing.Point(193, 71)
        Me.txtJTasASLabSel.Name = "txtJTasASLabSel"
        Me.txtJTasASLabSel.ReadOnly = True
        Me.txtJTasASLabSel.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASLabSel.TabIndex = 9
        Me.txtJTasASLabSel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASLabUnb
        '
        Me.txtJTasASLabUnb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASLabUnb.Location = New System.Drawing.Point(104, 73)
        Me.txtJTasASLabUnb.Name = "txtJTasASLabUnb"
        Me.txtJTasASLabUnb.ReadOnly = True
        Me.txtJTasASLabUnb.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASLabUnb.TabIndex = 8
        Me.txtJTasASLabUnb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASMatUnb
        '
        Me.txtJTasASMatUnb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASMatUnb.Location = New System.Drawing.Point(104, 99)
        Me.txtJTasASMatUnb.Name = "txtJTasASMatUnb"
        Me.txtJTasASMatUnb.ReadOnly = True
        Me.txtJTasASMatUnb.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASMatUnb.TabIndex = 7
        Me.txtJTasASMatUnb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtJTasASSubUnb
        '
        Me.txtJTasASSubUnb.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJTasASSubUnb.Location = New System.Drawing.Point(104, 126)
        Me.txtJTasASSubUnb.Name = "txtJTasASSubUnb"
        Me.txtJTasASSubUnb.ReadOnly = True
        Me.txtJTasASSubUnb.Size = New System.Drawing.Size(83, 22)
        Me.txtJTasASSubUnb.TabIndex = 6
        Me.txtJTasASSubUnb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(9, 104)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Material"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(9, 130)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Subcontract"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(113, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 32)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Total" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unbilled"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(184, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 32)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Selected for" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Invoicing"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Labor"
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(19, 469)
        Me.TextBox18.Multiline = True
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(679, 62)
        Me.TextBox18.TabIndex = 23
        Me.TextBox18.Text = resources.GetString("TextBox18.Text")
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'btnJTasLineItemCorr
        '
        Me.btnJTasLineItemCorr.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJTasLineItemCorr.Location = New System.Drawing.Point(710, 467)
        Me.btnJTasLineItemCorr.Name = "btnJTasLineItemCorr"
        Me.btnJTasLineItemCorr.Size = New System.Drawing.Size(115, 59)
        Me.btnJTasLineItemCorr.TabIndex = 24
        Me.btnJTasLineItemCorr.Text = "Line Item" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Correction/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " Review"
        Me.btnJTasLineItemCorr.UseVisualStyleBackColor = True
        '
        'JTas
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1139, 555)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJTasLineItemCorr)
        Me.Controls.Add(Me.BtnJTasInvoice)
        Me.Controls.Add(Me.TextBox18)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtJTasPricing)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtJTasSubID)
        Me.Controls.Add(Me.txtJTasJobDesc)
        Me.Controls.Add(Me.txtJTasJobID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.tabJTas)
        Me.Name = "JTas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Job Information Manager - Activity Summary"
        Me.TopMost = True
        Me.tabJTas.ResumeLayout(False)
        Me.tabLabor.ResumeLayout(False)
        Me.tabLabor.PerformLayout()
        Me.tabMaterial.ResumeLayout(False)
        Me.tabMaterial.PerformLayout()
        Me.tabSubContrctr.ResumeLayout(False)
        Me.tabSubContrctr.PerformLayout()
        Me.tabAUP.ResumeLayout(False)
        Me.tabAUP.PerformLayout()
        Me.tabJobPandL.ResumeLayout(False)
        Me.tabJobPandL.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tabJTas As TabControl
    Friend WithEvents tabLabor As TabPage
    Friend WithEvents tabMaterial As TabPage
    Friend WithEvents tabSubContrctr As TabPage
    Friend WithEvents btnCancel As Button
    Friend WithEvents JTaslvLab As ListView
    Friend WithEvents JTaslvSub As ListView
    Friend WithEvents tabAUP As TabPage
    Friend WithEvents tabJobPandL As TabPage
    Friend WithEvents lvLabDate As ColumnHeader
    Friend WithEvents lvLabDescrpt As ColumnHeader
    Friend WithEvents lvLabCat As ColumnHeader
    Friend WithEvents lvHours As ColumnHeader
    Friend WithEvents lvCharge As ColumnHeader
    Friend WithEvents lvWorker As ColumnHeader
    Friend WithEvents lvMargin As ColumnHeader
    Friend WithEvents lvSubDate As ColumnHeader
    Friend WithEvents lvSubSupplier As ColumnHeader
    Friend WithEvents lvSubSupDesc As ColumnHeader
    Friend WithEvents lvSubSupInv As ColumnHeader
    Friend WithEvents lvSubCustChg As ColumnHeader
    Friend WithEvents lvSubInvNum As ColumnHeader
    Friend WithEvents lvSubMrgn As ColumnHeader
    Friend WithEvents JTaslvMat As ListView
    Friend WithEvents lvMatDate As ColumnHeader
    Friend WithEvents lvMatSupplier As ColumnHeader
    Friend WithEvents lvMatDescrpt As ColumnHeader
    Friend WithEvents lvMatSupCst As ColumnHeader
    Friend WithEvents lvMatTBInvd As ColumnHeader
    Friend WithEvents lvMatInvNum As ColumnHeader
    Friend WithEvents lvMatMrgn As ColumnHeader
    Friend WithEvents Label1 As Label
    Friend WithEvents txtJTasJobID As TextBox
    Friend WithEvents txtJTasJobDesc As TextBox
    Friend WithEvents txtJTasSubID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJTasPricing As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtJTasASTlSel As TextBox
    Friend WithEvents txtJTasASTlUnb As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtJTasASSubSel As TextBox
    Friend WithEvents txtJTasASMatSel As TextBox
    Friend WithEvents txtJTasASLabSel As TextBox
    Friend WithEvents txtJTasASLabUnb As TextBox
    Friend WithEvents txtJTasASMatUnb As TextBox
    Friend WithEvents txtJTasASSubUnb As TextBox
    Friend WithEvents txtJtasLabTlDol As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtJTasLabMrgnDol As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtJTasLabHrs As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtJTasMatCstInvDol As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtJTasMatMrgnDol As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtJTasMatCostDol As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtJTasSubCstInvDol As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtJTasSubMrgnDol As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtJTasSubCstDol As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents BtnJTasInvoice As Button
    Friend WithEvents mtxtJTasCutOffDate As MaskedTextBox
    Friend WithEvents mtxtJTasInvDate As MaskedTextBox
    Friend WithEvents TextBox18 As TextBox
    Friend WithEvents lvJTasInvHist As ListView
    Friend WithEvents JTasIHInvDate As ColumnHeader
    Friend WithEvents Label20 As Label
    Friend WithEvents lvJTasIHInvNum As ColumnHeader
    Friend WithEvents lvJTasIHLab As ColumnHeader
    Friend WithEvents lvJTasIHMat As ColumnHeader
    Friend WithEvents lvJTasIHSub As ColumnHeader
    Friend WithEvents lvJTasIHInvTl As ColumnHeader
    Friend WithEvents lvJTasIHEstMrgn As ColumnHeader
    Friend WithEvents lvJTasIHAUPInv As ColumnHeader
    Friend WithEvents lvJTasIHAUPNet As ColumnHeader
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtJTasIHInvTl As TextBox
    Friend WithEvents txtJTasIHEstMrgnTl As TextBox
    Friend WithEvents txtJTasIHAUPInvTl As TextBox
    Friend WithEvents txtJTasIHSubTl As TextBox
    Friend WithEvents txtJTasIHMatTl As TextBox
    Friend WithEvents txtJTasIHLabTl As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents JTaslvAUP As ListView
    Friend WithEvents lvAUPDate As ColumnHeader
    Friend WithEvents lvAUPDesc As ColumnHeader
    Friend WithEvents lvAUPAmount As ColumnHeader
    Friend WithEvents txtJTasAUPInvDol As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents lvAUPFinalInv As ColumnHeader
    Friend WithEvents lvLabJobID As ColumnHeader
    Friend WithEvents lvLabSubJobID As ColumnHeader
    Friend WithEvents lvMatJobID As ColumnHeader
    Friend WithEvents lvMatSubJobID As ColumnHeader
    Friend WithEvents lvJobID As ColumnHeader
    Friend WithEvents lvSubJobID As ColumnHeader
    Friend WithEvents lvAUPJobID As ColumnHeader
    Friend WithEvents lvAUPSubJobID As ColumnHeader
    Friend WithEvents txtJTasASaupSel As TextBox
    Friend WithEvents txtJTasASaupUnb As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents lvJTasIHJobID As ColumnHeader
    Friend WithEvents lvJTasIHSubJobID As ColumnHeader
    Friend WithEvents btnJTasLineItemCorr As Button
    Friend WithEvents cboxJTasDeact As CheckBox
End Class
