﻿Imports System.IO

''' <summary>
''' Class to handle PDF invoices and data from suppliers. Data will be edited and augmented.
''' It will then be transferred to NLC and also recorded to support historical vendor functions.
''' </summary>
Public Class JTedi
    '

    Public JTediTopInvNum As Integer = 0
    Public JTediNumofInv As Integer = 0
    Dim JTediFileName1 As String = Nothing
    Dim JTediFileName2 As String = Nothing
    Dim JTediFileName3 As String = Nothing
    '
    ' Keep track of the files that have been opened in webbrowser.
    Dim OpenedFiles1 As New List(Of WebBrowser)
    Dim OpenedFiles2 As New List(Of WebBrowser)
    Dim OpenedFiles3 As New List(Of WebBrowser)
    '
    ''' <summary>
    ''' Entry into EDI. Called from JTnlcM. Will be changed when code is functionally installed.
    ''' </summary>
    Public Sub JTediDispPDF()
        JTediTopInvNum = 0
        JTediFillInvImages()
    End Sub
    ' ----------------------------------------------------------------------------
    ' This array holds a list of files in ther directory. Leaving it filled may
    ' be causing deletes not to work. I am putting 'destroy' code in so that it is
    ' not left with data. 8/4/2020
    ' ----------------------------------------------------------------------------
    Dim JTediPDFFiles As New List(Of String)
    ''' <summary>
    ''' Develops list of files in EDIInputFiles folder. It then prepares JTedi panel for display.
    ''' </summary>
    Public Sub JTediFillInvImages()
        ' -------------------------------------------------------------------------------
        ' code assembles a list of *.pdf files
        ' -------------------------------------------------------------------------------
        JTediPDFFiles.Clear()
        '
        Dim di As New IO.DirectoryInfo(Path.Combine(JTVarMaint.JTvmFilePath, "EDIInputFiles\"))
        Dim aryFi As IO.FileInfo() = di.GetFiles("*.pdf")
        Dim fi As IO.FileInfo

        For Each fi In aryFi
            JTediPDFFiles.Add(fi.Name)
        Next
        '
        di = Nothing
        aryFi = Nothing
        fi = Nothing

        '
        Dim tmpWorkingFile As String = Nothing '
        '
        JTediNumofInv = JTediPDFFiles.Count
        '
        JTedidisplay()    ' set visible status for screen buttons
        '
        Dim tmpFileName As String = Nothing
        '
        Try
            If JTediTopInvNum < JTediPDFFiles.Count Then
                tmpFileName = Path.Combine(JTVarMaint.JTvmFilePath, "EDIInputFiles/", JTediPDFFiles(JTediTopInvNum))
                LabPages1.Text = "Pages - " & GetPDFPageCount(tmpFileName)
                JTediFileName1 = tmpFileName
                '
                If My.Computer.FileSystem.FileExists(tmpFileName) = False Then
                    MsgBox("Can't find file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~67")
                End If
                ' ================================================
                Try
                    JTediWorkFileCreate(tmpFileName, tmpWorkingFile)

                    WebBrowser1.Navigate(tmpWorkingFile)
                    '
                    OpenedFiles1.Add(WebBrowser1)
                Catch
                    MsgBox("WebBrowser can't process file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~79")
                End Try
                gboxInv1Btns.Visible = True
                gboxInv1PDF.Visible = True
            End If
            '
            If JTediTopInvNum + 1 < JTediPDFFiles.Count Then
                tmpFileName = Path.Combine(JTVarMaint.JTvmFilePath, "EDIInputFiles/", JTediPDFFiles(JTediTopInvNum + 1))
                '      tmpFileName = JTVarMaint.JTvmFilePath & "EDIInputFiles/" & JTediPDFFiles(JTediTopInvNum + 1)
                LabPages2.Text = "Pages - " & GetPDFPageCount(tmpFileName)
                JTediFileName2 = tmpFileName
                '
                If My.Computer.FileSystem.FileExists(tmpFileName) = False Then
                    MsgBox("Can't find file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~93")
                End If
                ' 
                Try
                    JTediWorkFileCreate(tmpFileName, tmpWorkingFile)
                    WebBrowser2.Navigate(tmpFileName)
                    '    WebBrowser2.Navigate(tmpWorkingFile)
                    OpenedFiles2.Add(WebBrowser2)
                Catch
                    MsgBox("WebBrowser can't process file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~104")
                End Try
                '
                gboxInv2Btns.Visible = True
                gboxInv2PDF.Visible = True
            End If
            '
            If JTediTopInvNum + 2 < JTediPDFFiles.Count Then
                tmpFileName = Path.Combine(JTVarMaint.JTvmFilePath, "EDIInputFiles/", JTediPDFFiles(JTediTopInvNum + 2))
                LabPages3.Text = "Pages - " & GetPDFPageCount(tmpFileName)
                JTediFileName3 = tmpFileName

                If My.Computer.FileSystem.FileExists(tmpFileName) = False Then
                    MsgBox("Can't find file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~119")
                End If
                ' 
                Try
                    JTediWorkFileCreate(tmpFileName, tmpWorkingFile)

                    WebBrowser3.Navigate(tmpWorkingFile)
                    ' & "#toolbar = 0&statusbar = 0&messages = 0&navpane = 0&page=1&view=FitH")
                    OpenedFiles3.Add(WebBrowser3)
                Catch
                    MsgBox("WebBrowser can't process file." & vbCrLf &
                           tmpFileName & vbCrLf &
                           "--- JTedi~131")
                End Try
                '
                gboxInv3Btns.Visible = True
                gboxInv3PDF.Visible = True
            End If
            '
            '
        Catch
            MsgBox("Error found in AxAcroPDF logic --- JTedi~140" & vbCrLf &
                   "File Name - - - >" & JTediPDFFiles(JTediTopInvNum + 2) & "<")
        End Try
        '
        JTstart.JTstartMenuOff()
        Me.MdiParent = JTstart
        Me.Show()
        '
    End Sub
    ''' <summary>
    ''' Close JTedi panel and return to the JTnlcM (nlc function menu).
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTediClose_Click(sender As Object, e As EventArgs) Handles btnJTediClose.Click
        ReleaseOpenPDFs()
        Me.Close()
        JTnlcM.JTnlcMinit()
    End Sub
    ''' <summary>
    ''' Handles back buttom on JTedi panel to rotate the PDF file display backwards.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTediBack_Click(sender As Object, e As EventArgs) Handles btnJTediBack.Click
        If JTediTopInvNum <> 0 Then
            JTediTopInvNum -= 1
        End If
        JTstart.JTediInvDispCount = JTediTopInvNum
        ReleaseOpenPDFs()
        Me.Close()
        JTstart.EDINavigationSub()
        '
    End Sub
    ''' <summary>
    ''' Handles back buttom on JTedi panel to rotate the PDF file display forward.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnJTediForward_Click(sender As Object, e As EventArgs) Handles btnJTediForward.Click
        If JTediTopInvNum < JTediNumofInv Then
            JTediTopInvNum += 1
            JTstart.JTediInvDispCount = JTediTopInvNum
            ReleaseOpenPDFs()
            Me.Close()
            JTstart.EDINavigationSub()
            '
        Else
            MsgBox("Last invoice is already being displayed.")
            End If

        End Sub
    Private Function JTedidisplay() As String
        gboxInv1Btns.Visible = False
        gboxInv1PDF.Visible = False
        gboxInv2Btns.Visible = False
        gboxInv2PDF.Visible = False
        gboxInv3Btns.Visible = False
        gboxInv3PDF.Visible = False
        Return ""
    End Function
    Private Sub btnProcess1_Click(sender As Object, e As EventArgs) Handles btnProcess1.Click
        '       WebBrowser1.Navigate("about:blank")
        '      WebBrowser1.Dispose()
        '     WebBrowser2.Navigate("about:blank")
        '    WebBrowser2.Dispose()
        '   WebBrowser3.Navigate("about:blank")
        '  WebBrowser3.Dispose()

        ReleaseOpenPDFs()
        JTnlcE.JtnlcEInit("EDI", JTediPDFFiles(JTediTopInvNum))
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnProcess2_Click(sender As Object, e As EventArgs) Handles btnProcess2.Click
        '       WebBrowser1.Navigate("about:blank")
        '      WebBrowser1.Dispose()
        '     WebBrowser2.Navigate("about:blank")
        '    WebBrowser2.Dispose()
        '   WebBrowser3.Navigate("about:blank")
        '  WebBrowser3.Dispose()
        ReleaseOpenPDFs()
        JTnlcE.JtnlcEInit("EDI", JTediPDFFiles(JTediTopInvNum + 1))
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnProcess3_Click(sender As Object, e As EventArgs) Handles btnProcess3.Click
        '       WebBrowser1.Navigate("about:blank")
        '      WebBrowser1.Dispose()
        '     WebBrowser2.Navigate("about:blank")
        '    WebBrowser2.Dispose()
        '   WebBrowser3.Navigate("about:blank")
        '  WebBrowser3.Dispose()
        ReleaseOpenPDFs()
        JTnlcE.JtnlcEInit("EDI", JTediPDFFiles(JTediTopInvNum + 2))
        Me.Close()
        Me.Dispose()
    End Sub
    ''' <summary>
    ''' Function be run at system startup. It will pass the PDF files in EDIInputFiles/.
    ''' If file are found with multiple pages, they will be split in preparation for
    ''' JTedi processing. IF an invoice is truly a multipage invoice, the operator can recombine it 
    ''' as part of JTnlcE edi functions.
    ''' </summary>
    ''' <returns></returns>
    Public Function JTediSplitPDFs() As Integer
        Dim tmpMultiPageFileCount As Integer = 0

        Dim fileEntries As String() = Directory.GetFiles(JTVarMaint.JTvmFilePath & "EDIInputFiles\", "*.pdf")
        Dim tmpPrefixLen As Integer = Len(JTVarMaint.JTvmFilePath & "EDIInputFiles\")
        ' Process the list of .pdf files found in the directory. '
        Dim fileName As String

        For Each fileName In fileEntries
            If GetPDFPageCount(fileName) > 1 Then
                tmpMultiPageFileCount += 1
                PDFSplitAndSave(fileName, JTVarMaint.JTvmFilePath & "EDIInputFiles/")
                '
                Me.Close()
                ' -------------------------------------------------------------------------------------
                ' Put code here to delete source file, etc.
                ' -------------------------------------------------------------------------------------
                If System.IO.File.Exists(fileName) = True Then
                    System.IO.File.Delete(fileName)
                Else
                    MsgBox("Source file in split routines not available for deletion." & vbCrLf &
                               fileName & vbCrLf & "JTedi~238")
                End If
            End If
        Next
        Erase fileEntries
        fileName = Nothing
        '
        Return tmpMultiPageFileCount
    End Function
    ''' <summary>
    ''' Logic to delete PDF file from disk without processing. Top File.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDelete1_Click(sender As Object, e As EventArgs) Handles btnDelete1.Click
        WebBrowser1.DocumentText = ""
        WebBrowser1.Navigate("about:blank")
        WebBrowser1.Navigate("about:blank")
        WebBrowser1.Dispose()
        '
        JTstart.JTediFileTBDeleted = JTediFileName1
        Me.Close()
        JTstart.EDINavigationSub()
    End Sub
    ''' <summary>
    ''' Logic to delete PDF file from disk without processing. Middle File.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDelete2_Click(sender As Object, e As EventArgs) Handles btnDelete2.Click
        WebBrowser2.DocumentText = ""
        WebBrowser2.Navigate("about:blank")
        WebBrowser2.Navigate("about:blank")
        WebBrowser2.Dispose()
        '
        JTstart.JTediFileTBDeleted = JTediFileName2
        Me.Close()
        JTstart.EDINavigationSub()
    End Sub
    ''' <summary>
    ''' Logic to delete PDF file from disk without processing. Bottom File.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDelete3_Click(sender As Object, e As EventArgs) Handles btnDelete3.Click
        WebBrowser3.DocumentText = ""
        WebBrowser3.Navigate("about:blank")
        WebBrowser3.Navigate("about:blank")

        WebBrowser3.Dispose()
        '
        JTstart.JTediFileTBDeleted = JTediFileName3
        Me.Close()
        '
        JTstart.EDINavigationSub()
    End Sub
    ''' <summary>
    '''  Not used anymore. Logic to delete indivudual file moved to JTstart
    '''  to avoid delete failure because of file still in use.
    ''' </summary>
    ''' <param name="tmpFileTBDel"></param>
    ''' <returns></returns>
    Private Function JTediDelSingleFile(tmpFileTBDel) As Boolean
        Dim tmpDelLoop As Boolean = False
        If My.Computer.FileSystem.FileExists(tmpFileTBDel) Then
            ReleaseOpenPDFs()
            '
            Do While tmpDelLoop <> True
                Try
                    My.Computer.FileSystem.DeleteFile(tmpFileTBDel)
                    tmpDelLoop = True
                Catch
                    MsgBox("Delete Failed - JTedi~334" & vbCrLf &
                           tmpFileTBDel)
                    tmpDelLoop = False
                End Try
            Loop
        End If
        JTstart.JTediInvDispCount = 0
        Return tmpDelLoop
    End Function

    Private Sub BtnSplit1_Click(sender As Object, e As EventArgs) Handles BtnSplit1.Click
        If GetPDFPageCount(JTediFileName1) > 1 Then
            PDFSplitAndSave(JTediFileName1, JTVarMaint.JTvmFilePath & "EDIInputFiles/")
            ReleaseOpenPDFs()
            Me.Close()
            ' -------------------------------------------------------------------------------------
            ' Put code here to delete source file, etc.
            ' -------------------------------------------------------------------------------------
            If System.IO.File.Exists(JTediFileName1) = True Then
                System.IO.File.Delete(JTediFileName1)
            Else
                MsgBox("Source file in split routines not available for deletion." & vbCrLf &
                           JTediFileName1 & vbCrLf & "JTedi~258")
            End If
            JTnlcM.JTnlcMBounceBack()
        End If
    End Sub

    Private Sub BtnSplit2_Click(sender As Object, e As EventArgs) Handles BtnSplit2.Click
        If GetPDFPageCount(JTediFileName2) > 1 Then
            PDFSplitAndSave(JTediFileName2, JTVarMaint.JTvmFilePath & "EDIInputFiles/")
            ReleaseOpenPDFs()
            Me.Close()
            ' -------------------------------------------------------------------------------------
            ' Put code here to delete source file, etc.
            ' -------------------------------------------------------------------------------------
            If System.IO.File.Exists(JTediFileName2) = True Then
                System.IO.File.Delete(JTediFileName2)
            Else
                MsgBox("Source file in split routines not available for deletion." & vbCrLf &
                           JTediFileName2 & vbCrLf & "JTedi~258")
            End If
            JTnlcM.JTnlcMBounceBack()
        End If
    End Sub

    Private Sub BtnSplit3_Click(sender As Object, e As EventArgs) Handles BtnSplit3.Click
        If GetPDFPageCount(JTediFileName3) > 1 Then
            PDFSplitAndSave(JTediFileName3, JTVarMaint.JTvmFilePath & "EDIInputFiles/")
            ReleaseOpenPDFs()
            Me.Close()
            ' -------------------------------------------------------------------------------------
            ' Put code here to delete source file, etc.
            ' -------------------------------------------------------------------------------------
            If System.IO.File.Exists(JTediFileName3) = True Then
                System.IO.File.Delete(JTediFileName3)
            Else
                MsgBox("Source file in split routines not available for deletion." & vbCrLf &
                           JTediFileName3 & vbCrLf & "JTedi~258")
            End If
            JTnlcM.JTnlcMBounceBack()
        End If
    End Sub

    Private Function ReleaseOpenPDFs() As String
        ' ----------------------------------------------------------------------------------
        ' Clearing open files from browser --- hopefully fixed deleting - Open File problem.
        ' ----------------------------------------------------------------------------------
        '     MsgBox("Open Files Counts - JTedi~436" & vbCrLf &
        '           "OpenedFiles1 - " & OpenedFiles1.Count & vbCrLf &
        '          "OpenedFiles2 - " & OpenedFiles2.Count & vbCrLf &
        '         "OpenedFiles3 - " & OpenedFiles3.Count)
        '
        ' Tried code below to clear pdf file lock ... didn't work ... 8/7/2020
        '       WebBrowser1.DocumentText = ""

        WebBrowser1.Navigate("about:blank")
        '        Do Until WebBrowser1.ReadyState = WebBrowserReadyState.Complete
        '       Application.DoEvents()
        '      System.Threading.Thread.Sleep(100)
        '     Loop
        '
        '       WebBrowser2.DocumentText = ""
        WebBrowser2.Navigate("about:blank")
        '    Do Until WebBrowser2.ReadyState = WebBrowserReadyState.Complete
        '        Application.DoEvents()
        '        System.Threading.Thread.Sleep(100)
        '    Loop
        '
        '    WebBrowser3.Hide()
        '      WebBrowser3.DocumentText = ""
        WebBrowser3.Navigate("about:blank")
        '    Do Until WebBrowser3.ReadyState = WebBrowserReadyState.Complete
        '        Application.DoEvents()
        '        System.Threading.Thread.Sleep(100)
        '    Loop
        '
        '
        WebBrowser1.Dispose()
        WebBrowser2.Dispose()
        WebBrowser3.Dispose()
        '
        If OpenedFiles1.Count > 0 Then
            For Each tmpWebBrowser As WebBrowser In OpenedFiles1
                tmpWebBrowser.Dispose()
            Next
            OpenedFiles1.Clear()
        End If
        If OpenedFiles2.Count > 0 Then
            For Each tmpWebBrowser As WebBrowser In OpenedFiles2
                tmpWebBrowser.Dispose()
            Next
            OpenedFiles2.Clear()
        End If
        If OpenedFiles3.Count > 0 Then
            For Each tmpWebBrowser As WebBrowser In OpenedFiles3
                tmpWebBrowser.Dispose()
            Next
            OpenedFiles3.Clear()
        End If
        Return ""
    End Function
    ''' <summary>
    ''' Function to create working copy that will be displayed in browser. This logic
    ''' is being used so the program can avoid locked status on the original PDF
    ''' file.
    ''' </summary>
    ''' <param name="tmpFileName">original PDf file name and path</param>
    ''' <param name="tmpWorkingFile2">path and name of newly created file</param>
    ''' <returns></returns>
    Public Function JTediWorkFileCreate(ByRef tmpFileName As String, ByRef tmpWorkingFile2 As String) As Boolean
        ' ----------------------------------------------------------------------------------
        ' Code placed here to just copy the incoming name to the outbound name
        ' Function will do nothing. Old code commented out.
        ' ----------------------------------------------------------------------------------
        Dim tmpWorkingFile As String = tmpFileName
        '      Dim tmpWorkingFile As String = "file" & ":///" & tmpFileName.Replace("\", "/")
        tmpWorkingFile2 = CreateWorkingFile(tmpWorkingFile)

        '

        Return True
    End Function
End Class