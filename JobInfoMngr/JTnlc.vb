﻿Imports System.IO
Imports com.itextpdf.text.pdf
Imports iTextSharp.text
Imports iTextSharp.text.pdf
''' <summary>
''' JTnlc class handles data management and the review panel (JTnlc) for
''' non-labor-cost. It works in conjuntion with a menuing panel (JTnlcM)
''' as well as several other panels for manual and EDI entry and control
''' of transactions.
''' </summary>
Public Class JTnlc
    ' ========================================================================
    ' Field I think I want to add.  2/3/2018
    '
    ' Dim Taxable as boolean -------------- Not Added --- Have to think about how it would be used
    '
    ' ========================================================================
    '
    Public Structure JTnlcStructure
        Dim EntryDate As String
        Dim InvDate As String
        Dim YrWkNum As String
        Dim JobID As String
        Dim SubID As String
        Dim MorS As String          'M, S, IS, IH, ADJ, NJR
        Dim Descrpt As String
        Dim Supplier As String
        Dim SupInvNum As String
        Dim SupCrossRefNum As String     ' added 2/21/2020 --- Used to document receiver papwerwork to invoice
        Dim SupInvImageFile As String    ' added 2/21/2020 --- PDF file name of invoice image
        Dim SupCost As String
        Dim MrgnPC As String
        Dim TBInvd As String
        Dim CustInvDate As String
        Dim CustInvNumber As String
        Dim ReconcileDate As String
        Dim ReconcileDoc As String     ' Name of Reconcile report
        Dim AcctExtDate As String
        Dim AcctExtDoc As String     ' Name of File/Report when extracted to Accounting
        Dim PymtSwitch As String ' R - Reconciled, M - Manual, E - Export to Acct, NA - used on IS, IH and ADJ
        ' Fields added related to reconciliation and NJR
        Dim NJREAcct As String           'added 4/8/2019
        Dim viAcctNum As String          'added 4/8/2019
        Dim viVendName As String         'added 4/8/2019
        Dim viStreet As String           'added 4/8/2019
        Dim viCityState As String        'added 4/8/2019
        Dim viTerms As String            'added 4/8/2019
        Dim viDueDate As String          'added 4/8/2019
        Dim viDiscpercent As String      'added 4/8/2019
        Dim viDiscAmt As String          'added 4/8/2019
        Dim viRcrdEntrySrce As String    'added 4/16/2019  null most of the time; on NJR entries - NLC or REC for where it was entered
    End Structure
    '
    Public slJTnlc As New SortedList
    Dim JTnlcEntrySeq As Integer = 10000
    Dim JTnlcKeyCollPos As Integer = 0
    Dim JTnlcLastFocusKey As String = Nothing
    Dim JTnlcChgsMadeToImageFile As Boolean = False

    ''' <summary>
    ''' This sub prepares the JTnlc panel for display. The heading information is loaded 
    ''' in this sub. The lv is loaded through a call to JTnlcLoadLV.
    ''' </summary>
    Public Sub JTnlcInit()
        mtxtJTnlcWEDate.Text = String.Format("{0:MM/dd/yyyy}", Date.Today)
        '
        Dim JTnlcJobList As New AutoCompleteStringCollection
        Dim tmpJobSub = 0
        Dim tmpJobName As String = ""
        Dim tmpSubName As String = ""
        Dim tmpDeactDate As String = ""
        Dim tmpJobType As String = ""
        Dim tmpCstMeth As String = ""
        ' ----------------------------------------------------------
        ' Code to populate Job Name (Customer Name) list for new
        ' item entry on NLC screen.
        ' ----------------------------------------------------------
        Dim tmpLstName As String = Nothing
        Do While JTjim.JTjimReadFunc(tmpJobSub, tmpJobName, tmpSubName, tmpDeactDate, tmpJobType, tmpCstMeth) = "DATA"
            If tmpDeactDate = "" And
                tmpJobType <> "STMT" Then     'not sure STMT applies to this record ... perhaps put in by mistake thinking this was VI.
                If tmpJobName <> tmpLstName Then
                    JTnlcJobList.Add(tmpJobName)
                    tmpLstName = tmpJobName
                End If
            End If
            tmpJobSub += 1
        Loop
        '
        '
        JTnlcLoadlv()
        ' ------------------------------------------------------------------
        ' Code to reposition data on nlc lv ...
        ' logic to be used when nlc is queued from suspended reconciliation.
        ' ------------------------------------------------------------------
        If JTrec.JTrecSuspendedFlag = True Then
            If JTrec.JTrecSuspndDate <> Nothing Then
                MsgBox("Checking to see this code is ever used..." & vbCrLf &
                       "Code used for JTrec suspend to position screen" & vbCrLf &
                       "It should no longer be needed - - - JTnlc~95 - JTnlnInit")
                Dim tmpSub As Integer = 0
                Do While tmpSub < lvJTnlcData.Items.Count
                    If lvJTnlcData.Items(tmpSub).Text = JTrec.JTrecSuspndDate Then
                        If lvJTnlcData.Items(tmpSub).SubItems(6).Text = JTrec.JTrecSuspndInvNum Then
                            If lvJTnlcData.Items(tmpSub).SubItems(7).Text = JTrec.JtrecSuspndAmount Then
                                tmpSub += 13
                                '
                                ' check to see if we have enough item to scroll selected item to top.
                                If tmpSub >= lvJTnlcData.Items.Count Then
                                    tmpSub = lvJTnlcData.Items.Count - 1
                                End If
                                lvJTnlcData.EnsureVisible(tmpSub)
                                JTrec.JTrecSuspndDate = Nothing
                                JTrec.JTrecSuspndInvNum = Nothing
                                JTrec.JtrecSuspndAmount = Nothing
                                Exit Do
                            End If
                        End If
                    End If
                    tmpSub += 1
                Loop
            End If
        End If
        ' ---------------------------------------------------------
        ' End of positioning code
        ' ---------------------------------------------------------
        '

        cboxJTnlcDataFocus.Text = JTnlcLastFocusKey
        Me.Show()
        '
        If cboxJTnlcDataFocus.Text <> "" Then
            JTnlcSetFocus()
        End If
    End Sub
    ''' <summary>
    ''' This sub handles when the Cancel key is hit on the panel. NLC (as well as 
    ''' timekeeping) is session related. The operator can make lots of entries, but 
    ''' until they are accepted (updated), they are not permanent. If the cancel key 
    ''' is activated, this sub performs logic that brings the NLC data back to where 
    ''' it was at the start of the session.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnExittoMenu_Click(sender As Object, e As EventArgs) Handles btnExittoMenu.Click
        Me.Hide()
        JTnlcM.JTnlcMinit()
    End Sub
    ''' <summary>This sub loads the LV on the JTnlc panel. The source for the data is slJTnlc.</summary>
    ''' 
    Public Function JTnlcLoadlv() As String
        ' ---------------------------------------------------------------------
        ' code to take info from the sortedlist and prepare it for presentation
        ' on the JTnlc lv. It also updates the unbilled activity totals.
        ' ---------------------------------------------------------------------
        '
        ' Logic to make sure ListView is empty.
        '
        lvJTnlcData.Items.Clear()


        '
        ' clear total accumulators
        '
        Dim tmpTlMatCst As Double = 0
        Dim tmpTlMatMar As Double = 0
        Dim tmpTlMatInv As Double = 0
        Dim tmpTlSubCst As Double = 0
        Dim tmpTlSubMar As Double = 0
        Dim tmpTlSubInv As Double = 0
        Dim tmpTlTlCst As Double = 0
        Dim tmpTlTlMar As Double = 0
        Dim tmpTlTlInv As Double = 0

        '
        ' Constants for listview
        '
        Dim tmpSub = 0
        Dim itmBuild(13) As String
        Dim lvLine As ListViewItem
        '
        ' Get a collection of the keys. 
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            ' check to see if item is a NJR item generated in Jtrec. Do not display is true.
            '
            Dim tmpSkipThisLine As Boolean = False
            If slJTnlc(k).viRcrdEntrySrce = "REC" Then
                tmpSkipThisLine = True
            End If
            If slJTnlc(k).JobID = "ZZZSTMT" Then
                tmpSkipThisLine = True
            End If
            If tmpSkipThisLine = False Then

                ' move items to listview

                itmBuild(0) = slJTnlc(k).InvDate
                itmBuild(1) = slJTnlc(k).JobID
                itmBuild(2) = slJTnlc(k).SubID
                itmBuild(3) = slJTnlc(k).MorS
                itmBuild(4) = slJTnlc(k).Descrpt
                itmBuild(5) = slJTnlc(k).Supplier
                itmBuild(6) = slJTnlc(k).SupInvNum
                itmBuild(7) = String.Format("{0:0.00}", Val(slJTnlc(k).SupCost))
                itmBuild(8) = slJTnlc(k).MrgnPC
                If slJTnlc(k).MorS = "NJR" Then
                    itmBuild(9) = ""
                    ' ------------------------------------------------------------------------------------
                    ' Plug account name in SubID column on NJR items.
                    ' ------------------------------------------------------------------------------------
                    itmBuild(2) = slJTnlc(k).NJREAcct
                Else
                    itmBuild(9) = String.Format("{0:0.00}", Val(slJTnlc(k).TBInvd))
                End If
                itmBuild(10) = slJTnlc(k).PymtSwitch
                itmBuild(11) = slJTnlc(k).CustInvDate
                itmBuild(12) = k
                '
                lvLine = New ListViewItem(itmBuild)
                ' Code to change color of invoiced entries to Blue.
                lvLine.ForeColor = Color.Black
                If slJTnlc(k).CustInvDate <> "" Then
                    lvLine.ForeColor = Color.Blue
                End If
                lvJTnlcData.Items.Add(lvLine)
                '
                ' Accumulate total of all entries entered
                '
                If slJTnlc(k).CustInvDate = "" Then
                    If slJTnlc(k).MorS <> "NJR" Then
                        If slJTnlc(k).MorS = "M" Or
                           slJTnlc(k).MorS = "IS" Or
                           slJTnlc(k).MorS = "ADJ" Or
                           slJTnlc(k).MorS = "IH" Then
                            tmpTlMatCst += Val(slJTnlc(k).SupCost)
                            tmpTlMatMar += Val(slJTnlc(k).TBInvd) - Val(slJTnlc(k).SupCost)
                            tmpTlMatInv += Val(slJTnlc(k).TBInvd)
                        Else
                            tmpTlSubCst += Val(slJTnlc(k).SupCost)
                            tmpTlSubMar += Val(slJTnlc(k).TBInvd) - Val(slJTnlc(k).SupCost)
                            tmpTlSubInv += Val(slJTnlc(k).TBInvd)
                        End If
                        tmpTlTlCst += Val(slJTnlc(k).SupCost)
                        tmpTlTlMar += Val(slJTnlc(k).TBInvd) - Val(slJTnlc(k).SupCost)
                        tmpTlTlInv += Val(slJTnlc(k).TBInvd)
                    End If
                End If

                '
                ' set subscript for next item
                '
                tmpSub += 1
            End If
        Next k
        ' ---------------------------------------------------------
        ' Move accumulated unbilled activity totals to jtnlc panel.
        ' ---------------------------------------------------------
        txtJTnlcMatCst.Text = String.Format("{0:0.00}", tmpTlMatCst)
        txtJTnlcMatMrgn.Text = String.Format("{0:0.00}", tmpTlMatMar)
        txtJTnlcMatInvd.Text = String.Format("{0:0.00}", tmpTlMatInv)
        '
        txtJTnlcSubCst.Text = String.Format("{0:0.00}", tmpTlSubCst)
        txtJTnlcSubMrgn.Text = String.Format("{0:0.00}", tmpTlSubMar)
        txtJTnlcSubInvd.Text = String.Format("{0:0.00}", tmpTlSubInv)
        '
        txtJTnlcTlCst.Text = String.Format("{0:0.00}", tmpTlTlCst)
        txtJTnlcTlMrgn.Text = String.Format("{0:0.00}", tmpTlTlMar)
        txtJTnlcTlInvd.Text = String.Format("{0:0.00}", tmpTlTlInv)
        '
        Select Case JTVarMaint.JTgvNLCSeq
            Case "J"
                rbtnJtnlcSeqJob.Checked = True
            Case "D"
                rbtnJTnlcSeqDate.Checked = True
            Case "S"
                rbtnJTnlcseqSpplr.Checked = True
        End Select
        '  
        Return ""
    End Function
    ''' <summary>
    '''   <para>
    ''' This sub take the data from the sl for NLC and write the records to disk. 
    ''' This is called at various time and acts like a 'quiet point' to assure the data 
    ''' is stored in case of system malfunction. It is called at the end of an NLC entry 
    ''' session as well as several other places in the system.
    ''' </para>
    '''   <para>This sub also has logic to create archival backups of NLC data (there is 
    '''         matching logic for TK data in its flush routines) based upon global variables 
    '''         related to timing. </para>
    ''' </summary>
    ''' <param name="typeUpdt">
    ''' This field tell the function the source of the 'call'. If the type is 'suspend', 
    ''' that means it has been called because a reconciliation session has been suspended. 
    ''' The records are saved so that that session can be restarted later.
    ''' </param>
    Public Sub JTnlcArrayFlush(typeUpdt As String)
        ' ---------------------------------------------------------------------------
        ' This sub reformats nlc data into XML sentences and write the data to a file.
        ' ---------------------------------------------------------------------------
        '

        Dim fileRecord As String = ""
        '
        Dim tmpDoingArchive As Boolean = False
        Dim tmpRecordsToArchive As Decimal = 0
        Dim tmpSkipControl As String = ""
        '
        If JTmmArchiveDayCalc(JTVarMaint.LstNLCPurge) = "DoArchive" And typeUpdt <> "SUSPEND" Then
            tmpDoingArchive = True
        End If
        '
        Dim tmpDate As Date
        Dim tmpEntryDate As Date
        Dim tmpJulianToday As String
        tmpJulianToday = Format$(Date.Today, "yyyy") & Date.Today.DayOfYear.ToString.PadLeft(3, "0")
        Dim tmpJulianTran As String = ""
        Dim tmpJulianInv As String = ""
        '
        Dim tmpLoopControl As String = "Normal"
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTNonLaborCosts.dat"
        ' -------------------------------------------------------------------------
        ' Recovery Logic --- at Flush start
        ' -------------------------------------------------------------------------
        If File.Exists(filePath) Then
            My.Computer.FileSystem.RenameFile(filePath, "JTNonLaborCosts.Bkup")
        End If
        ' -------------------------------------------------------------------------
        ' Review image file names and update where necessary
        UpdateImageFileNames()
        '
        '

        Do While tmpLoopControl <> ""
            Try
                Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
                '
                ' Get a collection of the keys. 
                Dim key As ICollection = slJTnlc.Keys
                Dim k As String
                '
                For Each k In key
                    '
                    ' ---------------------------------------------------------------------------------------------
                    ' Code to check to see if a record is being saved without mandatory information.
                    ' --- message displayed and record dropped.
                    ' ---------------------------------------------------------------------------------------------
                    If slJTnlc(k).EntryDate = "" Or slJTnlc(k).InvDate = "" Then
                        MsgBox("Trying to save a bad record --- think about what you've been doing" & vbCrLf &
                               "This record will be dropped. Call Ed and tell him it happened. - JTnlc~734")
                        Continue For
                    End If
                    '
                    Dim RecDone As Boolean = False
                    ' 
                    Dim RecDate As String = slJTnlc(k).ReconcileDate
                    '
                    Select Case slJTnlc(k).PymtSwitch
                        Case "NA"
                            RecDone = True
                        Case "M"
                            If slJTnlc(k).AcctExtDate <> "" Then
                                RecDone = True
                            End If
                        Case "E"
                            If slJTnlc(k).AcctExtDate <> "" Then
                                RecDone = True
                            End If
                        Case "R"
                            If slJTnlc(k).ReconcileDate <> "" Then
                                If RecDate = "PENDING" Then
                                    RecDate = ""
                                Else
                                    RecDone = True
                                End If
                            End If
                            '
                    End Select
                    tmpDate = CDate(slJTnlc(k).InvDate)
                    tmpEntryDate = CDate(slJTnlc(k).InvDate)
                    tmpJulianTran = Format$(tmpDate, "yyyy") & tmpDate.DayOfYear.ToString.PadLeft(3, "0")
                    '
                    If slJTnlc(k).CustInvDate = "" Then
                        If slJTnlc(k).MorS = "NJR" Then
                            ' ---------------------------------------------------------------------------------------
                            ' Plug entry date as invoice date for "NJR" items to facilitate archive of these records.
                            ' ---------------------------------------------------------------------------------------
                            tmpJulianInv = Format$(tmpEntryDate, "yyyy") & tmpEntryDate.DayOfYear.ToString.PadLeft(3, "0")
                        Else
                            tmpJulianInv = tmpJulianToday    'plug today's date so item will not be archived.
                        End If
                    Else
                        tmpDate = CDate(slJTnlc(k).CustInvDate)
                        tmpJulianInv = Format$(tmpDate, "yyyy") & tmpDate.DayOfYear.ToString.PadLeft(3, "0")
                    End If
                    '
                    '
                    Dim tmpWorkDays As Decimal = 0
                    Dim tmpInvDays As Decimal = 0
                    If tmpJulianToday.Substring(0, 4) > tmpJulianTran.Substring(0, 4) Then
                        tmpWorkDays = Val(tmpJulianToday.Substring(4, 3)) + (365 - Val(tmpJulianTran.Substring(4, 3)))
                    Else
                        tmpWorkDays = Val(tmpJulianToday) - Val(tmpJulianTran)
                    End If
                    '
                    If tmpJulianToday.Substring(0, 4) > tmpJulianInv.Substring(0, 4) Then
                        tmpInvDays = Val(tmpJulianToday.Substring(4, 3)) + (365 - Val(tmpJulianInv.Substring(4, 3)))
                    Else
                        tmpInvDays = Val(tmpJulianToday) - Val(tmpJulianInv)
                    End If
                    Dim tmpArchiveEligible As Boolean = True
                    ' -----------------------------------------------------------------------------
                    ' The following code tests dates and activities related to a nlc line item to
                    ' determine if it is elligible for archival. On "NJR" items, the invoice date 
                    ' is plugged to the entry date. On not invoiced items, the invoice date is plugged
                    ' today's date so it won't be archived.
                    ' -----------------------------------------------------------------------------
                    If tmpWorkDays < JTVarMaint.NLCMinLife Or
                        tmpInvDays < JTVarMaint.MinDaysAfterInv Or
                        RecDone = False Then
                        tmpArchiveEligible = False
                    End If
                    '
                    ' -------------------------------------------------------------------------------
                    ' Following code handles statements (ZZZSTMT) related to archiving. A statement 
                    ' will be archived on the next purge following its accounting extract.
                    ' -------------------------------------------------------------------------------
                    If slJTnlc(k).JobID = "ZZZSTMT" Then
                        If slJTnlc(k).AcctExtDate <> "" Then
                            tmpArchiveEligible = True
                        End If
                    End If
                    '
                    tmpSkipControl = "Save"
                    '
                    If tmpDoingArchive = True Then
                        If tmpArchiveEligible = True Then
                            If tmpLoopControl = "Normal" Then
                                tmpRecordsToArchive += 1
                                tmpSkipControl = "Drop"
                            End If
                        Else
                            If tmpLoopControl = "Archive" Then
                                tmpSkipControl = "Drop"
                            End If
                        End If
                    End If
                    '
                    If tmpSkipControl = "Save" Then
                        fileRecord = "<nlcRecord>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<EntryDate>" & slJTnlc(k).EntryDate & "</EntryDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<InvDate>" & slJTnlc(k).InvDate & "</InvDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If slJTnlc(k).YrWkNum <> "" Then
                            fileRecord = "<YrWkNum>" & slJTnlc(k).YrWkNum & "</YrWkNum>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).JobID <> "" Then
                            fileRecord = "<JobID>" & slJTnlc(k).JobID & "</JobID>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).SubID <> "" Then
                            fileRecord = "<SubID>" & slJTnlc(k).SubID & "</SubID>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "<EntryDate>" & slJTnlc(k).EntryDate & "</EntryDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<InvDate>" & slJTnlc(k).InvDate & "</InvDate>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If slJTnlc(k).YrWkNum <> "" Then
                            fileRecord = "<YrWkNum>" & slJTnlc(k).YrWkNum & "</YrWkNum>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "<MorS>" & slJTnlc(k).MorS & "</MorS>"
                        writeFile.WriteLine(fileRecord)
                        '
                        ' ---------------------------------------------------------------------------
                        ' Code to break records longer than 75 character into multiple short records.
                        ' ---------------------------------------------------------------------------
                        If slJTnlc(k).Descrpt <> "" Then
                            Dim tmpFieldData As String = slJTnlc(k).Descrpt.replace(vbLf, "~!")
                            Dim tmpFieldData2 As String = tmpFieldData.Replace(vbCr, "~@")
                            tmpFieldData = tmpFieldData2
                            tmpFieldData2 = ""
                            Dim tmpLength As Integer = tmpFieldData.Length
                            Dim tmpFieldPos As Integer = 0
                            Do While tmpLength > 60
                                fileRecord = "<Descrpt>" & tmpFieldData.Substring(tmpFieldPos, 60) & "</Descrpt>"
                                writeFile.WriteLine(fileRecord)
                                tmpLength -= 60
                                tmpFieldPos += 60
                            Loop
                            If tmpLength > 0 Then
                                fileRecord = "<Descrpt>" & tmpFieldData.Substring(tmpFieldPos, tmpLength) & "</Descrpt>"
                                writeFile.WriteLine(fileRecord)
                            End If
                        End If
                        '
                        fileRecord = "<Supplier>" & slJTnlc(k).Supplier & "</Supplier>"
                        writeFile.WriteLine(fileRecord)
                        '
                        fileRecord = "<SupInvNum>" & slJTnlc(k).SupInvNum & "</SupInvNum>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If slJTnlc(k).SupCrossRefNum <> "" Then
                            fileRecord = "<SupCrossRefNum>" & slJTnlc(k).SupCrossRefNum & "</SupCrossRefNum>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).SupInvImageFile <> "" Then
                            fileRecord = "<SupInvImageFile>" & slJTnlc(k).SupInvImageFile & "</SupInvImageFile>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        fileRecord = "<SupCost>" & slJTnlc(k).SupCost & "</SupCost>"
                        writeFile.WriteLine(fileRecord)
                        '
                        If Val(slJTnlc(k).MrgnPC) <> 0 Then
                            fileRecord = "<MrgnPC>" & slJTnlc(k).MrgnPC & "</MrgnPC>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If Val(slJTnlc(k).TBInvd) <> 0 Then
                            fileRecord = "<TBInvd>" & slJTnlc(k).TBInvd & "</TBInvd>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).CustInvDate <> "" Then
                            fileRecord = "<CustInvDate>" & slJTnlc(k).CustInvDate & "</CustInvDate>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).CustInvDate <> "" Then
                            If slJTnlc(k).CustInvNumber <> "" Then
                                fileRecord = "<CustInvNumber>" & slJTnlc(k).CustInvNumber & "</CustInvNumber>"
                                writeFile.WriteLine(fileRecord)
                            End If
                        End If
                        '
                        If slJTnlc(k).ReconcileDate <> "" Then
                            fileRecord = "<ReconcileDate>" & slJTnlc(k).ReconcileDate & "</ReconcileDate>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).ReconcileDoc <> "" Then
                            fileRecord = "<ReconcileDoc>" & slJTnlc(k).ReconcileDoc & "</ReconcileDoc>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).AcctExtDate <> "" Then
                            fileRecord = "<AcctExtDate>" & slJTnlc(k).AcctExtDate & "</AcctExtDate>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).AcctExtDoc <> "" Then
                            fileRecord = "<AcctExtDoc>" & slJTnlc(k).AcctExtDoc & "</AcctExtDoc>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        If slJTnlc(k).PymtSwitch <> "" Then
                            fileRecord = "<PymtSwitch>" & slJTnlc(k).PymtSwitch & "</PymtSwitch>"
                        Else
                            If slJTnlc(k).MorS = "M" Or slJTnlc(k).MorS = "S" _
                                        Or slJTnlc(k).MorS = "NJR" Then
                                Select Case slJTnlc(k).ReconcileDate
                                    Case "QBExport"
                                        fileRecord = "<PymtSwitch>" & "E" & "</PymtSwitch>"
                                    Case "Manual"
                                        fileRecord = "<PymtSwitch>" & "M" & "</PymtSwitch>"
                                    Case Else
                                        fileRecord = "<PymtSwitch>" & "R" & "</PymtSwitch>"
                                End Select
                            Else
                                fileRecord = "<PymtSwitch>" & "NA" & "</PymtSwitch>"
                            End If
                        End If
                        writeFile.WriteLine(fileRecord)
                        If slJTnlc(k).NJREAcct <> "" Then
                            fileRecord = "<NJREAcct>" & slJTnlc(k).NJREAcct & "</NJREAcct>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viVendName <> "" Then
                            fileRecord = "<viVendName>" & slJTnlc(k).viVendName & "</viVendName>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viStreet <> "" Then
                            fileRecord = "<viStreet>" & slJTnlc(k).viStreet & "</viStreet>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viCityState <> "" Then
                            fileRecord = "<viCityState>" & slJTnlc(k).viCityState & "</viCityState>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viTerms <> "" Then
                            fileRecord = "<viTerms>" & slJTnlc(k).viTerms & "</viTerms>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        '
                        If slJTnlc(k).viDueDate <> "" Then
                            If slJTnlc(k).viDueDate <> "  /  /" Then
                                fileRecord = "<viDueDate>" & slJTnlc(k).viDueDate & "</viDueDate>"
                                writeFile.WriteLine(fileRecord)
                            End If
                        End If
                        If slJTnlc(k).viDiscpercent <> "" Then
                            fileRecord = "<viDiscpercent>" & slJTnlc(k).viDiscpercent & "</viDiscpercent>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viDiscAmt <> "" Then
                            fileRecord = "<viDiscAmt>" & slJTnlc(k).viDiscAmt & "</viDiscAmt>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        If slJTnlc(k).viRcrdEntrySrce = "" Then
                            If slJTnlc(k).MorS = "NJR" Then
                                fileRecord = "<viRcrdEntrySrce>" & "NLC" & "</viRcrdEntrySrce>"
                                writeFile.WriteLine(fileRecord)
                            End If
                        Else
                            fileRecord = "<viRcrdEntrySrce>" & slJTnlc(k).viRcrdEntrySrce & "</viRcrdEntrySrce>"
                            writeFile.WriteLine(fileRecord)
                        End If
                        '
                        '
                        fileRecord = "</nlcRecord>"
                        writeFile.WriteLine(fileRecord)
                        '
                    End If
                Next
                '
                '
                writeFile.Flush()
                writeFile.Close()
                writeFile = Nothing
            Catch ex As IOException
                MsgBox("Trouble with NLC Array FLush ~650/JTnlc" & vbCrLf & ex.ToString)
            End Try
            ' ------------------------------------------------------------------------
            ' Recovery logic --- after successful save
            ' ------------------------------------------------------------------------
            If File.Exists(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.Bkup") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.Bkup")
            End If
            ' ------------------------------------------------------------------------
            If tmpDoingArchive = True And
                tmpLoopControl = "Normal" And
                tmpRecordsToArchive <> 0 Then
                tmpLoopControl = "Archive"
                Dim tmpArchiveFileName As String = "JTNLCArchive" & tmpJulianToday & ".dat"
                Dim tmpTodayYear As String = Date.Today.Year
                Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "DataArchive\" & tmpTodayYear
                If Not System.IO.Directory.Exists(tmpFilePath) Then
                    System.IO.Directory.CreateDirectory(tmpFilePath)
                End If
                filePath = tmpFilePath & "\" & tmpArchiveFileName
                JTVarMaint.LstNLCPurge = tmpJulianToday
            Else
                tmpLoopControl = ""
            End If
            '
        Loop
        '
        ' Rebuild sljtnlc after archive routines have executed.
        '
        If tmpDoingArchive = True Then
            JTnlcArrayLoad()
        End If

    End Sub
    ''' <summary>
    ''' This function loads the sl for NLC items. It is used primarily by NLc function but 
    ''' is also called to vreate the dash board, while recording invoices and by the year-end 
    ''' routines. If an NLC entry session is aborted, this function is run to restore the sl 
    ''' to the state prior to the session that is being aborted.
    ''' </summary>
    Public Function JTnlcArrayLoad() As String
        ' -----------------------------------------------------------------------------
        ' This function reads the NonLaborCost XML data and rebuilds slJTnlc.
        ' -----------------------------------------------------------------------------
        '
        Dim tmpJIarray As Integer = 0
        Dim fileRecord As String = ""
        Dim tmpChar As String = ""
        Dim tmpLoc As Integer = 0
        Dim tmpLocKey As Integer = 0
        '
        Dim tmpSaveYrWk As String = ""
        Dim tmpSaveEmpID As String = ""
        '
        Dim JTnlcReadFileKey As String = ""
        Dim JTnlcReadFileData As String = ""
        JTnlcEntrySeq = 1000
        '
        Dim tmpJTnlc As New JTnlcStructure
        tmpJTnlc = Nothing
        slJTnlc.Clear()
        '
        Dim tmpRecordType As String = ""
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JTNonLaborCosts.dat"
        ' ------------------------------------------------------------------
        ' Recovery Logic --- at beginning of load
        ' ------------------------------------------------------------------
        If File.Exists(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.Bkup") Then
            MsgBox("JTNonLaborCosts --- Loaded from recovery file" & vbCrLf &
                   "An issue was identified where previous file save" & vbCrLf &
                   "did not complete successfully (NLC.017")
            If File.Exists(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.dat") Then
                My.Computer.FileSystem.DeleteFile(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.dat")
            End If
            My.Computer.FileSystem.RenameFile(JTVarMaint.JTvmFilePath & "JTNonLaborCosts.Bkup", "JTNonLaborCosts.dat")
        End If
        ' ------------------------------------------------------------------

        '
        Try
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                fileRecord = readFile.ReadLine()
                If fileRecord Is Nothing Then
                    Exit While
                Else
                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    tmpLoc = 1

                    Do While fileRecord.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    JTnlcReadFileKey = fileRecord.Substring(1, tmpLoc - 1)
                    If JTnlcReadFileKey = "nlcRecord" Or JTnlcReadFileKey = "/nlcRecord" Then
                        JTnlcReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While fileRecord.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        JTnlcReadFileData = fileRecord.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If
                    '
                    Select Case JTnlcReadFileKey
                        '
                        Case "EntryDate"
                            tmpJTnlc.EntryDate = JTnlcReadFileData
                        Case "InvDate"
                            tmpJTnlc.InvDate = JTnlcReadFileData
                        Case "YrWkNum"
                            tmpJTnlc.YrWkNum = JTnlcReadFileData
                        Case "JobID"
                            tmpJTnlc.JobID = JTnlcReadFileData
                        Case "SubID"
                            tmpJTnlc.SubID = JTnlcReadFileData
                        Case "MorS"
                            tmpJTnlc.MorS = JTnlcReadFileData
                        Case "Descrpt"
                            tmpJTnlc.Descrpt &= JTnlcReadFileData
                        Case "Supplier"
                            tmpJTnlc.Supplier = JTnlcReadFileData
                        Case "SupInvNum"
                            tmpJTnlc.SupInvNum = JTnlcReadFileData
                        Case "SupCrossRefNum"
                            tmpJTnlc.SupCrossRefNum = JTnlcReadFileData
                        Case "SupInvImageFile"
                            tmpJTnlc.SupInvImageFile = JTnlcReadFileData
                        Case "SupCost"
                            tmpJTnlc.SupCost = JTnlcReadFileData
                        Case "MrgnPC"
                            tmpJTnlc.MrgnPC = JTnlcReadFileData
                        Case "TBInvd"
                            tmpJTnlc.TBInvd = JTnlcReadFileData
                        Case "CustInvDate"
                            tmpJTnlc.CustInvDate = JTnlcReadFileData
                        Case "CustInvNumber"
                            tmpJTnlc.CustInvNumber = JTnlcReadFileData
                        Case "ReconcileDate"
                            tmpJTnlc.ReconcileDate = JTnlcReadFileData
                        Case "ReconcileDoc"
                            tmpJTnlc.ReconcileDoc = JTnlcReadFileData
                        Case "AcctExtDate"
                            tmpJTnlc.AcctExtDate = JTnlcReadFileData
                        Case "AcctExtDoc"
                            tmpJTnlc.AcctExtDoc = JTnlcReadFileData
                        Case "PymtSwitch"
                            tmpJTnlc.PymtSwitch = JTnlcReadFileData
                        Case "NJREAcct"
                            tmpJTnlc.NJREAcct = JTnlcReadFileData
                        Case "viAcctNum"
                            tmpJTnlc.viAcctNum = JTnlcReadFileData
                        Case "viVendName"
                            tmpJTnlc.viVendName = JTnlcReadFileData
                        Case "viStreet"
                            tmpJTnlc.viStreet = JTnlcReadFileData
                        Case "viCityState"
                            tmpJTnlc.viCityState = JTnlcReadFileData
                        Case "viTerms"
                            tmpJTnlc.viTerms = JTnlcReadFileData
                        Case "viDueDate"
                            If JTnlcReadFileData <> "" Then
                                tmpJTnlc.viDueDate = JTnlcReadFileData
                            End If
                        Case "viDiscpercent"
                            tmpJTnlc.viDiscpercent = JTnlcReadFileData
                        Case "viDiscAmt"
                            tmpJTnlc.viDiscAmt = JTnlcReadFileData
                        Case "viRcrdEntrySrce"
                            tmpJTnlc.viRcrdEntrySrce = JTnlcReadFileData
                            '
                        Case "/nlcRecord"
                            '
                            If tmpJTnlc.Descrpt <> "" Then
                                tmpJTnlc.Descrpt = tmpJTnlc.Descrpt.Replace("~!", vbLf)
                                tmpJTnlc.Descrpt = tmpJTnlc.Descrpt.Replace("~@", vbCr)
                            End If
                            '
                            '
                            If tmpJTnlc.InvDate = "" Then
                                MsgBox("Found a bad date ~867/JTnlc" & vbCrLf &
                                       tmpJTnlc.Supplier & " " & tmpJTnlc.SupInvNum)
                            End If
                            Dim tmpinvdate As Date = CDate(tmpJTnlc.InvDate)
                            '
                            '
                            ' =========================================================================
                            ' Call to build key here ============================================
                            ' =========================================================================
                            Dim tmpKey As String = JTnlcBldSLKey(tmpinvdate, tmpJTnlc.JobID, tmpJTnlc.SubID, tmpJTnlc.CustInvDate, tmpJTnlc.Supplier)
                            '
                            JTnlcAddSLItem(tmpKey, tmpJTnlc, "JTnlcArrayLoad - JTnlc~1170")
                            tmpJTnlc.Descrpt = ""
                            '
                            JTnlcEntrySeq -= 1
                            ' --------------------------------------
                            ' Update not invoiced amount totals
                            ' --------------------------------------
                            If tmpJTnlc.CustInvDate = "" Then
                                If tmpJTnlc.MorS = "M" Or tmpJTnlc.MorS = "IS" Or tmpJTnlc.MorS = "IH" Then
                                    JTMainMenu.JTMMNotInvAmts(tmpJTnlc.JobID, tmpJTnlc.SubID, 0, Val(tmpJTnlc.TBInvd), 0)
                                Else
                                    JTMainMenu.JTMMNotInvAmts(tmpJTnlc.JobID, tmpJTnlc.SubID, 0, 0, Val(tmpJTnlc.TBInvd))
                                End If
                            End If
                            ' ------------------------------------------------------------
                            ' clear fields that might not have a value in the next record.
                            ' ------------------------------------------------------------
                            tmpJTnlc = Nothing
                            '
                    End Select
                    '
                    '
                End If
            End While
            readFile.Close()
            readFile = Nothing
            '
            '
            '
        Catch ex As IOException
            MsgBox("Trouble with NLC array load ~875/JTnlc" & vbCrLf & ex.ToString)
        End Try
        '
        Return "Done"
    End Function
    ''' <summary>
    ''' This sub is executed when an item(line) is selected in the lvJTnlcData. 
    ''' The values from that line are transferred to the top-of-panel fields where 
    ''' editting can take place. The LV is refilled without the selected item.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    '''
    Private Sub lvJTnlcData_DoubleClick(sender As Object, e As EventArgs) Handles lvJTnlcData.DoubleClick
        ' -------------------------------------------------------------------------------
        ' line in lvJTtkdata selected. Sub will move it to top for re-edit. It deletes
        ' the entry in the lv and subtracts the entries amount out of the unbilled 
        ' activity totals - --it will be replaced with newly edited entry.
        ' -------------------------------------------------------------------------------
        '
        ' Get a collection of the keys. 
        '
        Dim tmpSelectedNLCKey As String = lvJTnlcData.SelectedItems(0).SubItems(12).Text
        '


        ' ------------------------------------------------------------------------------
        ' Code to pass control to JTnlcE for review/modification.
        ' ------------------------------------------------------------------------------
        JTnlcE.JtnlcEInit("REVMODNLC", tmpSelectedNLCKey)
        Me.Hide()
        Exit Sub

        '
        ' -------------------------------------------------------------
        ' Reduce not invoiced amount totals by amount of selected item.
        ' -------------------------------------------------------------

        If slJTnlc(tmpSelectedNLCKey).MorS = "M" Or
            slJTnlc(tmpSelectedNLCKey).MorS = "Is" Or
            slJTnlc(tmpSelectedNLCKey).MorS = "IH" Then
            JTMainMenu.JTMMNotInvAmts(slJTnlc(tmpSelectedNLCKey).JobID,
                                      slJTnlc(tmpSelectedNLCKey).SubID,
                                      0,
                                      Val(slJTnlc(tmpSelectedNLCKey).TBInvd) * -1,
                                      0)
        Else
            If slJTnlc(tmpSelectedNLCKey).MorS <> "NJR" Then
                JTMainMenu.JTMMNotInvAmts(slJTnlc(tmpSelectedNLCKey).JobID,
                                          slJTnlc(tmpSelectedNLCKey).SubID,
                                          0,
                                          0,
                                          Val(slJTnlc(tmpSelectedNLCKey).TBInvd) * -1)
            End If
        End If
        '

    End Sub
    Public Function JTNLCrecExt(ByRef tmpSLkey As String, ByRef tmpSLnlcItem As JTnlcStructure) As String

        Dim tmpSLKeySave As String = tmpSLkey
        Dim nlckey As ICollection = slJTnlc.Keys
        If tmpSLKeySave = "FIRST" Then
            JTnlcKeyCollPos = 0
            tmpSLKeySave = ""
        End If
        Do While JTnlcKeyCollPos < nlckey.Count
            If slJTnlc(nlckey(JTnlcKeyCollPos)).PymtSwitch = "R" Then
                If slJTnlc(nlckey(JTnlcKeyCollPos)).ReconcileDate = "" Or
                slJTnlc(nlckey(JTnlcKeyCollPos)).ReconcileDate = "PENDING" Then
                    tmpSLkey = nlckey(JTnlcKeyCollPos)
                    tmpSLnlcItem = slJTnlc.Item(nlckey(JTnlcKeyCollPos))
                    JTnlcKeyCollPos += 1
                    Return "DATA"
                    '
                End If
            End If
            JTnlcKeyCollPos += 1
        Loop

        Return "DONE"
    End Function
    Public Function JTNLCasExt(tmpJobID As String, tmpSubID As String, ByRef tmpSLkey As String, ByRef tmpSLnlcItem As JTnlcStructure) As String

        Dim tmpSLKeySave As String = tmpSLkey
        Dim nlckey As ICollection = slJTnlc.Keys
        If tmpSLKeySave = "FIRST" Then
            JTnlcKeyCollPos = 0
            tmpSLKeySave = ""
        End If
        Do While JTnlcKeyCollPos < nlckey.Count
            If tmpJobID = slJTnlc(nlckey(JTnlcKeyCollPos)).JobID _
                And (tmpSubID = slJTnlc(nlckey(JTnlcKeyCollPos)).SubID Or tmpSubID = "MSCOMB") _
                And slJTnlc(nlckey(JTnlcKeyCollPos)).CustInvDate = "" Then
                tmpSLkey = nlckey(JTnlcKeyCollPos)
                tmpSLnlcItem = slJTnlc.Item(nlckey(JTnlcKeyCollPos))
                JTnlcKeyCollPos += 1
                Return "DATA"
                '
            End If
            JTnlcKeyCollPos += 1
        Loop

        Return "DONE"
    End Function
    ''' <summary>
    ''' Function to handle updating IS/IH transactions into the slJTnlc data base.
    ''' </summary>
    ''' <param name="tmpJobID"></param>
    ''' <param name="tmpSubID"></param>
    ''' <param name="tmpEntryDate"></param>
    ''' <returns></returns>
    Public Function JTnlcISItemUpdt(tmpJobID As String, tmpSubID As String, tmpEntryDate As String) As String
        ' Function was named JTnlcBldSLKey ... I think I may have copied over original name.
        Dim tmpSL As New SortedList
        Dim tmpSlkey As Integer = 0

        Dim key As ICollection = JTis.slJTisData.Keys
        Dim k As String
        '
        For Each k In key
            If JTis.slJTisData(k).JTissUnitToJob <> "" Then
                tmpSL.Add(Str(tmpSlkey), JTis.slJTisData(k))
                tmpSlkey += 1
            End If
        Next
        Dim tmpslJTnlcKey As String = Nothing
        '
        Dim key1 As ICollection = tmpSL.Keys
        For Each k In key1
            Dim tmpArea As New JTnlcStructure
            tmpArea.Descrpt = tmpSL(k).JTissItem & "(" & tmpSL(k).jtissUnitToJob & " X " &
                tmpSL(k).JTissUnitCost & "/" & tmpSL(k).JTissSellUnit & ")"

            tmpArea.Supplier = tmpSL(k).JTissPrVerSrce
            tmpArea.SupInvNum = tmpSL(k).JTissPrVerInv
            Dim tmpUnitSold As Double = tmpSL(k).JTissUnitToJob
            Dim tmpUnitPrice As Double = tmpSL(k).JTissUnitCost
            Dim tmpCostAmt As Double = tmpUnitSold * tmpUnitPrice
            ' -------------------------------------------------------------------------------
            ' Code to set price and calculated cost for InHouse items
            ' -------------------------------------------------------------------------------
            If tmpSL(k).JTissPrVerSrce = "" Then
                tmpArea.TBInvd = String.Format("{0:0.00}", tmpCostAmt)
                tmpArea.SupCost = String.Format("{0:0.00}", tmpCostAmt / (1 + Val(JTVarMaint.JTvmINHOUSEMargin / 100)))
            Else
                tmpArea.MrgnPC = JTjim.JTjimMrgnPC(tmpJobID, tmpSubID, "M")  ' this line put in inc ase this is first entry.
                ' Regular IS items cost and selling price calculation
                tmpArea.SupCost = String.Format("{0:0.00}", tmpCostAmt)
                tmpArea.TBInvd = String.Format("{0:0.00}", (((Val(tmpArea.MrgnPC) / 100) * Val(tmpCostAmt)) +
                                               Val(tmpCostAmt)))
            End If
            '
            ' -------------------------------------------------------------------------------
            ' Code to update I.S. items into slJTnlc.
            ' -------------------------------------------------------------------------------
            '
            ' ------------------------------------------------------------
            ' Calculate last Saturdays date based on today's date and show
            ' on the panel as a suggestion.
            ' ------------------------------------------------------------
            Dim tmpDate As Date = Today
            Dim tmpDayofWeek As Integer = tmpDate.DayOfWeek
            Dim tmpLastSatDate As Date = tmpDate.AddDays((tmpDayofWeek + 1) * -1)
            ' -------------------------------------------------------------------------
            '
            Dim tmpDone As String = ""
            Dim tmpFileSeq As Integer = 10
            Dim tmpYrWkNum As String = ""
            Dim tmpInvDate As Date = CDate(tmpEntryDate)
            Dim tmpWeekNo As Integer = 0
            tmpWeekNo = (tmpInvDate.DayOfYear / 7) + 1
            If tmpWeekNo < 10 Then
                tmpYrWkNum = ((Str(tmpInvDate.Year)).Trim(" ") & "0" & (Str(tmpWeekNo)).Trim(" "))
            Else
                tmpYrWkNum = ((Str(tmpInvDate.Year)).Trim(" ") & (Str(tmpWeekNo)).Trim(" "))
            End If
            '
            tmpArea.EntryDate = String.Format("{0:MM/dd/yyyy}", tmpInvDate)   ' entry and invoice date the same on IS/IH
            tmpArea.InvDate = String.Format("{0:MM/dd/yyyy}", tmpInvDate)
            '
            tmpArea.YrWkNum = tmpYrWkNum
            tmpArea.JobID = tmpJobID
            tmpArea.SubID = tmpSubID

            ' Still need to figure this one out ------------------------------------
            If tmpSL(k).JTissCategory = "INHOUSE" Then
                tmpArea.MorS = "IH"
            Else
                tmpArea.MorS = "IS"
            End If
            ' ----------------------------------------------------------
            '
            tmpArea.NJREAcct = ""
            tmpArea.viDueDate = ""
            tmpArea.viDiscAmt = ""
            '
            tmpArea.PymtSwitch = "NA"
            '
            ' =========================================================================
            ' Call to build key here ============================================
            ' =========================================================================
            tmpslJTnlcKey = JTnlcBldSLKey(tmpInvDate, tmpArea.JobID, tmpArea.SubID, tmpArea.CustInvDate, "")
            '
            JTnlcAddSLItem(tmpslJTnlcKey, tmpArea, "JTnlcISItemUpdt - JTnlc~1555")
            JTnlcEntrySeq -= 1
            ' -------------------------------------------------------------------
            ' Call routine to update last activity date on the JIM panel/database
            ' -------------------------------------------------------------------
            JTjim.JTJimUpdtLstActivity(tmpArea.JobID, tmpArea.SubID, tmpArea.InvDate)
            '

        Next
        '
        Return "DONE"
    End Function
    ''' <summary>
    ''' Function to check for duplicate Supplier/invoice number entered.
    ''' </summary>
    ''' <param name="tmpSupplier"></param>
    ''' <param name="tmpInvNum"></param>
    ''' <returns>"UNIQUE" - No duplicate number found. "DUP" - Duplicate entry found.</returns>
    Public Function JTnlcDupInvNumChk(ByRef tmpSupplier As String, ByRef tmpInvNum As String) As String
        ' -------------------------------------------------------------------------------------
        ' This code checked for supplier/invoice number duplicates. It is a warning message.
        ' -------------------------------------------------------------------------------------
        Dim tmpStatus As String = "UNIQUE"
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            If tmpSupplier = slJTnlc(k).Supplier Then
                If tmpInvNum = slJTnlc(k).SupInvNum Then
                    tmpStatus = "DUP"
                    Exit For
                End If
            End If
        Next
        Return tmpStatus
    End Function
    Public Function JTnlcUnbilledNLC(ByRef tmpMat As Double, ByRef tmpSub As Double, ByRef tmpAdj As Double,
                                     ByRef PendRecCnt As Integer, ByRef PendRecAmount As Double,
                                     ByRef PendQBECnt As Integer, ByRef PendQBEAmount As Double,
                                     ByRef PendManCnt As Integer, ByRef PendManAmount As Double)
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            If slJTnlc(k).CustInvDate = "" Then
                If slJTnlc(k).MorS = "S" Then
                    tmpSub += Val(slJTnlc(k).TBInvd)
                End If
                If slJTnlc(k).MorS = "ADJ" Then
                    tmpAdj += Val(slJTnlc(k).TBInvd)
                End If

                If slJTnlc(k).MorS = "M" Or slJTnlc(k).MorS = "IS" Then
                    tmpMat += Val(slJTnlc(k).TBInvd)
                End If
            End If
            If slJTnlc(k).AcctExtDate = "" And (slJTnlc(k).MorS = "M" Or slJTnlc(k).MorS = "S") Then

                If slJTnlc(k).ReconcileDate = "" Then
                    PendRecCnt += 1
                    PendRecAmount += Val(slJTnlc(k).SupCost)
                End If
                If slJTnlc(k).PymtSwitch = "E" Then
                    PendQBECnt += 1
                    PendQBEAmount += Val(slJTnlc(k).SupCost)
                End If
                If slJTnlc(k).PymtSwitch = "M" Then
                    PendManCnt += 1
                    PendManAmount += Val(slJTnlc(k).SupCost)
                End If
            End If
        Next
        Return ""
    End Function
    Public Function JTnlcJobUnbilled(tmpJob As String, tmpSubJob As String, ByRef tmpLstDate As String) As Double
        Dim tmpUnbilled As Double = 0
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            If slJTnlc(k).JobID = tmpJob And slJTnlc(k).SubID = tmpSubJob And slJTnlc(k).CustInvDate = "" Then
                tmpUnbilled += Val(slJTnlc(k).TBInvd)
                If tmpLstDate = "" Then
                    tmpLstDate = slJTnlc(k).InvDate
                ElseIf CDate(tmpLstDate) < CDate(slJTnlc(k).InvDate) Then
                    tmpLstDate = slJTnlc(k).InvDate
                End If
            End If
        Next
        Return tmpUnbilled
    End Function

    Public Function JTnlcChkForNonRecActiveItems(tmpVendName As String) As Boolean
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            If tmpVendName = slJTnlc(k).Supplier And (slJTnlc(k).ReconcileDate = "QBExport" Or slJTnlc(k).ReconcileDate = "Manual") Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Function JTnlcMakeReconcilable(tmpVendName As String) As Boolean
        Dim tmpItem As New JTnlcStructure
        Dim tmpConverted = False
        Do While tmpConverted = False
            Dim key As ICollection = slJTnlc.Keys
            Dim k As String
            For Each k In key
                If tmpVendName = slJTnlc(k).Supplier And (slJTnlc(k).ReconcileDate = "QBExport" Or slJTnlc(k).ReconcileDate = "Manual") Then
                    tmpItem = slJTnlc(k)
                    tmpItem.ReconcileDate = ""
                    slJTnlc.Remove(k)
                    JTnlcAddSLItem(k, tmpItem, "JTnlcMakeReconcilable - JTnlc~1270")
                    tmpConverted = True
                    Exit For
                End If
            Next
            If tmpConverted = False Then
                Return True
            End If
            tmpConverted = False
        Loop
        Return True
    End Function
    Public Function JTnlcLstEntryDate() As String
        Dim tmpLstEntryDate As String = ""
        Dim tmpLstEntryDateJulian As String = ""
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String = ""
        For Each k In key
            Dim tmpLatestJulDate As String = Gregarian2Julian(CDate(slJTnlc(k).EntryDate))
            If tmpLatestJulDate > tmpLstEntryDateJulian Then
                tmpLstEntryDateJulian = tmpLatestJulDate
                tmpLstEntryDate = slJTnlc(k).EntryDate
            End If
        Next
        Return tmpLstEntryDate
    End Function
    Function JTnlcBldSLKey(tmpinvdate As Date, tmpJob As String, tmpSubjob As String, tmpInvDte As String, tmpSupplier As String) As String
        Dim tmpDateKeyComp As Double = tmpinvdate.DayOfYear
        Dim tmpDateKeyComp2 As String = (Str(tmpinvdate.Year)).Trim(" ") & String.Format("{0:000}", tmpDateKeyComp)
        Dim tmpInvdFlag As String = ""
        If tmpInvDte <> "" Then
            tmpInvdFlag = "1"
        Else
            tmpInvdFlag = "0"
        End If
        Dim tmpKeyPrefix As String = ""
        Select Case JTVarMaint.JTgvNLCSeq
            Case "J"
                tmpKeyPrefix = "j" & tmpJob & tmpSubjob & tmpInvdFlag
            Case "D"
                tmpKeyPrefix = "d" & tmpInvdFlag
            Case "S"
                tmpKeyPrefix = "s" & tmpSupplier & tmpJob & tmpSubjob & tmpInvdFlag    ' added Supplier sequence 2/21/2020
        End Select
        Dim tmpslKey As String = tmpKeyPrefix & Str(10000000 - Val(tmpDateKeyComp2)) & Str(JTnlcEntrySeq)
        Do While slJTnlc.ContainsKey(tmpslKey)
            JTnlcEntrySeq -= 1
            tmpslKey = tmpKeyPrefix & Str(10000000 - Val(tmpDateKeyComp2)) & Str(JTnlcEntrySeq)
        Loop
        Return tmpslKey
    End Function

    Private Sub rbtnJtnlcSeqJob_CheckedChanged(sender As Object, e As EventArgs) _
        Handles rbtnJtnlcSeqJob.CheckedChanged, rbtnJTnlcSeqDate.CheckedChanged, rbtnJTnlcseqSpplr.CheckedChanged
        ' ----------------------------------------------------------------------------------------
        ' Resequence SL and LV for NLC panel.
        ' ----------------------------------------------------------------------------------------
        JTnlcLastFocusKey = ""    'clear out value of value being focused (Date, Vendor, Jeb)
        Dim tmpCodeToFind As String = ""
        If rbtnJtnlcSeqJob.Checked = True Then
            JTVarMaint.JTgvNLCSeq = "J"
            tmpCodeToFind = "j"
        End If
        If rbtnJTnlcSeqDate.Checked = True Then
            JTVarMaint.JTgvNLCSeq = "D"
            tmpCodeToFind = "d"
        End If
        If rbtnJTnlcseqSpplr.Checked = True Then
            JTVarMaint.JTgvNLCSeq = "S"
            tmpCodeToFind = "s"
        End If
        Me.Hide()
        JTnlcReSeqDB(tmpCodeToFind)
        JTnlcLoadlv()
        JTnlcFillCbox()
        If cboxJTnlcDataFocus.Text <> "" Then
            JTnlcSetFocus()
        End If
        Me.Show()
    End Sub
    Public Function JTnlcReSeqDB(tmpCodeToFind As String)
        ' ============================================================
        ' Code to cycle thru lv to change keys
        ' ============================================================
        Dim FoundItem As Boolean = True
        Do While FoundItem = True
            Dim key As ICollection = slJTnlc.Keys
            Dim k As String = ""
            FoundItem = False
            For Each k In key
                If k.Substring(0, 1) <> tmpCodeToFind Then
                    Dim tmpItem As New JTnlcStructure
                    tmpItem = slJTnlc(k)
                    slJTnlc.Remove(k)
                    Dim tmpInvDate As Date = CDate(tmpItem.InvDate)
                    Dim tmpSLKey As String = JTnlcBldSLKey(tmpInvDate,
                                                           tmpItem.JobID,
                                                           tmpItem.SubID,
                                                           tmpItem.CustInvDate,
                                                           tmpItem.Supplier)
                    JTnlcAddSLItem(tmpSLKey, tmpItem, "rbtn_CheckedChanged - JTnlc~1836")
                    FoundItem = True
                    Exit For
                End If
            Next
        Loop
        Return ""
    End Function
    ''' <summary>
    ''' Function to make sure that NLC db is in job order. This is important
    ''' when an accounting extract is being executed.
    ''' </summary>
    ''' <returns></returns>
    Public Function JTnlcPutinJobSeq()
        If JTVarMaint.JTgvNLCSeq = "J" Then     ' Already in Job Sequence
            Return ""
        End If
        JTVarMaint.JTgvNLCSeq = "J"
        Dim tmpCodeToFind As String = "j"
        JTnlcReSeqDB(tmpCodeToFind)

        Return ""
    End Function
    ''' <summary>
    ''' Function to extract data for NLC items being exported to acctng. Logic
    ''' to export Manual series ("M") and export to acctng series ("E") based 
    ''' on switch settings (tmpExptMode).
    ''' Type of Transaction also controlled through tmpNLCType ...
    ''' "M" = Material, "S" = Services
    ''' </summary>
    ''' <param name="tmpRprtName"></param>
    ''' <param name="tmpNLCType"></param>
    ''' <param name="tmpExptMode"></param>
    ''' <param name="tmpCurrRow"></param>
    ''' <param name="tmpSLkey"></param>
    ''' <param name="tmpSLnlcItem"></param>
    ''' <returns></returns>
    Public Function JTasNLCExt(tmpRprtName As String, ByRef tmpNLCType As String,
                               ByRef tmpExptMode As String, ByRef tmpCurrRow As Integer,
                               ByRef tmpSLkey As String, ByRef tmpSLnlcItem As JTnlcStructure) As String
        '
        Dim nlckey As ICollection = slJTnlc.Keys
        Do While tmpCurrRow < nlckey.Count
            Dim tmpPymtSwitch As String = slJTnlc(nlckey(tmpCurrRow)).PymtSwitch
            Dim tmpnlcAcctExtDate As String = slJTnlc(nlckey(tmpCurrRow)).AcctExtDate

            If tmpRprtName = "IE" Then
                If tmpnlcAcctExtDate = "" Then
                    If tmpExptMode = slJTnlc(nlckey(tmpCurrRow)).PymtSwitch Then
                        If tmpNLCType = slJTnlc(nlckey(tmpCurrRow)).MorS Then
                            tmpSLkey = nlckey(tmpCurrRow)
                            tmpSLnlcItem = slJTnlc(nlckey(tmpCurrRow))
                            tmpCurrRow += 1
                            Return "DATA"
                        End If
                    End If
                End If
            End If
            '
            If tmpRprtName = "JR" Then
                If slJTnlc(nlckey(tmpCurrRow)).CustInvDate = "" Then
                    tmpSLkey = nlckey(tmpCurrRow)
                    tmpSLnlcItem = slJTnlc(nlckey(tmpCurrRow))
                    tmpCurrRow += 1
                    Return "DATA"
                End If
            End If
            tmpCurrRow += 1
        Loop
        Return "DONE"
    End Function
    Public Function JTnlcExptDateUpdt(tmpKey As String, tmpExptDate As String,
                                      tmpExptFileName As String) As String
        ' ---------------------------------------------------------------------
        ' Function to update Acct Expt Date for exported nlc items.
        ' ---------------------------------------------------------------------
        If slJTnlc.ContainsKey(tmpKey) Then
            Dim tmpItem As JTnlcStructure
            tmpItem = slJTnlc(tmpKey)
            slJTnlc.Remove(tmpKey)
            tmpItem.AcctExtDate = tmpExptDate
            tmpItem.AcctExtDoc = tmpExptFileName
            JTnlcAddSLItem(tmpKey, tmpItem, "JTnlcExptDateUpdt - JTnlc~1897")
        Else
            MsgBox("Error updating nlc acctng extract date --- JTnlc ~1599")
        End If
        Return ""
    End Function
    ''' <summary>
    ''' Function to count current number of NLC item pending accounting extract.
    ''' </summary>
    ''' <returns>Returns an integer representing # pending extract.</returns>
    Public Function JTnlcQBExtractchk() As Integer
        Dim tmpCount As Integer = 0
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String = ""
        For Each k In key
            If slJTnlc(k).PymtSwitch = "M" Or slJTnlc(k).PymtSwitch = "E" Then
                If slJTnlc(k).MorS = "M" Or
                   slJTnlc(k).MorS = "S" Or
                   slJTnlc(k).MorS = "NJR" Then
                    If slJTnlc(k).AcctExtDate = "" Then
                        tmpCount += 1
                        ' Research related to count for QB Extract ---
                        ' Can be deleted whenever --- 4/9/2024
                        '              MsgBox(slJTnlc(k).InvDate & vbCrLf &
                        '                     slJTnlc(k).JobID & vbCrLf &
                        '                     slJTnlc(k).SubID & vbCrLf &
                        '                     slJTnlc(k).MorS & vbCrLf &
                        '                     slJTnlc(k).Supplier & vbCrLf &
                        '                     slJTnlc(k).SupInvNum)
                    End If
                End If
            End If
        Next
        Return tmpCount
    End Function
    Public Function JTnlcJobHasActivity(tmpJobName As String, tmpSubname As String) As Boolean
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String = ""
        For Each k In key
            If slJTnlc(k).JobID = tmpJobName Then
                If slJTnlc(k).SubID = tmpSubname Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function
    Public Function JTnlcDelSLRecord(tmpRecKey As String) As String
        Dim tmpDelResult As String = "ERROR"
        If slJTnlc.ContainsKey(tmpRecKey) = True Then
            slJTnlc.Remove(tmpRecKey)
            tmpDelResult = "DELETED"
        Else
            MsgBox("JTnlc record deletion failed - Record not found - JTnlc~2157" & vbCrLf &
                   "Record key --->" & tmpRecKey & "<" & vbCrLf & vbCrLf & "CONTACT ED")
        End If
        Return tmpDelResult
    End Function
    Public Function JTnlcAEExtract(ByRef tmpSub As Integer, ExtRecType As String, ExtMode As String,
                                   ByRef tmpNLCKey As String, ByRef tmpNCLRecord As JTnlcStructure) As String
        If tmpSub = 0 Then                ' If start of file, resequence to make sure sequence is predictable.

        End If
        Dim tmpCount As Integer = 0
        Dim key As ICollection = slJTnlc.Keys
        Do While tmpSub < key.Count
            If (ExtRecType = "STMT" And
            slJTnlc(key(tmpSub)).JobID = "ZZZSTMT" And
            slJTnlc(key(tmpSub)).AcctExtDate = "") Then
                tmpNLCKey = key(tmpSub)
                tmpNCLRecord = slJTnlc(key(tmpSub))
                tmpSub += 1
                Return "DATA"
            End If

            If slJTnlc(key(tmpSub)).PymtSwitch = "R" Or slJTnlc(key(tmpSub)).PymtSwitch = "NA" Or
               slJTnlc(key(tmpSub)).AcctExtDate <> "" Then
                tmpSub += 1
                Continue Do
            End If
            If ExtRecType = slJTnlc(key(tmpSub)).MorS Then
                If slJTnlc(key(tmpSub)).PymtSwitch = ExtMode Then
                    tmpNLCKey = key(tmpSub)
                    tmpNCLRecord = slJTnlc(key(tmpSub))
                    tmpSub += 1
                    Return "DATA"
                End If
            End If


            tmpSub += 1
        Loop
        Return "END"
    End Function
    ''' <summary>
    ''' Function to make customer and/or job names changes. Used by JTjm.
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTnlcCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String
        '
        MsgBox("Function JTnlcCustJobNameChg - JTnlc~1478 has been taken" & vbCrLf &
               "out of service. It is associated with customer and job" & vbCrLf &
               "name changes. If this message comes up, talk to Ed.")
        Return ""
        ' --------------------------------------------------
        ' Start of slJTnlc section
        ' --------------------------------------------------
        '       Dim tmpKeyCollection As New collection()

        '
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                '             If JTjmSettings.ExCust = slJTnlc(k).JobID Then
                '            tmpKeyCollection.Add(k)
                '   End If
            Next
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTnlc(k).JobID Then
                    '              If JTjmSettings.ExJob = slJTnlc(k).SubID Then
                    '             tmpKeyCollection.Add(k)
                    '        End If
                End If
            Next
        End If
        ' Modify logic for slJTnlc placed here .....
        '      For Each k In tmpKeyCollection
        '     Dim tmpItem As JTnlcStructure = slJTnlc(k)
        '    slJTnlc.Remove(k)
        '
        '   tmpItem.JobID = JTjmSettings.NewCust
        '
        '  If JTjmSettings.JobNameChg = True Then
        ' tmpItem.SubID = JTjmSettings.NewJob
        '    End If

        '    Dim tmpinvdate As Date = CDate(tmpItem.InvDate)
        ' =========================================================================
        ' Call to build key here ============================================
        ' =========================================================================
        '   Dim tmpKey As String = JTnlcBldSLKey(tmpinvdate, tmpItem.JobID, tmpItem.SubID, tmpItem.CustInvDate, tmpItem.Supplier)
        '
        '     JTnlcAddSLItem(tmpKey, tmpItem, "JTnlcCustJobNameChg - JTnlc~2275")
        '    Next
        ' --------------------------------------------------
        ' End of slJTnlc section
        ' --------------------------------------------------
        Return ""
    End Function
    ''' <summary>
    ''' Function to update Accounting extract date and time on items that were extracted.
    ''' Function called from JTae.
    ''' </summary>
    ''' <param name="tmpKey"></param>
    ''' <param name="tmpExtRprtName"></param>
    ''' <returns></returns>
    Public Function JTnlcUpdtAcctExtrctDate(tmpKey As String, tmpExtRprtName As String) As Boolean
        If slJTnlc.ContainsKey(tmpKey) Then
            Dim tmpSLItem As JTnlcStructure
            tmpSLItem = slJTnlc(tmpKey)
            slJTnlc.Remove(tmpKey)
            tmpSLItem.AcctExtDate = String.Format("{0:MM/dd/yyyy}", Date.Today)
            tmpSLItem.AcctExtDoc = tmpExtRprtName
            JTnlcAddSLItem(tmpKey, tmpSLItem, "JTnlcUpdtAcctExtrctDate - JTnlc~2296")
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Common function to add items to slJTnlc array. It checked for a few 'must have' elements
    ''' and reports if something is wrong. Some editing has been added prior to adding record.
    ''' More can be added if the need arises ... no sense in adding a record that will cause problems
    ''' later in the system. This is the only place in the entire system where items are
    ''' added to this array. Potentially, this is the spot where the array could be converted
    ''' to an sqldb approach.
    ''' </summary>
    ''' <param name="tmpKey">sl array key</param>
    ''' <param name="tmpRecord">sl array record</param>
    ''' <param name="tmpCalledFrom">text related to who called this function.</param>
    ''' <returns>True --- if add function was completed, 
    ''' False --- if something is wrong with the record to be added.</returns>
    Public Function JTnlcAddSLItem(tmpKey As String, tmpRecord As JTnlcStructure, tmpCalledFrom As String) As Boolean
        ' ------------------------------------------------------------------------------
        ' Check for valid and complete record prior to inserting into slJTnlc. This add
        ' routine is called from several location. This message is a debugging message
        ' and should never appear in production. More edits can be added if necessary to
        ' insure that all records added are valid.
        ' ------------------------------------------------------------------------------
        If tmpRecord.InvDate = "" Or tmpRecord.EntryDate = "" Then
            MsgBox("Record being added to slJTnlc appears to be invalid. Record not added." & vbCrLf &
                   "Add routine called from >" & tmpCalledFrom & "< Write routine name down and" & vbCrLf &
                   "tell Ed - JTnlc~2315")
            Return False
        End If
        slJTnlc.Add(tmpKey, tmpRecord)
        Return True
    End Function
    '
    ''' <summary>
    ''' Function to check to see if the Statement invoice number is unique. Key for 
    ''' a Vendor/Statment number is stored in the SubID field in slJTnlc.
    ''' </summary>
    ''' <param name="tmpDupKeyChk">Value is "0" + Vendor short name + Statement Invoice Number</param>
    ''' <returns>True = Duplicate exists. --- False = Key is unique.</returns>
    Public Function JTDupStmtNumChk(tmpDupKeyChk As String) As Boolean
        Dim key As ICollection = slJTnlc.Keys
        Dim k As String
        For Each k In key
            If slJTnlc(k).SubID = tmpDupKeyChk Then
                Return True
            End If
        Next
        Return False
    End Function
    ''' <summary>
    ''' Simple function that returns JTnlc record based on the record count 
    ''' supplied in tmpSub. Primary user ... JTnlcM.
    ''' </summary>
    ''' <param name="tmpSub">Record number of requested record.</param>
    ''' <param name="slRecord">Area to return data from slJTnlc record.</param>
    ''' <returns>DATA = Record is being returned; EOT = End of SL Taable has been reached.</returns>
    Public Function JTnlcReadslNLC(ByVal tmpSub As Integer, ByRef slRecord As JTnlcStructure) As String
        Dim key As ICollection = slJTnlc.Keys
        If tmpSub < key.Count Then
            slRecord = slJTnlc(key(tmpSub))
            Return "DATA"
        End If
        Return "EOT"
    End Function
    ''' <summary>
    ''' Function to return slJTnls record based on a specific key.
    ''' </summary>
    ''' <param name="tmpKey">Key of nlc record</param>
    ''' <param name="slRecord"></param>
    ''' <returns></returns>
    Public Function JTnlcGetslNLCRec(ByVal tmpKey As String, tmpOption As String, ByRef slRecord As JTnlcStructure) As String
        If slJTnlc.ContainsKey(tmpKey) = True Then
            slRecord = slJTnlc(tmpKey)
            '
            If tmpOption = "DELETE" Then
                slJTnlc.Remove(tmpKey)
            End If
            Return "DATA"
        End If
        Return "EOT"
    End Function
    ''' <summary>
    ''' Function to fill values in cboxJTnlcDataFocus
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcFillCbox()
        cboxJTnlcDataFocus.Items.Clear()
        cboxJTnlcDataFocus.Text = ""
        Dim tmpDataSeq As String = ""
        If rbtnJTnlcSeqDate.Checked = True Then
            tmpDataSeq = "DATE"
        End If
        If rbtnJtnlcSeqJob.Checked = True Then
            tmpDataSeq = "JOB"
        End If
        If rbtnJTnlcseqSpplr.Checked = True Then
            tmpDataSeq = "SPPLR"
        End If
        Dim tmpLastItemAdded As String = Nothing
        Dim tmpSub As Integer = 0
        Do While tmpSub < lvJTnlcData.Items.Count
            Select Case tmpDataSeq
                Case "DATE"
                    If lvJTnlcData.Items(tmpSub).Text <> tmpLastItemAdded Then
                        tmpLastItemAdded = lvJTnlcData.Items(tmpSub).Text
                        cboxJTnlcDataFocus.Items.Add(tmpLastItemAdded)
                    End If
                Case "JOB"
                    If lvJTnlcData.Items(tmpSub).SubItems(1).Text <> "" Then
                        If lvJTnlcData.Items(tmpSub).SubItems(1).Text <> tmpLastItemAdded Then
                            tmpLastItemAdded = lvJTnlcData.Items(tmpSub).SubItems(1).Text
                            cboxJTnlcDataFocus.Items.Add(tmpLastItemAdded)
                        End If
                    End If
                Case "SPPLR"
                    If lvJTnlcData.Items(tmpSub).SubItems(5).Text <> "" Then
                        If lvJTnlcData.Items(tmpSub).SubItems(5).Text <> tmpLastItemAdded Then
                            tmpLastItemAdded = lvJTnlcData.Items(tmpSub).SubItems(5).Text
                            cboxJTnlcDataFocus.Items.Add(tmpLastItemAdded)
                        End If
                    End If
            End Select
            tmpSub += 1
        Loop
        cboxJTnlcDataFocus.Text = JTnlcLastFocusKey
        '
        Return ""
    End Function
    ''' <summary>
    ''' handles focus button changes.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboxJTnlcDataFocus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxJTnlcDataFocus.SelectedIndexChanged,
                                   cboxJTnlcDataFocus.LostFocus
        JTnlcSetFocus()
    End Sub
    ''' <summary>
    ''' Function to resequence JTnlc panel based on value in cboxJTnlcDataFocus.text
    ''' </summary>
    ''' <returns></returns>
    Private Function JTnlcSetFocus()
        Dim tmpSub As Integer = 0
        Dim tmpDataSeq As String = ""
        If rbtnJTnlcSeqDate.Checked = True Then
            tmpDataSeq = "DATE"
        End If
        If rbtnJtnlcSeqJob.Checked = True Then
            tmpDataSeq = "JOB"
        End If
        If rbtnJTnlcseqSpplr.Checked = True Then
            tmpDataSeq = "SPPLR"
        End If
        Do While tmpSub < lvJTnlcData.Items.Count
            Select Case tmpDataSeq
                Case "DATE"
                    If lvJTnlcData.Items(tmpSub).Text = cboxJTnlcDataFocus.Text Then
                        Exit Do
                    End If
                Case "JOB"
                    If lvJTnlcData.Items(tmpSub).SubItems(1).Text = cboxJTnlcDataFocus.Text Then
                        Exit Do
                    End If
                Case "SPPLR"
                    If lvJTnlcData.Items(tmpSub).SubItems(5).Text = cboxJTnlcDataFocus.Text Then
                        Exit Do
                    End If
            End Select
            tmpSub += 1
        Loop
        tmpSub += 21
        If tmpSub >= lvJTnlcData.Items.Count Then    'check to make sure the code doesn't try to process past the end of the lv
            tmpSub = lvJTnlcData.Items.Count - 1
        End If
        lvJTnlcData.Items(tmpSub).EnsureVisible()
        JTnlcLastFocusKey = cboxJTnlcDataFocus.Text
        Return ""
    End Function
    Private Sub cboxJTnlcDataFocus_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) _
                    Handles cboxJTnlcDataFocus.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub
    ''' <summary>
    ''' Function to rename NLC PDF file when they are invoiced. The cust invoice # and date and the customer and job will be added.
    ''' This function is called during the record invoice routines. It is also used in the one-time conversion for JRV files.
    ''' </summary>
    ''' <param name="tmpOrigImageFileName"></param>
    ''' <param name="tmpCustomer"></param>
    ''' <param name="tmpJob"></param>
    ''' <param name="tmpCustInvNum "></param>
    ''' <param name="tmpCustInvDate"></param>
    ''' <returns></returns>
    Public Function JTnlcAddInvInfotoPDFName(tmpOrigImageFileName As String,
                                             tmpOrigFileNameRoute As String,
                                             tmpCustomer As String,
                                             tmpJob As String,
                                             tmpCustInvNum As String,
                                             tmpCustInvDate As String,
                                             tmpInvTotal As String) As String
        '
        '
        Dim tmpNewFileName As String = Nothing
        '
        Dim tmp2digmonth As String = Nothing
        Dim tmp2digday As String = Nothing
        Dim tmp4digyear As String = Nothing
        '
        If tmpCustInvDate <> "" Then
            tmp2digmonth = String.Format("{0:MM}", CDate(tmpCustInvDate))
            tmp2digday = String.Format("{0:dd}", CDate(tmpCustInvDate))
            tmp4digyear = String.Format("{0:yyyy}", CDate(tmpCustInvDate))
        End If
        '
        ' Determine correct year of invoice for selection appropiate year folder
        ' 
        Dim tmpFileNameYearPos As Short = tmpOrigImageFileName.Length - 7
        Dim tmpInvYear As String = Mid(tmpOrigImageFileName, tmpFileNameYearPos, 4)
        '
        Dim tmpYMDInvDate As String = tmp4digyear & tmp2digmonth & tmp2digday
        '
        Dim tmpEDIFileName As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF\" & tmpInvYear &
            "\" & tmpOrigImageFileName & ".PDF"
        '
        ' Replace any ":" in file name being built with "~". This will occur when NJR account like "SHOP:TOOLS"
        ' are used. Perhaps an edit should be created when creating accounts so that this would not happen. 1/2/2021
        '
        If tmpCustomer <> "" Then
            tmpCustomer = tmpCustomer.Replace(":", "~")
        End If
        If tmpJob <> "" Then
            tmpJob = tmpJob.Replace(":", "~")
        End If
        ' building new file name
        '       MsgBox("tmpOrigFileNameRoute ->" & tmpOrigFileNameRoute & "< ~1784/JTnlc")
        tmpNewFileName = tmpOrigFileNameRoute & "_" & tmpCustomer & "_" & tmpJob
        If tmpCustInvDate <> "" Then
            tmpNewFileName &= "_" & tmpYMDInvDate & "_" & tmpCustInvNum & ".PDF"
        Else
            tmpNewFileName &= ".PDF"
        End If

        '
        If tmpInvYear > "2022" Then
            If File.Exists(tmpEDIFileName) = True Then
                My.Computer.FileSystem.RenameFile(tmpEDIFileName, tmpNewFileName)

                '
                Dim tmpOrigFilePath As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF\" & tmpInvYear & "\"
                If tmpInvTotal <> "" Then
                    AddMetaToExistingPDF(tmpOrigFilePath,
                             tmpNewFileName,
                             tmpInvTotal)
                End If
                '         Else
                '            MsgBox("File to be renamed not found - >" & tmpEDIFileName & "< ~1796")
            End If
        End If

        '
        Return tmpNewFileName
    End Function
    ''' <summary>
    ''' Function to add metadata to NLC PDF files. This function will be used in the
    ''' initial conversion as well as when a new entry is made.
    ''' </summary>
    ''' <param name="OrigFilePath"></param>
    ''' <param name="OrigFileName"></param>
    ''' <param name="KeyWord"></param>
    ''' <returns></returns>
    Public Shared Function AddMetaToExistingPDF(ByVal OrigFilePath As String,
                           ByVal OrigFileName As String,
                           ByVal KeyWord As String) As Boolean

        Dim tmpReturnValue As Boolean = True
        Dim fileName As String = OrigFilePath & OrigFileName



        ' -----------------------------------------------------------------------------------
        ' Reading metadata field in PDF to see if there is already a metadata inv. total.
        ' If there is a figure, it is added to the total passed in the entry to the function.
        ' This will happen when an invoice has multiple jobs being invoiced.
        ' -----------------------------------------------------------------------------------
        Dim s As String = Nothing
        Dim reader As PdfReader = New PdfReader(fileName)
        Try
            s = reader.Info("Keywords")
        Catch ex As Exception
            s = "0.00"
        End Try
        reader.Close()
        Dim tmpInvTotal As String = Val(KeyWord) + Val(s)

        Dim document = New Document()
        Dim outfile = OrigFilePath & "TempModifiedFile.PDF"
        Dim writer = New PdfCopy(document, New FileStream(outfile, FileMode.Create))
        document.AddKeywords(tmpInvTotal)
        writer.CreateXmpMetadata()
        Try

            document.Open()

            reader = New PdfReader(fileName)


            For i As Integer = 1 To reader.NumberOfPages

                Dim page = writer.GetImportedPage(reader, i)
                writer.AddPage(page)

            Next i

            reader.Close()


            writer.Close()
            document.Close()

        Catch ex As Exception
            tmpReturnValue = False

            MsgBox("Failed to add metadata - " & vbCrLf & fileName)
        Finally

            writer.Close()
            document.Close()

        End Try

        If tmpReturnValue = True Then
            File.Delete(fileName)
            My.Computer.FileSystem.RenameFile(outfile, OrigFileName)
        End If

        Return tmpReturnValue

    End Function
    ''' <summary>
    ''' Logic inserted here to analyze and update .PDF name (if there is a .PDF.
    ''' The current appropriate name will be created and compared to the current
    ''' .PDF name. If it is different, the current name will be updated.
    ''' Data in the file name is used by the DocLib function. The invoice total
    ''' is also stored in metadata.
    ''' Routine written --- 3/10/2024
    ''' </summary>
    ''' <returns></returns>
    Public Function UpdateImageFileNames() As Boolean
        Dim tmpItem As New JTnlcStructure
        Dim tmpConverted = False
        Dim key As ICollection = slJTnlc.Keys
        Dim k As Short = 0
        Dim tmpslRecCnt As Short = key.Count
        Do While k < tmpslRecCnt

            tmpItem = slJTnlc(key(k))

            Dim tmpYYYYMMDD As String = Nothing
            Dim tmpGeneratedImageName As String = Nothing
            '
            If tmpItem.MorS = "M" Or          ' exclude "IH", "IS", "ADJ" or ""(Stmt)
                    tmpItem.MorS = "S" Or
                    tmpItem.MorS = "NJR" Then
                If tmpItem.SupInvImageFile <> "" Then
                    tmpYYYYMMDD = MMSDDSYYYYtoYYYYMMDD(tmpItem.InvDate)
                    tmpGeneratedImageName = tmpItem.Supplier & "_" &
                            tmpItem.SupInvNum & "_" & tmpYYYYMMDD & "_"
                    If tmpItem.MorS <> "NJR" Then
                        tmpGeneratedImageName &= tmpItem.JobID & "_" & tmpItem.SubID
                    Else
                        tmpGeneratedImageName &= "NJR" & "_" & tmpItem.NJREAcct.Replace(":", "~")
                    End If
                    Dim tmpInvYYYYMMDD As String = Nothing
                    If tmpItem.MorS = "NJR" Then
                        If tmpItem.AcctExtDate <> "" Then
                            Dim tmpAEDYYYYMMDD As String = MMSDDSYYYYtoYYYYMMDD(tmpItem.AcctExtDate)
                            tmpGeneratedImageName &= "_" & tmpAEDYYYYMMDD & "_"
                        End If
                    Else
                        If tmpItem.CustInvDate <> "" Then
                            tmpInvYYYYMMDD = MMSDDSYYYYtoYYYYMMDD(tmpItem.CustInvDate)
                            tmpGeneratedImageName &= "_" & tmpInvYYYYMMDD & "_" &
                                    tmpItem.CustInvNumber
                        End If
                    End If
                    tmpGeneratedImageName &= ".PDF"

                    If tmpGeneratedImageName.ToUpper <> (tmpItem.SupInvImageFile & ".PDF").ToUpper Then
                        Dim tmpInvDtYr As String = tmpItem.InvDate.Substring(tmpItem.InvDate.Length - 4, 4)
                        Dim tmpFilePath As String = JTVarMaint.JTvmFilePathDoc & "VendInvoicePDF/" & tmpInvDtYr & "/"
                        If File.Exists(tmpFilePath & tmpItem.SupInvImageFile & ".PDF") Then
                            My.Computer.FileSystem.RenameFile(tmpFilePath & tmpItem.SupInvImageFile & ".PDF", tmpGeneratedImageName)

                            tmpItem.SupInvImageFile = tmpGeneratedImageName.Substring(0, tmpGeneratedImageName.Length - 4)
                            Dim tmpslkey As String = key(k)
                            slJTnlc.Remove(tmpslkey)
                            JTnlcAddSLItem(tmpslkey, tmpItem, "JTnlcUpdateImageName - JTnlc~1969")
                            ' ------------------------------------------------------------------------
                            ' Check to make sure Matedata invoice balance exists -- If not, update it.
                            ' ------------------------------------------------------------------------

                            ' --------------------------------------------------------------
                            ' Reading metadata field in PDF to get invoice total
                            ' --------------------------------------------------------------
                            Dim tmpMetaInvTl As String = ""
                            AddMetaToExistingPDF(tmpFilePath,
                                         tmpGeneratedImageName,
                                         tmpItem.SupCost)
                            '
                        End If
                    End If
                End If
            End If
            '
            k += 1
        Loop
        Return True
    End Function





    '
End Class