﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class JTaupSum
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvJTaupSum = New System.Windows.Forms.ListView()
        Me.Customer = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Job = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.CreateDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.JobQuote = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ChgQuote = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.InvdTD = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LstInvDt = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PendInv = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.FutQueue = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TBInvd = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TBPosted = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.NotPosted = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.FinalFlag = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ExcludedAct = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.JTaupSumClose = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.JobDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuspendLayout()
        '
        'lvJTaupSum
        '
        Me.lvJTaupSum.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lvJTaupSum.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Customer, Me.Job, Me.CreateDate, Me.JobQuote, Me.ChgQuote, Me.InvdTD, Me.LstInvDt, Me.PendInv, Me.FutQueue, Me.TBInvd, Me.TBPosted, Me.NotPosted, Me.FinalFlag, Me.ExcludedAct, Me.JobDesc})
        Me.lvJTaupSum.FullRowSelect = True
        Me.lvJTaupSum.Location = New System.Drawing.Point(9, 58)
        Me.lvJTaupSum.MultiSelect = False
        Me.lvJTaupSum.Name = "lvJTaupSum"
        Me.lvJTaupSum.Size = New System.Drawing.Size(1214, 379)
        Me.lvJTaupSum.TabIndex = 0
        Me.lvJTaupSum.UseCompatibleStateImageBehavior = False
        Me.lvJTaupSum.View = System.Windows.Forms.View.Details
        '
        'Customer
        '
        Me.Customer.Text = "Customer"
        Me.Customer.Width = 120
        '
        'Job
        '
        Me.Job.Text = "Job"
        Me.Job.Width = 120
        '
        'CreateDate
        '
        Me.CreateDate.Text = "Date"
        Me.CreateDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.CreateDate.Width = 70
        '
        'JobQuote
        '
        Me.JobQuote.Text = "Job Quote"
        Me.JobQuote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.JobQuote.Width = 80
        '
        'ChgQuote
        '
        Me.ChgQuote.Text = "Chg Quotes"
        Me.ChgQuote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ChgQuote.Width = 80
        '
        'InvdTD
        '
        Me.InvdTD.Text = "Inv'd TD"
        Me.InvdTD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.InvdTD.Width = 80
        '
        'LstInvDt
        '
        Me.LstInvDt.Text = "Lst Inv"
        Me.LstInvDt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.LstInvDt.Width = 70
        '
        'PendInv
        '
        Me.PendInv.Text = "Pend Inv"
        Me.PendInv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.PendInv.Width = 80
        '
        'FutQueue
        '
        Me.FutQueue.Text = "Future"
        Me.FutQueue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.FutQueue.Width = 80
        '
        'TBInvd
        '
        Me.TBInvd.Text = "TB Invd"
        Me.TBInvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TBInvd.Width = 80
        '
        'TBPosted
        '
        Me.TBPosted.Text = "Pst'd Act."
        Me.TBPosted.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TBPosted.Width = 80
        '
        'NotPosted
        '
        Me.NotPosted.Text = "Not Post'd"
        Me.NotPosted.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NotPosted.Width = 80
        '
        'FinalFlag
        '
        Me.FinalFlag.Text = "Final"
        Me.FinalFlag.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ExcludedAct
        '
        Me.ExcludedAct.Text = "Excluded"
        Me.ExcludedAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ExcludedAct.Width = 80
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(962, 457)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(257, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Double Cick  JOB to go  to  AUP  panel."
        '
        'JTaupSumClose
        '
        Me.JTaupSumClose.Location = New System.Drawing.Point(22, 448)
        Me.JTaupSumClose.Name = "JTaupSumClose"
        Me.JTaupSumClose.Size = New System.Drawing.Size(134, 35)
        Me.JTaupSumClose.TabIndex = 2
        Me.JTaupSumClose.Text = "Close"
        Me.JTaupSumClose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(18, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(340, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "ACTIVE AUP JOB -  CONTROL PANEL"
        '
        'JobDesc
        '
        Me.JobDesc.Text = "Job Description"
        Me.JobDesc.Width = 0
        '
        'JTaupSum
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(1231, 486)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.JTaupSumClose)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lvJTaupSum)
        Me.Name = "JTaupSum"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "J.I.M. - AUP Job Recap"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lvJTaupSum As ListView
    Friend WithEvents Customer As ColumnHeader
    Friend WithEvents Job As ColumnHeader
    Friend WithEvents CreateDate As ColumnHeader
    Friend WithEvents JobQuote As ColumnHeader
    Friend WithEvents ChgQuote As ColumnHeader
    Friend WithEvents InvdTD As ColumnHeader
    Friend WithEvents LstInvDt As ColumnHeader
    Friend WithEvents PendInv As ColumnHeader
    Friend WithEvents FutQueue As ColumnHeader
    Friend WithEvents TBInvd As ColumnHeader
    Friend WithEvents TBPosted As ColumnHeader
    Friend WithEvents NotPosted As ColumnHeader
    Friend WithEvents FinalFlag As ColumnHeader
    Friend WithEvents ExcludedAct As ColumnHeader
    Friend WithEvents Label1 As Label
    Friend WithEvents JTaupSumClose As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents JobDesc As ColumnHeader
End Class
