﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class JTedi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnJTediClose = New System.Windows.Forms.Button()
        Me.gboxInv1PDF = New System.Windows.Forms.GroupBox()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.gboxInv1Btns = New System.Windows.Forms.GroupBox()
        Me.BtnSplit1 = New System.Windows.Forms.Button()
        Me.LabPages1 = New System.Windows.Forms.Label()
        Me.btnDelete1 = New System.Windows.Forms.Button()
        Me.btnProcess1 = New System.Windows.Forms.Button()
        Me.gboxInv2Btns = New System.Windows.Forms.GroupBox()
        Me.BtnSplit2 = New System.Windows.Forms.Button()
        Me.btnDelete2 = New System.Windows.Forms.Button()
        Me.LabPages2 = New System.Windows.Forms.Label()
        Me.btnProcess2 = New System.Windows.Forms.Button()
        Me.gboxInv3Btns = New System.Windows.Forms.GroupBox()
        Me.BtnSplit3 = New System.Windows.Forms.Button()
        Me.btnDelete3 = New System.Windows.Forms.Button()
        Me.LabPages3 = New System.Windows.Forms.Label()
        Me.btnProcess3 = New System.Windows.Forms.Button()
        Me.gboxInv2PDF = New System.Windows.Forms.GroupBox()
        Me.WebBrowser2 = New System.Windows.Forms.WebBrowser()
        Me.gboxInv3PDF = New System.Windows.Forms.GroupBox()
        Me.WebBrowser3 = New System.Windows.Forms.WebBrowser()
        Me.btnJTediForward = New System.Windows.Forms.Button()
        Me.btnJTediBack = New System.Windows.Forms.Button()
        Me.gboxInv1PDF.SuspendLayout()
        Me.gboxInv1Btns.SuspendLayout()
        Me.gboxInv2Btns.SuspendLayout()
        Me.gboxInv3Btns.SuspendLayout()
        Me.gboxInv2PDF.SuspendLayout()
        Me.gboxInv3PDF.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnJTediClose
        '
        Me.btnJTediClose.Location = New System.Drawing.Point(538, 708)
        Me.btnJTediClose.Name = "btnJTediClose"
        Me.btnJTediClose.Size = New System.Drawing.Size(173, 31)
        Me.btnJTediClose.TabIndex = 0
        Me.btnJTediClose.Text = "Close Panel"
        Me.btnJTediClose.UseVisualStyleBackColor = True
        '
        'gboxInv1PDF
        '
        Me.gboxInv1PDF.Controls.Add(Me.WebBrowser1)
        Me.gboxInv1PDF.Location = New System.Drawing.Point(143, 6)
        Me.gboxInv1PDF.Name = "gboxInv1PDF"
        Me.gboxInv1PDF.Size = New System.Drawing.Size(1020, 219)
        Me.gboxInv1PDF.TabIndex = 2
        Me.gboxInv1PDF.TabStop = False
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.Location = New System.Drawing.Point(3, 18)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(1014, 198)
        Me.WebBrowser1.TabIndex = 0
        '
        'gboxInv1Btns
        '
        Me.gboxInv1Btns.Controls.Add(Me.BtnSplit1)
        Me.gboxInv1Btns.Controls.Add(Me.LabPages1)
        Me.gboxInv1Btns.Controls.Add(Me.btnDelete1)
        Me.gboxInv1Btns.Controls.Add(Me.btnProcess1)
        Me.gboxInv1Btns.Location = New System.Drawing.Point(25, 9)
        Me.gboxInv1Btns.Name = "gboxInv1Btns"
        Me.gboxInv1Btns.Size = New System.Drawing.Size(107, 219)
        Me.gboxInv1Btns.TabIndex = 3
        Me.gboxInv1Btns.TabStop = False
        Me.gboxInv1Btns.Text = "Invoice 1"
        '
        'BtnSplit1
        '
        Me.BtnSplit1.Location = New System.Drawing.Point(16, 162)
        Me.BtnSplit1.Name = "BtnSplit1"
        Me.BtnSplit1.Size = New System.Drawing.Size(86, 39)
        Me.BtnSplit1.TabIndex = 4
        Me.BtnSplit1.Text = "Split"
        Me.BtnSplit1.UseVisualStyleBackColor = True
        '
        'LabPages1
        '
        Me.LabPages1.AutoSize = True
        Me.LabPages1.Location = New System.Drawing.Point(29, 129)
        Me.LabPages1.Name = "LabPages1"
        Me.LabPages1.Size = New System.Drawing.Size(67, 17)
        Me.LabPages1.TabIndex = 3
        Me.LabPages1.Text = "Pages - x"
        '
        'btnDelete1
        '
        Me.btnDelete1.Location = New System.Drawing.Point(16, 75)
        Me.btnDelete1.Name = "btnDelete1"
        Me.btnDelete1.Size = New System.Drawing.Size(86, 39)
        Me.btnDelete1.TabIndex = 2
        Me.btnDelete1.Text = "Delete"
        Me.btnDelete1.UseVisualStyleBackColor = True
        '
        'btnProcess1
        '
        Me.btnProcess1.Location = New System.Drawing.Point(15, 30)
        Me.btnProcess1.Name = "btnProcess1"
        Me.btnProcess1.Size = New System.Drawing.Size(86, 39)
        Me.btnProcess1.TabIndex = 0
        Me.btnProcess1.Text = "Process"
        Me.btnProcess1.UseVisualStyleBackColor = True
        '
        'gboxInv2Btns
        '
        Me.gboxInv2Btns.Controls.Add(Me.BtnSplit2)
        Me.gboxInv2Btns.Controls.Add(Me.btnDelete2)
        Me.gboxInv2Btns.Controls.Add(Me.LabPages2)
        Me.gboxInv2Btns.Controls.Add(Me.btnProcess2)
        Me.gboxInv2Btns.Location = New System.Drawing.Point(25, 238)
        Me.gboxInv2Btns.Name = "gboxInv2Btns"
        Me.gboxInv2Btns.Size = New System.Drawing.Size(107, 219)
        Me.gboxInv2Btns.TabIndex = 5
        Me.gboxInv2Btns.TabStop = False
        Me.gboxInv2Btns.Text = "Invoice 2"
        '
        'BtnSplit2
        '
        Me.BtnSplit2.Location = New System.Drawing.Point(16, 161)
        Me.BtnSplit2.Name = "BtnSplit2"
        Me.BtnSplit2.Size = New System.Drawing.Size(86, 39)
        Me.BtnSplit2.TabIndex = 6
        Me.BtnSplit2.Text = "Split"
        Me.BtnSplit2.UseVisualStyleBackColor = True
        '
        'btnDelete2
        '
        Me.btnDelete2.Location = New System.Drawing.Point(15, 75)
        Me.btnDelete2.Name = "btnDelete2"
        Me.btnDelete2.Size = New System.Drawing.Size(86, 39)
        Me.btnDelete2.TabIndex = 2
        Me.btnDelete2.Text = "Delete"
        Me.btnDelete2.UseVisualStyleBackColor = True
        '
        'LabPages2
        '
        Me.LabPages2.AutoSize = True
        Me.LabPages2.Location = New System.Drawing.Point(29, 128)
        Me.LabPages2.Name = "LabPages2"
        Me.LabPages2.Size = New System.Drawing.Size(67, 17)
        Me.LabPages2.TabIndex = 5
        Me.LabPages2.Text = "Pages - x"
        '
        'btnProcess2
        '
        Me.btnProcess2.Location = New System.Drawing.Point(15, 30)
        Me.btnProcess2.Name = "btnProcess2"
        Me.btnProcess2.Size = New System.Drawing.Size(86, 39)
        Me.btnProcess2.TabIndex = 0
        Me.btnProcess2.Text = "Process"
        Me.btnProcess2.UseVisualStyleBackColor = True
        '
        'gboxInv3Btns
        '
        Me.gboxInv3Btns.Controls.Add(Me.BtnSplit3)
        Me.gboxInv3Btns.Controls.Add(Me.btnDelete3)
        Me.gboxInv3Btns.Controls.Add(Me.LabPages3)
        Me.gboxInv3Btns.Controls.Add(Me.btnProcess3)
        Me.gboxInv3Btns.Location = New System.Drawing.Point(25, 471)
        Me.gboxInv3Btns.Name = "gboxInv3Btns"
        Me.gboxInv3Btns.Size = New System.Drawing.Size(107, 219)
        Me.gboxInv3Btns.TabIndex = 6
        Me.gboxInv3Btns.TabStop = False
        Me.gboxInv3Btns.Text = "Invoice 3"
        '
        'BtnSplit3
        '
        Me.BtnSplit3.Location = New System.Drawing.Point(15, 153)
        Me.BtnSplit3.Name = "BtnSplit3"
        Me.BtnSplit3.Size = New System.Drawing.Size(86, 39)
        Me.BtnSplit3.TabIndex = 8
        Me.BtnSplit3.Text = "Split"
        Me.BtnSplit3.UseVisualStyleBackColor = True
        '
        'btnDelete3
        '
        Me.btnDelete3.Location = New System.Drawing.Point(15, 75)
        Me.btnDelete3.Name = "btnDelete3"
        Me.btnDelete3.Size = New System.Drawing.Size(86, 39)
        Me.btnDelete3.TabIndex = 2
        Me.btnDelete3.Text = "Delete"
        Me.btnDelete3.UseVisualStyleBackColor = True
        '
        'LabPages3
        '
        Me.LabPages3.AutoSize = True
        Me.LabPages3.Location = New System.Drawing.Point(29, 123)
        Me.LabPages3.Name = "LabPages3"
        Me.LabPages3.Size = New System.Drawing.Size(67, 17)
        Me.LabPages3.TabIndex = 7
        Me.LabPages3.Text = "Pages - x"
        '
        'btnProcess3
        '
        Me.btnProcess3.Location = New System.Drawing.Point(15, 30)
        Me.btnProcess3.Name = "btnProcess3"
        Me.btnProcess3.Size = New System.Drawing.Size(86, 39)
        Me.btnProcess3.TabIndex = 0
        Me.btnProcess3.Text = "Process"
        Me.btnProcess3.UseVisualStyleBackColor = True
        '
        'gboxInv2PDF
        '
        Me.gboxInv2PDF.Controls.Add(Me.WebBrowser2)
        Me.gboxInv2PDF.Location = New System.Drawing.Point(146, 238)
        Me.gboxInv2PDF.Name = "gboxInv2PDF"
        Me.gboxInv2PDF.Size = New System.Drawing.Size(1020, 219)
        Me.gboxInv2PDF.TabIndex = 3
        Me.gboxInv2PDF.TabStop = False
        '
        'WebBrowser2
        '
        Me.WebBrowser2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser2.Location = New System.Drawing.Point(3, 18)
        Me.WebBrowser2.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser2.Name = "WebBrowser2"
        Me.WebBrowser2.Size = New System.Drawing.Size(1014, 198)
        Me.WebBrowser2.TabIndex = 0
        '
        'gboxInv3PDF
        '
        Me.gboxInv3PDF.Controls.Add(Me.WebBrowser3)
        Me.gboxInv3PDF.Location = New System.Drawing.Point(149, 471)
        Me.gboxInv3PDF.Name = "gboxInv3PDF"
        Me.gboxInv3PDF.Size = New System.Drawing.Size(1020, 219)
        Me.gboxInv3PDF.TabIndex = 4
        Me.gboxInv3PDF.TabStop = False
        '
        'WebBrowser3
        '
        Me.WebBrowser3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser3.Location = New System.Drawing.Point(3, 18)
        Me.WebBrowser3.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser3.Name = "WebBrowser3"
        Me.WebBrowser3.Size = New System.Drawing.Size(1014, 198)
        Me.WebBrowser3.TabIndex = 0
        '
        'btnJTediForward
        '
        Me.btnJTediForward.Location = New System.Drawing.Point(341, 708)
        Me.btnJTediForward.Name = "btnJTediForward"
        Me.btnJTediForward.Size = New System.Drawing.Size(173, 31)
        Me.btnJTediForward.TabIndex = 7
        Me.btnJTediForward.Text = "> > > >"
        Me.btnJTediForward.UseVisualStyleBackColor = True
        '
        'btnJTediBack
        '
        Me.btnJTediBack.Location = New System.Drawing.Point(152, 708)
        Me.btnJTediBack.Name = "btnJTediBack"
        Me.btnJTediBack.Size = New System.Drawing.Size(173, 31)
        Me.btnJTediBack.TabIndex = 8
        Me.btnJTediBack.Text = "< < < <"
        Me.btnJTediBack.UseVisualStyleBackColor = True
        '
        'JTedi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(1196, 751)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnJTediBack)
        Me.Controls.Add(Me.btnJTediForward)
        Me.Controls.Add(Me.gboxInv3PDF)
        Me.Controls.Add(Me.gboxInv2PDF)
        Me.Controls.Add(Me.gboxInv3Btns)
        Me.Controls.Add(Me.gboxInv2Btns)
        Me.Controls.Add(Me.gboxInv1Btns)
        Me.Controls.Add(Me.gboxInv1PDF)
        Me.Controls.Add(Me.btnJTediClose)
        Me.Name = "JTedi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JTedi"
        Me.gboxInv1PDF.ResumeLayout(False)
        Me.gboxInv1Btns.ResumeLayout(False)
        Me.gboxInv1Btns.PerformLayout()
        Me.gboxInv2Btns.ResumeLayout(False)
        Me.gboxInv2Btns.PerformLayout()
        Me.gboxInv3Btns.ResumeLayout(False)
        Me.gboxInv3Btns.PerformLayout()
        Me.gboxInv2PDF.ResumeLayout(False)
        Me.gboxInv3PDF.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnJTediClose As Button
    Friend WithEvents gboxInv1PDF As GroupBox
    Friend WithEvents gboxInv1Btns As GroupBox
    Friend WithEvents btnDelete1 As Button
    Friend WithEvents btnProcess1 As Button
    Friend WithEvents gboxInv2Btns As GroupBox
    Friend WithEvents btnDelete2 As Button
    Friend WithEvents btnProcess2 As Button
    Friend WithEvents gboxInv3Btns As GroupBox
    Friend WithEvents btnDelete3 As Button
    Friend WithEvents btnProcess3 As Button
    Friend WithEvents gboxInv2PDF As GroupBox
    Friend WithEvents gboxInv3PDF As GroupBox
    Friend WithEvents btnJTediForward As Button
    Friend WithEvents btnJTediBack As Button
    Friend WithEvents BtnSplit1 As Button
    Friend WithEvents LabPages1 As Label
    Friend WithEvents BtnSplit2 As Button
    Friend WithEvents LabPages2 As Label
    Friend WithEvents BtnSplit3 As Button
    Friend WithEvents LabPages3 As Label
    Friend WithEvents WebBrowser1 As WebBrowser
    Friend WithEvents WebBrowser2 As WebBrowser
    Friend WithEvents WebBrowser3 As WebBrowser
End Class