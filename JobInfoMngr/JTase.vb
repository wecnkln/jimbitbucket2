﻿Imports System.IO
Imports System.Net

''' <summary>
''' Accounting System Extract
''' This class will build the accounting system extract file. It will be called in parallel with
''' the accounting system extract report production. It is originally being written for QuickBooks
''' but other accounting solutions will be added.
''' </summary>
Public Class JTase
    '
    ''' <summary>
    ''' Function that does the actual file processing. It write a record to the accounting system import
    ''' file. It checked to see if the file exists. If it does not, it write the "schema" records
    ''' as defined by the accounting system.
    ''' 
    ''' Not currently in this function but ... file naming logic must be incorporated so that it matched 
    ''' the extract report titles for this specific extract.
    ''' </summary>
    ''' <param name="tmpFileRecord"></param>
    ''' <returns></returns>
    Public Shared Function JTaseWriteFile(tmpFileRecord As String) As Boolean
        '
        Dim TRNS As String = "!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab &
        "ACCNT" & vbTab & "NAME" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab &
        "MEMO" & vbTab & "CLEAR" & vbTab & "TOPRINT" & vbTab & "NAMEISTAXABLE" & vbTab & "DUEDATE" & vbTab &
        "TERMS" & vbTab & "PAYMETH" & vbTab & "SHIPVIA" & vbTab & "SHIPDATE" & vbTab & "REP" & vbTab &
        "FOB" & vbTab & "PONUM" & vbTab & "INVMEMO" & vbTab & "ADDR1" & vbTab & "ADDR2" & vbTab & "ADDR3" & vbTab &
        "ADDR4" & vbTab & "ADDR5" & vbTab & "SADDR1" & vbTab & "SADDR2" & vbTab & "SADDR3" & vbTab & "SADDR4" & vbTab & "SADDR5"
        '
        Dim SPL As String = "!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab &
        "ACCNT" & vbTab & "NAME" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab &
        "CLEAR" & vbTab & "QNTY" & vbTab & "PRICE" & vbTab & "INVITEM" & vbTab & "PAYMETH" & vbTab & "TAXABLE" & vbTab &
        "EXTRA" & vbTab & "VATCODE" & vbTab & "VATRATE" & vbTab & "VATAMOUNT" & vbTab & "VALADJ" & vbTab &
        "SERVICEDATE" & vbTab & "TAXCODE" & vbTab & "TAXRATE" & vbTab & "TAXAMOUNT" & vbTab & "OTHER2" & vbTab & "OTHER3"
        '
        Dim ENDTRNS As String = "!ENDTRNS"
        '
        Dim ACCNT As String = "!ACCNT" & vbTab & "NAME" & vbTab & "REFNUM" & vbTab & "ACCNTTYPE" & vbTab & "ACCNUM"
        '
        Dim TIMEACT As String = "!TIMEACT" & vbTab & vbTab & "DATE" & vbTab & "JOB" & vbTab & "EMP" & vbTab & "ITEM" & vbTab &
        "PITEM" & vbTab & "DURATION" & vbTab & "PROJ" & vbTab & "NOTE" & vbTab & "XFERTOPAYROLL" & vbTab & "BILLINGSTATUS"
        '
        Dim INVITEM As String = "!INVITEM" & vbTab & vbTab & "NAME" & vbTab & "INVITEMTYPE" & vbTab & "DESC" & vbTab &
        "PURCHASEDESC" & vbTab & "ACCNT" & vbTab & "ASSETACCNT" & vbTab & "COGSACCNT" & vbTab & "PRICE" & vbTab &
        "COST" & vbTab & "TAXABLE" & vbTab & "PAYMETH" & vbTab & "TAXVEND" & vbTab & "TAXDIST" & vbTab &
        "PREFVEND" & vbTab & "REORDERPOINT" & vbTab & "EXTRA" & vbTab & "CUSTFLD1" & vbTab & "CUSTFLD2" & vbTab &
        "CUSTFLD3" & vbTab & "CUSTFLD4" & vbTab & "CUSTFLD5" & vbTab & "DEP_TYPE" & vbTab & "ISPASSEDTHRU" & vbTab & "HIDDEN"
        '
        Dim CUST As String = "!CUST" & vbTab & "NAME" & vbTab & "BADDR1" & vbTab & "BADDR2" & vbTab & "BADDR3" & vbTab &
        "BADDR4" & vbTab & "BADDR5" & vbTab & "COMPANYNAME" & vbTab & "FIRSTNAME" & vbTab & "MIDINIT" & vbTab &
        "LASTNAME" & vbTab & "CONT1" & vbTab & "EMAIL"
        '
        Dim VEND As String = "!VEND" & vbTab & "NAME" & vbTab & "ADDR1" & vbTab & "ADDR2" & vbTab &
        "ADDR3" & vbTab & "ADDR4" & vbTab & "ADDR5"
        '
        Dim OTHERNAME As String = "!OTHERNAME" & vbTab & "NAME" & vbTab & "BADDR1" & vbTab & "BADDR2" & vbTab &
        "BADDR3" & vbTab & "BADDR4" & vbTab & "BADDR5"
        '
        Dim filePath As String = JTVarMaint.JTvmFilePath & "QBFile.txt"
        '
        Dim NewFileFlag As Boolean = False

        If My.Computer.FileSystem.FileExists(filePath) = False Then
            NewFileFlag = True
        End If
        '
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter(filePath, True)
        If NewFileFlag = True Then
            file.WriteLine(TRNS)
            file.WriteLine(SPL)
            file.WriteLine(ENDTRNS)
            file.WriteLine(ACCNT)
            file.WriteLine(TIMEACT)
            file.WriteLine(INVITEM)
            file.WriteLine(CUST)
            file.WriteLine(VEND)
            file.WriteLine(OTHERNAME)
        End If
        file.WriteLine(tmpFileRecord)
        '
        file.Close()
        Return True
    End Function
    '


End Class
