﻿Imports System.IO
Public Class JThr
    Public Structure JThrEmpData
        Dim JThraID As String
        Dim JThraEmpName As String
        Dim JThraLabCat As String
        Dim JThraPayMeth As String
        Dim JThraPRId As String
        Dim JTHRaPayRate As String
        Dim JThraEmpEmail As String
        Dim JThraTermDate As String
        Dim JThraWebAuth As String
        Dim JThraWebUpdtPend As Boolean
    End Structure
    '
    Public JThrArray As New SortedList

    Public JThrChange As String = "N"
    Dim saveSubScript As Integer
    Dim saveJThrID As String

    '
    Public JThrSub As Integer = 0

    Public Sub JThrInit()
        ' ----------------------------------------------------------
        ' This SUB prepares Human Resource for presentation.
        ' ----------------------------------------------------------
        '
        ' Make sure ListView is empty.
        '
        JThrListView.Items.Clear()
        '
        Dim key As ICollection = JThrArray.Keys
        Dim k As String = Nothing
        For Each k In key
            '
            Dim itmBuild(10) As String
            Dim lvLine As ListViewItem
            '
            itmBuild(0) = JThrArray(k).JThraID
            itmBuild(1) = JThrArray(k).JThraEmpName
            itmBuild(2) = JThrArray(k).JThraLabCat
            itmBuild(3) = JThrArray(k).JThraPayMeth
            itmBuild(4) = JThrArray(k).JThraPRId
            itmBuild(5) = String.Format("{0:0.00}", Val(JThrArray(k).JTHRaPayRate))
            itmBuild(6) = JThrArray(k).JThraEmpEmail
            If JThrArray(k).JThraTermDate <> "" Then
                itmBuild(7) = JThrArray(k).JThraTermDate
            Else
                itmBuild(7) = ""
            End If
            If JThrArray(k).JThraWebAuth <> "" Then
                itmBuild(8) = JThrArray(k).JThraWebAuth
            Else
                itmBuild(8) = ""
            End If
            If JThrArray(k).JThraWebUpdtPend = True Then
                itmBuild(9) = True
            Else
                itmBuild(9) = False
            End If
            ' --------------------------------------------------------------------
            ' I think I put the following tags here just to remember the spelling.
            ' --------------------------------------------------------------------
            '       Dim JThraWebAuth As String
            '       Dim JThraWebUpdtPend As Boolean





            lvLine = New ListViewItem(itmBuild)
                JThrListView.Items.Add(lvLine)
                Next
        '
        ' clear Panel entry fields.
        '
        txtJThrID.Text = ""
        txtJThrEmpName.Text = ""
        txtJThrJobCat.Text = ""
        radiobtn1099.Checked = False
        radiobtnW2.Checked = True
        txtJThrPRID.Text = ""
        txtJThrRate.Text = ""
        txtJThrEmail.Text = ""
        If JThrArray(k).JThraTermDate <> "" Then
            btnTerminate.Text = "Re-activate"
        End If
        '
        Me.Show()

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Hide()
        MsgBox("Human Resource maintenance cancelled." & vbCrLf &
               "All changes made in this session will be rolled back.")
        '
        JTMainMenu.JTMMinit()
    End Sub
    Public Sub JThrArrayFill()
        ' -----------------------------------------------
        ' This SUB initial the array and read the file to
        ' fill the array with existing values.
        ' -----------------------------------------------
        '
        '
        ' load array with values from JTlcLabCat.dat
        '
        JThrSub = 0
        Dim JThrWhichField As Integer = 1

        Dim filePath As String = JTVarMaint.JTvmFilePath & "JThrData.dat"
        Try
            JThrArray.Clear()
            Dim tmpArrayItem As JThrEmpData = Nothing
            Dim line As String
            Dim readFile As System.IO.TextReader = New _
                StreamReader(filePath)
            While True
                line = readFile.ReadLine()
                If line Is Nothing Then
                    Exit While
                Else

                    Dim tmpReadFileKey As String
                    Dim tmpReadFileData As String

                    ' ----------------------------------------------------------------
                    ' Logic to handle each record and where to store it.
                    ' ----------------------------------------------------------------
                    Dim tmpLocKey As Integer = 0
                    Dim tmpLoc As Integer = 1

                    Do While line.Substring(tmpLoc, 1) <> ">"
                        tmpLoc += 1
                    Loop
                    tmpReadFileKey = line.Substring(1, tmpLoc - 1)
                    If tmpReadFileKey = "hrRecord" Or tmpReadFileKey = "/hrRecord" Then
                        tmpReadFileData = ""
                    Else
                        tmpLoc += 1
                        tmpLocKey = tmpLoc
                        '
                        Do While line.Substring(tmpLoc, 1) <> "<"
                            tmpLoc += 1
                        Loop
                        tmpReadFileData = line.Substring(tmpLocKey, tmpLoc - tmpLocKey)
                    End If

                    Select Case tmpReadFileKey
                    '                     '
                        Case "ID"
                            tmpArrayItem.JThraID = tmpReadFileData
                        Case "EmpName"
                            tmpArrayItem.JThraEmpName = tmpReadFileData
                        Case "LabCat"
                            tmpArrayItem.JThraLabCat = tmpReadFileData
                        Case "PayMeth"
                            tmpArrayItem.JThraPayMeth = tmpReadFileData
                        Case "PRId"
                            tmpArrayItem.JThraPRId = tmpReadFileData
                        Case "PayRate"
                            tmpArrayItem.JTHRaPayRate = String.Format("{0: 0.00}", Val(tmpReadFileData))
                        Case "EmpEmail"
                            tmpArrayItem.JThraEmpEmail = tmpReadFileData
                        Case "TermDate"
                            tmpArrayItem.JThraTermDate = tmpReadFileData
                        Case "WebAuth"
                            tmpArrayItem.JThraWebAuth = tmpReadFileData
                        Case "WebPendUpdt"
                            tmpArrayItem.JThraWebUpdtPend = tmpReadFileData
                        Case "/hrRecord"
                            If tmpArrayItem.JThraWebUpdtPend <> True Then
                                tmpArrayItem.JThraWebUpdtPend = False
                            End If
                            JThrArray.Add(tmpArrayItem.JThraID, tmpArrayItem)
                            tmpArrayItem = Nothing
                    End Select
                End If
            End While
            readFile.Close()
            readFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        ' -----------------------------------------------------
        ' Logic to process add request for HR subsystem.
        ' -----------------------------------------------------
        Dim tmpErrorFlag As String = "N"
        txtErrorMsg.Text = ""
        If radiobtn1099.Checked = True Or radiobtnW2.Checked = True Then
        Else
            txtErrorMsg.Text = "No pay method selected."
            tmpErrorFlag = "Y"
        End If

        If txtJThrJobCat.Text = "" Then
            txtErrorMsg.Text = "No employee labor category entered."
            tmpErrorFlag = "Y"
        End If

        If txtJThrEmpName.Text = "" Then
            txtErrorMsg.Text = "No employee name entered."
            tmpErrorFlag = "Y"
        End If
        If txtJThrID.Text = "" Then
            txtErrorMsg.Text = "No employee Id entered."
            tmpErrorFlag = "Y"
        End If
        txtJThrRate.Text = String.Format("{0:0.00}", Val(txtJThrRate.Text))
        Dim funcPos As Integer
        Dim funcID As String = txtJThrID.Text
        Dim funcEmpName As String = ""
        Dim funcLabCat As String = ""
        Dim funcPayMeth As String = ""
        If JThrFindFunc(funcPos, funcID, funcEmpName, funcLabCat, funcPayMeth) = "DATA" Then
            txtErrorMsg.Text = "Duplicate Employee ID."
            tmpErrorFlag = "Y"
        End If




        Dim funcKey As String = txtJThrJobCat.Text
        Dim funcName As String = ""
        Dim funcStanRate As String = ""
        Dim funcShopRate As String = ""


        If LaborCategories.JTlcFindFunc(funcPos, funcKey, funcName, funcStanRate, funcShopRate) <> "DATA" Then
            txtErrorMsg.Text = "Invalid Labor Category entered."
            tmpErrorFlag = "Y"
        End If

        If tmpErrorFlag = "Y" Then
            Me.Hide()
            Me.Show()
            Exit Sub
        End If
        '
        ' Edit complete. Following logic will insert the new item into array.
        '
        Dim tmpRecordInserted As String = "N"
        Dim tmpArrayItem As JThrEmpData = Nothing
        tmpArrayItem.JThraID = txtJThrID.Text
        tmpArrayItem.JThraEmpName = txtJThrEmpName.Text
        tmpArrayItem.JThraLabCat = txtJThrJobCat.Text
        tmpArrayItem.JThraPRId = txtJThrPRID.Text
        tmpArrayItem.JTHRaPayRate = txtJThrRate.Text
        tmpArrayItem.JThraEmpEmail = txtJThrEmail.Text
        If radiobtn1099.Checked = True Then
            tmpArrayItem.JThraPayMeth = "1099"
        Else
            tmpArrayItem.JThraPayMeth = "W2"
        End If
        Dim tmpwebauthkey As String = Nothing
        JThrWebAuthFormat(tmpwebauthkey)
        tmpArrayItem.JThraWebAuth = tmpwebauthkey
        tmpArrayItem.JTHRaPayRate = True
        JThrArray.Add(tmpArrayItem.JThraID, tmpArrayItem)
        tmpArrayItem = Nothing

        JThrChange = "Y"
        JThrFlush()
        '
        ' Redisplay panel with updated Listview
        '
        JThrInit()

        '

    End Sub
    Private Sub JThrListView_SelectedIndexChanged(sender As Object, e As EventArgs) Handles JThrListView.DoubleClick
        ' ----------------------------------------------------------------
        ' Sub moved data to textboxes for additional processing after a dbl-click
        ' on a specific line.
        '
        ' listview line index ... first data item in listview is item zero
        '-----------------------------------------------------------------
        Dim tmpHRID As String = JThrListView.SelectedItems(0).Text
        txtJThrID.Text = JThrArray(tmpHRID).JThraID
        txtJThrEmpName.Text = JThrArray(tmpHRID).JThraEmpName
        txtJThrJobCat.Text = JThrArray(tmpHRID).JThraLabCat
        txtJThrPRID.Text = JThrArray(tmpHRID).JThraPRId
        txtJThrRate.Text = JThrArray(tmpHRID).JTHRaPayRate
        txtJThrEmail.Text = JThrArray(tmpHRID).JThraEmpEmail
        If JThrArray(tmpHRID).JThraPayMeth = "1099" Then
            radiobtn1099.Checked = True
        Else
            radiobtnW2.Checked = True
        End If
        If JThrArray(tmpHRID).JThraTermDate <> "" Then
            btnTerminate.Text = "Re-Activate"
        Else
            btnTerminate.Text = "Terminate"
        End If
        '
        If JThrArray(tmpHRID).JThraWebAuth <> Nothing Then
            If JThrArray(tmpHRID).JThraWebAuth.substring(0, 1) = "X" Then
                chkbJThrWABlog.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(1, 1) = "X" Then
                chkbJThrWAtk.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(2, 1) = "X" Then
                chkbJThrWAtkMgmt.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(3, 1) = "X" Then
                chkbJThrWAvend.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(4, 1) = "X" Then
                chkbJThrWAcust.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(5, 1) = "X" Then
                chkbJThrWAcustcomm.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(6, 1) = "X" Then
                chkbJThrWAcustimage.Checked = True
            End If
            If JThrArray(tmpHRID).JThraWebAuth.substring(7, 1) = "X" Then
                chkbJThrWAlabor.Checked = True
            End If
        End If
        '
        txtErrorMsg.Text = ""
        '
        ' save constants for later processing
        '
        saveJThrID = JThrArray(tmpHRID).JThraID
        saveSubScript = JThrSub

    End Sub
    Private Sub txtJThrID_LostFocus(sender As Object, e As EventArgs) Handles txtJThrID.LostFocus
        ' ------------------------------------------------------------------------
        ' Convert employee ID entry to add caps.
        ' ------------------------------------------------------------------------
        txtJThrID.Text = txtJThrID.Text.ToUpper()
    End Sub
    Private Sub txtJThrJobCat_LostFocus(sender As Object, e As EventArgs) Handles txtJThrJobCat.LostFocus
        ' ------------------------------------------------------------------------
        ' Convert Labor Cat entry to all caps.
        ' ------------------------------------------------------------------------
        txtJThrJobCat.Text = txtJThrJobCat.Text.ToUpper()
    End Sub
    Private Sub btnChange_Click(sender As Object, e As EventArgs) Handles btnChange.Click
        ' -------------------------------------------
        ' Logic to make changes to a Labor Categpry.
        ' -------------------------------------------
        Dim tmpErrorFlag As String = "N"
        txtErrorMsg.Text = ""
        '
        '
        '
        '
        If radiobtn1099.Checked = True Or radiobtnW2.Checked = True Then
        Else
            txtErrorMsg.Text = "No pay method selected."
            tmpErrorFlag = "Y"
        End If

        If txtJThrJobCat.Text = "" Then
            txtErrorMsg.Text = "No employee labor category entered."
            tmpErrorFlag = "Y"
        End If

        If txtJThrEmpName.Text = "" Then
            txtErrorMsg.Text = "No employee name entered."
            tmpErrorFlag = "Y"
        End If
        If txtJThrID.Text = "" Then
            txtErrorMsg.Text = "No employee Id entered."
            tmpErrorFlag = "Y"
        End If
        Dim funcPos As Integer
        Dim funcID As String = txtJThrID.Text
        '
        '
        Dim funcKey As String = txtJThrJobCat.Text
        Dim funcName As String = ""
        Dim funcStanRate As String = ""
        Dim funcShopRate As String = ""


        If LaborCategories.JTlcFindFunc(funcPos, funcKey, funcName, funcStanRate, funcShopRate) <> "DATA" Then
            txtErrorMsg.Text = "Invalid Labor Category entered."
            tmpErrorFlag = "Y"
        End If
        '
        If StrComp(txtJThrID.Text, saveJThrID) <> 0 Then
            txtErrorMsg.Text = "Employee ID Key cannot be changed. Delete and Re-add if necessary."
            tmpErrorFlag = "Y"
        End If
        If tmpErrorFlag = "Y" Then
            Me.Hide()
            Me.Show()
            Exit Sub
        End If
        JThrArray.Remove(txtJThrID.Text)
        Dim tmpArrayItem As JThrEmpData = Nothing
        tmpArrayItem.JThraID = txtJThrID.Text
        tmpArrayItem.JThraEmpName = txtJThrEmpName.Text
        tmpArrayItem.JThraLabCat = txtJThrJobCat.Text
        tmpArrayItem.JThraPRId = txtJThrPRID.Text
        tmpArrayItem.JTHRaPayRate = txtJThrRate.Text
        tmpArrayItem.JThraEmpEmail = txtJThrEmail.Text

        If radiobtn1099.Checked = True Then
            tmpArrayItem.JThraPayMeth = "1099"
        Else
            tmpArrayItem.JThraPayMeth = "W2"
        End If
        Dim tmpwebauthkey As String = Nothing
        JThrWebAuthFormat(tmpwebauthkey)
        tmpArrayItem.JThraWebAuth = tmpwebauthkey
        tmpArrayItem.JTHRaPayRate = True
        JThrArray.Add(tmpArrayItem.JThraID, tmpArrayItem)
        JThrFlush()
        JThrChange = "Y"
        JThrInit()

    End Sub
    ''' <summary>
    ''' Function to convert checkboxes on panel to concatinated web key
    ''' </summary>
    ''' <param name="tmpWebAuthKey"></param>
    ''' <returns></returns>
    Private Function JThrWebAuthFormat(tmpWebAuthKey As String) As Boolean
        If chkbJThrWABlog.Checked = True Then
            tmpWebAuthKey = "X"
        Else
            tmpWebAuthKey = "_"
        End If
        If chkbJThrWAtk.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAtkMgmt.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAvend.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAcust.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAcustcomm.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAcustimage.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        If chkbJThrWAlabor.Checked = True Then
            tmpWebAuthKey &= "X"
        Else
            tmpWebAuthKey &= "_"
        End If
        Return True
    End Function
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        ' -----------------------------------------------------------
        ' Sub to delete an employee.
        ' -----------------------------------------------------------
        txtErrorMsg.Text = ""
        If StrComp(saveJThrID, txtJThrID.Text) <> 0 Then
            txtErrorMsg.Text = "Labor Code change not allowed on Delete. No record deleted."
            Exit Sub
        End If
        JThrArray.Remove(txtJThrID.Text)
        JThrChange = "Y"
        JThrFlush()
        JThrInit()

    End Sub
    Private Sub btnDone_Click(sender As Object, e As EventArgs) Handles btnDone.Click
        ' ------------------------------------------------------------
        ' Code to complete Worker maintenance. If changes have
        ' been made, a new file is written to save the changes.
        ' ------------------------------------------------------------
        If JThrChange <> "Y" Then
            Me.Hide()

            JTMainMenu.JTMMinit()
            Exit Sub
        End If
        JThrFlush()
        Me.Hide()
        JTMainMenu.JTMMFirstTimeThru = "FIRST"
        JTMainMenu.JTMMinit()

    End Sub
    Private Function JThrFlush() As String
        JThrSub = 0
        Dim fileRecord As String
        Dim key As ICollection = JThrArray.Keys
        Dim k As String = Nothing
        Dim filePath As String = JTVarMaint.JTvmFilePath & "JThrData.dat"
        Try
            Dim writeFile As System.IO.TextWriter = New StreamWriter(filePath)
            For Each k In key
                '
                Dim tmpActivity As String = "YES"
                If JThrArray(k).JThraTermDate <> "" Then
                    '
                    ' look to see if there is still activity present in the tk DB.
                    ' If yes, keep the record ... if No delete the record.
                    '
                    tmpActivity = JTtk.JTtkChkForActivity(JThrArray(k).JThraID)

                End If
                If tmpActivity <> "NO" Then
                    '
                    fileRecord = "<hrRecord>"
                    writeFile.WriteLine(fileRecord)
                    '
                    fileRecord = "<ID>" & JThrArray(k).JThraID & "</ID>"
                    writeFile.WriteLine(fileRecord)
                    '
                    fileRecord = "<EmpName>" & JThrArray(k).JThraEmpName & "</EmpName>"
                    writeFile.WriteLine(fileRecord)
                    '
                    fileRecord = "<LabCat>" & JThrArray(k).JThraLabCat & "</LabCat>"
                    writeFile.WriteLine(fileRecord)
                    '
                    fileRecord = "<PayMeth>" & JThrArray(k).JThraPayMeth & "</PayMeth>"
                    writeFile.WriteLine(fileRecord)

                    If JThrArray(k).JThraPRId <> "" Then
                        fileRecord = "<PRId>" & JThrArray(k).JThraPRId & "</PRId>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    '
                    If JThrArray(k).JTHRaPayRate <> "" Then
                        fileRecord = "<PayRate>" & JThrArray(k).JTHRaPayRate & "</PayRate>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    '
                    If JThrArray(k).JThraEmpEmail <> "" Then
                        fileRecord = "<EmpEmail>" & JThrArray(k).JThraEmpEmail & "</EmpEmail>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    If JThrArray(k).JThraTermDate <> "" Then
                        fileRecord = "<TermDate>" & JThrArray(k).JThraTermDate & "</TermDate>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    If JThrArray(k).JThraWebAuth <> "" Then
                        fileRecord = "<WebAuth>" & JThrArray(k).JThraWebAuth & "</WebAuth>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    If JThrArray(k).JThraWebUpdtPend = True Then
                        ' ------------------------------------------------------------------------------------------
                        ' Logic to update internet DB goes here. If update is successful, blank out pendwebupdt field.
                        ' If updt is unsuccessful, write tTRUE value to file to queue future attempt.
                        ' ------------------------------------------------------------------------------------------
                        fileRecord = "<WebPendUpdt>" & JThrArray(k).JThraWebUpdtPend & "</WebPendUpdt>"
                        writeFile.WriteLine(fileRecord)
                    End If
                    '
                    fileRecord = "</hrRecord>"
                    writeFile.WriteLine(fileRecord)
                End If
                '
            Next
            '
            writeFile.Flush()
            writeFile.Close()
            writeFile = Nothing
        Catch ex As IOException
            MsgBox(ex.ToString)
        End Try
        Return ""
    End Function

    ' ==========================================================
    ' Functions
    ' ==========================================================
    Function JThrFindFunc(ByVal JThrfPOS As Integer, ByRef JThrfID As String,
                          ByRef JThrfEmpName As String, ByRef JThrfLabCat As String,
                          ByRef JThrfPayMeth As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values and check for a keys existance. Row is
        ' selected by 'JThrfID' variable set by calling SUB. The entire Array will be searched.
        ' If the value is found, the position and the remaining field values are returned.
        ' If the key does not exist, the function returns a 'INVALID' value.
        ' -----------------------------------------------------------------------------
        '
        If JThrfID = "" Then
            Return "INVALID"
        End If
        If JThrArray.ContainsKey(JThrfID) = False Then
            Return "INVALID"
        End If
        JThrfEmpName = JThrArray(JThrfID).JThraEmpName
        JThrfLabCat = JThrArray(JThrfID).JThraLabCat
        JThrfPayMeth = JThrArray(JThrfID).JThraPayMeth
        Return "DATA"
    End Function
    Function JThrReadFunc(JThrfPOS As Integer, ByRef JThrfID As String, ByRef JThrfEmpName As String,
                          ByRef JThrfLabCat As String, ByRef JThrfPayMeth As String, ByRef JThrfTermDate As String) As String
        '
        ' -----------------------------------------------------------------------------
        ' Function to return values from single row of array JTlcArray. Row to be
        ' selected by 'JThrfPOS' variable set by calling SUB. Limits must be set to
        ' make sure JThrfPOS value is within the array scope.
        ' -----------------------------------------------------------------------------
        '
        '      If JThrfPOS > 74 Then
        '     Return "EMPTY"
        '    Exit Function
        '   End If
        Dim key As ICollection = JThrArray.Keys
        If JThrfPOS >= key.Count Then
            Return "EMPTY"
            '        Exit Function
        Else
            JThrfID = JThrArray(key(JThrfPOS)).JThraID
            JThrfEmpName = JThrArray(key(JThrfPOS)).JThraEmpName
            JThrfLabCat = JThrArray(key(JThrfPOS)).JThraLabCat
            JThrfPayMeth = JThrArray(key(JThrfPOS)).JThraPayMeth
            JThrfTermDate = JThrArray(key(JThrfPOS)).JThraTermDate
        End If
        Return "DATA"
    End Function
    Function JThrNewReadFunc(ByRef tmpPos As Integer, ByRef tmpID As String, ByRef tmpName As String,
                             ByRef tmpLabCat As String, ByRef tmpPayMeth As String, ByRef tmpPRID As String,
                             ByRef tmpPayRate As String, ByRef tmpEmail As String, ByRef tmpTermDate As String) As String
        '
        Dim key As ICollection = JThrArray.Keys
        '
        If tmpPos < key.Count Then

            '    If JThrArray(key(tmpPos)).JThraID <> "" Then
            tmpID = JThrArray(key(tmpPos)).JThraID
            tmpName = JThrArray(key(tmpPos)).JThraEmpName
            tmpLabCat = JThrArray(key(tmpPos)).JThraLabCat
            tmpPRID = JThrArray(key(tmpPos)).JThraPRId
            tmpPayRate = JThrArray(key(tmpPos)).JTHRaPayRate
            tmpEmail = JThrArray(key(tmpPos)).JThraEmpEmail
            tmpPayMeth = JThrArray(key(tmpPos)).JThraPayMeth
            tmpTermDate = JThrArray(key(tmpPos)).JThraTermDate
            Return "DATA"
        Else
            Return "EMPTY"
        End If
    End Function

    Private Sub btnTerminate_Click(sender As Object, e As EventArgs) Handles btnTerminate.Click
        Dim tmpArrayItem As JThrEmpData = JThrArray(txtJThrID.Text)
        If btnTerminate.Text = "Terminate" Then
            Dim tmpYesorNo = MsgBox("Selected employee will be terminated. No future timekeepings entries" _
                                               & vbCrLf & "will be allowed. The record will be completely purged from the system" _
                                               & vbCrLf & "when all activity asssociated with this employee has been purged." _
                                               & vbCrLf & "Do you want to continue(Yes Or No)?", vbYesNo, "J.I.M. - Terminate Employee")
            '
            If tmpYesorNo = DialogResult.No Then
                Exit Sub
            End If
            '
            JThrSub = saveSubScript
            tmpArrayItem.JThraTermDate = Date.Today
        Else
            JThrSub = saveSubScript
            tmpArrayItem.JThraTermDate = ""
        End If
        JThrArray.Remove(txtJThrID.Text)
        JThrArray.Add(tmpArrayItem.JThraID, tmpArrayItem)
        JThrChange = "Y"
        JThrInit()
    End Sub

    Private Sub txtJThrEmail_LostFocus(sender As Object, e As EventArgs) Handles txtJThrEmail.LostFocus
        If txtJThrEmail.Text <> "" Then
            Dim tmpJTEmailEntry As String = txtJThrEmail.Text
            Dim RU As New RegexUtilities
            If RU.IsValidEmail(tmpJTEmailEntry) = False Then
                ToolTip1.Show("Email address not valid. Correct and resubmit.(HR.001)", txtJThrEmail, 35, -55, 7000)
                Exit Sub
            End If
        End If
    End Sub


End Class