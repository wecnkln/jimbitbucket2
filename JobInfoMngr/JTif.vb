﻿Public Class JTif

    ' ----------------------------------------------------------------------------------
    ' Class to handle job specific, non-standard invoice format changes by job.
    ' ----------------------------------------------------------------------------------
    Structure JTifslStruct
        Dim Job As String
        Dim Subjob As String
        ' Labor
        Dim InvLabTaskSummary As Boolean
        Dim InvLabWorkerDetail As Boolean
        Dim InvLabTotals As Boolean
        ' Services
        Dim InvSubSerDetails As Boolean
        Dim InvSubSerTotals As Boolean
        Dim InvMatDetails As Boolean
        Dim InvMatTotals As Boolean
        ' Material/Subcontractor Cost Options
        Dim InvMatShowCost As Boolean
        Dim InvSubShowCost As Boolean
    End Structure
    '
    Public slJTif As New SortedList
    '
    '
    Public Sub JTifUpdate(tmpJob As String, tmpSubjob As String)
        Dim tmpKey As String = tmpJob & tmpSubjob
        txtJTifJob.Text = tmpJob
        txtJTifSubJob.Text = tmpSubjob
        If slJTif.ContainsKey(tmpKey) Then
            rbtnJTifLabTask.Checked = slJTif(tmpKey).InvLabTaskSummary
            rbtnJTifLabDetails.Checked = slJTif(tmpKey).InvLabWorkerDetail
            rbtnJTifLabTlsOnly.Checked = slJTif(tmpKey).InvLabTotals
            '
            rbtnJTifSubDetails.Checked = slJTif(tmpKey).InvSubSerDetails
            rbtnJTifSubTlsOnly.Checked = slJTif(tmpKey).InvSubSerTotals
            '
            rbtnJTifMatDetails.Checked = slJTif(tmpKey).InvMatDetails
            rbtnJTifMatTlsOnly.Checked = slJTif(tmpKey).InvMatTotals
            '
            cboxJTifMatShowCost.Checked = slJTif(tmpKey).InvMatShowCost
            cboxJTifSubShowCost.Checked = slJTif(tmpKey).InvSubShowCost
            '
            slJTif.Remove(tmpKey)
        Else
            rbtnJTifLabTask.Checked = JTas.JTasInvLabTaskSummary
            rbtnJTifLabDetails.Checked = JTas.JtasInvLabWorkerDetail
            rbtnJTifLabTlsOnly.Checked = JTas.JTasInvLabTotals
            '
            rbtnJTifSubDetails.Checked = JTas.JTasInvSubSerDetails
            rbtnJTifSubTlsOnly.Checked = JTas.JTasInvSubSerTotals
            '
            rbtnJTifMatDetails.Checked = JTas.JTasInvMatDetails
            rbtnJTifMatTlsOnly.Checked = JTas.JTasInvMatTotals
            '
            cboxJTifMatShowCost.Checked = JTas.JTasInvMatShowCost
            cboxJTifSubShowCost.Checked = JTas.JTasInvSubShowCost
            '
        End If
        Me.MdiParent = JTstart
        Me.Show()
    End Sub


    Private Sub btnJTifDone_Click(sender As Object, e As EventArgs) Handles btnJTifDone.Click
        Dim tmpItem As JTifslStruct
        tmpItem.Job = txtJTifJob.Text
        tmpItem.Subjob = txtJTifSubJob.Text
        Dim tmpKey As String = tmpItem.Job & tmpItem.Subjob
        ' Labor
        tmpItem.InvLabTaskSummary = rbtnJTifLabTask.Checked
        tmpItem.InvLabWorkerDetail = rbtnJTifLabDetails.Checked
        tmpItem.InvLabTotals = rbtnJTifLabTlsOnly.Checked
        ' Services
        tmpItem.InvSubSerDetails = rbtnJTifSubDetails.Checked
        tmpItem.InvSubSerTotals = rbtnJTifSubTlsOnly.Checked
        tmpItem.InvMatDetails = rbtnJTifMatDetails.Checked
        tmpItem.InvMatTotals = rbtnJTifMatTlsOnly.Checked
        ' Material/Subcontractor Cost Options
        tmpItem.InvMatShowCost = cboxJTifMatShowCost.Checked
        tmpItem.InvSubShowCost = cboxJTifSubShowCost.Checked
        slJTif.Add(tmpKey, tmpItem)
        '
        JTas.JTasInvLabTaskSummary = rbtnJTifLabTask.Checked
        JTas.JtasInvLabWorkerDetail = rbtnJTifLabDetails.Checked
        JTas.JTasInvLabTotals = rbtnJTifLabTlsOnly.Checked
        '
        JTas.JTasInvSubSerDetails = rbtnJTifSubDetails.Checked
        JTas.JTasInvSubSerTotals = rbtnJTifSubTlsOnly.Checked
        '
        JTas.JTasInvMatDetails = rbtnJTifMatDetails.Checked
        JTas.JTasInvMatTotals = rbtnJTifMatTlsOnly.Checked
        '
        JTas.JTasInvMatShowCost = cboxJTifMatShowCost.Checked
        JTas.JTasInvSubShowCost = cboxJTifSubShowCost.Checked
        '
        Me.Hide()
        JTjim.JobArrayFlush()
        JTas.BtnJTasInvoice_Continue()
        '
    End Sub
    '
    Private Sub btnJTifTurnOFF_Click(sender As Object, e As EventArgs) Handles btnJTifTurnOff.Click
        JTjim.JTjimTurnOffNonStdFrmt(txtJTifJob.Text, txtJTifSubJob.Text)
        '
        Me.Hide()
        JTas.BtnJTasInvoice_Continue()
    End Sub
    Public Function JTifGetArrayLine(tmpSub As Integer, ByRef tmpArrayItem As JTifslStruct) As String
        ' Msg to check to see if array records are being flushed. Seems to be working.
        ' MsgBox("tmpSub - " & tmpSub & vbCrLf & "slJTif.Count - " & slJTif.Count)
        If tmpSub = slJTif.Count Then
            Return "NODATA"
        End If
        Dim key As ICollection = slJTif.Keys
        tmpArrayItem = slJTif(key(tmpSub))
        Return "DATA"
    End Function
    Public Function JTifAddArrayLine(tmpArrayItem As JTifslStruct) As String
        Dim tmpKey As String = tmpArrayItem.Job & tmpArrayItem.Subjob
        slJTif.Add(tmpKey, tmpArrayItem)
        Return ""
    End Function
    Public Function JTifClrArray() As String
        slJTif.Clear()
        Return ""
    End Function
    Public Function JTifRemoveRecord(tmpJob As String, tmpSubjob As String) As String
        Dim tmpkey As String = tmpJob & tmpSubjob
        If slJTif.ContainsKey(tmpkey) Then
            slJTif.Remove(tmpkey)
            Return "DELETED"
        End If
        Return "RECNOTFND"
    End Function
    ''' <summary>
    ''' Function to make customer and/or job names changes. Used by JTjm.
    ''' </summary>
    ''' <param name="JTjmSettings"></param>
    ''' <returns></returns>
    Public Function JTifCustJobNameChg(JTjmSettings As JTjm.JTjmSettingsStru) As String


        ' Public Structure JTjmSettingsStru
        '      Dim ExCust As String
        '      Dim ExJob As String
        '      Dim NewCust As String
        '      Dim NewJob As String
        '      Dim CustNameChg As Boolean
        '      Dim JobNameChg As Boolean
        ' End Structure
        ' --------------------------------------------------
        ' Start of slJTif section
        ' --------------------------------------------------
        Dim tmpKeyCollection As New Collection()
        '
        Dim key As ICollection = slJTif.Keys
        Dim k As String = Nothing
        If JTjmSettings.CustNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTif(k).Job Then
                    tmpKeyCollection.Add(k)
                End If
            Next
            '         MsgBox("slJTif Records found - >" & tmpKeyCollection.Count & "< - " & JTjmSettings.ExCust & "- JTif~165")
        End If
        If JTjmSettings.JobNameChg = True Then
            For Each k In key
                If JTjmSettings.ExCust = slJTif(k).Job Then
                    If JTjmSettings.ExJob = slJTif(k).Subjob Then
                        tmpKeyCollection.Add(k)
                    End If
                End If
            Next
            '         MsgBox("slJTif Records found - >" & tmpKeyCollection.Count & "< - " & JTjmSettings.ExCust & "~" & JTjmSettings.ExJob & "- JTif~175")
        End If
        ' Modify logic for slJTif placed here .....
        For Each k In tmpKeyCollection
            Dim tmpItem As JTifslStruct = slJTif(k)
            slJTif.Remove(k)
            '
            tmpItem.Job = JTjmSettings.NewCust
            '
            If JTjmSettings.JobNameChg = True Then
                tmpItem.Subjob = JTjmSettings.NewJob
            End If
            Dim tmpKey As String = tmpItem.Job & tmpItem.Subjob
            slJTif.Add(tmpKey, tmpItem)
        Next
        ' --------------------------------------------------
        ' End of slJTif section
        ' --------------------------------------------------
        Return ""
    End Function

End Class